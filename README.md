This project contains custom collectors and support components for CMDAA.

This repository makes use of sub-modules. To fully clone, use:
```
git clone --recurse-submodules https://gitlab.com/cmdaa/custom-collectors.git
```

If you cloned without `--recurse-submodules`, you can update via:
```
git submodule update --init --recursive
```

### Disk metrics collection
- [sg3_utils](sg3_utils) : a custom version of the
  [public sg3_utils](https://github.com/hreinecke/sg3_utils) which enables
  access to SCSI drives through a MegaRaid controller that manages those
  drives;
- [python-sgutils](python-sgutils) : provides python bindings to the custom
  [sg3_utils](sg3_utils) library;
- [sg-logs](sg-logs) : a python collector for SCSI LOG SENSE-based drive
  metrics which makes use of [python-sgutils](python-sgutils) and through it
  the [sg3_utils](sg3_utils) library;
- [smartctl](smartctl) : a simple wrapper around the `smartctl` system
  utilty;

### Metrics bridge
- [prometheus-bridge](prometheus-bridge) : provides a bridge between
  Prometheus exporters, including [sg-logs](sg-logs) and the Kafka and
  TimescaleDb components of CMDAA;

### System metric sources
- [procfs-collector](procfs-collector) : a procfs-based metrics collector that
  provides metrics similar to the beat-driven metrics from the kernel-beat
  kernel module;

- [proc-mon](proc-mon) : an eBPF CO-RE module that provides process termination
  metrics similar to the asynchronous, process monitor metrics from the
  kernel-beat kernel module;

- [journald](journald) : documentation for forwarding logs to fluentd along
  with examples for accessing journald logs;

- [salt](salt) : documentation about using external loggers to forward salt
  logs to fluentd;

- [yum](yum) : example python script to access yum history and output details
  about each transaction (the set of involved packages and, for each package,
  the set of package-related files);

### Miscellaneous metrics
- [canaries/sleep](canaries/sleep) : a simple go-based canary to log variance
  between expected and actual sleep times to Prometheus;
