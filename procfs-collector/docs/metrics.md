## kernel-beat vs proc-based metrics:

    cpu       : os_cpu_[steal,soft_irq,iowait,sys,user,idle]
                + user, nice, sys, idle, iowait
                + irq
                + softIrq [soft_irq]
                + steal, guest, guestNice [guest_nice]

                ---
                Additional proc-fs metrics:

                bootTime
                cpuTime[ per-cpu ]
                  user, nice, system, idle, iowait,
                  irq, softirq, steal, guest, guestNice

                systemCpuTime
                  user, nice, system, idle, iowait,
                  irq, softirq, steal, guest, guestNice

                interrupts
                softInterrupts
                contextSwitches
                forks

                numberedInterrupts[]
                numberedSoftInterrupts[]

                processesRunning
                processesBlocked

    mem       : os_mem_[total,free]
                + total, free, available                ( bytes )
                + buffer, shared, cached                ( bytes )
                  swap                                  ( bytes )

                + swapTotal,  swapFree, swapCached      ( bytes )
                  swapUsed                              ( bytes )
                  highTotal,  highFree                  ( bytes )
                  lowTotal,   lowFree                   ( bytes )
                + commitLimit                           ( bytes )
                  commited                              ( bytes )
                + kernStack [kernelStack]               ( bytes )
                + vmallocTotal                          ( bytes )

                + active,     inactive                  ( pages )
                  activeAnon, inactiveAnon              ( pages )
                  activeFile, inactiveFile              ( pages )
                  unevictable                           ( pages )
                  mlock                                 ( pages )
                + dirty                                 ( pages )
                + writeback                             ( pages )
                  anonMapped                            ( pages )
                  nfsUnstable                           ( pages )

                + slab                                  ( pages )
                + slabReclaimable                       ( slabs )
                + slabUnreclaimable                     ( slabs )

                + pageTables                            ( count )
                + bounce [bounceBuffers]                ( pages )
                + writebackTmp                          ( pages )
                  unit                                  ( page size bytes )

                + hugePagesTotal    [hugepagesTotal]    ( huge pages )
                + hugePagesFree     [hugepagesFree]     ( huge pages )
                + hugePagesReserved [hugepagesReserved] ( huge pages )
                + hugePagesSurplus  [hugepagesSurplus]  ( huge pages )
                + hugePageSize      [hugepagesSize]     ( huge page size bytes )

                ---
                Additional proc-fs metrics:

                  directMap4K
                  directMap2M
                  directMap4M
                  directMap1G
                  anonPages
                  mapped
                  shmem
                  kernelReclaimable
                  hardwareCorrupted
                  anonHugePages
                  shmemHugePages
                  shmemPmdMapped

    filesystem: os_fs_volume_
                  id                                    ( filesystem device )
                  magic
                + type                                  ( char* )
                + filesTotal, filesFree
                + sizeTotal, sizeFree, sizeAvail

                + mountpoint                            ( char* )
                + devName                               ( char* )

    fsstat    : os_fs_size_[total,free,avail]
                os_fs_files_[total,free]
                os_fs_count
                  count                                 ( mounted filesystems )
                  filesTotal, filesFree                 ( files )
                  sizeTotal, sizeFree, sizeAvail        ( bytes )

    load      : os_load_
                  one                                   ( avg over  1 min )
                  five                                  ( avg over  5 mins )
                  fifteen                               ( avg over 15 mins )
                  cores                                 ( processing cores )

    net       : os_net_iface_
                  id                                    ( network device )
                  rxBytes       [_receive]              ( bytes )
                  rxPackets     [_receive]              ( packets )
                  rxErrors      [_receive_error]
                  rxErrDropped  [_receive_error_dropped]
                  rxErrFifo     [_receive_error_fifo]
                  rxErrFrame    [_receive_error_frame]
                  rxCompressed  [_receive_compressed]

                  txBytes       [os_net_iface_transmit] ( bytes )
                  txPackets     [os_net_iface_transmit] ( packets )
                  txErrors      [_transmit_error]
                  txErrDropped  [_transmit_error_dropped]
                  txErrFifo     [_transmit_error_fifo]
                  txErrCarrier  [_transmit_error_carrier]
                  txCompressed  [_transmit_compressed]

                  multicast     [_receive_multicast]    ( packets )
                  collisions    [_collision]            ( packets )

    uptime    : os_uptime
                  seconds

    procSum   : os_proc_[total,sleeping,running,...]
                  total                                 ( processes )
                  sleeping  'S'
                  running   'R'
                  idle      'D'
                  stopped   'T' | 't'
                  zombie    'Z'
                  dead      'X'
                  unknown   All others (x, K, W, P, N, n)

    process   : os_proc_process_
                (terminations indicated by labels
                  error|exit|exception|trap_num|sig_num)

                + name            [comm]                ( char* )
                + cmdLine         [args]                ( char* )
                + state                                 ( char* )
                    For process termination reports, the state will ALWAYS be
                    'R' (running)

                + pid
                + ppid
                  pgrp
                + uid
                + cgroups[]
                + cpuStart        [cpu_start (ns)]      ( seconds )
                + cpuUser         [cpu_user  (ns)]
                + cpuSys          [cpu_sys   (ns)]
                  fdOpen      --+
                  fdMax         | NOT available for process terminations
                  fdLimitHard   | but is available via proc-fs
                  fdLimitSoft --+
                + memRss          [mem_rss]
                  memShare
                + memSize         [total_vm]            ( pages )
                + memFaultsMinor  [min_flt]
                + memFaultsMajor  [maj_flt]

                  exception                             ( char* )
                + exit            [exit_code]
                  error
                  trapNum
                + sigNum          [signal]

                  regIp, regCs, regFlags, regSp, regSs,
                  regAx, regAxOrig, regBp, regCx, regDx,
                  regSi, regDi

                ---
                  gid
                  arg_full  (full length of `args`)
                  arg_len   (available)
                  exit_code
                  signal

                  task_size                             ( bytes )
                  hiwater_rss
                  hiwater_vm
                  locked_vm                             ( pages )
                  data_vm                               ( pages )
                  exec_vm                               ( pages )
                  stack_vm                              ( pages )

## Disk and Volume information

### /proc/partitions

This file contains partition block allocation information. A sampling of this
file from a basic system looks similar to the following:
```
major minor  #blocks  name
  3     0   19531250 hda
  3     1     104391 hda1
  3     2   19422585 hda2
253     0   22708224 dm-0
253     1     524288 dm-1
```

Most of the information here is of little importance to the user, except for
the following columns:

| Field   | description
| ------- | ----------------------------------------------
| major   | The major number of the device with this partition. The major number in the `/proc/partitions`, (3), corresponds with the block device ide0, in `/proc/devices`.
| minor   | The minor number of the device with this partition. This serves to separate the partitions into different physical devices and relates to the number at the end of the name of the partition.
| #blocks | Lists the number of physical disk blocks contained in a particular partition.
| name    | The name of the partition.


It appears that a `block` is 1024-bytes based upon:
- https://unix.stackexchange.com/questions/497717/interpreting-proc-partitions
- https://serverfault.com/questions/750960/units-for-size-in-proc-partitions-dont-make-sense


### /proc/diskstats

From: https://www.kernel.org/doc/Documentation/ABI/testing/procfs-diskstats
More detail: https://www.kernel.org/doc/Documentation/admin-guide/iostats.rst

The /proc/diskstats file displays the I/O statistics of block devices. Each
line contains the following 14 fields:

| Field | description
| ----- | ----------------------------------------------
| 1     | major number
| 2     | minor mumber
| 3     | device name
| 4     | reads completed successfully
| 5     | reads merged
| 6     | sectors read
| 7     | time spent reading (ms)
| 8     | writes completed
| 9     | writes merged
| 10    | sectors written
| 11    | time spent writing (ms)
| 12    | I/Os currently in progress
| 13    | time spent doing I/Os (ms)
| 14    | weighted time spent doing I/Os (ms)


Kernel 4.18+ appends four more fields for discard tracking putting the total at
18:

| Field | description
| ----- | ----------------------------------------------
| 15    | discards completed successfully
| 16    | discards merged
| 17    | sectors discarded
| 18    | time spent discarding


Kernel 5.5+ appends two more fields for flush requests:

| Field | description
| ----- | ----------------------------------------------
| 19    | flush requests completed successfully
| 20    | time spent flushing


### /sys/block/%dev%/stat

From: https://www.kernel.org/doc/Documentation/block/stat.txt

The stat file consists of a single line of text containing 11 decimal values
separated by whitespace.  The fields are summarized in the following table, and
described in more detail below.

| Name            | units        | description
| --------------- | ------------ | ----------------------------------------------
| read I/Os       | requests     | number of read I/Os processed
| read merges     | requests     | number of read I/Os merged with in-queue I/O
| read sectors    | sectors      | number of sectors read
| read ticks      | milliseconds | total wait time for read requests
| write I/Os      | requests     | number of write I/Os processed
| write merges    | requests     | number of write I/Os merged with in-queue I/O
| write sectors   | sectors      | number of sectors written
| write ticks     | milliseconds | total wait time for write requests
| in_flight       | requests     | number of I/Os currently in flight
| io_ticks        | milliseconds | total time this block device has been active
| time_in_queue   | milliseconds | total wait time for all requests
| discard I/Os    | requests     | number of discard I/Os processed
| discard merges  | requests     | number of discard I/Os merged with in-queue I/O
| discard sectors | sectors      | number of sectors discarded
| discard ticks   | milliseconds | total wait time for discard requests


#### read I/Os, write I/Os, discard I/0s

These values increment when an I/O request completes.


#### read merges, write merges, discard merges

These values increment when an I/O request is merged with an already-queued I/O
request.


#### read sectors, write sectors, discard_sectors

These values count the number of sectors read from, written to, or discarded
from this block device.  The "sectors" in question are the standard UNIX
512-byte sectors, not any device- or filesystem-specific block size.  The
counters are incremented when the I/O completes.


#### read ticks, write ticks, discard ticks

These values count the number of milliseconds that I/O requests have waited on
this block device.  If there are multiple I/O requests waiting, these values
will increase at a rate greater than 1000/second; for example, if 60 read
requests wait for an average of 30 ms, the read_ticks field will increase by
60*30 = 1800.

#### in_flight

This value counts the number of I/O requests that have been issued to the
device driver but have not yet completed.  It does not include I/O requests
that are in the queue but not yet issued to the device driver.


#### io_ticks

This value counts the number of milliseconds during which the device has had
I/O requests queued.


#### time_in_queue

This value counts the number of milliseconds that I/O requests have waited on
this block device.  If there are multiple I/O requests waiting, this value will
increase as the product of the number of milliseconds times the number of
requests waiting (see "read ticks" above for an example).


### /sys/block/%dev%/size

https://unix.stackexchange.com/questions/555838/calculate-disk-byte-size-exclusively-from-sys-block
> After more research I finally found the answer:
> 
> Linux always considers sectors to be 512 bytes long independently of the
> devices real block size.
> 
> According to the source:
> https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/linux/types.h?id=v4.4-rc6#n121
> 
> I was led there by this comment: Determine the size of a block device
> 
> So as silly as it may seem the [size] should simply be multiplied by 512.
> 
> I just hope it will still be a correct assumption 15 years from now, because
> I don't want my programs to be broken. physical_block_size seems wrong
> because it can sometimes be 4096 as I found with some Googling, but maybe it
> is still worth reading logical_block_size or hw_sector_size even if it will
> always be 512? Any comments on this?
>

For a deeper discussion of the use of 512 byte blocks, see
https://unix.stackexchange.com/questions/512945/what-units-are-the-values-in-proc-partitions-and-sys-dev-block-block-size/512959#512959

---

## cgroups

The set of cgroup subsystems is available via `/proc/cgroups` where each line
has the form `%subsys_name% %hierarchy% %num_cgroups% %enabled%`.


The actual cgroup data is available via the `/sys/fs/cgroup/` sysfs filesystem.
Files within this filesystem contain cgroup-subsystem-specific information.

The majority of files within the cgroup sysfs filesystem contain a single
integer value, but some have more involved formats described in the following
sub-sections.

### blkio.*

These files should have the form `%major%:%minor% %Stat% %value%`

`Stat` may be [ Read, Write, Sync, Async, Discard, Total ]

The final entry has the form `Total %value%`

Example:
```
259:0 Read 125723648
259:0 Write 4967289344
259:0 Sync 1694532608
259:0 Async 3398480384
259:0 Discard 0
259:0 Total 5093012992
7:1 Read 0
7:1 Write 0
7:1 Sync 0
7:1 Async 0
7:7 Discard 0
7:1 Total 0
7:0 Read 0
7:0 Write 0
7:0 Sync 0
7:0 Async 0
7:0 Discard 0
7:0 Total 0
Total 5093012992
```


### cgroup.events

Contains multiple lines of the form `%type% %value%`, example:
```
populated 1
frozen 0
```


### cgroup.freeze

Contains multiple lines representing the process ids assigned to the cgroup
that are currently frozen (guess).


### cgroup.procs

Contains multiple lines representing the process ids that are assigned to the
target cgroup based upon the current directory/path.


### cgroup.stat

Contains multiple lines of the form `%type% %value%`, example:
```
nr_descendants 131
nr_dying_descendants 0
```

### cgroup.threads

Contains multiple lines representing the thread ids that are assigned to the
target cgroup based upon the current directory/path.


### cgroup.type

Contains a single line that is a string representing the type of cgroup
(e.g. `domain`);


### cpuacct.*_percpu_*

Contains a single line with one measurement per cpu, example:
```
670794067810 150727641085 79750598640 41217730457 34868326260 37375260135 32745451119 306871560078 40149301349 36690819869 62211364889 37197135082 44206166398 46659049240 53041224177 47296142586
```


### cpuacct.stat

Contains multiple lines of the form `%type% %value`, example:
```
user 696511
system 409984
```


### cpuacct.usage_all

Contains multiple lines of the form `%cpu% %user_value% %sys_value`, example:
```
cpu user system
0 514793498496 0
1 663888840217 0
2 689719534052 0
3 688186668439 0
4 713375979497 0
5 706833217749 0
6 713547313624 0
7 444154457982 0
8 710372550624 0
9 717051129720 0
10 793930924586 0
11 719153199764 0
12 715879848064 0
13 711285410609 0
14 714320889525 0
15 649941119565 0
```


### cpu.stat

Contains multiple lines of the form `%name% %value%`, example:
```
nr_periods 0
nr_throttled 0
throttled_time 0
```

Example 2:
```
usage_usec 1361656185
user_usec 1171606377
system_usec 19004980
```


### cpu.uclamp.min

Contains a single line that is typically a floating point value.


### devices.list

Contains multiple lines of the form `%type% %major%:%minor% %mode%`, example:
```
c 1:3 rwm
c 1:5 rwm
c 1:7 rwm
c 1:8 rwm
c 1:9 rwm
c 5:0 rwm
c 5:2 rwm
c 0:0 rwm
b 0:0 rwm
c 136:* r
```


### [cpu,io,memory].pressure

Contains multiple lines of the form
`%type% avg10=%value% avg60=%value% avg300=%value% total=%value%`, example:
```
some avg10=0.00 avg60=0.00 avg300=0.00 total=24047903
full avg10=0.00 avg60=0.00 avg300=0.00 total=2365284
```


### memory.numa_stat

Contains multiple lines of the form `%name%=%value% N0=%value%`, example:
```
total=0 N0=0
file=0 N0=0
anon=0 N0=0
unevictable=0 N0=0
hierarchical_total=1799853 N0=1800182
hierarchical_file=602151 N0=602285
hierarchical_anon=1195788 N0=1195879
hierarchical_unevictable=1914 N0=201
```


### memory.stat

Contains multiple lines of the form `%name% %value%`, example:
```
cache 0
rss 0
rss_huge 0
shmem 0
mapped_file 0
dirty 0
writeback 0
pgpgin 0
pgpgout 0
pgfault 0
pgmajfault 0
inactive_anon 0
active_anon 0
inactive_file 0
active_file 0
unevictable 0
hierarchical_memory_limit 9223372036854771712
total_cache 2784550912
total_rss 4586082304
total_rss_huge 0
total_shmem 318951424
total_mapped_file 691384320
total_dirty 0
total_writeback 1081344
total_pgpgin 92216288
total_pgpgout 90416323
total_pgfault 69713925
total_pgmajfault 7326
total_inactive_anon 310751232
total_active_anon 4587196416
total_inactive_file 1229623296
total_active_file 1236787200
total_unevictable 783974
```


### memory.*.slabinfo

Contains multiple lines, example:
```
slabinfo - version: 2.1
# name            <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> : tunables <limit> <batchcount> <sharedfactor> : slabdata <active_slabs> <num_slabs> <sharedavail>
kmalloc-rcl-96       168    168     96   42    1 : tunables    0    0    0 : slabdata      4      4      0
TCPv6                  0      0   2432   13    8 : tunables    0    0    0 : slabdata      0      0      0
...
```


### pids.events

A single line of the form `max %value%`


### pids.max

A single line with either `max` or `%value%`.
