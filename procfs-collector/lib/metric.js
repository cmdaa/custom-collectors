/**
 *  A metric class for collected timestamp, value, and labels.
 *  @class  Metric
 */
class Metric {
  /**
   *  Create a new instance.
   *  @constructor
   *  @param  data                      The incoming metric data {Object};
   *  @param  data.name                 The metric name {String};
   *  @param  data.value                The metric value {Number};
   *  @param  data.labels               A set of key/value pairs providing
   *                                    additional context for this metric
   *                                    {Object};
   *  @param  [data.timestamp = now()]  A timestamp representing when this
   *                                    metric data was collected {Date};
   */
  constructor( data ) {

    Object.defineProperties( this, {
      /**
       *  The normalized metric data
       *  @property _data
       *  @readonly
       */
      _data : { value: _normalizeData( data ) },
    });
  }

  /**
   *  Retrieve the metric name
   *  @property name
   *  @readonly
   */
  get name()  { return this._data.name }

  /**
   *  Retrieve the metric value
   *  @property value
   *  @readonly
   */
  get value() { return this._data.value }

  /**
   *  Retrieve the metric labels
   *  @property labels
   *  @readonly
   */
  get labels()  { return this._data.labels }

  /**
   *  Retrieve the metric timestamp
   *  @property timestamp
   *  @readonly
   */
  get timestamp()  { return this._data.timestamp }

  /**
   *  Add a new label, maintaining sorted order.
   *  @method addLabel
   *  @param  label   The label name {String};
   *  @param  value   The label value {String};
   *
   *  @return the new set of labels {Object};
   */
  addLabel( label, value ) {
    const labels  = this._data.labels;

    labels[ label ] = value;

    this._data.labels = _normalizeLabels( labels );

    return this._data.labels;
  }

  /**
   *  Return a string version of this metric in Prometheus Exposition format.
   *  @method toString
   *
   *  @return A Prometheus Exposition formatted version of this metric
   *          {String};
   */
  toString() { return this.toPrometheus() }

  /**
   *  Return a string version of this metric in Prometheus Exposition format.
   *  @method toPrometheus
   *
   *  @return A Prometheus Exposition formatted version of this metric
   *          {String};
   */
  toPrometheus() {
    const labels    = this.labels;
    const labelStr  = Object.keys( labels )
                        .map( key => {
                          const val = _esc_prometheusString( labels[ key ] );

                          return `${key}="${val}"`
                        })
                        .join(',');
    const nameLabel = this.name
                    + (labelStr.length > 0
                        ? `{${labelStr}}`
                        : '');

    return `${nameLabel} ${this.value} ${this.timestamp}`;
  }

  /**
   *  Return a simple object representation of this instance.
   *  @method toJSON
   *
   *  @return  A simple object representation of this instance {Object}.
   */
  toJSON() {
    return this._data;
  }
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Escape the given string for use in prometheus data.
 *  @method _esc_prometheusString
 *  @param  src   The source string {String};
 *
 *  Escape characters:
 *    "   \"
 *    \n  \\n
 *    \   \\
 *
 *  @return The escaped version of `str` {String};
 *  @private
 */
function _esc_prometheusString( src ) {
  const chars = [];

  if (typeof(src) !== 'string') { src = String(src) }

  for (let idex = 0; idex < src.length; idex++) {
    let   seq   = src[ idex ];
    const code  = src.codePointAt( idex );

    switch( code ) {
      case 10 : seq = '\\n';  break;
      case 34 : seq = '\\"';  break;
      case 92 : seq = '\\';   break;
    }

    chars.push( seq );
  }

  return chars.join('');
}

/**
 *  Validate and normalize incoming metric data.
 *  @method _normalizeData
 *  @param  data
 *  @param  data                      The incoming metric data {Object};
 *  @param  data.name                 The metric name {String};
 *  @param  data.value                The metric value {Number};
 *  @param  data.labels               A set of key/value pairs providing
 *                                    additional context for this metric
 *                                    {Object};
 *  @param  [data.timestamp = now()]  A timestamp representing when this
 *                                    metric data was collected {Date};
 *
 *  @return A normalized version of `data` {Object};
 *          On error, throw an error {Error};
 *  @private
 */
function _normalizeData( data ) {
  // Requires `name` and `value`
  if (typeof(data.name) !== 'string' || data.name.length < 1) {
    throw new Error('invalid metric name (must be a non-empty string)');
  }
  if (data.value === undefined) {
    throw new Error('missing metric value');
  }

  // Normalize 'labels'
  data.labels = _normalizeLabels( data.labels );

  // Normalize 'timestamp'
  data.timestamp = _normalizeTimestamp( data.timestamp );

  return data;
}

/**
 *  Validate and normalize metric labels
 *  @method _normalizeLabels
 *  @param  labels  A set of key/value pairs providing additional context for
 *                  this metric {Object};
 *
 *  @return A normalized, key-sorted version of `labels` {Object};
 *          On error, throw an error {Error};
 *  @private
 */
function _normalizeLabels( labels ) {
  const sorted      = {};

  if (typeof(labels) === 'object' && ! Array.isArray( labels )) {
    Object.keys( labels )
      .sort()
      .forEach( key => {
        const val = labels[ key ];

        sorted[ key ] = val;
      });
  }

  return sorted;
}

/**
 *  Normalize metric timestamp, defaulting to now().
 *  @method _normalizeTimestamp
 *  @param  timestamp The incoming timescampe {Number | String | Date};
 *
 *  @return A normalized timestamp {Number};
 *          On error, throw an error {Error};
 *  @private
 */
function _normalizeTimestamp( timestamp ) {

  switch( typeof(timestamp) ) {
    case 'number':
      if (Number.isNaN( timestamp )) {
        throw new Error(`Invalid timestamp number [ ${timestamp} ]`);
      }
      break;

    case 'string': {
      const ts  = new Date( timestamp );
      const time  = ts.getTime();

      if (Number.isNaN( time )) {
        throw new Error(`Invalid timestamp string [ ${timestamp} ]`);
      }

      timestamp = time;
    } break;

    case 'object':
      if (timestamp instanceof Date) {
        timestamp = timestamp.getTime();
      }
      break;
  }

  if (typeof(timestamp) !== 'number') {
    // Fall-back : Create a new timestamp
    timestamp = Date.now();
  }

  return timestamp;
}

/* Private helpers }
 ****************************************************************************/

module.exports  = Metric;
