/**
 *  Provide access to the linux `df` utility to retrieve filesystem
 *  information.
 *
 */
const ExecSync = require('child_process').execSync

/**
 *  Provide access to filesystem information from the linux `df` utility
 *  @method df
 *  @param  [exclude = null]  If null, exclude no filesystem types. If an
 *                            array, the set of filesystem types to exclude
 *                            {null | Array};
 *
 *  @return An array of information about mounted filesystems {Array};
 *            { source:
 *              fstype:
 *              size  : (in bytes),
 *              used  : (in bytes),
 *              avail : (in bytes),
 *              target:
 *            }
 */
function df( exclude = null ) {
  const command = [
    'df',
    '--block-size=1', // Scale sizes to bytes
    '--output=source,fstype,itotal,iused,iavail,size,used,avail,target',
  ];

  if (Array.isArray( exclude )) {
    exclude.forEach( type => command.push( `--exclude-type=${type}` ) );
  }

  const stdout  = ExecSync( command.join(' '), {encoding:'utf-8'} );

  return _parse_df( stdout );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse the response from a df child process.
 *  @method _parse_df
 *  @param  stdout  stdout output {String};
 *
 *  @return void
 *  @private
 */
function _parse_df( stdout ) {
  /* Parse stdout:
   *  Filesystem  Type  Inodes  Iused Ifree 1B-blocks Used  Avail Mounted
   *  0           1     2       3     4     5         6     7     8
   */
  const entries = stdout
    .trim()
    .split(/\r|\n|\r\n/)  // split into rows
    .slice(1)             // strip column headers away
    .map( (row,idex) => {
      const cols  = row.split(/\s+/);
      const entry = {
        devName   : cols[0],
        type      : cols[1],
        filesTotal: parseInt( cols[2], 10 ),
        filesUsed : parseInt( cols[3], 10 ),
        filesFree : parseInt( cols[4], 10 ),
        sizeTotal : parseInt( cols[5], 10 ),
        sizeUsed  : parseInt( cols[6], 10 ),
        sizeAvail : parseInt( cols[7], 10 ),
        mountpoint: cols[8],
      };

      if (entry.filesTotal < 1) {
        /* :TODO: Retrieve file information using
         *          statvfs
         *          npm install statvfs
         *
         */
      }

      return entry;
    });

  return entries;
}
/* Private helpers }
 ****************************************************************************/
module.exports  = df;
