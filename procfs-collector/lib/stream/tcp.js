const Net = require('net');

/**
 *  A flow-controlled TCP client stream connected to a remote TCP server.
 *  @class  TcpStream
 */
class TcpStream {
  // TCP xon/xoff flow-control indicators
  static xon  = Buffer.from( [17] );  // Transmit on,  SIGSTART, ^Q, ASCII 17
  static xoff = Buffer.from( [19] );  // Transmit off, SIGSTOP,  ^S, ASCII 19

  get xon()   { return this.constructor.xon }
  get xoff()  { return this.constructor.xoff }

  /**
   *  Create a new instance, initializing this collector.
   *  @constructor
   *  @param  config              Stream configuration {Object};
   *  @param  config.collector    The controlling collector instance
   *                              {Collector};
   *  @param  config.host         The remove server host {String};
   *  @param  config.port         The remove server port {String | Number};
   */
  constructor( config ) {
    if (typeof(config.collector) !== 'object') {
      throw new Error(`missing/invalid 'config.collector'`);
    }
    if (typeof(config.host) !== 'string' || config.host.length < 1) {
      throw new Error(`missing 'config.host'`);
    }
    if (typeof(config.port) !== 'string' && typeof(config.port) !== 'number') {
      throw new Error(`missing 'config.port'`);
    }

    Object.defineProperties( this, {
      /**
       *  Configuration data
       *  @property config {Object};
       *  @readonly
       */
      config  : { value: config },

      /**
       *  The active socket
       *  @property sock {Socket};
       */
      sock    : { value: null, writable: true },
    });

    this.open();
  }

  /**
   *  Open a socket to the configured host/port.
   *  @method open
   *
   *  @return The new socket {Socket};
   */
  open() {
    if (this.sock != null) {
      this.sock.end();
    }

    this.sock = new Net.Socket();

    this.sock.once('connect', ()     => this._handleConnect() );

    this.sock.on(  'error',   (err)  => this._handleError( err ) );
    this.sock.on(  'end',     ()     => this._handleEnd() );
    this.sock.on(  'data',    (data) => this._handleData( data ) );

    console.log('=== TcpStream: Connecting to %s:%s ...',
                this.config.host, this.config.port);

    this.sock.connect( this.config.port, this.config.host );

    return this.sock;
  }

  /**
   *  Write data to the current socket (if open).
   *  @method write
   *  @param  data    The data to write {String | Buffer};
   *
   *  @return void
   */
  write( data ) {
    if (this.sock) {
      this.sock.write( data );
    }
  }

  /**
   *  Close the current socket connection.
   *  @method close
   *
   *  @return void
   */
  close() {
    console.log('>>> TcpStream.close( %s:%s )',
                  this.config.host, this.config.port);

    if (this.sock) {
      this.sock.end();
      this.sock = null;
    }
  }

  /**************************************************************************
   * Private helpers {
   */

  /**
   *  Handle a socket 'connect' event.
   *  @method _handleConnect
   *
   *  @return void
   *  @protected
   */
  _handleConnect() {
    console.log('>>> TcpStream.connect( %s:%s )',
                this.config.host, this.config.port);
  }

  /**
   *  Handle a socket 'end' event.
   *  @method _handleEnd
   *
   *  @return void
   *  @protected
   */
  _handleEnd() {
    console.log('>>> TcpStream.end( %s:%s )',
                this.config.host, this.config.port);

    // Notify the collector to stop
    this.config.collector.stop();
  }

  /**
   *  Handle a socket 'error' event.
   *  @method _handleError
   *  @param  err       The error {Error};
   *
   *  @return void
   *  @protected
   */
  _handleError( err ) {
    console.log('*** TcpStream.error( %s:%s ):',
                this.config.host, this.config.port, err);

    // Notify the collector to stop
    this.config.collector.stop();
  }

  /**
   *  Handle a socket 'data' event.
   *  @method _handleData
   *  @param  data  The incoming socket data {Buffer};
   *
   *  @return void
   *  @protected
   */
  _handleData( data ) {
    switch( data[0] ) {
      case this.xoff[0]: // Transmit off / Flow stop
        console.log('=== TcpStream.data( %s:%s ): xoff',
                    this.config.host, this.config.port);
        this.config.collector.pause();
        break;

      case this.xon[0]:  // Transmit on / Flow start
        console.log('=== TcpStream.data( %s:%s ): xon',
                    this.config.host, this.config.port);
        this.config.collector.resume();
        break;

      default:
        console.log('=== TcpStream.data( %s:%s ):',
                    this.config.host, this.config.port, data);
        break;
    }
  }

  /* Private helpers }
   **************************************************************************/
}

module.exports  = TcpStream;
