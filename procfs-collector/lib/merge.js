/**
 *  Merge configuration.
 *  @property config {Object};
 */
const config  = {
  /**
   *  Should duplicate values within merged arrays be filtered out?
   *  @property uniqueArray {Boolean};
   */
  uniqueArray: true,
};

/**
 *  Provide deep merging of objects and arrays.
 *  @method merge
 *  @param  target    The target {Object | Array};
 *  @param  ...       The source(s) {Object | Array};
 *
 *  @return A new object {Object | Array};
 */
function merge( target, ...sources ) {
  return sources.reduce( (res, src) => {
    return _mergeTwo( res, src, config );

  }, _cloneObject( target ));
}

/****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Provide deep merging of objects and arrays.
 *  @method _mergeTwo
 *  @param  target  The target {Object | Array};
 *  @param  source  The source {Object | Array};
 *
 *
 *  @note Based off the deepmerge and is-mergable-object npm packages for
 *        encapsulation, and limiting of external npm packages.
 *
 *  @return A new object {Object | Array};
 *  @private
 */
function _mergeTwo( target, source ) {
  const target_isArray  = Array.isArray( target );
  const source_isArray  = Array.isArray( source );
  const typesMatch      = (target_isArray === source_isArray);

  if (! typesMatch) {
    return _cloneObject( source );

  } else if (source_isArray) {
    // Merge arrays
    return _mergeArray( target, source );

  } else {
    // Merge non-arrays (objects)
    return _mergeObject( target, source );

  }
}


/**
 *  Is the given value a mergable object?
 *  @method _isMergableObject
 *  @param  val   The target value {Mixed};
 *
 *  @return true | false {Boolean};
 *  @private
 */
function _isMergableObject( val ) {
  const stringVal     = Object.prototype.toString.call( val );
  const isNonNullObj  = (!!val && typeof(val) === 'object');
  const isSpecial     = (stringVal === '[object RegExp]' ||
                         stringVal === '[object Date]');
  const res           = (isNonNullObj && !isSpecial);

  /*
  console.log('merge._isMergableObject():', val);
  console.log('  stringVal   :', stringVal);
  console.log('  isNonNullObj:', isNonNullObj);
  console.log('  isSpecial   :', isSpecial);
  console.log('  res         :', res);
  // */

  return res;
}

/**
 *  Return an empty object of the same type as the provided object.
 *  @method _emptyObject
 *  @param  obj   The target object {Array | Object};
 *
 *  @return An empty object of the same type as `obj` {Array | Object};
 *  @private
 */
function _emptyObject( obj ) {
  return (Array.isArray(obj) ? [] : {});
}

/**
 *  Retrieve the set of enumerable, own-property symbols for the given object.
 *  @method _getSymbols
 *  @param  obj   The target object {Object};
 *
 *  @return The set of symbols {Array};
 *  @private
 */
function _getSymbols( obj ) {
  return (Object.getOwnPropertySymbols
            ?  Object.getOwnPropertySymbols(obj)
                .filter( sym => {
                  return obj.propertyIsEnumerable( sym );
                })
            : []);
}

/**
 *  Retrieve the full set of keys for the given object.
 *  @method _getKeys
 *  @param  obj   The target object {Object};
 *
 *  @return The set of keys {Array};
 *  @private
 */
function _getKeys( obj ) {
  return Object
          .keys( obj )
          .concat( _getSymbols( obj ) );
}

/**
 *  Is the given property "in" the given object?
 *  @method _propIsInObject
 *  @param  target  The target object {Object};
 *  @param  key     The target key {String};
 *
 *  @return true | false {Boolean};
 *  @private
 */
function _propIsInObject( target, key ) {
  try {
    return key in target

  } catch(ex) {
    return false;

  }
}

/**
 *  Is the given property of the given object safe for merging?
 *  @method _propIsUnsafe
 *  @param  target  The target object {Object};
 *  @param  key     The target key {String};
 *
 *  Protects from prototype poisoning and unexpected merging up the prototype
 *  chain.
 *
 *  @return true | false {Boolean};
 *  @private
 */
function _propIsUnsafe( target, key ) {
  /* Properties are:
   *  1) safe to merge if they don't exist in the target yet 
   *  2) unsafe if they exist up the prototype chain;
   *  3) unsafe if they are nonenumerable;
   */
  return (_propIsInObject( target, key ) &&                     // 1
          !( Object.hasOwnProperty.call( target, key ) &&       // 2
             Object.propertyIsEnumerable.call( target, key )))  // 3

}

/**
 *  Create a clone of the given object.
 *  @method _cloneObject
 *  @param  obj   The target object {Array | Object};
 *
 *  @return A clone of `obj` {Array | Object};
 *  @private
 */
function _cloneObject( obj ) {
  const res = (_isMergableObject( obj )
                ? _mergeTwo( _emptyObject(obj), obj )
                : obj);

  /*
  console.log('merge._cloneObject():', obj);
  console.log('  res:', res);
  // */

  return res;
}

/**
 *  Merge the `source` array into the `target` array.
 *  @method _mergeArray
 *  @param  target  The target {Array};
 *  @param  source  The source {Array};
 *
 *  @return A new array merging `source` into `target` {Array};
 *  @private
 */
function _mergeArray( target, source ) {
  let res = target
                .concat( source )
                .map( el => {
                  return _cloneObject( el );
                });

  if (config.uniqueArray) { res = Array.from( new Set(res) ) }

  /*
  console.log('merge._array():');
  console.log('   target:', target);
  console.log('   source:', source);
  console.log('   res   :', res);
  // */

  return res;
}

/**
 *  Merge the `source` object into the `target` object.
 *  @method _mergeObject
 *  @param  target  The target {Object};
 *  @param  source  The source {Object};
 *
 *  @return A new object merging `source` into `target` {Object};
 *  @private
 */
function _mergeObject( target, source ) {
  const res = {};

  if (_isMergableObject( target )) {
    // Include all keys from the target.
    _getKeys( target )
      .forEach( key => {
        res[ key ] = _cloneObject( target[key] );
      });
  }

  // Include all keys from the source
  _getKeys( source )
    .forEach( key => {
      if (_propIsUnsafe( target, key )) { return }

      if (_propIsInObject( target, key ) && _isMergableObject( source[key] )) {
        res[ key ] = _mergeTwo( target[key], source[key] );

      } else {
        res[ key ] = _cloneObject( source[key] );
      }
    });

  /*
  console.log('merge._object():');
  console.log('   target:', target);
  console.log('   source:', source);
  console.log('   res   :', res);
  // */

  return res;
}

/* Private helpers }
 ****************************************************************************/

module.exports = {
  config: config,
  merge : merge,
};
