/**
 *  Provide access to cgroup information.
 *
 *  Based off code from:
 *    https://gist.github.com/lovasoa/8691344
 */
const Fs    = require("fs");
const Path  = require("path");

/**
 *  Retrieve the set of cgroups
 *  @method get_cgroups
 *  @param  [procRoot = '/proc']  The root of the proc filesystem {String};
 *
 *  @return The set of cgroups, indexed by cgroup name {Object};
 *            { name    : Subsystem name {String},
 *              hid     : Hierarchy id {Number},
 *              num     : Number of cgroups {Number},
 *              enabled : Is this subsystem enabled {Boolean},
 *            }
 */
function get_cgroups( procRoot = '/proc' ) {
  const cgroups = {};

  Fs.readFileSync( Path.join( procRoot, 'cgroups' ), {encoding:'utf-8'} )
    .trim()
    .split('\n')
    .slice(1)     // remove the header
    .forEach( line => {
      /* #subsys_name hierarchy num_cgroups enabled
       * 0            1         2           3
       */
      const cols  = line.split('\t');
      const entry = {
        name    : cols[0],
        hid     : Number(cols[1]),
        num     : Number(cols[2]),
        enabled : Boolean( cols[3] ),
      };

      cgroups[ entry.name ] = entry;
    });

  return cgroups;
}

/**
 *  Attempt to read the contents of a cgroup entry.
 *  @method read_cgroup
 *  @param  path    The path to the target cgroup file {String};
 *
 *  @return The contents, split into lines (Error if not accessible)
 *          {Array | Error};
 */
function read_cgroup( path ) {
  let res;

  try {
    res = Fs.readFileSync( path, {encoding:'utf-8'} )
            .trim()
            .split('\n')
            .filter( line => line );

  } catch(ex) {
    res = ex;
  }

  return res;
}

/**
 *  Given a path to a directory, this generator recursively walks all
 *  subdirectories, yielding the path for each file.
 *  @method walk
 *  @param  [dirPath = '/sys/fs/cgroup/'] The path to the target directory
 *                                        {String};
 *
 *  @yield  For directories, a recursive call to walk();
 *          For files, the absolute path to the file {String};
 */
async function* walk( dirPath = '/sys/fs/cgroup/' ) {
  for await (const dir of await Fs.promises.opendir(dirPath)) {
    const path  = Path.join(dirPath, dir.name);

    if (dir.isDirectory()) { yield* walk(path) }
    else if (dir.isFile()) { yield path }
  }
}

module.exports  = {
  get : get_cgroups,
  read: read_cgroup,
  walk,         /* for await (const path of walk( '/path' ) {
                 *   ... process `path` ...
                 * }
                 */
};
