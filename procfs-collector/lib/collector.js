const Os          = require('os');
const Fs          = require('fs');
const Url         = require('url');
const Net         = require('net');
const { Procfs }  = require('@stroncium/procfs');
const TcpStream   = require('./stream/tcp');
const Merge       = require('./merge');

// assert( Merge.config.uniqueArray === true );

/**
 *  The primary procfs collector, which manages a beat timer, collection
 *  modules, and differential metric reporting.
 *  @class  Collector
 */
class Collector {
  static  defaults  = {
    beatTime: 5,            // The collection beat time in seconds;
    syncBeat: 60,           /* The number of beats at which differential
                             * state will be reset and all metrics reported
                             * regardless of difference;
                             *
                             * With a beatTime of 5s, a sync would trigger
                             * every 5 minutes;
                             */
    path    : '/proc',      /* The absolute path to the system mount-point for
                             * procfs;
                             */
    labels  : {             /* Additional, system-level labels to include in
                             * all metrics.
                             */
      node:  null,          // @note Initialized in the constructor
    },

    output  : {             // Output configuration
      endpoint: 'stderr',     // endpoint: protocol[://host:port[/path]]
      format  : 'prometheus', // format  : [ prometheus | json | uni-variate ]
    },

    // Module-specific configuration
    fs      : {
      // The set of filesystem types to exclude (null == none)
      exclude: [ 'devtmpfs', 'tmpfs', 'squashfs', 'overlay' ],
    },
    cgroups : {
      sysfs : '/sys',
    },
  };

  /**
   *  Create a new instance, initializing this collector.
   *  @constructor
   *  @param  config                  Collector configuration {Object};
   *  @param  [config.beatTime = 1]   The collection beat time in seconds
   *                                  {Number};
   *  @param  [config.syncBeat = 60]  The number of beats at which differential
   *                                  state will be reset and all metrics
   *                                  reported regardless of difference
   *                                  {Number};
   *  @param  [config.path = '/proc'] If provided, the absolute path to the
   *                                  system mount-point for procfs {String};
   *  @param  [config.labels = {}]    If provided, a set of additional,
   *                                  system-level labels to include in all
   *                                  metrics {Object};
   *  @param  [config.output.endpoint = 'stdout']
   *                                  Output endpoint {String} in the form:
   *                                    'protocol[://host:port[/path]]'
   *  @param  [config.output.format = 'prometheus']
   *                                  Output format to send to the configured
   *                                  endpoint (prometheus | json | uni-variate)
   *                                  {String};
   *  @param  [config.fs.exclude]     Module-specific configuration for the fs
   *                                  module specifying the set of filesystem
   *                                  types to exclude from the report {Array}
   *                                  [ 'devtmpfs', 'tmpfs', 'squashfs',
   *                                    'overlay' ];
   *
   *  @note By default, `labels` will include 'node' with the value of the
   *        system hostname;
   *
   *  @note Valid endpoint protocols: tcp, kafka, stdout, stderr
   *          'tcp'     REQUIRES a `host` and `port` and IGNORES any `path`;
   *          'kafka'   REQUIRES a `host`, `port`, and uses `path` as the kafka
   *                    topic **NYI**;
   *          'stdout'  IGNORES any `host`, `port`, or `path;
   *          'stderr'  IGNORES any `host`, `port`, or `path;
   */
  constructor( config ) {
    // Initialize the system hostname as the 'node' label
    this.constructor.defaults.labels.node = _hostname();

    if (config.labels) {
      if (config.labels.node == null) {
        // Remove the null 'node' label so the default can over-ride
        delete config.labels.node;
      }
    }

    // Assign defaults overridden by the incoming `config`
    const finalConfig = Merge.merge( this.constructor.defaults, config );

    Object.defineProperties( this, {
      /**
       *  Configuration data
       *  @property config {Object};
       *  @readonly
       */
      config  : { value: finalConfig },

      /**
       *  Procfs access instance
       *  @property procfs {Procfs};
       *  @readonly
       */
      procfs  : { value: new Procfs( config.path ) },

      /**
       *  The current beat.
       *  @property beat {Number};
       */
      beat    : { value: -1, writable: true },

      /**
       *  When was the beat timer been started (0 == not started).
       *  @property started {Number};
       */
      started : { value: 0, writable: true },

      /**
       *  Periodic beat timer.
       *  @property timer {Timer};
       */
      timer   : { value: null, writable: true },
    });

    /* Seperate to allow _loadModules() access to `this.config` and
     * `this.procfs`
     */
    Object.defineProperties( this, {
      /**
       *  The set of collection modules
       *  @property modules {Array of Module instances};
       *  @readonly
       */
      modules : { value: _loadModules( this ) },
    });

  }

  /**
   *  Start the collection beat.
   *  @method start
   *
   *  @return An indication of the start time {Number};
   */
  start() {
    if (! this.timer) {
      console.log('>>> Collector.start()');
      this._outStream = _open_outputStream( this );
      if (this._outStream == null) {
        return;
      }

      this.started = Date.now();
      this.beat    = -1;

      // Trigger the initial sync beat
      _handleBeat( this );

      // Initialize the interval timer
      this.timer = setInterval( () => _handleBeat( this ),
                                this.config.beatTime * 1000 );
    }

    return this.started;
  }

  /**
   *  Stop the collection beat.
   *  @method stop
   *
   *  @return An indication of the start time {Number};
   */
  stop() {
    const started = this.started;

    if (started) {
      console.log('>>> Collector.stop()');

      this.started = 0;

      this.pause();

      if (this._outStream) {
        if (this._outStream.close) {
          this._outStream.close();
        } else if (this._outStream.end) {
          this._outStream.end();
        }

        this._outStream = null;
      }
    }

    return started;
  }

  /**
   *  Resume the beat timer.
   *  @method resume
   *
   *  @return void
   */
  resume() {
    if (! this.timer) {
      // Initialize the interval timer
      console.log('>>> Collector.resume()');
      this.timer = setInterval( () => _handleBeat( this ),
                                this.config.beatTime * 1000 );
    }
  }

  /**
   *  Pause the beat timer.
   *  @method pause
   *
   *  @return void
   */
  pause() {
    if (this.timer) {
      console.log('>>> Collector.pause()');
      clearInterval( this.timer );

      this.timer = null;
    }
  }

  /**
   *  Report the given set of metrics to the configured endpoint.
   *  @method report
   *  @param  metrics   The set of metrics to report
   *                    {Array of Metric instances};
   *  @param  module    The module that generated `metrics` {Module};
   *
   *  @return An indication of whether metrics were reported {Boolean};
   */
  report( metrics, module ) {
    if (! this.started) { return false }

    /*
    console.log('beat %d: %s, %d metrics:',
                this.beat, module.constructor.name, metrics.length);
    // */
    metrics.forEach( metric => {
      let output  = null;

      switch( this.config.output.format ) {
        case 'uni-variate':
          output = _format_uniVariate( metric );
          break;

        case 'json':
          output = _format_json( metric );
          break;

        case 'prometheus':
        // fall-through
        default:
          output = _format_prometheus( metric );
          break;
      }

      if (this._outStream) {
        this._outStream.write( output );
        this._outStream.write( '\n' );
      } else {
        console.error('*** No _outStream for output[ %s ]', output);
      }
    });

    return true;
  }
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Handle a collection beat
 *  @method _handleBeat
 *  @param  collector   The controlling instance {Collector};
 *
 *  @return void
 *  @private
 */
function _handleBeat( collector ) {
  if (collector._pendingBeat) {
    console.error('*** Collector._handleBeat(): Previous beat still '
                  +       'being processed. Consider increasing the '
                  +       'beat time from %d seconds ...',
                  collector.config.beatTime);
    return;
  }
  collector._pendingBeat = true;

  // Increment our beat counter
  collector.beat++;

  if (collector.beat % collector.config.syncBeat === 0) {
    // Sync beat -- reset the differential state of all modules
    collector.modules.forEach( module => module.reset() );
  }

  /* Invoke gather for all modules and report the generated differential
   * metrics
   */
  const pending = [];
  collector.modules.forEach( async (module) => {
    pending.push(
      module.gather()
        .then( metrics => {
          collector.report( metrics, module );
        })
        .catch( err => {
          console.error('*** Collector._handleBeat(): %s.gather(): ERROR:',
                        module.constructor.name, err);
        })
    );
  });

  Promise.all( pending )
    .catch( err => {
      console.error('*** Collector._handleBeat(): ERROR:', err);
    })
    .finally( () => {
      collector._pendingBeat = false;
    });
}

/**
 *  Retrieve the hostname.
 *  @method _hostname
 *
 *  If this is running within a k8s pod that has injected host-based
 *  information, we can pull the host name of the containing node instead of
 *  the name of this pod.
 *
 *  :NOTE: This requires the deployment/daemonset to use the k8s DownwardAPI to
 *         inject this information, for example:
 *          env:
 *            - name: K8S_NODE_NAME
 *              valueFrom:
 *                fieldRef:
 *                  fieldPath: spec.nodeName
 *
 *  If this information is not avaialble, simply return the OS-reported
 *  hostname.
 *
 *  @return The host name {String};
 *  @private
 */
function _hostname() {
  return process.env['K8S_NODE_NAME'] || Os.hostname();
}

/**
 *  Load all modules from `./module`
 *  @method _loadModules
 *  @param  collector   The controlling instance {Collector};
 *
 *  @return An array of initialized modules {Array of Module instances};
 *  @private
 */
function _loadModules( collector ) {
  const modules     = [];
  const modulePath  = `${__dirname}/module`;
  const dir         = Fs.opendirSync( modulePath );
  let   entry       = null;

  while ( entry = dir.readSync() ) {
    const name  = entry.name;

    if (!name.endsWith('.js') || name === 'index.js') { continue }

    const path    = `${modulePath}/${name}`;
    const Module  = require( path );
    const module  = new Module( collector );

    modules.push( module );
  }

  dir.closeSync();

  return modules;
}

/**
 *  Open/connect the given endpoint as a writable stream.
 *  @method _open_outputStream
 *  @param  collector   The controlling instance {Collector};
 *
 *  Open the endpoint specified in `collector.config.endpoint`, which should
 *  have the form:
 *    `protocol[://host:port[/path]]` {String};
 *
 *  @note Valid endpoint protocols:
 *          'tcp'     REQUIRES a `host` and `port` and IGNORES any `path`;
 *          'kafka'   REQUIRES a `host`, `port`, and uses `path` as the kafka
 *                    topic **NYI**;
 *          'stdout'  IGNORES any `host`, `port`, or `path;
 *          'stderr'  IGNORES any `host`, `port`, or `path;
 *
 *  @return A writable stream {Stream};
 *  @private
 */
function _open_outputStream( collector ) {
  const endpoint  = collector.config.output.endpoint;
  const url       = Url.parse( endpoint );
  let   stream;

  switch( url.protocol ) {
    case 'tcp:': {
      const config  = {
        collector : collector,
        host      : url.hostname,
        port      : url.port,
      };

      stream = new TcpStream( config );
    } break;

    case 'kafka:':
      stream = _open_kafkaStream( collector, url.hostname, url.port,
                                             url.path || '');
      break;

    default:
      switch( endpoint ) {
        case 'stdout':
          stream = process.stdout;
          break;

        case 'stderr':
        // fall-through
        default:
          stream = process.stderr;
          break;
      }
  }

  return stream;
}

/**
 *  Open/connect to the given Kafka host/port/topic as a writable stream.
 *  @method _open_kafkaStream
 *  @param  collector   The controlling instance {Collector};
 *  @param  host        The target Kafka host {String};
 *  @param  port        The target Kafka port {String | Number};
 *  @param  topic       The target Kafka topic {String};
 *
 *  @return A writable stream {Stream};
 *  @private
 */
function _open_kafkaStream( collector, hostname, port, topic ) {
  console.log('*** Collector kafka( %s:%s%s ): NYI', hostname, port, topic);

  return null;
}

/**
 *  Format the given metric in the Prometheus Exposition format.
 *  @method _format_prometheus
 *  @param  metric  The target metric {Metric};
 *
 *  @return The formatted metric {String};
 *  @private
 */
function _format_prometheus( metric ) {
  return metric.toPrometheus();
}

/**
 *  Format the given metric as a simple JSON-based metric.
 *  @method _format_json
 *  @param  metric  The target metric {Metric};
 *
 *  @note   The metric name is formatted as the `__name` label and not included
 *          in the top-level JSON object;
 *
 *  @return The formatted metric {String};
 *  @private
 */
function _format_json( metric ) {
  const labels  = Object.assign({__name: metric.name}, metric.labels);
  const json    = {
    value     : metric.value,
    timestamp : metric.timestamp,
    labels    : labels,
  };

  return JSON.stringify( json );
}

/**
 *  Format the given metric as a JSON-based uniVariate metric.
 *  @method _format_uniVariate
 *  @param  metric  The target metric {Metric};
 *
 *  @return The formatted metric {String};
 *  @private
 */
function _format_uniVariate( metric ) {
  const labels      = metric.labels;
  const jsonLabels  = Object.assign({}, labels);

  delete jsonLabels.node;
  delete jsonLabels.unit;

  const json    = {
    type      : 'uni-variate',
    name      : metric.name,
    node      : labels.node,
    unit      : labels.unit,
    value     : metric.value,
    timestamp : metric.timestamp,
    labels    : jsonLabels,
  };

  return JSON.stringify( json );
}

/* Private helpers }
 ****************************************************************************/
module.exports  = Collector;
