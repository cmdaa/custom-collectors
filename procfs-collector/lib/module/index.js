/**
 *  Base class for a procfs collection module.
 *  @class  Module
 */
class Module {
  /**
   *  Create a new instance, initializing this collector.
   *  @constructor
   *  @param  procfs              The procfs access instance {Procfs};
   *  @param  [systemLabels = {}] If provided, system-level labels to include
   *                              in all metrics. {Object}; 
   */
  constructor( collector ) {
    if (collector == null) {
      throw new Error('missing collector');
    }

    Object.defineProperties( this, {
      collector : { value: collector },
      _state    : { value: {}, writable: true },
    });
  }

  get procfs()        { return this.collector.procfs }
  get systemLabels()  { return this.collector.config.labels || {} }

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() { return [] }

  /**
   *  Reset any differential reporting state.
   *  @method reset
   *
   *  @return void
   */
  reset() {
    // Remove the set of last-reported metrics to clear our differential state
    this._state = {};
  }

  /**
   *  Generate a set of labels using the given label set and the system labels
   *  established upon creation.
   *  @method generateLabels
   *  @param  labels    The generation-time label set {Object};
   *
   *  @note   If `labels.__metric_class` is not provided, create it with the
   *          value of 'system';
   *
   *  @return The set of labels that includes system labels {Object};
   */
  generateLabels( labels ) {
    labels = labels || {};

    if (labels.__metric_class == null) {
      // Add the '__metric_class' meta-label
      labels.__metric_class = 'system';
    }

    return Object.assign( labels || {}, this.systemLabels );
  }
}

module.exports  = Module;
