const Fs      = require('fs');
const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based process data collection module.
 *  @class  Proc
 *
 *  This module generates Process-based metrics with labels:
 *    name    Name Command run by the process (comm);
 *    args    Command arguments;
 *    state   Current state of the process;
 */
class Proc extends Module {
	/**
   *  A map between procfs process status keys, base metric name and unit.
   *  @property keyMap
   *  @static
   *
   *  @note The 'name' portion will be prefixed with 'os_proc_process_'
   *
   *  @note Several metric values will be used as labels:
   *          metric                | label
   *          ----------------------+--------------------------
   *          cgroups               | cgroups
   *          cmdLine               | cmdLine
   *          comm                  | name
   *          flags                 | flags
   *          nice                  | sched_nice
   *          parent                | parent_id
   *          pid                   | id
   *          priority              | sched_priority
   *          processGroup          | process_group
   *          realtimePriority      | sched_priority_realtime
   *          schedulingPolicy      | sched_policy
   *          session               | session
   *          state                 | state
   *          terminalProcessGroup  | process_group_terminal
   *          tty                   | tty
   *                                |
   *          exitSignal            | IGNORED (leave this to the eBPF process
   *                                           monitor)
   */
  static  keyMap  = {
    // procfs.processStat( pid )
    blockIoTicks : {
      /* integer  : delayacct_blkio_ticks Aggregated block I/O delays, in
       *            ticks.
       */
      name: 'cpu_io',                   unit: 'ticks',
    },
    childGuestTicks : {
      /* integer  : Guest time of the process's children, in ticks.
       */
      name: 'child_cpu_guest',          unit: 'ticks',
    },
    childKernelTicks : {
      /* integer  : cstime Amount of time that this process's waited-for
       *            children have been scheduled in kernel mode, in ticks.
       */
      name: 'child_cpu_sys',            unit: 'ticks',
    },
    childMajorFaults : {
      /* integer  : cmajflt The number of major faults that the process's
       *            waited-for children have made.
       */
      name: 'child_mem_major_faults',   unit: 'count',
    },
    childMinorFaults : {
      /* integer  : cminflt The number of minor faults that the process's
       *            waited-for children have made.
       */
      name: 'child_mem_minor_faults',   unit: 'count',
    },
    childUserTicks : {
      /* integer  : cutime Amount of time that this process's waited-for
       *            children have been scheduled in user mode, in ticks.
       */
      name: 'child_cpu_user',           unit: 'ticks',
    },
    /* comm : string  : comm The filename of the executable. Visible whether or
     *                  not the executable is swapped out.
     *  (label  : name)
     *
     * exitSignal : integer  : exit_signal Signal to be sent to parent when we
     *                         die (IGNORED)
     *
     * flags : integer  : flags The kernel flags word of the process.
     *  (label  : flags)
     */
    guestTicks : {
      /* integer  : Guest time of the process (time spent running a virtual CPU
       *            for a guest operating system), in ticks.
       */
      name: 'cpu_guest',                unit: 'ticks',
    },
    kernelTicks : {
      /* integer  : stime Amount of time that this process has been scheduled
       *            in kernel mode, in ticks.
       */
      name: 'cpu_sys',                  unit: 'ticks',
    },
    lastCpu : {
      /* integer  : processor CPU number last executed on.
       */
      name: 'last_cpu',                 unit: 'index',
    },
    majorFaults : {
      /* integer  : majflt The number of major faults the process has made
       *            which have required loading a memory page from disk.
       */
      name: 'mem_major_faults',         unit: 'count',
    },
    minorFaults : {
      /* integer  : minflt The number of minor faults the process has made
       *            which have not required loading a memory page from disk.
       */
      name: 'mem_minor_faults',         unit: 'count',
    },
    /* nice : integer : nice The nice value, a value in the range 19(low
     *                  priority) to -20(high priority).
     *  (label  : nice)
     *
     * parent : integer : ppid The PID of the parent of the process.  not the
     *                    executable is swapped out.
     *  (label  : parent_id)
     *
     * pid   : integer : The process PID
     *  (label  : id)
     *
     * priority : integer : priority For processes running a real-time
     *                      scheduling policy (policy below), this is the
     *                      negated scheduling priority, minus one; that is, a
     *                      number in the range -2 to -100, corresponding to
     *                      real-time priorities 1 to 99. For processes running
     *                      under a non-real-time scheduling policy, this is
     *                      the raw nice value (setpriority(2)) as represented
     *                      in the kernel. The kernel stores nice values as
     *                      numbers in the range 0 (high) to 39 (low),
     *                      corresponding to the user-visible nice range of -20
     *                      to 19.
     *  (label  : sched_priority)
     *
     * processGroup : integer : pgrp The process group ID of the process.
     *  (label : process_group)
     *
     * realtimePriority : integer : rt_priority Real-time scheduling priority,
     *                              a number in the range 1 to 99 for processes
     *                              scheduled under a real-time policy, or 0,
     *                              for non-real-time processes.
     *  (label : sched_priority_realtime))
     */
    rss : {
      /* integer  : rss Resident Set Size, number of pages the process has in
       *            real memory. This is just the pages which count toward
       *            text, data, or stack space. This does not include pages
       *            which have not been demand-loaded in, or which are swapped
       *            out.
       */
      name: 'mem_rss',                  unit: 'bytes',
    },
    rssSoftLimit : {
      /* Float    : rsslim Current soft limit in bytes on the rss of the
       *            process.
       */
      name: 'mem_rss_soft_limit',       unit: 'bytes',
    },
    /* schedulingPolicy : integer  : policy Scheduling policy.
     *  (label : sched_policy)
     *
     * session : integer  : session The session ID of the process.
     *  (label : session)
     */
    startTicks : {
      /* integer  : starttime The time the process started after system boot,
       *            in ticks.
       */
      name: 'cpu_start',                unit: 'ticks',
    },
    /* state: ProcessState  : process sate
     *  (label  : state)
     *
     * terminalProcessGroup : integer : tpgid The ID of the foreground process
     *                                  group of the controlling terminal of
     *                                  the process.
     *  (label : process_group_terminal)
     */
    threads : {
      /* integer  : num_threads Number of threads in this process.
       */
      name: 'threads',                  unit: 'count',
    },
    /* tty : integer  : tty_nr The controlling terminal of the process.
     *  (label : tty)
     */
    userTicks : {
      /* integer  : utime Amount of time that this process has been scheduled
       *            in user mode(includes guest time), in ticks.
       */
      name: 'cpu_user',                 unit: 'ticks',
    },
    vsize : {
      /* integer  : vsize Virtual memory size in bytes.
       */
      name: 'mem_size',                 unit: 'bytes',
    },
  };

	/**
   *  A map between schedule policy integer and name.
   *  @property policyMap
   *  @static
   *
   *  @note Based upon /usr/include/linux/sched.h from Linux 5.4.0.
   */
  static policyMap = {
    0 : 'normal',
    1 : 'fifo',
    2 : 'rr',
    3 : 'batch',
    4 : 'iso',
    5 : 'idle',
    6 : 'deadline',
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const keyMap    = this.constructor.keyMap;
    const pids      = this.procfs.processes();
    const summary   = {
      total   : 0,  // pids.length,
      sleeping: 0,  // 'S'
      running : 0,  // 'R'
      idle    : 0,  // 'D'
      stopped : 0,  // 'T' | 't'
      zombie  : 0,  // 'Z'
      dead    : 0,  // 'X'
      unknown : 0,  // All others (x, K, W, P, N, n)
    };
    const newState  = {
      proc    : {},
      summary : summary,
    };
    const metrics   = [];

    // Collect process stats
    pids.forEach( pid => {
      const oldStat = (this._state.proc
                          ? this._state.proc[ pid ] || {}
                          : {});
      let   newStat;
      let   cmdline;

      try {
        newStat = this.procfs.processStat( pid );
        cmdline = this.procfs.processCmdline( pid );

      } catch(ex) {
        console.error('*** Failed to access info for process %d:',
                      pid, ex);
        return;
      }

      /* From https://man7.org/linux/man-pages/man5/proc.5.html
      /*  The tty number is encoded:
       *    the minor device number is contained in the combination of bits 31
       *    to 20 and 7 to 0;
       *    the major device number is in bits 15 to 8;
       */
      const tty_min = (newStat.tty >> 20) | (newStat.tty & 0xFF);
      const tty_maj = (newStat.tty >> 8) & 0xFF;
      const policy  = (this.constructor.policyMap[ newStat.schedulingPolicy ]
                        || 'unknown');
      const labels  = this.generateLabels( {
        name                    : newStat.comm,
        cgroups                 : _proc_cgroups( this.procfs, pid ).join(';'),
        cmdLine                 : (Array.isArray(cmdline)
                                    ? cmdline.join(' ')
                                    : cmdline),
        flags                   : newStat.flags,
        id                      : pid,
        parent_id               : newStat.parent,
        process_group           : newStat.processGroup,
        process_group_terminal  : newStat.terminalProcessGroup,
        state                   : newStat.state,
        sched_policy            : `${newStat.schedulingPolicy} (${policy})`,
        sched_priority          : newStat.priority,
        sched_priority_realtime : newStat.realtimePriority,
        sched_nice              : newStat.nice,
        session                 : newStat.session,
        tty                     : `${tty_maj}:${tty_min}`,
      } );

      newState.proc[pid] = newStat;

      Object.keys( newStat )
        .forEach( key => {
          if (keyMap[ key ] == null) {
            /* Skip keys that have no map entry.
             *
             * These are typically values that have already been extracted as
             * labels (e.g. comm, state, pid, parent, processGroup).
             */
            if (key === 'state') {
              _update_summary( summary, newStat[key] );
            }

            return;
          }

          const oldVal  = oldStat[ key ];
          const newVal  = newStat[ key ];

          if (oldVal == newVal) {
            // Skip this metric
            return;
          }

          // Include this metric
          const metric  = this._create_process_metric( newStat, key, labels );

          metrics.push( metric );
        });
    });

    // Finally, report process summary information
    Object.keys( summary )
      .forEach( key => {
        const oldVal  = (this._state.summary
                          ? this._state.summary[ key ]
                          : null);
        const newVal  = summary[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // :NOTE: These metrics use the 'system' metric-class
        const data  = {
          name  : `os_proc_${key}`,
          value : newVal,
          labels: this.generateLabels( {
            __metric_class: 'system',
            unit          : 'count',
          } ),
        };

        metrics.push( new Metric( data ) );
      });

    // Remember the current stats
    this._state = newState;

    return metrics;
  }

  /**
   *  Generate a set of labels using the given label set and the system labels
   *  established upon creation.
   *  @method generateLabels
   *  @param  labels    The generation-time label set {Object};
   *
   *  @note   If `labels.__metric_class` is not provided, create it with the
   *          value of 'process';
   *
   *  @return The set of labels that includes system labels {Object};
   */
  generateLabels( labels ) {
    labels = labels || {};

    if (labels.__metric_class == null) {
      // Add the '__metric_class' meta-label
      labels.__metric_class = 'process';
    }

    return super.generateLabels( labels );
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given process data and a metric key, create a new metric instance.
   *  @method _create_process_metric
   *  @param  proc    The full process data {Object};
   *  @param  key     The key from `proc` {String};
   *  @param  labels  The shared process labels {Object};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_process_metric( proc, key, labels ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const fullLabels  = Object.assign({unit: mapEntry.unit || 'count'},
                                      labels);
    const data        = {
      name  : `os_proc_process_${mapEntry.name}`,
      value : proc[ key ],
      labels: fullLabels,
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Given a process summary and state, update the proper summary count.
 *  @method _update_summary
 *  @param  summary   The summary collection object {Object};
 *  @param  state     The process state {String};
 *
 *    'S'                           : sleeping
 *    'R'                           : running
 *    'D'                           : idle
 *    'T' | 't'                     : stopped
 *    'Z'                           : zombie
 *    'X'                           : dead
 *    All others (x, K, W, P, N, n) : unknown
 *
 *  @return void
 *  @private
 */
function _update_summary( summary, state ) {
  summary.total++;

  switch( state ) {
    case 'S': summary.sleeping++; break;
    case 'R': summary.running++;  break;
    case 'T': summary.stopped++;  break;
    case 't': summary.stopped++;  break;
    case 'Z': summary.zombie++;   break;
    case 'X': summary.dead++;     break;
    default:  summary.unknown++;  break;
  }
}

/**
 *  Given a procfs instance and process id, retrieve the set of cgroups to
 *  which the given process is assigned.
 *  @method _proc_cgroups
 *  @param  procfs  Procfs access instance {Procfs};
 *  @param  pid     The id of the target process {Number};
 *
 *  @note   Each cgroup in the returned list have the form:
 *            %cgroup-subsystem-id%:%subsystem-name%:%cgroup-name%
 *            e.g. 3:cpu,cpuacct:/user.slice
 *
 *  @return A list of cgroups to which the given process is assigned {Array};
 *  @private
 */
function _proc_cgroups( procfs, pid ) {
  return Fs.readFileSync(`${procfs.root}/${pid}/cgroup`, {encoding:'utf-8'})
            .trim()
            .split('\n');
}

/* Private helpers }
 ****************************************************************************/
module.exports  = Proc;
