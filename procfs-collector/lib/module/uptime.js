const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based uptime data collection module.
 *  @class  Uptime
 *
 *  This module generates Uptime-based metrics:
 */
class Uptime extends Module {
	/**
   *  A map between procfs keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    time  : {name:null,       unit:'seconds'},
    idle  : {name:'idle',     unit:'seconds'},
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const uptime = this.procfs.uptime();
    const metrics = [];

    // Collect system-level cpu time measurements

    Object.keys( this.constructor.keyMap )
      .forEach( key => {
        const oldVal  = this._state[ key ];
        const newVal  = uptime[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // Include this metric
        const metric  = this._create_uptime_metric( uptime, key );

        metrics.push( metric );
      });

    // Remember the current stats
    this._state = uptime;

    return metrics;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given uptime data and a metric key, create a new metric instance.
   *  @method _create_uptime_metric
   *  @param  uptime  The full uptime data {Object};
   *  @param  key     The key from `uptime` {String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_uptime_metric( uptime, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const name  = (mapEntry.name === null
                    ? 'os_uptime'
                    : `os_uptime_${mapEntry.name}`);
    const data  = {
      name  : name,
      value : uptime[ key ],
      labels: this.generateLabels( {unit: mapEntry.unit || 'seconds'} ),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = Uptime;
