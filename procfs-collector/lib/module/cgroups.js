const Module    = require('.');
const Metric    = require('../metric');
const CgroupFs  = require('../cgroups');
const Path      = require('path');
const Parser    = {
  blkio   : require('./cgroups/blkio'),
  cgroup  : require('./cgroups/cgroup'),
  cpu     : require('./cgroups/cpu'),
  cpuacct : require('./cgroups/cpuacct'),
  cpuset  : require('./cgroups/cpuset'),
  freezer : require('./cgroups/freezer'),
  memory  : require('./cgroups/memory'),
  net_cls : require('./cgroups/net_cls'),
  net_prio: require('./cgroups/net_prio'),
  pids    : require('./cgroups/pids'),

  /******************************************
   * NOT handled by kernel-beat {
   */
  devices : require('./cgroups/devices'),
  hugetlb : require('./cgroups/hugetlb'),
  io      : require('./cgroups/io'),
  /* NOT handled by kernel-beat }
   ******************************************/
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  sysfs-based cgroup data collection module.
 *  @class  Cgroups
 *
 *  This module generates cgroups-based metrics (`os_cgroup` prefix) with
 *  several labels identifying the specific cgroup.
 *
 *  Labels:
 *    hid     The cgroup hierarchy id;
 *    path    The cgroup subsystem path relative to '/sys/fs/cgroup';
 *
 *  The generated metrics depend upon the cgroup subsystem, identified via the
 *  first component of `basename(path)` when split by '.':
 *    first     : base metric name
 *    ----------:---------------------
 *    blkio     : os_cgroup_blkio
 *    cgroup    : os_cgroup
 *    cpu       : os_cgroup_cpu
 *    cpuacct   : os_cgroup_cpuacct
 *    cpuset    : os_cgroup_cpuset
 *    devices   : os_cgroup_devices
 *    freezer   : os_cgroup_freezer
 *    hugetlb   : os_cgroup_hugetlb
 *    io        : os_cgroup_io
 *    memory    : os_cgroup_memory
 *    net_cls   : os_cgroup_net_cls
 *    net_prio  : os_cgroup_net_prio
 *    pids      : os_cgroup_pids
 */
class Cgroups extends Module {
  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const config    = this.collector.config;
    const procRoot  = this.collector.procfs.root;
    const sysfsRoot = (config.cgroups && config.cgroups.sysfs
                        ? config.cgroups.sysfs
                        : '/sys');

    // Module-wide new state
    this._newState  = {
      sysfsRoot : `${sysfsRoot}/fs/cgroup/`,
      labels    : {},
      subsys    : CgroupFs.get( procRoot ), // cgroup subsystems

      // cgroup-specific state by subsystem name (e.g. blkio: {})
    };

    // First, report any difference in the cgroup subsystem list
    let  metrics  = _process_subsystems( this );

    // Now, recursively process all file entries in the sysfs cgroup volume
    for await (const path of CgroupFs.walk( this._newState.sysfsRoot )) {
      const subsysMetrics = _process_cgroup( this, path );

      if (Array.isArray(subsysMetrics)) {
        metrics = metrics.concat( subsysMetrics );
      }
    }

    // Remember the new state
    this._state    = this._newState;
    this._newState = undefined;

    return metrics;
  }

  /**
   *  Generate a set of labels using the given label set and the system labels
   *  established upon creation.
   *  @method generateLabels
   *  @param  labels    The generation-time label set {Object};
   *
   *  @note   If `labels.__metric_class` is not provided, create it with the
   *          value of 'cgroup';
   *
   *  @return The set of labels that includes system labels {Object};
   */
  generateLabels( labels ) {
    labels = labels || {};

    if (labels.__metric_class == null) {
      // Add the '__metric_class' meta-label
      labels.__metric_class = 'cgroup';
    }

    // Mixin top-level cgroup labels
    labels = Object.assign( labels, this.cgroup_labels );

    return super.generateLabels( labels );
  }

  /**
   *  Retrieve the current set of shared labels from the new state.
   *  @property cgroupLabels
   *
   *  @return The set of shared labels {Object};
   */
  get cgroup_labels() {
    return (this._newState ? this._newState.labels : {});
  }
  set cgroup_labels( labels ) {
    if (this._newState == null) { this._newState = {} }
    this._newState.labels = labels;
  }

  /**
   *  Given a sysfs-relative path to cgroup data, retrieve/create the related,
   *  current state entry.
   *  @method get_current_state
   *  @param  path    The sysfs-relative path to the cgroup data {String};
   *
   *  @return The related, current state entry {Object};
   */
  get_current_state( path ) {
    if (this._state == null)  { this._state = {} }

    let state = this._state[ path ];
    if (state == null) {
      this._state[ path ] = state = {};
    }

    return state;
  }

  /**
   *  Given a sysfs-relative path to cgroup data, retrieve/create the related,
   *  new state entry.
   *  @method get_new_state
   *  @param  path    The sysfs-relative path to the cgroup data {String};
   *
   *  @return The related, current state entry {Object};
   */
  get_new_state( path ) {
    if (this._newState == null)  { this._newState = {} }

    let state = this._newState[ path ];
    if (state == null) {
      this._newState[ path ] = state = {};
    }

    return state;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Process the top-level cgroup subsystems.
 *  @method _process_subsystems
 *  @param  mod   The controlling module {Cgroups};
 *
 *  @return An array of new metrics {Array};
 *  @private
 */
function _process_subsystems( mod ) {
  const metrics   = [];
  const newState  = mod._newState;

  // First, report any difference in the cgroup subsystem list
  Object.values( newState.subsys ).forEach( newEntry => {
    const oldEntry  = (mod._state && mod._state.subsys
                        ? mod._state.subsys[ newEntry.name ]
                        : {});

    if (oldEntry          ==  null ||
        oldEntry.hid      !== newEntry.hid ||
        oldEntry.enabled  !== newEntry.enabled) {

      const data  = {
        name  : 'os_cgroup_subsystem',
        value : (newEntry.enabled ? 1 : 0),
        labels: mod.generateLabels({
                  name: newEntry.name,
                  hid : newEntry.hid,
                  unit: 'enabled',
                }),
      };

      const metric  = new Metric( data );

      metrics.push( metric );
    }
  });

  return metrics;
}

/**
 *  Process a path to a cgroup entry.
 *  @method _process_cgroup
 *  @param  mod   The controlling module {Cgroups};
 *  @param  path  The path to the target cgroup entry {String};
 *
 *  @return On success, an array of new metrics {Array | undefined};
 *  @private
 */
function _process_cgroup( mod, path ) {
  const relPath   = path.replace( mod._newState.sysfsRoot, '' );
  const cg_parts  = Path.basename( path ).split('.');

  if (cg_parts.length === 1) {
    // cgroup management file -- skip
    return;
  }

  const lines = CgroupFs.read( path );
  if (! Array.isArray(lines) || lines.length < 1) {
    /* No data or error reading the target cgroup file
     *
     * In the case of an error, the target is likely a control file that should
     * only be written.
     */
    return;
  }

  // Labels to be shared by all cgroup modules
  mod.cgroup_labels = {
    hid : 0,        // :XXX: Root by default.
    path: relPath,  // The cgroup subsystem path relative to '/sys/fs/cgroup';
  };

  const subsys_name = cg_parts[0];
  const subsys      = mod._newState.subsys[ subsys_name ];
  if (subsys) {
    mod.cgroup_labels.hid = subsys.hid;
  }

  // Retrieve the subsystem parser and, if found, invoke it.
  const parse = Parser[ subsys_name ];
  if (parse != null) {
    return parse( mod, relPath, cg_parts, lines );
  }

  if (! Warned.hasOwnProperty(path)) {
    Warned[path] = true;

    console.error('*** Unexpected cgroup subsystem [ %s ] from [ %s ]',
                  JSON.stringify(cg_parts), relPath);
  }
}

/* Private helpers }
 ****************************************************************************/
module.exports  = Cgroups;
