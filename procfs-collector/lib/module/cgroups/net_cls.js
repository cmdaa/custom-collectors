const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_net_cls_'
 */
const KeyMap  = {
  'net_cls.classid' :{name:'classid',   unit:'major/minor'},
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the net_cls subsystem
 *  @method parse_net_cls
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/net_cls.txt
 *    The Network classifier cgroup provides an interface to tag network
 *    packets with a class identifier (classid).
 *
 *    The Traffic Controller (tc) can be used to assign different priorities to
 *    packets from different cgroups. Also, Netfilter (iptables) can use this
 *    tag to perform actions on such packets.
 *
 *    Creating a net_cls cgroups instance creates a net_cls.classid file.
 *    This net_cls.classid value is initialized to 0.
 *
 *    You can write hexadecimal values to net_cls.classid; the format for these
 *    values is 0xAAAABBBB; AAAA is the major handle number and BBBB is the
 *    minor handle number.  Reading net_cls.classid yields a decimal result.
 *
 *    Example:
 *      mkdir /sys/fs/cgroup/net_cls
 *      mount -t cgroup -onet_cls net_cls /sys/fs/cgroup/net_cls
 *      mkdir /sys/fs/cgroup/net_cls/0
 *      echo 0x100001 >  /sys/fs/cgroup/net_cls/0/net_cls.classid
 *        - setting a 10:1 handle.
 *
 *      cat /sys/fs/cgroup/net_cls/0/net_cls.classid
 *      1048577
 *
 *    configuring tc:
 *      tc qdisc add dev eth0 root handle 10: htb
 *
 *      tc class add dev eth0 parent 10: classid 10:1 htb rate 40mbit
 *        - creating traffic class 10:1
 *
 *      tc filter add dev eth0 parent 10: protocol ip prio 10 handle 1: cgroup
 *
 *    configuring iptables, basic example:
 *      iptables -A OUTPUT -m cgroup ! --cgroup 0x100001 -j DROP
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_net_cls( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'net_cls' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_net_cls( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    net_cls.classid   [ %int% ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_net_cls_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_net_cls;
