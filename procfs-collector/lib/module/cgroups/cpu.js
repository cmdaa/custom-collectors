const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  Note that cpu.pressure and cpu.stat have sub-maps for individual
 *  lines/fields.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_cpu_'
 */
const KeyMap  = {
  'cpu.cfs_period_us' :{name:'cfs_period',    unit:'microseconds'},
  'cpu.cfs_quota_us'  :{name:'cfs_quota',     unit:'microseconds'},

  'cpu.pressure': {   // os_cgroup_cpu_pressure_%type%_
    parser            : _parse_pressure,

    'avg10'           :{name:'ten_seconds',   unit:'average'},
    'avg60'           :{name:'one_minute',    unit:'average'},
    'avg300'          :{name:'five_minute',   unit:'average'},
    'total'           :{name:'total',         unit:'microseconds'},
  },

  'cpu.rt_runtime_us' :{name:'rt_runtime',    unit:'microseconds'},
  'cpu.rt_period_us'  :{name:'rt_period',     unit:'microseconds'},
  'cpu.shares'        :{name:'shares',        unit:'count'},

  'cpu.stat': {       // os_cgroup_cpu_stat_
    parser            : _parse_stat,

    'throttled_time'  :{name:'throttle',      unit:'nanoseconds'},
    'nr_throttled'    :{name:'throttle',      unit:'count'},
    'nr_periods'      :{name:'period',        unit:'count'},

    'usage_usec'      :{name:'usage',         unit:'microseconds'},
    'user_usec'       :{name:'user',          unit:'microseconds'},
    'system_usec'     :{name:'system',        unit:'microseconds'},
  },

  'cpu.uclamp.min'    :{name:'uclamp_min',    unit:'count'},
  'cpu.uclamp.max'    :{name:'uclamp_max',    unit:'count'},
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the cpu subsystem
 *  @method parse_cpu
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    cpu.cfs_period_us     [ %int% ]
 *    cpu.cfs_quota_us      [ %int% ]
 *    cpu.pressure          [ 'some avg10=0.00 avg60=0.00 avg300=0.00 total=1' ]
 *    cpu.shares            [ %int% ]
 *    cpu.stat              [ 'nr_periods %int%',       # per cpu.cfs_period_us
 *                            'nr_throttled %int%',
 *                            'throttled_time %int%' ]  # nanoseconds
 *                          OR
 *                          [ 'usage_usec %int%',
 *                            'user_usec %int%',
 *                            'system_usec %int%' ]
 *    cpu.uclamp.max        [ %float% ]   # MAY be 'max'
 *    cpu.uclamp.min        [ %float% ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_cpu( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'cpu' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_cpu( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    cpu.cfs_period_us [ %int% ]
 *    cpu.cfs_quota_us  [ %int% ]
 *    cpu.shares        [ %int% ]
 *    cpu.uclamp.max    [ %float% ] (MAY be 'max' => -1)
 *    cpu.uclamp.min    [ %float% ]
 *
 *  From https://patchwork.kernel.org/project/linux-pm/patch/20190822132811.31294-2-patrick.bellasi@arm.com/
 *    - uclamp.min: defines the minimum utilization which should be considered
 *      i.e. the RUNNABLE tasks of this group will run at least at a minimum
 *           frequency which corresponds to the uclamp.min utilization
 *
 *    - uclamp.max: defines the maximum utilization which should be considered
 *      i.e. the RUNNABLE tasks of this group will run up to a maximum
 *           frequency which corresponds to the uclamp.max utilization
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_cpu_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a cpu 'stat' metric.
 *  @method _parse_stat
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    nr_periods %int%
 *    nr_throttled %int%
 *    throttled_time %int%
 *        OR
 *    usage_usec %int%
 *    user_usec %int%
 *    system_usec %int%
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_stat( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'cpu.stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_stat( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const statName    = cols[0];
    const mapEntry    = statMap[ statName ];
    if (mapEntry == null) {
      if (! Warned.hasOwnProperty(path)) {
        Warned[path] = true;

        console.error('*** _parse_stat( %s ): Unexpected name[ %s ]:',
                      path, statName, cols);
      }
      return;
    }

    const metricName  = `os_cgroup_cpu_stat_${mapEntry.name}`;
    const newValue    = cols[1];
    const oldValue    = oldState[ metricName ];

    // Record the new metric value
    newState[ metricName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {unit: mapEntry.unit}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/**
 *  Parse a cpu 'pressure' metric.
 *  @method _parse_pressure
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  pressureMap The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    some avg10=0.00 avg60=0.00 avg300=0.00 total=1
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/pressure-metrics.html
 *    The output format for CPU is:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    and for memory and IO:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *      full avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    Memory and IO show two metrics: some and full. The CPU controller shows
 *    only the some metric. The values for both some and full are shown in
 *    running averages over the last 10 seconds, 1 minute, and 5 minutes,
 *    respectively. The total statistic is the accumulated microseconds.
 *
 *    PSI shows pressure as a percentage of walltime in which some or all
 *    processes are blocked on a resource:
 *    - 'some' is the percentage of walltime that some (one or more) tasks were
 *      delayed due to lack of resources—for example, a lack of memory.
 *    - 'full' is the percentage of walltime in which all tasks were delayed by
 *      lack of resources, i.e., the amount of completely unproductive time.
 *
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_pressure( mod, path, pressureMap, lines ) {
  // assert( pressureMap === KeyMap[ 'cpu.pressure' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  if (lines.length !== 2) {
    console.error('*** _parse_pressure( %s ): expected 2 lines, see %d',
                  path, lines.length);
  }

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 5) {
      console.error('*** _parse_pressure( %s ): line %d has '
                    +       '%d columns instead of 5:',
                    path, idex, cols.length, line);
      return;
    }

    // Process this line
    const lineName  = cols.shift(); // some | full

    // assert( lineName === 'some' );

    cols.forEach( stat => {
      const [statName, newValue]  = stat.split('=');
      const mapEntry              = pressureMap[ statName ];

      if (mapEntry == null) {
        if (! Warned.hasOwnProperty(path)) {
          Warned[path] = true;

          console.error('*** _parse_pressure( %s ): Unexpected name[ %s ]:',
                        path, statName, stat);
        }
        return;
      }

      const metricName  = `os_cgroup_cpu_pressure_${lineName}_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ metricName ];

      // Record the new metric value
      newState[ metricName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {unit: mapEntry.unit}, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_cpu;
