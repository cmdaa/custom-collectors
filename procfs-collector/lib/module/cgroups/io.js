const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  Note that several entries include a parser as well as a sub-map for
 *  individual lines/fields.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_io_'
 */
const KeyMap  = {
  'io.latency'                :{name:'latency', unit:'milliseconds'},

  'io.pressure'               :{ // os_cgroup_io_pressure_%type%_
    parser                    : _parse_pressure,

    'avg10'                   :{name:'ten_seconds',         unit:'average'},
    'avg60'                   :{name:'one_minute',          unit:'average'},
    'avg300'                  :{name:'five_minute',         unit:'average'},
    'total'                   :{name:'total',               unit:'microseconds'},
  },

  'io.stat'                   :{ // os_cgroup_io_
    parser                    : _parse_stat,

    'rbytes'                  :{name:'read',                unit:'bytes'},
    'wbytes'                  :{name:'write',               unit:'bytes'},
    'dbytes'                  :{name:'discarded',           unit:'bytes'},

    'rios'                    :{name:'read',                unit:'ios'},
    'wios'                    :{name:'write',               unit:'ios'},
    'dios'                    :{name:'discarded',           unit:'ios'},
  },

  'io.max'                    :{ // os_cgroup_io_max_
    parser                    : _parse_max,

    'riops'                   :{name:'read',                unit:'iops'},
    'wiops'                   :{name:'write',               unit:'iops'},

    'rbps'                    :{name:'read',                unit:'bps'},
    'wbps'                    :{name:'write',               unit:'bps'},
  },
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the io subsystem
 *  @method parse_io
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  This subsystem is for cgroups v2.
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    io.pressure   [ ... ]
 *
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/io-controller.html
 *    File        Definition
 *    io.latency  Quality of service mechanism to guarantee a cgroup's level of
 *                IO completion latency. Specifies the number of milliseconds a
 *                process can wait before IO from other processes is given to
 *                it.
 *
 *                If the average completion latency is longer than the target
 *                set here, other processes are throttled to provide more IO,
 *                effectively prioritizing the job with the lowest io.latency
 *                setting.
 *
 *                ...
 *
 *    io.pressure Gives the percentage of wall time in which some or all tasks
 *                are waiting for a block device, or IO.
 *
 *                See the PSI resource pressure metrics page for more details.
 *
 *    io.stat     IO usage statistics.
 *                Lines are keyed by $MAJ:$MIN device numbers and not ordered.
 *                The following keys are defined:
 *
 *                  rbytes: bytes read
 *                  wbytes: bytes written
 *                  dbytes: number of bytes discarded
 *                  rios: number of read IOs
 *                  wios: number of write IOs
 *                  dios: number of discard (or trim) IOs.
 *
 *                  Example:
 *                    8:16 rbytes=1459200 wbytes=314773504 rios=192 wios=353 dbytes=9973760 dios=79
 *                    8:0 rbytes=90430464 wbytes=299008000 rios=8950 wios=1252 dbytes=69632 dios=158
 *
 *    io.max      This is where you specify IO limits. Lines are keyed by
 *                $MAJ:$MIN device numbers and not ordered.
 *
 *                The following nested keys are defined.
 *                  riops: Max read IO operations per second
 *                  wiops: Max write IO operations per second
 *                  rbps: Max read bytes per second
 *                  wbps: Max write bytes per second
 *
 *                When writing, any number of nested key-value pairs can be
 *                specified in any order. You can specify max to remove a
 *                specific limit. If the same key is specified multiple times,
 *                the outcome is undefined.
 *
 *                ...
 *
 *                Example:
 *                  8:16 rbps=2097152 wbps=max riops=max wiops=120
 *
 *
 *  For metrics that identify a target device (e.g. io.stat, io.max), the
 *  target device will be identified via label:
 *    device_id   '%major%:%minor%'
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_io( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'io' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_io( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  if (typeof(mapEntry.parser) !== 'function') {
    console.error('*** parse_io( %s ): Missing parser for [ %s ]',
                  path, baseName);
    return;
  }

  return mapEntry.parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse an io 'pressure' metric.
 *  @method _parse_pressure
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  pressureMap The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    some avg10=0.00 avg60=0.00 avg300=0.00 total=1
 *    full avg10=0.00 avg60=0.00 avg300=0.00 total=1
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/pressure-metrics.html
 *    The output format for CPU is:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    and for memory and IO:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *      full avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    Memory and IO show two metrics: some and full. The CPU controller shows
 *    only the some metric. The values for both some and full are shown in
 *    running averages over the last 10 seconds, 1 minute, and 5 minutes,
 *    respectively. The total statistic is the accumulated microseconds.
 *
 *    PSI shows pressure as a percentage of walltime in which some or all
 *    processes are blocked on a resource:
 *    - 'some' is the percentage of walltime that some (one or more) tasks were
 *      delayed due to lack of resources—for example, a lack of memory.
 *    - 'full' is the percentage of walltime in which all tasks were delayed by
 *      lack of resources, i.e., the amount of completely unproductive time.
 *
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_pressure( mod, path, pressureMap, lines ) {
  // assert( pressureMap === KeyMap[ 'io.pressure' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  if (lines.length !== 2) {
    console.error('*** _parse_pressure( %s ): expected 2 line, see %d',
                  path, lines.length);
  }

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 5) {
      console.error('*** _parse_pressure( %s ): line %d has '
                    +       '%d columns instead of 5:',
                    path, idex, cols.length, line);
      return;
    }

    // Process this line
    const lineName  = cols.shift(); // some | full

    // assert( lineName === 'some' || lineName === 'full');

    cols.forEach( stat => {
      const [statName, newValue]  = stat.split('=');
      const mapEntry              = pressureMap[ statName ];

      if (mapEntry == null) {
        if (! Warned.hasOwnProperty(path)) {
          Warned[path] = true;

          console.error('*** _parse_pressure( %s ): Unexpected name[ %s ]:',
                        path, statName, stat);
        }
        return;
      }

      const metricName  = `os_cgroup_io_pressure_${lineName}_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ metricName ];

      // Record the new metric value
      newState[ metricName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {unit: mapEntry.unit}, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

  });

  return metrics;
}

/**
 *  Parse an io 'stat' metric.
 *  @method _parse_stat
 *  @param  mod       The controlling module {Cgroups};
 *  @param  path      The sysfs-relative path to the source cgroup file
 *                    {String};
 *  @param  statMap   The sub-map for this parser {Object};
 *  @param  lines     The content of the target cgroup file, split by new-line
 *                    {Array};
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/io-controller.html
 *  `lines` should have the form:
 *    %major%:%minor% rbytes=%int% wbytes=%int% rios=%int% wios=%int%
 *      dbytes=%int% dios=%int%
 *
 *    io.stat     IO usage statistics.
 *                Lines are keyed by $MAJ:$MIN device numbers and not ordered.
 *                The following keys are defined:
 *
 *                  rbytes: bytes read
 *                  wbytes: bytes written
 *                  dbytes: number of bytes discarded
 *                  rios: number of read IOs
 *                  wios: number of write IOs
 *                  dios: number of discard (or trim) IOs.
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_stat( mod, path, statMap, lines ) {
  // assert( lines.length > 0 );
  // assert( statMap === KeyMap[ 'io.stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 7) {
      console.error('*** _parse_stat( %s ): line %d has '
                    +       '%d columns instead of 7:',
                    path, idex, cols.length, line);
      return;
    }

    // Process this line
    const devName   = cols.shift(); // %major%:%minor%

    cols.forEach( stat => {
      const [statName, newValue]  = stat.split('=');
      const mapEntry              = statMap[ statName ];

      if (mapEntry == null) {
        if (! Warned.hasOwnProperty(path)) {
          Warned[path] = true;

          console.error('*** _parse_stat( %s ): Unexpected name[ %s ]:',
                        path, statName, stat);
        }
        return;
      }

      const stateName   = `io_stat_${devName}_${mapEntry.name}`;
      const metricName  = `os_cgroup_io_stat_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {
                  device_id : devName,
                  unit      : mapEntry.unit,
                }, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

  });

  return metrics;
}

/**
 *  Parse an io 'max' metric.
 *  @method _parse_max
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file
 *                  {String};
 *  @param  subMap  The sub-map for this parser {Object};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/io-controller.html
 *  `lines` should have the form:
 *    %major%:%minor% rbps=(%int%|max) wbps=(%int%|max) riops=(%int%|max)
 *      wiops=(%int%|max)
 *
 *    io.max      This is where you specify IO limits. Lines are keyed by
 *                $MAJ:$MIN device numbers and not ordered.
 *
 *                The following nested keys are defined.
 *                  riops: Max read IO operations per second
 *                  wiops: Max write IO operations per second
 *                  rbps: Max read bytes per second
 *                  wbps: Max write bytes per second
 *
 *                When writing, any number of nested key-value pairs can be
 *                specified in any order. You can specify max to remove a
 *                specific limit. If the same key is specified multiple times,
 *                the outcome is undefined.
 *
 *                ...
 *
 *                Example:
 *                  8:16 rbps=2097152 wbps=max riops=max wiops=120
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_max( mod, path, subMap, lines ) {
  // assert( lines.length > 0 );
  // assert( subMap === KeyMap[ 'io.max' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 7) {
      console.error('*** _parse_max( %s ): line %d has '
                    +       '%d columns instead of 7:',
                    path, idex, cols.length, line);
      return;
    }

    // Process this line
    const devName   = cols.shift(); // %major%:%minor%

    cols.forEach( stat => {
      let   [statName, newValue]  = stat.split('=');
      const mapEntry              = subMap[ statName ];

      if (mapEntry == null) {
        if (! Warned.hasOwnProperty(path)) {
          Warned[path] = true;

          console.error('*** _parse_max( %s ): Unexpected name[ %s ]:',
                        path, statName, stat);
        }
        return;
      }

      const stateName   = `io_max_${devName}_${mapEntry.name}`;
      const metricName  = `os_cgroup_io_max_${mapEntry.name}`;

      if (newValue === 'max') {
        newValue = -1;
      }

      // Retrieve the old value
      const oldValue  = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {
                  device_id : devName,
                  unit      : mapEntry.unit,
                }, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_io;
