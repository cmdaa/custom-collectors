const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  Note that several entries include a parser as well as a sub-map for
 *  individual lines/fields.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_memory_'
 *
 *  Ref: https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt
 */
const KeyMap  = {
  'memory.failcnt'            :{name:'fail',                unit:'count'},
  'memory.kmem.failcnt'       :{name:'kmem_fail',           unit:'count'},
  'memory.kmem.limit_in_bytes':{name:'kmem_limit',          unit:'bytes'},
  'memory.kmem.max_usage_in_bytes'
                              :{name:'kmem_max_usage',      unit:'bytes'},

  'memory.kmem.slabinfo'      :{  // os_cgroup_memory_kmem_slabinfo_
    parser  : _parse_slabinfo,

    /* 0    1             2           3         4             5
     * name <active_objs> <num_objs>  <objsize> <objperslab>  <pagesperslab>
     *
     * 6  7         8       9             10
     * :  tunables  <limit> <batchcount>  <sharedfactor>
     *
     * 11 12        13              14          15
     * :  slabdata  <active_slabs>  <num_slabs> <sharedavail>
     */
    columns : [
      /*  0 */  false,  // skip %name%
      /*  1 */  {name:'objects_active',   unit:'count'},
      /*  2 */  {name:'objects',          unit:'count'},
      /*  3 */  {name:'object_size',      unit:'bytes'},
      /*  4 */  {name:'objects_per_slab', unit:'count'},
      /*  5 */  {name:'pages_per_slab',   unit:'count'},
      /*  6 */  false,  // skip ':'
      /*  7 */  false,  // skip 'tunables'
      /*  8 */  {name:'limit',            unit:'count'},
      /*  9 */  {name:'batch',            unit:'count'},
      /* 10 */  {name:'shared_factor',    unit:'factor'},
      /* 11 */  false,  // skip ':'
      /* 12 */  false,  // skip 'slabdata'
      /* 13 */  {name:'active',           unit:'count'},
      /* 14 */  {name:'total',            unit:'count'},
      /* 15 */  {name:'shared_available', unit:'count'},
    ],
  },

  'memory.kmem.tcp.failcnt'   :{name:'kmem_tcp_fail',       unit:'count'},
  'memory.kmem.tcp.limit_in_bytes'
                              :{name:'kmem_tcp_limit',      unit:'bytes'},
  'memory.kmem.tcp.max_usage_in_bytes'
                              :{name:'kmem_tcp_max_usage',  unit:'bytes'},
  'memory.kmem.tcp.usage_in_bytes'
                              :{name:'kmem_tcp_usage',      unit:'bytes'},
  'memory.kmem.usage_in_bytes':{name:'kmem_usage',          unit:'bytes'},

  'memory.limit_in_bytes'     :{name:'limit',               unit:'bytes'},
  'memory.max_usage_in_bytes' :{name:'max_usage',           unit:'bytes'},
  'memory.move_charge_at_immigrate'
                              :{name:'move_charge_at_immigrate',
                                                            unit:'charge'},
  'memory.numa_stat'          :{  // os_cgroup_memory_numa_stat_
    parser                    : _parse_numa_stat,

    // total=%int% N0=%int% [N1=%int% ...]
    'total'                   :{name:'total',               unit:'bytes'},
    'file'                    :{name:'file',                unit:'bytes'},
    'anon'                    :{name:'anon',                unit:'bytes'},
    'unevictable'             :{name:'unevictable',         unit:'bytes'},
    'hierarchical_total'      :{name:'hierarchical_total',  unit:'bytes'},
    'hierarchical_file'       :{name:'hierarchical_file',   unit:'bytes'},
    'hierarchical_anon'       :{name:'hierarchical_anon',   unit:'bytes'},
    'hierarchical_unevictable':{name:'hierarchical_unevictable',
                                                            unit:'bytes'},
  },

  'memory.oom_control'        :{ // os_cgroup_memory_oom_control_
    parser                    : _parse_oom_control,

    'oom_kill_disable'        :{name:'kill_disable',        unit:'boolean'},
    'under_oom'               :{name:'under',               unit:'boolean'},
    'oom_kill'                :{name:'kill',                unit:'boolean'},
  },

  'memory.pressure'           :{ // os_cgroup_memory_pressure_%type%_
    parser                    : _parse_pressure,

    'avg10'                   :{name:'ten_seconds',         unit:'average'},
    'avg60'                   :{name:'one_minute',          unit:'average'},
    'avg300'                  :{name:'five_minute',         unit:'average'},
    'total'                   :{name:'total',               unit:'microseconds'},
  },

  'memory.soft_limit_in_bytes':{name:'soft_limit',          unit:'bytes'},

  'memory.stat'               :{ // os_cgroup_memory_stat_
    parser                    : _parse_stat,

    'cache'                   :{name:'cache',               unit:'bytes'},
    'rss'                     :{name:'rss',                 unit:'bytes'},
    'rss_huge'                :{name:'rss_huge',            unit:'bytes'},
    'shmem'                   :{name:'shmem',               unit:'bytes'},
    'mapped_file'             :{name:'mapped_file',         unit:'bytes'},
    'dirty'                   :{name:'dirty',               unit:'bytes'},
    'writeback'               :{name:'writeback',           unit:'bytes'},
    'pgpgin'                  :{name:'pgpgin',              unit:'events'},
    'pgpgout'                 :{name:'pgpgout',             unit:'events'},
    'pgfault'                 :{name:'pgfault',             unit:'bytes'},
    'pgmajfault'              :{name:'pgmajfault',          unit:'bytes'},
    'inactive_anon'           :{name:'inactive_anon',       unit:'bytes'},
    'active_anon'             :{name:'active_anon',         unit:'bytes'},
    'inactive_file'           :{name:'inactive_file',       unit:'bytes'},
    'active_file'             :{name:'active_file',         unit:'bytes'},
    'swap'                    :{name:'swap',                unit:'bytes'},
    'unevictable'             :{name:'unevictable',         unit:'bytes'},
    'hierarchical_memory_limit'
                              :{name:'hierarchical_memory_limit',
                                                            unit:'bytes'},
    'hierarchical_memsw_limit':{name:'hierarchical_swap_limit',
                                                            unit:'bytes'},
    'total_cache'             :{name:'total_cache',         unit:'bytes'},
    'total_rss'               :{name:'total_rss',           unit:'bytes'},
    'total_rss_huge'          :{name:'total_rss_huge',      unit:'bytes'},
    'total_shmem'             :{name:'total_shmem',         unit:'bytes'},
    'total_mapped_file'       :{name:'total_mapped_file',   unit:'bytes'},
    'total_dirty'             :{name:'total_dirty',         unit:'bytes'},
    'total_writeback'         :{name:'total_writeback',     unit:'bytes'},
    'total_pgpgin'            :{name:'total_pgpgin',        unit:'events'},
    'total_pgpgout'           :{name:'total_pgpgout',       unit:'events'},
    'total_pgfault'           :{name:'total_pgfault',       unit:'bytes'},
    'total_pgmajfault'        :{name:'total_pgmajfault',    unit:'bytes'},
    'total_inactive_anon'     :{name:'total_inactive_anon', unit:'bytes'},
    'total_active_anon'       :{name:'total_active_anon',   unit:'bytes'},
    'total_inactive_file'     :{name:'total_inactive_file', unit:'bytes'},
    'total_active_file'       :{name:'total_active_file',   unit:'bytes'},
    'total_unevictable'       :{name:'total_unevictable',   unit:'bytes'},
    'total_swap'              :{name:'total_swap',          unit:'bytes'},
  },

  'memory.swappiness'         :{name:'swappiness',          unit:'swappiness'},

  'memory.usage_in_bytes'     :{name:'usage',               unit:'bytes'},

  'memory.use_hierarchy'      :{name:'use_hierarchy',       unit:'boolean'},

  // memory+Swap
  'memory.memsw.usage_in_bytes'
                              :{name:'swap',                unit:'bytes'},
  'memory.memsw.limit_in_bytes'
                              :{name:'swap_limit',          unit:'bytes'},
  'memory.memsw.max_usage_in_bytes'
                              :{name:'swap_max',            unit:'bytes'},
  'memory.memsw.failcnt'      :{name:'swap_fail',           unit:'count'},
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the memory subsystem
 *  @method parse_memory
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    memory.failcnt                      [ %int% ]
 *    memory.kmem.failcnt                 [ %int% ]
 *    memory.kmem.limit_in_bytes          [ %int% ]
 *    memory.kmem.max_usage_in_bytes      [ %int% ]
 *    memory.kmem.slabinfo                [ ... ]
 *    memory.kmem.tcp.failcnt             [ %int% ]
 *    memory.kmem.tcp.limit_in_bytes      [ %int% ]
 *    memory.kmem.tcp.max_usage_in_bytes  [ %int% ]
 *    memory.kmem.tcp.usage_in_bytes      [ %int% ]
 *    memory.kmem.usage_in_bytes          [ %int% ]
 *    memory.limit_in_bytes               [ %int% ]
 *    memory.max_usage_in_bytes           [ %int% ]
 *    memory.move_charge_at_immigrate     [ %int% ]
 *    memory.numa_stat                    [ ... ]
 *    memory.oom_control                  [ 'oom_kill_disable %int%',
 *                                          'under_oom %int%',
 *                                          'oom_kill %int%' ]
 *    memory.pressure                     [ ... ]
 *    memory.soft_limit_in_bytes          [ %int% ]
 *    memory.stat                         [ ... ]
 *    memory.swappiness                   [ %int% ]
 *    memory.usage_in_bytes               [ %int% ]
 *    memory.use_hierarchy                [ %int% ]
 *
 *    memory.memsw.failcnt                [ %int% ]
 *    memory.memsw.limit_in_bytes         [ %int% ]
 *    memory.memsw.max_usage_in_bytes     [ %int% ]
 *    memory.memsw.usage_in_bytes         [ %int% ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_memory( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'memory' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_memory( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    memory.failcnt              [ %int% ]
 *    memory.kmem.failcnt         [ %int% ]
 *    memory.kmem.limit_in_bytes  [ %int% ]
 *    memory.limit_in_bytes       [ %int% ]
 *    memory.max_usage_in_bytes   [ %int% ]
 *    memory.usage_in_bytes       [ %int% ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_memory_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a memory 'kmem.slabinfo' metric.
 *  @method _parse_slabinfo
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  This cgroup contains fairly complex formatting and data, for example:
 *    slabinfo - version: 2.1
 *    # name            <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> : tunables <limit> <batchcount> <sharedfactor> : slabdata <active_slabs> <num_slabs> <sharedavail>
 *    kmalloc-rcl-96       168    168     96   42    1 : tunables    0    0    0 : slabdata      4      4      0
 *    TCPv6                  0      0   2432   13    8 : tunables    0    0    0 : slabdata      0      0      0
 *    TCP                    0      0   2240   14    8 : tunables    0    0    0 : slabdata      0      0      0
 *    eventpoll_pwq        896    896     72   56    1 : tunables    0    0    0 : slabdata     16     16      0
 *    buffer_head          702    702    104   39    1 : tunables    0    0    0 : slabdata     18     18      0
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_slabinfo( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'memory.kmem.slabinfo' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  const colMap    = statMap.columns;
  let   metrics;

  lines.every( (line,idex) => {
    if (idex === 0) {
      // assert( line.startsWith('slabinfo') );
      if (line !== 'slabinfo - version: 2.1') {
        console.error('*** _parse_slabinfo( %s ): unknown version (! 2.1):',
                      path, line);
        return false; // terminate iteration
      }

      return true;  // continue iteration
    }

    let isComment = (line[0] === '#');
    if (isComment) {
      // Strip the '#'
      line = line.slice(1).trim();
    }

    const cols  = line.split(/\s+/g);
    if (cols.length !== colMap.length) {
      console.error('*** _parse_slabinfo( %s ): expected %d colums, see %d:',
                    path, colMap.length, cols.length, line, cols);

      if (isComment) {
        /* The comment about columns doesn't match our map,
         * terminate iteration
         */
        return false;
      }

      // skip this line but continue iteration
      return true;
    }
    if (isComment) {
      return true;  // continue iteration
    }

    // Process the data from this line
    const slabName  = cols[0];

    cols.forEach( (newValue, cdex) => {
      const mapEntry  = colMap[cdex];
      if ( !mapEntry ) {
        // Skip this column
        return;
      }

      const stateName   = `kmem_slab_${slabName}_${mapEntry.name}`;
      const metricName  = `os_cgroup_memory_kmem_slab_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {
          slab_name : slabName,
          unit      : mapEntry.unit,
        }, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

    return true;  // continue iteration
  });

  return metrics;
}

/**
 *  Parse a memory 'numa_stat' metric.
 *  @method _parse_numa_stat
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    total=%int% N0=%int% [N1=%int% ...]
 *    file=%int% N0=%int% [N1=%int% ...]
 *    anon=%int% N0=%int% [N1=%int% ...]
 *    unevictable=%int% N0=%int% [N1=%int% ...]
 *    hierarchical_total=%int% N0=%int% [N1=%int% ...]
 *    hierarchical_file=%int% N0=%int% [N1=%int% ...]
 *    hierarchical_anon=%int% N0=%int% [N1=%int% ...]
 *    hierarchical_unevictable=%int% N0=%int% [N1=%int% ...]
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt
 *    ... useful for providing visibility into the numa locality information
 *    within an memcg since the pages are allowed to be allocated from any
 *    physical node.  One of the use cases is evaluating application
 *    performance by combining this information with the application's CPU
 *    allocation.
 *
 *    Each memcg's numa_stat file includes "total", "file", "anon" and
 *    "unevictable" per-node page counts including "hierarchical_<counter>"
 *    which sums up all hierarchical children's values in addition to the
 *    memcg's own value.
 *
 *    The output format of memory.numa_stat is:
 *      total=<total pages> N0=<node 0 pages> N1=<node 1 pages> ...
 *      file=<total file pages> N0=<node 0 pages> N1=<node 1 pages> ...
 *      anon=<total anon pages> N0=<node 0 pages> N1=<node 1 pages> ...
 *      unevictable=<total anon pages> N0=<node 0 pages> N1=<node 1 pages> ...
 *      hierarchical_<counter>=<counter pages> N0=<node 0 pages> N1=<node 1 pages> ...
 *
 *    The "total" count is sum of file + anon + unevictable.
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_numa_stat( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'memory.numa_stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length < 2) {
      console.error('*** _parse_numa_stat( %s ): line %d has '
                    +       '%d columns, require at least 2:',
                    path, idex, cols.length, line);
      return;
    }

    let mapEntry;
    let numaNode  = 'all';

    cols.forEach( stat => {
      const [statName, newValue]  = stat.split('=');
      if (mapEntry == null) {
        mapEntry = statMap[ statName ];

        if (mapEntry == null) {
          if (! Warned.hasOwnProperty(path)) {
            Warned[path] = true;

            console.error('*** _parse_numa_stat( %s ): Unexpected name[ %s ]:',
                          path, statName, stat);
          }
          return;
        }

      } else {
        // This is a numa-node stat
        numaNode = statName.slice(1);
      }

      const stateName   = `numa_stat_${numaNode}_${mapEntry.name}`;
      const metricName  = `os_cgroup_memory_numa_stat_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {
          numa_node : numaNode,
          unit      : mapEntry.unit,
        }, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });
  });

  return metrics;
}

/**
 *  Parse a memory 'oom_control' metric.
 *  @method _parse_oom_control
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    oom_kill_disable  %int%
 *    under_oom         %int%
 *    oom_kill          %int%
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_oom_control( mod, path, controlMap, lines ) {
  // assert( controlMap === KeyMap[ 'memory.oom_control' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_oom_control( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const statName    = cols[0];
    const mapEntry    = controlMap[ statName ];
    if (mapEntry == null) {
      if (! Warned.hasOwnProperty(path)) {
        Warned[path] = true;

        console.error('*** _parse_oom_control( %s ): Unexpected name[ %s ]:',
                      path, statName, cols);
      }
      return;
    }

    const metricName  = `os_cgroup_memory_oom_control_${mapEntry.name}`;
    const newValue    = cols[1];
    const oldValue    = oldState[ metricName ];

    // Record the new metric value
    newState[ metricName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {unit: mapEntry.unit}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/**
 *  Parse a memory 'pressure' metric.
 *  @method _parse_pressure
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  pressureMap The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    some avg10=0.00 avg60=0.00 avg300=0.00 total=1
 *    full avg10=0.00 avg60=0.00 avg300=0.00 total=1
 *
 *  From https://facebookmicrosites.github.io/cgroup2/docs/pressure-metrics.html
 *    The output format for CPU is:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    and for memory and IO:
 *      some avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *      full avg10=0.00 avg60=0.00 avg300=0.00 total=0
 *
 *    Memory and IO show two metrics: some and full. The CPU controller shows
 *    only the some metric. The values for both some and full are shown in
 *    running averages over the last 10 seconds, 1 minute, and 5 minutes,
 *    respectively. The total statistic is the accumulated microseconds.
 *
 *    PSI shows pressure as a percentage of walltime in which some or all
 *    processes are blocked on a resource:
 *    - 'some' is the percentage of walltime that some (one or more) tasks were
 *      delayed due to lack of resources—for example, a lack of memory.
 *    - 'full' is the percentage of walltime in which all tasks were delayed by
 *      lack of resources, i.e., the amount of completely unproductive time.
 *
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_pressure( mod, path, pressureMap, lines ) {
  // assert( pressureMap === KeyMap[ 'memory.pressure' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  if (lines.length !== 2) {
    console.error('*** _parse_pressure( %s ): expected 2 line, see %d',
                  path, lines.length);
  }

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 5) {
      console.error('*** _parse_pressure( %s ): line %d has '
                    +       '%d columns instead of 5:',
                    path, idex, cols.length, line);
      return;
    }

    // Process this line
    const lineName  = cols.shift(); // some | full

    // assert( lineName === 'some' || lineName === 'full');

    cols.forEach( stat => {
      const [statName, newValue]  = stat.split('=');
      const mapEntry              = pressureMap[ statName ];

      if (mapEntry == null) {
        if (! Warned.hasOwnProperty(path)) {
          Warned[path] = true;

          console.error('*** _parse_pressure( %s ): Unexpected name[ %s ]:',
                        path, statName, stat);
        }
        return;
      }

      const metricName  = `os_cgroup_memory_pressure_${lineName}_${mapEntry.name}`;

      // Retrieve the old value
      const oldValue  = oldState[ metricName ];

      // Record the new metric value
      newState[ metricName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {unit: mapEntry.unit}, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });

  });

  return metrics;
}

/**
 *  Parse a memory 'stat' metric.
 *  @method _parse_stat
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    cache %int%
 *    rss %int%
 *    rss_huge %int%
 *    shmem %int%
 *    mapped_file %int%
 *    dirty %int%
 *    writeback %int%
 *    pgpgin %int%
 *    pgpgout %int%
 *    pgfault %int%
 *    pgmajfault %int%
 *    inactive_anon %int%
 *    active_anon %int%
 *    inactive_file %int%
 *    active_file %int%
 *    unevictable %int%
 *    hierarchical_memory_limit %int%
 *    total_cache %int%
 *    total_rss %int%
 *    total_rss_huge %int%
 *    total_shmem %int%
 *    total_mapped_file %int%
 *    total_dirty %int%
 *    total_writeback %int%
 *    total_pgpgin %int%
 *    total_pgpgout %int%
 *    total_pgfault %int%
 *    total_pgmajfault %int%
 *    total_inactive_anon %int%
 *    total_active_anon %int%
 *    total_inactive_file %int%
 *    total_active_file %int%
 *    total_unevictable %int%
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/memory.txt
 *    cache         - # of bytes of page cache memory.
 *    rss           - # of bytes of anonymous and swap cache memory (includes
 *                    transparent hugepages).
 *    rss_huge      - # of bytes of anonymous transparent hugepages.
 *    mapped_file   - # of bytes of mapped file (includes tmpfs/shmem)
 *    pgpgin        - # of charging events to the memory cgroup. The charging
 *                    event happens each time a page is accounted as either
 *                    mapped anon page(RSS) or cache page(Page Cache) to the
 *                    cgroup.
 *    pgpgout       - # of uncharging events to the memory cgroup. The
 *                    uncharging event happens each time a page is unaccounted
 *                    from the cgroup.
 *    swap          - # of bytes of swap usage
 *    dirty         - # of bytes that are waiting to get written back to the
 *                    disk.
 *    writeback     - # of bytes of file/anon cache that are queued for syncing
 *                    to disk.
 *    inactive_anon - # of bytes of anonymous and swap cache memory on inactive
 *                    LRU list.
 *    active_anon   - # of bytes of anonymous and swap cache memory on active
 *                    LRU list.
 *    inactive_file - # of bytes of file-backed memory on inactive LRU list.
 *    active_file   - # of bytes of file-backed memory on active LRU list.
 *    unevictable	  - # of bytes of memory that cannot be reclaimed (mlocked
 *                    etc).
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_stat( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'memory.stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_stat( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const statName    = cols[0];
    const mapEntry    = statMap[ statName ];
    if (mapEntry == null) {
      if (! Warned.hasOwnProperty(path)) {
        Warned[path] = true;

        console.error('*** _parse_stat( %s ): Unexpected name[ %s ]:',
                      path, statName, cols);
      }
      return;
    }

    const metricName  = `os_cgroup_memory_stat_${mapEntry.name}`;
    const newValue    = cols[1];
    const oldValue    = oldState[ metricName ];

    // Record the new metric value
    newState[ metricName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {unit: mapEntry.unit}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_memory;
