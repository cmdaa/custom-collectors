const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_freezer_'
 */
const KeyMap  = {
  /* :NOTE: We use os_cgroup_freezer_freeze_ vice os_cgroup_freezer_freezing_
   *        to match kernel-beat
   */
  'freezer.self_freezing'   :{  // os_cgroup_freezer_freeze_%type%
    parser  : _parse_freezing,
    type    : 'self',
    unit    : 'boolean',
  },
  'freezer.parent_freezing' :{  // os_cgroup_freezer_freeze_%type%
    parser  : _parse_freezing,
    type    : 'parent',
    unit    : 'boolean',
  },

  'freezer.state'           :{  // os_cgroup_freezer_state
    parser  : _parse_state,
    unit    : 'state',

    // Value mapping
    valueMap: {
      'THAWED'  : 0,
      'FREEZING': 1,
      'FROZEN'  : 2,
    },
  },
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the freezer subsystem
 *  @method parse_freezer
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    freezer.self_freezing   [ %int% ] (boolean)
 *    freezer.parent_freezing [ %int% ] (boolean)
 *    freezer.state           [ 'THAWED' | 'FREEZING' | 'FROZEN' ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_freezer( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'freezer' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_freezer( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  if (typeof(mapEntry.parser) !== 'function') {
    console.error('*** parse_freezer( %s ): Missing parser for [ %s ]',
                  path, baseName);
    return;
  }

  return mapEntry.parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a freezer 'freezing' metric.
 *  @method _parse_freezing
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    freezer.self_freezing   [ %int% ] (boolean)
 *    freezer.parent_freezing [ %int% ] (boolean)
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/freezer-subsystem.txt
 *    The following cgroupfs files are created by cgroup freezer.
 *
 *    * freezer.self_freezing: Read only.
 *
 *      Shows the self-state. 0 if the self-state is THAWED; otherwise, 1.
 *      This value is 1 iff the last write to freezer.state was "FROZEN".
 *
 *    * freezer.parent_freezing: Read only.
 *
 *      Shows the parent-state.  0 if none of the cgroup's ancestors is frozen;
 *      otherwise, 1.
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_freezing( mod, path, mapEntry, lines ) {
  const metricName  = `os_cgroup_freeze_freezing_${mapEntry.type}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = lines[0];
  const labels      = mod.generateLabels( {
                        freezer_state : lines[0],
                        unit          : mapEntry.unit,
                      } );

  if (lines.length !== 1) {
    console.error('*** _parse_freezing( %s ): Expected 1 line, see %d',
                  path, lines.length);
  }

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a freezer 'state' metric.
 *  @method _parse_state
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    freezer.state           [ 'THAWED' | 'FREEZING' | 'FROZEN' ]
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/freezer-subsystem.txt
 *    The following cgroupfs files are created by cgroup freezer.
 *
 *    * freezer.state: Read-write.
 *
 *      When read, returns the effective state of the cgroup - "THAWED",
 *      "FREEZING" or "FROZEN". This is the combined self and parent-states.
 *      If any is freezing, the cgroup is freezing (FREEZING or FROZEN).
 *
 *      FREEZING cgroup transitions into FROZEN state when all tasks belonging
 *      to the cgroup and its descendants become frozen. Note that a cgroup
 *      reverts to FREEZING from FROZEN after a new task is added to the cgroup
 *      or one of its descendant cgroups until the new task is frozen.
 *
 *      When written, sets the self-state of the cgroup. Two values are allowed
 *      - "FROZEN" and "THAWED". If FROZEN is written, the cgroup, if not
 *      already freezing, enters FREEZING state along with all its descendant
 *      cgroups.
 *
 *      If THAWED is written, the self-state of the cgroup is changed to
 *      THAWED.  Note that the effective state may not change to THAWED if the
 *      parent-state is still freezing. If a cgroup's effective state becomes
 *      THAWED, all its descendants which are freezing because of the cgroup
 *      also leave the freezing state.
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_state( mod, path, mapEntry, lines ) {
  const metricName  = `os_cgroup_freeze_state`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = mapEntry.valueMap[ lines[0] ];
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  if (lines.length !== 1) {
    console.error('*** _parse_state( %s ): Expected 1 line, see %d',
                  path, lines.length);
  }

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_freezer;
