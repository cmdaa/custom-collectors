const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  Note that cpuacct.stat has a sub-map for individual lines/fields.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_cpuacct_'
 */
const KeyMap  = {
  'cpuacct.usage'               :{name:'total',   unit:'nanoseconds'},
  'cpuacct.usage_user'          :{name:'user',    unit:'nanoseconds'},
  'cpuacct.usage_sys'           :{name:'sys',     unit:'nanoseconds'},

  'cpuacct.stat': {             // os_cgroup_cpuacct_stat_
    parser: _parse_stat,

    'user'                      :{name:'user',    unit:'nanoseconds'},
    'system'                    :{name:'sys',     unit:'nanoseconds'},
  },
  'cpuacct.usage_all': {        /* os_cgroup_cpuacct_usage_
                                 *    label 'cpu_idex'
                                 */
    parser: _parse_usage_all,                     unit:'nanoseconds',
  },

  'cpuacct.usage_percpu': {     /* os_cgroup_cpuacct_usage_
                                 *    label 'cpu_idex'
                                 */
    parser: _parse_usage_percpu,  name:'total',   unit:'nanoseconds',
  },

  'cpuacct.usage_percpu_user':{ /* os_cgroup_cpuacct_usage_
                                 *    label 'cpu_idex'
                                 */
    parser: _parse_usage_percpu,  name:'user',    unit:'nanoseconds',
  },

  'cpuacct.usage_percpu_sys': { /* os_cgroup_cpuacct_usage_
                                 *    label 'cpu_idex'
                                 */
    parser: _parse_usage_percpu,  name:'sys',     unit:'nanoseconds',
  },
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the cpuacct subsystem
 *  @method parse_cpuacct
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    cpuacct.stat              [ 'user %int%', 'system %int%' ]
 *    cpuacct.usage             [ %int% ]
 *    cpuacct.usage_all         [ 'cpu user system',
 *                                '%int% %int% %int%', 
 *                                ...
 *                              ]
 *    cpuacct.usage_percpu      [ %int%, ... ]
 *    cpuacct.usage_percpu_sys  [ %int%, ... ]
 *    cpuacct.usage_percpu_user [ %int%, ... ]
 *    cpuacct.usage_sys         [ %int% ]
 *    cpuacct.usage_user        [ %int% ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_cpuacct( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'cpuacct' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_cpuacct( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    cpu.cfs_period_us [ %int% ]
 *    cpu.cfs_quota_us  [ %int% ]
 *    cpu.shares        [ %int% ]
 *    cpu.uclamp.max    [ %float% ] (MAY be 'max' => -1)
 *    cpu.uclamp.min    [ %float% ]
 *
 *  From https://patchwork.kernel.org/project/linux-pm/patch/20190822132811.31294-2-patrick.bellasi@arm.com/
 *    - uclamp.min: defines the minimum utilization which should be considered
 *      i.e. the RUNNABLE tasks of this group will run at least at a minimum
 *           frequency which corresponds to the uclamp.min utilization
 *
 *    - uclamp.max: defines the maximum utilization which should be considered
 *      i.e. the RUNNABLE tasks of this group will run up to a maximum
 *           frequency which corresponds to the uclamp.max utilization
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_cpuacct_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a cpuacct 'stat' metric.
 *  @method _parse_stat
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    user %int%
 *    system %int%
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_stat( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'cpuacct.stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_stat( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const statName    = cols[0];
    const mapEntry    = statMap[ statName ];
    if (mapEntry == null) {
      if (! Warned.hasOwnProperty(path)) {
        Warned[path] = true;

        console.error('*** _parse_stat( %s ): Unexpected name[ %s ]:',
                      path, statName, cols);
      }
      return;
    }

    const metricName  = `os_cgroup_cpuacct_stat_${mapEntry.name}`;
    const newValue    = cols[1];
    const oldValue    = oldState[ metricName ];

    // Record the new metric value
    newState[ metricName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {unit: mapEntry.unit}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/**
 *  Parse a cpuacct 'usage_all' metric.
 *  @method _parse_usage_all
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    cpu user system
 *    %int% %int% %int%   # One line per cpu
 *    ...
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_usage_all( mod, path, statMap, lines ) {
  // assert( statMap === KeyMap[ 'cpuacct.stat' ] );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels( {unit: statMap.unit} );
  let   metrics;

  // Process each usage line, one per cpu (plus an initial header)
  lines.forEach( (line,idex) => {
    if (idex === 0) {
      // Skip the header: 'cpu user system'
      return;
    }

    const cols  = line.split(/\s+/g);
    if (cols.length !== 3) {
      console.error('*** _parse_usage_all( %s ): line %d has '
                    +       '%d columns instead of 3:',
                    path, idex, cols.length, line);
      return;
    }

    // cpu_idex user_usage system_usage
    const cpuIdex = cols[0];
    const stats   = [
      {type:'user', value:cols[1]},
      {type:'sys',  value:cols[2]},
    ];

    // Generate two metrics -- usage_user and usage_sys
    stats.forEach( stat => {
      // :XXX: Use a special `stateName` to account for the cpu index
      const stateName   = `cpuacct_usage_all_${cpuIdex}_${stat.type}`;
      const metricName  = `os_cgroup_cpuacct_usage_${stat.type}`;
      const newValue    = stat.value;
      const oldValue    = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: Object.assign( {cpu_idex: cpuIdex}, labels ),
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    });
  });

  return metrics;
}

/**
 *  Parse a cpuacct per-cpu metric.
 *  @method _parse_usage_percpu
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  statMap     The sub-map for this parser {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    %int%   # One line per cpu
 *    ...
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_usage_percpu( mod, path, statMap, lines ) {
  // assert( lines.length === 1 );
  // assert( statMap === KeyMap[ 'cpuacct.usage_percpu*' ] );
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const metricName  = `os_cgroup_cpuacct_usage_${statMap.name}`;
  const labels      = mod.generateLabels( {unit: statMap.unit} );
  let   metrics;

  if (lines.length !== 1) {
    console.error('*** _parse_usage_percpu( %s ): %d lines instead of 1',
                  path, lines.length);
  }

  // Split the single line and process each cpu entry.
  const cpus  = lines[0].split(/\s+/g);
  cpus.forEach( (newValue, cpuIdex) => {
    // :XXX: Use a special `stateName` to account for the cpu index
    const stateName = `cpuacct_usage_${cpuIdex}_${statMap.name}`;
    const oldValue  = oldState[ stateName ];

    // Record the new metric value
    newState[ stateName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {cpu_idex: cpuIdex}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_cpuacct;
