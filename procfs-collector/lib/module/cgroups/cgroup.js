const Metric  = require('../../metric');

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse top-level cgroup data from any cgroup subsystem
 *  @method parse_cgroup
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    cgroup.clone_children   [ %int% ]
 *    cgroup.events           [ 'populated %int%',
 *                              'frozen %int%' ]
 *    cgroup.freeze           [ %int% ]
 *    cgroup.max.depth        [ %int% ] (MAY be 'max')
 *    cgroup.max.descendants  [ %int% ] (MAY be 'max')
 *    cgroup.procs            [ %pid%, ... ]
 *    cgroup.sane_behavior    [ %int% ]
 *    cgroup.stat             [ 'nr_descendants %int%',
 *                              'nr_dying_descendants %int%' ]
 *    cgroup.threads          [ %tid%, ... ]
 *    cgroup.type             [ %string% ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_cgroup( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'cgroup' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = _generate_basename( names );
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  let   metrics;

  switch( baseName ) {
    case 'events':
      metrics = _parse_events( mod, path, lines );
      break;

    case 'stat':
      metrics = _parse_stat( mod, path, lines );
      break;

    case 'procs':
    case 'threads':
      metrics = _parse_ids( mod, path, baseName, lines );
      break;

    case 'type':
      // SKIP -- no metric
      break;

    default:
      if (lines.length === 1) {
        /* clone_children
         * freeze
         * max_depth
         * max_descendants
         * sane_behavior
         */
        metrics = _parse_int( mod, path, baseName, lines );

      } else if (! Warned.hasOwnProperty(path)) {
        Warned[path] = true;

        console.error('*** parse_cgroup( %s ): Unexpected baseName[ %s ] '
                      +     'with multiple[ %d ] lines',
                      path, baseName, lines.length);
      }
      break;
  }

  return metrics;
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Given the components of the base cgroup file name split by '.', generate
 *  the basename that will be used to create ths final metric name.
 *  @method _generate_basename
 *  @param  names   The components of the base cgroup file name split by '.'
 *                  {Array};
 *
 *  Examples:
 *    names.join('.')                       : basename
 *    --------------------------------------:--------------------------------
 *    cgroup.clone_children                 : clone_children
 *    cgroup.events                         : events
 *    cgroup.freeze                         : freeze
 *    cgroup.max.depth                      : max_depth
 *    cgroup.max.descendants                : max_descendants
 *    cgroup.procs                          : procs
 *    cgroup.sane_behavior                  : sane_behavior
 *    cgroup.stat                           : stat
 *    cgroup.threads                        : threads
 *    cgroup.type                           : type
 *
 *  @return The basename {String};
 *  @private
 */
function _generate_basename( names ) {
  return names.slice(1)     // strip 'cgroup'
            .join('_');     // '_' seperated string
}

/**
 *  Parse a simple, single integer metric.
 *  @method _parse_int
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  metricBase  The basename of any generated metric {String};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `metricBase`:
 *    clone_children   [ %int% ]
 *    freeze           [ %int% ]
 *    max_depth        [ %int% ] (MAY be 'max' === -1)
 *    max_descendants  [ %int% ] (MAY be 'max' === -1)
 *    sane_behavior    [ %int% ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_int( mod, path, metricBase, lines ) {
  // assert( lines.length === 1 );
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const oldValue  = oldState[ metricBase ];
  const newValue  = (lines[0] === 'max' ? -1 : lines[0]);
  const labels    = mod.generateLabels( {unit:'boolean'} );

  if (metricBase.startsWith('max')) {
    labels.unit = 'count';

  }

  // Record the new metric value
  newState[ metricBase ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : `os_cgroup_${metricBase}`,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a cgroup 'events' metric.
 *  @method _parse_events
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    populated %int%
 *    frozen %int%
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_events( mod, path, lines ) {
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels( {unit:'count'} );
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_events( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const name        = cols[0];
    const newValue    = cols[1];
    const stateName   = `events_${name}`;
    const metricName  = `os_cgroup_${stateName}`;
    const oldValue    = oldState[ stateName ];

    // Record the new metric value
    newState[ stateName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: labels,
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/**
 *  Parse a cgroup 'stat' metric.
 *  @method _parse_stat
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  `lines` should have the form:
 *    nr_descendants %int%
 *    nr_dying_descendants %int%
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_stat( mod, path, lines ) {
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels( {unit:'count'} );
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_stats( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const name        = cols[0].replace('nr_', '');
    const newValue    = cols[1];
    const stateName   = `stat_${name}`;
    const metricName  = `os_cgroup_${stateName}`;
    const oldValue    = oldState[ stateName ];

    // Record the new metric value
    newState[ stateName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: labels,
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/**
 *  Parse an array of ids.
 *  @method _parse_ids
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  metricBase  The basename of any generated metric {String};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `metricBase`:
 *    procs            [ %pid%, ... ]
 *    threads          [ %tid%, ... ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_ids( mod, path, metricBase, lines ) {
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const labels      = mod.generateLabels( {unit:'count'} );
  const newValue    = lines.length;
  const stateName   = metricBase;
  const metricName  = `os_cgroup_${stateName}`;
  const oldValue    = oldState[ stateName ];

  // Record the new metric value
  newState[ stateName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_cgroup;
