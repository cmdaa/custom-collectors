const Metric  = require('../../metric');

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the devices subsystem
 *  @method parse_devices
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *
 *  There is only one readable metric available from this subsystem:
 *    devices.list  [ '%type% %major%:%minor% %mode%', ... ]
 *
 *  Examples:
 *    [ 'a *:* rwm' ]
 *
 *    [ 'c 1:3 rwm',    'c 1:5 rwm',
 *      'c 1:7 rwm',    'c 1:8 rwm',
 *      'c 1:9 rwm',    'c 5:0 rwm',
 *      'c 5:1 rwm',    'c 5:2 rwm',
 *      'c 136:* rwm',  'c 137:* rwm',
 *      'c 138:* rwm',  'c 139:* rwm',
 *      'c 140:* rwm',  'c 141:* rwm',
 *      'c 142:* rwm',  'c 143:* rwm',
 *      'c 195:0 rwm',  'c 195:255 rwm',
 *      'c 511:0 rwm',  'c 195:254 rwm',
 *      'c 10:239 rwm', 'c 10:200 rwm',
 *      'c 226:1 rwm',  'c 226:129 rwm',
 *      'c 226:0 rwm',  'c 226:128 rwm' ]
 *
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/devices.txt:
 *    A device cgroup associates a device access whitelist with each cgroup.
 *    A whitelist entry has 4 fields.
 *      'type' is a (all), c (char), or b (block).
 *        'all' means it applies to all types and all major and minor numbers.
 *      Major and minor are either an integer or * for all.
 *      Access is a composition of r (read), w (write), and m (mknod).
 *  
 *  For these metrics, we will report:
 *    'type', 'major', and 'minor' as labels:
 *       device_type    'char' | 'block' | 'all'
 *       device_id      '%major%:%minor%'
 *       device_access  string-based-access-value
 *       unit           'flags'
 *
 *    'access' will also be reported as the metric value formatted similar to a
 *    file-mode-type value with one bit per mode:
 *        mode       binary    decimal
 *         m      ==  001   =>   1
 *         w      ==  010   =>   2
 *         wm     ==  011   =>   3
 *         r      ==  100   =>   4
 *         rm     ==  101   =>   5
 *         rw     ==  110   =>   6
 *         rwm    ==  111   =>   7
 *    
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_devices( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'devices' );
  // assert( names[1] === 'list' );
  // assert( Array.isArray(lines) && lines.length > 0 );
  if (names[1] !== 'list') {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_devices( %s ): Unexpected name[ %s ] '
                    +     'with %d lines',
                    path, names.join('.'), lines.length);
    }
    return;
  }

  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels();
  let   metrics;

  lines.forEach( (line, idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }
    if (cols.length !== 3) {
      console.error('*** parse_devices( %s ): line %d has '
                    +       '%d columns instead of 3:',
                    path, idex, cols.length, line);
      return;
    }

    const type      = _parse_type( path, cols[0] );
    const dev       = cols[1];
    const access    = cols[2];
    const newValue  = _parse_access( path, access );
    const stateName = `devices_${dev}`;
    const oldValue  = oldState[ stateName ];

    // Record the new metric value
    newState[ stateName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : `os_cgroup_devices`,
      value : newValue,
      labels: Object.assign(  { device_type   : type,
                                device_id     : dev,
                                device_access : access,
                                unit          : 'flags' }, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Convert a single-character device type into a full type.
 *  @method _parse_type
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  type    The device type {String};
 *
 *    type    full-type
 *     a       all
 *     c       char
 *     b       block
 *
 *  @return The numeric equivalent {Number};
 *  @private
 */
function _parse_type( path, type ) {
  let fullType;

  switch( type ) {
    case 'a'  : fullType = 'all';   break;
    case 'c'  : fullType = 'char';  break;
    case 'b'  : fullType = 'block'; break;

    default:
      console.error('*** _parse_type( %s, %s ): cannot convert',
                    path, type);
  }

  return fullType;
}

/**
 *  Convert a string-based device list access to a numeric value.
 *  @method _parse_access
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  access  The device list access {String};
 *
 *    access     binary    decimal
 *     m      ==  001   =>   1
 *     w      ==  010   =>   2
 *     wm     ==  011   =>   3
 *     r      ==  100   =>   4
 *     rm     ==  101   =>   5
 *     rw     ==  110   =>   6
 *     rwm    ==  111   =>   7
 *
 *  @return The numeric equivalent {Number};
 *  @private
 */
function _parse_access( path, access ) {
  let val = 0;

  switch( access ) {
    case 'm'  : val = 1;  break;
    case 'w'  : val = 2;  break;
    case 'wm' : val = 3;  break;
    case 'r'  : val = 4;  break;
    case 'rm' : val = 5;  break;
    case 'rw' : val = 6;  break;
    case 'rwm': val = 7;  break;

    default:
      console.error('*** _parse_access( %s, %s ): cannot convert',
                    path, access);
  }

  return val;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_devices;
