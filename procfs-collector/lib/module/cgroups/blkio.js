const Metric  = require('../../metric');

/**
 *  Entries to map between substrings of the last portion of the cgroup file
 *  name split by '.' to the unit for the contained metric data.
 */
const UnitMap = [
  { str: '_time',   unit: 'seconds' },
  { str: '_sector', unit: 'sectors' },
  { str: '_bps',    unit: 'bytes/second' },
  { str: '_byte',   unit: 'bytes' },
  { str: '_iops',   unit: 'iops' },
  { default: true,  unit: 'count' },
];

/**
 *  Parse data from the blkio cgroup subsystem
 *  @method parse_blkio
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  If there is a single line, the metric is a simple, single value fully named
 *  by `names`.
 *
 *  For multiple lines, there are multiple, device-specific metrics of the
 *  form:
 *    %major%:%minor% %Stat% %value%
 *
 *    major   The major device number;
 *    minor   The minor device number;
 *    Stat    The statistic (Read | Write | Sync | Async | Discard | Total);
 *    value   The statistic value, in bytes;
 *
 *  with a final line that is:
 *    Total: %value%
 *
 *  @note Labels provided by kernel-beat that are missing with this parser:
 *          weight      : report['blkio.weight']
 *          leaf_weight : report['blkio.leaf_weight']
 *          dev_id      : report.devices[].id
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_blkio( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'blkio' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = _generate_basename( names );
  const unit        = _get_unit( names );
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const metrics     = [];

  /**************************************************************************
   * Process lines, possibly per-device metrics
   */
  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    let stateKey  = idex;
    let metricName;
    let labels;
    let newValue;

    if (cols.length === 1) {
      // No label / value : simple numeric metric
      // assert( lines.length === 1 );
      metricName = `os_cgroup_blkio_device_${baseName}`;
      labels     = {};
      newValue   = cols[0];

    } else if (cols[0] === 'Total:') {
      // Final line: "Total: %value%"
      stateKey   = 'total';
      metricName = `os_cgroup_blkio_device_total_${baseName}`;
      labels     = {unit};
      newValue   = cols[1];

    } else {
      // assert( cols.length === 3 );

      // Device line: %major%:%minor% %Stat% %value%
      const device  = cols[0];                // %major%:%minor%
      const ioKey   = cols[1].toLowerCase();  // %Stat%

      stateKey   = `${device}:${ioKey}`;
      metricName = `os_cgroup_blkio_device_${ioKey}_${baseName}`;
      labels     = { device, unit };
      newValue   = cols[2];
    }

    // Record the new metric value
    newState[ stateKey ] = newValue;

    // Retrieve the old metric value
    const oldValue  = oldState[ stateKey ];

    /*
    console.log('=== parse_blkio( %s ): line %d / %d[ %s ], '
                +   'metricName[ %s ], newValue[ %s ], oldValue[ %s ]',
                path, idex, lines.length, line, metricName, newValue, oldValue);
    // */

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: mod.generateLabels( labels ),
    };

    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Given the components of the base cgroup file name split by '.', generate
 *  the basename that will be used to create ths final metric name.
 *  @method _generate_basename
 *  @param  names   The components of the base cgroup file name split by '.'
 *                  {Array};
 *
 *  Examples:
 *    names.join('.')                       : basename
 *    --------------------------------------:--------------------------------
 *    blkio.throttle.io_serviced            : throttle_io_serviced
 *    blkio.throttle.io_serviced_recursive  : throttle_io_serviced_recursive
 *    blkio.throttle.io_service_bytes       : throttle_io_service
 *    blkio.throttle.read_iops_device       : throttle_read
 *    blkio.throttle.read_bps_device        : throttle_read
 *
 *  @return The basename {String};
 *  @private
 */
function _generate_basename( names ) {
  return names.slice(1)     // strip 'blkio'
            .join('_')      // '_' seperated string
            .replace(/_(time|sectors|bps|bytes|iops)/g, '') // Remove unit
            .replace('_device', '');                        // Remove '_device'
}

/**
 *  Given the components of the base cgroup file name split by '.', retrieve
 *  the appropriate unit from `UnitMap`.
 *  @method _get_unit
 *  @param  names   The components of the base cgroup file name split by '.'
 *                  {Array};
 *
 *  Examples:
 *    names.last             : unit
 *    -----------------------:--------------------------------
 *    io_serviced            : count
 *    io_serviced_recursive  : count
 *    io_service_bytes       : bytes
 *    read_iops_device       : iops
 *    read_bps_device        : bytes/second
 *
 *  @return The unit associated with the given metric name {String};
 *  @private
 */
function _get_unit( names ) {
  const lastName  = names[ names.length-1 ];
  const match     = UnitMap.find( (entry) => {
                      if (entry.default)  {
                        // We've reached the end of the UnitMap, use it
                        return true;
                      }

                      /* Check if this unit map string appears in the lastName
                       * and if so, use the entries unit.
                       */
                      return lastName.includes(entry.str);
                    });

  // assert( match != null );
  return (match ? match.unit : null);
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_blkio;
