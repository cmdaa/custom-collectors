const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_cpuset_'
 *
 *  Reference:
 *    https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/cpusets.html
 */
const KeyMap  = {
  'cpuset.cpu_exclusive'      :{name:'cpu_exclusive',       unit:'boolean'},
  'cpuset.cpus'               :{
    // list of CPUs available
    // lines: [ '0-15' ]  : CPUs
    //    label: cpu_index
    parser: _parse_set,         name:'cpu',                 unit:'boolean',
                                label: 'cpu_idex',
  },
  'cpuset.effective_cpus'     :{
    // list of CPUs currently in use
    // lines: [ '0-15' ]  : CPUs
    //    label: cpu_index
    parser: _parse_set,         name:'effective_cpu',       unit:'boolean',
                                label: 'cpu_idex',
  },
  'cpuset.mems'               :{
    // list of Memory Nodes available
    // lines: [ '0' ]     : Memory nodes
    //    label: mem_node
    parser: _parse_set,         name:'mem',                 unit:'boolean',
                                label: 'mem_node',
  },
  'cpuset.effective_mems'     :{
    // list of Memory Nodes currently in use
    // lines: [ '0' ]     : Memory nodes
    //    label: mem_node
    parser: _parse_set,         name:'effective_mem',       unit:'boolean',
                                label: 'mem_node',
  },

  'cpuset.mem_exclusive'      :{name:'mem_exclusive',       unit:'boolean'},
  'cpuset.mem_hardwall'       :{name:'mem_hardwall',        unit:'boolean'},
  'cpuset.memory_migrate'     :{name:'mem_migrate',         unit:'boolean'},

  'cpuset.memory_pressure'    :{name:'mem_pressure',        unit:'pressure'},
  'cpuset.memory_pressure_enabled':
                               {name:'mem_pressure',        unit:'boolean'},

  'cpuset.memory_spread_page' :{name:'mem_spread_page',     unit:'boolean'},
  'cpuset.memory_spread_slab' :{name:'mem_spread_slab',     unit:'boolean'},

  'cpuset.sched_load_balance' :{name:'sched_load_balance',  unit:'boolean'},

  // the searching range when migrating tasks
  'cpuset.sched_relax_domain_level':
                               {name:'sched_relax_domain_level',
                                                            unit:'boolean'},
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the cpuset subsystem
 *  @method parse_cpuset
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    cpuset.cpu_exclusive            [ '1' ]
 *    cpuset.cpus                     [ '0-15' ]
 *    cpuset.effective_cpus           [ '0-15' ]
 *    cpuset.effective_mems           [ '0' ]
 *    cpuset.mem_exclusive            [ '1' ]
 *    cpuset.mem_hardwall             [ '0' ]
 *    cpuset.memory_migrate           [ '0' ]
 *    cpuset.memory_pressure          [ '0' ]
 *    cpuset.memory_pressure_enabled  [ '0' ]
 *    cpuset.memory_spread_page       [ '0' ]
 *    cpuset.memory_spread_slab       [ '0' ]
 *    cpuset.mems                     [ '0' ]
 *    cpuset.sched_load_balance       [ '1' ]
 *    cpuset.sched_relax_domain_level [ '-1' ]
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_cpuset( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'cpuset' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_cpuset( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_bool);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single boolean metric.
 *  @method _parse_bool
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_bool( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_cpuset_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = lines[0];
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a cpu or memory set.
 *  @method _parse_set
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_set( mod, path, mapEntry, lines ) {
  const metricName  = `os_cgroup_cpuset_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const labels      = mod.generateLabels();
  let   metrics;

  lines.forEach( (line,idex) => {
    let [min,max] = line.split('-');
    if (max == null) { max = min };

    for (idex = min; idex <= max; idex++) {
      // :XXX: Use a special `stateName` to account for the cpu/mem index
      const stateName   = `cpuset_${mapEntry.name}_${idex}`;
      const newValue    = 1;
      const oldValue    = oldState[ stateName ];

      // Record the new metric value
      newState[ stateName ] = newValue;

      if (newValue == oldValue) {
        // No change, no metric
        return;
      }

      // Include a 'cpu_idex' or 'mem_node' label
      labels[mapEntry.label] = idex;

      // Create a new metric.
      const data  = {
        name  : metricName,
        value : newValue,
        labels: labels,
      };

      if (! Array.isArray(metrics)) { metrics = [] }
      metrics.push( new Metric( data ) );
    }
  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_cpuset;
