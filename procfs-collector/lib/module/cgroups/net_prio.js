const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_net_prio_'
 */
const KeyMap  = {
  'net_prio.prioidx'  :{name:'priority',    unit:'index' },
  'net_prio.ifpriomap':{parser: _parse_map, unit:'priority' },
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the net_prio subsystem
 *  @method parse_net_prio
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/net_prio.txt
 *    net_prio.prioidx
 *      This file is read-only, and is simply informative.  It contains a
 *      unique integer value that the kernel uses as an internal representation
 *      of this cgroup.
 *
 *    net_prio.ifpriomap
 *      This file contains a map of the priorities assigned to traffic
 *      originating from processes in this group and egressing the system on
 *      various interfaces. It contains a list of tuples in the form
 *      <ifname priority>. Contents of this file can be modified by echoing a
 *      string into the file using the same tuple format.
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_net_prio( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'net_prio' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_net_prio( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    net_prio.prioidx  [ %int% ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_net_prio_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a cgroup 'ifpriomap' metric.
 *  @method _parse_map
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    net_prio.ifpriomap  [ '%iface% %priority%, ... ]
 *
 *  The `iface` will be reported via label:
 *    iface   interface-name
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_map( mod, path, mapEntry, lines ) {
  const oldState  = mod.get_current_state( path );
  const newState  = mod.get_new_state( path );
  const labels    = mod.generateLabels( {unit:mapEntry.unit} );
  let   metrics;

  lines.forEach( (line,idex) => {
    const cols  = line.split(/\s+/g);
    if (cols.length < 1) { return }

    if (cols.length !== 2) {
      console.error('*** _parse_map( %s ): line %d has '
                    +       '%d columns instead of 2:',
                    path, idex, cols.length, line);
      return;
    }

    const ifaceName   = cols[0];
    const newValue    = cols[1];
    const stateName   = `net_prio_${ifaceName}`;
    const metricName  = `os_cgroup_net_prio_priority`;
    const oldValue    = oldState[ stateName ];

    // Record the new metric value
    newState[ stateName ] = newValue;

    if (newValue == oldValue) {
      // No change, no metric
      return;
    }

    // Create a new metric.
    const data  = {
      name  : metricName,
      value : newValue,
      labels: Object.assign( {iface:ifaceName}, labels ),
    };

    if (! Array.isArray(metrics)) { metrics = [] }
    metrics.push( new Metric( data ) );
  });

  return metrics;
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_net_prio;
