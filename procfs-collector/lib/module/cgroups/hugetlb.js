const Metric  = require('../../metric');

/**
 *  Mapping between the metric name from within the cgroup content and the
 *  reported metric name suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_hugetlb_'
 *
 *  Ref: https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/hugetlb.html
 */
const KeyMap  = {
  'limit_in_bytes'          :{name:'limit',               unit:'bytes'},
  'max_usage_in_bytes'      :{name:'max_usage',           unit:'bytes'},
  'usage_in_bytes'          :{name:'usage',               unit:'bytes'},
  'failcnt'                 :{name:'fail',                unit:'count'},

  'rsvd.limit_in_bytes'     :{name:'reserved_limit',      unit:'bytes'},
  'rsvd.max_usage_in_bytes' :{name:'reserved_max_usage',  unit:'bytes'},
  'rsvd.usage_in_bytes'     :{name:'reserved_usage',      unit:'bytes'},
  'rsvd.failcnt'            :{name:'reserved_fail',       unit:'count'},
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the hugetlb subsystem
 *  @method parse_hugetlb
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    hugetlb.2MB.limit_in_bytes
 *    hugetlb.2MB.max_usage_in_bytes
 *    hugetlb.2MB.usage_in_bytes
 *    hugetlb.2MB.failcnt
 *    hugetlb.1GB.limit_in_bytes
 *    hugetlb.1GB.max_usage_in_bytes
 *    hugetlb.1GB.usage_in_bytes
 *    hugetlb.1GB.failcnt
 *
 *    hugetlb.2MB.rsvd.limit_in_bytes
 *    hugetlb.2MB.rsvd.max_usage_in_bytes
 *    hugetlb.2MB.rsvd.usage_in_bytes
 *    hugetlb.2MB.rsvd.failcnt
 *    hugetlb.1GB.rsvd.limit_in_bytes
 *    hugetlb.1GB.rsvd.max_usage_in_bytes
 *    hugetlb.1GB.rsvd.usage_in_bytes
 *    hugetlb.1GB.rsvd.failcnt
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_hugetlb( mod, path, names, lines ) {
  // assert( names.length > 0 );
  // assert( names[0] === 'hugetlb' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  if (names.length !== 3 && names.length !== 4) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_hugetlb( %s ): Expected a 3 or 4-part name, '
                    +         'see %d:',
                    path, names.length, names);
    }
    return;
  }

  if (lines.length !== 1) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_hugetlb( %s ): Expected a single line, see %d:',
                    path, lines.length, lines);
    }
    return;
  }

  const mapName   = ( names.length > 3
                        ? names.slice(2).join('.')
                        : names[2]);
  const tlbSize   = names[1];
  const mapEntry  = KeyMap[ mapName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_hugetlb( %s ): Unexpected metric[ %s ] '
                    +     'with %d lines',
                    path, mapName, lines.length);
    }
    return;
  }

  return _parse_num( mod, path, mapEntry, tlbSize, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  tlbSize     The size of the target TLB {String};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/hugetlb.txt
 *    Brief summary of control files
 *
 *      # set/show limit of "hugepagesize" hugetlb usage
 *      hugetlb.<hugepagesize>.limit_in_bytes
 *
 *      # show max "hugepagesize" hugetlb  usage recorded
 *      hugetlb.<hugepagesize>.max_usage_in_bytes
 *
 *      # show current usage for "hugepagesize" hugetlb
 *      hugetlb.<hugepagesize>.usage_in_bytes
 *
 *      # show the number of allocation failure due to HugeTLB limit
 *      hugetlb.<hugepagesize>.failcnt
 *
 *    For a system supporting three hugepage sizes (64k, 32M and 1G), the
 *    control files include:
 *
 *      hugetlb.1GB.limit_in_bytes
 *      hugetlb.1GB.max_usage_in_bytes
 *      hugetlb.1GB.usage_in_bytes
 *      hugetlb.1GB.failcnt
 *      hugetlb.64KB.limit_in_bytes
 *      hugetlb.64KB.max_usage_in_bytes
 *      hugetlb.64KB.usage_in_bytes
 *      hugetlb.64KB.failcnt
 *      hugetlb.32MB.limit_in_bytes
 *      hugetlb.32MB.max_usage_in_bytes
 *      hugetlb.32MB.usage_in_bytes
 *      hugetlb.32MB.failcnt
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, tlbSize, lines ) {
  // assert( lines.length === 1 );
  const metricName  = `os_cgroup_hugetlb_${mapEntry.name}`;
  const stateName   = `hugetlb_${tlbSize}_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ stateName ];
  const newValue    = lines[0];
  const labels      = mod.generateLabels( {
                        tlb_size: tlbSize,
                        unit    : mapEntry.unit,
                      } );

  // Record the new metric value
  newState[ stateName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_hugetlb;
