const Metric  = require('../../metric');

/**
 *  Mapping between the base cgroup name ('.' joined) and the metric name
 *  suffix and unit.
 *
 *  The metric name prefix applied to all is: 'os_cgroup_pids_'
 */
const KeyMap  = {
  'pids.max'    :{name:'max',           unit:'count'},
  'pids.current':{name:'current',       unit:'count'},
  'pids.events' :{name:'fork_failures', unit:'count', parser:_parse_events },
};

// Used to limit warnings to a single report
const Warned  = {};

/**
 *  Parse cgroup data from the pids subsystem
 *  @method parse_pids
 *  @param  mod     The controlling module {Cgroups};
 *  @param  path    The sysfs-relative path to the source cgroup file {String};
 *  @param  names   The basename of `path` split by '.' {Array};
 *  @param  lines   The content of the target cgroup file, split by new-line
 *                  {Array};
 *
 *  Observed `names` (re-joined with '.') and `lines`:
 *    pids.max      [ %int% | 'max' ]
 *    pids.current  [ %int% ]
 *    pids.events   [ 'max %int%' ]
 *
 *  From https://www.kernel.org/doc/Documentation/cgroup-v1/pids.txt
 *    To set a cgroup to have no limit, set pids.max to "max". This is the
 *    default for all new cgroups (N.B. that PID limits are hierarchical, so
 *    the most stringent limit in the hierarchy is followed).
 *
 *    pids.current tracks all child cgroup hierarchies, so parent/pids.current
 *    is a superset of parent/child/pids.current.
 *
 *    The pids.events file contains event counters:
 *      - max: Number of times fork failed because limit was hit.
 *
 *  @return On success, an array of new metrics {Array | undefined};
 */
function parse_pids( mod, path, names, lines ) {
  // assert( names[0] === 'pids' );
  // assert( Array.isArray(lines) && lines.length > 0 );

  const baseName    = names.join('.');
  const mapEntry    = KeyMap[ baseName ];

  if (mapEntry == null) {
    if (! Warned.hasOwnProperty(path)) {
      Warned[path] = true;

      console.error('*** parse_pids( %s ): Unexpected baseName[ %s ] '
                    +     'with %d lines',
                    path, baseName, lines.length);
    }
    return;
  }

  const parser  = (typeof(mapEntry.parser) === 'function'
                    ? mapEntry.parser
                    : _parse_num);

  return parser( mod, path, mapEntry, lines );
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Parse a simple, single numeric metric.
 *  @method _parse_num
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  Example `baseName`:
 *    cpu.cfs_period_us [ %int% ]
 *    cpu.cfs_quota_us  [ %int% ]
 *    cpu.shares        [ %int% ]
 *    cpu.uclamp.max    [ %float% ] (MAY be 'max' => -1)
 *    cpu.uclamp.min    [ %float% ]
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_num( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  if (lines.length !== 1) {
    console.error('*** _parse_num( %s ): expected 1 line, see %d',
                  path, lines.length);
  }

  const metricName  = `os_cgroup_pids_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const newValue    = (lines[0] === 'max' ? -1 : lines[0]);
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/**
 *  Parse a 'pids' events metric.
 *  @method _parse_events
 *  @param  mod         The controlling module {Cgroups};
 *  @param  path        The sysfs-relative path to the source cgroup file
 *                      {String};
 *  @param  mapEntry    The `KeyMap` entry for this metric {Object};
 *  @param  lines       The content of the target cgroup file, split by
 *                      new-line {Array};
 *
 *  @return An array of new metrics {Array | undefined};
 *  @private
 */
function _parse_events( mod, path, mapEntry, lines ) {
  // assert( lines.length === 1 );
  if (lines.length !== 1) {
    console.error('*** _parse_events( %s ): expected 1 line, see %d',
                  path, lines.length);
  }

  const metricName  = `os_cgroup_pids_${mapEntry.name}`;
  const oldState    = mod.get_current_state( path );
  const newState    = mod.get_new_state( path );
  const oldValue    = oldState[ metricName ];
  const cols        = lines[0].split(/\s+/g);

  if (cols.length < 2) {
    console.error('*** _parse_events( %s ): expected 2 columns, see %d:',
                  path, cols.length, lines[0]);
    return;
  }

  const newValue    = cols[1];
  const labels      = mod.generateLabels( {unit: mapEntry.unit} );

  // Record the new metric value
  newState[ metricName ] = newValue;

  if (newValue == oldValue) {
    // No change, no metric
    return;
  }

  // Create a new metric.
  const data  = {
    name  : metricName,
    value : newValue,
    labels: labels,
  };

  return [ new Metric( data ) ];
}

/* Private helpers }
 ****************************************************************************/

module.exports = parse_pids;
