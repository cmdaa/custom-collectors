const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based network data collection module.
 *  @class  Net
 *
 *  This module generates Network-based metrics (`os_net_iface` prefix) with a
 *  'name' label identifying the target interface:
 *		_receive                Receive/bytes number of received bytes;
 *    _receive_compressed     Receive/compressed number of compressed packets
 *                            received;
 *    _receive_error_dropped  Receive/drop number of received packets dropped;
 *    _receive_error          Receive/errs total number of receive errors;
 *    _receive_error_fifo     Receive/fifo number of receive FIFO buffer
 *                            errors;
 *    _receive_error_frame    Receive/frame number of packet framing errors;
 *    _receive_multicast      Receive/multicast number of multicast frames
 *                            received;
 *    _receive                Receive/packets number of received packets;
 *
 *    _transmit               Transmit/bytes transmitted bytes;
 *    _transmit_error_carrier Transmit/carrier number of carrier losses
 *                            detected on the interface;
 *    _transmit_collision     Transmit/colls number of collisions detected on
 *                            the interface;
 *    _transmit_compressed    Transmit/compressed number of compressed packets
 *                            transmitted;
 *    _transmit_error_dropped Transmit/drop number of transmit packets dropped;
 *    _transmit_error         Transmit/errs total number of transmit errors;
 *    _transmit_error_fifo    Transmit/fifo number of transmit FIFO buffer
 *                            errors;
 *    _transmit               Transmit/packets transmitted packets;
 */
class Net extends Module {
	/**
   *  A map between procfs keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    rxBytes           : {name:'receive',                unit:'bytes'},
    rxCompressed      : {name:'receive_compressed',     unit:'packets'},
    rxDrop            : {name:'receive_error_dropped',  unit:'packets'},
    rxErrors          : {name:'receive_error',          unit:'count'},
    rxFifo            : {name:'receive_error_fifo',     unit:'count'},
    rxFrame           : {name:'receive_error_frame',    unit:'count'},
    rxMulticast       : {name:'receive_multicast',      unit:'frames'},
    rxPackets         : {name:'receive',                unit:'packets'},

    txBytes           : {name:'transmit',               unit:'bytes'},
    txCarrier         : {name:'transmit_error_carrier', unit:'count'},
    txColls           : {name:'transmit_collision',     unit:'count'},
    txCompressed      : {name:'transmit_compressed',    unit:'packets'},
    txDrop            : {name:'transmit_error_dropped', unit:'packets'},
    txErrors          : {name:'transmit_error',         unit:'count'},
    txFifo            : {name:'transmit_error_fifo',    unit:'count'},
    txPackets         : {name:'transmit',               unit:'packets'},
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const keyMap    = this.constructor.keyMap;
    const netinfo   = this.procfs.netDev();
    const metrics   = [];
    const newState  = {};

    // Collect system-level cpu time measurements
    netinfo.forEach( newIface => {
      const oldIface  = this._state[ newIface.name ] || {};

      // Index this state entry by interface name
      newState[ newIface.name ] = newIface;

      Object.keys( keyMap )
        .forEach( key => {
          if (key === 'name') { return }

          const oldVal  = oldIface[ key ];
          const newVal  = newIface[ key ];

          if (oldVal == newVal) {
            // Skip this metric
            return;
          }

          // Include this metric
          const metric  = this._create_iface_metric( newIface, key );

          metrics.push( metric );
        });
    });

    // Remember the current stats
    this._state = newState;

    return metrics;
  }

  /**
   *  Generate a set of labels using the given label set and the system labels
   *  established upon creation.
   *  @method generateLabels
   *  @param  labels    The generation-time label set {Object};
   *
   *  @note   If `labels.__metric_class` is not provided, create it with the
   *          value of 'network';
   *
   *  @return The set of labels that includes system labels {Object};
   */
  generateLabels( labels ) {
    labels = labels || {};

    if (labels.__metric_class == null) {
      // Add the '__metric_class' meta-label
      labels.__metric_class = 'network';
    }

    return super.generateLabels( labels );
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given network interface data and a metric key, create a new metric
   *  instance.
   *  @method _create_iface_metric
   *  @param  iface   The full interface data {Object};
   *  @param  key     The key from `iface`{String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_iface_metric( iface, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const data  = {
      name  : `os_net_iface_${mapEntry.name}`,
      value : iface[ key ],
      labels: this.generateLabels({
                name: iface.name,
                unit: mapEntry.unit || 'bytes',
              }),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = Net;
