const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based memory data collection module.
 *  @class  Mem
 *
 *  This module generates Memory-based metrics:
 *    os_mem_active               Active Memory that has been used more
 *                                recently and usually not reclaimed unless
 *                                absolutely necessary;
 *    os_mem_anon_huge_pages      AnonHugePages Non-file backed huge pages
 *                                mapped into userspace page tables. Available
 *                                if the kernel is configured with
 *                                CONFIG_TRANSPARENT_HUGEPAGE;
 *    os_mem_anon_pages           AnonPages Non-file backed pages mapped into
 *                                user-space page tables;
 *    os_mem_available            MemAvailable An estimate of how much memory
 *                                is available for starting new applications,
 *                                without swapping;
 *    os_mem_bounce               Bounce Memory used for block device
 *                                "bounce buffers";
 *    os_mem_buffers              Buffers Relatively temporary storage for raw
 *                                disk blocks that shouldn't get tremendously
 *                                large;
 *    os_mem_cached               Cached In-memory cache for files read from
 *                                the disk (the page cache). Doesn't include
 *                                Swap‐Cached;
 *    os_mem_commit_limit         CommitLimit Total amount of memory currently
 *                                available to be allocated on the system;
 *    os_mem_direct_map_1g        DirectMap1G Number of bytes of RAM linearly
 *                                mapped by kernel in 1GB pages;
 *    os_mem_direct_map_2m        DirectMap2M Number of bytes of RAM linearly
 *                                mapped by kernel in 2MB pages;
 *    os_mem_direct_map_4k        DirectMap4k Number of bytes of RAM linearly
 *                                mapped by kernel in 4kB pages;
 *    os_mem_direct_map_4m        DirectMap4M Number of bytes of RAM linearly
 *                                mapped by kernel in 4MB pages;
 *    os_mem_dirty                Dirty Memory which is waiting to get written
 *                                back to the disk;
 *    os_mem_free                 MemFree Amount of free RAM;
 *    os_mem_inactive             Inactive Memory which has been less recently
 *                                used. It is more eligible to be reclaimed for
 *                                other purposes;
 *    os_mem_kern_stack           KernelStack Amount of memory allocated to
 *                                kernel stacks;
 *    os_mem_mapped               Mapped Files which have been mapped into
 *                                memory (with mmap), such as libraries;
 *    os_mem_pageTables           PageTables Amount of memory dedicated to the
 *                                lowest level of page tables;
 *    os_mem_shmem                Shmem Amount of memory consumed in tmpfs
 *                                filesystems;
 *    os_mem_slab                 Slab In-kernel data structures cache;
 *    os_mem_slab_reclaimable     SReclaimable Part of Slab, that might be
 *                                reclaimed, such as caches;
 *    os_mem_slab_unreclaimable   SUnreclaim Part of Slab, that cannot be
 *                                reclaimed on memory pressure;
 *    os_mem_swap_cached          SwapCached Memory that once was swapped out,
 *                                is swapped back in but still also is in the
 *                                swap file. (If memory pressure is high, these
 *                                pages don't need to be swapped out again
 *                                because they are already in the swap file.
 *                                This saves I/O.);
 *    os_mem_swap_free            SwapFree Amount of swap space that is
 *                                currently unused;
 *    os_mem_swap_total           SwapTotal Total amount of swap space
 *                                available;
 *    os_mem_total                MemTotal Total usable RAM;
 *    os_mem_vm_total             VmallocTotal Total size of vmalloc memory
 *                                area;
 *    os_mem_writeback            Writeback Memory which is actively being
 *                                written back to the disk;
 *    os_mem_writeback_tmp        WritebackTmp Memory used by FUSE for
 *                                temporary writeback buffers;
 *
 *    [os_mem_hardwareCorrupted]  HardwareCorrupted Available if the kernel is
 *                                configured with CONFIG_MEMORY_FAILURE;
 *    [os_mem_hugepageSize]       Hugepagesize The size of huge pages.
 *                                Available if the kernel is configured with
 *                                CONFIG_HUGETLB_PAGE;
 *    [os_mem_hugepagesFree]      HugePages_Free The number of huge pages in
 *                                the pool that are not yet allocated.
 *                                Available if the kernel is configured with
 *                                CONFIG_HUGETLB_PAGE;
 *    [os_mem_hugepagesReserved]  HugePages_Rsvd This is the number of huge
 *                                pages for which a commitment to allocate from
 *                                the pool has been made, but no allocation has
 *                                yet been made. These reserved huge pages
 *                                guarantee that an application will be able to
 *                                allocate a huge page from the pool of huge
 *                                pages at fault time. Available if the kernel
 *                                is configured with CONFIG_HUGETLB_PAGE;
 *    [os_mem_hugepagesSurplus]   HugePages_Surp This is the number of huge
 *                                pages in the pool above the value in
 *                                /proc/sys/vm/nr_hugepages. The maximum number
 *                                of surplus huge pages is controlled by
 *                                /proc/sys/vm/nr_overcommit_hugepages.
 *                                Available if the kernel is configured with
 *                                CONFIG_HUGETLB_PAGE;
 *    [os_mem_hugepagesTotal]     HugePages_Total The size of the pool of huge
 *                                pages. Available if the kernel is configured
 *                                with CONFIG_HUGETLB_PAGE;
 *    [os_mem_kernelReclaimable]  Reclaimable Kernel allocations that the
 *                                kernel will attempt to reclaim under memory
 *                                pressure. Includes SReclaimable and other
 *                                direct allocations with a shrinker. Available
 *                                since Linux 4.20;
 *    [os_mem_shmemHugePages]     ShmemHugePages Memory used by shared memory
 *                                and tmpfs allocated with huge pages.
 *                                Available since Linux 4.8 if the kernel is
 *                                configured with CONFIG_TRANSPARENT_HUGEPAGE;
 *    [os_mem_shmemPmdMapped]     ShmemPmdMapped Shared memory mapped into user
 *                                space with huge pages. Available since Linux
 *                                4.8 if the kernel is configured with
 *                                CONFIG_TRANSPARENT_HUGEPAGE;
 */
class Mem extends Module {
	/**
   *  A map between procfs keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    active            : {name:'active',             unit:'bytes'},
    anonPages         : {name:'anon_pages',         unit:'pages'},
    anonHugePages     : {name:'anon_huge_pages',    unit:'pages'},
    available         : {name:'available',          unit:'bytes'},
    bounceBuffers     : {name:'bounce',             unit:'bytes'},
    buffers           : {name:'buffers',            unit:'bytes'},
    cached            : {name:'cached',             unit:'bytes'},
    commitLimit       : {name:'commit_limit',       unit:'bytes'},
    directMap1G       : {name:'direct_map_1g',      unit:'bytes'},
    directMap2M       : {name:'direct_map_2m',      unit:'bytes'},
    directMap4K       : {name:'direct_map_4k',      unit:'bytes'},
    directMap4M       : {name:'direct_map_4m',      unit:'bytes'},
    dirty             : {name:'dirty',              unit:'bytes'},
    free              : {name:'free',               unit:'bytes'},
    inactive          : {name:'inactive',           unit:'bytes'},
    kernelStack       : {name:'kern_stack',         unit:'bytes'},
    mapped            : {name:'mapped',             unit:'bytes'},
    pageTables        : {name:'page_tables',        unit:'bytes'},
    shmem             : {name:'shmem',              unit:'bytes'},
    slab              : {name:'slab',               unit:'pages'},
    slabReclaimable   : {name:'slab_reclaimable',   unit:'slabs'},
    slabUnreclaimable : {name:'slab_unreclaimable', unit:'slabs'},
    swapCached        : {name:'swap_cached',        unit:'bytes'},
    swapFree          : {name:'swap_free',          unit:'bytes'},
    swapTotal         : {name:'swap_total',         unit:'bytes'},
    total             : {name:'total',              unit:'bytes'},
    vmallocTotal      : {name:'vm_total',           unit:'bytes'},
    writeback         : {name:'writeback',          unit:'bytes'},
    writebackTmp      : {name:'writeback_tmp',      unit:'bytes'},

    // Optional metrics depending upon kernel configuratin
    hardwareCorrupted : {name:'hardware_corrupted', unit:'bytes'},
    hugepageSize      : {name:'page_huge_size',     unit:'bytes'},
    hugepagesFree     : {name:'page_huge_free',     unit:'hugepages'},
    hugepagesReserved : {name:'page_huge_reserved', unit:'hugepages'},
    hugepagesTotal    : {name:'page_huge_total',    unit:'hugepages'},
    hugepagesSurplus  : {name:'page_huge_surplus',  unit:'hugepages'},

    kernelReclaimable : {name:'kernel_reclaimable', unit:'bytes'},

    shmemHugePages    : {name:'page_huge_shmem',    unit:'bytes'},
    shmemPmdMapped    : {name:'page_huge_shmem_pmd',unit:'bytes'},
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const keyMap  = this.constructor.keyMap;
    const meminfo = this.procfs.meminfo();
    const metrics = [];

    // Collect system-level cpu time measurements
    Object.keys( keyMap )
      .forEach( key => {
        const oldVal  = this._state[ key ];
        const newVal  = meminfo[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // Include this metric
        const metric  = this._create_mem_metric( meminfo, key );

        metrics.push( metric );
      });

    // Remember the current stats
    this._state = meminfo;

    return metrics;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given memory data and a metric key, create a new metric instance.
   *  @method _create_mem_metric
   *  @param  mem   The full meminfo data {Object};
   *  @param  key   The key from `mem`{String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_mem_metric( mem, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const data  = {
      name  : `os_mem_${mapEntry.name}`,
      value : mem[ key ],
      labels: this.generateLabels({
                unit  : mapEntry.unit || 'bytes',
              }),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = Mem;
