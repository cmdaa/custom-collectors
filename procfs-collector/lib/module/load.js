const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based load-average data collection module.
 *  @class  Load
 *
 *  This module generates Load-based metrics:
 *    os_load_one     1-minute load average;
 *    os_load_five    5-minute load average;
 *    os_load_fifteen 15-minute load average;
 */
class Load extends Module {
	/**
   *  A map between procfs keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    jobsAverage1Minute  : {name:'one',      unit:'percent'},
    jobsAverage5Minutes : {name:'five',     unit:'percent'},
    jobsAverage15Minutes: {name:'fifteen',  unit:'percent'},

    /* Unused
    runnableEntities      : 1,
    existingEntities      : 1450,
    mostRecentlyCreatedPid: 88198
    // */
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const loadavg = this.procfs.loadavg();
    const metrics = [];

    // Collect system-level cpu time measurements

    Object.keys( this.constructor.keyMap )
      .forEach( key => {
        const oldVal  = this._state[ key ];
        const newVal  = loadavg[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // Include this metric
        const metric  = this._create_load_metric( loadavg, key );

        metrics.push( metric );
      });

    // Remember the current stats
    this._state = loadavg;

    return metrics;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given load data and a metric key, create a new metric instance.
   *  @method _create_load_metric
   *  @param  load  The full load data {Object};
   *  @param  key   The key from `load`{String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_load_metric( load, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const data  = {
      name  : `os_load_${mapEntry.name}`,
      value : load[ key ],
      labels: this.generateLabels({
                unit  : mapEntry.unit || 'percent',
              }),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = Load;
