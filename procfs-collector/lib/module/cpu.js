const Module  = require('.');
const Metric  = require('../metric');

/**
 *  Procfs-based CPU data collection module.
 *  @class  Cpu
 *
 *  This module generates CPU-based metrics:
 *    os_cpu_boot       System boot time (milliseconds);
 *    os_cpu_user       CPU time spent in user space (ticks);
 *    os_cpu_sys        CPU time spent in kernel space (ticks);
 *    os_cpu_idle       CPU time spent idle (ticks);
 *    os_cpu_iowait     CPU time spent waiting for IO/disk access (ticks);
 *    os_cpu_irq        CPU time spent servicing hardware interrupts (ticks);
 *    os_cpu_soft_irq   CPU time spent servicing software interrupts (ticks);
 *    os_cpu_steal      CPU time spent in involuntary wait by the virtual CPU
 *                      while the hypervisor was servicing another processor
 *                      (ticks);
 *    os_cpu_nice       CPU time spent on low-priority processes (ticks);
 *    os_cpu_guest      CPU time spent by a virtual CPU in a hypervisor
 *                      (ticks);
 *    os_cpu_guest_nice CPU time spent by a virtual CPU in a hypervisor on
 *                      low-priority processes (ticks);
 */
class Cpu extends Module {
	/**
   *  A map between procfs keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    guestNice: {name:'guest_nice'},
    softirq  : {name:'soft_irq'},
    system   : {name:'sys'},
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const stat    = this.procfs.stat();
    const metrics = [];

    // On initial gather or reset, report boot time
    if (this._state.bootTime == null) {
      const data    = {
        name  : 'os_cpu_boot',
        value : stat.bootTime.getTime(),
        labels: this.generateLabels( {unit: 'milliseconds'} ),
      };

      metrics.push( new Metric( data ) );
    }

    if (this._state.systemCpuTime == null) {
      this._state.systemCpuTime = {};
    }

    // Collect system-level cpu time measurements
    Object.keys( stat.systemCpuTime )
      .sort() // report in key sorted order
      .forEach( key => {
        const oldVal  = this._state.systemCpuTime[ key ];
        const newVal  = stat.systemCpuTime[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // Include this metric
        const metric  = this._create_cputime_metric( newVal, key );

        metrics.push( metric );
      });

    // Remember the current stats
    this._state = {
      bootTime      : stat.bootTime,
      systemCpuTime : stat.systemCpuTime,
    };

    return metrics;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given a cputime value and metric key, create a new metric instance.
   *  @method _create_cputime_metric
   *  @param  val   The cputime value {Number};
   *  @param  key   The key from `uptime` {String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_cputime_metric( val, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    const name      = (mapEntry
                        ? mapEntry.name
                        : key);
    const data      = {
      name  : `os_cpu_${name}`,
      value : val,
      labels: this.generateLabels( {unit: 'ticks'} ),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

module.exports  = Cpu;
