const Module  = require('.');
const Metric  = require('../metric');
const Df      = require('../df');

/**
 *  df-based filesystem/volume data collection module.
 *  @class  Fs
 *
 *  This module generates filesystem-based metrics (`os_fs_volume` prefix) with
 *  a several labels (dev_name, type, mountpoint) identifying the specific
 *  volume:
 *    _files_total    Total files/inodes in the volume;
 *    _files_used     Number of files/inodes used in the volume;
 *    _files_free     Number of files/inodes free in the volume;
 *    _size_total     Total size of the volume (bytes);
 *    _size_used      Used size from the volume (bytes);
 *    _size_avail     Available size in the volume (bytes);
 *
 *  In addition, summary metrics are produced (`os_fs` prefix):
 *    _count          Number of volumes;
 *    _files_total    Total files/inodes used in all volumes;
 *    _files_used     Number of files/inodes used in all volumes;
 *    _files_free     Number of files/inodes free in all volumes;
 *    _size_total     Total size of all volumes (bytes);
 *    _size_used      Used size of all volumes (bytes);
 *    _size_avail     Available size in all volumes (bytes);
 *
 */
class Fs extends Module {
	/**
   *  A map between df keys, base metric name and unit.
   *  @property keyMap
   *  @static
   */
  static  keyMap  = {
    filesTotal  : {name:'files_total', unit:'inodes'},
    filesUsed   : {name:'files_used',  unit:'inodes'},
    filesFree   : {name:'files_free',  unit:'inodes'},

    sizeTotal   : {name:'size_total',  unit:'bytes'},
    sizeUsed    : {name:'size_used',   unit:'bytes'},
    sizeAvail   : {name:'size_avail',  unit:'bytes'},

    /* Labels
     *    devName   : dev_name
     *    type      : type
     *    mountpoint: mountpoint
     */
  };

	/**
   *  A map between summary keys and unit.
   *  @property summaryKeyMap
   *  @static
   */
  static  summaryKeyMap  = {
    count       : 'count',

    files_total : 'inodes',
    files_used  : 'inodes',
    files_free  : 'inodes',

    size_total  : 'bytes',
    size_used   : 'bytes',
    size_avail  : 'bytes',
  };

  /**
   *  Gather data, returning a set of reportable metrics based upon current
   *  differential reporting state.
   *  @method gather
   *
   *  @return The set of reportable metrics {Array of Metric instances};
   *  @async
   */
  async gather() {
    const config    = this.collector.config;
    const exclude   = (config.fs && Array.isArray( config.fs.exclude )
                        ? config.fs.exclude
                        : null);
    const keyMap    = this.constructor.keyMap;
    const sumKeyMap = this.constructor.summaryKeyMap;
    const volumes   = Df( exclude );
    const metrics   = [];
    const summary   = {
      count       : 0,  // Volume count

      files_total : 0,
      files_used  : 0,
      files_free  : 0,

      size_total  : 0,
      size_used   : 0,
      size_avail  : 0,

    };
    const newState  = {
      volumes : {},
      summary : summary,
    };

    // Collect system-level cpu time measurements
    volumes.forEach( newVol => {
      const oldVol  = (this._state.volumes
                        ? this._state.volumes[ newVol.name ] || {}
                        : {});

      // Index this state entry by interface name
      newState.volumes[ newVol.name ] = newVol;

      _update_summary( summary, newVol );

      Object.keys( keyMap )
        .forEach( key => {
          const oldVal  = oldVol[ key ];
          const newVal  = newVol[ key ];

          if (oldVal == newVal) {
            // Skip this metric
            return;
          }

          // Include this metric
          const metric  = this._create_volume_metric( newVol, key );

          metrics.push( metric );
        });
    });

    // Finally, report volume summary information
    Object.keys( summary )
      .forEach( key => {
        const oldVal  = (this._state.summary
                          ? this._state.summary[ key ]
                          : null);
        const newVal  = summary[ key ];

        if (oldVal == newVal) {
          // Skip this metric
          return;
        }

        // :NOTE: These metrics use the 'system' metric-class
        const data  = {
          name  : `os_fs_${key}`,
          value : newVal,
          labels: this.generateLabels( {
            __metric_class: 'system',
            unit          : (sumKeyMap[ key ] || 'count'),
          } ),
        };

        metrics.push( new Metric( data ) );
      });

    // Remember the current stats
    this._state = newState;

    return metrics;
  }

  /**
   *  Generate a set of labels using the given label set and the system labels
   *  established upon creation.
   *  @method generateLabels
   *  @param  labels    The generation-time label set {Object};
   *
   *  @note   If `labels.__metric_class` is not provided, create it with the
   *          value of 'filesystem';
   *
   *  @return The set of labels that includes system labels {Object};
   */
  generateLabels( labels ) {
    labels = labels || {};

    if (labels.__metric_class == null) {
      // Add the '__metric_class' meta-label
      labels.__metric_class = 'filesystem';
    }

    return super.generateLabels( labels );
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Given volume data and a metric key, create a new metric instance.
   *  @method _create_volume_metric
   *  @param  volume  The full volume data {Object};
   *  @param  key     The key from `volume`{String};
   *
   *  @return The new metric instance {Metric};
   *  @protected
   */
  _create_volume_metric( volume, key ) {
    const keyMap    = this.constructor.keyMap;
    const mapEntry  = keyMap[ key ];
    if (mapEntry == null) {
      return null;
    }

    const data  = {
      name  : `os_fs_volume_${mapEntry.name}`,
      value : volume[ key ],
      labels: this.generateLabels({
                dev_name  : volume.devName,
                type      : volume.type,
                mountpoint: volume.mountpoint,
                unit      : mapEntry.unit,
              }),
    };

    return new Metric( data );
  }
  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 */

/**
 *  Given a volume summary and volume entry, update the summary counts.
 *  @method _update_summary
 *  @param  summary   The summary collection object {Object};
 *  @param  volume    The volume entry {Object};
 *
 *  @return void
 *  @private
 */
function _update_summary( summary, volume ) {
  summary.count++;

  summary.files_total += volume.filesTotal;
  summary.files_used  += volume.filesUsed;
  summary.files_free  += volume.filesFree;

  summary.size_total  += volume.sizeTotal;
  summary.size_used   += volume.sizeUsed;
  summary.size_avail  += volume.sizeAvail;
}

/* Private helpers }
 ****************************************************************************/
module.exports  = Fs;
