procfs-collector is a linux nodejs-based application that provides differential
reporting of metrics collected via procfs and sysfs.

It is configured via a YAML-formatted configuration file of the form:
```yaml
beatTime: 5       # The collection beat time in seconds;
syncBeat: 12      # The number of beats at which differential
                  # state will be reset and all metrics reported regardless of
                  # difference;

path    : /proc   # The absolute path to the system mount-point for
                  # procfs;

labels  : #{      # Additional, system-level labels to include in
                  # all metrics.
  node  :  null   # If not provided, initialized in the constructor
                  # from either the environment variable K8S_NODE_NAME or
                  # os.hostname().
#} labels

output  : #{      # Output configuration
  endpoint: stderr      # endpoint: protocol[://host:port[/path]]
                        #   Valid endpoint protocols:
                        #     'stdout'  IGNORES any `host`, `port`, or `path;
                        #     'stderr'  IGNORES any `host`, `port`, or `path;
                        #     'tcp'     REQUIRES a `host` and `port` and
                        #               IGNORES any `path`;
                        #     'kafka'   **NYI**
                        #               REQUIRES a `host`, `port`, and uses
                        #               `path` as the kafka topic

  format  : prometheus  # format  : [ prometheus | json | uni-variate ]
#} output

#############################################################################
# Module-specific configuration
#
fs: #{
  # The set of filesystem types to exclude
  exclude: [ devtmpfs, tmpfs, squashfs, overlay ]
#} fs

cgroups: #{
  sysfs: /sys # The absolute path to the system mount-point for
              # sysfs which SHOULD contain a 'fs/cgroup/' sub-mount;
#} cgroups
```

When started, a full report of all available metrics is generated. Following
this initial report, follow-on measurements are taken based upon a periodic
timer and only measurements that have changed will be reported. We call this
periodic, differential report a `beat` and the interval between beats is
specified in seconds via `beatTime`. In addition to these differential reports,
there is a full report produced every so many beats. This is referred to as the
`sync` beat, configured via `syncBeat`. Using the example settings above
(`beatTime: 5` and `syncBeat: 12`) a sync beat would occur every 60 seconds
(12x 5 second beats).

## Collection Modules

The procfs-collector is logically divided into multiple collection modules, all
driven by the periodic timer:

<table>
 <tr>
  <td>[cpu](#cpu)</td>
  <td>reports basic cpu usage measurements;</td>
 </tr><tr>
 <tr>
  <td>[cgroups](#cgroups)</td>
  <td>
   Uses sysfs (i.e. `/sys/fs/cgroup`) to report the set of active cgroup
   subsystems (`os_cgroup_subsystem`) as well as metrics from each active
   cgroup subsystem.<br />&nbsp;<br />
   <p>
    The currently supported subsytems are:
    [blkio](#cgroups:blkio),</li>
    [cgroup](#cgroups:cgroup),</li>
    [cpu](#cgroups:cpu),</li>
    [cpuacct](#cgroups:cpuacct),</li>
    [cpuset](#cgroups:cpuset),</li>
    [devices](#cgroups:devices),</li>
    [freezer](#cgroups:freezer),</li>
    [hugetlb](#cgroups:hugetlb),</li>
    [io](#cgroups:io),</li>
    [memory](#cgroups:memory),</li>
    [net_cls](#cgroups:net_cls),</li>
    [net_prio](#cgroups:net_prio), and</li>
    [pids](#cgroups:pids)</li>
   </p>
   <p>
    As of this writing, the <code>bcache</code> cgroup subsystem is not
    supported.
   </p>
  </td>
 </tr><tr>
  <td>[fs](#fs)</td>
  <td>
   provides two sub-modules, one which reports per-filesystem metrics, and one
   which provides an overall filesystem usage summary;
  </td>
 </tr><tr>
  <td>[load](#load)</td>
  <td>reports system load information;</td>
 </tr><tr>
  <td>[mem](#mem)</td>
  <td>reports memory usage measurements;</td>
 </tr><tr>
  <td>[net](#net)</td>
  <td>reports per-interface network metrics;</td>
 </tr><tr>
  <td>[proc](#proc)</td>
  <td>
   provides two sub-modules, one which reports per-process metrics, and one
   which provides an overall process summary;
  </td>
 </tr><tr>
  <td>[uptime](#uptime)</td>
  <td>reports system uptime information;</td>
 </tr>
</table>

### Beat-driven reports

For each beat:
- if a sync beat, each module's `reset()` method is invoked to remove any
  previous differential state;
- each module's `gather()` method is invoked to gather any metrics that have
  changed from the previous differential state. Once the gather for a module is
  complete, any returned metrics are reported using the configured
  `output.endpoint` and `output.format`;


#### <a name="cpu">cpu</a>

This module generates CPU-based metrics:

| metric name       | description (unit)
| ----------------- | ------------------------------------------------------
| os_cpu_boot       | System boot time (milliseconds);
| os_cpu_user       | CPU time spent in user space (ticks);
| os_cpu_sys        | CPU time spent in kernel space (ticks);
| os_cpu_idle       | CPU time spent idle (ticks);
| os_cpu_iowait     | CPU time spent waiting for IO/disk access (ticks);
| os_cpu_irq        | CPU time spent servicing hardware interrupts (ticks);
| os_cpu_soft_irq   | CPU time spent servicing software interrupts (ticks);
| os_cpu_steal      | CPU time spent in involuntary wait by the virtual CPU while the hypervisor was servicing another processor (ticks);
| os_cpu_nice       | CPU time spent on low-priority processes (ticks);
| os_cpu_guest      | CPU time spent by a virtual CPU in a hypervisor (ticks);
| os_cpu_guest_nice | CPU time spent by a virtual CPU in a hypervisor on low-priority processes (ticks);

These metrics will include the default `__metric_class` label of 'system'.


#### <a name="cgroups">cgroups</a>

This module generates top-level cgroup subsystem metrics as well as invoking
sub-modules to generate metrics for all available cgroup subsystems.

For each available cgroup subsystem, a 'fact-of' metric is generated:

| metric name         | description
| ------------------- | ----------------------------------------------------
| os_cgroup_subsystem | Identifies a single, enabled cgroup subsystem.


Each `os_cgroup_subsystem` metric will include the following labels to identify
the metric class and cgroup subsystem:

| label name          | description
| ------------------- | ----------------------------------------------------
| __metric_class      | The metric class 'cgroup';
| name                | The name of the cgroup subsystem (e.g. blkio);
| hid                 | The id of the cgroup hierarchy;
| unit                | The metric unit (enabled);

The value of each `os_cgroup_subsystem` metric will be `1` indicating that the
cgroup subsystem is enabled.


##### <a name="cgroups:blkio">cgroups : blkio</a>

This cgroup sub-module generates block io metrics. The majority of these
metrics are per-device, but some are totals.

###### Proportional weight policy files

| metric name                           | description
| ------------------------------------- | ----------------------------------
| os_cgroup_blkio_device_weight         | [blkio.weight](#blkio.weight)
| os_cgroup_blkio_device_weight_device  | [blkio.weight_device](#blkio.weight_device)
| os_cgroup_blkio_device_leaf_weight    | [blkio.leaf_weight](#blkio.leaf_weight)
| os_cgroup_blkio_device                | [blkio.time](#blkio.time)
| os_cgroup_blkio_device                | [blkio.sectors](#blkio.sectors)
| os_cgroup_blkio_device_io_service     | [blkio.io_service_bytes](#blkio.io_service_bytes)
| os_cgroup_blkio_device_io_serviced    | [blkio.io_serviced](#blkio.io_serviced)
| os_cgroup_blkio_device_io_service     | [blkio.io_service_time](#blkio.io_service_time)
| os_cgroup_blkio_device_io_wait        | [blkio.io_wait_time](#blkio.io_wait_time)
| os_cgroup_blkio_device_io_merged      | [blkio.io_merged](#blkio.io_merged)
| os_cgroup_blkio_device_io_queued      | [blkio.io_queued](#blkio.io_queued)
| os_cgroup_blkio_device_group_wait     | [blkio.group_wait_time](#blkio.group_wait_time)
| os_cgroup_blkio_device_empty          | [blkio.empty_time](#blkio.empty_time)
| os_cgroup_blkio_device_idle           | [blkio.idle_time](#blkio.idle_time)
| os_cgroup_blkio_device_dequeue        | [blkio.dequeue](#blkio.dequeue)

All metrics MAY also have a `recursive` version (post-fix).


###### Throttling/Upper limit policy files

| metric name                                 | description
| ------------------------------------------- | ----------------------------
| os_cgroup_blkio_device_throttle_read        | [blkio.throttle.read_bps_device](#blkio.throttle.read_bps_device)
| os_cgroup_blkio_device_throttle_write       | [blkio.throttle.write_bps_device](#blkio.throttle.write_bps_device)
| os_cgroup_blkio_device_throttle_read        | [blkio.throttle.read_iops_device](#blkio.throttle.read_iops_device)
| os_cgroup_blkio_device_throttle_write       | [blkio.throttle.write_iops_device](#blkio.throttle.write_iops_device)
| os_cgroup_blkio_device_throttle_io_serviced | [blkio.throttle.io_serviced](#blkio.throttle.io_serviced)
| os_cgroup_blkio_device_throttle_io_service  | [blkio.throttle.io_service_bytes](#blkio.throttle.io_service_bytes)

Each metric in this list the following labels to identify the metric class and
block device:

| label name          | description
| ------------------- | ----------------------------------------------------
| __metric_class      | The metric class 'cgroup';
| device              | The target block device in the form '%major-device%:%minor-device%;


###### cgroups : blkio : Metric descriptions
Metric descriptions are taken from https://www.kernel.org/doc/Documentation/cgroup-v1/blkio-controller.txt

- <a name="blkio.weight">blkio.weight</a>
  - Specifies per cgroup weight. This is default weight of the group on all the
    devices until and unless overridden by per device rule.
    (See blkio.weight_device).
    Currently allowed range of weights is from 10 to 1000.

- <a name="blkio.weight_device">blkio.weight_device</a>
  - One can specify per cgroup per device rules using this interface.
    These rules override the default value of group weight as specified by
    blkio.weight.

    Following is the format.

    # echo dev_maj:dev_minor weight > blkio.weight_device
    Configure weight=300 on /dev/sdb (8:16) in this cgroup
    # echo 8:16 300 > blkio.weight_device
    # cat blkio.weight_device
    dev     weight
    8:16    300

    Configure weight=500 on /dev/sda (8:0) in this cgroup
    # echo 8:0 500 > blkio.weight_device
    # cat blkio.weight_device
    dev     weight
    8:0     500
    8:16    300

    Remove specific weight for /dev/sda in this cgroup
    # echo 8:0 0 > blkio.weight_device
    # cat blkio.weight_device
    dev     weight
    8:16    300

- <a name="blkio.leaf_weight">blkio.leaf_weight[_device]</a>
  - Equivalents of blkio.weight[_device] for the purpose of
          deciding how much weight tasks in the given cgroup has while
          competing with the cgroup's child cgroups. For details,
          please refer to Documentation/block/cfq-iosched.txt.

- <a name="blkio.time">blkio.time</a>
  - disk time allocated to cgroup per device in milliseconds. First
    two fields specify the major and minor number of the device and
    third field specifies the disk time allocated to group in
    milliseconds.

- <a name="blkio.sectors">blkio.sectors</a>
  - number of sectors transferred to/from disk by the group. First
    two fields specify the major and minor number of the device and
    third field specifies the number of sectors transferred by the
    group to/from the device.

- <a name="blkio.io_service_bytes">blkio.io_service_bytes</a>
  - Number of bytes transferred to/from the disk by the group. These
    are further divided by the type of operation - read or write, sync
    or async. First two fields specify the major and minor number of the
    device, third field specifies the operation type and the fourth field
    specifies the number of bytes.

- <a name="blkio.io_serviced">blkio.io_serviced</a>
  - Number of IOs (bio) issued to the disk by the group. These
    are further divided by the type of operation - read or write, sync
    or async. First two fields specify the major and minor number of the
    device, third field specifies the operation type and the fourth field
    specifies the number of IOs.

- <a name="blkio.io_service_time">blkio.io_service_time</a>
  - Total amount of time between request dispatch and request completion
    for the IOs done by this cgroup. This is in nanoseconds to make it
    meaningful for flash devices too. For devices with queue depth of 1,
    this time represents the actual service time. When queue_depth > 1,
    that is no longer true as requests may be served out of order. This
    may cause the service time for a given IO to include the service time
    of multiple IOs when served out of order which may result in total
    io_service_time > actual time elapsed. This time is further divided by
    the type of operation - read or write, sync or async. First two fields
    specify the major and minor number of the device, third field
    specifies the operation type and the fourth field specifies the
    io_service_time in ns.

- <a name="blkio.io_wait_time">blkio.io_wait_time</a>
  - Total amount of time the IOs for this cgroup spent waiting in the
    scheduler queues for service. This can be greater than the total time
    elapsed since it is cumulative io_wait_time for all IOs. It is not a
    measure of total time the cgroup spent waiting but rather a measure of
    the wait_time for its individual IOs. For devices with queue_depth > 1
    this metric does not include the time spent waiting for service once
    the IO is dispatched to the device but till it actually gets serviced
    (there might be a time lag here due to re-ordering of requests by the
    device). This is in nanoseconds to make it meaningful for flash
    devices too. This time is further divided by the type of operation -
    read or write, sync or async. First two fields specify the major and
    minor number of the device, third field specifies the operation type
    and the fourth field specifies the io_wait_time in ns.

- <a name="blkio.io_merged">blkio.io_merged</a>
  - Total number of bios/requests merged into requests belonging to this
    cgroup. This is further divided by the type of operation - read or
    write, sync or async.

- <a name="blkio.io_queued">blkio.io_queued</a>
  - Total number of requests queued up at any given instant for this
    cgroup. This is further divided by the type of operation - read or
    write, sync or async.

- <a name="blkio.avg_queue_size">blkio.avg_queue_size</a>
  - Debugging aid only enabled if CONFIG_DEBUG_BLK_CGROUP=y.
    The average queue size for this cgroup over the entire time of this
    cgroup's existence. Queue size samples are taken each time one of the
    queues of this cgroup gets a timeslice.

- <a name="blkio.group_wait_time">blkio.group_wait_time</a>
  - Debugging aid only enabled if CONFIG_DEBUG_BLK_CGROUP=y.
    This is the amount of time the cgroup had to wait since it became busy
    (i.e., went from 0 to 1 request queued) to get a timeslice for one of
    its queues. This is different from the io_wait_time which is the
    cumulative total of the amount of time spent by each IO in that cgroup
    waiting in the scheduler queue. This is in nanoseconds. If this is
    read when the cgroup is in a waiting (for timeslice) state, the stat
    will only report the group_wait_time accumulated till the last time it
    got a timeslice and will not include the current delta.

- <a name="blkio.empty_time">blkio.empty_time</a>
  - Debugging aid only enabled if CONFIG_DEBUG_BLK_CGROUP=y.
    This is the amount of time a cgroup spends without any pending
    requests when not being served, i.e., it does not include any time
    spent idling for one of the queues of the cgroup. This is in
    nanoseconds. If this is read when the cgroup is in an empty state,
    the stat will only report the empty_time accumulated till the last
    time it had a pending request and will not include the current delta.

- <a name="blkio.idle_time">blkio.idle_time</a>
  - Debugging aid only enabled if CONFIG_DEBUG_BLK_CGROUP=y.
    This is the amount of time spent by the IO scheduler idling for a
    given cgroup in anticipation of a better request than the existing ones
    from other queues/cgroups. This is in nanoseconds. If this is read
    when the cgroup is in an idling state, the stat will only report the
    idle_time accumulated till the last idle period and will not include
    the current delta.

- <a name="blkio.dequeue">blkio.dequeue</a>
  - Debugging aid only enabled if CONFIG_DEBUG_BLK_CGROUP=y. This
    gives the statistics about how many a times a group was dequeued
    from service tree of the device. First two fields specify the major
    and minor number of the device and third field specifies the number
    of times a group was dequeued from a particular device.

- blkio.*_recursive
  - Recursive version of various stats. These files show the
          same information as their non-recursive counterparts but
          include stats from all the descendant cgroups.

- <a name="blkio.throttle.read_bps_device">blkio.throttle.read_bps_device</a>
  - Specifies upper limit on READ rate from the device. IO rate is
    specified in bytes per second. Rules are per device. Following is
    the format.

  echo "<major>:<minor>  <rate_bytes_per_second>" > /cgrp/blkio.throttle.read_bps_device

- <a name="blkio.throttle.write_bps_device">blkio.throttle.write_bps_device</a>
  - Specifies upper limit on WRITE rate to the device. IO rate is
    specified in bytes per second. Rules are per device. Following is
    the format.

  echo "<major>:<minor>  <rate_bytes_per_second>" > /cgrp/blkio.throttle.write_bps_device

- <a name="blkio.throttle.read_iops_device">blkio.throttle.read_iops_device</a>
  - Specifies upper limit on READ rate from the device. IO rate is
    specified in IO per second. Rules are per device. Following is
    the format.

  echo "<major>:<minor>  <rate_io_per_second>" > /cgrp/blkio.throttle.read_iops_device

- <a name="blkio.throttle.write_iops_device">blkio.throttle.write_iops_device</a>
  - Specifies upper limit on WRITE rate to the device. IO rate is
    specified in io per second. Rules are per device. Following is
    the format.

  echo "<major>:<minor>  <rate_io_per_second>" > /cgrp/blkio.throttle.write_iops_device

Note: If both BW and IOPS rules are specified for a device, then IO is
      subjected to both the constraints.

- <a name="blkio.throttle.io_serviced">blkio.throttle.io_serviced</a>
  - Number of IOs (bio) issued to the disk by the group. These
    are further divided by the type of operation - read or write, sync
    or async. First two fields specify the major and minor number of the
    device, third field specifies the operation type and the fourth field
    specifies the number of IOs.

- <a name="blkio.throttle.io_service_bytes">blkio.throttle.io_service_bytes</a>
  - Number of bytes transferred to/from the disk by the group. These
    are further divided by the type of operation - read or write, sync
    or async. First two fields specify the major and minor number of the
    device, third field specifies the operation type and the fourth field
    specifies the number of bytes.

##### <a name="cgroups:cgroup">cgroups : cgroup</a>
##### <a name="cgroups:cpu">cgroups : cpu</a>
##### <a name="cgroups:cpuacct">cgroups : cpuacct</a>
##### <a name="cgroups:cpuset">cgroups : cpuset</a>
##### <a name="cgroups:devices">cgroups : devices</a>
##### <a name="cgroups:freezer">cgroups : freezer</a>
##### <a name="cgroups:hugetlb">cgroups : hugetlb</a>
##### <a name="cgroups:io">cgroups : io</a>
##### <a name="cgroups:memory">cgroups : memory</a>
##### <a name="cgroups:net_cls">cgroups : net_cls</a>
##### <a name="cgroups:net_prio">cgroups : net_prio</a>
##### <a name="cgroups:pids">cgroups : pids</a>

#### <a name="fs">fs</a>

This module generates filesystem/volume metrics, both overall summary and
device-specific.


#### Overall summary metrics

| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_fs_count               | Number of volumes (count);
| os_fs_files_total         | Total files/inodes for all volumes (inodes);
| os_fs_files_used          | Number of files/inodes used in all volumes (inodes);
| os_fs_files_free          | Number of files/inodes free in all volumes (inodes);
|                           |
| os_fs_size_total          | Total size of all volumes (bytes);
| os_fs_size_used           | Used size across all volumes (bytes);
| os_fs_size_avail          | Available size across all volumes (bytes);


These metrics will include a `__metric_class` label of 'system'.


#### Device-specific metrics

| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_fs_volume_files_total  | Total files/inodes in the volume (inodes);
| os_fs_volume_files_used   | Number of files/inodes used in the volume (inodes);
| os_fs_volume_files_free   | Number of files/inodes free in the volume (inodes);
| os_fs_volume_size_total   | Total size of the volume (bytes);
| os_fs_volume_size_used    | Used size of the volume (bytes);
| os_fs_volume_size_avail   | Available size of the volume (bytes);


Device-specific metrics will include labels to identify the metric class and
target filesystem/volume:

| label name          | description
| ------------------- | ----------------------------------------------------
| __metric_class      | The metric class 'filesystem';
| dev_name            | Filesystem/Volume device name (e.g. '/dev/nvme0n1p3');
| type                | Filesystem/Volume type (e.g. 'ext4');
| mountpoint          | Filesystem/Volume mount point (e.g. '/');


#### <a name="load">load</a>

This module generates system load average metrics:

| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_load_one               | 1-minute load average (percent);
| os_load_five              | 5-minute load average (percent);
| os_load_fifteen           | 15-minute load average (percent);

These metrics will include the default `__metric_class` label of 'system'.


#### <a name="mem">mem</a>

This module generates system memory metrics.

All memory metrics will include the default `__metric_class` label of 'system'.


| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_mem_active             | Active Memory that has been used more recently and usually not reclaimed unless absolutely necessary (bytes);
| os_mem_anon_huge_pages    | AnonHugePages Non-file backed huge pages mapped into userspace page tables. Available if the kernel is configured with CONFIG_TRANSPARENT_HUGEPAGE (pages);
| os_mem_anon_pages         | AnonPages Non-file backed pages mapped into user-space page tables (pages);
| os_mem_available          | MemAvailable An estimate of how much memory is available for starting new applications, without swapping (bytes);
| os_mem_bounce             | Bounce Memory used for block device "bounce buffers" (bytes);
| os_mem_buffers            | Buffers Relatively temporary storage for raw disk blocks that shouldn't get tremendously large (bytes);
| os_mem_cached             | Cached In-memory cache for files read from the disk (the page cache). Doesn't include Swap‐Cached (bytes);
| os_mem_commit_limit       | CommitLimit Total amount of memory currently available to be allocated on the system (bytes);
| os_mem_direct_map_1g      | DirectMap1G Number of bytes of RAM linearly mapped by kernel in 1GB pages (bytes);
| os_mem_direct_map_2m      | DirectMap2M Number of bytes of RAM linearly mapped by kernel in 2MB pages (bytes);
| os_mem_direct_map_4k      | DirectMap4k Number of bytes of RAM linearly mapped by kernel in 4kB pages (bytes);
| os_mem_direct_map_4m      | DirectMap4M Number of bytes of RAM linearly mapped by kernel in 4MB pages (bytes);
| os_mem_dirty              | Dirty Memory which is waiting to get written back to the disk (bytes);
| os_mem_free               | MemFree Amount of free RAM (bytes);
| os_mem_inactive           | Inactive Memory which has been less recently used. It is more eligible to be reclaimed for other purposes (bytes);
| os_mem_kern_stack         | KernelStack Amount of memory allocated to kernel stacks (bytes);
| os_mem_mapped             | Mapped Files which have been mapped into memory (with mmap), such as libraries (bytes);
| os_mem_pageTables         | PageTables Amount of memory dedicated to the lowest level of page tables (bytes);
| os_mem_shmem              | Shmem Amount of memory consumed in tmpfs filesystems (bytes);
| os_mem_slab               | Slab In-kernel data structures cache (pages);
| os_mem_slab_reclaimable   | SReclaimable Part of Slab, that might be reclaimed, such as caches (slabs);
| os_mem_slab_unreclaimable | SUnreclaim Part of Slab, that cannot be reclaimed on memory pressure (slabs);
| os_mem_swap_cached        | SwapCached Memory that once was swapped out, is swapped back in but still also is in the swap file. (If memory pressure is high, these pages don't need to be swapped out again because they are already in the swap file.  This saves I/O.) (bytes);
| os_mem_swap_free          | SwapFree Amount of swap space that is currently unused (bytes);
| os_mem_swap_total         | SwapTotal Total amount of swap space available (bytes);
| os_mem_total              | MemTotal Total usable RAM (bytes);
| os_mem_vm_total           | VmallocTotal Total size of vmalloc memory area (bytes);
| os_mem_writeback          | Writeback Memory which is actively being written back to the disk (bytes);
| os_mem_writeback_tmp      | WritebackTmp Memory used by FUSE for temporary writeback buffers (bytes);

Configuration-dependent memory metrics:
| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_mem_hardwareCorrupted  | HardwareCorrupted Available if the kernel is configured with CONFIG_MEMORY_FAILURE (bytes);
| os_mem_hugepageSize       | Hugepagesize The size of huge pages.  Available if the kernel is configured with CONFIG_HUGETLB_PAGE (bytes);
| os_mem_hugepagesFree      | HugePages_Free The number of huge pages in the pool that are not yet allocated.  Available if the kernel is configured with CONFIG_HUGETLB_PAGE (hugepages);
| os_mem_hugepagesReserved  | HugePages_Rsvd This is the number of huge pages for which a commitment to allocate from the pool has been made, but no allocation has yet been made. These reserved huge pages guarantee that an application will be able to allocate a huge page from the pool of huge pages at fault time. Available if the kernel is configured with CONFIG_HUGETLB_PAGE (hugepages);
| os_mem_hugepagesSurplus   | HugePages_Surp This is the number of huge pages in the pool above the value in /proc/sys/vm/nr_hugepages. The maximum number of surplus huge pages is controlled by /proc/sys/vm/nr_overcommit_hugepages.  Available if the kernel is configured with CONFIG_HUGETLB_PAGE (hugepages);
| os_mem_hugepagesTotal     | HugePages_Total The size of the pool of huge pages. Available if the kernel is configured with CONFIG_HUGETLB_PAGE (hugepages);
| os_mem_kernelReclaimable  | Reclaimable Kernel allocations that the kernel will attempt to reclaim under memory pressure. Includes SReclaimable and other direct allocations with a shrinker. Available since Linux 4.20 (bytes);
| os_mem_shmemHugePages     | ShmemHugePages Memory used by shared memory and tmpfs allocated with huge pages.  Available since Linux 4.8 if the kernel is configured with CONFIG_TRANSPARENT_HUGEPAGE (bytes);
| os_mem_shmemPmdMapped     | ShmemPmdMapped Shared memory mapped into user space with huge pages. Available since Linux 4.8 if the kernel is configured with CONFIG_TRANSPARENT_HUGEPAGE (bytes);

#### <a name="net">net</a>

This module generates metrics for specific network interfaces:

| metric name                           | description (unit)
| ------------------------------------- | -----------------------------------
| os_net_iface_receive                  | Receive/bytes number of received bytes (bytes);
| os_net_iface_receive_compressed       | Receive/compressed number of compressed packets received (packets);
| os_net_iface_receive_error_dropped    | Receive/drop number of received packets dropped (packets);
| os_net_iface_receive_error            | Receive/errs total number of receive errors (count);
| os_net_iface_receive_error_fifo       | Receive/fifo number of receive FIFO buffer errors (count);
| os_net_iface_receive_error_frame      | Receive/frame number of packet framing errors (count);
| os_net_iface_receive_multicast        | Receive/multicast number of multicast frames received (frames);
| os_net_iface_receive                  | Receive/packets number of received packets (packets);
|                                       |
| os_net_iface_transmit                 | Transmit/bytes transmitted bytes (bytes);
| os_net_iface_transmit_error_carrier   | Transmit/carrier number of carrier losses detected on the interface (count);
| os_net_iface_transmit_collision       | Transmit/colls number of collisions detected on the interface (count);
| os_net_iface_transmit_compressed      | Transmit/compressed number of compressed packets transmitted (packets);
| os_net_iface_transmit_error_dropped   | Transmit/drop number of transmit packets dropped (packets);
| os_net_iface_transmit_error           | Transmit/errs total number of transmit errors (count);
| os_net_iface_transmit_error_fifo      | Transmit/fifo number of transmit FIFO buffer errors (count);
| os_net_iface_transmit                 | Transmit/packets transmitted packets (packets);


The metrics metrics will include labels to identify the metric class and target
interface:

| label name          | description
| ------------------- | ----------------------------------------------------
| __metric_class      | The metric class 'network';
| name                | Network interface name (e.g. 'eno2');


#### <a name="proc">proc</a>

This module generates metrics about processes, both overall summary and
process-specific.


#### Overall summary metrics

| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_proc_total             | Total number of processes (count);
| os_proc_sleeping          | Number of sleeping processes (count);
| os_proc_running           | Number of running processes (count);
| os_proc_idle              | Number of idle processes (count);
| os_proc_stopped           | Number of stopped processes (count);
| os_proc_zombie            | Number of zombie processes (count);
| os_proc_dead              | Number of dead processes (count);
| os_proc_unknown           | Number of processes with with unknown status (count);


These metrics will include a `__metric_class` label of 'system'.


#### Process-specific metrics

| metric name                             | description (unit)
| --------------------------------------- | --------------------------------
| os_proc_process_cpu_io                  | Aggregated block IO delays (ticks);
| os_proc_process_child_cpu_guest         | Guest time of the process's children (ticks);
| os_proc_process_child_cpu_sys           | Amount of time this process's waited-for children have been scheduled in kernel mode (ticks);
| os_proc_process_child_mem_major_faults  | Number of major faults that the process's waited-for children have made (count);
| os_proc_process_child_mem_minor_faults  | Number of minor faults that the process's waited-for children have made (count);
| os_proc_process_child_cpu_user          | Amount of time this process's waited-for children have been scheduled in user mode (ticks);
| os_proc_process_guest                   | Guest time of the process, spent running a virtual CPU for a guest operating system (ticks);
| os_proc_process_sys                     | Amount of time this process has been scheduled in kernel mode (ticks);
| os_proc_process_last_cpu                | CPU number this process was last executed on (index);
| os_proc_process_mem_major_faults        | The number of major faults the process has made which have required loading a memory page from disk (count);
| os_proc_process_mem_minor_faults        | The number of minor faults the process has made which have not required loading a memory page from disk (count);
| os_proc_process_mem_rss                 | Resident Set Size, the number of pages the process has in real memory (bytes);
| os_proc_process_mem_rss_soft_limit      | Current soft limit on the RSS of the process (bytes);
| os_proc_process_cpu_start               | The time the process started after system boot (ticks);
| os_proc_process_threads                 | Number of threads in this process (count);
| os_proc_process_cpu_user                | Amount of time this process has been scheduled in user mode, including guest time (ticks);
| os_proc_process_mem_size                | Virtual memory size (bytes);


Process-specific metrics will include labels to identify the metric class and
target process:

| label name              | description
| ----------------------- | ------------------------------------------------
| __metric_class          | The metric class 'process';
| name                    | The process name / comm (e.g. 'systemd');
| cgroups                 | A semicolan-separated list of cgroups to which this process is assigned (e.g. '12:blkio:/init.scope;11:perf_event:/; ....');
| cmdLine                 | The full, space-separated command line of the process (:NOTE: The camelCase label name is used here for backward compatability);
| flags                   | The kernel flags word of the process. For bit meanings, see the PF_* defines in the Linux kernel source file include/linux/sched.h.  Details depend on the kernel version;
| id                      | Id of the target process (PID);
| parent_id               | Id of the parent process (PPID);
| process_group           | Id of the process group;
| process_group_terminal  | Id of the process group terminal;
| sched_policy            | Scheduling policy (see sched_setscheduler(2)) in the form '%policy-number% (%policy-name%)'. Decode using the SCHED_* constants in linux/sched.h;
| sched_priority          | Scheduling priority : For processes running a real-time scheduling policy (`sched_policy`; see sched_setscheduler(2)), this is the negated scheduling priority, minus one; that is, a number in the range -2 to -100, corresponding to real-time priorities 1 to 99. For processes running under a non-real-time scheduling policy, this is the raw nice value (setpriority(2)) as represented in the kernel. The kernel stores nice values as numbers in the range 0 (high) to 39 (low), corresponding to the user-visible nice range of -20 to 19. Before Linux 2.6, this was a scaled value based on the scheduler weighting given to this process;
| sched_priority_realtime | Real-time scheduling priority, a number in the range 1 to 99 for processes scheduled under a real-time policy, or 0, for non-real-time processes (see sched_setscheduler(2));
| session                 | ID of the process session (e.g. '2839');
| state                   | Process state : 'R' Running, 'S' Sleeping in an interruptible wait, 'D' Waiting in uninterruptible disk sleep, 'Z' Zombie, 'T' Stopped (on a signal) or (before Linux 2.6.33) trace stopped, 't' Tracing stop (Linux 2.6.33 onward), 'W' Paging (only before Linux 2.6.0), 'X' Dead (from Linux 2.6.0 onward), 'x' Dead (Linux 2.6.33 to 3.13 only), 'K' Wakekill (Linux 2.6.33 to 3.13 only), 'W' Waking (Linux 2.6.33 to 3.13 only), 'P' Parked (Linux 3.9 to 3.13 only);
| tty                     | The controlling terminal of the process in the form '%major-device%:%minor-device%. :NOTE: This is extracted from the integer tty number where the minor device number is contained in the combination of bits 31 to 20 and 7 to 0; the major device number is in bits 15 to 8;


#### <a name="uptime">uptime</a>

This module generates system uptime metrics:

| metric name               | description (unit)
| ------------------------- | ----------------------------------------------
| os_uptime                 | The uptime of the system, including time spent in suspend (seconds);
| os_uptime_idle            | The amount of time spent in the idle process (seconds);

These metrics will include the default `__metric_class` label of 'system'.

