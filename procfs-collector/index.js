#!/usr/bin/env node
const { ArgumentParser }  = require('argparse');
const Fs                  = require('fs');
const Yaml                = require('js-yaml');
const Collector           = require('./lib/collector');

/**
 *  The main entry point.
 *  @method main
 */
function main() {
  const argParser = new ArgumentParser({
    description: 'proc/sys-fs based metrics collector',
  });

  argParser.add_argument('-c', '--config', {
    help    : 'The YAML configuration file.',
    default : 'etc/procfs.yaml',
    type    : 'str',
  });

  const args  = argParser.parse_args();
  let   config;

  //console.log( 'args:', args );

  try {
    const data  = Fs.readFileSync( args.config, 'utf8' );

    config = Yaml.load( data );

  } catch(ex) {
    console.error('*** Cannot read config file [ %s ]:',
                  args.config, ex);
    return;
  }

  //console.log('config:', config);

  /**************************************************************************
   * Create and start the new collector
   *
   */
  const col = new Collector( config );

  process.on('SIGINT', (sig, code) => col.stop());

  col.start();
}

/***************************************************************************/
main();
