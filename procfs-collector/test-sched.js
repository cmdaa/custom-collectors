#!/usr/bin/env node
const Os  = require('os');
const Fs  = require('fs');
const pid = process.pid;

/**
 *  Given a process id, use procfs to identify the scheduling policy and
 *  priority as compared to the nodejs Os.getPriority() value (which is not
 *  accurate when the scheduling policy is changed).
 *
 *  @method show_sched
 *  @param  pid   The id of the target process {Number};
 *
 *  @return void
 */
function show_sched( pid ) {
  const pairs = Fs.readFileSync( `/proc/${pid}/sched`, {encoding:'utf-8'} )
                .split('\n')
                .reduce( (obj, line) => {
                  //console.log('line  :', line);

                  const tuples  = line.split(/\s+:\s+/);
                  if (tuples.length > 1) {
                    const [key,val] = tuples;

                    obj[key] = val;
                    //obj[key] = parseFloat( val );
                  }
                  return obj;
                }, {});

  console.log('policy       :', pairs.policy);
  console.log('prio         :', pairs.prio);
  console.log('getPriority():', Os.getPriority());
  console.log('------------------------------');
}

show_sched(pid);
setInterval( () => show_sched(pid), 2000 );
