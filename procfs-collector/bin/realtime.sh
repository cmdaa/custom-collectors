#!/bin/bash
#
# Run the given command using the real-time scheduler with the specified
# priority.
#
# Usage: ./realtime.sh priority command ...
# 
function usage() {
  cat <<EOF
***
*** Usage: $(basename "$0") priority command ...
***
***   'priority'  : a value between 0 (low) and 99 (high).
***
***   'command'   : any command and arguments to run with the given priority;
***
EOF
  exit -1
}

function handle_sig() {
  echo ">>> handle_sig, kill $PID ..."
  kill -9 $PID
}

if [ $# -lt 2 ]; then
  usage
fi

PRI="$1"; shift
CMD=$@
if [ $PRI -lt 0 -o $PRI -gt 99 ]; then
  echo "*** Invalid priority [ $PRI ]: should be in the range 0-99"
  usage
fi

# Run the given command in the background
echo ">>> Run command [ ${CMD[@]} ] ..."
${CMD[@]} &
PID=$!

# See if the command is running
ps -q $PID > /dev/null 2>&1
RC=$?
if [ $RC -ne 0 ]; then
  echo "*** Background command is not running"
  exit $RC
fi

# Trap exit to kill the background process
trap "handle_sig" EXIT


echo -n ">>> Initial priority for process[ $PID ]: "
ps -heo policy,pri,nice -q $PID

echo ">>> Change scheduler to RR and priority to $PRI for process[ $PID ] ..."
sudo chrt --rr -p $PRI $PID

echo -n ">>> Updated priority for process[ $PID ]: "
ps -heo policy,pri,nice -q $PID

# Wait for the background process to exit
wait $PID
