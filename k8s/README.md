Samples to provide access to the k8s api via node.js.

:NOTE: For `log-pod` to actually work, the provided patch must be applied
       once the @kubernetes package has been installed (via `npm install`).

       patch node_modules/@kubernetes/client-node/dist/gen/api/coreV1Api.js	\
             coreV1Api.patch
