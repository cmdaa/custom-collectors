const k8s = require('@kubernetes/client-node');

/**
 *  Retrieve a configuration object based upon the incoming arguments.
 *  @method getConfig
 *  @param  [args = null] If provided, yargs generated arguments {Object};
 *
 *  @return A configuration object {Object}
 *          { args      : The original, yargs generated arguments,
 *            k8s       : The kubernetes client instance,
 *            kube      : The full kubectl configuration,
 *            context   : The active kubectl context,
 *            user      : The active kubectl user,
 *            namespace : The active k8s namespace,
 *            api       : A k8s.CoreV1Api instance,
 *          }
 */
function getConfig( args = null ) {
  const config  = {
    args      : args,
    k8s       : k8s,
    kube      : new k8s.KubeConfig(),
    context   : args.context,
    user      : null,
    namespace : args.namespace,
    api       : null,
  };

  config.kube.loadFromDefault();

  if (config.context == null) {
    config.context = config.kube.getCurrentContext();

  } else {
    config.kube.setCurrentContext( config.context );
  }

  if (config.namespace == null) {
    // Identify the target namespace (if any)
    config.namespace = config.kube.contexts.reduce( (ns, obj) => {
      if (obj.name === config.context) { ns = obj.namespace }
      return ns
    }, undefined )
  }

  // Identify the target user (if any)
  config.user = config.kube.getCurrentUser();
  if (config.user && config.user.name) { config.user = config.user.name }

  console.log('Current Context  :', config.kube.getCurrentContext());
  console.log('Current User     :', config.user);
  console.log('Current Namespace:', config.namespace);

  config.api = config.kube.makeApiClient( k8s.CoreV1Api );

  return config
}

module.exports = {
  getConfig : getConfig,
};
