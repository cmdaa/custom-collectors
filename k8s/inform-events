#!/usr/bin/env node
const sprintf = require('sprintf-js').sprintf;

/***************************************************************
 * EventWatcher {
 *
 */
class EventWatcher {
  /**
   *  Create a new instance
   *  @constructor
   *  @param  config              The incoming configuration {Object};
   *  @param  config.args         The original yargs arguments {Object};
   *  @param  config.k8s          A reference to the k8s top-level singleton
   *                              {Module};
   *  @param  config.kube         A k8s configuration instance
   *                              {k8s.KubeConfig};
   *  @param  config.api          A K8s API instance created to access the core
   *                              v1 API {k8s.CoreV1Api};
   *  @param  [config.namespace]  The target context {String};
   *
   */
  constructor( config ) {
    this._config  = config;
    this._cache   = {};

    console.log('API Path: %s', this.apiPath());

    this._informer  = config.k8s.makeInformer( this._config.kube,
                                               this.apiPath(),
                                               this.apiList.bind( this ) );

    this._informer.on( 'add',     this._handleAdd.bind( this ) );
    this._informer.on( 'update',  this._handleUpdate.bind( this ) );
    this._informer.on( 'delete',  this._handleDelete.bind( this ) );
    this._informer.on( 'error',   this._handleError.bind( this ) );
  }

  start() {
    this._informer.start();
  }

  apiPath() {
    return (this._config.namespace
              ? `/api/v1/namespaces/${this._config.namespace}/events`
              : '/api/v1/events');
  }

  apiList() {
    return (this._config.namespace
              ? this._config.api.listNamespacedEvent( this._config.namespace )
              : this._config.api.listEventForAllNamespaces())
  }

  /************************************************************
   * Private helpers {
   *
   */
  _reportAction( action, key, obj, skip = false ) {
    if (skip) {
      console.log( sprintf('%-8s %s: skip', action, key) );

    } else {

      if (this._config.args.verbosity) {
        console.log( '%s:', sprintf('%-8s %s:', action, key), obj );

      } else {
        console.log( sprintf('%-8s %-50s: %-10s [ %s ]',
                             action, key, obj.reason, obj.message) );
      }
    }
  }

  _handleAdd( obj ) {
    const key = this._cacheKey( obj );
    if (key == null)  { return }

    const action  = 'ADDED';
    const co      = this._getCache( obj, key );

    if (! co) {
      this._reportAction( action, key, obj );

      this._putCache( obj );

    } else if (this._config.args.verbosity > 1) {
      this._reportAction( action, key, obj, 'skip' );
    }
  }

  _handleUpdate( obj ) {
    const key = this._cacheKey( obj );
    if (key == null)  { return }

    const action  = 'UPDATED';
    const co      = this._getCache( obj, key );

    if (co !== obj) {
      this._reportAction( action, key, obj );

      this._putCache( obj );

    } else if (this._config.args.verbosity > 1) {
      this._reportAction( action, key, obj, 'skip' );
    }
  }

  _handleDelete( obj ) {
    const key = this._cacheKey( obj );
    if (key == null)  { return }

    const action  = 'DELETED';
    const co      = this._getCache( obj, key );

    if (co) {
      this._reportAction( action, key, obj );

      this._delCache( obj );

    } else if (this._config.args.verbosity > 1) {
      this._reportAction( action, key, obj, 'skip' );
    }
  }

  _handleError( err ) {
    const rsp  = (err.response        ? err.response      : null);
    const res  = (rsp                 ? rsp.body          : err);
    const path = (rsp && rsp.request
                    ? rsp.request.path
                    : this.apiPath());
    console.error('*** Error %s:', path, res);

    if (! this._pendingRestart) {
      this._pendingRestart = setTimeout(() => {
        this._pendingRestart = null;

        // Re-start the watch
        this.start();

      }, this._config.args.timeout);
    }
  }

  /*********************************
   * Cache management {
   *
   */
  _cacheKey( obj ) {
    if (obj == null || obj.kind == null || obj.metadata == null) { return }

    return obj.kind
         + ':'
         + obj.metadata.namespace
         + ':'
         + obj.metadata.name;
   }

  _getCache( obj, key = null ) {
    key = key || this._cacheKey( obj );

    return (key ? this._cache[key] : null);
  }

  _putCache( obj, key = null ) {
    key = key || this._cacheKey( obj );

    if (key != null) {
      this._cache[key] = true;  //obj;
    }
  }

  _delCache( obj, key = null ) {
    key = key || this._cacheKey( obj );

    if (key != null) {
      delete this._cache[ key ];

      //this._cache[key] = false; //null;
    }
  }
  /* Cache management }
   *********************************
   * Private helpers }
   ************************************************************/
}

/* EventWatcher }
 ***************************************************************/
const cliOpts = {
  c: { alias:'context',   describe:'The kubectl context', default:null },
  n: { alias:'namespace', describe:'The k8s namespace',   default:null },
  v: { alias:'verbosity', describe:'Increase verbosity', count:true },
  t: { alias:'timeout',   describe:'Restart timeout',     default:10000 },
  h: { alias:['?','help'] },
};

const args    = require('yargs').options( cliOpts ).help().argv;
const k8s     = require('./config.js').getConfig( args );

const watcher = new EventWatcher( k8s );
watcher.start();
