SaltStack has a `fluentd` module that allows masters/minions to log via fluentd.
- `salt.log.handlers.fluent_mod`
  - https://docs.saltstack.com/en/latest/ref/configuration/logging/handlers/salt.log.handlers.fluent_mod.html

### Fluentd configuration:
```
<source>
  type forward
  bind localhost
  port 24224
</source>
```

Salt (master and/or minion) configuration (Logstash format):
```
fluent_handler:
  host: localhost
  port: 24224
```
