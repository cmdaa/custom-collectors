#!/bin/bash
#
# Run unit tests
#
# To run individual tests:
#   ./tests.sh tests.tests_t10.Test.test_page-3e,00
#              ^^^^^ ^^^^^^^^^ ^^^^ ^^^^^^^^^^^^^^^
#              |     |         |    |
#              |     |         |    method
#              |     |         Class
#              |     file/module
#              dir/module
#
if [ -d env ]; then
  echo ">>> Running tests within virtual environment"
  source env/bin/activate
fi

if [ $# -gt 0 -a "$1" != "-v" ]; then
  args=$@
else
  args=( 'discover' '-s' 'tests' $@ )
fi

python3 -m unittest ${args[@]}
