#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import setuptools
import metadata

DIR     = os.path.dirname( os.path.realpath( __file__ ) )
SG3_EXT = os.path.realpath( DIR +'/../python-sgutils' )

with open( metadata.long_description.file, 'r') as fh: #{
  metadata.long_description.content = fh.read()
#}

with open( DIR +'/requirements.txt', 'r') as fh: #{
  requires = fh.read().splitlines()
#}

#print('requires:', requires)

setuptools.setup(
  name                          = metadata.name,
  version                       = metadata.version,
  author_email                  = metadata.author.email,
  maintainer                    = metadata.maintainer.name,
  maintainer_email              = metadata.maintainer.email,
  description                   = metadata.description,
  long_description              = metadata.long_description.content,
  long_description_content_type = metadata.long_description.type,
  url                           = metadata.url,
  license                       = 'Apache 2.0',

  packages                      = setuptools.find_packages(
                                    include=['sg_logs', 'sg_logs.*']
                                  ),

  include_package_data          = True,
  data_files                    = [
    ('.',['scsi_log_page_db.yml']),
  ],

  classifiers                   = [
    # https://pypi.org/pypi?%3Aaction=list_classifiers
    #
    # Maturity:
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 4 - Beta',

    'License :: OSI Approved :: Apache Software License',

    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.6',

    'Operating System :: POSIX :: Linux',

    'Intended Audience :: System Administrators',

    'Topic :: System :: Hardware',
    'Topic :: System :: Monitoring',
    'Topic :: Utilities',
  ],

  project_urls                  = {
    'Documentation' : 'https://gitlab.com/cmdaa/custom-collectors/blob/master/sg-logs/README.md',
    'Source'        : 'https://gitlab.com/cmdaa/custom-collectors/tree/master/sg-logs',
    'Tracker'       : 'https://gitlab.com/cmdaa/custom-collectors/issues',
  },

  scripts                       = [ 'sg-logs' ],

  install_requires              = requires,

  python_requires               = '>=3.6'
)
