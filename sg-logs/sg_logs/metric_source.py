#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys
import traceback

from sg_logs.metric   import Metric
from sg_logs          import has_method

class MetricSource: #{
  """
    A class representing a metric source from which raw metric data may be
    fetched, parsed, diffed, and metricized.
  """
  def __init__( self, src_drive, title, level = 0, cost = 1.0 ): #{
    """
      Create a new instance.

      Args:
        src_drive (ScsiDrive) : The source drive for accessing the log
                                page/sub-page;
        title (str)           : The title of this source;
        [level = 0] (int)     : Reporting level
                                (0  : always report,
                                 >0 : report only if the
                                      application level report level has been
                                      elevated to at least the same level);
        [cost = 1.0] (float)  : The cost of retrieving data from this source
                                where 0 is free and 1 is very expensive;
    """
    if level is None: level = 0
    if cost  is None: cost  = 1.0

    self.src_drive  = src_drive
    self.level      = level
    self.cost       = cost
    self.title      = title
    self.prior_data = None

    if not hasattr(self, 'name'): #{
      self.name = title
    #}
  #}

  def __str__( self ): #{
    """
      Generate a string representation of this instance.

      Returns:
        (str)
    """
    return self.title
  #}

  def __repr__( self ): #{
    """
      Generate a string representation of this instance.

      Returns:
        (str)
    """
    return ('%s(src_drive=%s, title=%s, level=%s, cost=%s)' %
              (type(self).__name__,
               repr(self.src_drive),
               repr(self.title),
               repr(self.level),
               repr(self.cost)))
  #}

  @property
  def prior_data( self ): #{
    return self._prior_data
  #}

  @prior_data.setter
  def prior_data( self, val ): #{
    self._prior_data = val
  #}

  def reset_diff( self ): #{
    """
      Clear any prior simplified metric data that would be used by metricize()
      to report only diffs.
    """
    self.prior_data = None
  #}

  def fetch( self, verbosity=0, no_extension=False ): #{
    """
      Fetch the raw data from this metric source.

      Args:
        [verbosity = 0] (int)         The debug verbosity;
        [no_extension = False] (bool) If true, do NOT attempt to pass this off
                                      to an extension. This is most likly a
                                      call FROM an extension that is
                                      falling back to the non-extension method;

      Returns:
        (mixed) : The raw data from this metric source.
    """
    source_data = None

    if verbosity: #{
      print('>>> %s.fetch(): src_drive[ %s ], ext[ has:%s / use:%s ]' %
            (type(self).__name__,
             self.src_drive,
             (has_method( self.src_drive.extension, 'fetch_source')
                if self.src_drive and self.src_drive.extension
                else False),
             (not no_extension)),
            file=sys.stderr)
    #}

    #
    # If we're not explicitly requested to ignore extensions, see if the
    # source drive has an extension that includes a fetch_source() method
    #
    if (not no_extension and
        self.src_drive and
        self.src_drive.extension and
        has_method( self.src_drive.extension, 'fetch_source' )): #{
      # Hand off to the drive-specific extension
      try: #{
        source_data = self.src_drive.extension.fetch_source( self, verbosity )

      except Exception as ex: #}{
        print('*** Exception from extension.fetch_source() for "%s":' %
                (self), file=sys.stderr)
        traceback.print_exc( file=sys.stderr )
      #}
    #}

    return source_data
  #}

  def parse( self, fetch_data,
                   verbosity      = 0,
                   report_level   = 0,
                   parse_unknowns = False,
                   no_extension   = False ): #{
    """
      Parse data from `fetch()` into a list of simplified metrics.

      Args:
        fetch_data (mixed)              Data from `fetch()`;
        [report_level = 0] (int)        Active report level;
        [verbosity = 0] (int)           The debug verbosity;
        [parse_unknowns = False] (bool) Should unknowns be parsed
                                        (e.g. parameters);

        [no_extension = False] (bool)   If true, do NOT attempt to pass this
                                        off to an extension. This is most likly
                                        a call FROM an extension that is
                                        falling back to the non-extension
                                        method;

      Returns:
        (list | None) : If the data is parsed, it should be returned as a list
                        of simplified metrics extracted from `fetch_data` where
                        each entry has the form:
                        { name    : (str),
                          value   : (int|float),
                          labels  : {
                            units :  (str),
                            ...
                          },
                          timestamp : (int) # Optional timestamp, milliseconds
                                            # since epoch;
                        }
    """
    parsed = None

    if verbosity: #{
      print('>>> %s.parse(): src_drive[ %s ], ext[ has:%s / use:%s ]' %
            (type(self).__name__,
             self.src_drive,
             (has_method( self.src_drive.extension, 'fetch_source')
                if self.src_drive and self.src_drive.extension
                else False),
             (not no_extension)),
            file=sys.stderr)
    #}


    #
    # If we're not explicitly requested to ignore extensions, see if the
    # source drive has an extension that includes a parse_source() method
    #
    if (not no_extension and
        self.src_drive and
        self.src_drive.extension and
        has_method( self.src_drive.extension, 'parse_source' )): #{
      # Hand off to the drive-specific extension
      try: #{
        parsed = self.src_drive.extension.parse_source( self,
                                                        fetch_data,
                                                        report_level,
                                                        verbosity,
                                                        parse_unknowns )

      except Exception as ex: #}{
        print('*** Exception from extension.parse_source() for "%s":' %
                (self), file=sys.stderr)
        traceback.print_exc( file=sys.stderr )
      #}
    #}

    return parsed
  #}

  def diff( self, data_cur, data_prior ): #{
    """
      Given current and previous simplified metric data from `parse()`,
      generate the set of simplified metrics that differ.

      Args:
        data_cur   (list)   The current  simplified metric data (from parse())
        data_prior (list)   The previous simplified metric data (from parse())

      Returns:
        (dict)  A new dict of the same form returned by fetch() that contains
                JUST the data that differs.
    """
    data_diff = data_cur

    cnt_cur   =  len( data_cur )
    cnt_prior = (len( data_prior ) if isinstance(data_prior, list)
                                   else 0)

    if cnt_cur > 0 and cnt_prior >= cnt_cur: #{
      # :ASSUME: data_cur and data_prior are parallel and filter out any that
      #          evaluate as equal according to Python dict equality
      #          (which includes any contained dict entries -- e.g. 'labels').
      data_diff = []
      for idex in range(0, cnt_cur): #{
        cur   = data_cur[ idex ]
        prior = data_prior[ idex ]

        # assert( isinstance(cur, dict) and isinstance(prior, dict) )
        if cur != prior: #{
          # This entry is different so include it
          data_diff.append( cur )
        #}
      #}
    #}

    return data_diff
  #}

  def metricize( self, labels         = None,
                       parse_unknowns = False,
                       fetch_data     = None,
                       report_level   = 0,
                       verbosity      = 0 ): #{
    """
      Metricize the fetch data for this metric source:
        - If `fetch_data` is not provided, fetch from this source;
        - Parse the `fetch_data` into a simple, normalized metric list;
        - If metricize() was previously called for this page
          (indicated via `self.prior_data`), reduce the normalized metrics to
          only those that differ (`diff()`);
        - Generate full, normalized Metric instances for the remaining
          simplified metrics items;

      Args:
        [labels = None] (list)          If provided, a set of common labels to
                                        include in generated Metric instances;
        [parse_unknowns = False] (bool) Should unknown log pages and/or
                                        unknown parameter be parsed and
                                        included?
        [fetch_data = None] (dict)      Pre-fetched raw metric data (fetch())
        [report_level = 0] (int)        The active report level
        [verbosity = 0] (int)           The debug verbosity

      Returns:
        (list | None)  A list of Metric instances or None if not parsed
    """
    metrics = None

    if fetch_data is None: #{
      # Fetch raw metric data with the option of allowing the parser module to
      # perform the retrieval
      fetch_data = self.fetch( verbosity )
    #}

    if fetch_data is None: #{
      if verbosity: #{
        print('*** %s.metricize(): Cannot fetch data' %
              (type(self).__name__), file=sys.stderr)
      #}
      return metrics
    #}

    # Parse the raw metric data into simplified metric data
    parsed = self.parse( fetch_data, verbosity, report_level, parse_unknowns )
    if parsed is None: #{
      if verbosity: #{
        print('*** %s.metricize(): Cannot parse fetched data' %
              (type(self).__name__), file=sys.stderr)
      #}
      return metrics
    #}

    # Perform a diff of the current simplified metric data with any prior
    metric_diff = self.diff(  parsed, self.prior_data )
    if metric_diff is None: #{
      if verbosity: #{
        print('*** %s.metricize(): Cannot diff simplified metrics' %
              (type(self).__name__), file=sys.stderr)
      #}
      return metrics
    #}

    # Remember the current simplified metric data for any follow-on (diffs)
    self.prior_data = parsed

    # Generate Metric instances for all remaining simplified metrics
    metrics = []
    for metric in metric_diff: #{
      base_name     = (metric.get('name', 'unknown')
                              .lower()
                              .replace(' ', '_'))

      metric_name   = ('device_storage_%s' % (base_name))
      metric_value  = metric.get('value', -9999)
      metric_labels = (labels.copy() if labels is not None
                                     else {})
      metric_ts     = metric['timestamp'] if 'timestamp' in metric else None

      if 'labels' in metric: #{
        metric_labels.update( metric['labels'] )
      #}

      metrics.append( Metric( metric_name,
                              metric_labels,
                              metric_value,
                              metric_ts ) )
    #}

    return metrics
  #}
#}
