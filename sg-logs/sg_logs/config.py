#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import configparser
import re

Re  = {
  'bool'  : re.compile('^(?:(true|yes|on)|(false|no|off))$', re.IGNORECASE),
  'int'   : re.compile('^([0-9]+)$'),
  'float' : re.compile('^((?:[0-9]+)?\.[0-9]+)$'),
  'list'  : re.compile('\[((?:[^,]+)(?:,[^,]+)*)\]', re.MULTILINE),
}

Re_whitespace = re.compile('\s+', re.MULTILINE)


class SgInterpolation( configparser.BasicInterpolation ): #{
  """
    Extend BasicInterpolation to perform minimal interpolation in
    `before_get()` and `_interpolate_some()` iff the raw value is a string.

    Perform primary interpolation to base types in `before_set()`
  """
  def before_get( self, parser, section, option, value, defaults): #{
    """
      Retrieve the value of the given option for the given section with minimal
      conversion iff there are interpolations that can be performed.

      The primary goal is to maintain the raw value if possible.

      Args:
        parser (ConfigParser) The configuration parser;
        section (str)         The target section;
        option (str)          The target option;
        value (str)           The initial, pre-interpolation value;
        defaults (dict)       A defaults map;

      Returns:
        (mixed)   The value
    """
    L = []
    self._interpolate_some( parser, option, L, value, section, defaults, 1 )
    val = (L[0] if len(L) == 1 else ''.join( L ))

    return val
  #}

  def before_set( self, parser, section, option, value ): #{
    """
      Invoked before a value is set for the given option for the given section.

      Allow ANY value with no conversion to string but possible interpolation
      of strings to base types.

      Args:
        parser (ConfigParser) The configuration parser;
        section (str)         The target section;
        option (str)          The target option;
        value (str)           The initial, pre-interpolation value;

      Returns:
        (mixed) value
    """
    if isinstance(value, str): #{
      #print('>>> before_set(): section[ %s ], option[ %s ], value[ %s ]:' %
      #        (section, option, type(value)), value, end=' ')

      for key,exp in Re.items(): #{
        match = exp.match( value )

        if match is None: continue

        if key == 'bool': #{
          # 1 : true match, 2 : false match
          value = (True if match.group(1) is not None
                        else False)

        elif key == 'int': #}{
          value = int( value )

        elif key == 'float': #}{
          value = float( value )

        elif key == 'list': #}{
          value = re.sub( Re_whitespace, '', match.group(1) ).split(',')

        #}
        break
      #}

      #print('=> [ %s ]:' % (type(value)), value)
    #}

    return value
  #}

  def _interpolate_some( self, parser, option,
                          accum, rest, section, map, depth ): #{
    """
      Retrieve the raw value from the given `section` and `option` and if the
      raw value is a string, perform interpolation, otherwise, return the raw
      value AS the interpolation.

      Args:
        parser (ConfigParser) The configuration parser;
        option (str)          Interpolation options;
        accum (list)          An accumulation buffer to fill;
        rest (str)            The fallback value;
        section (str)         The target section;
        option (str)          The target option;
        map
        depth (int)           The current interpolation depth;
    """
    rawval = parser.get( section, option, raw=True, fallback=rest )

    if not isinstance(rawval, str): #{
      #print('>>> _interpolate_some(): section[ %s ], option[ %s ], '
      #                                                'rawval[ %s : %s ]' %
      #      (section, option, type(rawval), rawval))

      accum.append( rawval )

    else: #}{
      # Can this string value be interpolated (via before_set())?
      val = self.before_set( parser, section, option, rawval )

      if not isinstance(val, str): #{
        # YES -- use the interpolation
        accum.append( val )

      else: #}{
        # No, allow our superclass to perform default interpolation
        super()._interpolate_some( parser, option,
                                    accum, rest, section, map, depth)
      #}
    #}
  #}
#}

class SgConfig( configparser.ConfigParser ): #{
  """
    A ConfigParser subclass to use SgInterpolation as the default interpolator.
  """
  _DEFAULT_INTERPOLATION = SgInterpolation()

  def __init__( self, defaults = None ): #{
    """
      Create a new instance.

      Args:
        [defaults = None] (dict)  If provided, the set of defaults to apply
                                  to this configuration;

      If `defaults` is provided, it is expected to follow the form:
        {
          'SectionName': {
            'key':  value,
            ...
          },
          ...
        }

    """
    super( SgConfig, self ).__init__()

    if isinstance( defaults, dict ): #{
      # Initialize defaults, creating any referenced sections
      for section,obj in defaults.items(): #{
        if not isinstance( section, str ):  continue
        if not isinstance( obj,     dict ): continue

        if section != 'DEFAULT':  self.add_section( section )

        for key,val in obj.items(): #{
          #self.set( section, key, str(val) )
          self.set( section, key, val )
        #}
      #}
    #}
  #}

  def _validate_value_types( self, *, section='', option='', value='' ): #{
    """
      Raise a TypeError for non-string `section` or `option` allowing `value`
      to be any type.
    """
    #print('>>> _validate_value_types(): section[ %s ], option[ %s ], '
    #                                      'value[ %s : %s ]' %
    #      (section, option, type(value), value))

    if not isinstance(section, str): #{
        raise TypeError("section names must be strings")
    #}

    if not isinstance(option, str): #{
        raise TypeError("option keys must be strings")
    #}

    #if not self._allow_no_value or value: #{
    #  if not isinstance(value, str): #{
    #    raise TypeError("option values must be strings")
    #  #}
    ##}
  #}
#}
