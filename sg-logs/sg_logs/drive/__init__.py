#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import collections
import re
import time

from sg_logs.smart import Smart

# Timed cache for entries gathered by get_mount_devs()
dev_entries     = {}
dev_entries_ts  = None
dev_entries_ttl = 60  # time-to-live (seconds) for `dev_entries`


class Drive: #{
  """
    A class representing a single target drive.
  """
  def __init__( self, name, info = None, extension = None ): #{
    """
      Create a new instance.

      Args:
        name (str)                      The base/primary name of the target
                                        drive;
        [info] (dict)                   If provided, explicit information about
                                        the target drive;
        [info.sys_path] (str)           The sys-fs path to the target drive;
        [info.dev_type = 'disk'] (str)  The device type, from `ScsiType.str`;
        [info.dev_paths] (list)         If provided, the set of '/dev/*' paths
                                        for this drive [generated];
        [info.mount_points] (dict)      If provided, the set of mount points
                                        for this drive, indexed by partition
                                        [generated];
        [info.vendor] (str)             If provided, the drive vendor [sys-fs];
        [info.model] (str)              If provided, the drive model [sys-fs];
        [info.rev] (str)                If provided, the drive firmware
                                        revision [sys-fs];
        [info.serial] (str)             If provided, the drive serial number
                                        [sys-fs];
        [info.smart] (Smart)            If provided, explicit SMART data;
        [extension] (Module)            If provided, a drive/vendor-specific
                                        extension module;

      Any extension module will be used to allow drive/venndor-specific
      implementations of:
        update_metric_sources( Drive,
                                sources   = [],
                                verbosity = 0)
          => sources[]

        fetch_source( MetricSource,
                        verbosity = 0)
          => fetch_data

        parse_source( MetricSource,
                        fetch_data,
                        report_level    = 0,
                        verbosity       = 0,
                        parse_unknowns  = False)
          => [ {name:, value:, labels:{unit:,...}, [timestamp]}, ... ]
    """
    self.info = {
      'sys_path'    : None,
      'dev_type'    : 'disk',
      'dev_paths'   : None,
      'mount_points': None,
      'vendor'      : None,
      'model'       : None,
      'rev'         : None,
      'serial'      : None,
      'smart'       : None,
    }
    if isinstance(info, dict): #{
      self.info.update( info )
    #}

    self.name = (os.path.basename( self.sys_path )
                  if self.sys_path is not None
                  else name)

    self._disk_type    = ('hdd' if self.dev_type == 'disk' else None)
    self._mount_points = (self.info['mount_points']
                            if 'mount_points' in self.info and
                               isinstance(self.info['mount_points'], dict)
                            else None)
    self._dev_paths    = (self.info['dev_paths']
                            if 'dev_paths' in self.info and
                               isinstance(self.info['dev_paths'], list)
                            else None)
    self._smart        = (self.info['smart']
                            if 'smart' in self.info and
                               isinstance(self.info['smart'], Smart)
                            else None)

    self.set_extension( extension )
  #}

  def __del__( self ): #{
    self.close()
  #}

  @property
  def title( self ): #{
    """
      Returns:
        (str)   The title representation of this instance
    """
    return "%s %-8s" % ( self.name, self.dev_type )
  #}

  @property
  def disk_type( self ): #{
    """
      Returns:
        (str)   A disk type indicator
    """
    return self._disk_type
  #}

  @property
  def sys_path( self ): #{
    return (self.info['sys_path'] if 'sys_path' in self.info
                                  else None)
  #}

  @property
  def dev_type( self ): #{
    return (self.info['dev_type'] if 'dev_type' in self.info
                                  else 'disk')
  #}

  @property
  def dev_paths( self ): #{
    if self._dev_paths is None: #{
      self.gather_dev_paths()

      # Reflect this in `info`
      self.info['dev_paths'] = self._dev_paths
    #}

    return self._dev_paths
  #}

  @property
  def vendor( self ): #{
    if (self.info['vendor'] is None and isinstance(self.sys_path, str)): #{
      vendor = get_contents( self.sys_path +'/vendor' ).strip()

      if len(vendor) < 1 and len(self.model) > 0: #{
        # 'vendor' is empty but 'model' is not so check to see if 'model' is a
        # white-space separated string of 'vendor model'
        model_vendor, model = self.model.split(' ')

        if model: #{
          vendor             = model_vendor.strip()
          self.info['model'] = model.strip()
        #}
      #}

      self.info['vendor'] = vendor
    #}

    return self.info['vendor']
  #}

  @property
  def model( self ): #{
    if (self.info['model'] is None and isinstance(self.sys_path, str)): #{
      self.info['model'] = get_contents( self.sys_path +'/model' ).strip()
    #}

    return self.info['model']
  #}

  @property
  def rev( self ): #{
    if (self.info['rev'] is None and isinstance(self.sys_path, str)): #{
      rev = get_contents( self.sys_path +'/rev' ).strip()

      if len(rev) < 1: #{
        # 'rev' is empty so try an alternate source
        rev = get_contents( self.sys_path +'/firmware_rev' ).strip()
      #}

      self.info['rev'] = rev
    #}

    return self.info['rev']
  #}

  @property
  def serial( self ): #{
    if (self.info['serial'] is None and isinstance(self.sys_path, str)): #{
      self.info['serial'] = get_contents( self.sys_path +'/serial' ).strip()
    #}

    return self.info['serial']
  #}

  @property
  def smart( self ): #{
    """
      Returns:
        (Smart) SMART information about this drive
    """
    smart = self._smart

    if smart is None: #{
      # Perform an initial capabilities determination and, if capable, update
      smart = self.get_smart_capabilities()

      if smart.capable: #{
        # Update, retrieving the full SMART information and status
        smart.update()
      #}

      self.info['smart'] = smart
    #}

    return smart
  #}

  @property
  def mount_points( self ): #{
    """
      If this is a dictionary, it will be keyed by '/dev/*' path with values of
      the form:
        { 'mount':, 'type':, 'rest': }

      Returns:
        (dict | None)   The set of mount points associated with any
                        drive-related device;
    """
    if self._mount_points is None: #{
      self._mount_points = self.get_mount_points()

      self.info['mount_points'] = self._mount_points
    #}

    return self._mount_points
  #}

  def set_extension( self, extension ): #{
    """
      Set an extension for this drive instance

      An extension module will be used to allow drive/venndor-specific
      implementations of:
        update_metric_sources( Drive,
                                sources   = [],
                                verbosity = 0)
          => sources[]

        fetch_source( MetricSource,
                        verbosity = 0)
          => fetch_data

        parse_source( MetricSource,
                        fetch_data,
                        report_level    = 0,
                        verbosity       = 0,
                        parse_unknowns  = False)
          => [ {name:, value:, labels:{unit:,...}, [timestamp]}, ... ]

      Args:
        extension (Module)  A drive/vendor-specific extension module;

      Returnes:
        self
    """
    self.extension = extension

    return self
  #}

  def get_metric_sources( self, verbosity = 0 ): #{
    """
      Retrieve the set of metric sources available for this drive.

      Args:
        [verbosity = 0] (int)   The verbosity level

      Returns:
        (list)  The set of MetricSource instances supported by this drive
    """
    if self._metric_sources is not None: return self._metric_sources

    sources = []

    # If SMART is available, add it as a metric source
    smart = self.get_smart_capabilities()
    if smart is not None and smart.capable: #{
      # Add SMART as a metric source
      if verbosity > 0: #{
        print('=== This drive is SMART capable', file=sys.stderr)
      #}

      sources.append( smart )
    #}

    # If there is an extension module, invoke it to update existing sources and
    # add any extension-specific source
    if self.extension and hasattr( self.extension, 'update_metric_sources' ):#{
      sources = self.extension.update_metric_sources( self, sources, verbosity )
    #}

    self._metric_sources = sources

    return sources
  #}

  def __str__( self ): #{
    """
      Generate a simple string representation of this instance.

      Returns:
        (str)
    """
    #return ('%-10s %s' % (self.name, self.title))

    mount_points = (
      list(
        map( lambda kv: '%s => %s' % (kv[0], kv[1]), self.mount_points.items())
      ) if type(self.mount_points) is dict
        else ['No mountpoints']
    )

    return ('%-10s %s : [ %s : %s : %s : %s ]: %s : %s' %
            (self.name,
             self.title,
             self.vendor,
             self.model,
             self.rev,
             self.serial,
             ','.join( self.dev_paths ),
             ', '.join( mount_points ) ) )
  #}

  def __repr__( self ): #{
    """
      Generate a recoverable representation of this instance.

      Returns:
        (str)
    """
    # Ensure that all on-demand properties are set
    disk_type    = self.disk_type
    dev_paths    = self.dev_paths
    vendor       = self.vendor
    model        = self.model
    rev          = self.rev
    serial       = self.serial
    mount_points = self.mount_points
    smart        = self.smart

    return ('%s( name=%s, info=%s )' %
              (type(self).__name__, repr(self.name), self.info))
  #}

  def capacity( self, unit='bytes' ): #{
    """
      Retrieve the capacity of this drive.

      Args:
        [unit = 'bytes'] (str)  The desired unit (bytes | blocks);

      Returns:
        (int)   The size according to `unit`, -1 if the size cannot be
                determined;
    """
    return -1
  #}

  def gather_dev_paths( self ): #{
    """
      Based upon the sys-fs path of this instance, gather the set of /dev/
      paths with which this drive is directly associated, using the gathered
      set to initialize `_dev_paths`.

      Returns:
        (None)
    """
    if self._dev_paths is None: self._dev_paths = []

    if not isinstance(self.sys_path, str):  return

    blk_path = self.sys_path +'/block'
    src_path = (blk_path if os.path.isdir( self.sys_path +'/block' )
                         else None)

    if src_path: #{
      # Read the first entry in the sub-directory
      import sg_logs.drive.scan # Dev_root, Sys_fs_root, Bus_scsi_devs

      for dev_name in os.listdir( src_path ): #{
        self.add_dev_path( sg_logs.drive.scan.Dev_root +'/'+ dev_name )
        break
      #}
    #}
  #}

  def add_dev_path( self, path ): #{
    """
      Add a new path representing this drive.

      Args:
        path (str)  The new path
    """
    if self._dev_paths is None: self._dev_paths = []

    if path not in self._dev_paths: #{
      self._dev_paths.append( path )
    #}
  #}

  def add_dev_paths( self, paths ): #{
    """
      Add a set of new paths representing this drive.

      Args:
        paths (list)    The new paths

      Returns:
        (None)
    """
    if not isinstance(paths, list): return

    if self._dev_paths is None: self._dev_paths = []

    #_dev_paths.extend( paths )
    for path in paths: #{
      self.add_dev_path( path )
    #}
  #}

  def get_smart_capabilities( self ): #{
    """
      Gather basic SMART capabilities for this drive.

      Returns:
        (Smart) The basic SMART capabilities information
    """
    if self._smart is None: #{
      self._smart = Smart( src_drive = self )
    #}

    return self._smart
  #}

  def update_smart( self ): #{
    """
      Update SMART information for this drive.

      Returns:
        (Smart) The updated smart information
    """
    smart = self.get_smart_capabilities()

    smart.update()

    return smart
  #}

  def get_mount_points( self ): #{
    """
      Based upon the current set of `dev_paths`, determine whether any are used
      as a mount point per `/etc/mtab` (mounts).

      Returns:
        (list | None)  The mount point(s)
    """
    mount_points = {}

    if len( self.dev_paths ) > 0: #{
      # Gather the available dev-related mount points
      entries = get_mount_devs()

      # Iterate over the `dev_paths` for this drive to see if any partition(s)
      # are used for mount points.

      # Find all mount points that match any partition for any `dev_paths`
      # entry for this drive.
      #
      # :NOTE: This ASSUMES partitions are numeric suffixes on a device entry
      #         e.g. /dev/sdh5 is partition 5 on drive device /dev/sdh
      #
      for dev in self.dev_paths: #{
        #mnt_re = re.compile( ('^/dev/(%s[0-9]+)$' % (dev[5:])) )
        mnt_re = re.compile( ('^%s[0-9]+$' % (dev)) )

        for key in entries: #{
          match = mnt_re.match( key )

          if match: #{
            mount_point = entries[key].get( 'mount', None )
            if mount_point is not None: #{
              #partition = match.group( 1 )
              partition = key
              mount_points[ partition ] = mount_point
            #}
          #}
        #}
      #}
    #}

    return (mount_points if len(mount_points) > 1 else None)
  #}

  def close( self ): #{
    """
      If there is an open handle for this drive, close it

      Returns:
        (bool)  Indicating whether this drive had a handle that was closed
    """
    return False
  #}
#}

##########################################################################
# Helpers {
#
def get_contents( path ): #{
  """
    Read and return the contents of the given file/path

    Args:
      path (str)  The target file path (absolute);

    Returns:
      (str) The content of `path`;
  """
  contents = ''

  if os.path.exists( path ): #{
    with open( path, 'r' ) as fh: #{
      contents = fh.read()
    #}
  #}

  return contents
#}

def get_mount_devs(): #{
  """
    Gather the set of lines from '/etc/mtab' that begin with '/dev/'

    Returns:
      (dict)  For each line beginning with '/dev/', add an entry with the
              '/dev/*' path as the key and a value of the form:
                { 'mount':, 'type':, 'rest': }
  """
  global dev_entries
  global dev_entries_ts
  global dev_entries_ttl

  entries = dev_entries

  if dev_entries_ts is not None and \
     time.time() - dev_entries_ts < dev_entries_ttl: #{

    # Return the currently cached entries
    return entries
  #}

  # (Re)set the timestamp and (re)fetch the entries
  dev_entries_ts = time.time()

  with open( '/etc/mtab', 'r' ) as file_in: #{
    for line in file_in: #{
      if not line.startswith( '/dev/' ):  continue

      dev, mnt, typ, rest = line.split( maxsplit=3 )

      if dev in entries: #{
        # Duplicate???
        continue
      #}

      entries[ dev ] = { 'mount': mnt, 'type': typ, 'rest': rest }
    #}
  #}

  return entries
#}

# Helpers }
##########################################################################
