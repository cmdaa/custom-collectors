#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import sys

from sg_logs                import sg3utils
from sg_logs                import has_method
from sg_logs.utils          import get_contents
from sg_logs.drive          import Drive
from sg_logs.scsi.smart     import ScsiSmart
from sg_logs.scsi.err       import ScsiErr
from sg_logs.scsi.addr      import ScsiAddr
from sg_logs.scsi.vendor    import ScsiVendor
from sg_logs.scsi.log_page  import ScsiLogPage

import sg_logs.drive.scan # Dev_root, Sys_fs_root, Bus_scsi_devs

class ScsiDrive( Drive ): #{
  """
    A class representing a single target drive.
  """
  def __init__( self, name, info = None, extension = None, sg3  = None): #{
    """
      Create a new instance.

      Args:
        name (str)                      The base/primary name of the target
                                        drive;
        [info] (dict)                   If provided, explicit information about
                                        the target drive;
        [info.sys_path] (str)           The sys-fs path to the target drive;
        [info.dev_type = 'disk'] (str)  The device type, from `ScsiType.str`;
        [info.dev_paths] (list)         If provided, the set of '/dev/*' paths
                                        for this drive [generated];
        [info.mount_points] (list)      If provided, the set of mount points
                                        for this drive [generated];
        [info.vendor] (str)             If provided, the drive vendor [sys-fs];
        [info.model] (str)              If provided, the drive model [sys-fs];
        [info.rev] (str)                If provided, the drive firmware
                                        revision [sys-fs];
        [info.serial] (str)             If provided, the drive serial number
                                        [sys-fs];
        [info.scsi] (ScsiAddr)          SCSI address;
        [extension] (Module)            If provided, a drive/vendor-specific
                                        extension module;
        [sg3] (int)                     If provided, an existing sg3 handle
                                        from which `info` can be generated;

      Any extension module will be used to allow drive/venndor-specific
      implementations of:
        update_metric_sources( Drive,
                                sources   = [],
                                verbosity = 0)
          => sources[]

        fetch_source( MetricSource,
                        verbosity = 0)
          => fetch_data

        parse_source( MetricSource,
                        fetch_data,
                        report_level    = 0,
                        verbosity       = 0,
                        parse_unknowns  = False)
          => [ {name:, value:, labels:{unit:,...}, [timestamp]}, ... ]
    """
    if sg3 is not None and \
       (info is None or not info.get('sys_path', None)): #{
      if info is None:  info = {}

      # Fill in basic device information using sg3utils
      info.update( get_basic_sg3_info( sg3, name ) )
    #}

    if 'sg_path' not in info:  info['sg_path'] = None

    scsi = (info['scsi'] if 'scsi' in info else None)

    if isinstance(scsi, dict): #{
      # Convert to a ScsiAddr instance ASSUMING it follows the form:
      #   {host:, channel:, target:, lun:}
      info['scsi'] = ScsiAddr( **scsi )
    #}

    super( ScsiDrive, self ).__init__( name, info, extension )

    self.page_map          = None    # Set via Sglogs.scan()
    self._metric_sources   = None    # `self.get_metric_sources()`
    self._log_pages        = None    # `self.get_supported_log_pages()`
    self._sg3              = sg3     # `self.sg3`
    self._sg3_open_failed  = (None if sg3utils and sg3 is None else False)
    self._log_sense_failed = False
    self._inq_augmented    = False   # augment_with_scsi_inq() called?

    self._disk_type = self.info.get('disk_type', None)  # Disk type (hdd | ssd)
    self._vendor    = None # ScsiVendor identified via t10 vendor string
    self._chars     = None # device characteristics
    self._cap       = None # device capacity

  #}

  @property
  def title( self ): #{
    """
      Returns:
        (str)   The title representation of this instance
    """
    return "SCSI{%-3s:%-3s:%-3s:%-3s} %-4s" % ( self.scsi.host,
                                                self.scsi.channel,
                                                self.scsi.target,
                                                self.scsi.lun,
                                                self.disk_type )
  #}

  @property
  def serial( self ): #{
    if self.info['serial'] is None: #{
      self.info['serial'] = self.get_scsi_serial()
    #}

    return self.info['serial']
  #}

  @property
  def scsi( self ): #{
    return (self.info['scsi'] if 'scsi' in self.info else None)
  #}

  @property
  def disk_type( self ): #{
    if self._disk_type is None: #{
      # Attempt to fetch the device characteristics so we can determine the
      # disk type (hdd or ssd)
      if self.characteristics is not None: #{
        self._disk_type = self.characteristics['disk_type']

        # Propagate this to `info`
        self.info['disk_type'] = self._disk_type
      #}
    #}

    return self._disk_type
  #}

  @property
  def sg_path( self ): #{
    if self.info['sg_path'] is None: #{
      # sg_path is determined while gathering dev paths so do that now
      self.gather_dev_paths()
    #}

    return self.info['sg_path']
  #}

  def capacity( self, unit='bytes' ): #{
    """
      Retrieve the capacity of this SCSI drive.

      Args:
        [unit = 'bytes'] (str)  The desired unit (bytes | blocks);

      Returns:
        (int)   The size according to `unit`, -1 if the size cannot be
                determined;
    """
    size = -1

    if self._cap is None and \
       self.sg3  is not None and \
       self.dev_type == 'disk': #{

      # Attempt to read the capacity information
      try: #{
        self._cap = sg3utils.readcap( self.sg3 )

      except Exception as ex: #}{
        #print('*** Scsi.capacity( %s ):' % (self.name), ex, file=sys.stderr)
        pass
      #}
    #}

    if isinstance( self._cap, dict ): #{
      # Compute the desired capacity measure
      # (blocks)
      size = self._cap['logical_blk_addr'] + 1

      if unit == 'bytes': #{
        # (bytes)
        lb_size = self._cap['logical_blk_len']
        size    = size * lb_size
      #}
    #}

    return size
  #}

  def gather_dev_paths( self ): #{
    """
      Based upon the sys-fs path of this instance, gather the set of /dev/
      paths with which this drive is directly associated, using the gathered
      set to initialize `dev_paths`.

    """
    super().gather_dev_paths()

    if not isinstance(self.sys_path, str):  return

    if len(self._dev_paths) < 1: #{
      # Search for specific type indicators
      for name in os.listdir( self.sys_path ): #{
        if name.startswith('scsi_change') or \
           name.startswith('tape') or \
           name.startswith('scsi_tape:st') or \
           name.startswith('onstream_tape:os'): #{

          self.add_dev_path( self.sys_path +'/'+ name )
        #}
      #}
    #}

    ###################################################################
    # Now, determine the generic scsi path and, if located, add it
    # to `_dev_paths`.
    #
    sg_path = None

    if os.path.isdir( self.sys_path +'/scsi_generic' ): #{
      # Read the first entry in the sub-directory
      for sg_name in os.listdir( self.sys_path +'/scsi_generic' ): #{
        sg_path = sg_logs.drive.scan.Dev_root +'/'+ sg_name
        break
      #}

    elif os.path.exists( self.sys_path +'/generic' ): #}{
      # Read the generic soft-link
      name = os.readlink( self.sys_path +'/generic' )

      sg_path = sg_logs.drive.scan.Dev_root +'/'+ os.path.basename( name )
    #}

    if sg_path is not None: #{
      # Include the sg_path in dev_paths
      self.add_dev_path( sg_path )

      # and propagate it into `info`
      self.info['sg_path'] = sg_path
    #}
  #}

  def get_smart_capabilities( self ): #{
    """
      Gather basic SMART capabilities for this drive.

      Returns:
        (ScsiSmart) The basic gathered smart information
    """
    if self._smart is None: #{
      self._smart = ScsiSmart( src_drive = self )
    #}

    return self._smart
  #}

  def close( self ): #{
    """
      If there is an open handle for this drive, close it

      Returns:
        (bool)  Indicating whether this drive had a handle that was closed
    """
    res = False

    try: #{
      if self.sg3 is not None: #{
        sg3utils.close( self.sg3 )
        self._sg3 = None
        res = True
      #}
    except Exception as ex: #}{
      print('ScsiDrive.close(): %s' % (ex))
    #}

    return res
  #}

  ###########################################################################
  # SCSI {
  #
  @property
  def scsi_vendor( self ): #{
    """
      Returns:
        (ScsiVendor | None) The ScsiVendor instance that most closely represents
                            the t10 vendor identification from `self.vendor`

    """
    if self._vendor is None: #{
      self._vendor = ScsiVendor.by_t10( self.vendor )

      if self._vendor is None: #{
        self._vendor = ScsiVendor( 0xff, self.vendor.lower(), self.vendor )
      #}
    #}

    return self._vendor
  #}

  # SCSI methods }
  ###########################################################################
  # sg3utils-based methods {
  #
  @property
  def sg3( self ): #{
    """
      Returns:
        (int | None)    The sg3utils handle or None if unavailable/inaccessible
    """
    if sg3utils and \
       self._sg3 is None and \
       self.sg_path is not None and \
       self._sg3_open_failed is None: #{

      try: #{
        self._sg3 = sg3utils.open( self.sg_path )

      except Exception as ex: #}{
        #print("*** %s: Cannot open '%s' via sg3utils: %s" %
        #        (type(self).__name__, self.sg_path, ex), file=sys.stderr)

        self._sg3_open_failed = True
      #}
    #else: #}{
    #  if sg3utils: #{
    #    if self.sg_path is None: #{
    #      print('*** ScsiDrive.sg3: Missing sg_path', file=sys.stderr)
    #    else: #}{
    #      print('*** ScsiDrive._sg3_open_failed', file=sys.stderr)
    #    #}
    #  else: #}{
    #    print('*** ScsiDrive.sg3: Missing sg3utils', file=sys.stderr)
    #  #}
    #}

    return self._sg3
  #}

  @property
  def is_mr( self ): #{
    """
      Returns:
        (bool)  Is this drive controlled by a MegaRaid controller?
    """
    if self.sg3 is None: return False

    return sg3utils.is_mr( self.sg3 )
  #}

  @property
  def characteristics( self ): #{
    """
      If a dictionary is returned, it will have the form:
        {'rpm'          : (int)
         'product_type' : (str)
         'form_factor'  : (str)
         'disk_type'    : (str) (hdd | ssd)
        }

      Returns:
        (dict | None)   If available, specific device characteristics for this
                        SCSI drive.
    """
    if self.sg3 is None or self._chars is not None: return self._chars

    chars = None
    try: #{
      # Retrieve the vendor, model, and revision
      chars = sg3utils.inquiry( self.sg3, page=0xb1 )

    except Exception as ex: #}{
      #print('*** Scsi.characteristics( %s ):' % (self.name),
      #      ex, file=sys.stderr)
      pass
    #}

    if chars: #{
      #print('=== Scsi.characteristics( %s ):' % (name), chars)

      chars['disk_type'] = ('ssd' if chars['rpm'] == 'non-rotating'
                                  else 'hdd')

      self._chars = chars
    #}

    return self._chars
  #}

  def get_metric_sources( self, verbosity = 0 ): #{
    """
      Retrieve the set of metric sources available for this drive.

      Args:
        [verbosity = 0] (int)   The verbosity level

      Returns:
        (list)  The set of MetricSource instances supported by this drive
    """
    if self._metric_sources is not None: return self._metric_sources

    # Include theh supported log pages first
    sources = []

    sources.extend( self.get_supported_log_pages() )

    # If SMART is available, add it as a metric source
    smart = self.get_smart_capabilities()
    if smart is not None and smart.capable: #{
      # Add SMART as a metric source
      if verbosity > 0: #{
        print('=== This drive is SMART capable', file=sys.stderr)
      #}

      sources.append( smart )
    #}

    # If there is an extension module, invoke it to update existing sources and
    # add any extension-specific source
    if has_method( self.extension, 'update_metric_sources' ):#{
      sources = self.extension.update_metric_sources( self, sources, verbosity )
    #}

    self._metric_sources = sources

    return sources
  #}

  def get_supported_log_pages( self ): #{
    """
      Returns:
        (list)  The set of ScsiLogPage instances for the SCSI LOG SENSE pages
                supported by this drive;
    """
    if self._log_pages is not None: return self._log_pages

    self._log_pages = []
    if self.sg3: #{
      sense = None

      try: #{
        sense = sg3utils.log_sense( self.sg3, subpage=0xff )

      except Exception as ex: #}{
        self._log_sense_failed = ex #True

        #print('*** Fetching supported pages failed: %s' %
        #      (ex), file=sys.stderr)
      #}

      if not sense and self._log_sense_failed: #{
        # Try without subpages
        try: #{
          sense = sg3utils.log_sense( self.sg3 )

        except Exception as ex: #}{
          self._log_sense_failed = ex #True

          #print('*** Fetching supported pages failed: %s' %
          #      (ex), file=sys.stderr)
        #}
      #}

      if sense: #{
        for param in sense['params']: #{
          page    = param.get('page',    -1)
          subpage = param.get('subpage',  0)

          title = None
          level = None
          cost  = None
          if self.page_map: #{
            title = self.page_map.page_title( page, subpage )
            level = self.page_map.page_level( page, subpage )
            cost  = self.page_map.page_cost(  page, subpage )
          #}

          log_page = ScsiLogPage( self, page, subpage,
                                  title = title, level = level, cost = cost )

          self._log_pages.append( log_page )
        #}
      #}

      if self._log_sense_failed and \
         hasattr(self._log_sense_failed, 'errno') and \
         self._log_sense_failed.errno == ScsiErr.ILLEGAL_REQUEST: #{

        self._log_pages = get_simple_log_pages( self )

        if len(self._log_pages) > 0: #{
          self._log_sense_failed = False
        #}
      #}
    #}

    return self._log_pages
  #}

  def augment_with_scsi_inq( self, verbosity=0 ): #{
    """
      For cases when sys-fs may not have access to the proper information,
      augment this instance with information from a SCSI inquiry.

      This MAY update:
        - vendor
        - model
        - rev
        - serial

      Args:
        [verbosity = 0] (int)   The verbosity level

      Returns:
        (ScsiDrive) An updated version of `self`, with updated properties:
                      vendor, model, rev, serial

    """
    if self.sg3 is None:  return self

    # Use `sg_inq` to perform a "standard" inquiry
    #
    # This will return lines of the form:
    #   standard INQUIRY:
    #     PQual=0  Device_type=0  RMB=0  LU_CONG=0  version=0x06  [SPC-4]
    #     [AERC=0]  [TrmTsk=0]  NormACA=0  HiSUP=1  Resp_data_format=2
    #     SCCS=0  ACC=0  TPGS=0  3PC=0  Protect=1  [BQue=0]
    #     EncServ=0  MultiP=1 (VS=0)  [MChngr=0]  [ACKREQQ=0]  Addr16=0
    #     [RelAdr=0]  WBus16=0  Sync=0  [Linked=0]  [TranDis=0]  CmdQue=1
    #     [SPI: Clocking=0x0  QAS=0  IUS=0]
    #       length=144 (0x90)   Peripheral device type: disk
    #    Vendor identification: SEAGATE
    #    Product identification: ST10000NM0206
    #    Product revision level: K005
    #    Unit serial number: ZA29CG000000C908AM18
    #
    inq = None

    try: #{
      # Retrieve the vendor, model, and revision
      inq = sg3utils.inquiry( self.sg3 )

    except Exception as ex: #}{
      print('*** Cannot retrieve SCSI INQ data for %s: %s' %
            (self.name, ex), file=sys.stderr)
    #}

    if inq: #{
      self.info['vendor'] = inq['vendor'].strip()
      self.info['model']  = inq['product'].strip()
      self.info['rev']    = inq['revision'].strip()
      self.info['serial'] = inq['serial'].strip()   # Likely truncated

    #}

    # Retrieve the serial number via an inquiry
    serial = self.get_scsi_serial( force_inq = True )
    if len(serial) > 0: #{
      self.info['serial'] = serial
    #}

    # Note that augment_with_scsi_inq() has gathered whatever information is
    # available
    self._inq_augmented = True

    #print( '%s: ' % (self.sg_path), self)
    return self
  #}

  def get_scsi_serial( self, force_inq = False ): #{
    """
      Attempt to retrieve the SCSI serial number from VPD page 0x80.

      Args:
        [force_inq = False] (bool)  If true, do not rely on sysfs but rather
                                    force an INQ

      Returns:
        (str)   The serial number of the SCSI device;
    """
    serial = ''

    if not force_inq: #{
      # First, try the kernel-provided VPD 80 information
      serial = get_vpd( self.sys_path +'/vpd_pg80' ).strip()
    #}

    if (len(serial) < 1 and self.sg3 is not None): #{
      # NOT available -- try a direct inquiry for VPD 0x80
      inq = None
      try: #{
        # Retrieve the serial number
        inq = sg3utils.inquiry( self.sg3, page=0x80 )

      except Exception as ex: #}{
        print('*** Cannot retrieve SCSI INQ 0x80 data for %s: %s' %
              (self.name, ex), file=sys.stderr)
      #}

      if inq: #{
        serial = inq.strip()
      #}
    #}

    return serial
  #}
  #
  # sg3utils-based methods }
  ###########################################################################

#}

##########################################################################
# Helpers {
#
def get_basic_sg3_info( sg3, name ): #{
  """
    Given a sg3utils handle and device name, attempt to locate information
    about the target device.

    Args:
      sg3 (int)   The handle from sg3utils.open();
      name (str)  The name/path of the target device;

    Returns:
      (dict)  Information about the device
                { sys_path (str)  : The generated sys-fs path;
                  dev_type (str)  : The device type, from `ScsiType.str`;
                  scsi (ScsiAddr) : The SCSI address;
                }
  """
  # Retrieve the SCSI address
  scsi = sg3utils.scsi_id( sg3 )

  info = {
    'sys_path'  : '{root:s}{devs:s}/{host:d}:{channel:d}:{target:d}:{lun:d}'\
                    .format( root = sg_logs.drive.scan.Sys_fs_root,
                             devs = sg_logs.drive.scan.Bus_scsi_devs,
                             **scsi ),
    'dev_type'  : 'disk',
    'scsi'      : ScsiAddr( **scsi ),
  }

  return info
#}

def get_vpd( path ): #{
  """
    Read and return the contents of a raw SCSI VPD page from the given
    file/path

    A raw SCSI VPD page has the form:
        aaaa llll data....

    where:
        `aaaa` is a 2-byte SCSI action  (big-endian)
        `llll` is a 2-byte length       (big-endian)


    Args:
      path (str)  The target file path (absolute);

    Returns:
      (str) The content of `path`;
  """
  contents = ''

  if os.path.exists( path ): #{
    with open( path, 'r' ) as fh: #{
      try: #{
        # Seek beyond the action and length (4-bytes)
        fh.seek(4, 0)

        # Read the remainder as a NULL-terminated string
        contents = fh.read()
      except Exception as ex: #}{
        pass
        #print("*** Cannot read from '%s':" % (path), ex, file=sys.stderr)
      #}
    #}
  #}


  return contents
#}

def get_simple_log_pages( self ): #{
  """
    When a LOG SENSE 0x00,ff fails, we need to fall back to a simple request.

    If a SCSI drive doesn't support LOG SENSE 0x00,ff if most likely won't
    support subpage ff for ANY LOG PAGE so just retrieve the simple, top-level
    set of supported pages.

    Args:
      self (ScsiDrive)  The controlling instance;

    Returns:
      (list)  The set of ScsiLogPage instances for the SCSI LOG SENSE pages
              supported by this drive
  """

  # assert( self._log_pages is None )
  # assert( self.sg3 is not None )

  #print('*** Fall-back to the simple LOG SENSE[ 0x00,0 ]')

  log_pages = []
  sense     = None

  try: #{
    sense = sg3utils.log_sense( self.sg3 )

  except Exception as ex: #}{
    return log_pages

    #print('*** Fetching supported pages failed: %s' %
    #      (ex), file=sys.stderr)
  #}

  # assert( isinstance( sense, list ) )
  for page_param in sense['params']: #{
    sub_sense = None

    #####################################################
    # If a SCSI drive doesn't support LOG SENSE 0x00,ff
    # if most likely won't support subpage ff for ANY
    # LOG PAGE.
    #
    # If we WANT to try, uncomment the following {
    #page = page_param['page']
    #
    #if page != 0: #{
    #  try: #{
    #    sub_sense = sg3utils.log_sense( self.sg3, page=page, subpage=0xff )
    #
    #  except Exception as ex: #}{
    #    print('*** Fetching supported pages [0x%02x,ff] failed: %s' %
    #          (page, ex), file=sys.stderr)
    #    pass
    #  #}
    ##}

    if sub_sense is None: #{
      # Fallback to JUST the top-level page (subpage 0)
      page_param.update({'subpage':0})
      sub_sense = {'params': [page_param] }
    #}

    # assert( isinstance( sub_sense, list ) )
    for param in sub_sense['params']: #{
      title = None
      cost  = None
      if self.page_map: #{
        title = self.page_map.page_title( param['page'], param['subpage'] )
        cost  = self.page_map.page_cost(  param['page'], param['subpage'] )
      #}

      log_page = ScsiLogPage( self, param['page'], param['subpage'],
                              title = title, cost = cost )

      log_pages.append( log_page )
    #}
  #}

  return log_pages
#}

# Helpers }
##########################################################################
