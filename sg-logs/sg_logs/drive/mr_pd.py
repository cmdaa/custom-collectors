##
#
# An explicit physical SCSI disk behind a MegaRaid controller, accessible via
# sg3utils
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import collections

from sg_logs            import sg3utils
from sg_logs.scsi.type  import ScsiType
from sg_logs.drive.scsi import ScsiDrive

class MrPDrive( ScsiDrive ): #{
  """
    A class representing a single physical SCSI drive behind a MegaRaid
    controller.
  """
  def __init__( self, name, info, extension = None, sg3  = None): #{
    """
      Create a new instance.

      Args:
        name (str)                      The base/primary name of the target
                                        drive;
        info (dict)                     Information about the target drive;
        info.pd (dict)                  The top-level physical disk
                                        information;
                                          {device_id:, encl_device_id:,
                                           encl_index:, slot_number:,
                                           scsi_dev_type:}
        [info.sys_path] (str)           The sys-fs path to the target drive;
        [info.dev_type = 'disk'] (str)  The device type, from `ScsiType.str`;
        [info.dev_paths] (list)         If provided, the set of '/dev/*' paths
                                        for this drive [generated];
        [info.mount_points] (list)      If provided, the set of mount points
                                        for this drive [generated];
        [info.vendor] (str)             If provided, the drive vendor [sys-fs];
        [info.model] (str)              If provided, the drive model [sys-fs];
        [info.rev] (str)                If provided, the drive firmware
                                        revision [sys-fs];
        [info.serial] (str)             If provided, the drive serial number
                                        [sys-fs];
        [info.scsi] (ScsiAddr)          SCSI address;
        [extension] (Module)            If provided, a drive/vendor-specific
                                        extension module;
        [sg3] (int)                     If provided, an existing sg3 handle
                                        from which `info` can be generated;
    """
    if (not isinstance(info, dict) or
        not isinstance(info.get('pd',None), dict)): #{

      raise Exception('info must be a dict with a pd member')
    #}

    pd = info['pd']
    if not isinstance( pd.get('scsi_dev_type', None), int ): #{
      raise Exception('info.pd.scsi_dev_type must be an int')
    #}
    if not isinstance( pd.get('device_id', None), int ): #{
      raise Exception('info.pd.device_id must be an int')
    #}

    info['dev_type'] = ScsiType.str[ pd['scsi_dev_type'] ]

    super( MrPDrive, self ).__init__( name, info      = info,
                                            extension = extension,
                                            sg3       = sg3 )

    # assert( sg3utils is not None )
    # assert( self.sg3 is not None )  # Possibly re-created from `self.sg_path`
    # assert( sg3utils.is_mr( self.sg3 ) is True )

    # Ensure the target has been set to the requested physical target
    cid = sg3utils.mr_get_target( self.sg3 )
    did = pd.get('device_id', None)
    if (did is not None and did >= 0): #{
      # There is a targeted physical device
      #
      # Ensure there is a targeted version of the device path
      targeted_path = ('%s:MR:%d' % (self.sg_path, did))
      self.add_dev_path( targeted_path )

      if did != cid: #{
        # The targeted physical device is NOT the one that is being requested.
        #
        # We need to re-direct this RAID-controlled disk to the proper physical
        # device. To ensure we don't inadvertantly change the target of another
        # instance, create our own sg3 handle.
        self._sg3 = sg3utils.open( self.sg_path )

        # Explicitly set the target disk
        sg3utils.mr_set_target( self.sg3, did )
      #}
    #}
  #}

  @property
  def pd( self ): #{
    return (self.info['pd'] if 'pd' in self.info else None)
  #}

  #############################################################################
  # Since MegaRaid only exposes logical drives to the Operating System:
  #   - the SCSI information retrieved by our super-class represents the
  #     logical drive and not this specific physical drive;
  #   - augment SCSI information using an explicit INQ to the physical drive;
  #
  # Agumented property over-rides {
  @property
  def vendor( self ): #{
    if not self._inq_augmented: self.augment_with_scsi_inq()

    return  self.info['vendor']
  #}

  @property
  def model( self ): #{
    if not self._inq_augmented: self.augment_with_scsi_inq()

    return  self.info['model']
  #}

  @property
  def rev( self ): #{
    if not self._inq_augmented: self.augment_with_scsi_inq()

    return  self.info['rev']
  #}

  @property
  def serial( self ): #{
    if not self._inq_augmented: self.augment_with_scsi_inq()

    return  self.info['serial']
  #}
  # Agumented property over-rides }
  #############################################################################

  @property
  def title( self ): #{
    """
      Generate a title representation of this instance

      Returns:
        (str)
    """
    return "MegaRaid{%-3s:%-3s:%-3s} %-4s" % ( self.pd.get('encl_device_id',-1),
                                               self.pd.get('slot_number',-1),
                                               self.pd.get('device_id',-1),
                                               self.disk_type )
  #}
#}
