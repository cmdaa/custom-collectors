#!/usr/bin/env python
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import re

from sg_logs              import sg3utils
from sg_logs.utils        import get_contents

from sg_logs.scsi.addr    import ScsiAddr
from sg_logs.scsi.type    import ScsiType
from sg_logs.drive.scsi   import ScsiDrive
from sg_logs.drive.mr_pd  import MrPDrive

# System paths
Sys_fs_root   = '/sys'
Bus_scsi_devs = '/bus/scsi/devices'
Class_nvme    = '/class/nvme'
Dev_root      = '/dev'

############################################################################
# SCSI device support {
#

# RegExp to break out
#   SCSI host:channel:target:lun
#
Scsi_id_re = re.compile( r'^([0-9]+):([0-9]+):([0-9]+):([0-9]+)$' )

def get_scsi_devices( verbosity = 0 ): #{
  """
    Retrieve information about any SCSI devices on this system.

    Args:
      verbosity (int)   The verbosity level

    Returns:
      (list)  A list of ScsiDrive instances;
  """
  targets     = []
  target_path = Sys_fs_root + Bus_scsi_devs

  if not os.path.isdir( target_path ): #{
    return targets
  #}

  # We're only interested in SCSI target names of the form:
  #   host :chan :tgt  :lun
  #   [0-9]:[0-9]:[0-9]:[0-9]
  #
  # lsscsi ignores entries:
  #   - containing    : 'mt', 'ot', 'gen' (kernel < 2.6)
  #   - beginning     : 'host', 'target'
  #   - NOT containing: ':'
  #
  #   reExclude = re.compile( r'(mt|ot|gen|^host|^target)' )
  #
  mr_controllers = {}

  for scsi_id in os.listdir( target_path ): #{
    match = Scsi_id_re.match( scsi_id )
    if match: #{
      [ scsi_host, scsi_channel, scsi_target, scsi_lun ] = match.groups()

      sys_path = os.path.abspath( target_path +'/'+
                                   os.readlink( target_path +'/'+ scsi_id ) )
      type     = int( get_contents( sys_path +'/type' ), 10 )

      drive_info = {
        'sys_path': sys_path,
        'dev_type': ScsiType.str[ type ],
        'scsi'    : ScsiAddr( scsi_host, scsi_channel, scsi_target, scsi_lun ),
      }

      target = ScsiDrive( scsi_id, info=drive_info )

      if verbosity > 0: #{
        print('=== get_scsi_devices(): %s : sg_path[ %s ], is_mr[ %s ]' %
                (target.title, target.sg_path, str(target.is_mr)))
      #}

      if target.is_mr: #{
        #
        # The target is controlled by a MegaRaid controller and so MAY
        # represent a logical disk comprised of one or more physical disks...
        #
        pd_list = mr_controllers.get( scsi_host, None )

        if pd_list is None: #{
          # This is a new MegaRaid controller
          pd_list = sg3utils.mr_get_pd_list( target.sg3 )
          mr_controllers[ scsi_host ] = pd_list

          if verbosity > 0: #{
            print('=== get_scsi_devices(): %s : new MegaRaid controller[ %s ] '
                                                  'with %d physical disks' %
                  (target.title, scsi_host, len(pd_list)))
            print('===    sys_path: %s' % (drive_info['sys_path']))
            print('===    dev_type: %s' % (drive_info['dev_type']))
            print('===    scsi    :', drive_info['scsi'])
          #}

          # Create a pseudo-instance for each entry in the pd_list to cover the
          # case where there is a single logical disk backed by multiple
          # physical disks which have no separate device entries.
          for idex, pd in enumerate( pd_list ): #{
            if verbosity > 0: #{
              print('    %3d:' % (idex), pd)
            #}

            # Skip non-disk devices
            if pd['scsi_dev_type'] != ScsiType.DISK:  continue

            mr_info = {
              'pd'  : pd,

              # Pass along the sg_path of the current ScsiDrive instance.
              # This will provide access to the MegaRaid controller and will
              # allow MrPDrive to re-open sg3 if it needs to be re-targeted to
              # a different physical drive.
              'sg_path' : target.sg_path,
            }

            pd_target = MrPDrive( scsi_id, info = mr_info )

            if verbosity > 0: #{
              print('       :', pd_target)
            #}

            targets.append( pd_target )
          #}
        #}

        # Retrieve the current physical disk target (if set)
        did = sg3utils.mr_get_target( target.sg3 )

        if verbosity > 0: #{
          print('=== get_scsi_devices(): %s : did[ %s ]' %
                (target.title, str(did)))
        #}

        if (did is not None and did >= 0): #{
          # This target is a specific physical disk.
          #
          # Locate the target pd entry and create a single disk instance.
          pd = next((x for x in pd_list if x['device_id'] == did), None)

          if pd is not None: #{
            # assert( pd['scsi_dev_type'] == ScsiType.DISK )

            mr_info = drive_info.copy()
            mr_info['pd'] = pd

            pd_target = MrPDrive( scsi_id, info = mr_info )

            targets.append( pd_target )
          #}

        else: #}{
          # This target will not be used so explicitly close it
          target.close()
        #}

      else: #}{
        targets.append( target )
      #}

    #}

    #if reExclude.search( scsi_id ) or ':' not in scsi_id: continue
    #if reExclude.search( scsi_id ): continue

    #print( 'scsi_id[ %s ]' % (scsi_id) )
  #}

  return targets
#}

# SCSI device support }
############################################################################
if __name__ == '__main__': #{
  devs = get_scsi_devices()

  for dev in devs: #{
    print( dev )
    #print( '[%s] => %s' % (dev.get('scsi'), dev.get('info')) )
  #}
#}
