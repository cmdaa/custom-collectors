#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import decimal

class Metric: #{
  """
    A class representing a single collected metric with name, labels, and
    value.
  """
  def __init__( self, name, labels, value, timestamp = None ): #{
    """
      Create a new instance.

      Args:
        name (str)                The name of this metric;
        labels (dict)             The set of labels that apply to this metric;
        value (str)               The metric value;
        [timestamp = None] (int)  The timestamp for this metric;
    """
    # Ensure we have a numeric value
    if isinstance( value, str ): #{
      try: #{
        value = decimal.Decimal( value )
      except: #}{
        value = bytearray( value, 'ASCII' )
      #}
    #}

    if not isinstance( value, int ) and \
       not isinstance( value, float ) and \
       not isinstance( value, decimal.Decimal ): #{

      # Create a string representation of the raw value that Prometheus will
      # not choke on
      labels.update({'value_raw': prom_str( value )})
      value = -1
    #}

    self.name      = name
    self.labels    = labels
    self.value     = value
    self.timestamp = timestamp

  #}

  @property
  def prometheus_format( self ): #{
    """
      The Prometheus Exposition Format version of this metric

      Returns:
        (str) : The Prometheus Exposition Format version of this metric;
    """
    # Create a version of labels sorted by key (tuple[0] from items())
    labels = ','.join(
      '{k}="{v}"'.format( k=key, v=prom_str(val) ) \
          for key,val in sorted( self.labels.items(), key=lambda x:x[0] )
    )

    str = '{name}{{{labels}}} {value}'.format( name   = self.name,
                                               labels = labels,
                                               value  = self.value )
    if self.timestamp is not None: #{
      str += ' {ts}'.format( ts = self.timestamp )
    #}

    return str
  #}

  def toJSON( self, **kwargs ): #{
    """
      Return a JSON representation of this metric

      Args:
        [any args acceptable to json.dumps()]

      Returns:
        (str) : The JSON representation of this metric;
    """
    json = {
      'name'  : self.name,
      'labels': {},
      'value' : self.value,
    }

    for key,val in sorted( self.labels.items(), key=lambda x:x[0] ): #{
      if isinstance( val, bytearray ): #}{
        val_str = ('bytearray([%s])' % (','.join('%d' % x for x in val)))
      else: #}{
        val_str = str( val )
      #}
      json['labels'][ key ] = val_str
    #}

    if self.timestamp is not None: #{
      json['timestamp'] = self.timestamp
    #}

    return json_encode( json, **kwargs )
  #}

  def __str__( self ): #{
    """
      Generate a simple string representation of this instance.

      Returns:
        (str)
    """
    return self.prometheus_format
  #}

  def __repr__( self ): #{
    """
      Generate a string representation of this instance

      Returns:
        (str)
    """
    return ('%s( name=%s, labels=%s, value=%s, timestamp=%s )' %
              (type(self).__name__,
               repr(self.name),
               repr(self.labels),
               repr(self.value),
               repr(self.timestamp)) )
  #}

  def print_meta( self ): #{
    """
      Print the Prometheus Exposition Format meta-data for this metric
        HELP and TYPE
    """
    print('# HELP {metric.name} sg_log metric {metric.name}'\
            .format( metric = self ) )

    print('# TYPE {metric.name} gauge'.format( metric = self ) )
  #}

  def print_metric( self ): #{
    """
      Print the Prometheus Exposition Format version of this metric
    """
    print( self.prometheus_format )
  #}
#}

def prom_str( value ): #{
  """
    Create a string representation of `value` that will not choke Prometheus.

    :NOTE: This currently only perform special handling of a bytearray value.

    Args:
      value (mixed)   The raw value;

    Returns:
      (str)
  """

  if isinstance( value, bytearray ): #{
    val_str = ('bytearray([%s])' % (','.join('%d' % x for x in value)))

  else :#}{
    val_str = str( value )
  #}

  return val_str
#}

def json_encode( value, **kwargs ): #{
  """
    Encapsulate JSON encoding

    Args:
      value (mixed)   The raw value;
      [any args acceptable to json.dumps()]

    Returns:
      (str)           A JSON-encode string
  """
  import json

  return json.dumps( value, **kwargs )
#}
