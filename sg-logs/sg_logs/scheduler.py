#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import sys
import signal
import random
import time
import pytimeparse
import threading

from datetime  import datetime

#from apscheduler.schedulers.background  import BackgroundScheduler
from apscheduler.schedulers.blocking    import BlockingScheduler
from apscheduler.jobstores.memory       import MemoryJobStore
from apscheduler.executors.pool         import ThreadPoolExecutor
from apscheduler.util                   import datetime_repr
from apscheduler.events                 import (
    EVENT_JOB_EXECUTED, EVENT_JOB_ERROR, EVENT_JOB_MISSED )

from sg_logs.scsi.log_page              import ScsiLogPage



class Scheduler: #{
  """
    Disk metrics retrieval scheduler.
  """

  # Static DEFAULT settings
  DEFAULTS  = {
    'app'             : None,
    'period'          : '5m',
    'thread_pool'     : 10,
    'include_scsi'    : True,
    'sync_beat'       : 6,    # For the default period, every 30 minutes
    'unique_drives'   : None, # scan
    'reconnect_delay' : '5s', # Initial delay between attempts to reconnect
                              # multiplied by 2 for each failure up a maximum
                              # of 10x

    'one_shot'        : False,# Perform a single schedule/period and terminate
    'verbosity'       : 0,
  }

  def __init__( self, config ): #{
    """
      Create a new scheduler instance

      Args:
        config (dict)                         Configuration data;
        config.app                            The primary Sglogs instance
                                              providing access to available,
                                              unique disks and their metrics
                                              log pages;
        [config.period = '5m'] (str)          The period during which all logs
                                              from all available, unique disks
                                              should be collected once;
        [config.sync_beat = 6] (int)          The number of periods at which
                                              all log page data should be
                                              reported regardless of whether it
                                              has changed;
        [config.thread_pool   = 10] (int)     The size of the thread pool;
        [config.include_scsi = True] (bool)   Include SCSI drives in the scan?
        [config.unique_drives = None] (dict)  If provided, the set of drives to
                                              use for scheduling
                                              (keyed by unique id), otherwise a
                                              scan will be performed to
                                              identify the set of unique
                                              drives;
        [config.reconnect_delay = '5s'] (str) Initial delay between attempts to
                                              reconnect multiplied by 2 for
                                              each failure up a maximum of 10x;
        [config.one_shot = False] (bool)      Perform a single schedule/period
                                              and terminate;
        [config.verbosity = 0] (bool | int)   Debug verbosity;

    """
    self.config = Scheduler.DEFAULTS.copy()
    self.config.update( config )

    period = self.config.get( 'period', None )
    if not isinstance( period, int ) and not isinstance( period, str ): #{
      period = Scheduler.DEFAULTS['period']
    #}

    reconnect_delay = self.config.get( 'reconnect_delay', None )
    if not isinstance( reconnect_delay, int ) and \
       not isinstance( reconnect_delay, str ): #{

      reconnect_delay = Scheduler.DEFAULTS['reconnect_delay']
    #}

    # Cache a few configuration settings
    if isinstance( period, int ): #{
      # Period (seconds)
      self.period = period

    else: #}{
      # Parse the period string
      self.period = pytimeparse.parse( period )
    #}

    if isinstance( reconnect_delay, int ): #{
      # reconnect_delay (seconds)
      self.reconnect_delay = reconnect_delay

    else: #}{
      # Parse the reconnect_delay string
      self.reconnect_delay = pytimeparse.parse( reconnect_delay )
    #}

    self.app = self.config.get( 'app', None )

    if self.app is not None: #{
      # Make the app aware of this scheduler
      self.app.set_scheduler( self )
    #}

    jobstores = { 'default': MemoryJobStore() }
    executors = { 'default': ThreadPoolExecutor(  self.config['thread_pool'] ) }
    job_defaults = {
      'coalesce'          : True,
      'misfire_grace_time': self.period / 2,
      'max_instances'     : 1,
    }

    self._reconnecting    = False
    self._pending_lock    = threading.Lock()
    self._pending_metrics = []
    self._beat            = 0
    self._job_lock        = threading.Lock()
    self._job_cnt         = 0

    self._sched = BlockingScheduler( jobstores    = jobstores,
                                     executors    = executors,
                                     job_defaults = job_defaults )
  #}

  @property
  def verbosity( self ): #{
    return self.config.get('verbosity', 0)
  #}

  @property
  def is_running( self ): #{
    return self._sched is not None and self._sched.running
  #}

  def start( self, verbosity = None ): #{
    """
      Start the scheduler, gathering the set of available, unique disks and
      their log pages, creating and starting an initial collection schedule,
      and handling re-scheduling when each page is collected.

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if self.is_running: #{
      if verbosity: #{
        print('*** Scheduler already running', file=sys.stderr)
      #}
      return
    #}

    unique_drives = self.config['unique_drives']
    if unique_drives is None: #{
      # Gather all unique drives
      scan_config = {'include_scsi': self.config['include_scsi']}

      unique_drives = self.app.gather_unique_drives( scan_config )
    #}

    # For each unique drive, gather the set of supported metric sources along
    # with the cost of retrieving each
    self.drive_cnt   = 0
    self.drive_had   = 0
    self.drive_pages = []

    for serial,drive in unique_drives.items(): #{
      self.drive_cnt += 1

      # Retrieve the set of supported metric sources and filter out
      # ScsiLogPages:
      #   - 0x00,00   more comprehensive information available   via 0x00,ff
      #   - *,ff      the set of subpages is already represented via 0x00,ff
      #               (allow 0x00,ff);
      sources = list( filter( _filter_sources,
                              drive.get_metric_sources( verbosity ) ) )

      if len(sources) < 1: #{
        if verbosity: #{
          print('*** Cannot fetch supported metric sources for %s' %
                (drive), file=sys.stderr)
        #}
        continue
      #}

      if verbosity > 1: #{
        print('%s\n=== Scheduler.start(): %d metric sources: %s\n' %
              (repr(drive), len(sources), sources),
              file=sys.stderr )

      elif verbosity: #}{
        print('=== Scheduler.start(): %d metric sources for %s' %
              (len(sources), drive), file=sys.stderr)
      #}

      self.drive_had += 1
      self.drive_pages.extend( sources )

      #drive.close()
    #}

    if self.drive_cnt < 1: #{
      print('*** Scheduler.start(): Unable to locate any SCSI drives',
            file=sys.stderr)

      self.shutdown()
      return
    #}

    if len(self.drive_pages) < 1: #{
      print('*** Scheduler.start(): Unable to identify '
                        'any supported metric sources for %d / %d drives' %
            (self.drive_had, self.drive_cnt),
            file=sys.stderr)

      self.shutdown()
      return
    #}

    self._sched.add_listener( self._handle_job_event, EVENT_JOB_EXECUTED |
                                                      EVENT_JOB_ERROR    |
                                                      EVENT_JOB_MISSED )

    # Perform the first job scheduling
    self.reschedule()
  #}

  def _handle_job_event( self, event ): #{
    #if event.exception: job crashed, else worked or missed
    str = event
    if   event.code == EVENT_JOB_EXECUTED:  str = 'executed'
    elif event.code == EVENT_JOB_ERROR:     str = 'error'
    elif event.code == EVENT_JOB_MISSED:    str = 'missed'

    self._job_lock.acquire()

    remain = self._job_cnt - 1
    if self._job_cnt > 0: #{
      self._job_cnt -= 1
    #}

    self._job_lock.release()

    if self.verbosity: #{
      print('=== _handle_job_event():%s: %d jobs remain' %
            ( str, remain ), file=sys.stderr)
    #}

    if remain == 0: #{
      if self.config.get( 'one_shot', False ): #{
        # Shutdown the app and this scheduler by sending a SIGINT
        os.kill( os.getpid(), signal.SIGINT )

      else: #}{
        # Reschedule collection for the next period
        self.reschedule()
      #}

    #}
  #}

  def stop( self, verbosity = None ): #{
    """
      Stop the scheduler.

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if verbosity: #{
      jobs = self._sched.get_jobs()

      print('=== Scheduler.stop(): %d jobs' %
            ( (len(jobs) if jobs else 0) ), file=sys.stderr)

      #self._sched.print_jobs( out=sys.stderr )
      if jobs and verbosity > 1: #{
        _print_jobs( jobs )
      #}
    #}

    if self.is_running: #{
      # Shutdown the scheduler
      self._sched.remove_listener( self._handle_job_event )

      self._sched.shutdown()
    #}
  #}

  def pause( self, verbosity = None ): #{
    """
      Pause the scheduler.

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if verbosity: #{
      jobs = self._sched.get_jobs()

      print('=== Scheduler.pause(): %d jobs' %
            ( (len(jobs) if jobs else 0) ), file=sys.stderr)

      #self._sched.print_jobs( out=sys.stderr )
      if jobs and verbosity > 1: #{
        _print_jobs( jobs )
      #}
    #}

    if self.is_running: #{
      self._sched.pause()
    #}
  #}

  def resume( self, verbosity = None ): #{
    """
      Resume the scheduler.

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if verbosity: #{
      jobs = self._sched.get_jobs()

      print('=== Scheduler.resume(): %d jobs' %
            ( (len(jobs) if jobs else 0) ), file=sys.stderr)

      #self._sched.print_jobs( out=sys.stderr )
      if jobs and verbosity > 1: #{
        _print_jobs( jobs )
      #}
    #}

    if self.is_running: #{
      self._sched.resume()
    #}
  #}

  def shutdown( self, verbosity = None ): #{
    """
      Shutdown the scheduler and any pending reconnections

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if verbosity: #{
      print('=== Scheduler.shutdown() ...', file=sys.stderr)
    #}

    if self._reconnecting: #{
      # Interrupt the reconnect wait
      self._reconnecting.set()
    #}

    if self.is_running: #{
      self.stop( verbosity )
    #}
  #}

  def reschedule( self, verbosity = None ): #{
    """
      (Re)scehdule the set of drive/pages for random retrieval within the
      next scheduling period.

      Args:
        [verbosity = config.verbosity]  (int)   Debug verbosity
    """
    if verbosity is None: verbosity = self.verbosity

    if self._reconnecting: #{
      print('=== reschedule: skip : reconnecting[ %s ]' %
            (repr(self._reconnecting)),
            file=sys.stderr)
      return
    #}

    self._job_lock.acquire()
    if self._job_cnt > 0: #{
      print('=== reschedule: skip : %d pending jobs' %
            (self._job_cnt), file=sys.stderr)

      self._job_lock.release()
      return
    #}

    self._beat    += 1
    self._job_cnt  = 0
    is_sync        = (self._beat % self.config['sync_beat'] == 0)

    print('@%-6d%s Scheduler.reschedule(): %d metric sources for '
                                            '%d / %d drives' %
          (self._beat,
           ('*' if is_sync else ':'),
           len(self.drive_pages), self.drive_had, self.drive_cnt),
          file=sys.stderr)

    # Randomly shuffle the set of drive pages
    random.shuffle( self.drive_pages )

    # Add all pages as jobs within a period that begins now
    period_start = int( datetime.now().timestamp() )
    period_end   = period_start + self.period
    for page in self.drive_pages: #{
      # Add a job for the target page with a random run time within the
      # configured period
      #
      # :TODO: Take `metric_src.cost` into account when selecting the random
      #        slot perhaps by reducing `period_max`
      #
      period_min = period_start
      period_max = period_min + self.period

      # Randomly select a time (in seconds) between the start and end of the
      # computed period (period_min .. period_max)
      ts       = random.randint( period_start, period_end )
      run_date = datetime.fromtimestamp( ts )
      job      = self._sched.add_job( _gather_data, 'date',
                                          run_date= run_date,
                                          args    = [ self, page ] )

      self._job_cnt += 1

      if verbosity and job: #{
        print('=== Scheduler.reschedule(): ', end='', file=sys.stderr)
        _print_job( job )
      #}
    #}

    if verbosity: #{
      jobs = self._sched.get_jobs()

      print('=== Scheduler.reschedule(): %d jobs / %d metric sources '
                                          'for %d / %d drives' %
            (len(jobs), len(self.drive_pages), self.drive_had, self.drive_cnt),
            file=sys.stderr)

      #self._sched.print_jobs( out=sys.stderr )
      if jobs and verbosity > 1: #{
        _print_jobs( jobs )
      #}
    #}

    self._job_lock.release()

    if not self.is_running: #{
      self._sched.start()
    #}
  #}
#}

##############################################################################
# Private helpers {
#
def _filter_sources( src ): #{
  """
    This is a filter that may be used to exclude page 0x00,00, and *,ff

    Args:
      src (MetricSource, possibly ScsiLogPage)

    Returns:
      (bool)
  """
  if isinstance( src, ScsiLogPage ): #{
    if src.page == 0       and src.subpage == 0: return False
    if src.subpage == 0xff and src.page    != 0: return False
  #}

  return True
#}

def _drive_str( drive ): #{
  """
    A helper to generate a string representation for a metric source drive

    Args:
      drive (Drive)

    Returns:
      (str)
  """
  dev = (drive.dev_paths[0]
          if len(drive.dev_paths) > 0
          else 'no-dev')

  return ('%s [ %-10s ]' % (drive, dev))
#}

def _print_job( job ): #{
  """
    Helper to print a single job

    Args:
      job (Job)   The target job
  """
  (sched, page, *rest) = job.args
  next_run             = 'pending'
  if hasattr( job, 'next_run_time' ): #{
    next_run = (datetime_repr( job.next_run_time )
                  if job.next_run_time
                  else 'paused')
  #}

  print('%s @ %-24s : %s' %
        (_drive_str( page.src_drive ), next_run, page),
        file=sys.stderr)
#}

def _print_jobs( jobs ): #{
  """
    Helper to print a set of jobs

    Args:
      jobs (list)   The set of jobs
  """
  for job in jobs: #{
    print('===   ', end='', file=sys.stderr)
    _print_job( job )
  #}
#}

def _gather_data( self, metric_src ): #{
  """
    A scheduler callable invoked to gather the data from a specific source

    Args:
      self (Scheduler)          The controlling scheduler instance;
      metric_src (MetricSource) The target source;
  """

  # When the beat counter reaches a multiple of 'sync beat', then ALL data for
  # a source should be reported even if it hasn't changed.
  is_sync = (self._beat % self.config['sync_beat'] == 0)
  if is_sync: #{
    # (Re)set the source diff so all data will be reported
    metric_src.reset_diff()
  #}

  start = time.monotonic()

  # Fetch metrics for the target page
  metrics = self.app.metricize_source( metric_src )

  end = time.monotonic()

  metric_cnt = (len(metrics) if metrics is not None else 'NO')

  print('@%-6d%s Scheduler.gather(): %s : %s : '
                          '%s %s in %8.7f seconds' %
        ( self._beat,
          ('*' if is_sync else ':'),
          _drive_str( metric_src.src_drive ),
          metric_src,
          metric_cnt,
          ('metrics' if is_sync else 'diffs'),
          end - start ),
        file=sys.stderr)

  _publish_metrics( self, metrics )
#}

def _publish_metrics( self, metrics = True ): #{
  """
    Attempt to publish the given set of metrics or any pending metrics if this
    is a retry.

    Args:
      self (Scheduler)                The controlling scheduler instance;
      [metrics = True] (list | bool)  The set of metrics to publish or True if
                                      this is a retry;
  """
  verbosity = self.verbosity

  self._pending_lock.acquire()

  try_reconnect = False

  if metrics is True: #{
    # This is a retry
    if verbosity: #{
      print('=== _publish_metrics(): Retry (%d pending) ...' %
            (len(self._pending_metrics)), file=sys.stderr)
    #}

  else: #}{
    # This is NOT a retry
    #
    # Handle any needed reconnect iff there are currently no pending metrics
    #
    if isinstance(metrics, list) and len(metrics) > 0: #{
      self._pending_metrics.extend( metrics )
    #}
  #}

  # Attempt to publish ALL pending metrics
  try: #{
    if verbosity: #{
      print('=== _publish_metrics(): %d pending (%d new) ...' %
            (len(self._pending_metrics),
             (len(metrics) if isinstance(metrics, list) else 0)),
            file=sys.stderr)
    #}

    self.app.publish_metrics( self._pending_metrics )

    self._pending_metrics = []

  except Exception as ex: #}{
    try_reconnect = True

    self.pause()

    print('*** Exception:', ex, file=sys.stderr)

    # Pause scheduler until we can re-connect via
    #   self.app.connect_publish()
    print('=== Publish disconnected (%d pending) -- '
                          'pause until we can reconnect ...' %
          ( len(self._pending_metrics) ),
          file=sys.stderr)

  #}

  self._pending_lock.release()

  if try_reconnect: _reconnect( self )
#}

def _reconnect( self ): #{
  """
    Reconnection handler for when an exception is generated via
    self.app.publish_metrics() and we need to wait until
    self.app.connect_publish() can successfully reconnect.

    During this time, the scheduler should be paused.

    Args:
      self      (Scheduler)   The controlling scheduler instance;
  """
  if self._reconnecting:  return
  self._reconnecting = threading.Event()

  self._job_lock.acquire()

  # Cancel all current jobs
  for job in self._sched.get_jobs(): #{
    #self._sched.print_jobs( out=sys.stderr )
    job.remove()
  #}

  self._job_cnt = 0
  self._job_lock.release()

  delay     = self.reconnect_delay
  delay_max = delay * 10

  while self.app.is_active and not self._reconnecting.is_set(): #{
    print('=== Reconnecting ...', file=sys.stderr)

    try: #{
      self.app.connect_publish()

      break

    except Exception as ex: #}{
      pass
    #}

    print('=== Publish cannot reconnect (%d pending) -- wait %d seconds ...' %
          (len(self._pending_metrics), delay), file=sys.stderr)

    # Interruptable wait
    self._reconnecting.wait( delay )

    delay *= 2
    if delay > delay_max: delay = delay_max
  #}

  self._reconnecting = False

  print('=== Reconnected (%d pending, active[%s], running[%s]) ...' %
        ( len(self._pending_metrics),
          repr(self.app.is_active),
          repr(self.is_running) ),
        file=sys.stderr)

  if not self.app.is_active: return

  # Attempt to re-send pending metrics ( self._pending_metrics) )
  _publish_metrics( self )

  # Resume the scheduler
  self.resume()

  self.reschedule()
#}

# Private helpers }
##############################################################################
