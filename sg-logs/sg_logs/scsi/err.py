#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
class ScsiErr: #{
  """
    A class with static members defining useful SCSI Error codes and their
    associated string representations.

    Most are take from include/scsi/sg_lib.h
  """

  OK                    = 0x00
  NOT_READY             = 0x02
  MEDIUM_HARDWARE       = 0x03

  ILLEGAL_REQUEST       = 0x05
  UNIT_ATTENTION        = 0x06
  DATA_PROTECT          = 0x07
  INVALID_OPERATION     = 0x09

  COPY_ABORTED          = 0x0a
  ABORTED_COMMAND       = 0x0b

  MISCOMPARE            = 0x0e
  FILE_ERROR            = 0x0f

  NO_SENSE              = 0x14
  RECOVERED             = 0x15
  LBA_OUT_OF_RANGE      = 0x16

  RESERVATION_CONFLICT  = 0x18
  CONDITION_MET         = 0x19
  BUSY                  = 0x1a
  TS_FULL               = 0x1b
  ACA_ACTIVE            = 0x1c
  TASK_ABORTED          = 0x1d

  CONTRADICT            = 0x1f
  LOGIC_ERROR           = 0x20

  WINDOWS_ERROR         = 0x22

  NO_ERROR              = 0x24

  PROTECTION            = 0x28

  # UNIX errnos (shifted by OS_BASE) : 0x33[51] .. 0x60[96] {
  #   e.g. ENOMEM(12)  => 0x32 + 12 => 0x3e
  OS_BASE               = 0x32

  OS_EPERM              = 0x32 + 1  # 0x33: Operation not permitted
  OS_ENOENT             = 0x32 + 2  # 0x34: No such file or directory
  OS_ESRCH              = 0x32 + 3  # 0x35: No such process
  OS_EINTR              = 0x32 + 4  # 0x36: Interrupted system call
  OS_EIO                = 0x32 + 5  # 0x37: I/O error
  OS_ENXIO              = 0x32 + 6  # 0x38: No such device or address
  OS_E2BIG              = 0x32 + 7  # 0x39: Argument list too long
  OS_ENOEXEC            = 0x32 + 8  # 0x3a: Exec format error
  OS_EBADF              = 0x32 + 9  # 0x3b: Bad file number
  OS_ECHILD             = 0x32 + 10 # 0x3c: No child processes
  OS_EAGAIN             = 0x32 + 11 # 0x3d: Try again
  OS_ENOMEM             = 0x32 + 12 # 0x3e: Out of memory
  OS_EACCES             = 0x32 + 13 # 0x3f: Permission denied
  OS_EFAULT             = 0x32 + 14 # 0x40: Bad address
  OS_ENOTBLK            = 0x32 + 15 # 0x41: Block device required
  OS_EBUSY              = 0x32 + 16 # 0x42: Device or resource busy
  OS_EEXIST             = 0x32 + 17 # 0x43: File exists
  OS_EXDEV              = 0x32 + 18 # 0x44: Cross-device link
  OS_ENODEV             = 0x32 + 19 # 0x45: No such device
  OS_ENOTDIR            = 0x32 + 20 # 0x46: Not a directory
  OS_EISDIR             = 0x32 + 21 # 0x47: Is a directory
  OS_EINVAL             = 0x32 + 22 # 0x48: Invalid argument
  OS_ENFILE             = 0x32 + 23 # 0x49: File table overflow
  OS_EMFILE             = 0x32 + 24 # 0x4a: Too many open files
  OS_ENOTTY             = 0x32 + 25 # 0x4b: Not a typewriter
  OS_ETXTBSY            = 0x32 + 26 # 0x4c: Text file busy
  OS_EFBIG              = 0x32 + 27 # 0x4d: File too large
  OS_ENOSPC             = 0x32 + 28 # 0x4e: No space left on device
  OS_ESPIPE             = 0x32 + 29 # 0x4f: Illegal seek
  OS_EROFS              = 0x32 + 30 # 0x50: Read-only file system
  OS_EMLINK             = 0x32 + 31 # 0x51: Too many links
  OS_EPIPE              = 0x32 + 32 # 0x52: Broken pipe
  OS_EDOM               = 0x32 + 33 # 0x53: Math argument out of domain of func
  OS_ERANGE             = 0x32 + 34 # 0x54: Math result not representable

  OS_EDEADLK            = 0x32 + 35 # 0x55: Resource deadlock would occur
  OS_ENAMETOOLONG       = 0x32 + 36 # 0x56: File name too long
  OS_ENOLCK             = 0x32 + 37 # 0x57: No record locks available
  OS_ENOSYS             = 0x32 + 38 # 0x58: Function not implemented
  OS_ENOTEMPTY          = 0x32 + 39 # 0x59: Directory not empty
  OS_ELOOP              = 0x32 + 40 # 0x5a: Too many symbolic links encountered
  OS_EWOULDBLOCK        = 0x32 + 41 # 0x5b: Operation would block
  OS_ENOMSG             = 0x32 + 42 # 0x5c: No message of desired type
  OS_EIDRM              = 0x32 + 43 # 0x5d: Identifier removed
  OS_ECHRNG             = 0x32 + 44 # 0x5e: Channel number out of range
  OS_EL2NSYNC           = 0x32 + 45 # 0x5f: Level 2 not synchronized
  OS_EL3HLT             = 0x32 + 46 # 0x60: Level 3 halted

  # UNIX errnos (shifted by OS_BASE) : 0x33[51] .. 0x60[96] }

  MALFORMED             = 0x61
  SENSE                 = 0x62
  OTHER                 = 0x63

  # SCSI Error string map, by error code
  str_map   = {
    0x00  : 'OK',
    0x02  : 'NOT_READY',
    0x03  : 'MEDIUM_HARDWARE',

    0x05  : 'ILLEGAL_REQUEST',
    0x06  : 'UNIT_ATTENTION',
    0x07  : 'DATA_PROTECT',
    0x09  : 'INVALID_OPERATION',

    0x0a  : 'COPY_ABORTED',
    0x0b  : 'ABORTED_COMMAND',

    0x0e  : 'MISCOMPARE',
    0x0f  : 'FILE_ERROR',

    0x14  : 'NO_SENSE',
    0x15  : 'RECOVERED',
    0x16  : 'LBA_OUT_OF_RANGE',

    0x18  : 'RESERVATION_CONFLICT',
    0x19  : 'CONDITION_MET',
    0x1a  : 'BUSY',
    0x1b  : 'TS_FULL',
    0x1c  : 'ACA_ACTIVE',
    0x1d  : 'TASK_ABORTED',

    0x1f  : 'CONTRADICT',
    0x20  : 'LOGIC_ERROR',

    0x22  : 'WINDOWS_ERROR',

    0x24  : 'NO_ERROR',

    0x28  : 'PROTECTION',

    # UNIX errnos (shifted by OS_BASE) : 0x33[51] .. 0x60[96] {
    0x33  : 'Operation not permitted',
    0x34  : 'No such file or directory',
    0x35  : 'No such process',
    0x36  : 'Interrupted system call',
    0x37  : 'I/O error',
    0x38  : 'No such device or address',
    0x39  : 'Argument list too long',
    0x3a  : 'Exec format error',
    0x3b  : 'Bad file number',
    0x3c  : 'No child processes',
    0x3d  : 'Try again',
    0x3e  : 'Out of memory',
    0x3f  : 'Permission denied',
    0x40  : 'Bad address',
    0x41  : 'Block device required',
    0x42  : 'Device or resource busy',
    0x43  : 'File exists',
    0x44  : 'Cross-device link',
    0x45  : 'No such device',
    0x46  : 'Not a directory',
    0x47  : 'Is a directory',
    0x48  : 'Invalid argument',
    0x49  : 'File table overflow',
    0x4a  : 'Too many open files',
    0x4b  : 'Not a typewriter',
    0x4c  : 'Text file busy',
    0x4d  : 'File too large',
    0x4e  : 'No space left on device',
    0x4f  : 'Illegal seek',
    0x50  : 'Read-only file system',
    0x51  : 'Too many links',
    0x52  : 'Broken pipe',
    0x53  : 'Math argument out of domain of func',
    0x54  : 'Math result not representable',

    0x55  : 'Resource deadlock would occur',
    0x56  : 'File name too long',
    0x57  : 'No record locks available',
    0x58  : 'Function not implemented',
    0x59  : 'Directory not empty',
    0x5a  : 'Too many symbolic links encountered',
    0x5b  : 'Operation would block',
    0x5c  : 'No message of desired type',
    0x5d  : 'Identifier removed',
    0x5e  : 'Channel number out of range',
    0x5f  : 'Level 2 not synchronized',
    0x60  : 'Level 3 halted',
    # UNIX errnos (shifted by OS_BASE) : 0x33[51] .. 0x60[96] }

    0x61  : 'MALFORMED',
    0x62  : 'SENSE',
    0x63  : 'OTHER',
  }

  @staticmethod
  def err_str( err ): #{
    return (ScsiErr.str_map[err]
              if err in ScsErr.str_map
              else ('UNKNOWN: err=0x%x' % (err)))
  #}
#}
