#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs          import sg3utils
from sg_logs.scsi.asc import ScsiASC
from sg_logs.smart    import Smart

##
# General SMART information
class ScsiSmart(Smart): #{
  """
    A class representing SMART information for a single SCSI drive.
  """
  def __init__( self, src_drive,
                      title     = 'SMART',
                      level     = 0,
                      cost      = 0.25,
                      capable   = False,
                      enabled   = False,
                      status    = 'UNKNOWN',
                      attrs     = None ): #{
    """
      Create a new instance.

      Args:
        src_drive (ScsiDrive)     : The source drive for accessing the SMART
                                    information;
        [title = 'SMART') (str)   : The title of this source;
        [level = 0] (int)         : Reporting level
                                    (0  : always report,
                                     >0 : report only if the application level
                                          report level has been elevated to at
                                          least the same level);
        [cost = 0.25] (float)     : The cost of retrieving data from this
                                    source where 0 is free and 1 is very
                                    expensive;
        [capable = False] (bool)  : Is `src_drive` SMART capable;
        [enabled = False] (bool)  : Is SMART enabled for `src_drive`;
        [status = 'UNKNOWN'] (str): The overall 'PASS' / 'FAIL' status;
        [attrs = None] (list)     : SMART-related attributes;

      SCSI-specific attributes:
        temperature_warnings (bool) : are temperature warnings enabled?
        next_test (int)             : time until next internal self test
                                      (seconds) (-1 == unknown);
    """
    if not isinstance( attrs, list ): #{
      attrs = []
    #}

    super().__init__( src_drive,
                        title   = title,
                        level   = level,
                        cost    = cost,
                        capable = capable,
                        enabled = enabled,
                        status  = status,
                        attrs   = attrs )

    # :NOTE: For ScsiSmart, `value` is used as storage for raw sense data
    #        retrieve to provide SMART information and is updated
    #        in conjunction with update_status() with update_value() disabled.

    # sense information cache
    self._sense = { 'mode': {}, 'log': {} }

    self.get_capabilities()
  #}

  @property
  def asc( self ): #{
    """
      Return the latest erroneous ASC indicator (if any).
        ASC << 8 | ASC  == ASC * 256 + ASCQ

      Returns:
        (int)   The latest erroroneous ASC indicator (-1 if not set)
    """
    return self.asc_from_value( self.value )
  #}

  @property
  def ascq( self ): #{
    """
      Return the latest erroneous ASCQ indicator (if any).
        ASC << 8 | ASC  == ASC * 256 + ASCQ

      Returns:
        (int)   The latest erroroneous ASCQ indicator (-1 if not set)
    """
    return self.ascq_from_value( self.value )
  #}

  #############################################################################
  # Smart method overrides {
  #
  def update_value( self ): #{
    """
      Over-ride since `value` is updated via a different path.

      Returns:
        (int)   The updated `value`
    """
    return self.value
  #}

  def update( self ): #{
    """
      Gather SMART information for the source drive.

      Returns:
        (ScsiSmart) The updated smart information
    """
    # Update the SMART information
    self.update_status()
    self.update_next_test()

    return self
  #}

  def __repr__( self ): #{
    """
      Generate a string representation of this instance.

      Returns:
        (str)
    """
    return ('%s(src_drive=%s, title=%s, level=%s, cost=%s, '
                'capable=%s, enabled=%s, status=%s, '
                'asc=%s, ascq=%s )' %
              (type(self).__name__,
               repr(self.src_drive),
               repr(self.title),
               repr(self.level),
               repr(self.cost),
               repr(self.capable),
               repr(self.enabled),
               repr(self.status),
               repr(self.asc),
               repr(self.ascq)) )
  #}

  #
  # Smart method overrides }
  #############################################################################
  # SCSI-specific methods {
  #
  def get_capabilities( self ): #{
    """
      Gather basic SMART capabilities for the target drive.

      Returns:
        (ScsiSmart) The basic gathered smart information
    """
    if (self.src_drive     is None or
        self.src_drive.sg3 is None or
        self.src_drive.dev_type != 'disk'): #{
      return self
    #}

    ######################################################################
    # Check if the source drive is SMART capable
    #
    # For SCSI drives, this is indicated via the existence of the
    # Informational Exceptions Control Mode page (0x1c,00);
    #
    info = None
    try: #{
      # Fetch the Informational Exceptions Control Mode page
      info  = sg3utils.mode_sense( self.src_drive.sg3, page=0x1c )

    except Exception as ex: #}{
      # Squelch
      #print("*** mode_sense FAILED:", ex, file=sys.stderr)
      pass
    #}

    if info: #{
      self.capable = True

      # Remember this mode_sense data
      self._sense['mode']['0x1c,00'] = info

      state = info['data']

      #print('ScsiSmart.get_capabilities(): mode_sense(0x1c):', state)

      self.enabled = not state['disable_exception_control']

      attr = self.get_attr( 'temperature_warnings' )
      if attr is None: #{
        attr = {
          'name'  : 'temperature_warnings',
          'value' : state['enable_warning'],
          'units' : 'enabled',
        }

        self.attrs.append( attr )

      else: # }{
        attr['value'] = state['enable_warning']
      #}
    #}

    return self
  #}

  def update_status( self ): #{
    """
      Populate the SMART status

      Returns:
        (ScsiSmart) The (possibly) updated SMART information
    """
    if not self.capable:  return self

    # assert( self.src_drive.sg3 is not None and
    #         self.src_drive.dev_type == 'disk' )

    # Check the SMART status using the Informational Exceptions log page
    sense_data = None

    try: #{
      sense_data = sg3utils.log_sense( self.src_drive.sg3, page=0x2f )

    except Exception as ex: #}{
      # Squelch
      #print("*** log_sense(0x2f) FAILED:", ex, file=sys.stderr)
      pass
    #}

    if sense_data: #{
      # We have retrieved data from the Informational Exceptions log page.
      #
      # Remember this log_sense data
      self._sense['log']['0x2f,00'] = sense_data

      # Walk all parameters, skipping any that are NOT code 0 and use the ASC
      # and ASCQ values from each to determine the SMART status, stopping
      # with the first that reports anything other than 'OK'
      smart_map = {}
      for param in sense_data['params']: #{
        if param['code'] != 0x00: continue

        value   = param['value_raw']
        nBytes  = len(value)

        #  Parameter Code 0x00 (General parameter, parameter data)
        #
        #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
        #    Byte |     |     |     |     |     |     |     |     |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     0   | Informational Exception Additional Sense Code |
        #         | (ASC)                                         |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     1   | Informational Exception Additional Sense Code |
        #         | Qualifier (ASCQ)                              |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     2   | Most Recent Temperature Reading               |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     3   | Vendor HDA Temperature Trip Point             |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     4   | Maximum Temperature                           |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        #     5   | Vendor Specific                               |
        #    ...  |                                               |
        #     7   |                                               |
        #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
        if nBytes > 2: #{
          asc   = value[0]
          ascq  = value[1]

          #print('Scsi.update_status(): log_sense(0x2f): param code 00, '
          #        + 'asc[ 0x%02x ], ascq[ 0x%02x ]' %
          #        (asc, ascq))
          if asc != 0 or ascq != 0: #{

            # Generate a SMART value and status from ASC/ASCQ
            value  = self.value_from_as( asc, ascq )
            status = self.status_from_as( asc, ascq )

            smart_map[ value ] = self.status
          #}
        #}
      #}

      if len(smart_map) > 0: #{
        #
        # There were 1 or more unique indicators.
        #
        # Update the smart information:
        #   - value   : the last error status value;
        #   - status  : include all status indicators as a CSV ordered
        #               according to the ascending status value;
        #
        value      = None
        smart_strs = []
        for val,str in sorted( smart_map.items() ): #{
          if val: value = val

          smart_strs.append( str )
        #}

        self.value  = value
        self.status = ','.join( smart_strs )

      else: #}{
        self.set_status_from_scsi_as( 0, 0 )
      #}
    #}

    return self
  #}

  def update_next_test( self ): #{
    """
      Populate the next test time.

      Returns:
        (ScsiSmart) The (possibly) updated SMART information
    """
    if not self.capable: return self

    # assert( self.src_drive.sg3 is not None and
    #         self.src_drive.dev_type == 'disk' )

    # Check if the Factory Log page is available and reports the time until the
    # next internal SMART test
    sense_data = None

    try: #{
      sense_data = sg3utils.log_sense( self.src_drive.sg3, page=0x3e )

    except Exception as ex: #}{
      #print("*** log_sense(0x3e) FAILED:", ex, file=sys.stderr)
      pass
    #}

    if sense_data: #{
      # We have retrieved data from the Factory log page.
      #
      # Remember this log_sense data
      self._sense['log']['0x3e,00'] = sense_data

      # See if parameter code 0x08 (time to next internal SMART test) is
      # available.
      for param in sense_data['params']: #{
        if param['code'] != 0x08: continue

        value = param['value']
        if value > 0: #{
          # Seagate reports 'time to next internal SMART test' in minutes
          attr = self.get_attr( 'next_test' )
          if attr is None: #{
            attr = {
              'name'  : 'next_test',
              'value' : value,
              'units' : 'minutes',
            }

            self.attrs.append( attr )

          else: # }{
            attr['value'] = value
          #}
        #}
        break
      #}
    #}

    return self
  #}

  def set_status_from_scsi_as( self, asc, ascq ): #{
    """
      Given a SCSI Additional Sense Code (ASC) and Additional Sense Code
      Qualifier (ASCQ), set the SMART status.

      References:
        smartmontools/scsiprint.cpp: scsiGetSmartData()
        smartmontools/scsicmds.cpp : scsiGetIEString()

      Args:
        asc (int)             Additional Sense Code
        ascq (int)            Additional Sense Code Qualifier
                              for more comprehensive decoding

      Returns:
        (str)   The smart status
    """
    self.value  = self.value_from_as( asc, ascq )
    self.status = self.status_from_as( asc, ascq )

    return self.status
  #}

  def value_from_as( self, asc, ascq ): #{
    """
      Given a SCSI Additional Sense Code (ASC) and Additional Sense Code
      Qualifier (ASCQ), generate a single value that combines the two:
        ASC << 8 | ASC  == ASC * 256 + ASCQ

      Returns:
        (int)   The aggregate ASC/ASCQ value (-1 if ASC/ASCQ never set)
    """
    return ( ((asc << 8) | ascq) if asc >= 0 else -1)
  #}

  def asc_from_value( self, value ): #{
    """
      Given a value generated via value_from_as() return the ASC portion
        ASC << 8 | ASC  == ASC * 256 + ASCQ

      Returns:
        (int)   The ASC (-1 if not available)
    """
    if value is None or value == -1:  return -1

    return (value >> 8) & 0x0FF
  #}

  def ascq_from_value( self, value ): #{
    """
      Given a value generated via value_from_as() return the ASCQ portion
        ASC << 8 | ASC  == ASC * 256 + ASCQ

      Returns:
        (int)   The ASCQ (-1 if not available)
    """
    if value is None or value == -1:  return -1

    return value & 0x0FF
  #}

  def status_from_as( self, asc, ascq ): #{
    """
      Given a SCSI Additional Sense Code (ASC) and Additional Sense Code
      Qualifier (ASCQ), generate a status string.

      References:
        smartmontools/scsiprint.cpp: scsiGetSmartData()
        smartmontools/scsicmds.cpp : scsiGetIEString()

      Args:
        asc (int)             Additional Sense Code
        ascq (int)            Additional Sense Code Qualifier
                              for more comprehensive decoding

      Returns:
        (str)   The smart status
    """
    status = 'OK'

    if asc != 0 or ascq != 0: #{

      if sg3utils is not None: #{
        # Use sg3utils decoding of ASC/ASCQ
        status = sg3utils.asc_ascq_str( asc, ascq )

      elif asc == ScsiASC.IMPENDING_FAILURE: #}{
        status = ScsiASC.asc_impending_failure_str( ascq )

      elif asc == ScsiASC.WARNING: #}{
        status = ScsiASC.asc_warning_str( ascq )

      else: #}{
        status = ('UNKNOWN ASC=%02x, ASCQ=%02x (hex)' % (asc, ascq))

      #}
    #}

    return status
  #}

  #
  # SCSI-specific methods }
  #############################################################################
#}
