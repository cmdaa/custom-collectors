#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
class ScsiType: #{
  """
    A class with static members defining device type codes and their associated
    string representations.
  """

  # SCSI Device type codes {
  DISK      = 0x00  # Direct access block device (e.g. magnetic disk)
  TAPE      = 0x01  # Sequential access device (e.g. magnetic tape)
  PRINTER   = 0x02
  PROCESSOR = 0x03
  WO        = 0x04  # Write once device (e.g. some optical disks)
  MMC       = 0x05  # CD/DVD/BD (multi-media)
  SCANNER   = 0x06  # (obsolete)
  OPTICAL   = 0x07  # Optical memory device (e.g. some optical disks)
  MCHANGER  = 0x08  # Media changer (e.g. tape robot)
  COMMS     = 0x09  # Communications device (obsolete)
  # 0x0a-0b           (obsolete)
  SAC       = 0x0c  # Storage array controller (e.g. RAID)
  SES       = 0x0d  # SCSI Enclosure Services (SES)
  RBC       = 0x0e  # Reduced Block Commands (simplified DISK)
  OCRW      = 0x0f  # Optical card reader/writer
  BCC       = 0x10  # Bridge Controller Commands
  OSD       = 0x11  # Object Storage Device (OSD)
  ADC       = 0x12  # Automation/Drive Commands (ADC)
  SMD       = 0x13  # Security Manager Device (SMD)
  ZBC       = 0x14  # Zoned Block Commands (ZBC)
  # 0x15-1d           (reserved)
  WLUN      = 0x1e  # Well known logical unit (WLUN)
  UNKNOWN   = 0x1f  # Unknown or no device type
  #
  ANY       = 0xff  # ANY device type
  # SCSI Device type codes }

  # SCSI device type map indexed by scsi type code (int)
  str       = [
    'disk',    'tape',    'printer', 'process', 'worm',   'cd/dvd',
    'scanner', 'optical', 'mediumx', 'comms',
    '(0xa)',  '(0xb)',
    'storage', 'enclosu', 'sim dsk', 'opti rd', 'bridge', 'osd',
    'adi',     'sec man', 'zbc',
    '(0x15)',  '(0x16)', '(0x17)', '(0x18)',  '(0x19)',  '(0x1a)',  '(0x1b)',
    '(0x1c)', '(0x1d)',
    'wlun',    'no dev',
  ]
#}
