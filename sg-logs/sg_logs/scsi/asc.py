#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
class ScsiASC: #{
  """
    A class with static members defining useful Additional Sense Codes (ASC)
    and their associated string representations.

    Borrowed from: smartmontools/scsicmds.cpp
  """

  # SCSI Additional Sense Codes (ASC) {
  NOT_READY         = 0x04  # more info in ASCQ code
  WARNING           = 0x0b

  UNKNOWN_OPCODE    = 0x20
  INVALID_FIELD     = 0x24
  UNKNOWN_PARAM     = 0x26

  NO_MEDIUM         = 0x3a  # more info in ASCQ code

  IMPENDING_FAILURE = 0x5d  # SMART
  # SCSI Additional Sense Codes (ASC) }

  # SCSI ASC string map, by ASC
  #     http://www.t10.org/lists/asc-num.htm
  asc_map   = {
    0x04  : 'not ready',
    0x09  : 'track following error',
    0x0b  : 'warning',
    0x0b  : 'write error',
    0x10  : 'id crc or ecc error',
    0x11  : 'unrecovered read error',
    0x20  : 'unknown opcode',
    0x24  : 'invalid field',
    0x26  : 'unknown param',
    0x2e  : 'insufficient time for operation',
    0x34  : 'enclosure failure',
    0x35  : 'enclosure service failure',
    0x3a  : 'no medium',
    0x54  : 'scsi to host system interface failure',
    0x55  : 'system resource failure',
    0x5d  : 'impending failure',
  }

  # SCSI ASCQ string map for ASC 0x5d (IMPENDING_FAILURE), by ASCQ
  asc_5d_ascq_map  = {
    # General Hard Drive Failure
    0x00: "FAILURE PREDICTION THRESHOLD EXCEEDED",
    0x01: "MEDIA FAILURE PREDICTION THRESHOLD EXCEEDED",
    0x02: "LOGICAL UNIT FAILURE PREDICTION THRESHOLD EXCEEDED",
    0x03: "SPARE AREA EXHAUSTION PREDICTION THRESHOLD EXCEEDED",

    0x04: "BLOCK REASSIGN PREDICTION THRESHOLD EXCEEDED",
    0x05: "ACCESS TIME PREDICTION THRESHOLD EXCEEDED",
    0x06: "START UNIT TIME PREDICTION THRESHOLD EXCEEDED",
    0x07: "CHANNEL PARAMETRICS PREDICTION THRESHOLD EXCEEDED",
    0x08: "CONTROLLER PREDICTION THRESHOLD EXCEEDED",
    0x09: "THROUGHPUT PREDICTION THRESHOLD EXCEEDED",
    0x0a: "SEEK TIME PREDICTION THRESHOLD EXCEEDED",
    0x0b: "SPIN-UP RETRY PREDICTION THRESHOLD EXCEEDED",
    0x0c: "DRIVE CALIBRATION RETRY PREDICTION THRESHOLD EXCEEDED",

    # Hardware impending failure
    0x10: "IMPENDING FAILURE (hardware): GENERAL HARD DRIVE FAILURE",
    0x11: "IMPENDING FAILURE (hardware): DRIVE ERROR RATE TOO HIGH",
    0x12: "IMPENDING FAILURE (hardware): DATA ERROR RATE TOO HIGH",
    0x13: "IMPENDING FAILURE (hardware): SEEK ERROR RATE TOO HIGH",
    0x14: "IMPENDING FAILURE (hardware): TOO MANY BLOCK REASSIGNS",
    0x15: "IMPENDING FAILURE (hardware): ACCESS TIMES TOO HIGH",
    0x16: "IMPENDING FAILURE (hardware): START UNIT TIMES TOO HIGH",
    0x17: "IMPENDING FAILURE (hardware): CHANNEL PARAMETRICS",
    0x18: "IMPENDING FAILURE (hardware): CONTROLLER DETECTED",
    0x19: "IMPENDING FAILURE (hardware): THROUGHPUT PERFORMANCE",
    0x1a: "IMPENDING FAILURE (hardware): SEEK TIME PERFORMANCE",
    0x1b: "IMPENDING FAILURE (hardware): SPIN-UP RETRY COUNT",
    0x1c: "IMPENDING FAILURE (hardware): DRIVE CALIBRATION RETRY COUNT",

    # Controller impending failure
    0x20: "IMPENDING FAILURE (controller): GENERAL HARD DRIVE FAILURE",
    0x21: "IMPENDING FAILURE (controller): DRIVE ERROR RATE TOO HIGH",
    0x22: "IMPENDING FAILURE (controller): DATA ERROR RATE TOO HIGH",
    0x23: "IMPENDING FAILURE (controller): SEEK ERROR RATE TOO HIGH",
    0x24: "IMPENDING FAILURE (controller): TOO MANY BLOCK REASSIGNS",
    0x25: "IMPENDING FAILURE (controller): ACCESS TIMES TOO HIGH",
    0x26: "IMPENDING FAILURE (controller): START UNIT TIMES TOO HIGH",
    0x27: "IMPENDING FAILURE (controller): CHANNEL PARAMETRICS",
    0x28: "IMPENDING FAILURE (controller): CONTROLLER DETECTED",
    0x29: "IMPENDING FAILURE (controller): THROUGHPUT PERFORMANCE",
    0x2a: "IMPENDING FAILURE (controller): SEEK TIME PERFORMANCE",
    0x2b: "IMPENDING FAILURE (controller): SPIN-UP RETRY COUNT",
    0x2c: "IMPENDING FAILURE (controller): DRIVE CALIBRATION RETRY COUNT",

    # Data Channel impending failure
    0x30: "IMPENDING FAILURE (data channel): GENERAL HARD DRIVE FAILURE",
    0x31: "IMPENDING FAILURE (data channel): DRIVE ERROR RATE TOO HIGH",
    0x32: "IMPENDING FAILURE (data channel): DATA ERROR RATE TOO HIGH",
    0x33: "IMPENDING FAILURE (data channel): SEEK ERROR RATE TOO HIGH",
    0x34: "IMPENDING FAILURE (data channel): TOO MANY BLOCK REASSIGNS",
    0x35: "IMPENDING FAILURE (data channel): ACCESS TIMES TOO HIGH",
    0x36: "IMPENDING FAILURE (data channel): START UNIT TIMES TOO HIGH",
    0x37: "IMPENDING FAILURE (data channel): CHANNEL PARAMETRICS",
    0x38: "IMPENDING FAILURE (data channel): CONTROLLER DETECTED",
    0x39: "IMPENDING FAILURE (data channel): THROUGHPUT PERFORMANCE",
    0x3a: "IMPENDING FAILURE (data channel): SEEK TIME PERFORMANCE",
    0x3b: "IMPENDING FAILURE (data channel): SPIN-UP RETRY COUNT",
    0x3c: "IMPENDING FAILURE (data channel): DRIVE CALIBRATION RETRY COUNT",

    # Servo impending failure
    0x40: "IMPENDING FAILURE (servo): GENERAL HARD DRIVE FAILURE",
    0x41: "IMPENDING FAILURE (servo): DRIVE ERROR RATE TOO HIGH",
    0x42: "IMPENDING FAILURE (servo): DATA ERROR RATE TOO HIGH",
    0x43: "IMPENDING FAILURE (servo): SEEK ERROR RATE TOO HIGH",
    0x44: "IMPENDING FAILURE (servo): TOO MANY BLOCK REASSIGNS",
    0x45: "IMPENDING FAILURE (servo): ACCESS TIMES TOO HIGH",
    0x46: "IMPENDING FAILURE (servo): START UNIT TIMES TOO HIGH",
    0x47: "IMPENDING FAILURE (servo): CHANNEL PARAMETRICS",
    0x48: "IMPENDING FAILURE (servo): CONTROLLER DETECTED",
    0x49: "IMPENDING FAILURE (servo): THROUGHPUT PERFORMANCE",
    0x4a: "IMPENDING FAILURE (servo): SEEK TIME PERFORMANCE",
    0x4b: "IMPENDING FAILURE (servo): SPIN-UP RETRY COUNT",
    0x4c: "IMPENDING FAILURE (servo): DRIVE CALIBRATION RETRY COUNT",

    # Spindle impending failure
    0x50: "IMPENDING FAILURE (spindle): GENERAL HARD DRIVE FAILURE",
    0x51: "IMPENDING FAILURE (spindle): DRIVE ERROR RATE TOO HIGH",
    0x52: "IMPENDING FAILURE (spindle): DATA ERROR RATE TOO HIGH",
    0x53: "IMPENDING FAILURE (spindle): SEEK ERROR RATE TOO HIGH",
    0x54: "IMPENDING FAILURE (spindle): TOO MANY BLOCK REASSIGNS",
    0x55: "IMPENDING FAILURE (spindle): ACCESS TIMES TOO HIGH",
    0x56: "IMPENDING FAILURE (spindle): START UNIT TIMES TOO HIGH",
    0x57: "IMPENDING FAILURE (spindle): CHANNEL PARAMETRICS",
    0x58: "IMPENDING FAILURE (spindle): CONTROLLER DETECTED",
    0x59: "IMPENDING FAILURE (spindle): THROUGHPUT PERFORMANCE",
    0x5a: "IMPENDING FAILURE (spindle): SEEK TIME PERFORMANCE",
    0x5b: "IMPENDING FAILURE (spindle): SPIN-UP RETRY COUNT",
    0x5c: "IMPENDING FAILURE (spindle): DRIVE CALIBRATION RETRY COUNT",

    # Firmware impending failure
    0x60: "IMPENDING FAILURE (firmware): GENERAL HARD DRIVE FAILURE",
    0x61: "IMPENDING FAILURE (firmware): DRIVE ERROR RATE TOO HIGH",
    0x62: "IMPENDING FAILURE (firmware): DATA ERROR RATE TOO HIGH",
    0x63: "IMPENDING FAILURE (firmware): SEEK ERROR RATE TOO HIGH",
    0x64: "IMPENDING FAILURE (firmware): TOO MANY BLOCK REASSIGNS",
    0x65: "IMPENDING FAILURE (firmware): ACCESS TIMES TOO HIGH",
    0x66: "IMPENDING FAILURE (firmware): START UNIT TIMES TOO HIGH",
    0x67: "IMPENDING FAILURE (firmware): CHANNEL PARAMETRICS",
    0x68: "IMPENDING FAILURE (firmware): CONTROLLER DETECTED",
    0x69: "IMPENDING FAILURE (firmware): THROUGHPUT PERFORMANCE",
    0x6a: "IMPENDING FAILURE (firmware): SEEK TIME PERFORMANCE",
    0x6b: "IMPENDING FAILURE (firmware): SPIN-UP RETRY COUNT",
    0x6c: "IMPENDING FAILURE (firmware): DRIVE CALIBRATION RETRY COUNT",

    # 0x70 - 0xEF : Reserved

    #0xff: "FAILURE PREDICTION THRESHOLD EXCEEDED (FALSE)",
    0xff: "FAILURE PREDICTION (testing)",
  }

  # SCSI ASCQ string map for ASC 0x0b (WARNING), by ASCQ
  asc_0b_ascq_map  = {
    0x00: "WARNING",
    0x01: "WARNING - SPECIFIED TEMPERATURE EXCEEDED",
    0x02: "WARNING - ENCLOSURE DEGRADED",
  }

  @staticmethod
  def asc_str( asc ): #{
    return (ScsiASC.asc_map[asc] if asc in ScsiASC.asc_map
                                 else ('UNKNOWN: asc=0x%x' % (asc)))
  #}

  @staticmethod
  def asc_impending_failure_str( ascq ): #{
    return (ScsiASC.asc_5d_ascq_map[ascq]
              if ascq in ScsiASC.asc_5d_ascq_map
              else ('FAILURE PREDICTION THRESHOLD EXCEEDED: ascq=0x%x'
                      % (ascq)))
  #}

  @staticmethod
  def asc_warning_str( ascq ): #{
    return (ScsiASC.asc_0b_ascq_map[ascq]
              if ascq in ScsiASC.asc_0b_ascq_map
              else ('WARNING: ascq=0x%x' % (ascq)))
  #}
#}
