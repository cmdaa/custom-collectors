#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
class ScsiVendor: #{
  """
    A class representing a single SCSI vendor.
  """

  # Static identifiers
  SEAGATE   = 0x00
  HITACHI   = 0x01
  TOSHIBA   = 0x02
  SMRTSTOR  = 0x03

  HP        = 0x10
  IBM       = 0x11

  UNKNOWN   = 0xff

  def __init__( self, id, name, t10_str ): #{
    self.id      = id
    self.name    = name
    self.t10_str = t10_str
  #}

  def __repr__( self ): #{
    return ('%s [%s : 0x%02x]' % (self.name, self.t10_str, self.id))
  #}

  @staticmethod
  def by_t10( t10_str ): #{
    """
      Given a T10 vendor string, locate any matching, pre-defined ScsiVendor
      record

      Args:
        t10_str (str)   The T10 vendor string

      Returns:
        (ScsiVendor | None)
    """
    for vendor in ScsiVendors: #{
      if vendor.t10_str == t10_str: #{
        return vendor
      #}
    #}

    return None
  #}
#}

ScsiVendors = [
  #           id                    name                   t10_str
  ScsiVendor( ScsiVendor.SEAGATE,   'Seagate',             'SEAGATE' ),

  ScsiVendor( ScsiVendor.HITACHI,   'Hitachi',             'HITACHI' ),
  ScsiVendor( ScsiVendor.HITACHI,   'Hitachi GST',         'HGST' ),
  ScsiVendor( ScsiVendor.HITACHI,   'Hitachi/LG',          'HL-DT-ST' ),
  ScsiVendor( ScsiVendor.HITACHI,   'WDC/Hitachi',         'WDC' ),

  ScsiVendor( ScsiVendor.TOSHIBA,   'Toshiba',             'TOSHIBA' ),

  ScsiVendor( ScsiVendor.SMRTSTOR,  'SmrtStor (Sandisk)',  'SmrtStor' ),

  ScsiVendor( ScsiVendor.HP,        'Hewlett Packard',     'HP' ),
  ScsiVendor( ScsiVendor.IBM,       'IBM',                 'IBM' ),

  ScsiVendor( ScsiVendor.UNKNOWN,   'UNKNOWN',             'UNKNOWN' ),
]
