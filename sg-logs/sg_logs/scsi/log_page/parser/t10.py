#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs.drive.scsi     import ScsiDrive
from sg_logs.scsi.log_page  import ScsiLogPage

# Import all log page parameter parsers
from sg_logs.scsi.log_page.parser.t10_params import *

def get_drive_support_score( drive ): #{
  """
    Retrieve a drive support score.

    This should be an indication of the number of recognized metric sources
    the extension can parse for the target drive.

    If an extension deals with SCSI LOG pages/subpages and can delegate to
    the default T10 parser:
      - the basis of this score is the T10 parser score
        (the count of t10_parser entries);
      - this score should be increased for every page/subpage the extension
        explicitly recognizes:
        - by 1.0 for those unknown to the T10 parser;
        - by 0.5 for those known   to the T10 parser but presumably known more
                                   fully by the extension;

    Args:
      drive (Drive)   The target drive instance;

    Returns:
      (float) A score indicating how well the target drive is supported by the
              extension (0  : not at all,
                         >0 : an indication of recognized metric sources);
  """
  score = 0
  if isinstance( drive, ScsiDrive ): #{
    score = len(t10_parser)
  #}

  return score
#}

def parse_source( log_page, fetch_data,
                            report_level    = 0,
                            verbosity       = 0,
                            parse_unknowns  = False,
                            parser_map      = None ): #{
  """
    Given a log page and data fetched via `log_page.fetch()`, parse the
    SCSI LOG SENSE into a list of simplified metrics.

    Args:
      log_page (ScsiLogPage)          The target log page instance
      fetch_data (dict)               Pre-fetched metric/sense data retrieved
                                      via `log_page.fetch()`;
      [report_level = 0] (int)        Active report level;
      [verbosity = 0] (int)           Debug verbosity;
      [parse_unknowns = False] (bool) Should unknowns (e.g. parameters) be
                                      parsed?
      [parser_map = None] (dict)      If provided, a map used to over-ride the
                                      default `t10_parser` map for everything
                                      *except* 'default'.  This maps a SCSI LOG
                                      page/subpage to a page-specific parameter
                                      parser;

    Returns:
      (list)  : The set of simplified metrics extracted from `fetch_data`
                where each entry has the form:
                  { name    : (str),
                    value   : (int|float),
                    labels  : {
                      units :  (str),
                      ...
                    },
                    timestamp : (int) # Optional timestamp, milliseconds
                                      # since epoch;
                  }
  """
  if parser_map is None: parser_map = t10_parser

  parsed  = []

  if not isinstance( log_page, ScsiLogPage): #{
    # This pseudo-extension can only handle data from the ScsiLogPage metric
    # source
    return parsed
  #}

  page        = fetch_data['page']
  subpage     = fetch_data['subpage']
  parse_param = None

  if subpage == 0xff: #{
    # Page 0x00,ff is ALWAYS the set of all supported pages/sugpages
    #
    # SOME vendors also support Subpage 0xff for ALL available pages, returning
    # the list of supported sub-pages for the target log page
    parse_param = parser_map.get('00_ff', None)
  #}

  if parse_param is None: #{
    parser_name = '%02x_%02x' % (page, subpage)
    parse_param = parser_map.get( parser_name, None )

    if parse_param is None and parser_map != t10_parser: #{
      # Fall-back to the t10_parser
      parse_param = t10_parser.get( parser_name, None )
    #}

    if parse_param is None: #{
      # Fall-back to the t10_parser default paramaeter parser
      parse_param = t10_parser['default']
    #}
  #}

  #labels = {
  #  'metric_source': 'scsi-log-page:0x{0:x},{1:x}'.format(page, subpage),
  #}
  src_prefix = 'scsi-log-page:0x{0:x},{1:x}'.format(page, subpage)

  for param in fetch_data['params']: #{
    src_label = src_prefix

    # :XXX: Log page 0 has no code nor offset for returned parameters
    code   = param.get('code',   None)
    offset = param.get('offset', None)
    if code is not None: #{
      src_label += ':0x{0:x}'.format(code)
    #}
    if offset is not None: #{
      src_label += ':{0:d}'.format(offset)
    #}

    labels = {
      'metric_source': src_label
    }

    new_metrics = parse_param( log_page, param,
                                labels    = labels,
                                # report_unknowns = True
                                verbosity = verbosity )

    parsed.extend( new_metrics )
  #}

  return parsed
#}

#############################################################################

t10_parser = {
  '00_00'   : log_page.parse_param,
  '00_ff'   : log_page.parse_param,

  # :NOTE: Subpage 0xff is handled in the top-level parser selection
  #        for all log pages
  '02_00'   : error_counter.parse_counter_param,
  '03_00'   : error_counter.parse_counter_param,

  '05_00'   : error_counter.parse_counter_param,
  '06_00'   : error_counter.parse_non_medium_param,

  '08_00'   : format_status.parse_param,
  #'0c_00'   : logical block provisioning

  '0d_00'   : environment.parse_temperature_param,
  '0d_01'   : environment.parse_env_report_param,
  '0d_02'   : environment.parse_env_limits_param,

  '0e_00'   : start_stop.parse_param,
  '0e_01'   : utilization.parse_param,

  #'0f_00'   : application client

  '10_00'   : self_test_result.parse_param,
  '11_00'   : solid_state_media.parse_param,

  '15_00'   : background_scan.parse_param,
  '15_01'   : pending_defects.parse_param,
  #'15_02'   : background operation

  '18_00'   : protocol_specific_port.parse_param,

  '19_00'   : general_stats.parse_param,
  # 0x19,01-1f  : group statistics and performance
  # 0x19,20     : cache memory statistics

  '1a_00'   : power_transition.parse_param,

  '2f_00'   : ie.parse_param,

  # 0x30,00/hitachi: performance counters
  '30_00'   : page_30.parse_param,

  # 0x37,00/hitachi: miscellaneous
  # 0x37,00/*      : cache
  '37_00'   : page_37.parse_param,

  '3e_00'   : factory.parse_param,

  'default' : default.parse_param,
}

# Page/subpage specific parsers }
#############################################################################
