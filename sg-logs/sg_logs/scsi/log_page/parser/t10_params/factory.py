#
# Parameter parsing for log page 0x3e,00 (factory)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

#######
# SCSI Table 302: Factory Log page (seagate notes)
#   0000h   Power-on (number of drive power-on minutes)
#   0008h   Time (in minutes) until the next scheduled interrupt for a SMART
#           measurement
#   *       Reserved
Map_factory_code = {
  0x0000    : {'suffix' : 'power_on',         'units':'hours',
                                              # minutes to hours
                                              'divisor': 60},
  0x0008    : {'suffix' : 'smart_next_test',  'units':'minutes'},
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0,
                            code_map       = None): #{
  """
    Parse a single parameter from the factory log page 0x3e, 00

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity
      [code_map = None] (dict)        If provided, an over-ride of the default
                                      T10 `Map_factory_code`;
                                      Entries should include 'units' along with
                                      'suffix' and/or 'type':
                                        suffix  will be added to metric_name
                                                (e.g. 'Read');
                                        type    will result in a metric_name
                                                of 'stats'
                                                (if no 'suffix' is provided)
                                                and a label of 'stat_type';

    Returns:
      (list)  A list of simplified metrics;
  """
  if code_map is None:  code_map = Map_factory_code

  metrics = []

  info = code_map.get( param['code'], None )
  if info is None and report_unknowns: #{:
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  if info is None:  return metrics

  suffix    = info.get('suffix', 'factory')
  stat_type = info.get('type',   None)
  units     = info.get('units',  None)

  metric_name   = suffix
  metric_value  = param['value']

  divisor = info.get('divisor', None)
  if divisor: #{
    # Convert the value using the provided divisor
    # Primarily used to convert a power-on value to hours for standardization
    metric_value /= divisor
  #}

  metric_labels = labels.copy()
  if stat_type: metric_labels.update({'stat_type': stat_type})
  if units:     metric_labels.update({'units'    : units})

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
