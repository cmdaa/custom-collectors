#
# Parameter parsing for log page 0x2f,00 (informational exceptions)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs.scsi.vendor  import ScsiVendor
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param
from sg_logs.scsi.log_page.parser.t10_params.environment \
                          import parse_and_add_temperature

# Hitachi IE codes
Map_ie_hitachi_code = {
  0x01  : { 'type': 'remaining reserve 1',            'units': 'count' },
  0x02  : { 'type': 'remaining reserve xor',          'units': 'count' },
  0x03  : { 'type': 'xor depletion',                  'units': 'count' },
  0x04  : { 'type': 'volatile memory backup failure', 'units': 'count' },
  0x05  : { 'type': 'wear indicator',                 'units': 'count' },
  0x06  : { 'type': 'wear indicator system',          'units': 'count' },
  0x07  : { 'type': 'channel hanges',                 'units': 'count' },
  0x08  : { 'type': 'flush scan failure',             'units': 'count' },
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x2f,00 (informational exceptions)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  vendor  = (log_page.src_drive.scsi_vendor
                if log_page.src_drive is not None
                else ScsiVendor.by_t10( 'UNKNOWN' ))
  value   = param['value_raw']
  smart   = (log_page.src_drive.smart
                if log_page.src_drive is not None
                else None)

  # assert( isinstance(value, bytearray) )
  nBytes  = len(value)

  if param['code'] == 0x00: #{
    #####################################################################
    #  Parameter Code 0x00 (General parameter, parameter data)
    #
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Informational Exception Additional Sense Code |
    #         | (ASC)                                         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Informational Exception Additional Sense Code |
    #         | Qualifier (ASCQ)                              |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Most Recent Temperature Reading               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Vendor HDA Temperature Trip Point             |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | Maximum Temperature                           |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | Vendor Specific                               |
    #    ...  |                                               |
    #     7   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    if nBytes > 2: #{

      asc   = value[0]
      ascq  = value[1]

      if smart and smart.enabled: #{
        smart.set_status_from_scsi_as( asc, ascq )
      #}

      ####################################################
      # Specific Information Exception values
      #
      metric_name   = 'environment'
      metric_labels = labels.copy()

      metric_labels.update({
        'scsi_asc'    : ('0x%02x' % (asc)),
        'scsi_ascq'   : ('0x%02x' % (ascq)),
      })

      # Most recent temperature reading
      metric_value  = value[2]
      metric_labels.update({'stat_type': 'most recent temperature'})
      parse_and_add_temperature( metric_value, metric_name, metric_labels,
                                 metrics )

      # Vendor HDA Temperature Trip point
      metric_value  = value[3]
      metric_labels.update({'stat_type': 'trip point temperature'})
      parse_and_add_temperature( metric_value, metric_name, metric_labels,
                                 metrics )

      # Maximum Temperature
      if nBytes > 4: #{
        metric_value  = value[4]
        metric_labels.update({'stat_type': 'maximum temperature'})
        parse_and_add_temperature( metric_value, metric_name, metric_labels,
                                   metrics )
      #}

      #if nBytes > 5: #{
      #  # Vendor specific
      ##}
    #}

  #######################################################################
  # Vendor specific
  #   From sg3_utils/src/sg_logs.c : show_ie_page()
  #
  elif vendor.id == ScsiVendor.HITACHI: #}{
    #####################################################################
    # Hitachi
    #
    #   Command codes
    #     0x01  : Remaining reserve 1
    #     0x02  : Remaining reserve XOR
    #     0x03  : XOR depletion
    #     0x04  : Volatile memory backup failure
    #     0x05  : Wear indicator
    #     0x06  : System area wear indicator
    #     0x07  : Channel hangs
    #     0x08  : Flash scan failure
    #
    #   SMART sense_code=0x%x sense_qualifier=0x%x threshold=%d%% trip=%d
    #
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Informational Exception Additional Sense Code |
    #         | (ASC)                                         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Informational Exception Additional Sense Code |
    #         | Qualifier (ASCQ)                              |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Threshold                                     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Trip                                          |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    metric_name   = 'stats'
    metric_labels = labels.copy()

    info = Map_ie_hitachi_code.get( param['code'], None )
    if info is None and report_unknowns: #{
      metrics = default_parse_param( log_page, param,
                                      labels          = labels,
                                      report_unknowns = report_unknowns,
                                      verbosity       = verbosity )
    #}

    if info is None:  return metrics

    stat_type = info['type']

    asc   = (value[0] if nBytes > 0 else 0)
    ascq  = (value[1] if nBytes > 1 else 0)

    if smart and smart.enabled: #{
      smart.set_status_from_scsi_as( asc, ascq )
    #}

    metric_labels.update({
      'scsi_asc'    : ('0x%02x' % (asc)),
      'scsi_ascq'   : ('0x%02x' % (ascq)),
      'stat_type'   : ('%s threshold' % (stat_type)),
      'units'       : info['units'],
    })

    if nBytes > 2: #{
      metric_value = value[2]

      metric_labels.update({
        'stat_type'   : ('%s threshold' % (stat_type)),
      })

      metrics.append({
        'name'  : metric_name,
        'labels': metric_labels.copy(),
        'value' : metric_value,
      })
    #}


    if nBytes > 3: #{
      metric_value = value[3]

      metric_labels.update({
        'stat_type'   : ('%s trip' % (stat_type)),
      })

      metrics.append({
        'name'  : metric_name,
        'labels': metric_labels.copy(),
        'value' : metric_value,
      })
    #}

  elif vendor.id == ScsiVendor.SMRTSTOR: #}{
    #####################################################################
    # :TODO: 0x2f,0 Vendor-specific (SmrtStor) Informational Exceptions
    #
    print('=== ie.parse_param(): SMRTSTOR, ignore code 0x%x:' %
          (param['code']), value)

  elif report_unknowns: #}{
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}
