#
# Parameter parsing for log page 0x15,00 (background medium scan)
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys

from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

# SCSI Table 280: Background Scan Status field
#   09-FFh  Reserved
Map_background_scan_status = {
  0x00: 'no scans active',
  0x01: 'background medium scan active',
  0x02: 'background pre-scan is active',
  0x03: 'background medium scan halted due to fatal error',
  0x04: 'background medium scan halted due to '
        + 'vendor-specific pattern of errors',
  0x05: 'background medium scan halted due to medium formatted without P-list',
  0x06: 'background medium scan halted - vendor-specific cause',
  0x07: 'background medium scan halted due to temperature out of allowed range',
  0x08: 'background medium scan halted, waiting for '
        + 'Background Medium Interval timer expiration',
}

# SCSI Table 282: Reassign Status field
#   9-Fh  Reserved
#
# Instead of the full text from Table 282 we use abbreviations of the
# "official" strings from Seagate for the Reassign Status field
#   https://github.com/Seagate/opensea-parser/blob/0461dbee8a818f901ad681861112c7592e6f67f3/src/CScsi_Background_Scan_Log.cpp#L245
#
Map_background_scan_reassign = {
  ###
  # No reassignment
  0x00: 'no reassignment',  # NOT part of the standard but observed

  ###
  # Reassignment pending
  0x01: 'reassignment pending',

  ###
  # Reassignment passed
  0x02: 'reassignment passed',

  ###
  # Reassignment failed
  0x04: 'reassignment failed',

  ###
  # Repaired (scrubbed)
  0x05: 'repaired (scrubbed)',

  ###
  # Reassign by command (valid data)
  0x06: 'reassign by command (valid data)',

  ###
  # Reassign by command (invalid data)
  0x07: 'reassign by command (invalid data)',

  ###
  # Reassign by command (failed)
  0x08: 'reassign by command (failed)',
}

def parse_param( log_page, param, labels,
                     report_unknowns= True,
                     verbosity      = 0): #{
  """
    Parse a single parameter from page 0x15,00 (background medium scan)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( isinstance(value, bytearray) )

  #
  # SCSI Table 277: Background Scan log page parameter codes
  #
  #   0000h           Background Scan Status parameter
  #   0001h - 0800h   Background Scan parameter
  #   8000h - AFFFh   Vendor specific
  #   *               Reserved
  #
  if param['code'] == 0x00: #{
    metrics = parse_status_param( log_page, param, labels,
                                    report_unknowns,
                                    verbosity )

  elif param['code'] >= 0x0001 and param['code'] <= 0x800: #}{
    metrics = parse_scan_param( log_page, param, labels,
                                  report_unknowns,
                                  verbosity )

  elif report_unknowns: # }{
    # reserved or vendor-specific parameter/defect
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}

def parse_status_param( log_page, param, labels,
                          report_unknowns= True,
                          verbosity      = 0,
                          status_map     = None): #{
  """
    Parse a status parameter (0x00) from page 0x15,00 (background medium scan)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity
      [status_map = None] (dict)      If provided, an over-ride of the default
                                      T10 `Map_background_scan_status`;

    Returns:
      (list)  A list of simplified metrics;
  """
  if status_map is None:  status_map = Map_background_scan_status

  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x00 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # Background scan status (0x00)
  #
  # SCSI Table 279: Background Scan Status parameter format (parameter data)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Accumulated Power On Minutes                  |
  #    ...  | (drive-based event "timestamp")               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Reserved                                      |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     5   | Background Scan Status                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     6   | Number of Background Scans Performed          |
  #     7   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     8   | Background Medium Scan Progress               |
  #     9   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     10  | Number of Background Medium Scans Performed   |
  #     11  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  if len(value) < 12: #{
    # Truncated response
    print('background_scan.parse_param(): '
            + 'expected parameter length >= 12, received %d:' %
          (len(value)), value, file=sys.stderr )
    return metrics
  #}

  # Power-on (drive-based timestamp, applies to all entries)
  #   converted to hours
  power_on = int.from_bytes( value[0:4], 'big' ) / 60

  #################################################################
  # Background medium scan (status, count)
  #   - status  ( byte  5 )
  #   - count   ( bytes 6,7 )
  #
  metric_labels = labels.copy()

  if value[5] in status_map: #{
    status = status_map[ value[5] ]

  else: #}{
    # reserved
    status = ('reserved [ 0x%x ]' % (value[5]))
  #}

  scan_count = int.from_bytes( value[6:8], 'big' )

  metric_labels.update({
    'power_on_hours': ('%.4f' % (power_on)), # drive-based timestamp
    'status'        : status,
    'units'         : 'count',
  })

  metrics.append({
    'name'  : 'background_scan',
    'labels': metric_labels,
    'value' : scan_count,
  })

  #################################################################
  # Background medium scan (progress, count)
  #   - progress ( bytes 8,9 )
  #   - count    ( bytes 10,11 )
  #
  #   Background Medium Scan Progress is returned as a numerator that has
  #   65,536 as it's denominator.
  #
  progress   = int.from_bytes( value[8:10], 'big' )
  scan_count = int.from_bytes( value[10:12], 'big' )

  progress   = (progress / 65536 if progress > 0 else 0)

  metric_labels = labels.copy()
  metric_labels.update({
    'power_on_hours': ('%.4f' % (power_on)), # drive-based timestamp
    'progress'      : ('%5.2f percent' % (progress * 100)),
    'units'         : 'count',
  })

  metrics.append({
    'name'  : 'background_medium_scan',
    'labels': metric_labels,
    'value' : scan_count,
  })

  return metrics
#}

def parse_scan_param( log_page, param, labels,
                        report_unknowns= True,
                        verbosity      = 0): #{
  """
    Parse a scan parameter (0x001 .. 0x800) from page 0x15,00
    (background medium scan)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] >= 0x0001 and param['code'] <= 0x800 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # Background scan parameter (0x0001 - 0x0800)
  #
  #   A Background Scan parameter (see table 281) describes a defect
  #   location on the medium that was encountered by background scanning.
  #
  # SCSI Table 281: Background Scan parameter format (parameter data)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Accumulated Power On Minutes                  |
  #    ...  |                                               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Reassign Status       | Sense Key             |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     5   | Additional Sense Code                         |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     6   | Additional Sense Code Qualifier               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     7   | Vendor-specific                               |
  #    .... |                                               |
  #     11  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     12  | Logical Block Address                         |
  #    .... |                                               |
  #     19  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  if len(value) < 20: #{
    # Truncated response
    print('background_scan.parse_param(): '
            + 'expected parameter length >= 20, received %d:' %
          (len(value)), value, file=sys.stderr )
    return metrics
  #}

  # Convert power-on minutes to hours for standardization
  power_on = int.from_bytes( value[0:4], 'big' ) / 60

  metric_labels   = labels.copy()
  reassign_status = value[4] >> 4
  if reassign_status in Map_background_scan_reassign: #{
    reassign_status = Map_background_scan_reassign[ reassign_status ]
  else: #}{
    # reserved
    reassign_status = ('reserved [ 0x%x ]' % (reassign_status))
  #}

  # :XXX: Exclude the vendor-specific bytes (7..11)
  lba = int.from_bytes( value[ 12:20 ], 'big' )

  metric_labels.update({
    'power_on_hours'  : ('%.4f' % (power_on)), # drive-based timestamp
    'lba'             : ('0x%08x' % (lba)),

    'reassign_status' : reassign_status,
    'scsi_sense_key'  : '0x%02x' % (value[4] & 0x0F),
    'scsi_asc'        : '0x%02x' % (value[5]),
    'scsi_ascq'       : '0x%02x' % (value[6]),

    'units'           : 'index',
  })

  metrics.append({
    'name'  : 'medium_defect',
    'labels': metric_labels,
    'value' : param['code'],
  })

  return metrics
#}
