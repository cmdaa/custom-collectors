#
# Parameter parsing for log page 0x15,01 (pending defects)
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
# Western Digital/HGST information from:
#   Ultastar HC310 Serial Attached SCSI Hard Disk Drive Specification
#     Revision 1.4, 29 August 2018
#     https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/product/data-center-drives/ultrastar-dc-hc300-series/product-manual-ultrastar-dc-hc310-sas-oem-spec.pdf
#
# sg3_utils:
#   src/sg_logs.c : show_pending_defects_page()
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys

from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

def parse_param( log_page, param, labels,
                     report_unknowns= True,
                     verbosity      = 0): #{
  """
    Parse a single parameter from page 0x15,01 (pending defects)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( isinstance(value, bytearray) )

  # SCSI Table 326: Pending Defects log page parameter codes
  #   0000h           Pending Defect count
  #   0001h - F000h   Pending Defect
  #   *               Reserved
  if param['code'] == 0x00: #{
    metrics = parse_count_param( log_page, param, labels,
                                    report_unknowns,
                                    verbosity )

  elif param['code'] >= 0x0001 and param['code'] <= 0xF000: #}{
    metrics = parse_defect_param( log_page, param, labels,
                                  report_unknowns,
                                  verbosity )

  elif report_unknowns: # }{
    # reserved or vendor-specific parameter/defect
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}

def parse_count_param( log_page, param, labels,
                          report_unknowns= True,
                          verbosity      = 0): #{
  """
    Parse a count parameter (0x00) from page 0x15,01 (pending defects)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x00 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # Pending Defect count (0x0000; 4-byte binary format list)
  #
  # SCSI Table 328: Pending Defect Count log parmaeter format (param data)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Pending Defect Count                          |
  #    ...  |                                               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  defect_count = 0

  if isinstance(value, bytearray): #{
    if len(value) < 4: #{
      # Truncated response
      print('pending_defect.parse_count_param(): '
              + 'expected parameter length >= 4, received %d:' %
            (len(value)), value, file=sys.stderr )
      return metrics
    #}

    defect_count = int.from_bytes( value[0:4], 'big' )

  else: #}{
    # The value has already been converted to an integer
    defect_count = value
  #}

  #################################################################
  metric_labels = labels.copy()

  metric_labels.update({
    'units' : 'count',
  })

  metrics.append({
    'name'  : 'pending_defect',
    'labels': metric_labels,
    'value' : defect_count,
  })

  return metrics
#}

def parse_defect_param( log_page, param, labels,
                        report_unknowns= True,
                        verbosity      = 0): #{
  """
    Parse a pending defect parameter (0x0001 .. 0xF000) from page 0x15,01
    (pending defects)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] >= 0x0001 and param['code'] <= 0xF000 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # Pending Defect (0x0001 - 0xF000; 20-byte binary format list)
  #
  # SCSI Table 329: Pending Defect parameter format (parameter data)
  #                 (updated according to sg3_utils)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Accumulated Power On Hours (4-bytes)          |
  #    ...  |                                               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Logical Block Address      (8-bytes)          |
  #    .... |                                               |
  #     11  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  # :NOTE: The size of Accumulated Power On Hours was initially inferred to be
  #        an 8-byte field according to the SCSI documentation:
  #         "A value of FFFF_FFFF_FFFF_FFFFh indicates that the accumulated
  #          power on hours value is unknown."
  #
  #        This suggested that the Accumulated Power On Hours was an 8-byte
  #        field.
  #
  #        However, according to sg3_utils:src/sg_logs.c
  #        show_pending_defects_page(), Accumulated Power On Hours is actually
  #        a 32-bit/4-byte field.
  #
  if len(value) < 12: #{
    # Truncated response
    print('pending_defect.parse_defect_param(): '
            + 'expected parameter length >= 12, received %d:' %
          (len(value)), value, file=sys.stderr )
    return metrics
  #}

  # Power-on hours : 4-bytes
  # LBA            : 8-bytes
  power_on = int.from_bytes( value[ 0:4  ], 'big' )
  lba      = int.from_bytes( value[ 4:12 ], 'big' )

  metric_labels   = labels.copy()

  metric_labels.update({
    'power_on_hours'  : str(power_on), # drive-based timestamp
    'lba'             : ('0x%08x' % lba),
    'units'           : 'index',
  })

  metrics.append({
    'name'  : 'pending_defect',
    'labels': metric_labels,
    'value' : param['code'],
  })

  return metrics
#}
