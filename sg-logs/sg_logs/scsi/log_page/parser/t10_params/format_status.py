#
# Parameter parsing for log page 0x08,00 (format status)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######

from sg_logs.utils        import all_byte
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []

  # SCSI Table 303: Format Status log page parameter codes
  #   0x0000      Format Data Out
  #   0x0001      Grown Defects During Certification
  #   0x0002      Total Blocks Reassigned During Format
  #   0x0003      Total New Blocks Reassigned
  #   0x0004      Power On Minutes Since Format
  #   0x0005-7fff Reserved
  #   0x8000-ffff Vendor Specific
  #
  # If a format operation has never been performed by the logical unit, then
  # the log parameter for each Format Status log parameter listed in table 303
  # is not defined by this standard. If a device server begins a format
  # operation, then the device server shall set each byte of the log parameter
  # data (i.e., bytes four to n of the log parameter), if any, to FFh for each
  # Format Status log parameter (e.g., if the PARAMETER LENGTH field is set to
  # 02h, then the log parameter data is set to FFFFh).
  #
  if   param['code'] == 0x0: #{
    metrics = parse_format_data_out_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif param['code'] == 0x1: #}{
    metrics = parse_grown_defects_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif param['code'] == 0x2: #}{
    metrics = parse_blocks_reassigned_total_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif param['code'] == 0x3: #}{
    metrics = parse_blocks_reassigned_new_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif param['code'] == 0x4: #}{
    metrics = parse_power_on_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif report_unknowns: # }{
    # reserved or vendor-specific parameter/defect
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}

def parse_format_data_out_param( log_page, param, labels,
                                  report_unknowns= True,
                                  verbosity      = 0): #{
  """
    Parse a format data out parameter (0x00) from page 0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x00 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # Format Data Out (0x0000; binary format list)
  #
  #
  # SCSI Table 305: Format Data Out log parmaeter format (param data)
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0000)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (binary format list)   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (n-3)                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Format Data Out                               |
  #    ...  |                                               |
  #     n   |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   If the most recent format operation failed or the information for a
  #   Format Status log parameter is not available, then the device server
  #   shall return FFh in each byte of the log parameter data (i.e., bytes four
  #   to n of the log parameter), if any, for the Format Status log parameter
  #   (e.g., if the PARAMETER LENGTH field is set to 04h, then the log
  #   parameter data shall be set to FFFF_FFFFh). The device server shall set
  #   each Format Status log parameter to be a multiple of four bytes.
  #
  #   After a successful format operation, the FORMAT DATA OUT field contains
  #   the FORMAT UNIT parameter list (table 38).
  #
  # SCSI Table 38: Format Unit parameter list
  #   | Parameter List Header             (table 39/40)
  #   | Initialization Pattern Descriptor (if any, table 42)
  #   | Defect List                       (if any)
  #
  ###
  # Parameter List Header:
  #   The short parameter list header (table 39) is used if the LONGLIST
  #   bit is set to zero in the FORMAT UNIT CDB.
  #
  #   :XXX: It's not clear how we will know whether this is a short or long
  #         parameter list header since we have no access to the initiating
  #         FORMAT UNIT command...
  #
  #   SCSI Table 39: Short parameter list header
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Reserved                    | Protection Usage|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | FOV | DPRY| DCRT| STPF| IP  | OBS | IMMD|vendr|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Defect List Length                            |
  #     3   |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     
  #   SCSI Table 40: Long parameter list header
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Reserved                    | Protection Usage|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | FOV | DPRY| DCRT| STPF| IP  | OBS | IMMD|vendr|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Reserved                                      |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | PI Information (0)    | Prot Interval Expnt   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Defect List Length                            |
  #    ...  |                                               |
  #     7   |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   FOV                 : Format Options Valid
  #   DPRY                : Disable Primary
  #   DCRT                : Disable Certification
  #   STPF                : Stop Format
  #   IP                  : Initilization Pattern
  #                           0 - no initialization pattern included
  #   IMMD                : Immediate
  #   Prot Interval Expnt : Protection Interval Exponent
  #                           For a type 2 or 3 protection information request
  #   
  ###
  # Initialization Pattern Descriptor:
  #
  #   SCSI Table 42: Initialization pattern descriptor
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Obsolete  | SI  | Reserved                    |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | Initialization Pattern Type                   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Initialization Pattern Length                 |
  #     3   |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Initialization Pattern                        |
  #    ...  |                                               |
  #     n   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   SCSI Table 43: Initialization pattern type
  #     00h         use a default initialization pattern
  #     01h         repeat the pattern specified in Initialization Pattern
  #     02h - 7fh   Reserved
  #     80h - ffh   Vendor specific
  #
  ###
  # Defect List / Address Descriptor:
  #
  #   :XXX: It's not clear how we will know the address descriptor format since
  #         we have no access to the initiating FORMAT UNIT or READ DEFECT DATA
  #         command...
  #
  #   SCSI Table 44: Address descriptor formats
  #     000b    Short block format address descriptor
  #     001b    Extended bytes from index address descriptor
  #     010b    Extended physical sector address descriptor
  #     011b    Long block format address descriptor
  #     100b    Bytes from index format address descriptor
  #     101b    Physical sector format address descriptor
  #     110b    Vendor-specific
  #     *       Reserved
  #
  if all_byte( value, 0xff ): #{
    # Format has either never been started, has been started and not completed,
    # or has failed.
    return metrics
  #}

  #################################################################
  # :XXX: For now, simply use the raw value.
  #
  metric_labels = labels.copy()

  metric_labels.update({
    'stat_type' : 'format data out',
    'value_raw' : value,
  })

  metrics.append({
    'name'  : 'format_status',
    'labels': metric_labels,
    'value' : -1,
  })

  return metrics
#}

def parse_grown_defects_param( log_page, param, labels,
                                  report_unknowns= True,
                                  verbosity      = 0): #{
  """
    Parse a grown defects parameter (0x01) from page 0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x01 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # SCSI Table 306: Grown Defects During Certification
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0001)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (binary format list)   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (0x8)                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Grown Defects During Certification            |
  #    .... |                                               |
  #     11  |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  # 
  #   If the most recent format operation failed or the information for a
  #   Format Status log parameter is not available, then the device server
  #   shall return FFh in each byte of the log parameter data (i.e., bytes four
  #   to n of the log parameter), if any, for the Format Status log parameter
  #   (e.g., if the PARAMETER LENGTH field is set to 04h, then the log
  #   parameter data shall be set to FFFF_FFFFh). The device server shall set
  #   each Format Status log parameter to be a multiple of four bytes.
  #
  #   After a successful format operation during which certification was
  #   performed, the GROWN DEFECTS DURING CERTIFICATION field shall indicate
  #   the number of defects detected as a result of performing the
  #   certification. The value in the GROWN DEFECTS DURING CERTIFICATION field
  #   count reflects only those defects detected and replaced during the
  #   successful format operation that were not already part of the PLIST or
  #   GLIST.
  #
  #   After a successful format operation during which certification was not
  #   performed, the GROWN DEFECTS DURING CERTIFICATION field shall be set to
  #   zero.
  #
  if all_byte( value, 0xff ): #{
    # Format has either never been started, has been started and not completed,
    # or has failed.
    return metrics
  #}

  defect_count = 0

  if isinstance(value, bytearray): #{
    if len(value) < 8: #{
      # Truncated response
      print('format_status.parse_grown_defects_param(): '
              + 'expected parameter length >= 8, received %d:' %
            (len(value)), value, file=sys.stderr )
      return metrics
    #}

    defect_count = int.from_bytes( value[0:8], 'big' )

  else: #}{
    # The value has already been converted to an integer
    defect_count = value
  #}

  #################################################################
  metric_labels = labels.copy()

  metric_labels.update({
    'stat_type' : 'grown defects during certification',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : 'format_status',
    'labels': metric_labels,
    'value' : defect_count,
  })

  return metrics
#}

def parse_blocks_reassigned_total_param( log_page, param, labels,
                                          report_unknowns= True,
                                          verbosity      = 0): #{
  """
    Parse a total blocks reassigned parameter (0x02) from page
    0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x02 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # SCSI Table 307: Total Blocks Reassigned During Format
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0002)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (binary format list)   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (0x8)                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Total Blocks Reassigned During Format         |
  #    .... |                                               |
  #     11  |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  # 
  #   If the most recent format operation failed or the information for a
  #   Format Status log parameter is not available, then the device server
  #   shall return FFh in each byte of the log parameter data (i.e., bytes four
  #   to n of the log parameter), if any, for the Format Status log parameter
  #   (e.g., if the PARAMETER LENGTH field is set to 04h, then the log
  #   parameter data shall be set to FFFF_FFFFh). The device server shall set
  #   each Format Status log parameter to be a multiple of four bytes.
  #
  #   The TOTAL BLOCKS REASSIGNED DURING FORMAT field contains the count of the
  #   total number of logical blocks that were reassigned during the most
  #   recent successful format operation.
  #
  if all_byte( value, 0xff ): #{
    # Format has either never been started, has been started and not completed,
    # or has failed.
    return metrics
  #}

  block_count = 0

  if isinstance(value, bytearray): #{
    if len(value) < 8: #{
      # Truncated response
      print('format_status.parse_block_reassigned_total_param(): '
              + 'expected parameter length >= 8, received %d:' %
            (len(value)), value, file=sys.stderr )
      return metrics
    #}

    block_count = int.from_bytes( value[0:8], 'big' )

  else: #}{
    # The value has already been converted to an integer
    block_count = value
  #}

  #################################################################
  metric_labels = labels.copy()

  metric_labels.update({
    'stat_type' : 'total reassigned during format',
    'units'     : 'blocks',
  })

  metrics.append({
    'name'  : 'format_status',
    'labels': metric_labels,
    'value' : block_count,
  })

  return metrics
#}

def parse_blocks_reassigned_new_param( log_page, param, labels,
                                          report_unknowns= True,
                                          verbosity      = 0): #{
  """
    Parse a new blocks reassigned parameter (0x02) from page
    0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x03 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # SCSI Table 308: Total New Blocks Reassigned
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0003)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (binary format list)   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (0x8)                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Total New Blocks Reassigned                   |
  #    .... |                                               |
  #     11  |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  # 
  #   If the most recent format operation failed or the information for a
  #   Format Status log parameter is not available, then the device server
  #   shall return FFh in each byte of the log parameter data (i.e., bytes four
  #   to n of the log parameter), if any, for the Format Status log parameter
  #   (e.g., if the PARAMETER LENGTH field is set to 04h, then the log
  #   parameter data shall be set to FFFF_FFFFh). The device server shall set
  #   each Format Status log parameter to be a multiple of four bytes.
  #
  #   The TOTAL NEW BLOCKS REASSIGNED field contains a count of the total
  #   number of logical blocks that have been reassigned since the completion
  #   of the most recent successful format operation.
  #
  if all_byte( value, 0xff ): #{
    # Format has either never been started, has been started and not completed,
    # or has failed.
    return metrics
  #}

  block_count = 0

  if isinstance(value, bytearray): #{
    if len(value) < 8: #{
      # Truncated response
      print('format_status.parse_block_reassigned_new_param(): '
              + 'expected parameter length >= 8, received %d:' %
            (len(value)), value, file=sys.stderr )
      return metrics
    #}

    block_count = int.from_bytes( value[0:8], 'big' )

  else: #}{
    # The value has already been converted to an integer
    block_count = value
  #}

  #################################################################
  metric_labels = labels.copy()

  metric_labels.update({
    'stat_type' : 'total new reassigned',
    'units'     : 'blocks',
  })

  metrics.append({
    'name'  : 'format_status',
    'labels': metric_labels,
    'value' : block_count,
  })

  return metrics
#}

def parse_power_on_param( log_page, param, labels,
                                          report_unknowns= True,
                                          verbosity      = 0): #{
  """
    Parse a power-on parameter (0x04) from page
    0x08,00 (format status)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( param['code'] == 0x04 )
  # assert( isinstance(value, bytearray) )

  ##########################################################################
  # SCSI Table 309: Power On Minutes Since Format
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0004)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (binary format list)   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (0x8)                        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Power On Minutes Since Format                 |
  #    .... |                                               |
  #     11  |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  # 
  #   If the most recent format operation failed or the information for a
  #   Format Status log parameter is not available, then the device server
  #   shall return FFh in each byte of the log parameter data (i.e., bytes four
  #   to n of the log parameter), if any, for the Format Status log parameter
  #   (e.g., if the PARAMETER LENGTH field is set to 04h, then the log
  #   parameter data shall be set to FFFF_FFFFh). The device server shall set
  #   each Format Status log parameter to be a multiple of four bytes.
  #
  #   The POWER ON MINUTES SINCE FORMAT field contains the unsigned number of
  #   usage minutes (i.e., minutes with power applied regardless of power
  #   state) that have elapsed since the most recent successful format
  #   operation.
  #
  if all_byte( value, 0xff ): #{
    # Format has either never been started, has been started and not completed,
    # or has failed.
    return metrics
  #}

  power_on_minutes = 0

  if isinstance(value, bytearray): #{
    if len(value) < 8: #{
      # Truncated response
      print('format_status.parse_power_on_param(): '
              + 'expected parameter length >= 8, received %d:' %
            (len(value)), value, file=sys.stderr )
      return metrics
    #}

    power_on_minutes = int.from_bytes( value[0:8], 'big' )

  else: #}{
    # The value has already been converted to an integer
    power_on_minutes = value
  #}

  # Convert minutes to fractional hours
  power_on_hours = (power_on_minutes / 60)

  #################################################################
  metric_labels = labels.copy()

  metric_labels.update({
    'stat_type' : 'power on since format',
    'units'     : 'hours',
  })

  metrics.append({
    'name'  : 'format_status',
    'labels': metric_labels,
    'value' : power_on_hours,
  })

  return metrics
#}
