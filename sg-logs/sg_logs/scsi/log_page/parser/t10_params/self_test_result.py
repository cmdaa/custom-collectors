#
# Parameter parsing for log page 0x10,00 (self test results)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

#######
# SCSI Table 342 Self-Test Results
#
#   0h    The self-test completed without error
#   1h    The background self-test was aborted by the application using a
#         SEND DIAGNOSTIC with the SELF-TEST CODE set to 100b
#         (abort background self-test);
#   2h    The self-test was aborted by the application using a method other
#         than SEND DIAGNOSTIC with a SELF-TEST CODE of 100b;
#   3h    An unknown error occurred while processing the self-test and the
#         device server was unable to complete the self-test;
#   4h    The self-test completed with a failure in a test segment but the
#         test segment that failed is unknown;
#   5h    The first segment of the self-test failed;
#   6h    The second segment of the self-test failed;
#   7h    Another second segment of the self-test failed and which test is
#         indicated by the SELF-TEST NUMBER field;
#   8-Eh  Reserved;
#   Fh    The self-test is in progress;
Map_self_test_results = {
  0x0: 'completed without error',
  0x1: 'background self-test aborted by application (SEND DIAGNOSTIC)',
  0x2: 'self-test aborted by application',
  0x3: 'unknown error, unable to complete self-test',
  0x4: 'completed with failure (unknown test segment)',
  0x5: 'first segment failed',
  0x6: 'second segment failed',
  0x7: 'another segment failed',

  0xf: 'self-test in progress',
}


# SCSI Table 188 Self-Test Code field
#
# 000b [0h] No testing performed
# 001b [1h] Background short self-test
# 010b [2h] Background extended self-test
# 011b [3h] Reserved
# 100b [4h] Abort background self-test
# 101b [5h] Foreground short self-test
# 110b [6h] Foreground extended self-test
# 111b [7h] Reserved
Map_self_test_codes = {
  0x0:  'no test',
  0x1:  'Background (short)',
  0x2:  'Background (extended)',
  0x3:  'Reserved',
  0x4:  'Background (aborted)',
  0x5:  'Foreground (short)',
  0x6:  'Foreground (extended)',
  0x7:  'Reserved',
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x10,00 (self-test results)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []
  metric_name   = 'self_test'
  metric_labels = labels.copy()
  metric_value  = param['value']
  value_raw     = param['value_raw']

  if param['code'] < 1 or param['code'] > 0x14: #{
    if report_unknowns: # }{
      # reserved or vendor-specific parameter/defect
      metrics = default_parse_param( log_page, param,
                                      labels          = labels,
                                      report_unknowns = report_unknowns,
                                      verbosity       = verbosity )
    #}

    return metrics
  #}

  #
  # SCSI Table 341: Self-test results log parameter format (parameter data)
  #
  #   Parameter Codes 0x0001-14
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Self-Test Code  |RSRVD| Self-Test Results     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | Self-Test Number                              |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Accumulated Power On Hours                    |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Address of First Failure                      |
  #    .... |                                               |
  #     11  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     12  | Reserved              | Sense Key             |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     13  | Additional Sense Code (ASC)                   |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     14  | Additional Sense Code Qualifier (ASCQ)        |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     15  | Vendor specific                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  self_test_index   = param['code']

  self_test_code    = value_raw[0] >> 5
  self_test_results = value_raw[0] & 0x0f
  self_test_number  = value_raw[1]
  power_on_hours    = int.from_bytes( value_raw[2:4],  byteorder='big' )
  first_failure     = int.from_bytes( value_raw[4:12], byteorder='big' )
  sense_key         = value_raw[12] & 0x0f
  asc               = value_raw[13]
  ascq              = value_raw[14]

  # :XXX: Should we skip this parameter if
  #         `self_test_code` is 0x00 (no test)?

  if value_raw[0] == 0x00: #{
    # :XXX: If byte[0] is fully 0, no test has ever been run:
    #
    #         Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #        Byte |     |     |     |     |     |     |     |     |
    #       ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #         0   | Self-Test Code  |RSRVD| Self-Test Results     |
    #       ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    #       We will call this 'no test run' but still include
    #       the verbage for 'self_test_code'
    #
    stat_type = 'no test run'

  elif self_test_results in Map_self_test_results: #}{
    stat_type = Map_self_test_results[ self_test_results ]

  elif report_unknowns: #}{
    # reserved
    stat_type = ('reserved [0x%x]' % ( self_test_results ))

  else: #}{
    return metrics
  #}

  code_str = ('%s [0x%x]' %
              ( (Map_self_test_codes[ self_test_code ]
                  if self_test_code in Map_self_test_codes
                  else 'unknown'),
                self_test_code))


  metric_value  = self_test_results

  metric_labels.update({
    'stat_type'       : stat_type,

    'self_test_index' : str(self_test_index),
    'self_test_code'  : code_str,
    'self_test_number': str(self_test_number),
    'power_on_hours'  : str(power_on_hours),
    'first_failure'   : str(first_failure),
    'scsi_sense_key'  : str(sense_key),
    'scsi_asc'        : str(asc),
    'scsi_ascq'       : str(ascq),

    'units'           : 'failure',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
