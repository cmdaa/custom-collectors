#
# Parameter parsing for log page 0x18,00 (protocol specific port)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
# SCSI Table 336: Phy Attached Device Type field
#   100-111b  Reserved
Map_phy_device_type = {
  0x00: 'no device attached',
  0x01: 'SAS/SATA device',
  0x02: 'expander device',
  0x03: 'expander device (fanout)',
}

# SCSI Table 337: Reason field definition (ATTACHED REASON/REASON)
#   0xA-F     Reserved
Map_phy_attach_reason = {
  0x0: 'unknown',
  0x1: 'power on',
  0x2: 'hard reset',
  0x3: 'smp phy control function',
  0x4: 'loss of dword sync',
  0x5: 'mux mix up',
  0x6: 'I_T nexus loss timeout for STP/SATA',
  0x7: 'break timeout timer expired',
  0x8: 'phy test function stopped',
  0x9: 'expander device reduced functionality',
}

# SCSI Table 338: Negotiated Logical Link Rate field
#   0x7     Reserved
#   0xD-F   Reserved
Map_negotiated_logical_link_rate = {
  0x0: 'phy enabled: unknown rate',
  0x1: 'phy disabled',
  0x2: 'phy enabled: speed negotiation failed',
  0x3: 'phy enabled: SATA spinup hold state',
  0x4: 'phy enabled: port selector',
  0x5: 'phy enabled: reset in progress',
  0x6: 'phy enabled: unsupported phy attached',

  0x7: 'reserved',

  0x8: '1.5 Gb/s',
  0x9: '3.0 Gb/s',
  0xa: '6.0 Gb/s',
  0xb: '12.0 Gb/s',
}

# String values taken from:
#   - sg3_utils/src/sg_logs.c : show_sas_phy_event_info()
#   - opensea-parser/src/CScsi_Protocol_Specific_Port_Log.cpp :
#       process_Event_Description()
#
#       https://github.com/Seagate/opensea-parser/blob/develop/src/CScsi_Protocol_Specific_Port_Log.cpp
#
Map_sas_event_src = {
  0x00: {'str': 'no event'},

  ###
  # value only (ignore thresholds) {
  0x01: {'str': 'invalid dwords',                         'is_error': True},
  0x02: {'str': 'running disparity errors',               'is_error': True},
  0x03: {'str': 'loss of dword sync',                     'is_error': True},
  0x04: {'str': 'phy reset problems',                     'is_error': True},
  0x05: {'str': 'elasticity buffer overflows',            'is_error': True},
  0x06: {'str': 'received errors',                        'is_error': True},
  0x07: {'str': 'invalid SPL packets',                    'is_error': True},
  0x08: {'str': 'loss of SPL packet sync',                'is_error': True},

  # 0x09-1f : reserved for phy layer-based phy events

  0x20: {'str': 'received address frame errors',          'is_error': True},
  0x21: {'str': 'transmitted abandon-class open_rejects'},
  0x22: {'str': 'received abandon-class open_rejects'},
  0x23: {'str': 'transmitted retry-class open_rejects'},
  0x24: {'str': 'received retry-class open_rejects'},
  0x25: {'str': 'received (waiting on partial) apis'},
  0x26: {'str': 'received (waiting on connection) apis'},
  0x27: {'str': 'transmitted breaks'},
  0x28: {'str': 'received breaks'},
  0x29: {'str': 'break timeouts',                         'is_error': True},
  0x2a: {'str': 'connections'},

  # 0x2b-2e : Below (value and thresholds)

  0x2f: {'str': 'persistent connections'},

  # 0x30-3f : reserved for sas arbitration-related phy information

  0x40: {'str': 'transmitted ssp frames'},
  0x41: {'str': 'received ssp frames'},
  0x42: {'str': 'transmitted ssp frame errors',           'is_error': True},
  0x43: {'str': 'received ssp frame errors',              'is_error': True},
  0x44: {'str': 'transmitted credit_blockeds'},
  0x45: {'str': 'received credit_blockeds'},

  # 0x46-4f : reserved for ssp-related phy events

  0x50: {'str': 'transmitted sata frames'},
  0x51: {'str': 'received sata frames'},
  0x52: {'str': 'sata flow control buffer overflows',     'is_error': True},

  # 0x53-5f : reserved for stp and sata-related phy events

  0x60: {'str': 'transmitted smp frames'},
  0x61: {'str': 'received smp frames'},

  # 0x62    : reserved for smp-related phy events

  0x63: {'str': 'received smp frame errors',              'is_error': True},

  # 0x64-6f : reserved for smp-related phy events

  # }
  ###
  # value and thresholds {
  0x2b: {'str'        : 'peak transmitted pathway blocks',
         'time'       : False,
         'show_thresh': True },
  0x2c: {'str'        : 'peak transmitted arbitration wait time',
         'time'       : True,     # < 0x8000 : us else     : ms
         'show_thresh': True },
  0x2d: {'str'        : 'peak arbitration time',
         'time'       : 'microseconds',
         'show_thresh': True },
  0x2e: {'str'        : 'peak connection time',
         'time'       : 'microseconds',
         'show_thresh': True },
  # }
  ###
  # all others:
  # 'unknown phy event source: %d, val=%u, thresh_val=%u'
}

def parse_sas_port_param( log_page, param, labels,
                                    report_unknowns= True,
                                    verbosity      = 0): #{
 #try: #{
  """
    parse a single parameter from page 0x18,00 (protocol specific port)
    when the protocol is sas (0x6)

    args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    return:
      (list)  a list of metric instances;
  """
  #
  # scsi table 334: protocol-specific port log parameter for sas
  #
  #  parameter code (relative target port identifier)
  #
  #  parameter data:
  #
  #     bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | reserved              | proto identifier (0x6)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | reserved                                      |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | generation code                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | number of phys                                |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    ...  | sas phy log descriptor(s)                     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics       = []

  data          = param['value_raw']
  metric_labels = labels.copy()

  relative_target_port = param['code']

  protocol = data[0] & 0xf
  # assert( protocol == 0x6 )

  gen_code = data[2]
  num_phys = data[3]

  # Meta-data (number of phys)
  metric_name = 'protocol_phys'
  meta_labels = metric_labels.copy()
  meta_labels.update({
    'generation_code' : ('0x%02x' % (gen_code)),
    'units'           : 'count',
  })
  metrics.append({
    'name'  : metric_name,
    'labels': meta_labels,
    'value' : num_phys,
  })

  max_offset = len(data)
  offset     = 4
  for idex in range( 0, num_phys ): #{
    #
    # scsi table 335: sas phy log descriptor
    #
    #     bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | reserved                                      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | phy identifier                                |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | reserved                                      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | sas phy log descriptor length (m - 3)         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   |rsrvd| atchd dev type  | attached reason       |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | last reset reason     | negotiated logical    |
    #         |                       | link rate             |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | reserved              | atchd initiator ports |
    #         |                       |-----+-----+-----+-----+
    #         |                       | ssp | stp | smp |rsrvd|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     7   | reserved              | atchd target ports    |
    #         |                       |-----+-----+-----+-----+
    #         |                       | ssp | stp | smp |rsrvd|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     8   | sas address (u64)                             |
    #    .... |                                               |
    #     15  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     16  | attached sas address (u64)                    |
    #    .... |                                               |
    #     23  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     24  | attached phy identifier                       |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     25  | reserved                                      |
    #    .... |                                               |
    #     31  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     32  | invalid dword count (u32)                     |
    #    .... |                                               |
    #     35  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     36  | running disparity error count (u32)           |
    #    .... |                                               |
    #     39  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     40  | loss of dword synchronization (u32)           |
    #    .... |                                               |
    #     43  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     44  | phy reset problem (u32)                       |
    #    .... |                                               |
    #     47  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     48  | reserved                                      |
    #     49  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     50  | phy event descriptor length                   |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     51  | number of phy event descriptors               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #    ...  | sas phy event descriptor(s)                   |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    if (offset+51) > max_offset: #{
      # Out-of-data
      break
    #}

    phy_id   = data[ offset + 1 ]
    spld_len = data[ offset + 3 ]

    if spld_len < 44: spld_len  = 48  # sas-1 and sas-1.1
    else:             spld_len += 4

    dev_type          = (data[ offset + 4 ] & 0x70) >> 4
    att_reason        =  data[ offset + 4 ] & 0x0f
    last_reset_reason = (data[ offset + 5 ] & 0xf0) >> 4
    neg_lgc_lnk_rate  =  data[ offset + 5 ] & 0x0f
    iport_mask        =  data[ offset + 6 ]
    tport_mask        =  data[ offset + 7 ]

    sas_addr          = int.from_bytes( data[ offset + 8:offset  + 16 ], 'big' )
    att_sas_addr      = int.from_bytes( data[ offset + 16:offset + 24 ], 'big' )
    att_phy_id        = data[ offset + 24 ]
    inv_dwords        = int.from_bytes( data[ offset + 32:offset + 36], 'big' )
    running_disparity = int.from_bytes( data[ offset + 36:offset + 40], 'big' )
    loss_dword_sync   = int.from_bytes( data[ offset + 40:offset + 44], 'big' )
    phy_reset_probs   = int.from_bytes( data[ offset + 44:offset + 48], 'big' )

    shared_labels = labels.copy()
    shared_labels.update({
      'stat_type'         : None,
      'units'             : 'count',

      'sas_phy_id'        : str( phy_id ),
      'sas_dev_type'      : (Map_phy_device_type[ dev_type ]
                              if dev_type in Map_phy_device_type
                              else ('unknown [ 0x%x ]' % dev_type)),
      'sas_attach_reason' : (Map_phy_attach_reason[ att_reason ]
                              if dev_type in Map_phy_attach_reason
                              else ('unknown [ 0x%x ]' % att_reason)),
      'sas_reset_reason'  : (Map_phy_attach_reason[ last_reset_reason ]
                              if dev_type in Map_phy_attach_reason
                              else ('unknown [ 0x%x ]' % last_reset_reason)),
      'sas_link_rate'     : (Map_negotiated_logical_link_rate[neg_lgc_lnk_rate]
                              if dev_type in Map_negotiated_logical_link_rate
                              else ('unknown [ 0x%x ]' % neg_lgc_lnk_rate)),
      'sas_iport_mask'    : ('0x%02x : ssp[ %d ], stp[ %d ], smp[ %d ]' %
                              (iport_mask,
                               iport_mask >> 3 & 0x1,
                               iport_mask >> 2 & 0x1,
                               iport_mask >> 1 & 0x1) ),
      'sas_tport_mask'    : ('0x%02x : ssp[ %d ], stp[ %d ], smp[ %d ]' %
                              (tport_mask,
                               tport_mask >> 3 & 0x1,
                               tport_mask >> 2 & 0x1,
                               tport_mask >> 1 & 0x1) ),
      'sas_addr'          : ('0x%08x' % (sas_addr)),
      'sas_attached_addr' : ('0x%08x' % (att_sas_addr)),
      'sas_attached_phyid': ('0x%08x' % (att_phy_id)),
    })

    # Invalid dwords
    metric_name   = 'protocol_error'
    metric_labels = shared_labels.copy()
    metric_labels.update({
      'stat_type' : 'invalid dwords',
    })
    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : inv_dwords,
    })

    # Running disparity errors
    metric_name   = 'protocol_error'
    metric_labels = shared_labels.copy()
    metric_labels.update({
      'stat_type' : 'running disparity errors',
    })
    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : running_disparity,
    })

    # Loss of dword sync
    metric_name   = 'protocol_error'
    metric_labels = shared_labels.copy()
    metric_labels.update({
      'stat_type' : 'loss of dword sync',
    })
    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : loss_dword_sync,
    })

    # Phy reset problems
    metric_name   = 'protocol_error'
    metric_labels = shared_labels.copy()
    metric_labels.update({
      'stat_type' : 'phy reset problems',
    })
    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : phy_reset_probs,
    })

    if spld_len > 51: #{
      # there is 1 or more phy event descriptor
      #
      # scsi table 339: phy event descriptor
      #
      #     bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
      #    byte |     |     |     |     |     |     |     |     |
      #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
      #     0   | reserved                                      |
      #    .... |                                               |
      #     2   |                                               |
      #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
      #     3   | phy event source                              |
      #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
      #     4   | phy event                                     |
      #    .... |                                               |
      #     7   |                                               |
      #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
      #     8   | peak value detector threshold                 |
      #    .... |                                               |
      #     11  |                                               |
      #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
      #
      #
      # :xxx: why does sg3_utils ignore 'phy event descriptor length'?
      siz_ped = data[ offset + 50 ] # assert( siz_ped == 12 )
      num_ped = data[ offset + 51 ]

      #print('        : %d phy event%s' %
      #      (num_ped, ('s' if num_ped != 1 else '')) )

      for jdex in range( 0, num_ped ): #{
        event_off = offset + 52 + (jdex * 12)
        if (event_off+12) > max_offset: #{
          # Out-of-data
          break
        #}

        event_src = data[ event_off + 3 ]
        value     = int.from_bytes( data[ event_off + 4:event_off + 8 ], 'big' )
        pvdt      = int.from_bytes( data[ event_off + 8:event_off + 12 ],'big' )
        units     = 'count'

        metric_labels = shared_labels.copy()

        if event_src in Map_sas_event_src: #{
          src_info    = Map_sas_event_src[event_src]
          metric_name = 'protocol'
          if src_info.get('is_error', False): #{
            metric_name += '_error'
          #}

          stat_type   = src_info['str']

          if 'time' in src_info: #{
            # Time units

            if src_info['time'] is True: #{
              # value < 0x8000 (us), else (ms)
              units = ('microseconds' if value < 0x8000 else 'milliseconds')

            elif src_info['time'] is not False: #}{
              units = src_info['time']

            #}
          #}

          if 'show_thresh' in src_info: #{
            # Include a threshold label

            threshold = ('%u' % (pvdt))

            if src_info['time'] is True: #{
              # pvdt < 0x8000 (us), else (ms)
              threshold += (' (%s)' % ('microseconds' if pvdt < 0x8000
                                       else 'milliseconds'))

            elif src_info['time'] is not False: #}{
              threshold += (' (%s)' % (src_info['time']))

            #}

            metric_labels.update({
              'threshold': threshold
            })
          #}

        else: #}{
          stat_type = ('unknown phy event: %d, val=%u, thresh_val=%u' %
                        (event_src, value, pvdt))
        #}

        #print('        : %2d : phy event[ 0x%02x ] : %s' %
        #        (jdex, event_src, stat_type))

        metric_labels.update({
          'stat_type'           : stat_type,
          'sas_phy_event_index' : str(jdex),
          'units'               : units,
        })
        metrics.append({
          'name'  : metric_name,
          'labels': metric_labels,
          'value' : value,
        })

      #}
    #}

    offset += spld_len
  #}

  return metrics
 #except Exception as ex: #}{
 # import traceback
 # traceback.print_exception(type(ex), ex, ex.__traceback__)
 # return []
 ##}
#}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x18,00 (protocol specific port)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  #
  # SCSI Table 334: Protocol-Specific Port log parameter
  #
  #  Parameter Code (relative target port identifier)
  #
  #  Parameter data:
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Reserved              | Proto Identifier (0x6)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    ...  | Protocol-Specific parameter(s)                |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  metric_value = param['value_raw']
  protocol     = metric_value[0] & 0xf

  metrics = []
  if protocol == 0x6: #{
    metrics = parse_sas_port_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  elif report_unknowns: #}{
    ####################################################################
    # unsupported protocol
    #
    metric_name   = 'protocol'
    metric_labels = labels.copy()

    metric_labels.update({
      'protocol'  : protocol,
      'stat_type' : ('unsupported protocol [ 0x%x ]' % (protocol)),
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  return metrics
#}
