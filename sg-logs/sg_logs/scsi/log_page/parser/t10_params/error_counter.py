#
# Parameter parsing for error count log pages:
#   0x02,00 : write error counter
#   0x03,00 : read error counter
#   0x05,00 : verify error counter
#   0x06,00 : non-medium error counter
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
# SCSI Table 299: Error counter log page codes
#   0x2h    Write Error counter
#   0x3h    Read Error counter
#   0x5h    Verify Error counter
Map_error_page_name = {
  0x2 : 'write',
  0x3 : 'read',
  0x5 : 'verify',
}

# SCSI Table 300: Parameter codes for error counter log pages
#   0007-7FFFh  Reserved
#   8000-FFFFh  Vendor specific
Map_error_counter_code = {
  0x0000: {'type':'total corrected without substantial delay',
                                                          'units':'count'},
  0x0001: {'type':'total corrected with possible delay',  'units':'count'},
  0x0002: {'type':'total corrected by applying retries',  'units':'count'},
  0x0003: {'type':'total corrected',                      'units':'count'},
  0x0004: {'type':'total times correction algorithm processed',
                                                          'units':'count'},
  0x0005: {'type':'total bytes processed',                'units':'bytes',
                                                          'not_error': True},
  0x0006: {'type':'total uncorrected',                    'units':'count'},

  # Hitachi : sg3_utils/sg_logs.c:show_error_counter_page()
  0x8009: {'type':'track following errors',               'units':'count'},
  0x8015: {'type':'positioning errors',                   'units':'count'},
}

def parse_counter_param( log_page, param, labels,
                                    report_unknowns= True,
                                    verbosity      = 0): #{
  """
    Parse a single parameter from an error counter page 0x0[235], 00
      0x02,00   write error counter
      0x03,00   read error counter
      0x05,00   verify error counter

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []
  value   = param['value_raw']

  # assert( isinstance(value, bytearray) )

  # (write|read|verify)_error
  io_type     = Map_error_page_name[ log_page.page ]
  metric_name = ('%s_error' % (io_type))
  error_count = ( int.from_bytes( value, 'big' )
                    if isinstance( value, bytearray )
                    else value )

  metric_labels = labels.copy()

  if param['code'] in Map_error_counter_code: #{
    info = Map_error_counter_code[ param['code'] ]

    if info.get('not_error', False): #{
      # This is NOT an error, just a read/write/verify
      metric_name = io_type
    #}

    suffix    = info.get('suffix', None)
    stat_type = info.get('type',   None)
    units     = info.get('units',  'count')

    if suffix:    metric_name += '_'+ suffix
    if stat_type: metric_labels.update({'stat_type': stat_type})
    if units:     metric_labels.update({'units'    : units})


  elif report_unknowns: #}{
    # reserved or vendor-specific
    #
    # :NOTE: the parameter code SHOULD already be included in labels as
    #        'scsi_log_param_code')
    if param['code'] < 0x8000: #{
      metric_labels.update({'stat_type': 'reserved'})

    else: # }{
      metric_labels.update({'stat_type': 'vendor-specific'})
    #}

  else: #}{
    return metrics
  #}

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : error_count,
  })

  return metrics
#}

def parse_non_medium_param( log_page, param, labels,
                                        report_unknowns= True,
                                        verbosity      = 0): #{
  """
    Parse a single parameter from the non-medium error counter page 0x06, 00

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics   = []
  value     = param['value']
  stat_type = 'total error count'

  #
  # SCSI Table 325: Non-Medium Error Count log parameter (parameter data)
  #
  #   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #  Byte |     |     |     |     |     |     |     |     |
  # ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #   0   | Non-Medium Error Count                        |
  #  ...  |                                               |
  #  255  |                                               |
  # ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metric_name   = 'nonmedium_error'
  metric_labels = labels.copy()

  if param['code'] != 0x0000: #{
    # reserved or vendor-specific
    if not report_unknowns: #}{
      return metrics
    #}

    # :NOTE: the parameter code SHOULD already be included in labels as
    #        'scsi_log_param_code')
    if param['code'] >= 0x8000 and param['code'] <= 0xffff: #{
      # Vendor specific
      stat_type = 'vendor-specific'

    else: #}{
      # reserved
      stat_type = 'reserved'
    #}
  #}

  metric_labels.update({
    'stat_type' : stat_type,
    'units'     : 'count',
  })

  error_count = ( int.from_bytes( value, 'big' )
                    if isinstance( value, bytearray )
                    else value )

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : error_count,
  })

  return metrics
#}
