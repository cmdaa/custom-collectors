#
# Parameter parsing for log page 0x1a,00 (power condition transitions)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
# SCSI Table 330: Power Condition Transitions log page parameter codes
#   0x0000      Reserved
#   0x0005-0007 Reserved
#   0x000a-ffff Reserved
Map_power_transition_code = {
  0x0001:   {'type':'total transitions to active',    'units':'count'},
  0x0002:   {'type':'total transitions to idle_a',    'units':'count'},
  0x0003:   {'type':'total transitions to idle_b',    'units':'count'},
  0x0004:   {'type':'total transitions to idle_c',    'units':'count'},
  0x0008:   {'type':'total transitions to standby_z', 'units':'count'},
  0x0009:   {'type':'total transitions to standby_y', 'units':'count'},

  'default':{'type':'reserved',                       'units':'count'},
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0,
                            code_map       = None): #{
  """
    Parse a single parameter from page 0x1a,00 (power condition transitions)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity
      [code_map = None] (dict)        If provided, an over-ride of the default
                                      T10 `Map_power_transition_code`;

    Returns:
      (list)  A list of simplified metrics;
  """
  if code_map is None:  code_map = Map_power_transition_code

  metrics = []
  metric_name   = 'power'
  metric_labels = labels.copy()

  # SCSI Table 332: Power Condition Transitions log page parameter format
  #   All Power Condition Transitions log page counters are saturating
  #   counters (see table 270)
  #
  # SCSI Table 270 Parameter control byte values for unbounded data counter
  #                parameters
  #   Each unbounded data counter log parameter contains one or more
  #   saturating counters or wrapping counters.
  #
  metric_value  = ( int.from_bytes( param['value_raw'], 'big' )
                      if isinstance( param['value'], bytearray )
                      else param['value'] )

  info = None
  if param['code'] in code_map: #{
    info = code_map[ param['code'] ]

  elif report_unknowns: #}{
    # reserved or vendor-specific
    #
    # :NOTE: the parameter code SHOULD already be included in labels as
    #        'scsi_log_param_code')
    info = code_map['default']

  else: #}{
    return metrics
  #}

  suffix    = info.get('suffix', None)
  stat_type = info.get('type',   None)
  units     = info.get('units',  'count')

  if suffix:    metric_name += '_'+ suffix
  if stat_type: metric_labels.update({'stat_type': stat_type})
  if units:     metric_labels.update({'units'    : units})

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
