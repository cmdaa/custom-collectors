#
# Parameter parsing for log page 0x37,00:
#   0x37,00 : cache (common)
#   0x37,00 : miscellaneous (hitachi)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
# Western Digital/HGST information from:
#   Ultastar HC310 Serial Attached SCSI Hard Disk Drive Specification
#     Revision 1.4, 29 August 2018
#     https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/product/data-center-drives/ultrastar-dc-hc300-series/product-manual-ultrastar-dc-hc310-sas-oem-spec.pdf
#
# Additional information taken from:
#   sg3_utils:src/sg_logs.c
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys

from sg_logs.scsi.vendor  import ScsiVendor
from sg_logs.scsi.log_page.parser.t10_params.environment \
                          import parse_and_add_temperature
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

# SCSI Table 287: Cache Statistics parameter codes
#   0000h   Number of Logical Blocks sent to a SCSI initiator port
#   0001h   Number of Logical Blocks received from a SCSI initiator port
#   0002h   Number of Logical Blocks read from cache memory and sent to a
#           SCSI initiator port
#   0003h   Number of READ and WRITE commands that had data lengths equal or
#           less than the current segment size
#   0004h   Number of READ and WRITE commands that had data lengths greater
#           than the current segment size
#   *       Reserved
#
Map_cache_stats_code = {
  0x0000    : { 'suffix': 'read',                   'units': 'blocks' },
  0x0001    : { 'suffix': 'write',                  'units': 'blocks' },
  0x0002    : { 'suffix': 'cache_read',             'units': 'blocks' },
  0x0003    : { 'suffix': 'cache_io',
                'type'  : 'total (single segment)', 'units': 'commands' },
  0x0004    : { 'suffix': 'cache_io',
                'type'  : 'total (multi segment)',  'units': 'commands' },
}

def parse_cache_common( log_page, param, labels,
                                    report_unknowns= True,
                                    verbosity      = 0,
                                    code_map       = None): #{
  """
    Parse a single parameter from page 0x37,00 (cache statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity
      [code_map = None] (dict)        If provided, an over-ride of the default
                                      T10 `Map_cache_stats_code`;
                                      Entries should include 'units' along with
                                      'suffix' and/or 'type':
                                        suffix  will be added to metric_name
                                                (e.g. 'read');
                                        type    will result in a metric_name
                                                of 'cache'
                                                (if no 'suffix' is provided)
                                                and a label of 'stat_type';

    Returns:
      (list)  A list of simplified metrics;
  """
  if code_map is None:  code_map = Map_cache_stats_code

  metrics   = []
  value     = param['value']
  suffix    = 'cache'
  stat_type = None

  if param['code'] in code_map: #{
    info = code_map[ param['code'] ]

    suffix    = info.get('suffix', 'cache')
    stat_type = info.get('type',   None)

  elif report_unknowns: #}{
    # Reserved
    return default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}
  else: #}{
    return metrics
  #}

  metric_name   = suffix
  metric_value  = ( int.from_bytes( value, 'big' )
                      if isinstance( value, bytearray )
                      else value )

  # Assemble labels
  metric_labels = labels.copy()
  if stat_type is not None: #{
    # Stats : include 'stat_type'
    metric_labels.update({'stat_type':stat_type})
  #}
  metric_labels.update( {'units': info['units']} )

  if param['code'] == 0x0006: #{
    # 32-bit negative exponent, 32-bit integer time interval
    print('*** page_37.parse_cache_common: 32-bit negative exponent + integer:',
          value, file=sys.stderr)
  #}

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_misc_hitachi( log_page, param, labels,
                                    report_unknowns= True,
                                    verbosity      = 0): #{
  """
    Parse a single parameter from page 0x37,00 (miscellaneous (hitachi))


    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  #
  # HGST Table 120 Log Sense Page 37
  # sg3_utils:src/sg_logs.c : show_hgst_misc_page()
  #
  #   HGST/WDC miscellaneous page (0x30 bytes)
  #
  #   Parameter Code 0x00
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Power on hours (32-bit)                       |
  #      .... |                                               |
  #       3   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       4   | Total bytes read (64-bit)                     |
  #      .... |                                               |
  #       11  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       12  | Total bytes written (64-bit)                  |
  #      .... |                                               |
  #       19  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       20  | Max drive temp (degrees C)                    |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       21  | GList Size (16-bit)                           |
  #       22  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       23  | Number of Information Exceptions              |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       24  | MEDX| HDWX| Reserved                          |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       25  | Total Read Commands (64-bit)                  |
  #      .... |                                               |
  #       32  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       33  | Total Write Commands (64-bit)                 |
  #      .... |                                               |
  #       40  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       41  | Flash Correction Count (16-bit)               |
  #       42  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #
  #     'Number of Information Exceptions' gives the number of Information
  #       Exceptions during the life of the drive and not the number of
  #       Information Exceptions that have been reported. The number of
  #       reported Information Exceptions may be less due to the settings of
  #       Mode Page 0x1C.
  #
  #       :NOTE: This field does not include occurrences of any Information
  #              Exception Warnings.
  #
  #     'MEDX' (Media Exception) and 'HDWX' (Hardware Exception) bits indicate
  #       that an Information Exception has occurred during the life of the
  #       drive.  These flags are set during an Information Exception that may
  #       or may not coincide with the reporting of an Information Exception as
  #       mentioned above.
  #
  metrics = []
  if param['code'] != 0x00: #{
    if report_unknowns: #{
      metrics = default_parse_param( log_page, param,
                                      labels          = labels,
                                      report_unknowns = report_unknowns,
                                      verbosity       = verbosity )
    #}

    return metrics
  #}

  value   = param['value_raw']

  # assert( isinstance(value, bytearray) )
  nBytes  = len(value)

  #####################################################
  # 0..3  : Power on hours (32-bit)
  if nBytes > 3: #{
    metric_name   = 'power_on'
    metric_value  = int.from_bytes( value[0:4], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({'units': 'hours'})

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 4..11 : Total bytes read (64-bit)
  if nBytes > 11: #{
    metric_name   = 'read'
    metric_value  = int.from_bytes( value[4:12], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'total',
      'units'     : 'bytes',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 12..19: Total bytes written (64-bit)
  if nBytes > 19: #{
    metric_name   = 'write'
    metric_value  = int.from_bytes( value[12:20], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'total',
      'units'     : 'bytes',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 20    : Max drive temp (degress C)
  if nBytes > 20: #{
    metric_name   = 'environment'
    metric_value  = value[20]
    metric_labels = labels.copy()
    metric_labels.update({'stat_type': 'maximum'})

    parse_and_add_temperature( metric_value, metric_name, metric_labels,
                                 metrics )
  #}

  #####################################################
  # 21..22: GList Size (16-bit)
  if nBytes > 22: #{
    metric_name   = 'defects_growth'
    metric_value  = int.from_bytes( value[21:23], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'growth list',
      'units'     : 'count',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 23    : Number of Informational Exceptions
  # 24    : MEDX (bit 7), HDWX (bit 6)
  if nBytes > 23: #{
    metric_name   = 'informational_exception'
    metric_value  = value[23]

    metric_labels = labels.copy()
    metric_labels.update({'units': 'count'})

    # MEDX / HDWX
    flag_value = value[24]
    flag_strs  = []
    if flag_value & 0x80:   flag_strs.append( 'media' )
    if flag_value & 0x40:   flag_strs.append( 'hardware' )
    if len(flag_strs) < 1:  flag_strs.append( 'NONE' )
    metric_labels.update({
      'exception_flag': ('0x%02x [ %s ]' % (flag_value, ';'.join( flag_strs ))),
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 25..32: Total Read commands (64-bit)
  if nBytes > 31: #{
    metric_name   = 'read'
    metric_value  = int.from_bytes( value[25:33], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'total',
      'units'     : 'commands',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 33..40: Total Write commands (64-bit)
  if nBytes > 40: #{
    metric_name   = 'write'
    metric_value  = int.from_bytes( value[33:41], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'total',
      'units'     : 'commands',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  #####################################################
  # 41..42: Flash Correction Count (16-bit)
  if nBytes > 42: #{
    metric_name   = 'flash'
    metric_value  = int.from_bytes( value[41:43], 'big' )
    metric_labels = labels.copy()
    metric_labels.update({
      'stat_type' : 'total',
      'units'     : 'corrections',
    })

    metrics.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : metric_value,
    })
  #}

  return metrics
#}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x37,*
      0x37,00 : cache (common)
      0x37,00 : miscellaneous (hitachi)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  vendor = (log_page.src_drive.scsi_vendor
                if log_page.src_drive is not None
                else ScsiVendor.by_t10( 'UNKNOWN' ))

  if vendor.id == ScsiVendor.HITACHI: #{
    metrics = parse_misc_hitachi( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  else: #}{ elif vendor.id == ScsiVendor.SEAGATE: #}{
    metrics = parse_cache_common( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  #}

  return metrics
#}
