#
# Parameter parsing for log page 0x19,00 (general statistics and performance)
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
# Western Digital/HGST information from:
#   Ultastar HC310 Serial Attached SCSI Hard Disk Drive Specification
#     Revision 1.4, 29 August 2018
#     https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/product/data-center-drives/ultrastar-dc-hc300-series/product-manual-ultrastar-dc-hc310-sas-oem-spec.pdf
#
# Additional information taken from:
#   sg3_utils:src/sg_logs.c
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x19,00 (general statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []

  # HGST Table 115 Log Sense Page 19h - General Statistics and Performance
  #   0x0001  General Statistics
  #   0x0002  Idle Time Intervals (based upon parameter 0x0003)
  #   0x0003  Time Interval Descriptor (how LONG is a time interval)
  #
  # sg3_utils:
  #   0x0004  Force Unit Access (FUA) Statistics and performance
  #   0x0006  Time Intervals for cache stats (based upon parameter 0x0003)
  if param['code'] == 0x1: #{
    metrics = parse_general_stats_param( log_page, param, labels,
                                            report_unknowns, verbosity )
  elif param['code'] == 0x2: #{
    metrics = parse_idle_interval_param( log_page, param, labels,
                                            report_unknowns, verbosity )
  elif param['code'] == 0x3: #{
    metrics = parse_time_interval_param( log_page, param, labels,
                                            report_unknowns, verbosity )
  elif param['code'] == 0x4: #{
    metrics = parse_fua_stats_param( log_page, param, labels,
                                      report_unknowns, verbosity )
  elif param['code'] == 0x6: #{
    metrics = parse_time_interval_param( log_page, param, labels,
                                            report_unknowns, verbosity,
                                            interval_type = 'cache')
  elif report_unknowns: #}{
    # Reserved / unknown
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}

def parse_general_stats_param( log_page, param, labels,
                                report_unknowns= True,
                                verbosity      = 0): #{
  """
    Parse a single parameter (0x1) from page 0x19,00 (general statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  #
  # HGST Table 115 Log Sense Page 19h - General Statistics and Performance
  #
  #   Parameter Code 0x0001 (64-bytes of parameter data)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Number of Read Commands                       |
  #    .... |                                               |
  #     7   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     8   | Number of Write Commands                      |
  #    .... |                                               |
  #     15  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     16  | Number of Logical Blocks Received             |
  #    .... |                                               |
  #     23  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     24  | Number of Logical Blocks Transmitted          |
  #    .... |                                               |
  #     31  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     32  | Read Command Processing Intervals             |
  #    .... |                                               |
  #     39  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     40  | Write Command Processing Intervals            |
  #    .... |                                               |
  #     47  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     48  | Weighted Number of Read Commands plus         |
  #    .... |   Write Commands                              |
  #     55  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     56  | Weighted Read Command Processing plus         |
  #    .... |   Write Command Processing                    |
  #     63  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics = []
  value   = param['value_raw']

  ###############################################################
  # 0-7 : Read Commands
  metric_value  = int.from_bytes( value[0:8], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({'units': 'commands'})

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 8-16 : Write Commands
  metric_value  = int.from_bytes( value[8:16], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({'units': 'commands'})

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 16-23 : Logical Blocks Received
  metric_value  = int.from_bytes( value[16:24], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total logical blocks',
    'units'     : 'blocks',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 24-31 : Logical Blocks Transmitted
  metric_value  = int.from_bytes( value[24:32], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total logical blocks',
    'units'     : 'blocks',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 32-39 : Read Command Processing Intervals
  metric_value  = int.from_bytes( value[32:40], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 40-47 : Write Command Processing Intervals
  metric_value  = int.from_bytes( value[40:48], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 48-55 : Weighted Number of Read Commands plus Write Commands
  metric_value  = int.from_bytes( value[48:56], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'weighted total read commands plus write commands',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'io',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 56-63 : Weighted Read Command Processing plust Write Command
  #           Processing
  metric_value  = int.from_bytes( value[56:64], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'weighted read command processing '
                +     'plus write command processing',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'io',
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_idle_interval_param( log_page, param, labels,
                                report_unknowns= True,
                                verbosity      = 0): #{
  """
    Parse a single parameter (0x2) from page 0x19,00 (general statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  #
  # HGST Table 115 Log Sense Page 19h - General Statistics and Performance
  #
  #   Parameter Code 0x0002 (8-bytes of parameter data)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Idle Time Intervals                           |
  #    .... |                                               |
  #     7   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics = []

  value   = param.get('value_raw', None)
  if value is None: value = param.get('value', None)

  if isinstance(value, bytearray): #{
    metric_value  = int.from_bytes( value[0:8], 'big' )
  else: #}{
    metric_value  = value
  #}

  metric_labels = labels.copy()
  metric_labels.update({'units': 'intervals'})

  metrics.append({
    'name'  : 'idle',
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_time_interval_param( log_page, param, labels,
                                report_unknowns = True,
                                verbosity       = 0,
                                interval_type   = None): #{
  """
    Parse a single time interval parameter (0x3, 0x6) from page 0x19,00
    (general statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity
      [interval_type = None] (str)    If provided, the type of time interval
                                      (used as a metric name suffix);

    Returns:
      (list)  A list of simplified metrics;
  """
  #
  # HGST Table 115 Log Sense Page 19h - General Statistics and Performance
  # sg3_utils:src/sg_logs.c for Parameter Code 0x0006
  #
  #   Parameter Code 0x0003 : Time Interval Descriptor (seconds)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Exponent                                      |
  #    .... |   (negative power of 10)                      |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Integer                                       |
  #    .... |   (multiplied by exponent)                    |
  #     7   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics = []
  value   = param['value_raw']

  metric_name   = 'interval'
  if isinstance(interval_type, str):  metric_name += '_'+ interval_type

  exponent = int.from_bytes( value[0:4], 'big' )
  integer  = int.from_bytes( value[4:8], 'big' )

  metric_value  = (integer * pow(10, -exponent)) * 1000
  metric_labels = labels.copy()
  metric_labels.update({'units': 'milliseconds'})

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_fua_stats_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter (0x4) from page 0x19,00 (general statistics)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  # From sg3_utils:src/sg_logs.c
  #
  #   Parameter Code 0x0004: Force Unit Access (FUA) Statistics and performance
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Number of Read Commands                       |
  #    .... |                                               |
  #     7   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     8   | Number of Write Commands                      |
  #    .... |                                               |
  #     15  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     16  | Number of Read NV Commands                    |
  #    .... |                                               |
  #     23  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     24  | Number of Write NV Commands                   |
  #    .... |                                               |
  #     31  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     32  | Read Command Processing Intervals             |
  #    .... |                                               |
  #     39  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     40  | Write Command Processing Intervals            |
  #    .... |                                               |
  #     47  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     48  | Read NV Command Processing Intervals          |
  #    .... |                                               |
  #     55  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     56  | Write NV Command Processing Intervals         |
  #    .... |                                               |
  #     63  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics = []
  value   = param['value_raw']

  ###############################################################
  # 0-7 : Read Commands
  metric_value  = int.from_bytes( value[0:8], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA)',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 8-16 : Write Commands
  metric_value  = int.from_bytes( value[8:16], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA)',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 16-23 : Read NV Commands
  metric_value  = int.from_bytes( value[16:24], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) NV',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 24-31 : Write NV Commands
  metric_value  = int.from_bytes( value[24:32], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) NV',
    'units'     : 'commands',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 32-39 : Read Command Processing Intervals
  metric_value  = int.from_bytes( value[32:40], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 40-47 : Write Command Processing Intervals
  metric_value  = int.from_bytes( value[40:48], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 48-55 : Read NV Command Processing Intervals
  metric_value  = int.from_bytes( value[48:56], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) NV command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'read',
    'labels': metric_labels,
    'value' : metric_value,
  })

  ###############################################################
  # 56-63 : Weighted Read Command Processing plust Write Command
  #           Processing
  metric_value  = int.from_bytes( value[56:64], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total Force-Unit-Access (FUA) NV command processing',
    'units'     : 'intervals',
  })

  metrics.append({
    'name'  : 'write',
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
