#
# Parameter parsing for log page 0x11,00 (solid state media)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
# SCSI Table 343 Solid State Media log parameters
#   0x0000        Reserved
#   0x0002-0xffff Reserved
Map_solid_state_code = {
  0x0001: 'endurance indicator (used)',
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x11,00 (solid state media)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []
  metric_name   = 'solid_state_media'
  metric_labels = labels.copy()
  metric_value  = param['value_raw']

  #
  # SCSI Table 345: Percentage Used Endurance Indicator log parameter (data)
  #
  #   Parameter Code 0x0001
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Reserved                                      |
  #    .... |                                               |
  #     2   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Percentage Used Endurance Indicator           |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  if param['code'] in Map_solid_state_code: #{
    metric_labels.update({
      'stat_type' : Map_solid_state_code[ param['code'] ],
      'units'     : 'percent',
    })

    metric_value = metric_value[3]

  elif report_unknowns: #}{
    # Reserved / unknown
    # (the parameter code SHOULD already be included in labels as
    #   'scsi_log_param_code')
    metric_labels.update({ 'stat_type' : 'reserved' })

  else: #}{
    return metrics
  #}

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
