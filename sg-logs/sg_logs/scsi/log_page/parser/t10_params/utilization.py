#
# Parameter parsing for log page 0x0e,01 (utilization)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######

from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param


def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x0e,01 (utilization)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []

  # SCSI Table 353: Utilization log page parameter codes
  #   0x0000  Workload Utilization
  #   0x0001  Utilization Usage Rate Based on Date and Time
  #   *       Reserved
  #
  if   param['code'] == 0x0: #{
    metrics = parse_workload_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif param['code'] == 0x1: #}{
    metrics = parse_usage_param( log_page, param, labels,
                                                    report_unknowns,
                                                    verbosity )
  elif report_unknowns: # }{
    # reserved or vendor-specific parameter/defect
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}

  return metrics
#}

def parse_workload_param( log_page, param, labels,
                                  report_unknowns= True,
                                  verbosity      = 0): #{
  """
    Parse a workload utilization parameter (0x00) from page
    0x0e,01 (utilization)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []

  # assert( param['code'] == 0x00 )
  # assert( isinstance(param['value'], int) )

  ##########################################################################
  # Workload Utilization (0x0000; bounded data counter)
  #
  #
  # SCSI Table 355: Workload Utilization log parameter format
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Parameter Code (0x0000)                       |
  #     1   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Parameter Control Byte (bounded data counter) |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     3   | Parameter Length (2)                          |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Workload Utilization                          |
  #     5   |                                          (LSB)|
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  ###
  # SCSI Table 356 : Workload Utilization field
  #
  #   0 - 9,999       : Less than the designed workload has been utilized
  #                     (0.00 - 99.99%);
  #   10,000          : Exactly the designed workload for the device has
  #                     been utilized (100.00%);
  #   10,001 - 65,534 : Greater than the designed workload has been
  #                     utilized (100.01 - 655.34%);
  #   65,535          : Greater than the designed workload has
  #   
  value         = param['value']
  metric_labels = labels.copy()
  metric_value  = (value / 100)
  stat_type     = 'unknown'

  if   value <  10000: #{
    stat_type = 'within designed workload'

  elif value == 10000: #}{
    stat_type = 'at designed workload'

  elif value <  65535: #}{
    stat_type = 'exceeded designed workload'

  elif value == 65535: #}{
    stat_type = 'exceeded designed workload (data-limit)'

  else: #}{
    stat_type = ('unknown designed workload [ %d ]' % (value))
  #}

  metric_labels.update({
    'stat_type' : stat_type,
    'units'     : 'percent',
  })

  metrics.append({
    'name'  : 'utilization',
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_usage_param( log_page, param, labels,
                                  report_unknowns= True,
                                  verbosity      = 0): #{
  """
    Parse a date and time based utilization rate parameter (0x01) from page
    0x0e,01 (utilization)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics = []

  # assert( param['code'] == 0x01 )
  # assert( isinstance(param['value'], int) )

  ##########################################################################
  # SCSI Table 358 : Date and Time Based Utilization Rate field
  #
  #   0 - 99    : The Workload Utilization usage rate has been less than
  #               (i.e., 0% to 99% of) the designed usage rate during the
  #               interval that begins at the date and time of manufacture and
  #               ends at the timestamp
  #   100       : The Workload Utilization usage rate has been the exact
  #               designed usage rate during the interval that begins at the
  #               date and time of manufacture and ends at the timestamp
  #   101 - 254 : The Workload Utilization usage rate has been greater than
  #               (i.e., 101% to 254% of) the designed usage rate during the
  #               interval that begins at the date and time of manufacture and
  #               ends at the timestamp
  #   255       : The Workload Utilization usage rate has been greater than
  #               254% of designed usage rate during the interval that begins
  #               at the date and time of manufacture and ends at the timestamp
  #
  value         = ((param['value'] >> 8) & 0xff)
  metric_labels = labels.copy()
  metric_value  = value
  stat_type     = 'unknown'

  if   value <  100: #{
    stat_type = 'within designed usage rate'

  elif value == 100: #}{
    stat_type = 'at designed usage rate'

  elif value <  255: #}{
    stat_type = 'exceeded designed usage rate'

  elif value == 255: #}{
    stat_type = 'exceeded designed usage rate (data-limit)'

  else: #}{
    stat_type = ('unknown usage rate [ %d ]' % (value))
  #}

  metric_labels.update({
    'stat_type' : stat_type,
    'units'     : 'percent',
  })

  metrics.append({
    'name'  : 'utilization_rate',
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
