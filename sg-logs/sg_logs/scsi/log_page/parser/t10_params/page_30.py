#
# Parameter parsing for log page 0x30,00:
#   0x30,00 : performance counters (hitachi)
#
# Insights from sg3_utils:src/sg_logs.c
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs.scsi.vendor  import ScsiVendor
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

def parse_hitachi( log_page, param, labels,
                      report_unknowns= True,
                      verbosity      = 0): #{
  """
    Parse a single parameter from page 0x30,00 (performance counters (hitachi))


    From sg_logs, show_hgst_misc_page()

      HGST/WDC performance counters page (0x30 bytes)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  #  Parameter Code 0x00 (parameter data)
  #
  #    Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #   Byte |     |     |     |     |     |     |     |     |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    0   | Zero Seeks                                    |
  #    1   |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    2   | Seeks >= 2/3                                  |
  #    3   |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    4   | Seeks >= 1/3 and < 2/3                        |
  #    5   |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    6   | Seeks >= 1/6 and < 1/3                        |
  #    7   |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    8   | Seeks >= 1/12 and < 1/6                       |
  #    9   |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    10  | Seeks > 0 and < 1/12                          |
  #    11  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    12  | Reserved / Unknown                            |
  #   .... |                                               |
  #    15  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    16  | Overrun counter                               |
  #    17  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    18  | Underrun counter                              |
  #    19  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    20  | Device Cache Full Read Hits                   |
  #   .... |                                               |
  #    23  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    24  | Device Cache Partial Read Hits                |
  #   .... |                                               |
  #    27  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    28  | Device Cache Write Hits                       |
  #   .... |                                               |
  #    31  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    32  | Device Cache Fast Writes                      |
  #   .... |                                               |
  #    35  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    36  | Device Cache Read Misses                      |
  #   .... |                                               |
  #    39  |                                               |
  #  ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  metrics = []

  if param['code'] != 0x00: #{
    if report_unknowns: #{
      metrics = default_parse_param( log_page, param,
                                      labels          = labels,
                                      report_unknowns = report_unknowns,
                                      verbosity       = verbosity )
    #}

    return metrics
  #}

  value   = param['value_raw']

  # assert( isinstance(value, bytearray) )
  # assert( len(value) >= 0x30 )

  #####################################################
  # 0..1  : Zero Seeks
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[0:2], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks not required',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 2..3  : Seeks >= 2/3
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[2:4], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks >= 2/3 disk',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 4..5  : Seeks >= 1/3 and < 2/3
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[4:6], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks >= 1/3 and < 2/3 disk',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 6..7  : Seeks >= 1/6 and < 1/3
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[6:8], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks >= 1/6 and < 1/3 disk',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 8..9  : Seeks >= 1/12 and < 1/6
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[8:10], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks >= 1/12 and < 1/6 disk',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 10..11  : Seeks > 0 and < 1/12
  metric_name   = 'seek'
  metric_value  = int.from_bytes( value[10:12], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total seeks > 0 and < 1/12 disk',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 16..17  : Overruns
  metric_name   = 'cache'
  metric_value  = int.from_bytes( value[16:18], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total buffer overruns',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 18..19  : Underruns
  metric_name   = 'cache'
  metric_value  = int.from_bytes( value[18:20], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total buffer underruns',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 20..23  : Device Cache Full Read Hits
  metric_name   = 'cache_read'
  metric_value  = int.from_bytes( value[20:24], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total hits (full)',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 24..27  : Device Cache Partial Read Hits
  metric_name   = 'cache_read'
  metric_value  = int.from_bytes( value[24:28], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total hits (partial)',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 28..31  : Device Cache Write Hits
  metric_name   = 'cache_write'
  metric_value  = int.from_bytes( value[28:32], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total hits',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 32..35  : Device Cache Fast Writes
  metric_name   = 'cache_write'
  metric_value  = int.from_bytes( value[32:36], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total fast',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  #####################################################
  # 36..39  : Device Cache Read Misses
  metric_name   = 'cache_read'
  metric_value  = int.from_bytes( value[36:40], 'big' )
  metric_labels = labels.copy()
  metric_labels.update({
    'stat_type' : 'total misses',
    'units'     : 'count',
  })

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x37,*
      0x37,00 : cache (common)
      0x37,00 : miscellaneous (hitachi)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  vendor = log_page.src_drive.scsi_vendor

  if vendor.id == ScsiVendor.HITACHI: #{
    metrics = parse_hitachi( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  else: #}{
    # Fallback to the default parser
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  #}

  return metrics
#}
