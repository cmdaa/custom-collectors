#
# Parameter parsing for log page 0x0e,00 (start-stop cycle counter)
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
# SCSI Table 346: Start-Stop log parameter codes
#   0001h   Date of Manufacture
#   0002h   Accounting Date
#   0003h   Specified Cycle Count Over Device Lifetime
#   0004h   Accumulated Start-Stop Cycles
#   0005h   Specified Load-Unload Count Over Device Lifetime
#   0006h   Accumulated Load-Unload Cycles
#   *       Reserved
Map_start_stop_code = {
  0x0001: 'date of manufacture',
  0x0002: 'accounting date',
  0x0003: 'specified cycle count over device lifetime',
  0x0004: 'total start-stop cycles',
  0x0005: 'specified load-unload count over device lifetime',
  0x0006: 'total load-unload cycles',
}

def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from page 0x0e,00 (start-stop cycle counter)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []
  metric_name   = 'start_stop'
  metric_labels = labels.copy()
  metric_value  = param['value']
  value_raw     = param['value_raw']

  #
  # SCSI Table 347: Start-Stop Cycle Counter log page (parameter data)
  #
  #   Parameter Codes 0x0001-02 (dates)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Year (4 ASCII characters)                     |
  #    ...  |                                               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Week (2 ASCII characters)                     |
  #     5   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   All other Parameter Codes 0x0003-06 (cycle counters)
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | Count (4-byte binary number)                  |
  #    ...  |                                               |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  if param['code'] in Map_start_stop_code: #{
    stat_type = Map_start_stop_code[ param['code'] ]

  elif report_unknowns: #}{
    # reserved
    #
    # :NOTE: the parameter code SHOULD already be included in labels as
    #        'scsi_log_param_code')
    stat_type = 'reserved'

  else: #}{
    return metrics
  #}

  metric_labels.update({
    'stat_type' : stat_type,
    'units'     : 'count',
  })

  if   param['code'] <= 0x02: #{
    # Date (4-byte ascii year + 2-byte ascii week)
    metric_name   = 'drive'

    year = value_raw[0:4].decode('ascii').strip()
    week = value_raw[4:6].decode('ascii').strip()

    if len(year) == 4: #{
      year = int( year, base=10 )
      week = int( week, base=10 )

      metric_value = year * 100 + week
    else: #}{
      metric_value = -1
    #}

    metric_labels.update({
      'units' : 'year_week',
    })

  elif param['code'] <= 0x06: #}{
    # Counter (4-byte binary number)
    #assert( isinstance(param['value'], int) )
    if not isinstance( metric_value, int ): #{
      metric_value = int.from_bytes( value_raw[0:4], 'big' )
    #}

  elif report_unknowns: #}{
    metric_labels.pop('units')
    metric_labels.update({'value_raw': param['value_raw']})

    metric_value = param['code']

  else: #}{
    return metrics
  #}

  metrics.append({
    'name'  : metric_name,
    'labels': metric_labels,
    'value' : metric_value,
  })

  return metrics
#}
