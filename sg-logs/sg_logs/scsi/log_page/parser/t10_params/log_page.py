#
# Parameter parsing for log page lists:
#   0x00,00 : page list
#   0x00,ff : page/subpage list
#   *,ff    : supported subpages for the target page
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
def parse_param( log_page, param, labels,
                            report_unknowns= True,
                            verbosity      = 0): #{
  """
    Parse a single parameter from either page:
      0x00,00 (page list)
      0x00,ff (page/subpage list)
      *,ff    supported subpages for the target page

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  page    = param['page']
  subpage = 0x00
  if 'subpage' in param: #{
    subpage = param['subpage']
  #}

  db_labels = log_page.get_db_labels( page, subpage )

  # Include 'units'
  db_labels.update({ 'units': 'supported' })

  labels = labels.copy()
  labels.update( db_labels )

  metric = {
    'name'  : 'logpage',
    'labels': labels,
    'value' : 1,
  }

  return [ metric ]
#}
