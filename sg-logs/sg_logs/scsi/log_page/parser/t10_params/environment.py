#
# Parameter parsing for environmental log pages and parameters:
#   0x0d,00 : temperature
#   0x0d,01 : environmental reporting
#   0x0d,02 : environmental limits
#
#
# SCSI references are from:
#
#   Seagate SCSI Command Reference Manual: Fiber Channel & Serial Attach SCSI
#     100293068, Rev. K, Oct 2017
#     https://www.seagate.com/files/staticfiles/support/docs/manual/Interface%20manuals/100293068k.pdf
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
from sg_logs.scsi.log_page.parser.t10_params.default \
                          import parse_param as default_parse_param

def parse_and_add_temperature( value, prefix, labels, metrics, suffix=None ): #{
  """
    Given a temperature value, if the value is NOT 0xff (not reported),
    create a copy of the labels, update with 'celsius' units, create a new
    metric instance and append it to the incoming metrics list

    Args:
      value (int)     The incoming temperature value
      prefix (str)    The metric name prefix
      labels (dict)   The metric labels
      metrics (list)  The metrics list
      [suffix (str)]  If provided, a metric name suffix

    Returns:
      (list)  The updated `metrics`
  """

  name = prefix + '_temperature'
  if suffix is not None: name += '_'+ suffix

  if value < 0xff: #{
    labels = labels.copy()
    labels.update({'units': 'celsius'})

    metrics.append({
      'name'  : name,
      'labels': labels,
      'value' : value,
    })
  #}

  return metrics
#}

def parse_and_add_humidity( value, prefix, labels, metrics, suffix=None ): #{
  """
    Given a relative humidity value, if the value is value (<= 100),
    create a copy of the labels, update with 'percent' units, create a new
    metric instance and append it to the incoming metrics list

    Args:
      value (int)     The incoming humidity value
      prefix (str)    The metric name prefix
      labels (dict)   The metric labels
      metrics (list)  The metrics list
      [suffix (str)]  If provided, a metric name suffix

    Returns:
      (list)  The updated `metrics`
  """

  name = prefix + '_humidity'
  if suffix is not None: name += '_'+ suffix

  if value <= 100: #{
    labels = labels.copy()

    # :XXX: Should 'units' be 'relative humidity' instead of 'percent'?
    labels.update({'units': 'percent'})

    metrics.append({
      'name'  : name,
      'labels': labels,
      'value' : value,
    })
  #}

  return metrics
#}

# SCSI Table 351: Temperature log parameter codes
#   0000h   Temperature
#   0001h   Reference Temperature
#   *       Reserved
Map_temperature_code = {
  0x0000:'current',
  0x0001:'reference',
}

def parse_temperature_param( log_page, param, labels,
                                        report_unknowns= True,
                                        verbosity      = 0): #{
  """
    Parse a single parameter from page 0x0d,00 (temperature)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []
  metric_prefix = 'environment'
  metric_labels = labels.copy()
  metric_value  = param['value'][1]

  #
  # SCSI Table 352: Temperature log page (parameter data)
  #
  #   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #  Byte |     |     |     |     |     |     |     |     |
  # ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #   0   | Reserved                                      |
  # ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #   1   | Temperature (degrees C)                       |
  # ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  if param['code'] in Map_temperature_code: #{
    stat_type = Map_temperature_code[ param['code'] ]

  elif report_unknowns: #}{
    # reserved
    #
    # :NOTE: the parameter code SHOULD already be included in labels as
    #        'scsi_log_param_code')
    stat_type = 'reserved'

  else: #}{
    return metrics
  #}

  metric_labels.update({
    'stat_type' : stat_type,
  })

  parse_and_add_temperature( param['value'][1], metric_prefix, metric_labels,
                             metrics )
  return metrics
#}

def parse_env_report_param( log_page, param, labels,
                                        report_unknowns= True,
                                        verbosity      = 0): #{
  """
    Parse a single parameter from page 0x0d,01 (environmental reporting)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []

  # All generated metrics share this name
  metric_prefix = 'environment'

  #
  # SCSI Table 294: Environmental Reporting log page parameter codes
  #
  #                         : Parameter Codes
  # Temperature Report      : 0x0000 - 00ff
  # Relative Humidity Report: 0x0100 - 01ff
  # Reserved                : 0x0200 - ffff
  #
  if param['code'] >= 0x0000 and param['code'] <= 0x00ff: #{
    # Temperature Report log parameter
    #
    #   SCSI Table 296: Temperature Report log parameter (data)
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Reserved                                      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Temperature (degrees C)                       |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Lifetime Maximum Temperature (degrees C)      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Lifetime Minimum Temperature (degrees C)      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | Maximum Temperature Since Power on (degrees C)|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | Minimum Temperature Since Power on (degrees C)|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Reserved                                      |
    #     7   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    metric_labels = labels.copy()

    # Temperature
    metric_value = param['value'][1]
    metric_labels.update({'stat_type': 'current'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics )

    # Lifetime Maximum Temperature
    metric_value = param['value'][2]
    metric_labels.update({'stat_type': 'lifetime maximum'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics )

    # Lifetime Minimum Temperature
    metric_value = param['value'][3]
    metric_labels.update({'stat_type': 'lifetime minimum'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics )

    # Maximum Temperature Since Power on
    metric_value = param['value'][4]
    metric_labels.update({'stat_type': 'maximum since power on'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics )

    # Minimum Temperature Since Power on
    metric_value = param['value'][5]
    metric_labels.update({'stat_type': 'minimum since power on'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics )

  elif param['code'] >= 0x0100 and param['code'] <= 0x01ff: #}{
    # Relative Humidity Report log parameter
    #
    #   SCSI Table 297: Relative Humidity Report log parameter (data)
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Reserved                                      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Relative Humidity                             |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Lifetime Maximum Relative Humidity            |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Lifetime Minimum Relative Humidity            |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | Maximum Relative Humidity Since Power on      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | Minimum Relative Humidity Since Power on      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Reserved                                      |
    #     7   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    metric_labels = labels.copy()

    # Relative Humidity
    metric_value = param['value'][1]
    metric_labels.update({'stat_type': 'current'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels, metrics )

    # Lifetime Maximum Relative Humidity
    metric_value = param['value'][2]
    metric_labels.update({'stat_type': 'lifetime maximum'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels, metrics )

    # Lifetime Minimum Relative Humidity
    metric_value = param['value'][3]
    metric_labels.update({'stat_type': 'lifetime minimum'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels, metrics )

    # Maximum Relative Humidity Since Power on
    metric_value = param['value'][4]
    metric_labels.update({'stat_type': 'maximum since power on'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels, metrics )

    # Minimum Relative Humidity Since Power on
    metric_value = param['value'][5]
    metric_labels.update({'stat_type': 'minimum since power on'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels, metrics )

  elif report_unknowns: #}{
    # Reserved
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )

  #}


  return metrics
#}

def parse_env_limits_param( log_page, param, labels,
                                        report_unknowns= True,
                                        verbosity      = 0): #{
  """
    Parse a single parameter from page 0x0d,02 (environmental limits)

    Args:
      log_page (ScsiLogPage)          The log page instance that contains
                                      `param`
      param (dict)                    The log page parameter data to parse
      labels (dict)                   Additional labels to apply to each metric
      [report_unknowns = True] (bool) Should unknown parameters
                                      (e.g. vendor-specifc, reserved) be
                                      reported?
      [verbosity = 0] (int)           Debug verbosity

    Returns:
      (list)  A list of simplified metrics;
  """
  metrics       = []

  # All generated metrics share this name
  metric_prefix = 'environment'
  metric_suffix = 'limit'

  #
  # SCSI Environmental Limits log page parameter codes
  #
  #                         : Parameter Codes
  # Temperature Limits      : 0x0000 - 00ff
  # Relative Humidity Limits: 0x0100 - 01ff
  # Reserved                : 0x0200 - ffff
  #
  if param['code'] >= 0x0000 and param['code'] <= 0x00ff: #{
    # Temperature Limits log parameter
    #
    #   SCSI Table 291: Temperature Limits log parameter (data)
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | High Critical Temperature Limit Trigger       |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | High Critical Temperature Limit Reset         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Low  Critical Temperature Limit Reset         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Low  Critical Temperature Limit Trigger       |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | High Operating Temperature Limit Trigger      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | High Operating Temperature Limit Reset        |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Low  Operating Temperature Limit Reset        |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     7   | Low  Operating Temperature Limit Trigger      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    metric_labels = labels.copy()

    # High Critical Temperature Limit Trigger
    metric_value = param['value'][0]
    metric_labels.update({'stat_type': 'high critical trigger'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # High Critical Temperature Limit Reset
    metric_value = param['value'][1]
    metric_labels.update({'stat_type': 'high critical reset'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # Low  Critical Temperature Limit Reset
    metric_value = param['value'][2]
    metric_labels.update({'stat_type': 'low critical reset'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # Low  Critical Temperature Limit Trigger
    metric_value = param['value'][3]
    metric_labels.update({'stat_type': 'low critical trigger'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # High Operating Temperature Limit Trigger
    metric_value = param['value'][4]
    metric_labels.update({'stat_type': 'high operating trigger'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # High Operating Temperature Limit Reset
    metric_value = param['value'][5]
    metric_labels.update({'stat_type': 'high operating reset'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # Low  Operating Temperature Limit Reset
    metric_value = param['value'][6]
    metric_labels.update({'stat_type': 'low operating reset'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

    # Low  Operating Temperature Limit Trigger
    metric_value = param['value'][7]
    metric_labels.update({'stat_type': 'low operating trigger'})
    parse_and_add_temperature( metric_value, metric_prefix, metric_labels,
                               metrics, suffix=metric_suffix )

  elif param['code'] >= 0x0100 and param['code'] <= 0x01ff: #}{
    # Relative Humidity Limits log parameter
    #
    #   SCSI Table 291: Relative Humidity Limits log parameter (data)
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | High Critical Relative Humidity Limit Trigger |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | High Critical Relative Humidity Limit Reset   |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Low  Critical Relative Humidity Limit Reset   |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     3   | Low  Critical Relative Humidity Limit Trigger |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | High Operating Relative Humidity Limit Trigger|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | High Operating Relative Humidity Limit Reset  |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Low  Operating Relative Humidity Limit Reset  |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     7   | Low  Operating Relative Humidity Limit Trigger|
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    metric_labels = labels.copy()

    # High Critical Relative Humidity Limit Trigger
    metric_value = param['value'][0]
    metric_labels.update({'stat_type': 'high critical trigger'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # High Critical Relative Humidity Limit Reset
    metric_value = param['value'][1]
    metric_labels.update({'stat_type': 'high critical reset'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # Low  Critical Relative Humidity Limit Reset
    metric_value = param['value'][2]
    metric_labels.update({'stat_type': 'low critical reset'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # Low  Critical Relative Humidity Limit Trigger
    metric_value = param['value'][3]
    metric_labels.update({'stat_type': 'low critical trigger'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # High Operating Relative Humidity Limit Trigger
    metric_value = param['value'][4]
    metric_labels.update({'stat_type': 'high operating trigger'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # High Operating Relative Humidity Limit Reset
    metric_value = param['value'][5]
    metric_labels.update({'stat_type': 'high operating reset'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # Low  Operating Relative Humidity Limit Reset
    metric_value = param['value'][6]
    metric_labels.update({'stat_type': 'low operating reset'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

    # Low  Operating Relative Humidity Limit Trigger
    metric_value = param['value'][7]
    metric_labels.update({'stat_type': 'low operating trigger'})
    parse_and_add_humidity( metric_value, metric_prefix, metric_labels,
                            metrics, suffix=metric_suffix )

  elif report_unknowns: #}{
    # Reserved
    metrics = default_parse_param( log_page, param,
                                    labels          = labels,
                                    report_unknowns = report_unknowns,
                                    verbosity       = verbosity )
  #}


  return metrics
#}
