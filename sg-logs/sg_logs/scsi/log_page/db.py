#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import sys
import yaml
import re

from sg_logs.utils  import merge_dicts

class ScsiLogPageDb(): #{
  """
    A class that provides a database of SCSI LOG PAGE information that
    can be updated via a top-level YAML configuration file and is capable
    of identifying log page differences by SCSI drive vendor/model/firmware
  """
  def __init__( self, config_path = None ): #{
    """
      Create a new database instance.

      Args:
        [config_path = None] (str)  : The path to a YAML configuration file
                                      holding drive-specific log page details;

      The YAML file should contain a list of over-rides for default information
      where each over-ride has the form:
        { family (str)              : human-readable name;
          [vendor_regex = '*'] (str): regular expression matching SCSI vendor;
          [model_regex = '*'] (str) : regular expression matching SCSI model;
          [rev_regex = '*'] (str)   : regular expression matching SCSI firmware
                                      revision;
          page (dict)               : page-specific overrides keyed by page and
                                      having the form:
            { %subpage% : {[name] (str) : log page/subpage name over-ride,
                           level (int)  : reporting level over-ride,
                           cost (float) : log page/subpage cost over-ride },
              ...
            }
        }
    """
    self.db = [ Default ]

    if config_path: #{
      self.load_config( config_path )
    #}
  #}

  def add_entry( self, entry ): #{
    """
      Add a new entry to the log page database.

      Args:
        entry {dict}                Information about the entry to add;
        entry.family {str}          The name of the drive family represented by
                                    this new entry;
        entry.page {dict}           A map of page/subpage entries with the
                                    form:
                                      {
                                        0x01: {           # page    1
                                          0x02: {         # subpage 2
                                            'name' :'page/subpage name',
                                            'level':0,    # report level
                                            'cost' :0.25, # cost of fetching
                                                          # this page/subpage
                                                          # (0 == free,
                                                          #  1 == EXPENSIVE)
                                          },
                                          ...
                                        },
                                        ...
                                      }
        [entry.vendor_regex] {str}  A regular expression applied against the
                                    drive vendor;
        [entry.model_regex] {str}   A regular expression applied against the
                                    drive model;
        [entry.rev_regex] {str}     A regular expression applied against the
                                    drive firmware revision;

    """
    # assert( isinstance( family,   str ) )
    # assert( isinstance( page_map, dict ) )
    # assert( isinstance( drive_re, dict ) )
    #
    norm_entry = {
      'family'  : entry.get( 'family', '(family)'),
      'page'    : entry.get( 'page',   {} ),
    }

    # Process all optional regex fields
    for tgt in [ 'vendor', 'model', 'rev' ]: #{
      field               = ('%s_regex' % (tgt))
      src                 = entry.get( field, None )
      norm_entry[ field ] = _process_regex( src )
    #}

    self.db.append( norm_entry )
  #}

  def get_page_map( self, vendor, model, rev ): #{
    """
      Given a SCSI vendor, model, and firmware revision, return a map of
      page/subpage to information about each, including name, level and cost.

      Args:
        vendor (str)    : The target SCSI device vendor;
        model (str)     : The target SCSI device model;
        rev (str)       : The target SCSI device firmware revision;

      Returns:
        (dict | None)   : None if not found or a dictionary indexed by page
                          where each entry has the form:
                            { %subpage% : {
                                name : log page/subpage name (str);
                                level: log page/subpage report level (int);
                                cost : log page/subpage cost (float);
                              },
                              ...
                            }
    """
    #print('>>> ScsiLogPageDb.get_page_map( %s, %s, %s ) ...' %
    #      (vendor, model, rev))

    if (not isinstance(vendor, str) or
        not isinstance(model,  str) or
        not isinstance(rev,    str)): #{
      return None
    #}

    page_map = self.db[0].copy()
    pages    = page_map['page'].copy()

    l_vendor = vendor.lower()
    l_model  = model.lower()
    l_rev    = rev.lower()

    for idex in range( 1, len( self.db ) ): #{
      entry = self.db[idex]

      if entry['vendor_regex'] and not entry['vendor_regex'].match(l_vendor): #{
        continue
      #}
      if entry['model_regex'] and not entry['model_regex'].match(l_model): #{
        continue
      #}
      if entry['rev_regex'] and not entry['rev_regex'].match(l_rev): #{
        continue
      #}

      #print('>>> get_page_map(): Augment with:', entry)

      # Update the top-level information as well as the page set
      merge_dicts( page_map, entry )
      merge_dicts( pages, entry['page'] )
    #}

    # Put the final page set into place
    page_map['page'] = pages

    # And create a PageMap instance
    return ScsiLogPageMap( page_map )
  #}

  def load_config( self, config_path, verbosity=0 ): #{
    """
      Augment our current database with drive-specific log page details from
      the given YAML configuration file.

      Args:
        config_path (str)       The path to a YAML configuration file holding
                                drive-specific log page details;
        [verbosity = 0] (int)   Debug verbosity;

      Returns:
        (bool)  An indication of whether the file was loaded


      The YAML file should contain a list of over-rides for default information
      where each over-ride has the form:
        { family (str)              : human-readable name;
          [vendor_regex = '*'] (str): regular expression matching SCSI vendor;
          [model_regex = '*'] (str) : regular expression matching SCSI model;
          [rev_regex = '*'] (str)   : regular expression matching SCSI firmware
                                      revision;
          page (dict)               : page-specific overrides keyed by page and
                                      having the form:
            { %subpage% : {[name] (str) : name over-ride,
                           level (int)  : report level over-ride,
                           cost (float) : cost over-ride },
              ...
            }
        }
    """
    if not os.path.isfile( config_path ): #{
      print('*** ScsiLogPageDb.load_config( %s ): file does not exist' %
            (config_path), file=sys.stderr)
      return False
    #}

    if verbosity: #{
      print('>>> Load SCSI Log Page db: %s ...' % (config_path),
            file=sys.stderr)
    #}

    with open( config_path ) as file: #{
      src   = None
      field = None

      try: #{
        config = yaml.safe_load( file )

        for idex,entry in enumerate(config): #{
          src  = {'line': idex, 'entry': entry, 're': None}

          #print('>>> load_config: %d:' % (idex), entry)

          self.add_entry( entry )
        #}

      except Exception as ex: #}{
        if src is not None: #{
          entry = src['entry']

          if field is not None: #{
            print('*** ScsiLogPageDb: invalid config file [ %s ], '
                                        'family[ %s ], %s: "%s" :' %
                  (config_path, entry['family'], field, src['re']),
                  ex, file=sys.stderr)
          else: #}{
            print('*** ScsiLogPageDb: invalid config file [ %s ], family[ %s ]'%
                  (config_path, entry['family']),
                  ex, file=sys.stderr)
          #}

        else: #}{
          print('*** ScsiLogPageDb: invalid config file [ %s ]:' %
                (config_path), ex, file=sys.stderr)
        #}
      #}
    #}

    return True
  #}
#}

class ScsiLogPageMap(): #{
  """
    A class that provides a SCSI LOG PAGE map for a particular SCSI device.
  """
  def __init__( self, page_map ): #{
    """
      Create a new instance.

      Args:
        page_map (dict) : The raw page map data of the form:
          { family (str)              : human-readable name;
            [vendor_regex = None] (re): RegExp  matching SCSI vendor;
            [model_regex = None] (re) : RegExp  matching SCSI model;
            [rev_regex = None] (re)   : RegExp  matching SCSI firmware
                                        revision;
            page (dict)               : page-specific data keyed by page and
                                        having the form:
              { %subpage% : {
                  name (str)  : log page/subpage name;
                  level (int) : log page/subpage report level;
                  cost (float): log page/subpage retrieval cost (0..1);
                ...
              }
          }
    """
    # Validate 'page_map'
    if not isinstance( page_map, dict ): #{
      raise Exception( 'page_map MUST be a dict' )
    #}
    if not isinstance( page_map['page'], dict ): #{
      raise Exception( 'page_map.page MUST be a dict' )
    #}

    self.page_map = page_map
  #}

  def page_title( self, page, subpage = 0x00 ): #{
    """
      Retrieve the human readable title of the given page/subpage.

      Args:
        page (int)              : The target log page;
        [subpage = 0x00] (int)  : The target log subpage;

      Returns:
        (str)   : The human readable title of the given page/subpage;
    """
    title = None
    entry = None

    page_entry = self.page_map['page'].get( page, None )
    if page_entry: #{
      entry = page_entry.get( subpage, None )
    #}

    if entry: #{
      title = entry['name']

    else: #}{
      title = ('Log Page 0x%02x,%02x' % (page, subpage))

    #}

    return title
  #}

  def page_level( self, page, subpage = 0x00 ): #{
    """
      Retrieve the report level of the given page/subpage.

      Args:
        page (int)              : The target log page;
        [subpage = 0x00] (int)  : The target log subpage;

      Returns:
        (int) : The report level of the given page/subpage;
    """
    level = 0     # Default to "always"
    entry = None

    page_entry = self.page_map['page'].get( page, None )
    if page_entry: #{
      entry = page_entry.get( subpage, None )
    #}

    if entry: #{
      level = entry.get('level', level )

    #}

    return level
  #}

  def page_cost( self, page, subpage = 0x00 ): #{
    """
      Retrieve the retrieval cost of the given page/subpage.

      Args:
        page (int)              : The target log page;
        [subpage = 0x00] (int)  : The target log subpage;

      Returns:
        (float) : The retrieval cost of the given page/subpage;
    """
    cost  = 1.0   # Default to the highest cost
    entry = None

    page_entry = self.page_map['page'].get( page, None )
    if page_entry: #{
      entry = page_entry.get( subpage, None )
    #}

    if entry: #{
      cost = entry.get('cost', cost )

    #}

    return cost
  #}
#}


# The default SCSI LOG PAGE information
Default  = {
  'family':         'DEFAULTS',
  'vendor_regex':   None,
  'model_regex':    None,
  'firmware_regex': None,
  'warning':        'Default settings',
  #
  # The full set of known pages/subpages, names, and default cost
  #
  # Defaults are:
  #   name:   'Log Page 0x##,##'
  #   cost:   1.00
  #   level:  0
  #
  'page': {
    0x00: {
      0x00:       {'name': 'supported log pages',           'cost' : 0.25},
      0xff:       {'name': 'supported log pages/subpages',  'cost' : 0.25},
    },
    0x01: { 0x00: {'name': 'buffer over/under-run',         'cost' : 0.25}},
    0x02: { 0x00: {'name': 'write error counter',           'cost' : 0.25}},
    0x03: { 0x00: {'name': 'read error counter',            'cost' : 0.25}},
    0x05: { 0x00: {'name': 'verify error counter',          'cost' : 0.25}},
    0x06: { 0x00: {'name': 'non medium error counter',      'cost' : 0.25}},
    0x07: { 0x00: {'name': 'last n error counter',          'cost' : 0.25}},

    0x08: { 0x00: {'name': 'format status',                 'cost' : 0.50,
                                                            'level': 10 }},
    0x0b: {
      0x00:       {'name': 'last n deferred error counter', 'cost' : 0.25},
      0x01:       {'name': 'last n inquiry data changed',   'cost' : 0.25},
      0x02:       {'name': 'last n mode page data changed', 'cost' : 0.25},
    },
    0x0c: { 0x00: {'name': 'logical block provisioning',    'cost' : 0.25}},
    0x0d: {
      0x00:       {'name': 'temperature',                   'cost' : 0.25},
      0x01:       {'name': 'environmental reporting',       'cost' : 0.25},
      0x02:       {'name': 'environmental limits',          'cost' : 0.25},
    },
    0x0e: {
      0x00:       {'name': 'start-stop cycle counter',      'cost' : 0.25},
      0x01:       {'name': 'utilization',                   'cost' : 0.25},
    },
    0x0f: { 0x00: {'name': 'application client',            'cost' : 0.75,
                                                            'level': 50 }},
    0x10: { 0x00: {'name': 'self-test results',             'cost' : 0.25}},
    0x11: { 0x00: {'name': 'solid state media',             'cost' : 0.25}},
    0x14: { 0x00: {'name': 'zoned block device statistics', 'cost' : 0.25}},
    0x15: {
      0x00:       {'name': 'background scan results',       'cost' : 1.00},
      0x01:       {'name': 'pending defects',               'cost' : 1.00},
      0x02:       {'name': 'background operation',          'cost' : 1.00},
    },
    0x16: { 0x00: {'name': 'ata pass-through results',      'cost' : 0.25}},
    0x17: { 0x00: {'name': 'non-volatile cache',            'cost' : 0.25}},
    0x18: { 0x00: {'name': 'protocol specific port',        'cost' : 0.25}},
    0x19: {
      0x00:       {'name': 'general statistics and performance',
                                                            'cost' : 0.50},
      0x01:       {'name': 'group 0x01 statistics and performance',
                                                            'cost' : 0.50},
      0x02:       {'name': 'group 0x02 statistics and performance',
                                                            'cost' : 0.50},
      0x03:       {'name': 'group 0x03 statistics and performance',
                                                            'cost' : 0.50},
      0x04:       {'name': 'group 0x04 statistics and performance',
                                                            'cost' : 0.50},
      0x05:       {'name': 'group 0x05 statistics and performance',
                                                            'cost' : 0.50},
      0x06:       {'name': 'group 0x06 statistics and performance',
                                                            'cost' : 0.50},
      0x07:       {'name': 'group 0x07 statistics and performance',
                                                            'cost' : 0.50},
      0x08:       {'name': 'group 0x08 statistics and performance',
                                                            'cost' : 0.50},
      0x09:       {'name': 'group 0x09 statistics and performance',
                                                            'cost' : 0.50},
      0x0a:       {'name': 'group 0x0a statistics and performance',
                                                            'cost' : 0.50},
      0x0b:       {'name': 'group 0x0b statistics and performance',
                                                            'cost' : 0.50},
      0x0c:       {'name': 'group 0x0c statistics and performance',
                                                            'cost' : 0.50},
      0x0d:       {'name': 'group 0x0d statistics and performance',
                                                            'cost' : 0.50},
      0x0e:       {'name': 'group 0x0e statistics and performance',
                                                            'cost' : 0.50},
      0x0f:       {'name': 'group 0x0f statistics and performance',
                                                            'cost' : 0.50},

      0x10:       {'name': 'group 0x10 statistics and performance',
                                                            'cost' : 0.50},
      0x11:       {'name': 'group 0x11 statistics and performance',
                                                            'cost' : 0.50},
      0x12:       {'name': 'group 0x12 statistics and performance',
                                                            'cost' : 0.50},
      0x13:       {'name': 'group 0x13 statistics and performance',
                                                            'cost' : 0.50},
      0x14:       {'name': 'group 0x14 statistics and performance',
                                                            'cost' : 0.50},
      0x15:       {'name': 'group 0x15 statistics and performance',
                                                            'cost' : 0.50},
      0x16:       {'name': 'group 0x16 statistics and performance',
                                                            'cost' : 0.50},
      0x17:       {'name': 'group 0x17 statistics and performance',
                                                            'cost' : 0.50},
      0x18:       {'name': 'group 0x18 statistics and performance',
                                                            'cost' : 0.50},
      0x19:       {'name': 'group 0x19 statistics and performance',
                                                            'cost' : 0.50},
      0x1a:       {'name': 'group 0x1a statistics and performance',
                                                            'cost' : 0.50},
      0x1b:       {'name': 'group 0x1b statistics and performance',
                                                            'cost' : 0.50},
      0x1c:       {'name': 'group 0x1c statistics and performance',
                                                            'cost' : 0.50},
      0x1d:       {'name': 'group 0x1d statistics and performance',
                                                            'cost' : 0.50},
      0x1e:       {'name': 'group 0x1e statistics and performance',
                                                            'cost' : 0.50},
      0x1f:       {'name': 'group 0x1f statistics and performance',
                                                            'cost' : 0.50},

      0x20:       {'name': 'cache memory statistics',       'cost' : 0.50},
    },

    0x1a: { 0x00: {'name': 'power condition transitions',   'cost' : 0.25}},
    0x2f: { 0x00: {'name': 'informational exceptions',      'cost' : 0.25}},

    ###########################################################################
    # Vendor-specific (common) {
    #
    0x37: { 0x00: {'name': 'cache (common)',                'cost' : 0.25}},
    0x3e: { 0x00: {'name': 'factory (common)',              'cost' : 0.25}},
    # Vendor-specific (common) }
    ###########################################################################
  },
}

###############################################################################
# Private helpers {
#
def _process_regex( src ): #{
  res = None

  if src and src != '*': #{
    res = re.compile( src )
  #}

  return res
#}
