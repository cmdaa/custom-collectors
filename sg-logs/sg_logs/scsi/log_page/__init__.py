#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys
import collections
import re
import traceback

from sg_logs                import sg3utils
from sg_logs.metric         import Metric
from sg_logs.metric_source  import MetricSource

##
# A single parameter from a SCSI LOG SENSE page.
LogParam = collections.namedtuple( 'LogParam', [
                                    'code',
                                    'du',   # disable_update (bool)
                                    'tsd',  # target_save_disable (bool)
                                    'etc',  # enable_threshold_comparison (bool)
                                    'tmc',  # threshold_met_criteria (int)
                                    'fmt',  # format_and_linking (int)
                                    'value',# value (int | str)
                                    'name',
                                    'units',
                                   ] )

class ScsiLogPage (MetricSource): #{
  """
    A class representing a single SCSI LOG SENSE page.
  """
  def __init__( self, src_drive, page, subpage,
                        title = None, level = 0, cost = 1.0 ): #{
    """
      Create a new instance.

      Args:
        src_drive (ScsiDrive) : The source drive for accessing the log
                                page/sub-page;
        page (int)            : The log page code;
        subpage (int)         : The log sub-page code;
        [title = None] (str)  : The title of this log page/subpage;
        [level = 0] (int)     : Reporting level
                                (0  : always report,
                                 >0 : report only if the
                                      application level report level has been
                                      elevated to at least the same level);
        [cost = 1.0] (float)  : The cost of retrieving this page/subpage
                                where 0 is free and 1 is very expensive;
    """
    self.page    = page
    self.subpage = subpage
    self.name    = '0x%02x,%02x' % (page,subpage)

    if not title: #{
      # Generate a default title
      title = ('LogPage[%s]' % (self.name))
    #}
    super().__init__( src_drive, title, level, cost )
  #}

  def __repr__( self ): #{
    """
      Generate a string representation of this instance.

      Returns:
        (str)
    """
    return ('%s(src_drive=%s, page=0x%02x, subpage=0x%02x, title=%s, '
                    'level=%s, cost=%s)' %
              (type(self).__name__,
               repr(self.src_drive),
               self.page,
               self.subpage,
               repr(self.title),
               repr(self.level),
               repr(self.cost)))
  #}

  def fetch( self, verbosity=0, no_extension=False ): #{
    """
      Fetch the raw data from this SCSI LOG SENSE page/subpage

      Args:
        [verbosity = 0] (int)         The debug verbosity;
        [no_extension = False] (bool) If true, do NOT attempt to pass this off
                                      to an extension. This is most likly a
                                      call FROM an extension that is
                                      falling back to the non-extension method;

      The returned dict will have the form:
        { page          : (int),  # Page Code
          subpage       : (int),  # Subpage Code
          disable_save  : (bool), # DS
          subpage_format: (int),  # SPF
          params        : (list), # Log parameters
        }

      Each entry from `params` will have the form:
        { code      : (int),                    # Parameter Code
          offset    : (int),                    # Parameter Offset

          value_raw : (bytearray),              # The raw, uninterpreted bytes
                                                # of the parameter value

          value     : (str | int | bytearray),  # Parameter value interpreted
                                                # according to
                                                # `format_and_linking`

          # Extracted from the Parameter Control Byte {
          disable_update              : (bool), # DU
          target_save_disable         : (bool), # TSD
          enable_threshold_comparison : (bool), # ETC

          threshold_met_criteria      : (int),  # TMC
          # 0x00  every update of the cumulative value;
          # 0x01  Cumulative value  equal to      threshold value;
          # 0x02  Cumulative value  not equal to  threshold value;
          # 0x03  Cumulative value  greater than  threshold value;

          format_and_linking          : (int),
          # 0x00  Bounded data counter
          # 0x01  ASCII format list
          # 0x02  Bounded data counter or unbounded data counter
          # 0x03  Binary format list
          # Extracted from the Parameter Control Byte }
        }

      Returns:
        (dict | None)  : The retrived LOG SENSE data
    """
    metric_data = None

    #
    # First, give our super-class a shot in case there is an extension in
    # play
    #
    metric_data = super().fetch( verbosity, no_extension )
    if metric_data is not None: #{
      # Fetch was handled by an extension
      return metric_data
    #}

    ##################################################################
    # Fall-back to a SCSI standards-based data fetch via sg3utils
    #
    if verbosity: #{
      print('>>> %s.fetch(): src_drive[ %s ], fallback to SCSI standards' %
            (type(self).__name__,
             self.src_drive),
            file=sys.stderr)
    #}

    if self.src_drive.sg3 is None: return metric_data

    if verbosity: #{
      print('>>> %s.fetch(): invoke log_sense[ 0x%02x,%02x ] ...' %
            (type(self).__name__, self.page, self.subpage), file=sys.stderr)
    #}

    try: #{
      metric_data = sg3utils.log_sense( self.src_drive.sg3,
                                          page      = self.page,
                                          subpage   = self.subpage,
                                          verbosity = verbosity)

    except Exception as ex: #}{
      if verbosity: #{
        print('*** %s.fetch( page 0x%02x, subpage = 0x%02x): %s' %
              (type(self).__name__,
               self.page, self.subpage, ex), file=sys.stderr)
        traceback.print_exc( file=sys.stderr )
      #}
    #}

    return metric_data
  #}

  def parse( self, fetch_data,
                   verbosity      = 0,
                   report_level   = 0,
                   parse_unknowns = False,
                   no_extension   = False ): #{
    """
      Parse data from `fetch()` into a list of simplified metrics.

      Args:
        fetch_data (mixed)              Data from `fetch()`;
        [report_level = 0] (int)        Active report level;
        [verbosity = 0] (int)           The debug verbosity;
        [parse_unknowns = False] (bool) Should unknowns be parsed
                                        (e.g. parameters);

        [no_extension = False] (bool)   If true, do NOT attempt to pass this
                                        off to an extension. This is most likly
                                        a call FROM an extension that is
                                        falling back to the non-extension
                                        method;

      Returns:
        (list | None) : If the data is parsed, it should be returned as a list
                        of simplified metrics extracted from `fetch_data` where
                        each entry has the form:
                        { name    : (str),
                          value   : (int|float),
                          labels  : {
                            units :  (str),
                            ...
                          },
                          timestamp : (int) # Optional timestamp, milliseconds
                                            # since epoch;
                        }
    """
    #
    # First, give our super-class a shot in case there is an extension in play
    #
    parsed = super().parse( fetch_data, verbosity, report_level,
                            parse_unknowns, no_extension )
    if parsed is not None: #{
      # Parsing was handled by an extension
      return parsed
    #}

    ##################################################################
    # Fall-back to a SCSI T10 standard-based parse
    #
    if verbosity: #{
      print('>>> %s.parse(): src_drive[ %s ], fallback to T10 parser' %
            (type(self).__name__,
             self.src_drive),
            file=sys.stderr)
    #}

    from sg_logs.scsi.log_page.parser.t10 import parse_source as t10_parse

    return t10_parse( self, fetch_data, verbosity )
  #}

  def get_db_labels( self, page = None, subpage = None ): #{
    """
      Retrieve database-sourced labels. If this instance has a source drive
      with an associated SCSI Log Page database (src_drive.page_map), the
      values for these labels will be pulled from the database, otherwise
      defaults will be used.

      By default, the labels will be associated with *this* ScsiLogPage
      instance, but information about *other* SCSI Log Pages may be retrieved
      by providing explicit values for `page` and/or `subpage`.

      Args:
        [page = None] (int)     If not provided, use the `page` code for this
                                instance
        [subpage = None] (int)  If not provided, use the `subpage` code for
                                this instance

      Returns:
        (dict)    A dictionary containing database-sourced labels:
                    { 'log_page'      : (str),
                      'log_page_name' : (str),
                      'log_page_level': (int),
                      'log_page_cost' : (float),
                    }

    """
    if not isinstance( page,    int ): page    = self.page
    if not isinstance( subpage, int ): subpage = self.subpage

    log_page = '0x{0:x},{1:x}'.format( page, subpage )

    if self.src_drive and self.src_drive.page_map: #{
      page_map = self.src_drive.page_map

      name  = page_map.page_title( page, subpage )
      level = page_map.page_level( page, subpage )
      cost  = page_map.page_cost(  page, subpage )

    else: #}{
      name  = 'LogPage[0x{0:02x},{1:02x}]'.format( page, subpage )
      level = 0
      cost  = 1.0
    #}

    return {
      'log_page'      : log_page,
      'log_page_name' : name,
      'log_page_level': level,
      'log_page_cost' : cost,
    }
  #}
#}
