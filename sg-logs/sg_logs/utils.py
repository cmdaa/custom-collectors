#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import string

ascii_chars = set(bytes(string.printable, 'ascii'))

def get_contents( path ): #{
  """
    Read and return the contents of the given file/path

    Args:
      path (str)  The target file path (absolute);

    Returns:
      (str) The content of `path`;
  """
  contents = ''

  if os.path.exists( path ): #{
    with open( path, 'r' ) as fh: #{
      contents = fh.read()
    #}
  #}

  return contents
#}

def prhex( value, indent=' ', show_ascii=False ): #{
  """
    Given a bytearray, output it as a block of hex values

    Args:
      value (bytearray)           The byte array to print;
      [indent = ' '] (str)        The indent to use for each new line;
      [show_ascii = False] (bool) If true, show an ASCII representation of
                                  each line;
  """
  if not isinstance(value, bytearray): return

  bin   = []
  ascii = []

  print( end=indent )

  for idex,byte in enumerate(value): #{
    if idex % 16 == 0: #{
      if idex > 0: #{
        print('%s' % (' '.join(bin)), end='')
        if show_ascii: #{
          print(' : %s' % (''.join(ascii)), end='')
        #}

        print('\n', end=indent)
      #}

      bin   = []
      ascii = []

    elif idex % 8 == 0: #}{
      bin.append( ' ' )
      ascii.append( ' ' )

    #}

    bin.append( '%02x' % (byte) )
    if show_ascii: #{
      if byte >= 0x20 and byte <= 0x7e: #{
        ascii.append( chr( byte ) )
      else: #}{
        ascii.append( '.' )
      #}
    #}
  #}

  print('%s' % (' '.join(bin)), end='')

  if show_ascii: #{
    # len =   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
    fill   = '.. .. .. .. .. .. .. ..   .. .. .. .. .. .. .. ..'
    have   = len( bin )
    offset = have * 3
    if have < 8: offset -= 1
    else:        offset -= 2

    print('%s : %s' % (fill[ offset: ], ''.join(ascii)), end='')
  #}

  print()
#}

def all_ascii( data ): #{
  """
    Given an iterable array of bytes, determine if all bytes are within the
    ASCII range.

    Args:
      data (iterable)   The iterable array of bytes;

    Returns:
      (bool)            An indication of whether all bytes are in the ASCII
                        range;
  """
  return all(char in ascii_chars for char in data)
#}

def all_byte( data, match_byte ): #{
  """
    Given an iterable array of bytes, determine if all bytes are the given
    `match_byte`.

    Args:
      data (iterable)     The iterable array of bytes;
      match_byte (byte)   The byte to match;

    Returns:
      (bool)    An indication of whether all bytes are the given `match_byte`;
  """
  return all(iter_byte == match_byte for iter_byte in data)
#}

def json_default( obj ): #{
  """
    Invoked by json.dumps() for objects that cannot be serialized using the
    built-in methods.

    Args:
      obj (mixed)   The data to be serialized;

    Returns:
      (mixed)       A JSON encodable version of `obj`;
  """
  ret = obj

  if isinstance( obj, bytearray ): #{
    ret = list( obj )

  elif '__dict__' in obj: #}{
    ret = obj.__dict__

  #}

  return ret
#}

def json_encode( value, **kwargs ): #{
  """
    Encapsulate JSON encoding

    Args:
      value (mixed)   The raw value;
      [any args acceptable to json.dumps() excluding `default`]

    Returns:
      (str)           A JSON-encode string
  """
  import json

  return json.dumps( value, default=json_default, **kwargs )
#}

def merge_dicts(dst, src, path=None): #{
  """
    Recursivly merge two dictionaries, updating `dst` to include the contents
    of `src` with leaf values from `src` overriding `dst`.


    Args:
      dst (dict)      The destination dictionary (modified and returned);
      src (dict)      The source dictionary;

    Returns:
      (dict)          THe merged dictionary;
  """
  if path is None: path = []
  for key in src: #{
    if key in dst: #{
      if isinstance(dst[key], dict) and isinstance(src[key], dict): #{
        # Recurse
        merge_dicts(dst[key], src[key], path + [str(key)])
        continue
      #}
    #}

    # Either a key that does not exist in `dst` or has a non-dict value.
    dst[key] = src[key]
  #}
  return dst
#}
