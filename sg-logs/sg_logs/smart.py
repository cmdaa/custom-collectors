#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys

from sg_logs.metric_source  import MetricSource

##
# General SMART information
class Smart(MetricSource): #{
  """
    A class representing SMART information for a single drive.
  """
  def __init__( self, src_drive,
                      title     = 'SMART',
                      level     = 0,
                      cost      = 0.25,
                      capable   = False,
                      enabled   = False,
                      status    = 'UNKNOWN',
                      attrs     = None ): #{
    """
      Create a new instance.

      Args:
        src_drive (Drive)         : The source drive for accessing the SMART
                                    information;
        [title = 'SMART') (str)   : The title of this source;
        [level = 0] (int)         : Reporting level
                                    (0  : always report,
                                     >0 : report only if the application level
                                          report level has been elevated to at
                                          least the same level);
        [cost = 0.25] (float)     : The cost of retrieving data from this
                                    source where 0 is free and 1 is very
                                    expensive;
        [capable = False] (bool)  : Is `src_drive` SMART capable;
        [enabled = False] (bool)  : Is SMART enabled for `src_drive`;
        [status = 'UNKNOWN'] (str): The overall 'PASS' / 'FAIL' status;
        [attrs = None] (list)     : SMART-related attributes;

      `attrs` entries should be dicts with the form:
          name (str)        : attribute name;
          value (int)       : latest value;
          id (int)          : attribute id    [None];
          flags (dict)      : attribute flags [None];
          worst (int)       : worst value     [None];
          thresh (int)      : threshold value [None];
          type (str)        : attribute type  ['pre-fail' flags.prefailure
                                                          is True
                                               'old-age'  otherwise];
          updated (str)     : update timing   ['always'   flags.update_online
                                                          is True
                                               'offline'  otherwise];
          when_failed (time): failure time    [None];
          raw (bytes)       : raw data        [None];

      `attr.flags` should be a dict of the form:
          value (int)         : flag value;
          string (str)        : string representation;
          prefailure (bool)   : pre-failure condition?
          update_online (bool): can this attribute be updated while the drive
                                is on-line?
          performance (bool)  : has performance degraded?
          error_rate (bool)   : is there error rate information?
          event_count (bool)  : is there event count information?
          auto_keep (bool)    : is there event count information?
    """
    super().__init__( src_drive, title, level, cost )

    self.capable = capable
    self.enabled = enabled
    self.status  = status
    self.value   = -1       # :XXX: Should be set along with `status`
                            #       to -1 (UNKNOWN), 0 (OK), non-0 (FAILURE)
    self.attrs   = attrs

    self.update_value()
  #}

  def __repr__( self ): #{
    """
      Generate a string representation of this instance.

      Returns:
        (str)
    """
    return ('%s(src_drive=%s, title=%s, level=%s, cost=%s, '
                'capable=%s, enabled=%s, status=%s )' %
              (type(self).__name__,
               repr(self.src_drive),
               repr(self.title),
               repr(self.level),
               repr(self.cost),
               repr(self.capable),
               repr(self.enabled),
               repr(self.status)) )
  #}

  def get_attr( self, name ): #{
    """
      Retrieve the first attribute matching the given name

      Args:
        name (str)  The name of the target attribute

      Returns:
        (dict | None)   The matching attribute (dict) or None
    """
    return next((attr for attr in self.attrs if attr['name'] == name), None)
  #}

  def update_value( self ): #{
    """
      Update `value` based upon the current `status.

      Returns:
        (int)   The updated `value`
    """
    self.value = (0 if self.status == 'OK'
                    else 1)

    return self.value
  #}

  def update( self ): #{
    """
      Update SMART information, performing any required retrievals.

      Returns:
        (self)
    """
    return self
  #}

  #############################################################################
  # MetricSource method overrides {
  #
  def fetch( self, verbosity=0, no_extension=False ): #{
    """
      Fetch and return the raw metric data

      Args:
        [verbosity = 0] (int)         The debug verbosity;
        [no_extension = False] (bool) If true, do NOT attempt to pass this off
                                      to an extension. This is most likly a
                                      call FROM an extension that is
                                      falling back to the non-extension method;

      Returns:
        (Smart | None) : The updated instance
    """
    #
    # First, give our super-class a shot in case there is an extension in
    # play
    #
    metric_data = super().fetch( verbosity, no_extension )
    if metric_data is not None: #{
      # Fetch was handled by an extension
      return metric_data
    #}

    ##################################################################
    # Fall-back
    #
    if verbosity: #{
      print('>>> %s.fetch(): src_drive[ %s ], fallback to Smart.update()' %
            (type(self).__name__,
             self.src_drive),
            file=sys.stderr)
    #}

    return self.update()
  #}

  def parse( self, fetch_data,
                   verbosity      = 0,
                   report_level   = 0,
                   parse_unknowns = False,
                   no_extension   = False ): #{
    """
      Parse data from `fetch()` into a list of simplified metrics.

      Args:
        fetch_data (mixed)              Data from `fetch()`;
        [report_level = 0] (int)        Active report level;
        [verbosity = 0] (int)           The debug verbosity;
        [parse_unknowns = False] (bool) Should unknowns be parsed
                                        (e.g. parameters);

        [no_extension = False] (bool)   If true, do NOT attempt to pass this
                                        off to an extension. This is most likly
                                        a call FROM an extension that is
                                        falling back to the non-extension
                                        method;

      Returns:
        (list)  : The set of simplified metrics extracted from `fetch_data`
                  where each entry has the form:
                    { name    : (str),
                      value   : (int|float),
                      labels  : {
                        units :  (str),
                        ...
                      },
                      timestamp : (int) # Optional timestamp, milliseconds
                                        # since epoch;
                    }
    """
    #
    # First, give our super-class a shot in case there is an extension in play
    #
    parsed = super().parse( fetch_data, verbosity, report_level,
                            parse_unknowns, no_extension )
    if parsed is not None: #{
      # Parsing was handled by an extension
      return parsed
    #}

    ##################################################################
    # Fall-back simple SMART
    #
    # assert( isinstance( fetch_data, Smart )
    #
    if verbosity: #{
      print('>>> %s.parse(): src_drive[ %s ], fallback to simple SMART' %
            (type(self).__name__,
             self.src_drive),
            file=sys.stderr)
    #}

    # assert( isinstance( fetch_data, Smart ) )
    parsed = []

    metric_name   = 'smart_status'
    metric_labels = {
      'metric_source': 'smart',
      'status'       : fetch_data.status,
      'smart_capable': fetch_data.capable,
      'smart_enabled': fetch_data.enabled,
      'units'        : 'failure',
    }

    parsed.append({
      'name'  : metric_name,
      'labels': metric_labels,
      'value' : fetch_data.value,
    })

    # Include any attributes
    for attr in fetch_data.attrs: #{
      metric_name   = ('smart_%s' % (attr['name']))
      metric_labels = {
        'metric_source': 'smart',
      }

      for [key,val] in attr.items(): #{
        if key == 'name' or key == 'value': continue
        metric_labels.update({ key: val})
      #}

      parsed.append({
        'name'  : metric_name,
        'labels': metric_labels,
        'value' : int( attr['value'] ),
      })
    #}

    return parsed
  #}

  #
  # MetricSource method overrides }
  #############################################################################
#}
