#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import sys
import socket
import select
import uuid
import datetime
import traceback

from urllib.parse import urlparse

#import pprint
#pp = pprint.PrettyPrinter( indent=2, stream=sys.stderr )

try: #{
  import sg3utils
  #print('>>> sg_logs: sg3utils imported', file=sys.stderr)

except Exception as ex: #}{
  sg3utils = None

  print('*** sg_logs: Cannot load the sg3utils extension', file=sys.stderr)
#}

def has_method( obj, name ): #{
  """
    Determine if the given object has a callable method of the given name.

    Args:
      obj (mixed) : any container with attributes;
      name (str)  : the name of the target method;

    Returns:
      (bool)  : indication of whether `obj` contains a callable method with the
                given `name`;
  """
  return callable( getattr( obj, name, None ) )
#}

import sg_logs.utils
import sg_logs.metric
import sg_logs.drive
import sg_logs.drive.scan

from sg_logs.drive.scsi   import ScsiDrive
from sg_logs.drive.mr_pd  import MrPDrive

from sg_logs.scsi.type    import ScsiType

from sg_logs.metric       import Metric

class Sglogs(object): #{
  """
    A class wrapper around the sg3utils python extension.
  """

  # Static DEFAULT settings
  DEFAULTS  = {
    'debug'                 : 0,
    'publish'               : 'stdout',
    'publish_port'          : 3377,
    'publish_format'        : 'prometheus',
    'include_timestamp'     : False,

    'db'                    : 'scsi_log_page_db.yml',
    'parse_unknown_pages'   : False,
    'report_unknown_params' : True,
    'report_level'          : 1,
  }

  xoff  = bytearray( [19] )     # Flow control xoff/pause
  xon   = bytearray( [17] )     # Flow control xon /resume

  def __init__( self, config ): #{
    """
      Create a new instance.

      Args:
        config (dict)                   Configuration options
        [config.debug = 0] (bool|int)   The level/verbosity of debugging
                                        (True == 1, False == 0);
        [config.publish = 'stdout'] (str)
                                        The destination to which metrics
                                        should be published. This may be
                                        'stdout', 'stderr', or a URL;
        [config.publish_format = 'prometheus'] (str)
                                        The publication format
                                        ('prometheus' | 'json');
        [config.include_timestamp = False] (bool)
                                        Should a timestamp be included for any
                                        metric generated via
                                        metricize_source()?
        [config.db = 'scsi_log_page_db.yml'] (str)
                                        The path to the log page database;
        [config.parse_unknown_pages = False] (bool)
                                        Should unknown pages be parsed and
                                        included via metricize_source()?
        [config.report_unknown_params = True] (bool)
                                        Within parsed pages, should unknown
                                        parameters (e.g. vendor-specific) be
                                        parsed and included via
                                        metricize_source()?
        [config.report_level = 1] (int) The reporting level, along with the log
                                        page database, determines which log
                                        pages will be fetched, their parameters
                                        parsed, and resulting metrics reported;
        """
    self.config = Sglogs.DEFAULTS.copy()
    self.config.update( config )

    self._extensions    = {}

    self._db            = None
    self._shutting_down = False
    self._pause         = False   # Flow control xoff/xon : pause/resume
    self._scheduler     = None    # A reference to the active scheduler

    # Cache the node/hostname for use as a default label
    self._node  = self.get_hostname()

    # Cache of verbosity/debug level (populated on first access)
    self._verbosity = None

    # Cache of drive scan information (populated on first scan)
    self.drives = []

    # Defer connect_publish() until the first call to publish_metrics()
    self._pub_file = None
    self._pub_sock = None
  #}

  @property
  def db( self ): #{
    if self._db is None: #{
      # We need to identify and load the log database
      db_path = self.config.get( 'db', 'scsi_log_page_db.yml' )

      self.load_log_db( db_path )
    #}

    return self._db
  #}

  @property
  def is_shutting_down( self ): #{
    return self._shutting_down
  #}

  @property
  def is_active( self ): #{
    return not self._shutting_down
  #}

  @property
  def is_debug( self ): #{
    debug = self.config.get( 'debug', False )
    if isinstance( debug, int ): #{
      return (debug != 0)
    #}

    return debug
  #}

  @property
  def can_publish( self ): #{
    """
      Do we have an open connection to our publication endpoint?
    """
    return (self._pub_sock is not None or
            self._pub_file is not None)
  #}

  @property
  def verbosity( self ): #{
    if self._verbosity is None: #{
      debug = self.config.get( 'debug', 0 )

      if   debug == False:  self._verbosity = 0
      elif debug == True:   self._verbosity = 1
      else:                 self._verbosity = debug
    #}

    return self._verbosity
  #}

  def set_scheduler( self, scheduler ): #{
    """
      Set the current scheduler that controls this instance.

      Args:
        scheduler (Scheduler|None)  The active scehduler (or None to unset);

      Returns:
        (mixed) The previous scheduler;
    """
    prev_scheduler  = self._scheduler
    self._scheduler = scheduler

    return prev_scheduler
  #}

  def get_config_val( self, name ): #{
    """
      Return the value of the configuration variable, falling back to the value
      from `DEFAULTS` if one is available.

      Args:
        name (str)    The name of the target configuration variable

      Returns:
        (mixed)       The configuration value (None if not found)
    """
    return self.config.get( name, self.DEFAULTS.get( name, None ) )
  #}

  def get_hostname( self ): #{
    """
      Retrieve the hostname.

      If this is running within a k8s pod that has injected host-based
      information, we can pull the host name of the containing node instead of
      the name of this pod.

      :NOTE: This requires the deployment/daemonset to use the k8s DownwardAPI
             to inject this information, for example:
              env:
                - name: K8S_NODE_NAME
                  valueFrom:
                    fieldRef:
                      fieldPath: spec.nodeName

      If this information is not avaialble, simply return the OS-reported
      hostname.

      Args:
        none

      Returns:
        (string)  The hostname
    """
    return os.environ.get( 'K8S_NODE_NAME', socket.gethostname() )
  #}

  def get_drive_extension( self, drive ): #{
    """
      If any extensions are loaded, retrieve the one that best supports the
      provided drive.

      Args:
        drive (Drive)   The target drive instance;

      Returns:
        (mixed)   Either the best extension (Module) or None;
    """

    best = None
    for name,module in self._extensions.items(): #{
      if not has_method( module, 'get_drive_support_score' ): #{
        if self.verbosity > 1: #{
          print('=== get_drive_extension(): extension/module[ %s ] has no '
                        'get_drive_support_score() method' %
                    (name), file=sys.stderr)
        #}
        continue
      #}

      score = module.get_drive_support_score( drive )

      if self.verbosity > 1: #{
        print('=== get_drive_extension(): extension/module[ %s ], score[ %s ]' %
                  (name, score), file=sys.stderr)
      #}

      if score == 0.0:  continue

      # :XXX: We COULD collect all extensions that support this drive, sort
      #       them by score and then, when needed, apply in-order.
      #
      #       For `fetch_source()` and `parse_source()` we would stop the
      #       iteration with the first that returns a non-None result;
      #
      #         extensions.append( entry )

      if best is None or score > best['score']: #{
        best = {
          'score' : score,
          'module': module,
        }
      #}
    #}

    if self.verbosity > 1: #{
      if best is None: #{
        print('=== get_drive_extension(): no suitable extension: drive[ %s ]' %
                (drive), file=sys.stderr)

      else: #}{
        print('=== get_drive_extension(): extension/module[ %s ], score[ %s ] '
                            'for drive[ %s ]' %
                (best['module'], best['score'], drive), file=sys.stderr)
      #}
    #}

    return (best['module'] if best is not None
                           else None)
  #}

  def scan( self, opts = {} ): #{
    """
      Perform a (re)scan for drives connected to this system.

      Args:
        opts (dict)                       : scan options
        opts.include_nvme = False] (bool) : Include NVME drives?
        opts.include_scsi = False] (bool) : Include SCSI drives?

      Returns:
        (list)  : A list of dictionaries of the form:
                  { name    : device basename {str},
                    sys_path: sys-fs path to device {str},
                    type    : SCSI device type {int};
                    type_str: SCSI device type as a string {str};

                    vendor  : device vendor {str};
                    model   : device model {str};
                    serial  : device serial number {str};
                    dev     : The '/dev/' path to the device {str};

                    [rev]   : device revision {str};
                    [scsi   : {host:, channel:, target:, lun: }],
                    [nvme   : {controller:, minor: }],
                  }
    """
    verbosity = self.verbosity

    self.drives = []
    if opts.get('include_scsi', True ): #{
      self.drives.extend( sg_logs.drive.scan.get_scsi_devices( verbosity ) )
    #}

    #if opts.get('include_nvme', False): #{
    #  self.drives.extend( sg_logs.drive.scan.get_nvme_devices( verbosity ) )
    ##}

    for drive in self.drives: #{
      if isinstance( drive, ScsiDrive ): #{
        # Augment all SCSI drives with a page map.
        self.add_page_map( drive )
      #}

      # Determine the best extension based for this drive (if any).
      extension = self.get_drive_extension( drive )
      if extension: #{
        if extension and self.verbosity: #{
          print('=== Associate extension %s with new Drive [ %s ]' %
                (extension, drive), file=sys.stderr)
        #}

        drive.set_extension( extension )
      #}
    #}

    return self.drives
  #}

  def gather_unique_drives( self, opts = {} ): #{
    """
      Perform a scan of all drives, generating a unique set (by serial number)

      Args:
        opts (dict)                       : scan options
        opts.include_nvme = False] (bool) : Include NVME drives?
        opts.include_scsi = False] (bool) : Include SCSI drives?

      Returns:
        (dict)  The set of unique drives (by serial number)
    """
    # Scan all drives using the 'scan' configuration section
    # generating the set of unique drives (by serial number)
    unique_drives = {}
    for drive in self.scan( opts ): #{
      if drive.dev_type != 'disk': continue

      key = drive.serial

      if key is None or len(key) < 1: #{
        # The drive serial number is either missing or empty
        #
        # Generate a unique key to keep this drive independent of any others
        # which have no serial number
        key = uuid.uuid4().hex
      #}

      if key in unique_drives: #{
        # Include the paths from this drive instance in the single, unique
        # representation
        #
        # If the current unique drive has no `sys_path` but `drive` does,
        # replace the unique.
        ud = unique_drives[ key ]

        if ud.sys_path is None and drive.sys_path is not None: #{
          # Use `drive` as the unique device since it has a `sys_path` and the
          # current entry does not
          drive.add_dev_paths( ud.dev_paths )

          unique_drives[ key ] = drive
          ud = drive

        else: #}{
          ud.add_dev_paths( drive.dev_paths )
        #}

        if self.verbosity > 1: #{
          print('=== gather_unique_drives(): duplicate : %s : %s : %s' %
                  (drive.title, ','.join( drive.dev_paths ), key))
        #}
        continue
      #}

      #print('%s : add drive:' % (drive.name), key)
      unique_drives[key] = drive
    #}

    return unique_drives
  #}

  def probe_drive( self, path, verbosity = None ): #{
    """
      Probe a single drive

      `path` should have the form:
        /dev/%dev%[:%class_id%:%dev_id%]

        Example:
          /dev/sdho:MR:8
            => /dev/sdho : MegaRaid, disk id 8

      Args:
        path (str)                The path identifying the target drive
        [verbosity = None] (int)  The verbosity

      Returns:
        (ScsiDrive | MrPDrive | None)  A drive instance,
    """
    if verbosity is None: verbosity = self.verbosity

    parts  = path.split(':')
    nParts = len(parts)
    if nParts < 1 or nParts > 3: #{
      print('*** Invalid target (format path[:target]): %s' %
            (path), file=sys.stderr)
      return
    #}

    target  = None
    sg_path = parts[0]
    if nParts > 1: #{
      if nParts > 2: #{
        # MUST be of the form:
        #   /dev/%dev%:MR:%target%
        if parts[1].lower() != 'mr': #{
          print('*** Unknown target class [ %s ] : %s' %
                (parts[1], path), file=sys.stderr)
          return
        #}
        target = parts[2]

      else: #}{
        target = parts[1]
      #}
    #}

    try: #{
      sg3 = sg3utils.open( sg_path )

    except Exception as ex: #}{
      #print("*** sg_logs: Cannot open '%s' via sg3utils: %s" %
      #        (sg_path, ex), file=sys.stderr)
      return
    #}

    if sg3utils.is_mr( sg3 ): #{
      cur_did = sg3utils.mr_get_target( sg3 )
      pd_list = sg3utils.mr_get_pd_list( sg3 )
      if verbosity > 0: #{
        print('=== Current MegaRaid did[ %d ] for %s' % (cur_did, sg_path))
        print('=== %d physical devices behind MegaRaid controlled %s' %
                (len(pd_list), sg_path))
      #}

      if target is None: #{
        if cur_did >= 0: #{
          target_id = cur_did
        else: #}{
          # No target has been specified and none was automatically selected
          print("*** Drive '%s' requires a target ( e.g. '%s:MR:%d' )" %
                (sg_path, sg_path, pd_list[0]['device_id']), file=sys.stderr)
          print("***   Valid Targets:", file=sys.stderr)
          print("***     [ ", end="", file=sys.stderr)

          idex = 0
          for pd in pd_list: #{
            if pd['scsi_dev_type'] != ScsiType.DISK:  continue

            if idex > 0 and idex % 16 == 0: #{
              print("", file=sys.stderr)
              print("***       ", end="", file=sys.stderr)
            #}

            print("%3d" % (pd['device_id']), end=" ", file=sys.stderr)
            idex += 1
          #}
          print(" ]", file=sys.stderr)

          sg3utils.close( sg3 )
          return
        #}

      else: #}{
        # An explicit target was provided
        try: #{
          target_id = int( target, 0 )

        except Exception as ex: #}{
          print('*** The target must be an integer [ %s ]' % (target),
                file=sys.stderr)
          sg3utils.close( sg3 )
          return
        #}
      #}

      # Is the target device listed?
      mr_info = {
        'pd': None,

        # :NOTE: Since we'll be passing in our `sg3` handle, there is no need
        #        to include an explicit `sg_path` but it doesn't hurt.
        'sg_path' : sg_path,
      }

      for idex, pd in enumerate( pd_list ): #{
        # Skip non-disk devices
        if pd['scsi_dev_type'] != ScsiType.DISK:  continue
        if pd['device_id']     != target_id:      continue

        mr_info['pd'] = pd
        break
      #}

      if mr_info['pd'] is None: #{
        print("*** Target '%s' is not a device id for '%s'" %
              (target, sg_path), file=sys.stderr)
        sg3utils.close( sg3 )
        return
      #}

      drive = MrPDrive( sg_path, info = mr_info,
                                 sg3  = sg3)

    else: #}{
      drive = ScsiDrive( sg_path, sg3 = sg3)
    #}

    # Ensure we have inquiry-based meta-data
    drive.augment_with_scsi_inq( verbosity = self.verbosity )

    # Determine the best extension based for this drive (if any).
    extension = self.get_drive_extension( drive )
    if extension: #{
      if extension and self.verbosity: #{
        print('=== Associate extension %s with new Drive [ %s ]' %
              (extension, drive), file=sys.stderr)
      #}

      drive.set_extension( extension )
    #}

    # The `sg3` handle now belongs to `drive`
    return self.add_page_map( drive )
  #}

  def add_page_map( self, drive ): #{
    """
      If there is an available log page database, fetch and attach the page map
      entry for the target drive.

      Args:
        drive (Drive) The target drive instance

      Returns:
        (Drive)       The updated `drive`;
    """
    if self.db: #{
      drive.page_map = self.db.get_page_map( drive.vendor,
                                             drive.model,
                                             drive.rev )
    #}

    return drive
  #}

  def fetch_source( self, metric_src ): #{
    """
      Given a target metric source, with a source drive, retrieve and return
      the source data.

      Args:
        metric_src (MetricSource) The target metric source;

      Returns:
        (dict)  : The source data;
    """
    metric_data = None

    try: #{
      metric_data = metric_src.fetch( verbosity = self.verbosity )

    except Exception as ex: #}{
      print('*** Cannot fetch log_page %s: %s' % (log_page, ex),
            file=sys.stderr)
    #}

    return metric_data
  #}

  def metricize_source( self, metric_src ): #{
    """
      Given a target log page, with a source drive, retrieve and parse the
      sense data for the target page. If the page was previously parsed,
      indicated via `metric_src.prior_data`, only output parameters that have
      changed.

      Args:
        metric_src (MetricSource) The target metric source;

      Returns:
        (list | None)  A list of Metric instances or None if not parsed
    """
    metrics = None
    report_level = self.get_config_val( 'report_level' )

    if metric_src.level >= report_level: #{
      # Skip this log page since it's level is above the current report level
      if self.verbosity: #{
        print('=== metricize_source( %s ): SKIP : '
                        'page level[ %d ] >= report level[ %d ]' %
              (metric_src, metric_src.level, report_level ),
              file=sys.stderr)
      #}

      return metrics
    #}

    # Generate the set of /dev/partition => mount_point entries
    mount_points = []
    if type(metric_src.src_drive.mount_points) is dict: #{
      for dev,mount_point in metric_src.src_drive.mount_points.items(): #{
        mount_points.append( '%s => %s' % (dev, mount_point) )
      #}
    #}

    # The set of labels to apply to all metrics from the target log page
    #
    # :XXX: Should we reduce the common set of labels by reporting drive
    #       metadata once?
    labels     = {
      # common identifiers/keys
      'node'          : self._node,
      'vendor_name'   : metric_src.src_drive.vendor,
      'model_id'      : metric_src.src_drive.model,
      'revision_id'   : metric_src.src_drive.rev,
      'serial_id'     : metric_src.src_drive.serial,

      # Disk meta-data
      'dev_type'      : metric_src.src_drive.dev_type,
      'disk_type'     : metric_src.src_drive.disk_type,
      'dev_paths'     : ','.join( metric_src.src_drive.dev_paths ),
      'mount_points'  : ','.join( mount_points ),
    }

    parse_unknowns = ( self.get_config_val( 'parse_unknown_pages' ) or
                       self.get_config_val( 'report_unknown_params' ) )

    metrics = metric_src.metricize( labels          = labels,
                                    parse_unknowns  = parse_unknowns,
                                    report_level    = report_level,
                                    verbosity       = self.verbosity )

    if metrics: #{
      # :XXX: Should we reduce the common set of labels by reporting drive
      #       metadata once?
      #meta_labels = labels.copy()
      #meta_labels.update({
      #  'dev_type'      : metric_src.src_drive.dev_type,
      #  'disk_type'     : metric_src.src_drive.disk_type,
      #  'dev_paths'     : ','.join( metric_src.src_drive.dev_paths ),
      #  'mount_points'  : ','.join( mount_points ),
      #})
      #metrics.insert(0, Metric( 'device_storage', meta_labels, 1 ) )

      if self.get_config_val( 'include_timestamp' ): #{
        # Include a timestamp for all generated metrics
        #
        # :NOTE: Metric timestamps should be milliseconds since epoch and
        #        Python timestamp() returns a floating point seconds since
        #        epoch
        #
        ts = int( datetime.datetime.now().timestamp() * 1000 )

        for metric in metrics: #{
          metric.timestamp = ts
        #}
      #}
    #}

    return metrics
  #}

  def publish_metrics( self, metrics ): #{
    """
      Publish a set of metrics generated in a manner similar to
      metricize_source().

      Args:
        metrics (list)    The set of metrics to publish;
    """
    if not self.can_publish: #{
      # Attempt to connect to our publication endpoint
      self.connect_publish()
    #}

    try: #{
      iterator = iter( metrics )

    except TypeError: #}{
      # Not iterable
      pass

    else: #}{
      # Iterable
      format = self.config.get( 'publish_format', None )
      strs   = []
      for metric in iterator: #{
        str_val = (metric.toJSON()
                    if format == 'json'
                    else str(metric)) # metric.prometheus_format

        strs.append( str_val )
      #}

      if self._pub_sock: #{
        socks = [ self._pub_sock ]

        # Loop until we have been able to write this data
        while self._pub_sock: #{
          try: #{
            read, write, err = select.select( socks, socks, [] )

          except select.error as ex: #}{
            # Close the socket
            self._destroy_socket()

            # Re-raise the exception
            raise ex
          #}

          if read and len(read) > 0: #{
            # There is data to be read -- most likely, a flow-control message
            self._sock_read()
          #}

          if write and len(write) > 0: #{
            # The socket is ready to receive data
            #
            # Send the generated metric strings as newline-separated lines,
            # making sure we include a final newline to terminate the final
            # string.
            data = bytes('\n'.join( strs ) +'\n', 'utf-8')

            self._sock_write( data )

            # Stop this select loop
            break
          #}
        #}

      else: #}{
        print( '\n'.join( strs ), file = self._pub_file )
      #}
    #}
  #}

  def load_extension( self, name ): #{
    """
      Dynamically load a configurable drive/vendor-specific extension with the
      given name.

      Args:
        name (str)    The name, or path WITHIN this module of the extension to
                      load;

      Returns:
        (mixed)       The loaded module (None if not found)
    """
    if name in self._extensions: #{
      return self._extensions[name]
    #}

    import importlib.util

    DIR         = os.path.dirname( os.path.realpath( __file__ ) )
    norm_name   = os.path.normpath( name )
    MODULE_PATH = '{0:s}/{1:s}.py'.format( DIR, norm_name)
    module      = None

    if os.path.exists( MODULE_PATH ): #{
      MODULE_NAME = os.path.basename( MODULE_PATH ).split('.')[0]

      if self.verbosity > 1: #{
        print('>>> Load extension [ %s ]: %s ...' %
              (MODULE_NAME, MODULE_PATH), file=sys.stderr)

      elif self.verbosity: #}{
        rel_path = MODULE_PATH.replace( os.getcwd() +'/', '' )
        print('>>> Load extension [ %s ]: %s ...' %
              (MODULE_NAME, rel_path), file=sys.stderr)
      #}

      spec   = importlib.util.spec_from_file_location( MODULE_NAME,
                                                       MODULE_PATH )
      module = importlib.util.module_from_spec( spec )
      sys.modules[ spec.name ] = module

      spec.loader.exec_module( module )

    else: #}{
      if self.verbosity: #{
        print('>>> Load extension [ module : %s ]: ...' %
              (name), file=sys.stderr)
      #}

      # Attempt to import the extension as a module
      module = importlib.import_module( name )
    #}

    if module: #{
      self._extensions[name] = module
    #}

    return module
  #}

  def load_log_db( self, path ): #{
    """
      Dynamically load a configurable SCSI Log Page database from the given
      file/path.
    """
    from sg_logs.scsi.log_page.db import ScsiLogPageDb

    db_path = os.path.realpath( path )

    if not os.path.isfile( db_path ): #{
      # See if there is an accessible file within the containing package that
      # matches the basename of the target.

      full_path = os.path.realpath(
                    os.path.join( os.path.dirname( __file__ ), '..',
                                  os.path.basename( db_path ) ) )

      if os.path.isfile( full_path ): #{
        if self.verbosity: #{
          print('=== Redirect SCSI Log Page db from: %s ...' %
                (db_path), file=sys.stderr)
        #}

        db_path = full_path
      #}
    #}


    if self.verbosity > 1: #{
      print('>>> Load SCSI Log Page db: %s ...' % (db_path), file=sys.stderr)

    elif self.verbosity: #}{
      rel_path = db_path.replace( os.getcwd() +'/', '' )
      print('>>> Load SCSI Log Page db: %s ...' % (rel_path), file=sys.stderr)

    #}

    self._db = ScsiLogPageDb( db_path )

    # Iterate over all loaded extensions to see if any have the
    # `augment_log_page_db()` method.
    for name,module in self._extensions.items(): #{
      if has_method( module, 'augment_log_page_db' ): #{
        if self.verbosity: #{
          print('>>> Invoke extension %s.augment_log_page_db()' %
                  (name), file=sys.stderr)
        #}
        try: #{
          module.augment_log_page_db( self._db, self.verbosity )

        except Exception as ex: #}{
          print('*** Extension %s.augment_log_page_db() FAILED:' %
                (name), file=sys.stderr)
          traceback.print_exc( file=sys.stderr )
        #}
      #}
    #}

    return self._db
  #}

  def connect_publish( self ): #{
    """
      Prepare to publish metrics by interpreting `self.config['publish']`
      which may be 'stdout', 'stderr', or a URL of the form:
        tcp://%host%:%port%
    """
    if not self.is_active:  return

    def_pub = Sglogs.DEFAULTS['publish']
    pub_str = self.config.get( 'publish', 'stdout' )
    host    = None
    port    = None

    if pub_str != 'stdout' and pub_str != 'stderr': #{
      # Attempt to parse the 'publish' value as a URL which, if valid, will
      # have the form:
      #     tcp://%host%:%port%
      #
      try: #{
        url  = urlparse( pub_str )
        host = url.hostname
        port = url.port

        # :TODO: Add support for the 'file:' URL scheme:
        #   file:///path/to/file.txt  => output to file '/path/to/file.txt'
        if url.scheme != 'tcp': #{
          # Fallback to default
          print('*** Invalid scheme for publish[ %s ]' % (pub_str),
                file=sys.stderr)

          host = None
          port = None
          self.config['publish'] = def_pub

        elif not host: #}{
          # Fallback to default
          print('*** Missing host for publish[ %s ]' % (pub_str),
                file=sys.stderr)

          port = None
          self.config['publish'] = def_pub

        elif not port: #}{
          port = Sglogs.DEFAULTS['publish_port']

        #}
      except Exception as ex: #}{
        # Fallback to default
        print('*** Invalid publish URL[ %s ]: %s' % (pub_str, ex),
              file=sys.stderr)

        self.config['publish'] = def_pub
      #}
    #}

    if host and port: #{
      self._pub_file = None

      # Destroy any existing publish socket
      self._destroy_socket()

      # Create a new publish socket
      self._pub_sock = self._create_socket( host, port )
    #}

    if self._pub_sock is None: #{
      # Fallback to default
      if self.verbosity > 0: #{
        print('=== Publish to %s' % (pub_str), file=sys.stderr)
      #}

      self._pub_file = (sys.stderr if pub_str == 'stderr'
                                   else sys.stdout)
    #}
  #}

  def shutdown( self ): #{
    """
      Cleanly shutdown any existing connections
    """
    if self._shutting_down: return
    self._shutting_down = True

    if self.verbosity: #{
      print('=== Sglogs.shutdown() ...', file=sys.stderr)
    #}

    if self._pub_sock: #{
      self._destroy_socket()

    elif self._pub_file: #}{
      if self._pub_file != sys.stdout and self._pub_file != sys.stderr: #{
        self._pub_file.close()
      #}
    #}

    self._pub_sock = None
    self._pub_file = None
  #}

  def _create_socket( self, host, port ): #{
    """
      Attempt to create a TCP socket connected to the given host/port

      Args:
        host (str)    The target host;
        port (num)    The target port;
    """
    sock = None

    try: #{
      print('=== Open TCP socket to %s:%s ...' %
            (host, port), file=sys.stderr, end='.')
      sys.stderr.flush()

      sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
      sock.connect( (host, port) )

      print('. connected', file=sys.stderr)

    except Exception as ex: #}{
      print(' failed: %s' % (ex), file=sys.stderr)
      sock = None

      # Re-raise this exception
      raise ex
    #}

    return sock
  #}

  def _destroy_socket( self ): #{
    """
      Cleanly shutdown any current `_pub_sock`
    """
    sock = self._pub_sock
    self._pub_sock = None

    if sock: #{
      sock.shutdown( socket.SHUT_RDWR )
      sock.close()
    #}
  #}

  def _sock_write( self, data ): #{
    """
      Write the given data to our _pub_sock

      Args:
        data (bytes)    The data to write;
    """
    if self._pub_sock is None or self._pause: return

    try: #{
      self._pub_sock.send( data )

    except socket.error as ex: #}{
      # Close the socket
      self._destroy_socket()

      # Re-raise the exception
      raise ex
    #}
  #}

  def _sock_read( self ): #{
    """
      Read from our _pub_sock -- most likely flow-control
    """
    if self._pub_sock is None: return

    try: #{
      data = self._pub_sock.recv( 512 )

    except socket.err as ex: #}{
      # Close the socket
      self._destroy_socket()

      # Re-raise the exception
      raise ex
    #}

    if len(data) == 0: #{
      # select() said read data was available but there is none.
      # The other end has hung up
      print('=== sock.hangup')

      # Close the socket
      self._destroy_socket()

      return
    #}

    if data[0] == self.xoff[0]: #{
      # flow-control: pause
      if self.verbosity > 1: #{
        print('=== sock.xoff')
      #}

      self._pause = True

      if self._scheduler: #{
        self._scheduler.pause()
      #}

    elif data[0] == self.xon[0]: #}{
      # flow-control: resume
      if self.verbosity > 1: #{
        print('=== sock.xon')
      #}

      self._pause = False

      if self._scheduler: #{
        self._scheduler.resume()
      #}

    else: #}{
      # unexpected message
      if self.verbosity > 1: #{
        print('=== sock.data:', data)
      #}
    #}
  #}
#}
