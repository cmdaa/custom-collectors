#!/bin/bash
#
# Run a cmdaa/sg-logs docker container mounting the current working directory
# for use in testing.
#
cat <<EOF
>>>
>>> Once the container is running, you may run tests:
>>>   ./tests.sh
>>>
EOF

docker run --rm -it \
  --mount type=bind,source="$(pwd)",target=/root/sg-logs \
  --workdir=/root/sg-logs \
    cmdaa/sg-logs /bin/bash
