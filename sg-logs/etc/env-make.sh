#!/bin/bash
#
# Create (and enter) a Python virtual environment
#
DIR="$(realpath "$(dirname "$0")")"
ROOT="$(dirname "$DIR")"

function check_rc() {
  RC=$1; shift
  if [ $RC -ne 0 ]; then
    echo "*** Failure [ $RC ]: $@"
    exit $RC
  fi
}

if [ ! -f "$ROOT/../sg3_utils/lib/.libs/libsgutils2.a" ]; then
  echo ">>> Build sg3_utils ..."
  pushd "$ROOT/../sg3_utils"  > /dev/null

  if [ ! -f configure ]; then
     echo ">>>> Generate configure ..."
    ./autogen.sh
    check_rc $? "autogen.sh"
  fi

  if [ ! -f Makefile ]; then
     echo ">>>> Generate Makefile ..."
    ./configure --with-pic
    check_rc $? "configure"
  fi

  echo ">>>> Building ..."
  make
  check_rc $? "make"

  popd                        > /dev/null
fi


if [ ! -d env ]; then
  echo
  echo ">>> Creating a virtual environment 'env' ..."
  python3 -m venv env
  check_rc $? "venv"
fi

# Enter the virtual environment to install dependenties
source env/bin/activate

echo ">>> Install dependencies/requirements ..."

echo ">>>   sg3utils from python-sgutils ..."
make -C "$ROOT/../python-sgutils" install
check_rc $? "install sg3utils"

echo ">>>   pip (requirements.txt) ..."
pip3 install -r requirements.txt
check_rc $? "install requirements"

echo
echo ">>> Virtual environment ready"
echo ">>>   Use 'source env/bin/activate' to enter"
echo
