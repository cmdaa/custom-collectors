#!/bin/bash
#
# Run the sg-logs.py script using the locally compiled sg3_utils library and
# sg3utils python extension
#
# :NOTE: This ASSUMES the sg3_utils exists as a sibling of this directory and
#        has already been compiled.
#
# :NOTE: This ASSUMES the python-sgutils exists as a sibling of this directory
#        and has already been compiled.
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
DIR="$(realpath "$(dirname "$0")")"
ROOT="$(dirname "$DIR")"
#echo ">>> DIR: ${DIR}"

. ${ROOT}/env/bin/activate

${ROOT}/sg-logs "$@"
