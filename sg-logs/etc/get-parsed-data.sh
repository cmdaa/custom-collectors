#!/bin/bash
#
# Run the get-parsed-data script using the virtual environment
#
DIR="$(realpath "$(dirname "$0")")"
ROOT="$(dirname "$DIR")"

. $ROOT/env/bin/activate

$ROOT/get-parsed-data "$@"
