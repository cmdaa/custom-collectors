#!/bin/bash
#
# Update a python virtual environment to operate from a new location
#
function usage() {
  cat <<EOF

*** Usage $(basename "$0") %env% %absolute-path-to-new-location%

EOF
  exit -1
}

if [[ $# -lt 2 ]]; then
  usage
fi

VENV="$1"
 DST="$2"

if [[ ! -d "$VENV" ]]; then
  echo "*** '$VENV' is not a directory"
  usage
fi

if [[ ! -f "$VENV/bin/activate" ]]; then
  echo "*** '$VENV' contains no 'bin/activate'"
  usage
fi

SRC=$(awk -F= '/^VIRTUAL_ENV/{print substr($2, 2, length($2)-2)}' \
              "$VENV/bin/activate")

FIX=( "$(grep -rl "$SRC" "$VENV" | grep -Ev '.pyc|.so')" )
for path in ${FIX[@]}; do
  echo -n ">>> Fix-up $path ..."
  sed -e 's#'$SRC'#'$DST'#' -i '' "$path"
  rc=$?

  if [[ $rc -ne 0 ]]; then
    echo " FAILED[ $c ]"
  else
    echo " done"
  fi
done

echo
echo ">>> The virtual environment '$VENV' may now be moved to '$DST'"
echo
