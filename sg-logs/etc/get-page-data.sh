#!/bin/bash
#
# Run the get-page-data script using the virtual environment
#
DIR="$(realpath "$(dirname "$0")")"
ROOT="$(dirname "$DIR")"

. $ROOT/env/bin/activate

$ROOT/get-page-data "$@"
