#!/usr/bin/env python3
#
# Show a human-readable version of test fixure data
#
import os
import sys
import pickle
import pprint

from sg_logs.utils                    import prhex, all_ascii
from sg_logs.scsi.vendor              import ScsiVendor
from sg_logs.scsi.log_page            import ScsiLogPage
from sg_logs.scsi.log_page.parser.t10 import parse_source
from tests.parse                      import parse_logsense

def parse_value( value_raw, fmt ): #{
  """
    Given a raw LOG SENSE parameter value, convert it according to the provided
    format-and-linking value

    Args:
      value_raw (bytearray) The raw LOG SENSE parameter;
      fmt (int)             The SCSI format-and-linking value;

    Returns:
      value (int|bytearray) The value converted according to `fmt`;
  """
  value = value_raw

  if (fmt == 0x0 or fmt == 0x2): #{
    # 0x00: Bounded data counter
    # 0x02: Bounded or unbounded data counter
    ###
    # param_len:
    #   1:  8-bit : uint8_t
    #   2: 16-bit : uint16_t
    #   4: 32-bit : uint32_t
    #   8: 64-bit : uint64_t
    #
    value = int.from_bytes( value_raw, 'big' )

  elif (fmt == 0x1): #}{
    # 0x01: ASCII format list
    ###
    # Iff all bytes are ASCII, convert this to a string, otherwise, leave it as
    # a raw byte array
    #
    if all_ascii( value_raw ): #{
      value = value_raw.decode('ascii')
    #}
  #}

  return value
#}

def process_pickle( file_path ): #{
  print('>>> Pickled Data [ %s ]' % (os.path.basename(file_path)))

  with open(file_path, mode='rb') as file: #{
    data = pickle.load( file )
  #}

  pprint.pprint( data, indent=2 )
#}

def process_log_sense( file_path ): #{
  print('>>> Log Sense [ %s ]' % (os.path.basename(file_path)))

  # Read the raw SCSI LOG SENSE data
  with open(file_path, mode='rb') as file: #{
    data = bytearray( file.read() )
  #}

  print('>>> %d bytes' % (len(data)))
  prhex( data, indent='    : ', show_ascii=True)

  # Normalize the raw SCSI LOG SENSE data into our internal format
  log_sense = parse_logsense( data )

  print('>>> normalized:')
  pprint.pprint( log_sense, indent=2 )

  # Parse the normalized log_sense data using the default t10 parser
  log_page = ScsiLogPage( src_drive = None,
                          page      = log_sense['page'],
                          subpage   = log_sense['subpage'] )
  parsed = parse_source( log_page, log_sense )
  print('>>> parsed (t10):')
  pprint.pprint( parsed, indent=2 )
#}

def main(): #{
  if len(sys.argv) < 2: #{
    print('*** Usage %s /path/to/file.{pyb,bin}' %
            (os.path.basename(sys.argv[0])))
    sys.exit(-1)
  #}

  file_path = sys.argv[1]

  if file_path.endswith('.pyb'): #{
    process_pickle( file_path )

  elif file_path.endswith('.bin'): #}{
    process_log_sense( file_path )
  else: #}{
    print('*** Unknown file extension [ %s ]' % (os.path.basename(file_path)))
  #}
#}

main()
