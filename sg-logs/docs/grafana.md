# Disk Metrics

This document identifies a number of useful sg-logs disk metrics for Grafana
dashboards from the [full set of metrics](./sg-logs-metrics-v4-t10.csv).

## General labels
- metric_source ( scsi-log-page:<page>,<subpage>[:<param>[:offset]] )
- node          ( name of the host/node containing the SCSI device )

- dev_paths     ( CSV device paths, e.g. "/dev/sde,/dev/sg5" )
- mount_points  ( CSV mount points, e.g. "/tmp" )

- dev_type      ( SCSI device type, e.g. "disk" )
- disk_type     ( SCSI disk type,   e.g. "hdd", "ssd" )
- model_id      ( SCSI model id,    e.g. "VN123456NM0123G" )
- revision_id   ( SCSI revision id, e.g. "UZW1" )
- serial_id     ( SCSI serial id,   e.g. "SNM1234A192838C1012F591" )
- vendor_name   ( SCSI vendor name, e.g. "SEAGATE" )


## T10 IO metrics

### page 02, 03, 05, 06 (error counters)

| metric name                     | stat_type             | units |
| ------------------------------- | --------------------- | ----- |
| device_storage_write            | total bytes processed | bytes |
| device_storage_read             | total bytes processed | bytes |
| device_storage_verify           | total bytes processed | bytes |


### page 19 (western digital : general statistics and performance)

| metric name                     | stat_type             | units     |
| ------------------------------- | --------------------- | --------- |
| device_storage_write            | -none-                | commands  |
| device_storage_read             | -none-                | commands  |
| device_storage_write            | total logical blocks  | blocks    |
| device_storage_read             | total logical blocks  | blocks    |
| device_storage_write            | command processing    | intervals |
| device_storage_read             | command processing    | intervals |
| device_storage_io               | weighted total read commands plust write commands   | commands |
| device_storage_io               | weighted read command processing plus write commands processing | commands |
| device_storage_write            | total Force-Unit-Access (FUA)     | commands  |
| device_storage_read             | total Force-Unit-Access (FUA)     | commands  |
| device_storage_write            | total Force-Unit-Access (FUA) NV  | commands  |
| device_storage_read             | total Force-Unit-Access (FUA) NV  | commands  |
| device_storage_write            | total Force-Unit-Access (FUA) command processing    | intervals |
| device_storage_read             | total Force-Unit-Access (FUA) command processing    | intervals |
| device_storage_write            | total Force-Unit-Access (FUA) NV command processing | intervals |
| device_storage_read             | total Force-Unit-Access (FUA) NV command processing | intervals |


### page 37: (hitachi : miscellaneous)

| metric name                     | stat_type | units           |
| ------------------------------- | --------- | --------------- |
| device_storage_write            | total     | bytes, commands |
| device_storage_read             | total     | bytes, commands |
| device_storage_verify           | total     | bytes           |


## T10 error metrics

### page 02, 03, 05, 06 (error counters)

| metric name                     | stat_type                           |
| ------------------------------- | ----------------------------------- |
| device_storage_write_error      | total corrected, total uncorrected  |
| device_storage_read_error       | total corrected, total uncorrected  |
| device_storage_verify_error     | total corrected, total uncorrected  |
| device_storage_nonmedium_error  | total error count                   |


### page 18 (protocol specific port)

| metric name                     | stat_type                           |
| ------------------------------- | ----------------------------------- |
| device_storage_protocol_error   | invalid dword, invalid words, ...   |


### page 37: (hitachi : miscellaneous)

| metric name                             | stat_type                           |
| --------------------------------------- | ----------------------------------- |
| device_storage_defects_growth           | growth list                         |
| device_storage_informational_exceptions | -none-                              |


## S.M.A.R.T. metrics

### Status
`device_storage_smart_status` with `units:failure` will have a value of 0 for
"OK" and non-zero for any failure condition.

It will also have additional labels:
- smart_capable : True | False
- smart_enabled : True | False
- status        : OK   | String describing the failure


### Temperature Warnings
`device_storage_smart_temperature_warnings` with `units:enabled` will have a
value of 1 if temperature warnings are enabled.


### Next Test
`device_storage_smart_next_test` with `units:minutes` reports the number of
minutes until the next S.M.A.R.T. test.



## Seagate error metrics

| metric name                         | stat_type                           |
| ----------------------------------- | ----------------------------------- |
| device_storage_reallocation_error   | total defect list entries added     |
| device_storage_hardware_error       | total failures                      |
| device_storage_reassign_error       | total failures                      |
| device_storage_servo_start_error    | total failures                      |
| device_storage_storage_medium_error | total failures                      |
| device_storage_write_error          | total failures                      |
| device_storage_seek_error           | total failures                      |
| device_storage_spinup_error         | total failures                      |
| device_storage_firmware_error       | total failures                      |
| device_storage_read_seek_error      | total failures                      |
| device_storage_read_error           | total failures                      |
