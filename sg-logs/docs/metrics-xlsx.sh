#!/bin/bash
#
# Generate a metric XLSX from a set of page data from sg-logs generated via
# data/json-*/gather.sh to data/json-*/metrics.json
#
# :NOTE: This requires an up-to-date virtual environment that includes the
#        'xlsxwriter' pip module.
#
if [ ! -d ../env ]; then
  echo "*** Missing virtual environment"
  echo "*** Try running 'make env' from the parent directory"
  exit -1
fi

# Activate the virtual environment
source ../env/bin/activate

for type in t10 sg; do
  dst="data/metrics-v4-$type.xlsx"
  src="data/json-$type/metrics.json"

  echo -n ">>> Generate $dst ..."

  if [ ! -f "$src" ]; then
    echo " Missing src [ $src ]"
    continue
  fi

  cat "$src" | ./metrics-xlsx.py "$@" "$dst"
  echo ". done"
done
