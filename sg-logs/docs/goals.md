The goals of this system:
1. collect metrics as unobtrusively as possible;
2. get the metrics off the source system as soon as possible;
3. reduce the duplicate reporting of metrics that do not change often;

---
To address #1, a log page "database" is used to assign a cost and report
level to available metrics by disk type. The database is currently a simple
YAML file (e.g. `scsi_log_page_db.yml`) specified via configuration.

Currently, the "cost" is not used, but the idea was that it would provide a
measure of the time and performance impact of collecting metrics from a
specific metric source. It could be used for scheduling in order to spread out
costly collection.

The report level *is* actively checked against the configured report level
before a metric source is included in a collection schedule. The report level
is intended as a sort of tuning knob with higher reporting levels gathering
more data that has more potential of disrupting operational use of a disk.
This aligns with Jeff's goal of collecting the minimal set of metrics required
to unobtrusively monitor performance/reliability while allowing for more
intrusive collection when needed.

Collection scheduling is currently a simple, naive randomization of all
available metric sources over the defined sync period. This is handled by
`Scheduler` (`sg_logs/scheduler.py`).

Note that scheduling is used even with the 'one-shot' parameter. In this
special case, scheduling is terminated after the first completion.

---
To address #2, publication of metrics can be to one of three locations with the
first two primarily for debugging:
- stdout
- stderr
- tcp://%host%:%port%

The publication location is handled in `Sglogs.connect_publish()`
(`sg_logs/__init__.py`) and could be extended to support additional URL schemes
if needed. Supporting a `file://` scheme would be a simple addition but is
counter to the second goal.


Metrics can be published in one of two forms:
- Prometheus Exposition Format (verbose but with a rich ecosystem);
- JSON (much lighter weight);

Publication formatting is handled in `Sglogs.publish_metrics()`
(`sg_logs/__init__.py`).

---
To address #3, we have implemented "differential reporting". This is our term
for the "beat", "sync beat", and filtering that occurs during collection.

- beat
    a reliable timer which fires at a consistent, configurable time interval.
    At each beat, collection is triggered and differential reporting occurs;

- differential reporting
    reporting only those metrics whose value has changed from that observed
    during the previous beat;
    
- sync beat
    the number of beats at which all metrics will be reported regardless of
    whether they have changed since the last beat, within the restrictions of
    the current report level;


Differential reporting is handled by concrete sub-classes of `MetricSource`
(`sg_logs/metric_source.py`) -- e.g. `ScsiLogPage`
(`sg_logs/log_page/__init__.py`) -- when `metricize()` is invoked at the end of
a scheduled collection. This is primarily handled by a source-specific `diff()`
method.

