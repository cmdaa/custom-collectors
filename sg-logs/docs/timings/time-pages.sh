#!/bin/bash
#
#
# Time retrieval of available pages
#
if [ $(id -u) -ne 0 ]; then
  echo "*** Must be run as root"
  exit -1
fi
SRC_ROOT=$(realpath "$(dirname "$0")/../..")

 DISK=${1:-/dev/sdh}
ITERS=100

# Identify all available pages, excluding
PAGES=( $(sg_logs $DISK -ll | awk '
  /,0xff/ {if ($1 !~ /0x00/) {next}}
  /^ +0x/ {print $1}') )

# Output basic identification of the target
${SRC_ROOT}/sg-logs.sh ${DISK}
for PAGE in ${PAGES[@]}; do
  printf "%-10s: " "$PAGE"

  SUM=0
  for IDEX in $(seq 1 $ITERS); do
    START_TS=$(date -u +%s.%N)

    sg_logs ${DISK} --page=${PAGE} -r > /dev/null 2>&1
    rc=$?

    END_TS=$(date -u +%s.%N)

    # Since we're using fractions, use 'dc' (reverse polish notation calculator)
    SUM=$(dc -e "7k $SUM $END_TS $START_TS -+p")

    if [ $(( $IDEX % 10 )) -eq 0 ]; then
      [ $rc -eq 0 ] && echo -n "." || echo -n "*"
    fi
  done

  AVG=$(dc -e "7k $SUM $ITERS /p")

  printf " %7.4f total : %7.4f avg\n" "$SUM" "$AVG"

done
