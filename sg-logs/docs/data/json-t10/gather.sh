#!/bin/bash
#
DRIVE="/dev/sg5"
T10_SOURCES=(
  "0x00,00"   # Supported Log Pages
  "0x00,ff"   # Supported Log Pages and Subpages

  "0x02,00"   # Write      Error Counter
  "0x03,00"   # Read       Error Counter
  "0x05,00"   # Verify     Error Counter
  "0x06,00"   # Non-Medium Error Counter
  "0x08,00"   # Format Status

 #"0x0c,00"   # Logical Block Provisioning

  "0x0d,00"   # Temperature
  "0x0d,01"   # Environmental Reporting
  "0x0d,02"   # Environmental Limits

  "0x0e,00"   # Start-Stop Cycle Counter
 #"0x0e,01"   # Utilization

  "0x0f,00"   # Application Client

  "0x10,00"   # Self-Test Results
 #"0x11,00"   # Solid State Media

  "0x15,00"   # Background Scan
  "0x15,01"   # Pending Defects
 #"0x15,02"   # Background Operation

  "0x18,00"   # Protocol Specific Port

 #"0x19,00"   # General Statistics

  "0x1a,00"   # Power Condition Transitions
  "0x2f,00"   # Informational Exceptions

  "0x30,00"   # ???
  "0x33,00"   # ???
  "0x36,00"   # ???

  "0x37,00"   # Cache Statistics

  "0x38,00"   # ???
  "0x3d,01"   # ???
  "0x3d,02"   # ???
  "0x3d,03"   # ???
  "0x3d,04"   # ???
  "0x3d,05"   # ???

  "0x3e,00"   # Factory Log
  "SMART"

)

for src in ${T10_SOURCES[@]}; do
  sudo ../../../etc/get-parsed-data.sh \
          --source $src \
            $DRIVE > $src.json
done

sudo ../../../etc/sg-logs.sh \
        --metric-sources \
        --metricize \
        --report-level 100 \
        --publish-format json \
          $DRIVE > metrics.json
