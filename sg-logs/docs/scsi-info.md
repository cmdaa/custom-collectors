### Identifying SCSI disks
`sg-logs` follows the lead of
[lsscsi](https://github.com/hreinecke/lsscsi/blob/v0.28/src/lsscsi.c#L3088) in
using `/sys/bus/scsi/devices` to identify the set of available SCSI disks.
Within this sys directory, SCSI targets will have names of the form:
```
  ^([0-9]+):([0-9]+):([0-9]+):([0-9]+)$
    host   : channel: target : lun
```

[lsscsi](https://github.com/hreinecke/lsscsi/blob/v0.28/src/lsscsi.c#L3025)
ignores entries:
- containing    : 'mt', 'ot', 'gen' (kernel < 2.6)
- beginning     : 'host', 'target'
- NOT containing: ':'


For each SCSI target, follow the soft-link to a sys directory containing
information about the target. Retrieve the contents of the `type` sys file
which identifies the SCSI device type:
```
  0x00    : DISK        # Direct access block device (e.g. magnetic disk)
  0x01    : TAPE        # Sequential access device (e.g. magnetic tape)
  0x02    : PRINTER   
  0x03    : PROCESSOR 
  0x04    : WO          # Write once device (e.g. some optical disks)
  0x05    : MMC         # CD/DVD/BD (multi-media)
  0x06    : SCANNER     # (obsolete)
  0x07    : OPTICAL     # Optical memory device (e.g. some optical disks)
  0x08    : MCHANGER    # Media changer (e.g. tape robot)
  0x09    : COMMS       # Communications device (obsolete)
  0x0a-0b : (obsolete)
  0x0c    : SAC         # Storage array controller (e.g. RAID)
  0x0d    : SES         # SCSI Enclosure Services (SES)
  0x0e    : RBC         # Reduced Block Commands (simplified DISK)
  0x0f    : OCRW        # Optical card reader/writer
  0x10    : BCC         # Bridge Controller Commands
  0x11    : OSD         # Object Storage Device (OSD)
  0x12    : ADC         # Automation/Drive Commands (ADC)
  0x13    : SMD         # Security Manager Device (SMD)
  0x14    : ZBC         # Zoned Block Commands (ZBC)
  0x15-1d : (reserved)
  0x1e    : WLUN        # Well known logical unit (WLUN)
  0x1f    : UNKNOWN     # Unknown or no device type
  0xff    : ANY         # ANY device type
```

Limit the final set of targets to those that are identified as SCSI disks
(type: 0x00). For any that are controlled by a MegaRaid controller, expand the
presented logical disk into the full set of physical disks that underly the
logical disk.

The final step is to retrieve the serial number for each physical disk and
exclude any duplicate drives. Duplicates indicate that there are multiple SCSI
paths leading to the same physical disk (e.g. multi-path controllers).


### Identifying the set of available metrics
Given the set of unique SCSI disks, retrieve the set of metric sources (e.g.
SCSI logs page, SMART data) supported by each drive. Metric collection is
scheduled by metric source.
