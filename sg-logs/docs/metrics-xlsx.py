#!/usr/bin/env python3
#
# Reading single-line JSON-encoded metrics from stdin, generate an Excel (XLSX)
# file that records the set of available metrics, grouping metric details
# beneath a single-row metric summary
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import sys

import json
import xlsxwriter

def output_header( worksheet, row ): #{
  #print('#%s,%s,%s,%s,%s,%s,%s,%s,%s' %
  #      ('page','subpage','code','offset','vendor',
  #       'metric/label name','metric/label value','units','detail'))
  worksheet.write( row, 0, 'page' )
  worksheet.write( row, 1, 'subpg' )
  worksheet.write( row, 2, 'code' )
  worksheet.write( row, 3, 'offset' )
  worksheet.write( row, 4, 'vendor' )
  worksheet.write( row, 5, 'metric/label name' )
  worksheet.write( row, 6, 'metric/label value' )
  worksheet.write( row, 7, 'units' )
  worksheet.write( row, 8, 'detail' )

  worksheet.set_row(row, None, None, {'collapsed':True} )
#}

def create_xlsx( filename, exclude_propin=False ): #{
  workbook  = xlsxwriter.Workbook( filename )
  worksheet = workbook.add_worksheet()

  # Create available formats
  base_props = {
    'valign'    : 'top',
    'font_name' : 'Libration Sans',
    'font_size' : 10,
  }
  cell_fmt = workbook.add_format( base_props )

  detail_fmt = workbook.add_format( base_props )
  detail_fmt.set_text_wrap()

  propin_base = base_props.copy()
  propin_base.update({'font_color': '#770000'})

  propin_fmt = workbook.add_format( propin_base )

  propin_detail = workbook.add_format( propin_base )
  propin_detail.set_text_wrap()

  sum_fmt = workbook.add_format( base_props )
  sum_fmt.set_bold()
  sum_fmt.set_align( 'right' )

  # Establish worksheet settings
  worksheet.set_landscape()
  #worksheet.set_print_scale( 68 )
  #worksheet.fit_to_pages( 1, 40 )
  worksheet.fit_to_pages( 1, 0 )  # 1 page wide, as high as needed

  # Establish column widths and default formats
  #worksheet.set_column( first, last, width, format, options )
  worksheet.set_column( 0, 0, 5,  cell_fmt )    # page
  worksheet.set_column( 1, 1, 5,  cell_fmt )    # subpage
  worksheet.set_column( 2, 2, 7,  cell_fmt )    # code
  worksheet.set_column( 3, 3, 7,  cell_fmt )    # offset
  worksheet.set_column( 4, 4, 10, cell_fmt )    # vendor
  worksheet.set_column( 5, 5, 44, cell_fmt )    # name
  worksheet.set_column( 6, 6, 17, cell_fmt )    # value
  worksheet.set_column( 7, 7, 15, cell_fmt )    # units
  worksheet.set_column( 8, 8, 62, detail_fmt )  # detail

  worksheet.outline_settings( visible       = True,
                              symbols_below = False,
                              symbols_right = True,
                              auto_style    = False )


  ###
  # Generate rows from the incoming JSON data
  #
  lastPage    = None
  row         = 0
  cnt_total   = 0
  cnt_propin  = 0
  cnt_unknown = 0
  cnt_lines   = 0
  cnt_errors  = 0
  for line in sys.stdin: #{
    cnt_lines += 1

    if len(line) < 3: continue

    try: #{
      sample = json.loads( line )

    except Exception as ex: #}{
      if cnt_errors == 0: #{
        print('*', file=sys.stderr)
      #}

      print('*** Error on line %d: %s [ %s ]'
            % (cnt_lines, ex, line), file=sys.stderr)
      cnt_errors += 1
      continue
    #}

    name   = sample.get('name',  'UNKNOWN')
    value  = sample.get('value', 'UNSET')
    labels = sample.get('labels',{})

    page    = ''
    subpage = ''
    code    = ''
    offset  = ''
    vendor  = labels.get('vendor_name', '')
    units   = labels.get('units',       '')
    detail  = None

    ##
    # Is this a propin metric?
    is_propin = labels.get('propin', False)
    if is_propin and exclude_propin: #{
      continue
    #}

    ##
    # Identify the data source (page,subpage,code,offset)
    #
    src = labels.get('metric_source', None)
    if src is not None: #{
      if src.startswith('scsi-log-page'): #{
        # SCSI LOG page:
        #   scsi-log-page:page,subpage[:code[:offset]]
        #                 +----------+
        #                    src
        # Extract : src:code:offset from `src[14:]`
        _parts  = src[14:].split(':')
        src     = (_parts[0] if len(_parts) > 0 else '')
        code    = (_parts[1] if len(_parts) > 1 else '')
        offset  = (_parts[2] if len(_parts) > 2 else '')

        # Extract : page,subpage from `src`
        _parts  = src.split(',')
        page    = (_parts[0] if len(_parts) > 0 else src)

        if len(_parts) > 1: #{
          subpage = ('0x%s' % (_parts[1]))
        #}

      else: #}{
        # NOT a SCSI LOG page : record 'page' as src
        page = src
      #}

    else: #}{
      src  = 'UNKNOWN'
      page = src
    #}


    ###
    # Generate 'detail'
    #
    if page == '0x0': #{
      _page    = ''
      _subpage = ''

      _page_sub = labels.get('log_page', '')
      parts        = _page_sub.split(',')
      _page     = (parts[0] if len(parts) > 0 else _page_sub)
      _subpage  = (parts[1] if len(parts) > 1 else '')

      detail = ('Log Page %s,%s' % (_page,_subpage)) + ' : '

      page_name = labels.get('log_page_name', '')
      if not page_name.startswith('Log Page'): detail += page_name
      else:                                    detail += 'UNKNOWN PAGE'

    else :#}{
      detail                     = labels.get('stat_type',    None)
      if detail is None:  detail = labels.get('scan_type',    None)
      if detail is None:  detail = labels.get('status',       None)
      if detail is None:  detail = labels.get('log_page_name',None)
    #}


    if detail is None:  detail = ''

    sgt = labels.get('seagate_type', None)
    if sgt is not None: detail += ' / ' + sgt

    hd  = labels.get('disk_head', None)
    if hd is not None:  detail += ' [head #'+ hd +']'

    idx = labels.get('lct_index',
            labels.get('h2sat_index',
              labels.get('h2sat_cylinder_index',
                labels.get('task_activity_index',
                  labels.get('voltage_index', None) ) ) ) )
    if idx is not None: detail += ' [index #'+ idx +']'


    if units == 'flag': #{
      flag  = labels.get('flag_expanded', None)
      if flag is not None:  detail += ' { '+ flag +' }'
    #}

    ###
    # Generate page identification and output a section header if needed
    #
    pageId = '%s,%s' % (page,subpage)
    if pageId != lastPage: #{
      # Output a header/divider
      lastPage = pageId

      output_header( worksheet, row )
      row += 1
    #}

    ###
    # Determine the generate cell format (propin / not-propin)
    #
    cnt_total += 1
    if name.find('unknown') >= 0: cnt_unknown += 1

    if is_propin: #{
      cnt_propin += 1

      fmt_cell   = propin_fmt
      fmt_detail = propin_detail
    else: #}{
      fmt_cell   = cell_fmt
      fmt_detail = detail_fmt
    #}

    ###
    # Generate the summary row
    #
    worksheet.write( row, 0, page,    fmt_cell )
    worksheet.write( row, 1, subpage, fmt_cell )
    worksheet.write( row, 2, code,    fmt_cell )
    worksheet.write( row, 3, offset,  fmt_cell )
    worksheet.write( row, 4, vendor,  fmt_cell )
    worksheet.write( row, 5, name,    fmt_cell )
    worksheet.write( row, 6, value,   fmt_cell )
    worksheet.write( row, 7, units,   fmt_cell )
    worksheet.write( row, 8, detail,  fmt_detail )

    worksheet.set_row(row, None, None, {'collapsed':True} )

    ###
    # Generate the detail rows
    #
    for name,val in labels.items(): #{
      row += 1
      worksheet.write( row, 5, ('  %s' % (name)), fmt_cell )
      worksheet.write( row, 6, val,               fmt_cell )

      worksheet.set_row( row, None, None, {'level':1, 'hidden':True} )
    #}

    row += 1
  #}

  ###
  # Create final rows that summarize the type and number of available metrics
  #
  last_row = row - 1
  worksheet.write_formula( row, 0, ('=COUNTA(A2:A%d)-'
                                     'COUNTIF(A2:A%d,"page")' % (row, row)),
                                            sum_fmt, cnt_total )
  worksheet.write_formula( row, 1, ('=A%d-C%d' % (row+1,row+1)),
                                            sum_fmt, (cnt_total - cnt_propin ))
  worksheet.write_formula( row, 2, ('=COUNTIF(F2:F%d,"  propin")' % (row)),
                                            sum_fmt, cnt_propin )
  worksheet.write_formula( row, 3, ('=COUNTIF(F2:F%d,"*unknown")' % (row)),
                                            sum_fmt, cnt_unknown )

  row += 1
  worksheet.write( row, 0, 'total',   sum_fmt )
  worksheet.write( row, 1, 'open',    sum_fmt )
  worksheet.write( row, 2, 'propin',  sum_fmt )
  worksheet.write( row, 3, 'unknwn',  sum_fmt )
  worksheet.write( row, 4, 'metrics', sum_fmt )

  workbook.close()
#}

def main(): #{
  import argparse

  parser = argparse.ArgumentParser(
              prog        = 'metrics-xlsx',
              description = 'Create an Excel spreadsheet from sg-logs metrics')
  parser.add_argument(
      '--no-propin', '-P',
      action  = 'store_const',
      const   = True,
      help    = 'Exclude proprietary (propin) metrics' )

  parser.add_argument(
      'output',
      nargs   = '?',
      default = 'metrics.xlsx',
      help    = 'The name of the Excel file to generate.' )

  args = parser.parse_args()

  create_xlsx( args.output, args.no_propin )
#}

if __name__ == '__main__': #{
  main()
#}
