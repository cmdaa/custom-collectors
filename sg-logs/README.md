Prometheus-compatible collection of disk metrics via `sg3_utils`.

This makes use of the [python-sgutils](../python-sgutils)
binding to the custom [sg3_utils](../sg3_utils)
to provide SCSI LOG SENSE-based metrics collection.

**NOTE:** Until fully installed on the system, [sg-logs](sg-logs) must be run
from within a python virtual environment. This environment may be created
using the [etc/env-make.sh](etc/env-make.sh) helper script.

The [etc/sg-logs.sh](etc/sg-logs.sh) helper script ensures that the virtual
environment is active before invoking the [sg-logs](sg-logs) python script.

**Table of Contents**

[[_TOC_]]

---
### Usage
All available arguments:
```
usage: sg-logs [-h] [--config CONFIG] [--extension EXTENSION] [--list]
               [--one-shot] [--source SOURCE] [--parse-unknown-pages]
               [--period PERIOD] [--publish PUBLISH]
               [--publish-format PUBLISH_FORMAT] [--raw]
               [--report-level REPORT_LEVEL] [--no-report-unknown-params]
               [--metric-sources] [--verbosity] [--version]
               [target]

A SCSI disk metrics collector that: 1) locates all SCSI disks connected to the
system, including those behind a MegaRaid controller; 2) retrieves the
available metric sources (e.g. SCSI LOG SENSE pages) for each SCSI disk,
identifying the cost of fetching each metric source; 3) generates a collection
schedule for all metric sources to ensure application access to disk data is
minimally impacted by log collection activities; 4) runs the collection
schedule, fetching disk metrics as proscribed by the schedule and sending them
on to a configurable publication location (e.g. stdout, tcp socket) in a
configurable format (e.g. json | prometheus);

positional arguments:
  target                A single target to probe (e.g. /dev/sda,
                        /dev/sdho:MR:8).

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        The path to an INI formatted configuration file
  --extension EXTENSION, -e EXTENSION
                        Extension to load
  --list, -l            List the available drives
  --one-shot, -o        Perform a single collection schedule/period and
                        terminate
  --source SOURCE, -s SOURCE
                        Show the given metric source (e.g. log-page[,subpage])
                        for each drive (implies either --list or a specific
                        target)
  --parse-unknown-pages, -u
                        Parse pages that are not explicitly known
  --period PERIOD, -P PERIOD
                        An override of the scheduling period (implies either a
                        scan or a specific target) (default: 5m)
  --publish PUBLISH, -O PUBLISH
                        Set/Override the publish setting (default: stdout)
  --publish-format PUBLISH_FORMAT, -F PUBLISH_FORMAT
                        Set/Override the publish format (prometheus | json)
                        (default: prometheus)
  --raw, -r             Used with --page to output the raw sense data
  --report-level REPORT_LEVEL, -R REPORT_LEVEL
                        Set/Override the report level (default: 1)
  --no-report-unknown-params, -U
                        Within parsed pages, do NOT report parameters that are
                        not explicitly known (e.g. vendor-specific, reserved)
  --metric-sources, -S  Show the set of supported metric sources for each
                        drive (implies either --list or a specific target)
  --verbosity, -v       Increase verbosity
  --version, -V         Show the version and exit
```

### Configuration
sg-logs is configured using an INI-formatted file. The default
(`/etc/sg-logs/config.ini`) may be over-ridden using the `--config CONFIG`
parameter.

In combination with command-line arguments, this configuration file specifies
the operating conditions for sg-logs.

The `config.ini` is expected to follow the following format, which represents
the full set of available options and their defaults:
```ini
# Defaults applied to all other sections
[DEFAULT]
DEBUG             = False
publish           = stdout                    # --publish stdout
publish_format    = prometheus                # --publish-format prometheus
include_timestamp = False

# Log page / metric source parsing, meta-data, and reporting
[log_page]
db                    = scsi_log_page_db.yml
parse_unknown_pages   = False                 # --parse-unknown-pages
report_unknown_params = True                  # --no-report-unknown-params
report_level          = 1                     # --report-level 1

# Disk scanning options
[scan]
include_scsi  = True

# Collection scheduling
[scheduler]
# `period` specifies the time frame (aka beat) during which all metrics should
# be collected/reported. If there has been previous collection, the set of
# reported metrics will only be those that changed since the previous
# collection (differential reporting).
period          = 5m                          # --period 5m

# `sync_beat` specifies the number of periods (beats) at which all metrics will
# be reported regardless of differences (full report).
sync_beat       = 6

# `one_shot` specifies that a single collection schedule/period should occur,
# after which sg-logs will exit.
one_shot        = False                       # --one-shot

# AP Scheduler settings
thread_pool     = 10
reconnect_delay = 5s

# Custom Parser Extensions
# Every extension marked True would be loaded first, with the current
# --extension argument allowing additional run-time extensions.
[extensions]
parser_extension_1 = True
parser_extension_2 = False
```

#### <a name='publish_locations'>Available publish locations</a>
- `stdout` [default];
- `stderr`;
- `tcp://%host%:%port%`;


#### <a name='publish_formats'>Available publish formats</a>
- `prometheus` [ default ] &mdash; this format generates a single-line string
  for each metric that adheres to the
  [Prometheus Exposition Format](https://prometheus.io/docs/instrumenting/exposition_formats/).
  For example (shown as multiple lines for ease of viewing):
  ```python
  device_storage_environment_temperature{
    dev_paths="/dev/sdgn,/dev/sg198",
    dev_type="disk",
    disk_type="hdd",
    metric_source="scsi-log-page:0xd,0:0x00:0",
    model_id="GTG123456ZM7890",
    mount_points="",
    node="fqdn.hostname.com",
    revision_id="D27E",
    serial_id="3SFYDQ6C",
    stat_type="current",
    units="celsius",
    vendor_name="HGST"
  } 37 1603384176
  ```
- `json` &mdash; this format generates a single-line JSON-formatted string for
  each metric with the form:
  ```javascript
  {
    "name"        : str,      /* Metric name
                               *   e.g. device_storage_environment_temperature,
                               *        device_storage_read_error,
                               *        device_storage_write_error
                               */

    "labels"      : { ... },  /* Labels identifying metric context, for
                               * example:
                               *    node  FQDN of collecting node/host,
                               *    units the unit of measure for `value`,
                               *
                               *    dev_paths, dev_type, disk_type,
                               *    metric_source, model_id, mount_points,
                               *    revision_id, serial_id, stat_type,
                               *    vendor_name
                               */

    "value"       : num,      // Integer or Float
    "timestamp"   : u64       // Optional timestamp (milliseconds since epoch)
  }
  ```

#### Simplified metrics
Simplified metrics, generated via `parse()` and `parse_source()`, is a list of
entries each having the form:
``` python
{
  name      : (str),        # metric name
  value     : (int|float),  # metric value
  labels    : {             # a set of meta-data about this metric
    metric_source : (str),  # REQUIRED: the source of this metric
                            #
                            #   If the source is a SCSI LOG PAGE, the format
                            #   must be:
                            #     'scsi-log-page:0x<hex-page>:<hex-subpage>'
                            #
                            #     If available, additional information must be
                            #     appended in the order:
                            #         ':0x<hex-parameter-code>'
                            #         ':<decimal-parameter-offset>'
                            #
                            #     For this string, both hex and decimal formats
                            #     should use the minimum number of digits
                            #     necessary to represent the value.
                            #
                            #     :NOTE: The parameter offset is the byte
                            #            offset to the first byte of the
                            #            parameter from the beginning of the
                            #            parameter list. The first parameter in
                            #            the list would have offset 0.
                            #
    units         : (str),  # REQUIRED: the unit of measure for `value`
    ...
  },
  [timestamp: (int)],       # An optional timestamp (milliseconds since epoch)
}  
```

For SCSI LOG pages that report supported log pages/subpages (e.g. SCSI LOG
Pages `0x00,00` and `0x*,ff`), the following labels, primarily from [log page
metadata](#log_page_metadata), are also required:
``` python
labels    : {               # additional required labels
  log_page      : (str),    # '0x<hex-page>,<hex-sub-page>'

  log_page_name : (str),    # Lower-case page name
  log_page_level: (uint),   # Report level for the page [0]
  log_page_cost : (float),  # page retrieval/parsing cost (0 .. 1 ) [1.0]
}
```


#### Default metric sources
There are currently two metric sources that each have default implementations
for `fetch()` and `parse()` and may used without any extension.

*NOTE:* Additional metric sources and/or metric source processing may be added using [custom extension modules](#custom-extensions).

##### SCSI LOG page
The [SCSI LOG page](sg_logs/scsi/log_page/__init__.py) metric source:
- `fetch()` &mdash; the default operation retrieves SCSI LOG sense data via
  `sg3utils.log_sense()` and returns data in the form:
  ```python
  { disable_save  : (bool),
    subpage_format: (int),
    page          : (int),
    subpage       : (int),
    params        : (list of dicts),
  }

  # For page 0, subpage 0, each entry in `params` will be:
  { page: (int) }

  # For page *, subpage 0xff, each entry in `params` will be:
  { page: (int), subpage: (int) }

  # For all other log pages/subpages, each entry in `params` will be:
  { code                        : (int),

    # Byte offset within the parameter list
    offset                      : (int),

    # Raw bytes of the parameter value
    value_raw                   : (bytearray),

    # Parameter value interpreted according to `format_and_linking`
    value                       : (str | int | bytearray),

    # From the parameter control byte {
    disable_update              : (bool),
    target_save_disable         : (bool),
    enable_threshold_comparison : (bool),

    threshold_met_criteria      : (int),
    # 0x00  every update of the cumulative value;
    # 0x01  Cumulative value  equal to      threshold value;
    # 0x02  Cumulative value  not equal to  threshold value;
    # 0x03  Cumulative value  greater than  threshold value;

    format_and_linking          : (int),
    # 0x00  Bounded data counter                            (=> int)
    # 0x01  ASCII format list                               (=> str)
    # 0x02  Bounded data counter or unbounded data counter  (=> int)
    # 0x03  Binary format list                              (=> bytearray)
    # From the parameter control byte }
  }
  ```
- `parse()` &mdash; the default operation  uses a T10-based parser
  ([scsi/log_page/parser/t10](sg_logs/scsi/log_page/parser/t10.py)) to generate
  [simplified metrics](#simplified-metrics) from the data retrieved via
  `fetch()`;

##### SCSI SMART
The [SCSI SMART](sg_logs/scsi/smart.py) metric source:
- `fetch()` &mdash; the default operation retrieves SMART-like SCSI data from a
  number of SCSI MODE (0x1c : Informational Exceptions Control Mode) and LOG
  (0x2f : Informational Exceptions, 0x3e : Factory) pages using
  `sg3utils.mode_sense()` and `sg3utils.log_sense()`;  
- `parse()` &mdash; the default operation generates SMART-like
  [simplified metrics](#simplified-metrics) from the data retrieved via
  `fetch()`;

---
### Operations
When run with no arguments, [sg-logs](sg-logs) will:
- use the default [configuration](#configuration) to establish operating
  conditions;
- [scan the system](#scan) for all available unique SCSI disks
  (unique according to disk serial number), fetching the supported metric
  sources (e.g. [SCSI LOG pages](#scsi-log-page), [SCSI SMART](#scsi-smart)) for
  each. This process will also augment SCSI LOG pages based upon [log page
  metadata](#log_page_metadata) from any configured SCSI log page database;
- generate and start a random collection schedule for the set of unique disks
  and metric sources that will result in collection of metrics from all sources
  with a `log_page_level` less than the currently configured `report_level`
  within the configured collection period;
- when a scheduled collection triggers, if the source has a `log_page_level`
  less than the currently configured `report_level`, [gather and
  report](#gather_and_report), otherwise, skip the source;
- when all scheduled jobs complete, whether successfully or with an error,
  generate and start a new, random collection schedule for the next collection
  period;

#### <a name='scan'>Scanning for SCSI disks</a>
SCSI devices are identified via
[sys-fs](https://www.kernel.org/doc/Documentation/filesystems/sysfs.txt)
&mdash; in particular, entries from `/sys/bus/scsi/devices/` with the form
`[0-9]+:[0-9]+:[0-9]+:[0-9]+`, representing SCSI `host:channel:target:lun`.

This process is handled by [SgLogs](sg_logs/__init__.py) `scan()`.

For each SCSI drive (`%sys-path% = /sys/bus/scsi/devices/%SCSI-addr%/`), a
[ScsiDrive](sg_logs/drive/scsi.py) instance will be created and additional
information will be immediately retrieved:
- `sg3` : the [python-sgutils](../python-sgutils) extension (`sg3utils`) will
  be used to open a handle to the device;

  if the SCSI device is controlled by a MegaRaid controller
  (`sg3utils.is_mr(sg3)`), the `sg3` handle provides access to the controller.
  In order to access physical disks behind the controller, the set of physical
  disks must be enumerated (`sg3utils.mr_get_pd_list(sg3)`) and an individual
  [MrPDrive](sg_logs/drive/mr_pd.py) instance created for each targeting a
  specific disk (`sg3utils.mr_set_target(sg3, pd_id)`);
- `scsi` : SCSI address for this device ([ScsiAddr](sg_logs/scsi/addr.py));
- `dev_type` : SCSI device type (`%sys-path%/type`) converted via
  [ScsiType.str](sg_logs/scsi/type.py#L36) to a string;
- `dev_paths` : associated block device(s) as a list
  (`%sys-path%/block/%name%`) which *may* end up containing more than one entry
  if there are multiple paths to the same (according to serial number)
  SCSI device;

  **NOTE:** For disks behind a MegaRaid controller, the entries in
  `dev_paths` will be modified to identify the target physical disk
  (e.g. `/dev/sda` targeting physical disk 32 will be modified to
        `/dev/sda:MR:32`);

- `sg_path` : associated SCSI generic device (`%sys-path%/scsi_generic/%name%`
    or `%sys-path%/generic/%name%`);


For each SCSI device, additional information is available on-demand:
- `vendor` : SCSI vendor from `%sys-path%/vendor` or SCSI inquiry
  (VPD 0x0, simple);
- `model` : SCSI model from `%sys-path%/model` or SCSI inquiry
  (VPD 0x0, simple);
- `rev` : SCSI firmware revision from `%sys-path%/rev` or SCSI inquiry
  (VPD 0x0, simple);
- `serial` : SCSI serial number from `%sys-path%/vpd_pg80` or SCSI inquiry
  (VPD 0x80, serial number);
- `disk_type` : Disk type (hdd | ssd) from SCSI inquiry
  (VPD 0xb1, device characteristics);
- `mount_points` : Partition/filesystem dictionary populated by searching
  `/etc/mtab` for entries matching any item from `dev_paths`;
- `smart` : S.M.A.R.T. information ([Smart](sg_logs/smart.py));


#### <a name='log_page_metadata'>SCSI Log page meta-data</a>
Additional meta-data about SCSI LOG pages is provided by the
[SCSI Log Page Database](sg_logs/scsi/log_page/db.py). This in-memory database
contains SCSI disk families, identification heuristics, and a list of available
SCSI LOG pages/sub-pages with name, cost, and level information.

By default, this database has [a single entry](#database-default), but can be
augmented at run-time via the `log_page.db`
[configuration setting](#configuration) and/or a parser/extension's
`augment_log_page_db()` method.

This SCSI LOG page database is used when the set of available SCSI drives is
generated ([SgLogs](sg_logs/__init__.py) `scan()`). Once the set of drives is
retrieved:
- each drive will be augmented with a log page map via
  [SgLogs](sg_logs/__init__.py) `add_page_map()`. This method will pass
  the drive `vendor`, `model`, and `revision` to
  [Db](sg_logs/scsi/log_page/db.py) `get_page_map()`;
- [Db](sg_logs/scsi/log_page/db.py) `get_page_map()` will search the database
  for the best match for the provided vendor/model/revision. If a match if
  found, the drive will be augmented with additional information about the log
  pages that *should be* available for that drive. If no match is found,
  default values (name: [0x##,##], level: 0, cost: 1.0) will be used for all
  log pages accessed from that drive;


##### <a name='db_format'>Database file format</a>
The database file (identified via the `log_page.db`
[configuration setting](#configuration)) is expected to be YAML-formatted with
a top-level list of family items of the form:
```yaml
- family        : str   // Family name
  vendor_regex  : str   // optional regular expression for drive `vendor`
  model_regex   : str   // optional regular expression for drive `model`
  rev_regex     : str   // optional regular expression for drive `revision`
  page:                 // page/sub-page meta-data
    %page-hex%:
      %sub-page-hex%:
        name  : str     // required sub-page name
        cost  : float   // optional retrieval cost estimate (0 .. 1) [1.0]
                        // where 0 is free and 1 is extremely expensive
        level : uint    // optional minimum report level [0]
      ...
    ...
...
```

##### Database default
The initial, default SCSI LOG page database is established via `Default` in
[sg_logs/scsi/log_page/db.py](sg_logs/scsi/log_page/db.py):
```python
{
  'family':         'DEFAULTS',
  'vendor_regex':   None,
  'model_regex':    None,
  'firmware_regex': None,
  'warning':        'Default settings',
  #
  # The full set of known pages/subpages, names, and default cost
  #
  # Defaults are:
  #   name:   'Log Page 0x##,##'
  #   cost:   1.00
  #   level:  0
  #
  'page': {
    0x00: {
      0x00:       {'name': 'supported log pages',                   'cost' : 0.25},
      0xff:       {'name': 'supported log pages/subpages',          'cost' : 0.25},
    },
    0x01: { 0x00: {'name': 'buffer over/under-run',                 'cost' : 0.25}},
    0x02: { 0x00: {'name': 'write error counter',                   'cost' : 0.25}},
    0x03: { 0x00: {'name': 'read error counter',                    'cost' : 0.25}},
    0x05: { 0x00: {'name': 'verify error counter',                  'cost' : 0.25}},
    0x06: { 0x00: {'name': 'non medium error counter',              'cost' : 0.25}},
    0x07: { 0x00: {'name': 'last n error counter',                  'cost' : 0.25}},

    0x08: { 0x00: {'name': 'format status',                         'cost' : 0.50,
                                                                    'level': 10 }},
    0x0b: {
      0x00:       {'name': 'last n deferred error counter',         'cost' : 0.25},
      0x01:       {'name': 'last n inquiry data changed',           'cost' : 0.25},
      0x02:       {'name': 'last n mode page data changed',         'cost' : 0.25},
    },
    0x0c: { 0x00: {'name': 'logical block provisioning',            'cost' : 0.25}},
    0x0d: {
      0x00:       {'name': 'temperature',                           'cost' : 0.25},
      0x01:       {'name': 'environmental reporting',               'cost' : 0.25},
      0x02:       {'name': 'environmental limits',                  'cost' : 0.25},
    },
    0x0e: {
      0x00:       {'name': 'start-stop cycle counter',              'cost' : 0.25},
      0x01:       {'name': 'utilization',                           'cost' : 0.25},
    },
    0x0f: { 0x00: {'name': 'application client',                    'cost' : 0.75,
                                                                    'level': 50 }},
    0x10: { 0x00: {'name': 'self-test results',                     'cost' : 0.25}},
    0x11: { 0x00: {'name': 'solid state media',                     'cost' : 0.25}},
    0x14: { 0x00: {'name': 'zoned block device statistics',         'cost' : 0.25}},
    0x15: {
      0x00:       {'name': 'background scan results',               'cost' : 1.00},
      0x01:       {'name': 'pending defects',                       'cost' : 1.00},
      0x02:       {'name': 'background operation',                  'cost' : 1.00},
    },
    0x16: { 0x00: {'name': 'ata pass-through results',              'cost' : 0.25}},
    0x17: { 0x00: {'name': 'non-volatile cache',                    'cost' : 0.25}},
    0x18: { 0x00: {'name': 'protocol specific port',                'cost' : 0.25}},
    0x19: {
      0x00:       {'name': 'general statistics and performance',    'cost' : 0.50},
      0x01:       {'name': 'group 0x01 statistics and performance', 'cost' : 0.50},
      0x02:       {'name': 'group 0x02 statistics and performance', 'cost' : 0.50},
      0x03:       {'name': 'group 0x03 statistics and performance', 'cost' : 0.50},
      0x04:       {'name': 'group 0x04 statistics and performance', 'cost' : 0.50},
      0x05:       {'name': 'group 0x05 statistics and performance', 'cost' : 0.50},
      0x06:       {'name': 'group 0x06 statistics and performance', 'cost' : 0.50},
      0x07:       {'name': 'group 0x07 statistics and performance', 'cost' : 0.50},
      0x08:       {'name': 'group 0x08 statistics and performance', 'cost' : 0.50},
      0x09:       {'name': 'group 0x09 statistics and performance', 'cost' : 0.50},
      0x0a:       {'name': 'group 0x0a statistics and performance', 'cost' : 0.50},
      0x0b:       {'name': 'group 0x0b statistics and performance', 'cost' : 0.50},
      0x0c:       {'name': 'group 0x0c statistics and performance', 'cost' : 0.50},
      0x0d:       {'name': 'group 0x0d statistics and performance', 'cost' : 0.50},
      0x0e:       {'name': 'group 0x0e statistics and performance', 'cost' : 0.50},
      0x0f:       {'name': 'group 0x0f statistics and performance', 'cost' : 0.50},

      0x10:       {'name': 'group 0x10 statistics and performance', 'cost' : 0.50},
      0x11:       {'name': 'group 0x11 statistics and performance', 'cost' : 0.50},
      0x12:       {'name': 'group 0x12 statistics and performance', 'cost' : 0.50},
      0x13:       {'name': 'group 0x13 statistics and performance', 'cost' : 0.50},
      0x14:       {'name': 'group 0x14 statistics and performance', 'cost' : 0.50},
      0x15:       {'name': 'group 0x15 statistics and performance', 'cost' : 0.50},
      0x16:       {'name': 'group 0x16 statistics and performance', 'cost' : 0.50},
      0x17:       {'name': 'group 0x17 statistics and performance', 'cost' : 0.50},
      0x18:       {'name': 'group 0x18 statistics and performance', 'cost' : 0.50},
      0x19:       {'name': 'group 0x19 statistics and performance', 'cost' : 0.50},
      0x1a:       {'name': 'group 0x1a statistics and performance', 'cost' : 0.50},
      0x1b:       {'name': 'group 0x1b statistics and performance', 'cost' : 0.50},
      0x1c:       {'name': 'group 0x1c statistics and performance', 'cost' : 0.50},
      0x1d:       {'name': 'group 0x1d statistics and performance', 'cost' : 0.50},
      0x1e:       {'name': 'group 0x1e statistics and performance', 'cost' : 0.50},
      0x1f:       {'name': 'group 0x1f statistics and performance', 'cost' : 0.50},

      0x20:       {'name': 'cache memory statistics',               'cost' : 0.50},
    },

    0x1a: { 0x00: {'name': 'power condition transition',            'cost' : 0.25}},
    0x2f: { 0x00: {'name': 'informational exceptions',              'cost' : 0.25}},

    ################################################################################
    # Vendor-specific (common) {
    #
    0x37: { 0x00: {'name': 'cache (common)',                        'cost' : 0.25}},
    0x3e: { 0x00: {'name': 'factory (common)',                      'cost' : 0.25}},
    # Vendor-specific (common) }
    ################################################################################
  },
}
```

#### <a name='gather_and_report'>Gather and report metrics</a>
When a metric source (e.g. SCSI LOG page for a specific SCSI disk) is triggered
for collection, [SgLogs](sg_logs/__init__.py) `metricize_source()` will be
invoked:
- the level of the metric source (`log_page_level`) is checked against the
  currently configured report level (`report_level`). If the level is
  greater-than or equal-to (>=) the configured report level, collection is
  aborted and no collection will be performed for this source;
- otherwise (if the level falls below the current report level), collection
  will proceed via [MetricSource.metricize()](sg_logs/metric_source.py):
  - [MetricSource.fetch()](sg_logs/metric_source.py) will be invoked to fetch
    the data of the target source. If the drive associated with this source has
    an attached extension, the extension will be checked for the
    `fetch_source()` method. If the method exists, it will be invoked. If data
    is returned, the data will be used for following calls. If data is NOT
    returned, the default fetch for the source will proceed (e.g.
    [ScsiLogPage.fetch()](#scsi-log-page),
    [ScsiSmart.fetch()](#scsi-smart));
  - [MetricSource.parse()](sg_logs/metric_source.py) will be invoked to convert
    the data from `fetch()` into [simplified metrics](#simplified-metrics).
    If the drive associated with this source has an attached extension, the
    extension will be checked for the `parse_source()` method. If the method
    exists, it will be invoked. If data is returned, the data will be used for
    following calls. If data is NOT returned, the default parse for the source
    will proceed (e.g.
    [ScsiLogPage.parse()](#scsi-log-page),
    [ScsiSmart.parse()](#scsi-smart));
  - [MetricSource.diff()](sg_logs/metric_source.py) will be invoked to check for
    previous collection and, if it exists, filter the
    [simplified metrics](#simplified-metrics) to include only those that have
    changed since the last collection. The default version of this diff method
    is extremely naive. It ASSUMES the metrics will ALWAYS be in the same
    order, and does a simple parallel walk of the previous and current
    [simplified metrics](#simplified-metrics) filtering out any that evaluate
    as equal according to Python dict equality
    (which includes any contained dict entries -- e.g. 'labels');
  - all remaining [simplified metrics](#simplified-metrics) will be converted
    into full [Metric](sg_logs/metric.py) instances, mixing in additional
    drive and system-related labels, and returned;
- all retrieved [Metric](sg_logs/metric.py) instances (if any) will be published
  via [Sglogs.publish_metrics()](sg_logs/__init__.py) to  the
  [configured location](#publish_locations) in the
  [configured format](#publish_formats);


---
### Custom extensions
Multiple extensions may be created and used with sg-logs. These extensions can:
- identify the support score for a specific drive &mdash;
  `get_drive_support_score( drive )`;
- augment the default SCSI LOG page database &mdash;
  `augment_log_page_db( db, verbosity )`;
- update the set of metric sources supported by a specific drive &mdash;
  `update_metric_sources( drive, sources)`;
- fetch the data for a metric source &mdash;
  `fetch_source( metric_src, verbosity )`;
- parse the data for a metric source, retrieved via `fetch_source()` &mdash;
  `parse_source( metric_src, fetch_data, report_level, verbosity )`;

The API for these extensions supports the following methods:
``` python
###############################################################################
# All methods are technically optional but at least `get_drive_support_score()`
# and one other SHOULD BE provided for this extension to have any impact.
#
def get_drive_support_score( drive ): #{
  """
    Invoked via `Sglogs.get_drive_extension()` to determine the support score
    provided by this extension for the target drive.

    This should be an indication of the number of recognized metric sources
    the extension can parse for the target drive.

    If an extension deals with SCSI LOG pages/subpages and can delegate to
    the default T10 parser:
      - the basis of this score is the T10 parser score
        (the count of t10_parser entries);
      - this score should be increased for every page/subpage the extension
        explicitly recognizes:
        - by 1.0 for those unknown to the T10 parser;
        - by 0.5 for those known   to the T10 parser but presumably known more
                                   fully by the extension;

    :NOTE: `Sglogs.get_drive_extension()` is invoked via
            - `Sglogs.scan()`
            - `Sglogs.probe_drive()`

    Args:
      drive (Drive)   The target drive

    Returns:
      (float) A score indicating how well the target drive is supported by the
              extension (0  : not at all,
                         >0 : an indication of recognized metric sources);

  """
#}  

def augment_log_page_db( db, verbosity = 0): #{
  """
    Invoked via `Sglogs` when the `db` property is first accessed. This method
    is invoked to augment the SCSI Log page database with any extension-specific
    information.

    This method may augment the database by:
      - loading a YAML file: `db.load_config( path )`
      - selectively adding entries: `db.add_entry( entry )`

    Args:
      db {ScsiLogPageDb}      The database to augment;
      [verbosity = 0] (int)   Debug verbosity;

    Returns:
      (bool)  An indication of success (True) or failure (False)
  """
#}  

def update_metric_sources( drive, sources): #{
  """
    Invoked via `Drive.get_metric_sources()` to update the current set of
    sources and/or add extension-specific metric sources.

    Args:
      drive (Drive)   The target drive;
      sources (list)  The existing set of source (MetricSource instances);

    Returns:
      (list)    The updates `sources`
  """
#}  

def fetch_source( metric_src,
                  verbosity   = 0): #{
  """
    Invoked via `metric_src.fetch()` to retrieve the raw data for the
    provided metric source.

    Args:
      metric_src (MetricSource)   The target metric source;
      [verbosity = 0] (int)       Debug verbosity;

    Returns:
      (mixed) : The raw data from this metric source.
                If None is returned, any default, non-extension fetch will be
                performed by the target `metric_src`.

    If `metric_src` is  a `ScsiLogPage` instance, and there is no corresponding
    `parse_source()` method provided by this extension, then this method MUST
    return a dict of the form:
      { disable_save  : (bool),
        subpage_format: (int),
        page          : (int),
        subpage       : (int),
        params        : (list of dicts),
      }

      For page 0, subpage 0, each entry in `params` will be:
        { page: (int) }

      For page *, subpage 0xff, each entry in `params` will be:
        { page: (int), subpage: (int) }

      For all other log pages, each entry in `params` will be:
        { code                        : (int),

          # Byte offset within the parameter list
          offset                      : (int),

          # Raw bytes of the parameter value
          value_raw                   : (bytearray),

          # Parameter value interpreted according to `format_and_linking`
          value                       : (str | int | bytearray),

          # From the parameter control byte {
          disable_update              : (bool),
          target_save_disable         : (bool),
          enable_threshold_comparison : (bool),
          threshold_met_criteria      : (int),
          format_and_linking          : (int),
          # From the parameter control byte }
        }
  """
#}  

def parse_source( metric_source,
                    fetch_data,
                    report_level    = 0,
                    verbosity       = 0,
                    parse_unknowns  = False): #{
  """
    Invoked via `metric_src.parse()` to parse data fetched via
    `metric_src.fetch()` into a list of simplified metrics.

    Args:
      metric_src (MetricSource)       The target metric source;
      fetch_data (mixed)              Data from `metric_srce.fetch()`;
      [report_level = 0] (int)        The active report level;
      [verbosity = 0] (int)           The debug verbosity;
      [parse_unknowns = False] (bool) Should unknowns be parsed
                                      (e.g. parameters);

    Returns:
      (list)  : The set of simplified metrics extracted from `fetch_data`
                where each entry has the form:
                  { name    : (str),
                    value   : (int|float),
                    labels  : {
                      metric_source : (str),
                      units         : (str),
                      ...
                    },
                    timestamp : (int) # Optional timestamp, milliseconds
                                      # since epoch;
                  }
                If None is returned, any default, non-extension parse will be
                performed by the target `metric_src`.

    If `metric_src` is  a `ScsiLogPage` instance, and there is no corresponding
    `fetch_source()` method provided by this extension, then `fetch_data`
    SHOULD BE  a dict of the form:
      { disable_save  : (bool),
        subpage_format: (int),
        page          : (int),
        subpage       : (int),
        params        : (list of dicts),
      }

      For page 0, subpage 0, each entry in `params` will be:
        { page: (int) }

      For page *, subpage 0xff, each entry in `params` will be:
        { page: (int), subpage: (int) }

      For all other log pages, each entry in `params` will be:
        { code                        : (int),

          # Byte offset within the parameter list
          offset                      : (int),

          # Raw bytes of the parameter value
          value_raw                   : (bytearray),

          # Parameter value interpreted according to `format_and_linking`
          value                       : (str | int | bytearray),

          # From the parameter control byte {
          disable_update              : (bool),
          target_save_disable         : (bool),
          enable_threshold_comparison : (bool),
          threshold_met_criteria      : (int),
          format_and_linking          : (int),
          # From the parameter control byte }
        }
  """
#}  
```

---
### Installation

Installation requires that the [python-sgutils](../python-sgutils) first be
installed.

Once that has been accomplished, you may install this (sg_logs) module:
``` bash
$ make install              # MAY require sudo to install globally

# The above is equivalent to:
$ python3 setup.py install  # Again, MAY require sudo to install globally
```

If this was successful, you should be able to run `sg-logs` from its install
location:
``` bash
$ sg-logs --help
```


For completeness, the full start-to-finish installation of all required
components would be:
``` bash
#######
# compile the custom sg3_utils
$ cd custom-collectors/sg3_utils

# If `configure` does not exist, you will first need to run ./autogen.sh
$ ./configure --with-pic
$ make
...

#######
# compile and install the python-sgutils extension (sg3utils)
#
# :NOTE: This uses the static version of the sg3_utils library
#        compiled above, directly including it in the extension
#        so the custom sg3_utils library does not need to be
#        installed itself.
#        This avoids conflicts with any existing sg3_utils
#        package that may already be installed.
#
$ cd ../python-sgutils
$ make               # or `python3 setup.py build`
...

$ sudo make install  # or `sudo python3 setup.py install`
...

#######
# install the packages required by sg-logs
#
# :NOTE: This relies on the python-sgutils extension compiled and
#        installed above
#
$ cd ../sg-logs
$ sudo pip3 install -r requirements.txt
...

# OR fully install sg-logs (along with all required packages)
$ sudo pip3 install .   # or `sudo python3 setup.py install`
...

#######
# Test sg-logs by showing the SMART status of all identified drives
#
# :NOTE: if sg-logs was NOT fully installed, you will need to use
#        the full/absolute path to sg-logs here
#
$ sudo sg-logs --smart
...
```

### Docker image

You may also create a docker image that includes a compiled version of sg-logs
and all requirements.

```
make docker-image
...
#
# Or
#   ./etc/run-build-tarball.sh
#   docker build --build-arg "DOCKER_TAG=$(make docker-tag-name)" \
#                -t "$(make docker-full-name)" .
#
```

You may run the generate image via:
```
make docker-run
...
#
# Or `docker run --rm -it $(make docker-full-name) /bin/bash`
#
```

### Tests

There are tests and related fixtures in the [tests/](tests/) sub-directory.

These make use of the [Python3 unittest
framework](https://docs.python.org/3.6/library/unittest.html) and may be run
using the [tests.sh](tests.sh) helper script. This script will check for an
`env` sub-directory and, if it exists, activate the virtual environment
represented by the sub-directory. By default, it will "discover" and run all
available tests. All unittest parameters are supported (`./tests.sh -h`).

Nearly all test cases are dynamically generated based upon tests data in
[tests/fixtures/](tests/fixtures/).


### Required build packages

#### RPM-based systems
yum install python3 python3-devel make autoconf automake libtool

