#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
build: setup.py sg-logs metadata.py sg_logs
	@python3 setup.py build

install: setup.py sg-logs metadata.py sg_logs
	@python3 setup.py install

uninstall:
	@pip3 uninstall sg_logs

env: setup.py
	@./etc/env-make.sh

clean:
	rm -rf dist build __pycache__

dist-clean: clean
	rm -rf env

################################################################
# Docker image {
#
DOCKER_TAG   := $(shell python3 setup.py --version)
DOCKER_IMAGE := "cmdaa/sg-logs"

docker-image:
	make -C .. sg-logs-image

docker-push:
	make -C .. sg-logs-push

docker-run:
	docker run --rm -it $(DOCKER_IMAGE):$(DOCKER_TAG) /bin/bash

docker-clean:
	docker rmi $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker rmi $(DOCKER_IMAGE):latest

docker-image-name:
	@echo "$(DOCKER_IMAGE)"

docker-tag-name:
	@echo "$(DOCKER_TAG)"

docker-full-name:
	@echo "$(DOCKER_IMAGE):$(DOCKER_TAG)"

# Docker image }
################################################################
# Systemd service rules {
#
# Adjustable location for the configuration file and executable
DST_CONFIG= /etc/sg-logs.ini
EXEC_START= sg-logs

system/sg-logs.service.install: system/sg-logs.service
	@sed 's#%CONFIG%#'$(DST_CONFIG)'#g;s#%EXEC_START%#'$(EXEC_START)'#g' $< > $@

system/sg-logs.path.install: system/sg-logs.path
	@sed 's#%CONFIG%#'$(DST_CONFIG)'#g' $< > $@

/etc/systemd/system/sg-logs.service: system/sg-logs.service.install
	sudo cp $< $@
	@sudo chmod 664 $@

/etc/systemd/system/sg-logs.path: system/sg-logs.path.install
	sudo cp $< $@
	@sudo chmod 664 $@

$(DST_CONFIG): config.ini
	sudo cp $< $@
	@sudo chmod 664 $@

service-install: /etc/systemd/system/sg-logs.service \
	         /etc/systemd/system/sg-logs.path \
	         $(DST_CONFIG)
	sudo systemctl daemon-reload

service-uninstall:
	-sudo systemctl disable sg-logs sg-logs.path
	-sudo systemctl stop sg-logs sg-logs.path
	sudo rm -f /etc/systemd/system/sg-logs.*
	sudo systemctl daemon-reload
	rm -f system/sg-logs.*.install

# Immediately start/stop/restart the service
service-start:
	sudo systemctl start sg-logs sg-logs.path

service-stop:
	sudo systemctl stop sg-logs sg-logs.path

service-restart:
	sudo systemctl restart sg-logs sg-logs.path

# View the current status of the service
service-status:
	systemctl status sg-logs sg-logs.path

# Enable the service to be started at boot-time
service-enable:
	sudo systemctl enable sg-logs sg-logs.path

# Disable the service from being started at boot-time
service-disable:
	sudo systemctl disable sg-logs sg-logs.path

# View service-related logs
service-logs:
	sudo journalctl -u sg-logs -u sg-logs.path

#
# Systemd service rules }
################################################################
