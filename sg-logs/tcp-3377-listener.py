#!/usr/bin/env python3
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import select
import socket
import signal
import sys

HOST = '127.0.0.1'
PORT = 3377

with socket.socket( socket.AF_INET, socket.SOCK_STREAM) as sock: #{
  sock.bind( (HOST, PORT) )
  sock.listen()

  def _signal_handler( sig, frame ): #{
    print('\n=== Signal ...')
    sock.shutdown( socket.SHUT_RDWR )
    sock.close()
    #sock.close()

    sys.exit()
  #}

  signal.signal( signal.SIGINT, _signal_handler )

  print('>>> Listening at tcp://%s:%s ...' % (HOST,PORT))

  while True: #{
    try: #{
      print('>>> Wait for connection ...')
      conn, addr = sock.accept()

      host = ('%s:%s' % (addr[0], addr[1]))

      print('>>> %s: connected' % (host))
      while True: #{
        # Wait for incoming data ...
        try: #{
          ready_read, ready_write, ready_err = \
            select.select( [conn], [], [], 5 )

        except select.error as ex: #}{

          print('*** %s: connection error' % (host, ex))
          break
        #}

        if len(ready_read) > 0: #{
          data = conn.recv( 4096 )
          if not data: break

          print('>>> %s: %s' % (host, data))
        #}
      #}

      print('>>> %s: disconnected' % (host))
      conn.shutdown( socket.SHUT_RDWR )
      conn.close()

    except Exception as ex: #{
      print('*** Exception: %s' % (ex))
      break
    #}
  #}
#}
