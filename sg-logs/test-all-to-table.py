#!/usr/bin/env python3
#
# Generate the full set of generatable metrics from
#   tests/fixtures/page-*.yaml
#
import os
import glob
import yaml

script_path   = os.path.dirname( os.path.abspath( __file__ ) )
fix_path      = os.path.join( script_path, 'tests', 'fixtures' )
page_files    = glob.glob( os.path.join( fix_path, 'page-*.yaml' ) )

print('%-45s | %-10s | %-30s | %-12s | %s' %
      ('# metric-name', 'section', 'metric_source', 'units', 'stat_type'))

for data_path in page_files: #{
  data = {}
  with open(data_path, mode='r') as file: #{
    data = yaml.safe_load( file )
  #}

  page    = data.get('page',    -1)
  subpage = data.get('subpage', -1)
  section = data.get('ref',     '') #'<no-ref>')

  if section == '<not in the scsi standard>': #{
    section = ''  #'<no-ref>'
  #}

  for param in data.get('params', []): #{
    for metric in param.get('expect', []): #{
      name   = metric.get('name',   None)
      labels = metric.get('labels', None)

      source = labels.get('metric_source',
                          ('!scsi-log-page:0x%x,%x' % (page, subpage)))
      units  = labels.get('units',     '')  #'<no-unit>')
      type   = labels.get('stat_type', '')  #'<no-stat-type>')

      print('device_storage_%-30s | %-10s | %-30s | %-12s | %s' %
            (name, section, source, units, type))
    #}
  #}
#}
