import  yaml
from    sg_logs.utils import all_ascii

def parse_value( value_raw, fmt ): #{
  """
    Given a raw LOG SENSE parameter value, convert it according to the provided
    format-and-linking value

    Args:
      value_raw (bytearray) The raw LOG SENSE parameter;
      fmt (int)             The SCSI format-and-linking value;

    Returns:
      value (int|bytearray) The value converted according to `fmt`;
  """
  value = value_raw

  if (fmt == 0x0 or fmt == 0x2): #{
    # 0x00: Bounded data counter
    # 0x02: Bounded or unbounded data counter
    ###
    # param_len:
    #   1:  8-bit : uint8_t
    #   2: 16-bit : uint16_t
    #   4: 32-bit : uint32_t
    #   8: 64-bit : uint64_t
    #
    value = int.from_bytes( value_raw, 'big' )

  elif (fmt == 0x1): #}{
    # 0x01: ASCII format list
    ###
    # Iff all bytes are ASCII, convert this to a string, otherwise, leave it as
    # a raw byte array
    #
    if all_ascii( value_raw ): #{
      value = value_raw.decode('ascii')
    #}
  #}

  return value
#}

def parse_logsense( raw_data, two_byte_param_len = False ): #{
  """
    Given raw LOG SENSE data, convert it to the form that would be returned by
    `sg3utils.log_sense()` without the `raw` flag;

    Args:
      raw_data (bytearray)                The raw LOG SENSE data;
      [two_byte_param_len = False] (bool) Is the parameter control byte used as
                                          part of the parameter length?

    Returns:
      parse (dict)  The parsed LOG SENSE data

    ---
    SCSI SBC-3, Table 236: Log page format

        Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
       Byte |     |     |     |     |     |     |     |     |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
        0   | DS  | SPF | Page Code                         |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
        1   | Subpage Code                                  |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
        2   | Page Length (n-3)                             |
        3   |                                               |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
            | Log parameters                                |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
        4   | Log parameter (1, length x)                   |
       ...  |                                               |
       x+3  |                                               |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
       ...  |                                               |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+
      n-y+1 | Log parameter (n, length y)                   |
       ...  |                                               |
        n   |                                               |
      ------+-----+-----+-----+-----+-----+-----+-----+-----+

    Disable Save (DS):
      0   save log parameter if SP bit is set to 1
      1   do NOT save the log parameter;

    Subpage Format (SPF):
      0   the Subpage Code field will contain 00h
      1   the Subpage Code field will contain a value between 01h and FFh

    Page Code:
      The page code being transferred;

    Subpage Code:
      The subpage code being transferred;

    Page Length:
      The length, in bytes, of the following log parameters;

    ---
    Parameters will be decoded based upon the containing page/subpage

      Page/subpage: 0x00,00 (supported log pages)
          Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
         Byte |     |     |     |     |     |     |     |     |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          0   | Page Code                                     |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+

      Page/subpage: 0x00,ff (supported log pages and subpages)

          Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
         Byte |     |     |     |     |     |     |     |     |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          0   | Reserved  | Page Code                         |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          1   | Subpage Code                                  |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+

      All other standard T10 pages/subpages should have a form following
      SCSI SBC-3, Table 238: Log parameter

          Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
         Byte |     |     |     |     |     |     |     |     |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          0   | Parameter Code                                |
          1   |                                               |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          2   | Parameter control byte                        |
              |-----+-----+-----+-----+-----+-----+-----+-----|
              | DU  |Obslt| TSD | ETC | TMC       | Fmt/Link  |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          3   | Parameter Length (n-3)                        |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+
          4   | Parameter Value                               |
         ...  |                                               |
          n   |                                               |
        ------+-----+-----+-----+-----+-----+-----+-----+-----+

        Disable Update (DU):
          0   the device will update the log parameter value to reflect
              all events that should be noted by that parameter;
          1   the device will NOT update the log parameter value except
              in response to a LOG SELECT command;

        Target Save Disable (TSD):
          0   the log parameter is saved at vendor specific intervals;
          1   the log parameter is either nto implicitly saved or
              saving has been disabled;

        Enable Threshold Comparison (ETC):
          0   a comparison is not performed;
          1   a comparison to the threshold value is performed whenever
              the cumulative value is updated;

        Threshold Met Criteria (TMC):
          Defines the basis for comparison of the cumulative and
          threshold values:
            00b   every update of the cumulative value;
            01b   Cumulative value  equal to      threshold value;
            10b   Cumulative value  not equal to  threshold value;
            11b   Cumulative value  greater than  threshold value;

        Format and Linking (Fmt/Link):
            00b   Bounded data counter
            01b   ASCII format list
            10b   Bounded data counter or unbounded data counter
            11b   Binary format list

    ---
    On success, the returned dict will have the form:
        { page          : (int),
          subpage       : (int),
          disable_save  : (bool),
          subpage_format: (bool),
          params        : [ {}, ... ]
            ###
            # page/subpage 0x00,00 (supported log pages):
            #   {page: (int)}, ...
            ###
            # page/subPages 0x00,FF (supported log pages and subpages):
            #   {page: (int), subpage: (int)}, ...
            ###
            # All other page/subPages will have an entry per parsed parameter
            # code:
            #   {code                        : (int),
            #    offset                      : (int),
            #    disable_update              : (bool),
            #    target_save_disable         : (bool),
            #    enable_threshold_comparison : (bool),
            #    threshold_met_criteria      : (int),
            #    format_and_linking          : (int),
            #    value_raw                   : (bytearray),
            #    value                       : (str | int | bytearray),
            #   }, ...
          ]
        }
  """
  ds      = bool( raw_data[0] & 0x80 )
  spf     = bool( raw_data[0] & 0x40 )
  page    =       raw_data[0] & 0x3F
  subpage = (     raw_data[1] if spf else 0)

  sense_data = {
    'disable_save'  : ds,
    'subpage_format': spf,
    'page'          : page,
    'subpage'       : subpage,
    'params'        : [],
  }

  data_len = len( raw_data )
  idex     = 4

  while idex < data_len: #{
    if subpage == 0xff: #{
      #page_code = int( raw_data[idex] & 0x1f ) # technically  correct
      page_code = int( raw_data[idex] )         # functionally correct
      sub_code  = int( raw_data[idex+1] )

      param = { 'page':page_code, 'subpage':sub_code }
      sense_data['params'].append( param )
      idex += 2

    elif page  == 0x00: #}{
      page_code = int( raw_data[idex] )

      param = { 'page':page_code }
      sense_data['params'].append( param )
      idex += 1

    else: #}{
      param_code = int.from_bytes( raw_data[ idex:idex+2 ], 'big' )

      if two_byte_param_len: #{
        ###
        # Custom parameter format (2-byte parameter length, no control byte):
        #
        # Use defaults for values normally pulled from the parameter control
        # byte
        du        = False
        tsd       = False
        etc       = False
        tmc       = 0x00    # every update
        fmt       = 0x03    # Binary format list
        param_len = int.from_bytes( raw_data[ idex+2:idex+4 ], 'big' )

      else: #}{
        ###
        # Normal T10 parameter format
        #
        du        = bool( raw_data[ idex+2 ] & 0x80 )
        tsd       = bool( raw_data[ idex+2 ] & 0x20 )
        etc       = bool( raw_data[ idex+2 ] & 0x10 )
        tmc       = int(  raw_data[ idex+2 ] & 0x0c )
        fmt       = int(  raw_data[ idex+2 ] & 0x03 )
        param_len = int(  raw_data[ idex+3 ] )

      #}

      value_raw = raw_data[ idex+4:idex+4+param_len ]
      value     = parse_value( value_raw, fmt )

      param = {
        'code'                        : param_code,
        'offset'                      : idex - 4,
        'disable_update'              : du,
        'target_save_disable'         : tsd,
        'enable_threshold_comparison' : etc,
        'threshold_met_criteria'      : tmc,
        'format_and_linking'          : fmt,
        'value'                       : value,
        'value_raw'                   : value_raw,
      }

      sense_data['params'].append( param )

      idex += (param_len + 4 if param_len > 0 else 4)
    #}
  #}

  return sense_data
#}

def generate_logsense( data ): #{
  """
    Given data describing LOG SENSE data, generate the binary LOG SENSE data as
    would be returned by `sg3utils.log_sense()` with the `raw` flag;

    Args:
      data (dict)   The data describing the LOG SENSE data;

    Returns:
      (bytearray)       The binary data as a byte array;
  """

  #print('>>> Incoming data:')
  #pprint.pprint( data, indent=2 )

  ###
  # SCSI SBC-3, Table 236: Log page format
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | DS  | SPF | Page Code                         |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | Subpage Code                                  |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Page Length (n-3)                             |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #         | Log parameters                                |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Log parameter (1, length x)                   |
  #    ...  |                                               |
  #    x+3  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    ...  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #   n-y+1 | Log parameter (n, length y)                   |
  #    ...  |                                               |
  #     n   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  # Disable Save (DS):
  #   0   save log parameter if SP bit is set to 1
  #   1   do NOT save the log parameter;
  #
  # Subpage Format (SPF):
  #   0   the Subpage Code field will contain 00h
  #   1   the Subpage Code field will contain a value between 01h and FFh
  #
  # Page Code:
  #   The page code being transferred;
  #
  # Subpage Code:
  #   The subpage code being transferred;
  #
  # Page Length:
  #   The length, in bytes, of the following log parameters;
  #
  # ---
  # Parameters will be encoded based upon the containing page/subpage
  #
  #   Page/subpage: 0x00,00 (supported log pages)
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Page Code                                     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   Page/subpage: 0x00,ff (supported log pages and subpages)
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Reserved  | Page Code                         |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       1   | Subpage Code                                  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   All other standard T10 pages/subpages should have a form following
  #   SCSI SBC-3, Table 238: Log parameter
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Parameter Code                                |
  #       1   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       2   | Parameter control byte                        |
  #           |-----+-----+-----+-----+-----+-----+-----+-----|
  #           | DU  |Obslt| TSD | ETC | TMC       | Fmt/Link  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       3   | Parameter Length (n-3)                        |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       4   | Parameter Value                               |
  #      ...  |                                               |
  #       n   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #     Disable Update (DU):
  #       0   the device will update the log parameter value to reflect
  #           all events that should be noted by that parameter;
  #       1   the device will NOT update the log parameter value except
  #           in response to a LOG SELECT command;
  #
  #     Target Save Disable (TSD):
  #       0   the log parameter is saved at vendor specific intervals;
  #       1   the log parameter is either nto implicitly saved or
  #           saving has been disabled;
  #
  #     Enable Threshold Comparison (ETC):
  #       0   a comparison is not performed;
  #       1   a comparison to the threshold value is performed whenever
  #           the cumulative value is updated;
  #
  #     Threshold Met Criteria (TMC):
  #       Defines the basis for comparison of the cumulative and
  #       threshold values:
  #         00b   every update of the cumulative value;
  #         01b   Cumulative value  equal to      threshold value;
  #         10b   Cumulative value  not equal to  threshold value;
  #         11b   Cumulative value  greater than  threshold value;
  #
  #     Format and Linking (Fmt/Link):
  #         00b   Bounded data counter
  #         01b   ASCII format list
  #         10b   Bounded data counter or unbounded data counter
  #         11b   Binary format list
  ##
  page    = data['page']
  subpage = data['subpage']
  ds      = data['ds']
  spf     = data['spf']
  params  = []

  #################################################################
  # Generate parameter data
  #
  for param in data['params']: #{
    code  = param.get('code',  0)
    pcb   = param.get('pcb',   None)
    value = param.get('value', None)

    #
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    if page != 0x00 or subpage == 0xff: #{
      #
      #                          / 0x*,ff
      #   0   | (MSB) Parameter / Page Code                   |
      #
      params.append(  ((code >> 8) & 0x0ff) )
    #}

    #    v-----------------/ 0x00,00 / 0x*,ff
    #    0/1  | Parameter / Page    / Subpage Code      (LSB) |
    #
    params.append(     (code       & 0x0ff) )

    if pcb is not None: #{
      # assert( page != 0x00 and subpage != 0xff )
      #
      #   2   | Parameter control byte                        |
      #
      params.append( param['pcb'] )
    #}

    if isinstance(value, list): #{
      # assert( page != 0x00 and subpage != 0xff )
      #
      #   3   | Parameter Length (n-3)                        |
      #
      val_len = len( value )
      if val_len > 0xff: #{
        print('*** Page 0x%02x,%02x, code[ 0x%x ], value length > 255 [ %d ]' %
              (page, subpage, code, val_len))
      #}
      params.append( (val_len & 0x0ff) )

      #
      #   4   | Parameter Value                               |
      #  ...  |                                               |
      #   n   |                                               |
      #
      for val in value: #{
        params.append( val )
      #}
    #}
  #}

  #################################################################
  # Generate the full binary data for this page/subpage
  #
  param_cnt = len( params )
  out       = [
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | DS  | SPF | Page Code                         |
              ( ((ds   & 0x1) << 7) |
                ((spf  & 0x1) << 6) |
                ( page & 0x3f) ),
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Subpage Code                                  |
                subpage,
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Page Length (n-3)                             |
    #     3   |                                               |
              ( (param_cnt >> 8) & 0x0ff),
              (  param_cnt       & 0x0ff),
  ]

  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #           | Log parameters                                |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       4   | Log parameter (1, length x)                   |
  #      ...  |                                               |
  #      x+3  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #      ...  |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     n-y+1 | Log parameter (n, length y)                   |
  #      ...  |                                               |
  #       n   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  for param in params: #{
    out.append( param )
  #}

  #print('>>> Outgoing data (%d param bytes : 0x%02x):' %
  #        (param_cnt, param_cnt))
  #prhex( bytearray(out), indent='    ' )

  return bytearray(out)
#}

def generate_expect( data ): #{
  """
    Given data describing LOG SENSE data, generate the data that should be
    generated by the parser;

    Args:
      data (dict)   The data describing the LOG SENSE data;

    Returns:
      (list)        The simplfied metrics that should be generated;
  """

  res = []

  for param in data.get('params', []): #{
    for metric in param.get('expect', []): #{
      # Check for: value: '<raw>' => generate 'value_raw' from 'value'
      if metric.get('value', None) == '<raw>': #{
        metric['value_raw'] = bytearray( param.get('value',[]) )
        metric.pop('value',None)
      #}

      # Check for labels: {value_raw: '<generate>'}
      labels = metric.get('labels', None)
      if isinstance(labels, dict): #{
        value_raw = labels.get('value_raw', None)
        if value_raw == '<generate>': #{
          labels['value_raw'] = bytearray( param.get('value', []) )
        #}
      #}

      res.append( metric )
    #}
  #}

  return res
#}
