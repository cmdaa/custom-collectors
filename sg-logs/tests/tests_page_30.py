#
# Hitachi Performance Counter (0x30,00) parameter-based tests
#
# These tests are driven by:
#   fixtures/page_30.yaml
#
import unittest

from sg_logs.drive.scsi     import ScsiDrive
from sg_logs.scsi.log_page  import ScsiLogPage
from sg_logs.scsi.log_page.parser.t10_params.page_30 \
                            import parse_param

class Test( unittest.TestCase ): #{

  def _parse_param_test( self, log_page=None, test=None ): #{
    """
      Allow parser tests to be dynamically created from incoming test data.

      Args:
        log_page (ScsiLogPage)  The target log page with associated drive;
        test (dict)             Test data;
        test.data (dict)        The data/parameter to parse;
        test.expect (list)      The set of expected metrics data;
    """
    param     = test['data']
    expect    = test['expect']
    labels    = {}
    actual    = parse_param( log_page, param, labels, report_unknowns = True )

    self.maxDiff = None

    self.assertEqual( expect, actual )
  #}
#}

#############################################################################
# Dynamically test setup {
#
#
def make_gen_drive(): #{
  """
    Create a pseudo (non-hitachi) ScsiDrive instance for testing.

    Returns:
      (ScsiDrive) A pseudo Hitachi ScsiDrive instance;
  """
  # Create the pseudo ScsiDrive instance
  drive = ScsiDrive( name = '0:0:1:0',
                     info = {'sg_path': '/dev/sg5',
                             'serial' : 'FAKE12345',
                             'vendor' : 'generic' } )

  return drive
#}

def make_hitachi_drive(): #{
  """
    Create a pseudo Hitachi ScsiDrive instance for testing.

    Returns:
      (ScsiDrive) A pseudo Hitachi ScsiDrive instance;
  """
  # Create the pseudo ScsiDrive instance
  drive = ScsiDrive( name = '0:0:1:1',
                     info = {'sg_path': '/dev/sg6',
                             'serial' : 'FAKE67890',
                             'vendor' : 'HITACHI' } )

  return drive
#}

def setup_tests(): #{
  """
    Dynamically add test methods to `Test` for entries
    in the fixture:
      `./fixtures/page_30.yaml`

    This data should represent all publically known parameters for the Read
    Error Counter SCSI LOG page.
  """
  import os
  import yaml

  script_path   = os.path.dirname( os.path.abspath( __file__ ) )
  fixture_path  = os.path.join( script_path, 'fixtures',
                                             'page_30.yaml' )
  with open( fixture_path ) as fh: #{
    data = yaml.safe_load( fh )
  #}

  name    = data['name'].lower().replace(' ', '_')
  page    = data['page']
  subpage = data['subpage']

  gen     = make_gen_drive()
  hitachi = make_hitachi_drive()

  log_page_gen     = ScsiLogPage( src_drive=gen,    page=page, subpage=subpage )
  log_page_hitachi = ScsiLogPage( src_drive=hitachi,page=page, subpage=subpage )

  # Generate a test for each log page parameter
  for idex,test in enumerate(data['tests']): #{
    param  = test['data']
    if 'value_raw' in param: #{
      # Convert 'value_raw' from an array of `int` to a bytearray()
      param['value_raw'] = bytearray( param['value_raw'] )

      # Handle any 'expect' value that should reflect `value_raw`
      for entry in test['expect']: #{
        if 'value_raw' in entry['labels']: #{
          # Reflect the 'value_raw' down
          entry['labels']['value_raw'] = param['value_raw']
        #}
      #}
    #}

    method = ('test_%02d_code:0x%02x.%d' %
                (idex, param['code'], param['offset']))

    def do_test( self, test=test ): #{
      log_page = log_page_gen
      if 'hitachi' in test: log_page = log_page_hitachi

      self._parse_param_test( log_page=log_page, test=test )
    #}

    setattr( Test, method, do_test )
  #}

#}

setup_tests()

# Dynamically test setup }
#############################################################################

if __name__ == '__main__': #{
  unittest.main()
#}
