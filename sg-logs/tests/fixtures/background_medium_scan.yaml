######################################################################
# YAML-extended JSON to enable:
#   - comments
#   - hex-encoded values
#   - non-quoted keys
#   - commas on last elements
#
{
  name    : "Background Medium Scan",
  page    : 0x15,
  subpage : 0,

  tests: [
    ###################################################################
    # Background scan status (0x00) {
    #
    # SCSI Table 279: Background Scan Status parameter format
    #                 (parameter data)
    #
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Accumulated Power On Minutes                  |
    #    ...  | (drive-based event "timestamp")               |
    #     3   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | Reserved                                      |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | Background Scan Status                        |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Number of Background Scans Performed          |
    #     7   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     8   | Background Medium Scan Progress               |
    #     9   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     10  | Number of Background Medium Scans Performed   |
    #     11  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 0, 6,7, 8,9, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "no scans active",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": " 3.14 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 1, 0,1, 64,0, 0,2 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan active",
            "units" : "count"
          },
          "value" : 1
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "25.00 percent",
            "units" : "count"
          },
          "value" : 2
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 2, 0,7, 64,64, 0,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background pre-scan is active",
            "units" : "count"
          },
          "value" : 7
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "25.10 percent",
            "units" : "count"
          },
          "value" : 11
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 3, 1,0, 64,128, 2,0 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted due to fatal error",
            "units" : "count"
          },
          "value" : 256
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "25.20 percent",
            "units" : "count"
          },
          "value" : 512
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 4, 4,1, 128,0, 4,2 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted due to vendor-specific pattern of errors",
            "units" : "count"
          },
          "value" : 1025
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "50.00 percent",
            "units" : "count"
          },
          "value" : 1026
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 5, 6,7, 128,64, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted due to medium formatted without P-list",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "50.10 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 6, 6,7, 128,128, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted - vendor-specific cause",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "50.20 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 7, 6,7, 192,0, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted due to temperature out of allowed range",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "75.00 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 8, 6,7, 192,64, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "background medium scan halted, waiting for Background Medium Interval timer expiration",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "75.10 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    { "data"    : {"code":0, "offset":0,
                   "value_raw":[0,1,2,3, 0, 9, 6,7, 192,128, 10,11 ]},
      "expect"  : [
        { "name"  : "background_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "status": "reserved [ 0x9 ]",
            "units" : "count"
          },
          "value" : 1543
        },
        { "name"  : "background_medium_scan",
          "labels": {
            "power_on_hours": "1100.8500",
            "progress": "75.20 percent",
            "units" : "count"
          },
          "value" : 2571
        }
      ]
    },
    #
    # Background scan status (0x00) }
    ###################################################################
    # Background scan parameter (0x0001 - 0x0800) {
    #
    # SCSI Table 281: Background Scan parameter format (parameter data)
    #
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | Accumulated Power On Minutes                  |
    #    ...  |                                               |
    #     3   |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     4   | Reassign Status       | Sense Key             |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     5   | Additional Sense Code                         |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     6   | Additional Sense Code Qualifier               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     7   | Vendor-specific                               |
    #    .... |                                               |
    #     11  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     12  | Logical Block Address                         |
    #    .... |                                               |
    #     19  |                                               |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #
    { "data"    : {
        "code":0x01, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours
          0x0f,         # Reassign status / sense key
          0x01,         # Additional sense code (ASC)
          0x02,         # Additional sense code qualifier (ASCQ)

          # Vendor specific {
          0x10,         # Head (4-bits), cylinder (4-bits)
          0x00, 0x02,   # Cylinder (low 16-bits of 20)
          0x00, 0x03,   # Sector (16-bits)
          # Vendor specific }
          # Logical Block Address (8-bytes) {
          12,13,14,15,
          16,17,18,19,
          # Logical Block Address (8-bytes) }
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "no reassignment",
            "scsi_sense_key"  : "0x0f",
            "scsi_asc"        : "0x01",
            "scsi_ascq"       : "0x02",

            "units"           : "index"
          },
          "value" : 1
        }
      ]
    },
    { "data"    : {
        "code":0x02, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x14,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassignment pending",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 2
        }
      ]
    },
    { "data"    : {
        "code":0x03, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x24,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassignment passed",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 3
        }
      ]
    },
    { "data"    : {
        "code":0x04, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x34,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reserved [ 0x3 ]",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 4
        }
      ]
    },
    { "data"    : {
        "code":0x05, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x44,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassignment failed",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 5
        }
      ]
    },
    { "data"    : {
        "code":0x06, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x54,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "repaired (scrubbed)",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 6
        }
      ]
    },
    { "data"    : {
        "code":0x07, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x64,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassign by command (valid data)",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 7
        }
      ]
    },
    { "data"    : {
        "code":0x08, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x74,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassign by command (invalid data)",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 8
        }
      ]
    },
    { "data"    : {
        "code":0x09, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x84,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "reassign by command (failed)",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 9
        }
      ]
    },
    { "data"    : {
        "code":0x800, "offset":0,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x04,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "medium_defect",
          "labels": {
            "power_on_hours"  : "1100.8500",
            "lba"             : "0xc0d0e0f10111213",

            "reassign_status" : "no reassignment",
            "scsi_sense_key"  : "0x04",
            "scsi_asc"        : "0x05",
            "scsi_ascq"       : "0x06",

            "units"           : "index"
          },
          "value" : 2048
        }
      ]
    },

    #
    # Background scan parameter (0x0001 - 0x0800) }
    ###################################################################
    { "data"    : {
        "code":0x0801, "offset":0,
        "value": 42,
        "value_raw":[
          0,1,2,3,      # power-on-hours

          0x04,         # Reassign status / sense key

          5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
        ]
      },
      "expect"  : [
        { "name"  : "parameter_unknown",
          "labels": { "value_raw":true },
          "value" : 42
        }
      ]
    },
  ]
}
