#
# Non-Medium Error Counter (0x06,00) parameter-based tests
#
# These tests are driven by:
#   fixtures/non-medium_error.yaml
#
import unittest

from sg_logs.drive.scsi     import ScsiDrive
from sg_logs.scsi.log_page  import ScsiLogPage
from sg_logs.scsi.log_page.parser.t10_params.error_counter \
                            import parse_non_medium_param  as parse_param

class Test( unittest.TestCase ): #{

  def _parse_param_test( self, log_page=None, test=None ): #{
    """
      Allow parser tests to be dynamically created from incoming test data.

      Args:
        log_page (ScsiLogPage)  The target log page with associated drive;
        test (dict)             Test data;
        test.data (dict)        The data/parameter to parse;
        test.expect (list)      The set of expected metrics data;
    """
    param     = test['data']
    expect    = test['expect']
    labels    = {}
    actual    = parse_param( log_page, param, labels, report_unknowns = True )

    self.assertEqual( expect, actual )
  #}
#}

#############################################################################
# Dynamically test setup {
#
#
def setup_tests(): #{
  """
    Dynamically add test methods to `Test` for entries
    in the fixture:
      `./fixtures/non-medium_error.yaml`

    This data should represent all publically known parameters for the Read
    Error Counter SCSI LOG page.
  """
  import os
  import yaml

  script_path   = os.path.dirname( os.path.abspath( __file__ ) )
  fixture_path  = os.path.join( script_path, 'fixtures',
                                             'non-medium_error.yaml' )
  with open( fixture_path ) as fh: #{
    data = yaml.safe_load( fh )
  #}

  name    = data['name'].lower().replace(' ', '_')
  page    = data['page']
  subpage = data['subpage']

  log_page  = ScsiLogPage( src_drive=None, page=page, subpage=subpage )

  # Generate a test for each log page parameter
  for idex,test in enumerate(data['tests']): #{
    param  = test['data']
    if 'value_raw' in param: #{
      # Convert 'value_raw' from an array of `int` to a bytearray()
      param['value_raw'] = bytearray( param['value_raw'] )

      # Handle any 'expect' value that should reflect `value_raw`
      for entry in test['expect']: #{
        if 'value_raw' in entry['labels']: #{
          # Reflect the 'value_raw' down
          entry['labels']['value_raw'] = param['value_raw']
        #}
      #}
    #}

    method = ('test_%02d_code:0x%02x.%d' %
                (idex, param['code'], param['offset']))

    def do_test( self, test=test ): #{
      self._parse_param_test( log_page=log_page, test=test )
    #}

    setattr( Test, method, do_test )
  #}

#}

setup_tests()

# Dynamically test setup }
#############################################################################

if __name__ == '__main__': #{
  unittest.main()
#}
