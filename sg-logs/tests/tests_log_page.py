#
# Supported Log Pages (0x00,00; 0x00,ff) parameter-based tests
#
# These tests are driven by:
#   fixtures/log_page.json
#
import unittest

from sg_logs.drive.scsi                               import ScsiDrive
from sg_logs.scsi.log_page                            import ScsiLogPage
from sg_logs.scsi.log_page.parser.t10_params.log_page import parse_param

class Test( unittest.TestCase ): #{

  def _parse_param_test( self, log_page=None, test=None ): #{
    """
      Allow parser tests to be dynamically created from incoming test data.

      Args:
        log_page (ScsiLogPage)  The target log page with associated drive;
        test (dict)             Test data;
        test.data (dict)        The data/parameter to parse;
        test.expect (list)      The set of expected metrics data;
    """
    param     = test['data']
    expect    = test['expect']
    labels    = {}
    actual    = parse_param( log_page, param, labels, report_unknowns = True )

    self.assertEqual( expect, actual )
  #}
#}

#############################################################################
# Dynamically test setup {
#
#
def make_drive( data ): #{
  """
    Create a pseudo ScsiDrive instance for testing.

    Args:
      data (list) The incoming test data;
                  [ {data: {page:, subpage:}}, ... ]

    Returns:
      (ScsiDrive) A pseudo ScsiDrive instance;
  """
  # Create the pseudo ScsiDrive instance
  drive = ScsiDrive( name = '0:0:1:0',
                     info = {'sg_path': '/dev/sg5',
                             'serial' : 'FAKE12345'} )

  #
  # Generate a pseudo ScsiLogPage instance for each entry in `data`
  #
  pages = []
  for rec in data: #{
    param    = rec['data']
    log_page = ScsiLogPage( src_drive  = drive,
                            page       = param['page'],
                            subpage    = param['subpage'] )
    pages.append( log_page )

    rec['log_page'] = log_page
  #}

  ###
  # Associate the log pages with the drive.
  #   This is the cache used by ScsiDrive.get_supported_pages()
  #
  drive._log_pages = pages

  return drive
#}

def setup_tests(): #{
  """
    Dynamically add test methods to `Test` for all page/subpage entries
    in the log page fixtures:
      `./fixtures/log_page.json`

    This data should represent all publically known SCSI LOG pages.

    Args:
      data (list) The incoming test data;
                  [ {data: {page:, subpage:}}, ... ]

    Returns:
      (ScsiDrive) A pseudo ScsiDrive instance;
  """
  import os
  import json

  script_path   = os.path.dirname( os.path.abspath( __file__ ) )
  fixture_path  = os.path.join( script_path, 'fixtures', 'log_page.json' )
  with open( fixture_path ) as fh: #{
    data = json.load( fh )
  #}

  # Generate the pseudo-drive with pseudo-log_pages
  #   @note This will add 'log_page: ScsiLogPage()' to each record in `data`;
  drive = make_drive( data )

  # Generate a test for each log page
  for test in data: #{
    log_page = test['log_page']
    param    = test['data']
    method   = 'test_log_page:%02x,%02x' % (log_page.page, log_page.subpage)

    def do_test( self, test=test ): #{
      self._parse_param_test( log_page=log_page, test=test )
    #}

    setattr( Test, method, do_test )
  #}

#}

setup_tests()

# Dynamically test setup }
#############################################################################

if __name__ == '__main__': #{
  unittest.main()
#}
