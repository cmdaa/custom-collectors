#
# Page-based T10 parser tests
#
# These tests are driven by:
#   fixtures/page-*.yaml
#
import os
import sys
import pickle
import unittest

from sg_logs.drive.scsi               import ScsiDrive
from sg_logs.scsi.log_page            import ScsiLogPage
from sg_logs.scsi.log_page.db         import ScsiLogPageDb
from sg_logs.scsi.log_page.parser.t10 import parse_source

class Test( unittest.TestCase ): #{
  def _do_page_test( self, test=None ): #{
    """
      Allow parser tests to be dynamically created from incoming test data.

      Args:
        test (dict)                 Test data;
        test.raw_data (bytearray)   The raw SCSI LOG SENSE data as generated
                                    via `sg3utils.log_sense()` with the `raw`
                                    flag;
        test.log_sense (dict)       The internal form of `raw_data` as would be
                                    generated via `sg3utils.log_sense()`
                                    without the `raw` flag;
        test.log_page (ScsiLogPage) The SCSI LOG page that is the source of
                                    this data;
        test.expect (list)          The set of expected metrics data;
    """
    #raw_data  = test['raw_data']
    log_sense = test['log_sense']
    log_page  = test['log_page']
    expect    = test['expect']

    # assert( log_sense        == parse_logsense( raw_data ) )
    # assert( isInstance( log_page, ScsiLogPage ) )
    # assert( log_page.page    == log_sense['page'] )
    # assert( log_page.subpage == log_sense['subpage'] )
    actuals   = parse_source( log_page,
                              log_sense,
                              report_level    = 100,
                              parse_unknowns  = True )

    self.maxDiff = None


    ###
    # :XXX: Instead of a single, large `self.assertEqual( expect, actuals )`,
    #       use multiple subTest() calls to allow finer grained reporting of
    #       any difference.
    #
    with self.subTest( 'metric-count' ): #{
      self.assertEqual( len( expect ), len( actuals ) )
    #}

    use_find = True   # Too may mis-matches with the generated actuals
    if not use_find: #{
      # One large diff
      self.assertEqual( expect, actuals )

    else: #}{
      ###
      # Instead of a single, large `self.assertEqual( expect, actuals )`, use
      # multiple subTest() calls to allow finer grained reporting of any
      # difference.
      #
      # :NOTE: This requires that we can identify the proper expected entry
      #        using the generated actual value.
      #
      #        The current find heuristic requires a match by metric `name` and
      #        all included labels from the set (ordered by importance):
      #        - metric_source
      #        - log_page
      #        - sas_phy_id
      #        - sas_phy_event_index
      #        - stat_type
      #
      for expected in expect: #{
        name   = expected.get('name',   None)
        value  = expected.get('value',  None)
        labels = expected.get('labels', {})

        (actual,err_msg) = self._find_match( expected, actuals )

        if actual is None: #{
          # Cannot find actual -- punt to one large diff
          print('*** cannot find matching actual: %s' % (err_msg))
          print('*** Perform a single large diff ...')
          self.assertEqual( expect, actuals )
          break
        #}

        with self.subTest( msg = (name +'.name') ): #{
          self.assertEqual( name, actual.get('name', None) )
        #}

        with self.subTest( msg = (name +'.value') ): #{
          self.assertEqual( value, actual.get('value', None) )
        #}

        a_labels = actual.get('labels', {})

        with self.subTest( msg = (name +'.label-count') ): #{
          self.assertEqual( len(labels), len(a_labels) )
        #}

        for key,val in labels.items(): #{
          a_val = a_labels.get( key, None )

          with self.subTest( msg = (name +'.label:'+ key) ): #{
            self.assertEqual( val, a_val )
          #}
        #}
      #}
    #}
  #}

  def _find_match( self, expected, actuals ): #{
    """
      Given an expected entry, locate its match in the set of actuals based
      upon the metric name and a heuristic set of entries from `labels`.

      The incoming entries (`expected` and those in `actuals`) should have the
      form:
        {
          name  : metric-name   {str},
          value : metric-value  {Number},
          labels: {
            units               : unit-of-measure {str},
            metric_source       : metric-source {str},
            [log_page           : id {str}],
            [stat_type          : explicit-type {str}],
            [sas_phy_id         : id {str}],
            [sas_phy_event_index: index {str}],
            ...
          }
        }

      Args:
        expected (dict)   The expected entry;
        actuals (list)    A list of actual entries;

      Return:
        (dict | None, str | None) The matching actual entry or None if not
                                  found along with an error message if no entry
                                  was found or None;

    """
    name    = expected.get('name')
    labels  = expected.get('labels', {})

    # Gather the set of labels to match
    log_page            = labels.get('log_page', None)
    stat_type           = labels.get('stat_type', None)
    metric_source       = labels.get('metric_source', None)
    sas_phy_id          = labels.get('sas_phy_id', None)
    sas_phy_event_index = labels.get('sas_phy_event_index', None)

    found = None
    for actual in actuals: #{
      # MUST match 'name'
      actual_name = actual.get('name')
      if actual_name != name: continue

      # Perform any label-based checks
      actual_labels = actual.get('labels', {})
      if metric_source is not None: #{
        # MUST match 'labels.metric_source'
        if metric_source == actual_labels.get('metric_source', None): #{
          found = actual
        else: #}{
          found = None
          continue
        #}
      #}

      if log_page is not None: #{
        # MUST match 'labels.log_page'
        if log_page == actual_labels.get('log_page', None): #{
          found = actual
        else: #}{
          found = None
          continue
        #}
      #}

      if sas_phy_id is not None: #{
        # MUST match 'labels.sas_phy_id'
        if sas_phy_id == actual_labels.get('sas_phy_id', None): #{
          found = actual
        else: #}{
          found = None
          continue
        #}
      #}

      if sas_phy_event_index is not None: #{
        # MUST match 'labels.sas_phy_event_index'
        if (sas_phy_event_index ==
            actual_labels.get('sas_phy_event_index', None)): #{
          found = actual
        else: #}{
          found = None
          continue
        #}
      #}

      if stat_type is not None: #{
        # MUST match 'labels.stat_type'
        if stat_type == actual_labels.get('stat_type', None): #{
          found = actual
        else: #}{
          found = None
          continue
        #}
      #}

      if found is not None: break
    #}

    msg = None
    if found is None: #{
      # Missing actual
      missing = []

      if log_page is not None: #{
        missing.append ( ('log_page="%s"' % (log_page)) )
      #}
      if metric_source is not None: #{
        missing.append( ('metric_source="%s"' % (metric_source)) )
      #}
      if sas_phy_event_index is not None: #{
        missing.append( ('sas_phy_event_index="%s"' % (sas_phy_event_index)) )
      #}
      if sas_phy_id is not None: #{
        missing.append( ('sas_phy_id="%s"' % (sas_phy_id)) )
      #}
      if stat_type is not None: #{
        missing.append( ('stat_type="%s"' % (stat_type)) )
      #}

      msg = ('%s{ %s }' % (name, ', '.join( missing )))
    #}

    return (found,msg)
  #}
#}

#############################################################################
# Dynamic test setup {
#
#
def setup_tests(): #{
  """
    Dynamically add test methods to `Test` for entries in the fixtures
    subdirectory:
      `./fixtures/*.yaml`

      .yaml test description / data;
      .bin  contain raw SCSI LOG data. This file is generated from the .yaml;
      .pyb  contain a pickled version of the expected data -- i.e. the data
            that should be generated by parsing the contents of the `.bin`
            file. This file is generated from the .yaml;
  """
  import glob
  import yaml
  from   tests.parse import parse_logsense, generate_logsense, generate_expect

  script_path   = os.path.dirname( os.path.abspath( __file__ ) )
  fix_path      = os.path.join( script_path, 'fixtures' )
  page_files    = glob.glob( os.path.join( fix_path, 'page-*.yaml' ) )

  
  # Create a pseudo-drive instance
  src_drive = ScsiDrive( name = '0:0:1:0',
                         info = {'sg_path'  : '/dev/sg5',
                                 #'disk_type': 'hdd',
                                 'serial'   : 'FAKE12345',
                                 'vendor'   : 'SEAGATE' } )

  # Create a default ScsiLogPageDb and use it to retrieve a page map.
  #
  # This page map will be attached to the source drive AND used to create the
  # set of supported log pages (src_drive._log_pages).
  #
  db_path            = os.path.join( script_path, '..', 'scsi_log_page_db.yml' )
  db                 = ScsiLogPageDb( config_path = db_path )
  page_map           = db.get_page_map( src_drive.vendor, 'model', 'rev' )
  src_drive.page_map = page_map

  # Add all log pages from `page_map` to the source drive
  src_drive._log_pages = []
  for page,subpages in page_map.page_map['page'].items(): #{
    for subpage,info in subpages.items(): #{
      log_page = ScsiLogPage(src_drive = src_drive,
                             page      = page,
                             subpage   = subpage,
                             title     = info.get('name',
                                                  ('Log page 0x%02x,%2x' %
                                                      (page,subpage))),
                             level     = info.get('level', 0),
                             cost      = info.get('cost',  1.0) )
      src_drive._log_pages.append( log_page )
    #}
  #}

  #######################################################################
  # Iterate over all available page description files and for each:
  #   1) Load the page description file                     (%name%.yaml);
  #   2) Load or create the associated, raw SCSI LOG data   (%name%.bin);
  #   3) Generate the internal version of LOG SENSE data that would be
  #      generated via `sg3utils.log_sense()` without the `raw` flag;
  #   4) Locate the target ScsiLogPage instance for the source drive;
  #   5) Load or create the associated, pickled expect data (%name%.pyb);
  #   6) Generate a test-case for the log page;
  #
  failures = 0
  for data_path in page_files: #{
    base_name = os.path.basename( data_path ).replace('.yaml', '')
    bin_path  = os.path.join( fix_path, base_name +'.bin' )
    exp_path  = os.path.join( fix_path, base_name +'.pyb' )

    src_time  = os.path.getmtime( data_path )

    # 1) Load the page description file (base_data, .yaml)
    base_data = {}
    with open(data_path, mode='r') as file: #{
      base_data = yaml.safe_load( file )
    #}

    # 2) Load or create the associated, raw SCSI LOG data (.bin)
    if (not os.path.isfile( bin_path ) or
        os.path.getmtime( bin_path ) < src_time): #{
      print( '>>> Missing/Old binary [ %s ]: generate ...' % (base_name))

      data = generate_logsense( base_data )

      with open(bin_path, 'wb') as file: #{
        file.write( data )
      #}

    else: #}{
      # Read the existing binary data
      with open(bin_path, mode='rb') as file: #{ b is important -> binary
        data = bytearray( file.read() )
      #}
    #}

    # 3) Generate the internal version of LOG SENSE data that would be
    #    generated via `sg3utils.log_sense()` *without* the `raw` flag;
    log_sense = parse_logsense( data )

    # 4) Locate the target ScsiLogPage instance for the source drive;
    page     = log_sense['page']
    subpage  = log_sense['subpage']
    log_page = next( (lp for lp in src_drive._log_pages
                          if lp.page == page and lp.subpage == subpage),
                      None )
    if log_page is None: #{
      print( '*** Missing entry in SCSI Log Page db: page 0x%02x,%02x' %
              (page, subpage), file=sys.stderr)
      failures += 1
      continue
    #}

    # 5) Load or create the associated, pickled expect data (.pyb);
    if (not os.path.isfile( exp_path ) or
        os.path.getmtime(   exp_path ) < src_time): #{
      print( '>>> Missing/Old expect [ %s ]: generate ...' % (base_name))

      # Generate expected data from the YAML description
      expect = generate_expect( base_data )

      #expect = parse_source(log_page,
      #                      log_sense,
      #                      report_level    = 100,
      #                      parse_unknowns  = True )

      with open(exp_path, 'wb') as file: #{
        pickle.dump( expect, file )
      #}

    else: #}{
      # Read the existing pickled version of expected data
      with open(exp_path, mode='rb') as file: #{
        expect = pickle.load( file )
      #}
    #}

    # 6) Generate a test-case for the log page;
    test = {
      'raw_data'  : data,
      'log_sense' : log_sense,
      'log_page'  : log_page,
      'expect'    : expect,
    }

    method = ('test_%s' % (base_name))

    def do_test( self, test=test ): #{
      self._do_page_test( test=test )
    #}

    setattr( Test, method, do_test )
  #}

  if failures > 0: #{
    print( '*** Missing %d entr%s in SCSI Log Page db -- STOP' %
            (failures, ('y' if failures == 1 else 'ies')), file=sys.stderr)
    sys.exit(-1)
  #}
#}

setup_tests()

# Dynamic test setup }
#############################################################################

if __name__ == '__main__': #{
  unittest.main()
#}
