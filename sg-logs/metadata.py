#
# Meta-data about sg-logs
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
name        = 'sg-logs'
version     = '0.3.12'
description = """
  A SCSI disk metrics collector that:
    1) locates all SCSI disks connected to the system, including those behind a
       MegaRaid controller;
    2) retrieves the available metric sources (e.g. SCSI LOG SENSE pages) for
       each SCSI disk, identifying the cost of fetching each metric source;
    3) generates a collection schedule for all metric sources to ensure
       application access to disk data is minimally impacted by log collection
       activities;
    4) runs the collection schedule, fetching disk metrics as proscribed by the
       schedule and sending them on to a configurable publication location
       (e.g. stdout, tcp socket) in a configurable format (e.g. json |
       prometheus);
"""

url         = 'https://gitlab.com/cmdaa/custom-collectors/tree/master/sg-logs'

class author: #{
  name  = 'D. Elmo Peele'
  email = 'elmo.peele@gmail.com'
#}
maintainer    = author

class long_description: #{
  file  = 'README.md'
  type  = 'text/markdown'
#}
