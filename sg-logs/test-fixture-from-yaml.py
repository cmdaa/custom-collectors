#!/usr/bin/env python3
#
# Generate binary test data from a yaml description
#
import os
import sys
import yaml
import pprint

from sg_logs.utils import prhex

def process_yaml( file_path ): #{
  print('>>> YAML [ %s ]' % (os.path.basename(file_path)))

  data = {}
  with open(file_path, mode='r') as file: #{
    data = yaml.safe_load( file )
  #}

  print('>>> Incoming data:')
  pprint.pprint( data, indent=2 )

  ###
  # SCSI SBC-3, Table 236: Log page format
  #
  #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #    Byte |     |     |     |     |     |     |     |     |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     0   | DS  | SPF | Page Code                         |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     1   | Subpage Code                                  |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     2   | Page Length (n-3)                             |
  #     3   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #         | Log parameters                                |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #     4   | Log parameter (1, length x)                   |
  #    ...  |                                               |
  #    x+3  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #    ...  |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #   n-y+1 | Log parameter (n, length y)                   |
  #    ...  |                                               |
  #     n   |                                               |
  #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  # Disable Save (DS):
  #   0   save log parameter if SP bit is set to 1
  #   1   do NOT save the log parameter;
  #
  # Subpage Format (SPF):
  #   0   the Subpage Code field will contain 00h
  #   1   the Subpage Code field will contain a value between 01h and FFh
  #
  # Page Code:
  #   The page code being transferred;
  #
  # Subpage Code:
  #   The subpage code being transferred;
  #
  # Page Length:
  #   The length, in bytes, of the following log parameters;
  #
  # ---
  # Parameters will be decoded based upon the containing page/subpage
  #
  #   Page/subpage: 0x00,00 (supported log pages)
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Page Code                                     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   Page/subpage: 0x00,ff (supported log pages and subpages)
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Reserved  | Page Code                         |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       1   | Subpage Code                                  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #   All other standard T10 pages/subpages should have a form following
  #   SCSI SBC-3, Table 238: Log parameter
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Parameter Code                                |
  #       1   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       2   | Parameter control byte                        |
  #           |-----+-----+-----+-----+-----+-----+-----+-----|
  #           | DU  |Obslt| TSD | ETC | TMC       | Fmt/Link  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       3   | Parameter Length (n-3)                        |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       4   | Parameter Value                               |
  #      ...  |                                               |
  #       n   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #     Disable Update (DU):
  #       0   the device will update the log parameter value to reflect
  #           all events that should be noted by that parameter;
  #       1   the device will NOT update the log parameter value except
  #           in response to a LOG SELECT command;
  #
  #     Target Save Disable (TSD):
  #       0   the log parameter is saved at vendor specific intervals;
  #       1   the log parameter is either nto implicitly saved or
  #           saving has been disabled;
  #
  #     Enable Threshold Comparison (ETC):
  #       0   a comparison is not performed;
  #       1   a comparison to the threshold value is performed whenever
  #           the cumulative value is updated;
  #
  #     Threshold Met Criteria (TMC):
  #       Defines the basis for comparison of the cumulative and
  #       threshold values:
  #         00b   every update of the cumulative value;
  #         01b   Cumulative value  equal to      threshold value;
  #         10b   Cumulative value  not equal to  threshold value;
  #         11b   Cumulative value  greater than  threshold value;
  #
  #     Format and Linking (Fmt/Link):
  #         00b   Bounded data counter
  #         01b   ASCII format list
  #         10b   Bounded data counter or unbounded data counter
  #         11b   Binary format list
  ##
  page    = data['page']
  subpage = data['subpage']
  ds      = data['ds']
  spf     = data['spf']
  params  = []
  for param in data['params']: #{
    code  = param.get('code',  0)
    pcb   = param.get('pcb',   None)
    value = param.get('value', None)

    if page != 0x00 or subpage == 0xff: #{
      params.append(  ((code >> 8) & 0x0ff) )
    #}
    params.append(     (code       & 0x0ff) )

    if pcb is not None: params.append( param['pcb'] )

    if isinstance(value, list): #{
      val_len = len( value )
      params.append( (val_len & 0x0ff) )
      for val in value: #{
        params.append( val )
      #}
    #}
  #}

  param_cnt = len( params )
  out       = [
    #     Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
    #    Byte |     |     |     |     |     |     |     |     |
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     0   | DS  | SPF | Page Code                         |
              ( ((ds   & 0x1) << 7) |
                ((spf  & 0x1) << 6) |
                ( page & 0x3f) ),
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     1   | Subpage Code                                  |
                subpage,
    #   ------+-----+-----+-----+-----+-----+-----+-----+-----+
    #     2   | Page Length (n-3)                             |
    #     3   |                                               |
              ( (param_cnt >> 8) & 0x0ff),
              (  param_cnt       & 0x0ff),
  ]
  for param in params: #{
    out.append( param )
  #}

  print('>>> Outgoing data (%d param bytes : 0x%02x):' %
          (param_cnt, param_cnt))
  prhex( bytearray(out), indent='    ' )
#}

def main(): #{
  if len(sys.argv) < 2: #{
    print('*** Usage %s /path/to/file.yaml' %
            (os.path.basename(sys.argv[0])))
    sys.exit(-1)
  #}

  file_path = sys.argv[1]

  process_yaml( file_path )
#}

main()
