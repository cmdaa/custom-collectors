#!/usr/bin/env python3
#
# https://en.wikipedia.org/wiki/Public_domain
# This software was written by the US Government
# This software is not for commercial use
# Software is provided as-is without warranty
#
# :NOTE: We cannot internally modify LD_LIBRARY_PATH to properly allow
#        extensions and their dependencies to be loaded.
#
#        It must be done externally (e.g. via sg-logs.sh)
#
#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#######
import os
import sys
import signal
import re

DIR         = os.path.dirname( os.path.realpath( __file__ ) )
PROG_ROOT   = os.path.dirname( DIR )

DEFAULTS    = {
  'metric_sources'  : False,
  'config.ini'      : '/etc/sg-logs/config.ini',

  'config'  : {
    'DEFAULT': {
      'DEBUG'               : False,
      'publish'             : 'stdout',
      'publish_format'      : 'prometheus',
      'include_timestamp'   : False,
    },
    'log_page': {
      'db'                    : 'scsi_log_page_db.yml',
      'parse_unknown_pages'   : False,
      'report_unknown_params' : True,
      'report_level'          : 1,
    },
    'scan': {
      'include_scsi'      : True,
    },
    'scheduler': {
      'period'            : '5m',
      'sync_beat'         : 6,
      'thread_pool'       : 10,
      'reconnect_delay'   : '5s',
      'one_shot'          : False,
    },
  }
}

app       = None
scheduler = None

def main( args ): #{
  """
    The main sg-logs entry point.

    Args:
      args (obj)  Incoming arguments
  """
  global app

  import sg_logs

  signal.signal( signal.SIGINT, _signal_handler )

  try: #{
    config = _load_config( args )
  except Exception as ex: #}{
    print('*** Failed to load config:', ex, file=sys.stderr)
    sys.exit( -1 )
  #}

  #######################################
  # Pass the 'log_page' config section
  # to Sglogs().
  #
  app = sg_logs.Sglogs( config['log_page'] )

  # Add a reference to the top-level 'app' to the default section
  config.set( 'DEFAULT', 'app', app )

  ## Uncomment to view the final section/key/value entries
  #for name,proxy in config.items(): #{
  #  print('Config Section [ %s ]:' % (name), file=sys.stderr)
  #  for key,val in config.items(name): #{
  #    print('  %s: %s' % (key, val), file=sys.stderr)
  #  #}
  ##}

  # Load requested extension(s) from config file if present
  if config.has_section('extensions'): #{
    # Retrieve the list of extensions excluding entries from the defaults
    # section
    defaults   = config['DEFAULT'].items()
    extensions = filter(lambda v: v not in defaults,
                        config['extensions'].items())
    for name, load_extension in extensions: #{
      if args.verbosity > 1: #{
        print('>>> Load extension[%s]: %s' % (name, load_extension))
      #}
      if not load_extension: continue
      app.load_extension( name )
    #}
  #}

  if args.extension is not None: #{
    # Load requested extension(s)
    for name in args.extension: #{
      app.load_extension( name )
    #}
  #}

  if args.target is not None: #{
    # Probe a single drive
    _probe_drive( config, app, args )

  elif args.list: #}{
    # Scan and list all avaialble drives
    _scan_drives( config, app, args )

  else: #}{
    # Create a scheduler that will # scan all drives, enumerate all available
    # pages, create and implement a retrieval schedule
    _create_scheduler( config, app, args )
  #}
#}

def _load_config( args ): #{
  """
    Create a new configuration based upon `DEFAULTS`, locate and load any
    configuration file, and update the configuration with the provided
    command-line arguments.

    Args:
      args (Namespace)    The incoming command line arguments with which to
                          update `config`;

    Return:
      (SgConfig)  The new configuration;
  """
  import sg_logs.config

  config = sg_logs.config.SgConfig( DEFAULTS['config'] )

  #######################################
  # Attempt to locate the config file
  #
  do_load     = False
  config_path = None

  if isinstance(args.config, str): #{
    #
    # If provided as an argument, simply test for existence with no further
    # location attempts
    #
    config_path = args.config
    if not os.path.exists( config_path ): #{
      raise Exception('file[ %s ] does not exist' % (config_path))
    #}
    do_load = True

  else: #}{
    #
    # Heuristics to locate the config file:
    #   1) using the path from DEFAULTS
    #   2) prepended with PROG_ROOT
    #       e.g. /etc/sg-logs/config.ini
    #            => /usr/local/etc/sg-logs/config.ini
    #   3) basename (within the current directory)
    #
    config_path = DEFAULTS['config.ini']

    if os.path.exists( config_path ): #}{
      do_load = True

    else: #}{
      # 2) Try relative to PROG_ROOT
      try_path = PROG_ROOT + config_path

      if os.path.exists( try_path ): #{
        config_path = try_path
        do_load     = True

      else: #}{
        # 3) Try the basename
        try_path = os.path.basename( config_path )

        if os.path.exists( try_path ): #{
          config_path = try_path
          do_load     = True

        elif args.verbosity: #{
          print('=== Config file [ %s ] does not exist (skip)' %
                            (config_path), file=sys.stderr)
        #}
      #}
    #}
  #}

  if do_load: # {
    # Load the identified config file
    if args.verbosity > 1: #{
      print('>>> Load config file[ %s ] ...' % (config_path), file=sys.stderr)

    elif args.verbosity: #}{
      rel_path = config_path.replace( os.getcwd() +'/', '' )
      print('>>> Load config file[ %s ] ...' % (rel_path), file=sys.stderr)
    #}

    config.read( config_path )
  #}

  ###############################################################
  # Heuristics to locate the SCSI Log page db:
  #   1) using the path from config.log_page.db
  #   2) relative to the directory of the specified config file
  #      (whether or not the config file existed)
  #   3) basename (within the current directory)
  #
  # See if the named 'log_page.db' file can/should be modified according to the
  # specified location of the config file.
  db_path = config.get('log_page', 'db')
  if not os.path.exists( db_path ): #{
    # 2) Try relative to the config path
    config_base = os.path.dirname( config_path )

    do_update = False
    try_path  = os.path.join( config_base, db_path )
    if os.path.exists( try_path ): #{
      db_path = try_path
      do_update = True

    else: #}{
      # 3) Try the basename
      try_path = os.path.basename( db_path )

      if os.path.exists( try_path ): #{
        db_path   = try_path
        do_update = True
      #}
    #}

    if do_update: #{
      # Found it! Update the db path
      if args.verbosity > 1: #{
        print('=== Redirect SCSI Log Page db to [ %s ]' %
              (db_path), file=sys.stderr)

      elif args.verbosity: #}{
        rel_path = db_path.replace( os.getcwd() +'/', '' )

        print('=== Redirect SCSI Log Page db to [ %s ]' %
              (rel_path), file=sys.stderr)
      #}

      config.set( 'log_page', 'db', db_path )
    #}
  #}

  # Update `config` with the incoming `args`
  _update_config_with_args( config, args )

  return config
#}

def _update_config_with_args( config, args ): #{
  """
    Given an existing configuration and set of incoming command-line arguments,
    update the configuration.

    Args:
      config (SgConfig)   The configuration to be updated;
      args (Namespace)    The incoming command line arguments with which to
                          update `config`;

    Return:
      (SgConfig)  The updated `config`;
  """
  # assert( isinstance( config, SgConfig ) )

  # Mix-in argument-based over-rides for config section settings
  arg_vars = vars(args)
  for sect_name,sect_dict in DEFAULTS['config'].items(): #{

    for key,default_val in sect_dict.items(): #{
      if default_val is None: continue

      arg_val = arg_vars.get( key, None )
      if arg_val is None: #{
        # See if there is a negating version of this default key
        arg_val = arg_vars.get( 'no_'+ key, None )

        if arg_val: arg_val = False
        else:       arg_val = None
      #}

      if arg_val is not None: #{
        # An argument was provided as an over-ride
        str_val = (arg_val if isinstance(arg_val, str) else repr(arg_val))

        config.set( sect_name, key, str_val )
      #}
    #}
  #}

  # Special handling for `verbosity`
  if args.verbosity is not None: #{
    # Over-ride the DEFAULT DEBUG level from `config`
    #config['DEFAULT']['DEBUG'] = str(args.verbosity)
    config.set( 'DEFAULT', 'DEBUG',     str(args.verbosity) )
    config.set( 'DEFAULT', 'verbosity', str(args.verbosity) )
  #}

  return config
#}

def _signal_handler( sig, frame ): #{
  global scheduler
  global app
  print('\n=== SHUTDOWN', file=sys.stderr)

  if app is not None: #{
    app.shutdown()
    #app = None
  #}

  if scheduler is not None: #{
    scheduler.shutdown()
    #scheduler = None
  #}
#}

def _gather_drives( config, app, args ): #{
  """
    Perform a scan of all SCSI drives

    Args:
      config (SgConfig) The configuration data;
      app (SgLogs)      The primary app;
      args (obj)        Incoming arguments

    Returns:
      (dict)  The set of unique drives (by serial number)
  """
  if args.verbosity: #{
    print('=== Gather unique drives ...', file=sys.stderr)
  #}

  # Scan all drives using the 'scan' configuration section
  # generating the set of unique drives (by serial number)
  unique_drives = app.gather_unique_drives( config['scan'] )

  if args.verbosity: #{
    print('=== %d unique drives' % (len(unique_drives)), file=sys.stderr )
  #}

  return unique_drives
#}

def _probe_drive( config, app, args ): #{
  """
    Probe a single SCSI drive

    Args:
      config (SgConfig) The configuration data;
      app (SgLogs)      The primary app;
      args (obj)        Incoming arguments
  """
  # A target should have the format:
  #   /dev/%dev%[:%class_id%:%dev_id%]
  #
  # Example:
  #   /dev/sdho:MR:8
  drive = app.probe_drive( args.target )

  if drive: #{
    if args.source is not None or args.raw or args.metric_sources: #{
      _process_drive( app, args, drive )
    else: #}{
      # Create a scheduler for this single drive
      unique_drives = { drive.serial : drive }

      _create_scheduler( config, app, args, unique_drives )
    #}
  #}
#}

def _scan_drives( config, app, args ): #{
  """
    Perform a scan of all SCSI drives

    Args:
      config (SgConfig) The configuration data;
      app (SgLogs)      The primary app;
      args (obj)        Incoming arguments
  """
  # Gather the set of unique drives (by serial number)
  unique_drives = _gather_drives( config, app, args )

  # Iterate over all unique drives
  for serial,drive in unique_drives.items(): #{
    _process_drive( app, args, drive )

    #drive.close()
  #}
#}

def _print_drive( args, drive, file=sys.stdout ): #{
  """
    Print a representation of the given drive

    Args:
      args (dict)     Command line arguments
      drive (Drive)   The target drive
  """
  #print( repr(drive) )  #'%s' % (drive))
  print( drive, file=file )

  if args.source == 'smart': #{
    smart = drive.smart

    print('  SMART capable  : %s' % (smart.capable), file=file)
    if smart.capable: #{
      print('  SMART enabled  : %s' % (smart.enabled), file=file)
      print('  SMART status   : %s' % (smart.status), file=file)
    #}
  #}
#}

def _process_drive( app, args, drive ): #{
  """
    Process a single drive.

    Args:
      app (SgLogs)                  The top-level application instance
      args (dict)                   Command line arguments
      drive (ScsiDrive | MrPDrive)  The target drive
  """
  if args.source is not None or args.metric_sources: #{
    _iterate_metric_sources( app, args, drive )

  else: #}{
    # Simply output fact-of information about this drive
    _print_drive( args, drive )
  #}

  #if drive_type == 'scsi': #{
  #  dicts = app.get_drive_page( drive_path, drive, page )
  #  for metric in dicts: #{
  #    #print( '  %-50s: %s' % (metric.name, metric.value) )
  #    print( '  ', metric )
  #    #print( '  ', metric.prometheus_format )
  #  #}
  ##}
#}

def _iterate_metric_sources( app, args, drive ): #{
  """
    Iterate over the set of supported metric sources for this drive and output
    information about each

    Args:
      app (SgLogs)                  The top-level application instance
      args (dict)                   Command line arguments
      drive (ScsiDrive | MrPDrive)  The target drive

  """
  output = (sys.stderr if args.publish == 'stderr'
                       else sys.stdout)

  _print_drive( args, drive, file=sys.stderr )

  src_found = False
  metric_sources  = drive.get_metric_sources()
  if len(metric_sources) < 1: #{
    print('  *** Cannot fetch supported metric sources',
            end='', file=sys.stderr)
    if drive._log_sense_failed: #{
      print(': %s' % (drive._log_sense_failed), end='', file=sys.stderr)
    #}
    print( file=sys.stderr )
  #}

  if args.verbosity > 0: #{
    print('=== %d supported metric sources ...' % (len(metric_sources)),
            file=sys.stderr )

    if args.source: #{
      print('=== args.source:', args.source, file=output)
    #}
  #}

  argSourceIsLogPage = (args.source is not None       and
                        isinstance(args.source, dict) and
                        'page' in args.source)

  from sg_logs.scsi.log_page import ScsiLogPage
  if argSourceIsLogPage and len(metric_sources) < 1: #{
    # Cannot retrieve the set of supported log pages
    # create a ScsiLogPage instance directly so we can try
    # and fetch it...
    log_page = ScsiLogPage( drive, args.source.get('page',    0x00),
                                   args.source.get('subpage', 0x00) )
    metric_sources.append( log_page )
  #}

  if len(metric_sources) < 1: #{
    # Cannot retrieve the set of supported log pages...
    #print('  *** Cannot fetch supported metric sources', file=sys.stderr)
    return
  #}

  if (args.metric_sources and not args.metricize) or args.verbosity > 0: #{
    print('%s  %-8s: %3s : %4s : %s' %
          ( ('' if args.metric_sources else '==='),
            'source', 'lvl', 'cost', 'title'),
          file=output)
  #}

  for src in metric_sources: #{
    if args.source: #{
      if argSourceIsLogPage: #{
        if not isinstance(src, ScsiLogPage): continue

        if src.page != args.source.get('page'): continue

        if ('subpage' in args.source and
            src.subpage != args.source.get('subpage')): #{
          continue
        #}
      else: #}{
        if args.source != src.name.lower(): #{
          continue
        #}
      #}
    #}

    # Output information about this source
    src_found = True

    if (args.metric_sources and not args.metricize) or args.verbosity > 0: #{
      print('%s  %8s : %3d : %3.2f : %s' %
            ( ('' if args.metric_sources else '==='),
              src.name, src.level, src.cost, src.title),
            file=output)
    #}

    if args.raw: #{
      #from pprint import pprint
      from sg_logs.utils  import prhex

      # Fetch the unparsed source data
      if isinstance( src, ScsiLogPage ): #{
        #
        # Fetch and parse the log page parameters and then output the
        # raw parameter values.
        #
        # :NOTE: We COULD also fetch the log page as raw binary
        #        and dump that but this would require changes:
        #           Sglogs.sense_log_page()   - accept a 'raw' flag
        #             Parser.sense_log_page() - accept a 'raw' flag
        #             ScsiLogPage.fetch()     - accept a 'raw' flag
        #               pass 'raw' to sg3utils.log_sense()
        #
        sense_data = app.fetch_source( src )

        #sense_data = src.fetch( verbosity = app.verbosity )
        #pprint( sense_data, indent=2 )

        nParams = len(sense_data['params'])

        print('    DS[ %s ], Format[ 0x%x ], %d parameter%s' %
                (sense_data['disable_save'],
                 sense_data['subpage_format'],
                 nParams, ('s' if nParams != 1 else '')) )

        for idex,param in enumerate(sense_data['params']): #{
          if 'subpage' in param: #{
            # parameter from a page/subpage list
            print('    %3d: page 0x%02x,%02x' %
                    (idex, param['page'],param['subpage']))

          elif 'page' in param: #}{
            # parameter from a page list
            print('    %3d: page 0x%02x' % (idex, param['page']))

          else: #}{
            # parameter from a standard log page
            print('    %3d: code[ 0x%x ], du:%d, tsd:%d, fmt[ 0x%x ], '\
                            'etc:%d, tmc:%d' %
                  (idex, param['code'],
                   param['disable_update'],               # du
                   param['target_save_disable'],          # tsd
                   param['format_and_linking'],           # fmt
                   param['enable_threshold_comparison'],  # etc
                   param['threshold_met_criteria']) )     # tmd

            prhex( param['value_raw'], indent='       : ')
          #}
        #}

      else: #}{
        # Fetch and output the raw data
        data = src.fetch( verbosity = app.verbosity )
        pprint( data, indent=2 )
      #}

    elif args.metricize or not args.metric_sources: #}{
      # Fetch and metricize this source
      metrics = app.metricize_source( src )
      app.publish_metrics( metrics )
    #}
  #}

  if not src_found: #{
    #_print_drive( args, drive )
    if argSourceIsLogPage: #{
      srcStr = ('0x%02x' % args.source.get('page', 0x00))

      if 'subpage' in args.source: #{
        srcStr = srcStr + (',%02x' % (args.source.get('subpage', 0x00)))
      #}
    else: #}{
      srcStr = args.source
    #}

    print('  *** Metric source %s is not supported' % (srcStr), file=sys.stderr)
  #}
#}

def _create_scheduler( config, app, args, unique_drives = None ): #{
  """
    Gather all unique drives along with their supported log pages.
    From this, generate and activate a retrieval schedule

    Args:
      config (SgConfig)             The configuration data;
      app (SgLogs)                  The primary app;
      args (obj)                    Incoming arguments;
      [unique_drives = None] (list) If provided, use this set of drives instead
                                    of scanning for unique drives;

  """
  from sg_logs.scheduler import Scheduler

  global scheduler

  config.set( 'scheduler', 'unique_drives', unique_drives )

  # Gather the set of unique drives (by serial number)
  scheduler = Scheduler( config['scheduler'] )

  # Add a reference to the top-level 'scheduler' to the default section
  config.set( 'DEFAULT', 'scheduler', scheduler )

  scheduler.start()
  #scheduler.stop()
#}

###############################################################################
def _parseInt( val ): #{
  """
    Attempt to convert a string to integer guessing the base
  """
  return int( val, 0 )
#}

def _parseSource_page( args ): #{
  """
    Parse out a log-page metric source identifier:
      %page-num%[,%sub-page-num%]

    This will modify `args.soruce` to identify the target page and sub-page:
      { 'page'    : int,
        ['subpage': int]
      }

    Args:
      args (Namespace)  The set of parsed arguments
      args.source (str) The `source` argument;

    Returns:
      (bool)  An indication of success
  """
  baseRe = re.compile('(?:(0x)|(0[0-7]+|0o)|(0b)|([0-9]+))', re.IGNORECASE)

  # assert( args.source is not None )
  parts = args.source.split(',')

  page    =  parts[0]
  subpage = (parts[1] if len(parts) > 1 else None)

  # Instead of allowing int to guess the base, determine it directly so we
  # can re-use it for subpage if needed
  #   Valid bases:
  #     0b = 2, 0|0o = 8, 0x = 16, else 10
  baseMatch = baseRe.match( page )
  if baseMatch: baseMatch = baseMatch.groups()

  if   baseMatch and baseMatch[0] is not None:  base = 16
  elif baseMatch and baseMatch[1] is not None:  base = 8
  elif baseMatch and baseMatch[2] is not None:  base = 2
  else:                                         base = 10

  try: #{
    args.source = {
      'page'    :int( page, base ),
      'subpage' : 0x00,
    }

  except: #}{
    print('*** Invalid page number [ %s ]' % (page), file=sys.stderr)
    return False
  #}

  if subpage is not None: #{
    #if subpage.startswith('0x') or subpage.startswith('0'): base = 0
    try: #{
      args.source['subpage'] = int( subpage, base )

    except: #}{
      print('*** Invalid base %d subpage number [ %s ]' % (base, subpage),
              file=sys.stderr)
      return False
    #}
  #}

  return True
#}

def _parseSource( args ): #{
  """
    Parse out the metric source:
      - log page/subpage:
          %page-num%[,%sub-page-num%]
      - smart info:
          smart

    Args:
      args (Namespace)  The set of parsed arguments
      args.source (str) The `source` argument;

    Returns:
      (bool)  An indication of success
  """
  pageRe = re.compile('(?:'
                      +   '0x[0-9a-f]{1,2}(?:,[0-9a-f]{1,2})?|'
                      +   '0[0-7]{1,3}(?:,[0-7]{1,3})?|'
                      +   '[0-9]{1,3}(?:,[0-9]{1,3})?'
                      +')',
                      re.IGNORECASE)

  # assert( args.source is not None )
  if pageRe.match( args.source ): #{
    return _parseSource_page( args )
  else: #}{
    args.source = args.source.lower();
  #}

  # Leave the source string in place
  return True
#}

if __name__ == '__main__': #{
  import argparse

  # Retrieve metadata about this utility
  try: #{
    # First, see if there is a local metadata.py
    import metadata

  except: #}{
    # NO -- retrieve metadata from the package/distribution
    import pkg_resources
    import distutils.dist
    import io

    dist = pkg_resources.get_distribution('sg_logs')
    metadata_str = dist.get_metadata( dist.PKG_INFO )
    metadata_obj = distutils.dist.DistributionMetadata()
    metadata_obj.read_pkg_file( io.StringIO(metadata_str) )

    class metadata: #{
      name        = metadata_obj.get_name() #dist.project_name,
      description = metadata_obj.get_description()
      version     = metadata_obj.get_version()
    #}
  #}

  #
  # :XXX: In general, don't use `default` for added arguments so we can
  #       properly recognize when this argument has been provided.
  #
  #       Instead of using the `ArgumentDefaultsHelpFormatter` as the
  #       `formatter_class` for our parser, manually include any relevant
  #       default value in `help` for added arguments
  #
  #       To avoid default values being set for `store_true/false` actions,
  #       use `store_const` instead for parameters that have DEFAULT values
  parser = argparse.ArgumentParser(
              prog            = metadata.name,
              description     = metadata.description )
              #formatter_class = argparse.ArgumentDefaultsHelpFormatter )

  parser.add_argument(
      '--config', '-c',
      help    = 'The path to an INI formatted configuration file' )
  parser.add_argument(
      '--extension', '-e',
      action  = 'append',
      help    = 'Extension to load' )
  parser.add_argument(
      '--list', '-l',
      action  = 'store_const',
      const   = True,
      help    = 'List the available drives' )
  parser.add_argument(
      '--one-shot', '-o',
      action  = 'store_const',
      const   = True,
      help    = 'Perform a single collection schedule/period and terminate' )
  parser.add_argument(
      '--source', '-s',
      help    = 'Show the given metric source (e.g. log-page[,subpage])'
              + ' for each drive'
              + ' (implies either --list or a specific target)' )
  parser.add_argument(
      '--parse-unknown-pages', '-u',
      action  = 'store_const',
      const   = True,
      help    = 'Parse pages that are not explicitly known' )
  parser.add_argument(
      '--period', '-P',
      help    = 'An override of the scheduling period '
              + ' (implies either a scan or a specific target)'
              + ' (default: '+ DEFAULTS['config']['scheduler']['period'] +')')
  parser.add_argument(
      '--publish', '-O',
      help    = 'Set/Override the publish setting'
              + ' (default: '+ DEFAULTS['config']['DEFAULT']['publish'] +')')
  parser.add_argument(
      '--publish-format', '-F',
      help    = 'Set/Override the publish format (prometheus | json)'
              + ' (default: '
              +     DEFAULTS['config']['DEFAULT']['publish_format'] +')')
  parser.add_argument(
      '--raw', '-r',
      action  = 'store_true',
      help    = 'Used with --page to output the raw sense data' )
  parser.add_argument(
      '--report-level', '-R',
      help    = 'Set/Override the report level'
              + ' (default: '
              +     str(DEFAULTS['config']['log_page']['report_level']) +')')
  parser.add_argument(
      '--no-report-unknown-params', '-U',
      action  = 'store_const',
      const   = True,
      help    = 'Within parsed pages, do NOT report parameters that are not'
              + ' explicitly known (e.g. vendor-specific, reserved)' )
  parser.add_argument(
      '--metric-sources', '-S',
      action  = 'store_true',
      help    = 'Show the set of supported metric sources for each drive'
              + ' (implies either --list or a specific target)' )
  parser.add_argument(
      '--metricize', '-m',
      action  = 'store_true',
      help    = 'Metricize all sources instead of simply listing them'
              + ' (implies --metric-sources along with either'
              + ' --list or a specific target)' )
  parser.add_argument(
      '--verbosity', '-v',
      action  = 'count',
      default = 0,
      help    = 'Increase verbosity' )
  parser.add_argument(
      '--version', '-V',
      action  = 'store_true',
      help    = 'Show the version and exit' )
  parser.add_argument(
      'target',
      nargs   = '?',
      help    = 'A single target to probe (e.g. /dev/sda, /dev/sdho:MR:8).' )

  args = parser.parse_args()

  # Handle a 'page' string that may include a comma separated subpage
  args.ok = True

  if args.version: #{
    print('%s v%s' % (metadata.name, metadata.version), file=sys.stderr)
    print('%s' % (metadata.description), file=sys.stderr)
    sys.exit()
  #}

  if args.source is not None: #{
    args.ok = _parseSource( args )

  elif args.metricize: #}{
    # --metricize => --metric-sources
    args.metric_sources = True
  #}

  if args.source is not None or args.metric_sources: #{
    # Use of any of these arguments with no target implies `--list`
    if args.target is None: #{
      args.list = True
    #}
  #}

  ## Uncomment to view the final section/key/value entries
  #for key,val in args.__dict__.items(): #{
  #  print('  %s : %s' % (key,val), file=sys.stderr)
  ##}

  if args.ok: #{
    main( args )
  #}
#}

# vi: ft=python
