The [Advanced Intrusion Detection
Environment](https://linux.die.net/man/1/aide) (`aide`) is "an
intrusion detection system for checking the integrity of files".

### Configuration and Reporting
It is configured via `/etc/aide.conf` and uses a custom database
(`/var/lib/aide.db` by default) to track state.

On each run, a report is generated and sent to one or more endpoints, specified
via one or more `report` command-line or `report_url` configuration settings.
According to [aide.conf](https://linux.die.net/man/5/aide.conf#Urls), valid
endpoints are URLs from the list:
- `stdout`
- `stderr`
- `fd:%number%`
- `file://%file/path%`

Looking at the `aide` source
([src/util.c](https://github.com/aide/aide/blob/6a772b8f3d5db3ed3e5ec67211311cb500344176/src/util.c)
 [#47](https://github.com/aide/aide/blob/6a772b8f3d5db3ed3e5ec67211311cb500344176/src/util.c#L47)) and
 [#66](https://github.com/aide/aide/blob/6a772b8f3d5db3ed3e5ec67211311cb500344176/src/util.c#L66)),
there is also support for:
- `syslog:%facility%`;
- `sql:host:port:db:user:password:table` (`WITH_PSQL`);
- `http:[//authority]path[?query][#fragment]%` (`WITH_CURL`);
- `https:[//authority]path[?query][#fragment]%` (`WITH_CURL`);
- `ftp:[//authority]path[?query][#fragment]%` (`WITH_CURL`);


### Version and Compilation Options
Version and compilation options can be found via:
``` bash
$ sudo aide --version
Aide 0.15.1

Compiled with the following options:

WITH_MMAP
WITH_POSIX_ACL
WITH_SELINUX
WITH_PRELINK
WITH_XATTR
WITH_E2FSATTRS
WITH_LSTAT64
WITH_READDIR64
WITH_ZLIB
WITH_GCRYPT
WITH_AUDIT
CONFIG_FILE = "/etc/aide.conf"
```

The default configuration file (`/etc/aide.conf`) establishes two `report_url`s
&mdash; `file:/var/log/aide/aide.log` and `stdout`.


### Logging to `journald` via `syslog`

To change reporting to use `journald` via `syslog`, modify `/etc/aide.conf`:
``` config
#
# Disable the default report_url entries
#
#report_url=file:@@{LOGDIR}/aide.log
#report_url=stdout
#
# Add a new report_url for syslog
report_url=syslog:LOG_AUTH
```

**NOTE:** logged message are very textual and will require a bit of processing
to extract useful information.


### Setup and Testing
``` bash
#
# Initialize the aide database using the updated configuration file
#
$ sudo aide --init

AIDE, version 0.15.1

### AIDE database at /var/lib/aide/aide.db.new.gz initialized.

#
# Move the new database into place
#
$ sudo mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz

#
# Watch the journald log for messages
#
$ sudo journalctl -f
#########################################################
# After the commands run from the second window (below),
# you should see something like:
#
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]: AIDE 0.15.1 found differences between database and filesystem!!
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]: Start timestamp: 2020-01-21 16:39:18
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]:
                                                          Summary:
                                                            Total number of files:        1199
                                                            Added files:                        0
                                                            Removed files:                1
                                                            Changed files:                0
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]:
                                                          ---------------------------------------------------
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]: Removed files:
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]: ---------------------------------------------------
Jan 21 16:39:19 ip-172-31-38-142.ec2.internal aide[2708]: removed: /home/user/trans.id
```

In a second window:
``` bash
# Modify a file that is being watched and then run:
$ sudo aide --check
AIDE 0.15.1 found differences between database and filesystem!!
Start timestamp: 2020-01-21 16:39:18

Summary:
  Total number of files:	1199
  Added files:			0
  Removed files:		1
  Changed files:		0


---------------------------------------------------
Removed files:
---------------------------------------------------

removed: /home/user/trans.id
```

**NOTE:** differences will be reported until the database is re-initialized
with a new state.


