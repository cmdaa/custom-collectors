This Python module is a fork of
[python-sgutils](https://github.com/agrover/python-sgutils)
which adds support for the custom [sg3_utils](../sg3_utils) library.

# Python help

Generated via:
```python
import sg3utils

help(sg3utils)
```

---
```
Help on module sg3utils:

NAME
    sg3utils - sg3utils_module

FUNCTIONS
    asc_ascq_str(...)
        Given SCSI ASC and ASCQ codes, generate the string representing the
        codes.
        
        Args:
          asc {int}  The SCSI Additional Sense Code (ASC);
          ascq {int} The SCSI Additional Sense Code Qualifier (ASCQ);
        
        Returns:
          {str}  The string representation of the provide `asc` and `ascq`
        
        Example:
          sg3utils.asc_ascq_str( 0x5d, 0x00 )
            -> 'Failure prediction threshold exceeded'
    
    close(...)
        Close a SCSI device opened via sg3utils.open().
        
        Args:
          handle {int} The handle returned from sg3utils.open();
        
        Returns (on success):
          {int}  0
        
        Exception (on failure)
        
        Example:
          sg3utils.close( handle )
    
    inquiry(...)
        Returns the result of INQUIRY.
        See SCSI SPC-3 spec for more info:
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [page = 0] {int}       If provided, perform an inquiry for the given
                                 VPD page. If NOT provided (or 0), perform a
                                 simple inquiry.
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
          {bytearray}  If `raw` is True;
          {mixed}      If `raw` is False, the interpreted response data
                       depending on the requested page;
        
        Example:
          sg3utils.inquiry( handle ) ->
            {vendor:, product:, revision:, serial:, peripheral_qualifier:,
             peripheral_type:, version:, is_removable_media:,
             normal_aca_support:, hierarchical_support:, response_data_format:,
             scc_support:, access_controls_coordinator:,
             target_port_group_support:, third_party_copy:, protect:,
             enclosure_services:, multi_port:, command_queuing: }
        
        Currently supported pages:
          0x00 (default): Simple Inquiry
            Returns a dict of inquiry data:
               vendor                      : (str)
               product                     : (str)
               revision                    : (str)
               serial                      : (str)  
               peripheral_qualifier        : (long)
               peripheral_type             : (long)
               version                     : (long)
               is_removable_media          : (bool)
               normal_aca_support          : (bool)
               hierarchical_support        : (bool)
               response_data_format        : (long)
               scc_support                 : (bool)
               access_controls_coordinator : (bool)
               target_port_group_support   : (bool)
               third_party_copy            : (long)
               protect                     : (bool)
               enclosure_services          : (bool)
               multi_port                  : (bool)
               command_queuing             : (bool)
        
          0x80: Unit serial number
            Returns a string of the unit serial number.
        
          0x83: Device information
            Returns a list of identification descriptors where each
            element is a dict containing:
             protocol_id : (str)
             char_set    : (str) Character set
             association : (str) Associated entity
             id_type     : (str)
             id_value    : (bytearray)
        
          0xb1: Block Device Characteristics
            Returns a dict of inquiry data:
               product_type                : (str)
               form_factor                 : (str)
               rpm                         : (str | int)
                 valid strings: not-reported, non-rotating
    
    is_mr(...)
        Check if the SCSI device opened via sg3utils.open() is controlled by a
        MegaRaid controller. If True, this handle MAY already target a specific
        physical disk behind the controller (sg3utils.mr_get_target()). If it
        does not, it may be used to retrieve the set of physical disks
        associated with the controller (sg3utils.mr_get_pd_list()) and may then
        be targeted to a specific physical disk (sg3utils.mr_set_target()).
        Until a specific disk is targeted, such a handle will provide access
        to the controller but will likely fail when used with most other
        sg3utils calls.
        
        Args:
          handle {int} The handle returned from sg3utils.open();
        
        Returns (on success):
          {bool}  True | False
        
        Exception (on failure)
        
        Example:
          sg3utils.is_mr( handle ) -> True
    
    log_sense(...)
        Returns the result of LOG_SENSE.
        See SCSI SPC-3 spec for more info:
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [page = 0] {int}       The target log page (0 == list of pages);
          [subpage = 0] {int}    The target subpage;
          [param  = 0] {int}     The target parameter;
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
           {bytearray} If `raw` is True;
           {dict}      If `raw` is False;
                         { disable_save  : (bool),
                           subpage_format: (int),
                           page          : (int),
                           subpage       : (int),
                           params        : (list of dicts),
                         }
        
           For page 0, subpage 0, each entry in `params` will be:
             { page: (int) }
        
           For page 0, subpage 0xff, each entry in `params` will be:
             { page: (int), subpage: (int) }
        
           For all other log pages, each entry in `params` will be:
             { code                        : (int),
               offset                      : (int),
               disable_update              : (bool),
               target_save_disable         : (bool),
               enable_threshold_comparison : (bool),
        
               threshold_met_criteria      : (int),
               # 0x00  every update of the cumulative value;
               # 0x01  Cumulative value  equal to      threshold value;
               # 0x02  Cumulative value  not equal to  threshold value;
               # 0x03  Cumulative value  greater than  threshold value;
        
               format_and_linking          : (int),
               # 0x00  Bounded data counter
               # 0x01  ASCII format list
               # 0x02  Bounded data counter or unbounded data counter
               # 0x03  Binary format list
        
               value_raw                   : (bytearray),
               # Raw bytes of the parameter value
        
               value                       : (str | int | bytearray),
               # `value_raw` interpreted according to `format_and_linking`
             }
        
        Example:
          sg3utils.log_sense( fd ) ->
            { disable_save:,
              subpage_format:,
              page:,
              subpage:,
              params:,
            }
    
    mode_sense(...)
        Returns the result of MODE SENSE(10), as a dictionary of sense mode
        codes to page code data.
        See SCSI SPC-3 spec for more info.
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [page = 0x3f] {int}    The target log page (0x3f == supported pages);
          [subpage = 0] {int}    The target subpage;
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
           {bytearray} If `raw` is True;
           {dict}      If `raw` is False;
                         { page        : (int),
                           subpage     : (int),
                           medium_type : (int),
                           specific    : (int),
                           long_lba    : (bool),
                           data        : (list | dict | bytearray),
                         }
        
           The default type of `data` will bytearray;
        
           For page 0x1c,00, `data` will be a dict of the form:
             performance               : (bool)
             enable_background_function: (bool)
             enable_warning            : (bool)
             disable_exception_control : (bool)
             test                      : (bool)
             enable_background_error   : (bool)
             log_error                 : (bool)
             method_of_reporting_ie    : (int)
               0 : no reporting
               1 : async reporting (obsolete)
               2 : generate unit attention
               3 : conditionally generate recovered error
               4 : unconditionally generate recovered error
               5 : generate no sense
               6 : only report informational exception condition on request
             interval_timer            : (int) (100 ms increments)
             report_count              : (int)
        
           For page 0x3f, `data` will be a list of supported pages with
           bytearray data for each
        
        Example:
          sg3utils.mode_sense( fd ) ->
           { ... }
    
    mr_get_pd_list(...)
        Given a SCSI device opened via sg3utils.open() that is controlled by a
        MegaRaid controller, retrieve the set of physical disks behind the
        controller.
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
          {list} The list of physical disks where each entry contains:
                   {device_id:, encl_device_id:, encl_index:, slot_number:,
                    scsi_dev_type:, connect_port_bitmap:,
                    sas_addr_1:, sas_addr_2:}
        
        Exception (on failure)
        
        Example:
          sg3utils.mr_get_pd_list( handle ) ->
           [ {device_id:, encl_device_id:, encl_index:, slot_number:,
              scsi_dev_type:, connect_port_bitmap:,
              sas_addr_1:, sas_addr_2:},
             ...
           ]
    
    mr_get_target(...)
        Given a SCSI device opened via sg3utils.open() that is controlled by a
        MegaRaid controller, retrieve the id of the current target disk that
        will be used for further SCSI pass-through commands.
        
        Args:
          handle {int} The handle returned from sg3utils.open();
        
        Returns (on success):
          {int}  The target disk/device id;
        
        Exception (on failure)
        
        Example:
          did = sg3utils.mr_get_target( handle )
    
    mr_set_target(...)
        Given a SCSI device opened via sg3utils.open() that is controlled by a
        MegaRaid controller, set the target disk id for further SCSI
        pass-through commands.
        
        Args:
          handle {int} The handle returned from sg3utils.open();
          did    {int} The target disk/device id ('device_id' from the pd list);
        
        Returns (on success):
          {int}  The new target disk id;
        
        Exception (on failure)
        
        Example:
          sg3utils.mr_set_target( handle, 123 )
    
    open(...)
        Open a SCSI device returning a handle representing that opened device.
        
        Args:
          sg_name {str}                The path to the target SCSI device
          [read_write = False] {bool}  Open for read AND write;
          [verbosity = 0] {int}        Verbosity level;
        
        Returns (on success):
          {int}  file_descriptor
        
        Exception (on failure)
        
        Example:
          handle = sg3utils.open( "/dev/sda" )
    
    raw(...)
        Sends between 6 and 256 SCSI command bytes returning the response.
        
        Args:
          handle {int}                 The handle returned from sg3utils.open();
          cmd {bytearray}              The raw SCSI command bytes to send
                                       between 6 and 256 bytes, may be a CDB;
          [response_max = 1024] {int}  The maximum number of bytes to return;
          [data = None] {bytearray}    Additional data to send;
          [verbosity = 0] {int}        Verbosity level;
        
        Returns (on success):
           {dict}  of the form:
                     { status {int},           SCSI status response code
                       result_category {int},  SCSI result category
                       sense_category {int},   SCSI sense category
                       sense {bytearray},      Raw SCSI sense data
                       result {bytearray},     Raw SCSI result data
                     }
        
        Example:
          sg3utils.raw( fd, b'M
    
    readcap(...)
        Returns the result of READ CAPACITY(16), as a dict.
        See SCSI SBC-3 spec for more info
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
          {dict}       If `raw` is False, the constructed dictionary of
                       interpreted response data:
                         last_logical_block_address,
                         logical_block_length,
                         p_type,
                         prot_en,
                         p_i_exponent,
                         lbppbe,
                         lbpme,
                         lbprz,
                         lalba
          {bytearray}  If `raw` is True;
        
        Exception (on failure)
        
        Example:
          sg3utils.readcap( handle ) ->
           (last_logical_block_address, logical_block_length, p_type, prot_en,
            p_i_exponent, lbppbe, lbpme, lbprz, lalba)
    
    report_luns(...)
        Returns the result of REPORT LUNS.
        Currently only non-hierarchical LUNs are supported.
        See SCSI SPC-3 and SAM-5 specs for more info.
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
           {bytearray} If `raw` is True;
           {dict}      If `raw` is False;
        
        Example:
          sg3utils.report_luns( fd ) ->
           { ... }
    
    sat_identify(...)
        Perform a SCSI ATA PASSTHROUGH to identify a device.
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
           {dict}  of the form:
                     { status {int},           SCSI status response code
                       result_category {int},  SCSI result category
                       sense_category {int},   SCSI sense category
                       sense {bytearray},      Raw SCSI sense data
                       raw {bytearray},        Raw ATA IDENTIFY data
                       smart_available {bool}, Is SMART available?
                       smart_enabled {bool},   Is SMART enabled?
                       serial {str},           The device serial id
                       revision {str},         The device firmware revision
                       product {str},          The device product/model name
                     }
        
        Example:
          sg3utils.sat_identify( fd ) ->
           { 'status': 0
             'result_category': 0
             'sense_category': 0
             'sense': ...,
             'raw': (raw bytes from the identify command)
             'smart_available': True,
             'smart_enabled': True,
             'serial': 'SN1234567',
             'revision': '1011',
             'product': 'INTEL SSD12345',
           }
    
    sat_smart_data(...)
        Perform a SCSI ATA PASSTHROUGH to retrieve SMART data.
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [raw = False] {bool}   If True, return raw data;
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
           {dict}  of the form:
                     { status {int},           SCSI status response code
                       result_category {int},  SCSI result category
                       sense_category {int},   SCSI sense category
                       sense {bytearray},      Raw SCSI sense data
                       raw {bytearray},        Raw SCSI result data
                       offline_data_collection_status {int},
                       self_test_execution_status {int},
                       offline_data_collection_capability {int},
                       smart_capability {int},
                       error_logging_capability {int},
                       short_self_test_recommended_poll_time {int},
                       extended_self_test_recommended_poll_time {int},
                       conveyance_self_test_recommended_poll_time {int},
                       checkxum {int},
                       attrs {list},
                     }
        
           Each entry in `attrs` will have the form:
                     { id {int},               The SMART id of this attribute
                       flags {int},            Attribute flags
                       value_current {int},    The current value
                       value_worst {int},      The worst value
                       prefailure {bool},      Is this attribute prefail?
                       online {bool},          Is this attribute online?
                       value_raw {bytearray},  The raw value of the attribute
                     }
        
        Example:
          sg3utils.sat_smart_data( fd ) ->
           { 'status': 0
             'result_category': 0
             'sense_category': 0
             'sense': ...,
             'raw': b'<raw-result-data>'
             ...
           }
    
    scsi_id(...)
        Retrieve the SCSI id of a SCSI device opened via sg3utils.open().
        
        Args:
          handle {int}           The handle returned from sg3utils.open();
          [verbosity = 0] {int}  Verbosity level;
        
        Returns (on success):
          {dict|None} The SCSI id {host:, channel:, target:, lun:}
        
        Exception (on failure)
        
        Example:
          sg3utils.scsi_id( handle ) ->
           {host:1, channel:2, target:3, lun:0 }
```
---

# Build
``` bash
$ make

# OR
$ make include
$ python setup.py build
```

# Install
``` bash
$ make install

# OR
$ make include
$ python setup.py install
```
