import sys
import sg3utils

#from pprint import PrettyPrinter
#pprint = PrettyPrinter( indent=2 ).pprint
from pprint import pprint

def prhex( value, indent            = ' ',
                  no_initial_indent = False,
                  no_index          = False ): #{
  """
    Given a bytearray, output it as a block of hex values

    Args:
      value (bytearray)                   The byte array to print
      [indent = ' '] (str)                The indent to use for each new line
      [no_initial_indent = False] (bool)  If true, omit the initial indent
      [no_index = False] (bool)           If true, omit the index indicator
  """
  for idex,byte in enumerate(value): #{
    if idex % 16 == 0: #{
      if idex != 0 or not no_initial_indent: #{
        print('%s%s' % ('\n' if idex > 0 else '', indent), end='')
      #}
      if not no_index: #{
        print('%04x: ' % (idex), end='')
      #}

    elif idex % 8 == 0: #}{
      print('  ', end='')

    #}

    print('%02x' % (byte), end=' ')
  #}

  print('')
#}

def prval( value, indent='  ', no_initial_indent=False ): #{
  """
    Given a value, output it as either string, number, or block of hex values

    Args:
      value (str | int | bytearray) The value to print
      [indent = ' '] (str)          The indent to use for each new line
  """
  if isinstance(value, str): #}
    print('"%s"' % (value))

  elif isinstance(value, int): #}
    print('%d' % (value))

  elif isinstance(value, bytearray): #}{
    prhex( value, indent=indent, no_initial_indent=no_initial_indent )

  else: #}{
    print('')
    pprint( value, indent=len(indent) )
  #}
#}

###############################################################################
# Tests {
#
def test_inq_00( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.inquiry( handle, raw=raw, verbosity=verbosity )

    print('>>> inquiry: ', end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    prval( info )

  except Exception as ex: #}{
    print("*** inquiry FAILED:", ex)
  #}
  print("=========================================================")
#}


def test_inq_80( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.inquiry( handle, page=0x80, raw=raw, verbosity=verbosity )

    print(">>> inquiry 0x80 (unit serial number): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    prval( info )

  except Exception as ex: #}{
    print("*** inquiry 0x80 FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_inq_83( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.inquiry( handle, page=0x83, raw=raw, verbosity=verbosity )

    print(">>> inquiry 0x83: ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    prval( info )

  except Exception as ex: #}{
    print("*** inquiry 0x83 FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_inq_b1( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.inquiry( handle, page=0xb1, raw=raw, verbosity=verbosity )

    print(">>> inquiry 0xb1: ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    prval( info )

  except Exception as ex: #}{
    print("*** inquiry 0xb1 FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_report_luns( handle, raw=False, verbosity=0 ): #{
  try: #{
    luns = sg3utils.report_luns( handle, raw=raw, verbosity=verbosity )

    print(">>> report_luns: ", end='')
    if isinstance( luns, bytearray ): #{
      print(' (%d bytes)' % (len(luns)))
    #}

    prval( luns )

  except Exception as ex: #}{
    print("*** report_luns FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_readcap( handle, raw=False, verbosity=0 ): #{
  try: #{
    cap = sg3utils.readcap( handle, raw=raw, verbosity=verbosity )

    print(">>> readcap: ", end='')
    if isinstance( cap, bytearray ): #{
      print(' (%d bytes)' % (len(cap)))
    #}

    prval( cap )

  except Exception as ex: #}{
    print("*** readcap FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_1( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, raw       = raw,
                                       verbosity = verbosity )

    if raw: #{
      print('>>> supported log pages:')
      prhex( info )

    else: #}{
      print(">>> %d supported log pages:" % (len(info['params'])) )
      for param in info['params']: #{
        print("      0x%02x" % (param['page']))
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_2( handle, raw=False, verbosity=0 ): #{
  Page_map = [
    "supported log pages",            # 0x00  subpages 0xff
    "buffer over/under-run",          # 0x01
    "write error",                    # 0x02
    "read error",                     # 0x03
    "reverse read error",             # 0x04
    "verify error",                   # 0x05
    "non-medium error",               # 0x06
    "last n error",                   # 0x07
    "format status",                  # 0x08
    "0x09",                           # 0x09
    "0x0a",                           # 0x0a
    "last n deferred error",          # 0x0b  subpages 0x1, 0x2
    "logical block provisioning",     # 0x0c  OR Sequential access device (tape)
    "temperature",                    # 0x0d  subpages 0x1, 0x2
    "start-stop cycle counter",       # 0x0e  subpages 0x1
    "application client",             # 0x0f
    "self-test results",              # 0x10
    "solid state media",              # 0x11  OR device status
    "tape alert response",            # 0x12

    "requested recovery",             # 0x13
    "device statistics",              # 0x14  OR media changer statistics
    "background scan results",        # 0x15  subpages 0x2, 0x3
    "ata pass-through results",       # 0x16  OR tape diagnostic data
                                      #       OR media changer diagnostic data
    "non-volatile cache",             # 0x17  subpages 0x0 .. 0x1f (vlm stats)
    "protocol specific port",         # 0x18
    "general stats and perf",         # 0x19  subpages 0x1 .. 0x1f (grp stats)
    "power condition transition",     # 0x1a
    "data compression",               # 0x1b
    "0x1c",                           # 0x1c
    "0x1d",                           # 0x1d
    "0x1e",                           # 0x1e
    "0x1f",                           # 0x1f
    "0x20",                           # 0x20
    "0x21",                           # 0x21
    "0x22",                           # 0x22
    "0x23",                           # 0x23
    "0x24",                           # 0x24
    "0x25",                           # 0x25
    "0x26",                           # 0x26
    "0x27",                           # 0x27
    "0x28",                           # 0x28
    "0x29",                           # 0x29
    "0x2a",                           # 0x2a
    "0x2b",                           # 0x2b
    "0x2c",                           # 0x2c
    "current service information",    # 0x2d
    "tape alert",                     # 0x2e
    "informational exceptions",       # 0x2f
    "performance counters (hitachi)", # 0x30  (hitachi OR lto-5/6 tape usage)
    "tape capacity",                  # 0x31  (lto-5/6)
    "data compression",               # 0x32  (lto-5)
    "write errors",                   # 0x33  (lto-5)
    "read forward errors",            # 0x34  (lto-5)
    "dt device error",                # 0x35  (lto-5/6)
    "0x36",                           # 0x36
    "cache (seagate)",                # 0x37
    "blocks/bytes transferred",       # 0x38  (lto-5)
    "host port 0 iface errors",       # 0x39  (lto-5)
    "drive control verification",     # 0x3a  (lto-5)
    "host port 1 iface errors",       # 0x3b  (lto-5)
    "drive usage information",        # 0x3c  (lto-5)
    "subsystem statistics",           # 0x3d  (lto-5)
    "factory (seagate/hitachi)",      # 0x3e  OR device status for lto-5/6
    "0x3f",                           # 0x3f
  ]

  pages = []
  try: #{
    info = sg3utils.log_sense( handle,  subpage   = 0xff,
                                        raw       = raw,
                                        verbosity = verbosity )

    if raw: #{
      print('>>> supported log pages/subpages:')
      prhex( info )
      return

    else: #}{
      pages = info['params']

      #for param in info['params']: #{
      #  pages.append( param );
      #  print("      0x%02x, 0x%02x" % (param['page'], param['subpage']))
      ##}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}

  print(">>> %d supported log pages/subpages:" % (len(pages)))
  for info in pages: #{
    page_code = info['page']
    if page_code == 0x00:  continue

    page_name = (Page_map[page_code] if page_code < len(Page_map) \
                            else '0x%x' % (page_code))

    if verbosity > 1: #{
      try: #{
        info = sg3utils.log_sense( handle, page      = page_code,
                                           subpage   = info['subpage'],
                                           verbosity = verbosity )

        #print(">>> Page 0x%x [ %s ]:" % (page_code, page_name), info )

        print(">>> Page 0x%02x, %02x [ %s ]: %d params" %
                (page_code, info['subpage'], page_name, len(info['params'])) )

        for param in info['params']: #{
          val = param['value']

          print('  %6d:' % (param['code']), end=' ')

          if verbosity > 2: #{
            print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                          'value len[ %d ]:' %
                    (param['disable_update'],
                     param['target_save_disable'],
                     param['enable_threshold_comparison'],
                     param['threshold_met_criteria'],
                     param['format_and_linking'],
                     len(param['value'])), end=' ')

            if isinstance( val, bytearray ): #{
              if page_code == 0xe and param['code'] > 2: #{
                # Page 0x0E: parameters 3,4,5,6 are 4-byte binary numbers
                #             val = int.from_bytes(val, byteorder='big')
                #
                int_val = int.from_bytes( val, byteorder='big' )

                print(", value len[ %d ]:4-byte-binary: %u" %
                      (len(val), int_val))

              elif page_code == 0xd: #}{
                # Page 0x0D (temperature):
                #   Each parameter is a 2-byte value:
                #     Byte 0: Reserved
                #     Byte 1: Temerature (degrees C)
                int_val = val[1]

                print(", value len[ %d ]:%stemperature: %u C" %
                        (len(val), ("reference-" if param['code'] == 1 \
                                                 else ""), int_val) )

              else: #}{
                print(", value len[ %d ]:bytearray" % (len(val)))
                prhex( val, indent='          ' )
              #}
            else: #}{
              print(", value:", end=' ')
              prval( val, indent='          ' )
            #}

          else: #}{
            prval( val, indent='        : ', no_initial_indent=True )

          #}
        #}

      except Exception as ex: #}{
        print("*** log_sense FAILED:", ex)
      #}
    else: #}{
      print('  0x%02x, %02x [ %s ]' %
              (info['page'], info['subpage'], page_name))

    #}
  #}
  print("=========================================================")
#}

def test_log_sense_temperature( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, page=0xd, raw=raw, verbosity=verbosity )

    print(">>> log_sense (temperature): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    if raw: #{
      prhex( info )

    else: #}{
      print(">>>  %d params" % (len(info['params'])) )
      for param in info['params']: #{

        print('  %6d:' % (param['code']), end=' ')

        if verbosity: #{
          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                        'value len[ %d ]:' %
                  (param['disable_update'],
                   param['target_save_disable'],
                   param['enable_threshold_comparison'],
                   param['threshold_met_criteria'],
                   param['format_and_linking'],
                   len(param['value'])), end=' ')
        #}
        print('%d C' % (param['value'][1]) )
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_start_stop( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, page=0xe, raw=raw, verbosity=verbosity )
    print(">>> log_sense (start/stop): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}

    if isinstance(info, bytearray): prhex( info )
    elif verbosity:                 pprint( info, indent=2 )
    else: #{

      print(">>>  %d params" % (len(info['params'])) )
      for param in info['params']: #{
        fmt = param['format_and_linking']
        val = (int.from_bytes(param['value'], byteorder='big') \
                  if fmt == 3 else param['value'])

        print('  %6d:' % (param['code']), end=' ')

        if verbosity: #{
          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                        'value len[ %d ]:' %
                  (param['disable_update'],
                   param['target_save_disable'],
                   param['enable_threshold_comparison'],
                   param['threshold_met_criteria'],
                   param['format_and_linking'],
                   len(param['value'])), end=' ')

        else: #}{
          prval( val, indent='        : ', no_initial_indent=True )

        #}
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_background_scan( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, page=0x15, raw=raw, verbosity=verbosity )
    print(">>> log_sense (background scan): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}


    if isinstance(info, bytearray): prhex( info )
    elif verbosity:                 pprint( info, indent=2 )
    else: #{

      print(">>>   %d params" % (len(info['params'])) )
      for param in info['params']: #{
        fmt = param['format_and_linking']
        val = (int.from_bytes(param['value'], byteorder='big') \
                  if fmt == 3 else param['value'])

        print('  %6d:' % (param['code']), end=' ')

        if verbosity: #{
          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                        'value len[ %d ]:' %
                  (param['disable_update'],
                   param['target_save_disable'],
                   param['enable_threshold_comparison'],
                   param['threshold_met_criteria'],
                   param['format_and_linking'],
                   len(param['value'])), end=' ')

        else: #}{
          prval( val, indent='        : ', no_initial_indent=True )

        #}
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_cache( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, page=0x37, subpage=1,
                               raw=raw, verbosity=verbosity )
    print(">>> log_sense 0x37,1 (cache): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}


    if isinstance(info, bytearray): prhex( info )
    elif verbosity:                 pprint( info, indent=2 )
    else: #{

      print(">>>   %d params" % (len(info['params'])) )
      for param in info['params']: #{
        fmt = param['format_and_linking']
        val = (int.from_bytes(param['value'], byteorder='big') \
                  if fmt == 3 else param['value'])

        print('  %6d:' % (param['code']), end=' ')

        if verbosity: #{
          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                        'value len[ %d ]:' %
                  (param['disable_update'],
                   param['target_save_disable'],
                   param['enable_threshold_comparison'],
                   param['threshold_met_criteria'],
                   param['format_and_linking'],
                   len(param['value'])), end=' ')

        else: #}{
          prval( val, indent='        : ', no_initial_indent=True )

        #}
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_log_sense_application_client( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.log_sense( handle, page=0xf, raw=raw, verbosity=verbosity )
    print(">>> log_sense (application client): ", end='')
    if isinstance( info, bytearray ): #{
      print(' (%d bytes)' % (len(info)))
    #}


    if isinstance(info, bytearray): prhex( info )
    elif verbosity:                 pprint( info, indent=2 )
    else: #{

      print(">>>  %d params" % (len(info['params'])) )
      for param in info['params']: #{
        fmt = param['format_and_linking']
        val = param['value']

        print('  %6d:' % (param['code']), end=' ')

        if verbosity: #{
          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
                        'value len[ %d ]:' %
                  (param['disable_update'],
                   param['target_save_disable'],
                   param['enable_threshold_comparison'],
                   param['threshold_met_criteria'],
                   param['format_and_linking'],
                   len(param['value'])), end=' ')

        else: #}{
          prval( val, indent='        : ', no_initial_indent=True )

        #}
      #}
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_mode_sense( handle, raw=False, verbosity=0 ): #{
  Page_map = {
    0x00: "unit attention condition",       # common
    0x01: "read-write error recovery",      # disk
    0x02: "disconnect reconnect",           # common

    0x03: "format (obsolete)",              # disk
    0x04: "rigid disk geometry (obsolete)", # disk
    0x05: "flexible disk (obsolete)",       # disk

    0x07: "verify error recovery)",         # disk
    0x08: "caching",                        # disk
    0x09: "peripheral device (obsolete)",   # common
    0x0a: "page control mode",              # common
                                            #  00 control
                                            #  01 control extension
                                            #  03 command duration limit a
                                            #  04 command duration limit b
                                            #
                                            # disk
                                            #  02 application tag
                                            #  05 IO advice hints grouping
                                            #  06 background operation control
                                            #  f1 parallel ata control

    0x0b: "medium types supported",         # disk
    0x0c: "notch and partition (obsolete)", # disk
    0x0d: "power condition (obsolete)",     # disk
    0x10: "xor control (obsolete)",         # disk

    0x14: "enclosure services management",  # common
    0x15: "extended",                       # common
    0x16: "extended device-type specific",  # common

    0x18: "protocol specific lu",           # common
    0x19: "protocol specific port",         # common
                                            #  00 protocol specific port
                                            #  01 phys control & discover
                                            #  02 share port control
                                            #  03 enhanced phy control
                                            #  e5 transceiver control (out)
                                            #  e6 transceiver control (in)
    0x1a: "power condition",                # common
                                            #  00 power condition
                                            #  01 power consumption
                                            # disk
                                            #  f1 ata power condition

    0x1c: "control",                        # common
                                            #  00 informational exceptions ctl
                                            # disk
                                            #  01 background control
                                            #  02 logical block provisioning

    0x3f: "supported mode pages",           # common
                                            #  00 supported pages
                                            #  ff supported pages/subpages
  }

  pages = []
  try: #{
    info = sg3utils.mode_sense( handle, page      = 0x3f,
                                        subpage   = 0xff,
                                        raw       = raw,
                                        verbosity = verbosity )

    if raw: #{
      print('>>> supported mode pages/subpages:')
      prhex( info )
      return

    else: #}{
      print('>>> supported mode pages/subpages:')
      print('   page       : 0x%02x' % (info['page']))
      print('   subpage    : 0x%02x' % (info['subpage']))
      print('   medium_type: 0x%02x' % (info['medium_type']))
      print('   specific   : 0x%02x' % (info['specific']))
      print('   long_lba   : %s'     % (info['long_lba']))
      print('   data       :', end=' ')
      print( info['data'] )
      #prhex( info['data'], indent='          : ', no_initial_indent=True )
      return
    #}

  except Exception as ex: #}{
    print("*** log_sense FAILED:", ex)
  #}

  #print(">>> %d supported log pages/subpages:" % (len(pages)))
  #for info in pages: #{
  #  page_code = info['page']
  #  if page_code == 0x00:  continue

  #  page_name = (Page_map[page_code] if page_code < len(Page_map) \
  #                          else '0x%x' % (page_code))

  #  if verbosity > 1: #{
  #    try: #{
  #      info = sg3utils.log_sense( handle, page      = page_code,
  #                                         subpage   = info['subpage'],
  #                                         verbosity = verbosity )

  #      #print(">>> Page 0x%x [ %s ]:" % (page_code, page_name), info )

  #      print(">>> Page 0x%02x, %02x [ %s ]: %d params" %
  #              (page_code, info['subpage'], page_name, len(info['params'])) )

  #      for param in info['params']: #{
  #        val = param['value']

  #        print('  %6d:' % (param['code']), end=' ')

  #        if verbosity > 2: #{
  #          print('du[ %d ], tsd[ %d ], etc[ %d ], tmc[ 0x%x ], fmt 0x%x, '
  #                        'value len[ %d ]:' %
  #                  (param['disable_update'],
  #                   param['target_save_disable'],
  #                   param['enable_threshold_comparison'],
  #                   param['threshold_met_criteria'],
  #                   param['format_and_linking'],
  #                   len(param['value'])), end=' ')

  #          if isinstance( val, bytearray ): #{
  #            if page_code == 0xe and param['code'] > 2: #{
  #              # Page 0x0E: parameters 3,4,5,6 are 4-byte binary numbers
  #              #             val = int.from_bytes(val, byteorder='big')
  #              #
  #              int_val = int.from_bytes( val, byteorder='big' )

  #              print(", value len[ %d ]:4-byte-binary: %u" % (len(val), int_val))

  #            elif page_code == 0xd: #}{
  #              # Page 0x0D (temperature):
  #              #   Each parameter is a 2-byte value:
  #              #     Byte 0: Reserved
  #              #     Byte 1: Temerature (degrees C)
  #              int_val = val[1]

  #              print(", value len[ %d ]:%stemperature: %u C" %
  #                      (len(val), ("reference-" if param['code'] == 1 \
  #                                               else ""), int_val) )

  #            else: #}{
  #              print(", value len[ %d ]:bytearray" % (len(val)))
  #              prhex( val, indent='          ' )
  #            #}
  #          else: #}{
  #            print(", value:", end=' ')
  #            prval( val, indent='          ' )
  #          #}

  #        else: #}{
  #          prval( val, indent='        : ', no_initial_indent=True )

  #        #}
  #      #}

  #    except Exception as ex: #}{
  #      print("*** log_sense FAILED:", ex)
  #    #}
  #  else: #}{
  #    print('  0x%02x, %02x [ %s ]' %
  #            (info['page'], info['subpage'], page_name))

  #  #}
  ##}
  print("=========================================================")
#}

def test_mode_sense_ie( handle, raw=False, verbosity=0 ): #{

  try: #{
    info = sg3utils.mode_sense( handle, page      = 0x1c,
                                        raw       = raw,
                                        verbosity = verbosity )

    print('>>> informational exceptions page:')

    if raw: #{
      prhex( info )
      return

    else: #}{
      print('   page       : 0x%02x' % (info['page']))
      print('   subpage    : 0x%02x' % (info['subpage']))
      print('   medium_type: 0x%02x' % (info['medium_type']))
      print('   specific   : 0x%02x' % (info['specific']))
      print('   long_lba   : %s'     % (info['long_lba']))
      print('   data       :', end=' ')
      print( info['data'] )
      return
    #}

  except Exception as ex: #}{
    print("*** mode_sense FAILED:", ex)
  #}

  print("=========================================================")
#}

def test_raw( handle, verbosity=0 ): #{
  # request size
  # cdb
  # [data]
  #
  #   fd     = scsi_pt_open_device( device_name, readonly, verbose ))
  #   ptvp   = construct_scsi_pt_obj()
  #   is_cdb = sg_is_scsi_cdb( cdb, len )
  #   if (data) {
  #   }
  #
  ###
  #  SCSI SBC-3, Table 62: LOG SENSE command
  #
  #       Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
  #      Byte |     |     |     |     |     |     |     |     |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       0   | Operation Code (4Dh)                          |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       1   | Reserved                          |Obslt| SP  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       2   | PC        | Page Code                         |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       3   | Subpage Code                                  |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       4   | Reserved                                      |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       5   | Parameter Pointer                             |
  #       6   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       7   | Allocation Length                             |
  #       8   |                                               |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #       9   | Control                                       |
  #     ------+-----+-----+-----+-----+-----+-----+-----+-----+
  #
  #                  0   1   2   3   4   5   6   7   8   9
  #                  op      pg  sub     param   len     ctl
  cmd = bytearray( b'\x4d\x00\x4d\x00\x00\x00\x00\xc0\x82\x00' )

  try: #{
    # response_max = 1024, data = bytearray()
    info = sg3utils.raw( handle, cmd, verbosity = verbosity )

    print('>>> raw response:')

    print('   status          : 0x%02x' % (info['status']))
    print('   result_category : 0x%02x' % (info['result_category']))
    print('   sense_category  : 0x%02x' % (info['sense_category']))
    print('   sense           :')
    prhex( info['sense'], indent='    ' )

    print('   result          :', end=' ')
    prhex( info['result'] )

  except Exception as ex: #}{
    print("*** raw FAILED:", ex)
  #}

  print("=========================================================")
#}

def test_sat_identify( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.sat_identify( handle, raw=raw, verbosity=verbosity )

    print('>>> sat_identify response:')

    print('   status          : 0x%02x' % (info['status']))
    print('   result_category : 0x%02x' % (info['result_category']))
    print('   sense_category  : 0x%02x' % (info['sense_category']))
    print('   sense           :')
    prhex( info['sense'], indent='      ')

    print('   smart_available : %s' % (repr(info['smart_available'])) )
    print('   smart_enabled   : %s' % (repr(info['smart_enabled'])) )

    if 'product' in info: #{
      print('   product         : %s' % (info['product']) )
      print('   revision        : %s' % (info['revision']) )
      print('   serial          : %s' % (info['serial']) )
    #}

    print('   raw             :')
    prhex( info['raw'], indent='      ')

  except Exception as ex: #}{
    print("*** sat_identify FAILED:", ex)
  #}
  print("=========================================================")
#}

def test_sat_smart_data( handle, raw=False, verbosity=0 ): #{
  try: #{
    info = sg3utils.sat_smart_data( handle, raw=raw, verbosity=verbosity )

    print('>>> sat_smart_data response:')

    print('   status          : 0x%02x' % (info['status']))
    print('   result_category : 0x%02x' % (info['result_category']))
    print('   sense_category  : 0x%02x' % (info['sense_category']))
    print('   sense           :')
    prhex( info['sense'], indent='      ')

    print('   raw             :')
    prhex( info['raw'], indent='      ')

    # Need SK_ATA_COMMAND_IDENTIFY_DEVICE to determine if SMART is
    # avaialble/enabled:
    #   libatasmart/atasmart.c
    #     disk_smart_is_available()
    #     disk_smart_is_enabled()
    #     sk_disk_identify_parse()
    #       available                 : identify[164] & 1
    #       enabled                   : identify[170] & 1
    #       serial number             : identify[ 20:20 ]
    #       firmware revision         : identify[ 46:8 ]
    #       model number              : identify[ 54:40 ]
    #
    #   libatasmart/atasmart.c
    #     sk_disk_smart_parse()
    #       offline_data_collection_status    : raw[362]
    #       self-test-execution-percent-remain: 10 * (raw[363] & 0xf)
    #       self-test-execution-status        : (raw[363] >> 4) & 0xf
    #       total-offline-data-collection-mins: int.from_bytes(raw[364:366],
    #                                                          'big')
    #       short-test-polling-minutes        : raw[372]
    #       conveyance-test-polling-minutes   : raw[374]
    #       extended-test-polling-minutes     : (
    #           raw[373] if raw[373] != 0xff
    #                    else int.from_bytes( raw[375:377], 'big' ))
    #       ...
    #
    #     disk_smart_is_conveyance_test_available()
    #     disk_smart_is_short_and_extended_test_available()
    #     disk_smart_is_start_test_available()
    #     disk_smart_is_abort_test_available()
    #       conveyance-test-available         : raw[367] & 32
    #       short-extended-test-avail         : raw[367] & 16
    #       start-test-available              : raw[367] & 1
    #       abort-test-available              : raw[367] & 41
    #
    raw     = info['raw']
    raw_len = len(raw)

    print('>>> SMART information (%d bytes):' % (raw_len) )
    if raw_len > 367: #{
      print('>>>   Offline Data Collection status: 0x%x / r:%x' %
                              (info['offline_data_collection_status'],
                               raw[362]) )
      print('>>>   Self-test execution status    : 0x%x / r:%x' %
                              (info['self_test_execution_status'], raw[363]) )

      cap = info['offline_data_collection_capability']
      print('>>>   Offline data collection cap   : 0x%x / r:%x' %
                              (cap, raw[367]) )
      print('>>>     Start Test                  : %s' %
                              (True if cap & 1 else False))
      print('>>>     Short Extended Test         : %s' %
                              (True if cap & 16 else False))
      print('>>>     Conveyance test             : %s' %
                              (True if cap & 32 else False))
      print('>>>     Abort Test                  : %s' %
                              (True if cap & 41 else False))

      cap = info['smart_capability']
      print('>>>   SMART capability              : 0x%x / r:%x %x' %
                              (cap, raw[368], raw[369]) )

      cap = info['error_logging_capability']
      print('>>>   Error logging capability      : 0x%x / r:%x' %
                              (cap, raw[370]) )

      cap = info['short_self_test_recommended_poll_time']
      print('>>>   Short self-test poll time     : %d / r:%d' %
                              (cap, raw[372]) )

      cap = info['extended_self_test_recommended_poll_time']
      print('>>>   Extended self-test poll time  : %d / r:%d' %
                              (cap, raw[373]) )

      cap = info['conveyance_self_test_recommended_poll_time']
      print('>>>   Conveyance self-test poll time: %d / r:%d' %
                              (cap, raw[374]) )

      print('>>>   Checksum                      : 0x%x / r:%x' %
                              (info['checksum'], raw[511]) )
    #}

    print('>>> SMART attributes [ %d ]:' % (len(info['attrs'])))
    #      1    2   3   4   5    6    7
    print('%4s: %6s %6s %6s %-9s %-8s %s' %
            ('id',        # 1
             'flags',     # 2
             'val',       # 3
             'wrst',      # 4
             'type',      # 5
             'updated',   # 6
             'raw') )     # 7
    for attr in info['attrs']: #{
      #      1    2      3   4   5    6    7
      print('%4d: 0x%04x %6d %6d %-9s %-8s' %
              ( attr['id'],                       # 1
                attr['flags'],                    # 2
                attr['value_current'],            # 3
                attr['value_worst'],              # 4
                ('pre-fail' if attr['prefailure'] # 5
                            else 'old-age'),
                ('always'   if attr['online']     # 6
                            else 'offline') ),
              end='')

      prhex( attr['value_raw'], no_index=True )   # 7
    #}

  except Exception as ex: #}{
    print("*** sat_smart_data FAILED:", ex)
  #}
  print("=========================================================")
#}

# Tests }
###############################################################################
# Main {
#
def main( args ): #{
  raw       = args.raw
  verbosity = args.verbosity
  dev_path  = args.target

  # Check to see if a target is specified {
  #   /dev/sda:MR:1
  #           -----
  parts  = dev_path.split(':')
  nParts = len(parts)
  if nParts < 1 or nParts > 3: #{
    print('*** Invalid target (format path[:target]): %s' %
          (dev_path), file=sys.stderr)
    return
  #}

  target   = -1
  dev_path = parts[0]
  if nParts > 1: #{
    if nParts > 2: #{
      # MUST be of the form:
      #   /dev/%dev%:MR:%target%
      if parts[1].lower() != 'mr': #{
        print('*** Unknown target class [ %s ] : %s' %
              (parts[1], dev_path), file=sys.stderr)
        return
      #}
      target = parts[2]

    else: #}{
      target = parts[1]
    #}
  #}
  # Check to see if a target is specified }

  print(">>> Open %s ..." % (dev_path))
  handle = sg3utils.open( dev_path )

  scsi = sg3utils.scsi_id( handle )
  if scsi: #{
    print('>>> %s SCSI id: %3d:%3d:%3d:%3d' %
          (dev_path, scsi['host'], scsi['channel'],
                     scsi['target'], scsi['lun']))
  #}

  if sg3utils.is_mr( handle ): #{
    orig_target = sgutils.mr_get_target( handle )

    print(">>> Is controlled by a MegaRaid controller")
    print(">>> Current target id: %d" % (orig_target))

    try: #{
      pd_list = sg3utils.mr_get_pd_list( handle )

      print("%d physical devices found:" % (len(pd_list)))
      print("     : did : edid : eidx : slot : type")

      for idex, pd in enumerate(pd_list): #{
        print("  %3d: %3d : %4d : %4d : %4d : %4d" %
              (idex, pd['device_id'],
                     pd['encl_device_id'],
                     pd['encl_index'],
                     pd['slot_number'],
                     pd['scsi_dev_type']),
              end="")

        if pd['scsi_dev_type'] == 0: #{
          # Set the target device
          sg3utils.mr_set_target( handle, pd['device_id'] )

          # Query for basic information, including serial number
          info   = sg3utils.inquiry( handle )
          serial = sg3utils.inquiry( handle, page=0x80 )
          print(" : %s : %s : %s : %s" %
                ( info['vendor'].strip(),
                  info['product'].strip(),
                  info['revision'].strip(),
                  serial.strip() ),
                end="")
        #}
        print("")
      #}

    except Exception as ex: #}{
      print("*** mr_get_pd_list() FAILED:", ex)
    #}

    if target >= 0: #{
      print('>>> Set MegaRaid target to: %d' % (target))
      sg3utils.mr_set_target( handle, target )

    elif orig_target >= 0: #}{
      print('>>> Reset MegaRaid target to: %d' % (orig_target))
      sg3utils.mr_set_target( handle, orig_target )
    #}


    print("=========================================================")
  #}

  test_inq_00( handle, raw, verbosity )
  test_inq_80( handle, raw, verbosity )
  test_inq_83( handle, raw, verbosity )
  test_inq_b1( handle, raw, verbosity )

  #test_report_luns( handle, raw, verbosity )
  test_readcap( handle, raw, verbosity )

  test_log_sense_1( handle, raw, verbosity )
  #test_log_sense_2( handle, raw, verbosity )
  test_log_sense_temperature( handle, raw, verbosity )
  test_log_sense_start_stop( handle, raw, verbosity )
  #test_log_sense_background_scan( handle, raw, verbosity )
  #test_log_sense_application_client( handle, raw, verbosity )
  #test_log_sense_cache( handle, raw, verbosity )

  #test_mode_sense( handle, raw, verbosity )
  test_mode_sense_ie( handle, raw, verbosity )

  #test_raw( handle, verbosity )

  #test_sat_identify( handle, raw, verbosity )
  #test_sat_smart_data( handle, raw, verbosity )

  sg3utils.close( handle )
#}

# Main }
###############################################################################
if __name__ == '__main__': #{
  import argparse

  parser = argparse.ArgumentParser(
              formatter_class = argparse.ArgumentDefaultsHelpFormatter )

  parser.add_argument(
    '--verbosity', '-v',
    action  = 'count',
    default = 0,
    help    = 'Increase output verbosity' )
  parser.add_argument(
    '--raw', '-r',
    action  = 'store_true',
    default = False,
    help    = 'Show raw output' )
  parser.add_argument(
    'target',
    nargs   = 1,
    default = '/dev/sda',
    help    = 'The target device' )

  args = parser.parse_args()

  args.target = args.target[0]

  main( args )
#}
