/*
 * python-sg3utils -- Python interface to sg3_utils' libsg3utils.
 *
 * Copyright (C) 2013 Red Hat, Inc.
 * Copyright (C) 1999-2011 Douglas Gilbert
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Andy Grover (agrover redhat com)
 *
 * ATA references:
 *    Information Technology AT Attachment with Packet Interface - 7
 *      Volume 1 - Register Delivered Command Set,
 *      Logical Register Set (ATA/ATAPI-7 V1)
 *
 *    http://www.t13.org/Documents/UploadedDocuments/docs2007/
 *            D1532v1r4b-AT_Attachment_with_Packet_Interface_-_7_Volume_1.pdf
 */

/* From https://python.readthedocs.io/en/stable/c-api/arg.html#strings-and-buffers
 *    For all # variants of formats (s#, y#, etc.), the type of the length
 *    argument (int or Py_ssize_t) is controlled by defining the macro
 *    PY_SSIZE_T_CLEAN before including Python.h. If the macro was defined,
 *    length is a Py_ssize_t rather than an int. This behavior will change in a
 *    future Python version to only support Py_ssize_t and drop int support. It
 *    is best to always define PY_SSIZE_T_CLEAN.
 */
#define PY_SSIZE_T_CLEAN

#include <Python.h>

#include <stdio.h>  // vsnprintf(3)
#include <errno.h>  // <errno>, errno(3)
#include <string.h> // strerror(3)
#include <stdarg.h> // va_list, v_start, va_end, <__VA_ARGS__> macro
#include <stdlib.h> // malloc/free()
#include <fcntl.h>  // open(2)/close(2)
#include <ctype.h>  // isascii(3)

#include <scsi/scsi.h>          // TYPE_DISK

#include <scsi/sg_cmds.h>
#include <scsi/sg_lib.h>
#include <scsi/sg_pr2serr.h>    // pr2ws()
#include <scsi/sg_unaligned.h>  // sg_get_unaligned_be16()

#include <scsi/sg_pt_linux.h>   // sg_pt_base.impl

#include <scsi/sg_pt.h>         // construct/destruct_scsi_pt_obj()
#include <scsi/sg_pt_mr.h>      /* sg_mr_get_scsi_id(), sg_mr_get_pd_list()
                                 * MR_PD_LIST
                                 */

#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>

//#define LOG_SENSE_PROBE   1
//#define MODE_SENSE_PROBE  1

/**
 *  An overlay of raw response data to standard inquiry.
 */
typedef struct {
  // Byte 0
  uint8_t peripheral_qualifier: 3;
  uint8_t peripheral_type:      5;

  // Byte 1
  uint8_t rmb:                  1;
  uint8_t reserved_1:           7;

  // Byte 2
  uint8_t version;

  // Byte 3
  uint8_t obsolete_3a:          2;
  uint8_t norm_aca:             1;
  uint8_t hisup:                1;
  uint8_t response_data_fmt:    4;

  // Byte 4
  uint8_t additional_len;

  // Byte 5
  uint8_t sccs:                 1;
  uint8_t acc:                  1;
  uint8_t tpgs:                 2;
  uint8_t third_party_copy:     1;
  uint8_t reserved_5:           2;
  uint8_t protect:              1;

  // Byte 6
  uint8_t obsolete_6a:          1;
  uint8_t encserv:              1;
  uint8_t vendor_specific_6:    1;
  uint8_t multip:               1;
  uint8_t obsolete_6b:          4;

  // Byte 7
  uint8_t obsolete_7:           6;
  uint8_t cmdque:               1;
  uint8_t vendor_specific_7:    1;

  // Byte 8..15
  uint8_t vendor[8];

  // Byte 16..31
  uint8_t product[16];

  // Byte 32..35
  uint8_t revision[4];

  // Byte 36..43
  uint8_t serial[8];

  // Byte 44..55
  uint8_t vendor_unique[12];

  // Bytes 56..95 (ignored)
  //uint8_t ignored[40];

} sg_inquiry_data;

/*****************************************************************************
 * Globals {
 */

// ANSI SCSI-3 Mode Pages {                                subpage
#define MODE_PAGE_VENDOR_UNIQUE                   0x00
#define MODE_PAGE_READ_WRITE_ERROR_RECOVERY       0x01  // 00
#define MODE_PAGE_DISCONNECT_RECONNECT            0x02  // 00
#define MODE_PAGE_RIGID_DISK_GEOMETRY             0x04  // 00 (obsolete)

#define MODE_PAGE_CACHING                         0x08
#define MODE_PAGE_PERIPHERAL_DEVICE               0x09  // 00 (obsolete)
#define MODE_PAGE_CONTROL_MODE                    0x0a  /* 00 Ctl
                                                         * 01 Ctl Extension
                                                         * 02 Appl Tag
                                                         * 03 Cmd Dur Lim A
                                                         * 04 Cmd Dur Lim B
                                                         * 05 IO Advice Hints
                                                         * 06 Bkgd Op Ctl
                                                         */
#define MODE_PAGE_MEDIUM_TYPES_SUPPORTED          0x0b

#define MODE_PAGE_ENCLOSURE_SERVICES_MANAGEMENT   0x14

#define MODE_PAGE_PROTOCOL_SPECIFIC_LUN           0x18
#define MODE_PAGE_PROTOCOL_SPECIFIC_PORT          0x19  /* 00 Proto Spcfc
                                                         * 01 Phy Ctl & Dscvr
                                                         * 02 Share Prt Ctl
                                                         * 03 Enh Phy Ctl
                                                         * e5 Tcvr Ctl (out)
                                                         * e6 Tcvr Ctl (in)
                                                         */
#define MODE_PAGE_POWER_CONDITION                 0x1a  /* 00 Pwr Condtion
                                                         * 01 Pwr Consuption
                                                         */
#define MODE_PAGE_CONTROL                         0x1c  /* 00 IE Ctl
                                                         * 01 Bkgd Ctl
                                                         * 02 Lgcl Blk Prov
                                                         */

#define MODE_PAGE_ALL                             0x3f  /* 00
                                                         * ff to include
                                                         *    subpages
                                                         */
// ANSI SCSI-3 Mode Pages }

// MODE_PAGE_CONTROL subpages [0x1c]
#define MODE_SUBPAGE_CONTROL_INFORMATIONAL_EXCEPTIONS 0x0
#define MODE_SUBPAGE_CONTROL_BACKGROUND               0x1
#define MODE_SUBPAGE_CONTROL_LOGICAL_BLOCK_PROVISION  0x2

// Mode page control field {
#define MPAGE_CONTROL_CURRENT     0
#define MPAGE_CONTROL_CHANGEABLE  1
#define MPAGE_CONTROL_DEFAULT     2
#define MPAGE_CONTROL_SAVED       3
// Mode page control field }

// SCSI Vital Product Data (VPD) pages {
#define SCSI_VPD_SUPPORTED_PAGES              0x00

#define SCSI_VPD_UNIT_SERIAL_NUMBER           0x80
#define SCSI_VPD_DEVICE_IDENTIFICATION        0x83
#define SCSI_VPD_EXTENDED_INQUIRY_DATA        0x86
#define SCSI_VPD_ATA_INFORMATION              0x89
#define SCSI_VPD_POWER_CONDITION              0x8a
#define SCSI_VPD_POWER_CONSUMPTION            0x8d

#define SCSI_VPD_BLOCK_LIMITS                 0xb0
#define SCSI_VPD_BLOCK_DEVICE_CHARACTERISTICS 0xb1
#define SCSI_VPD_LOGICAL_BLOCK_PROVISIONING   0xb2
// SCSI Vital Product Data (VPD) pages }

// SCSI ATA PASSTHROUGH {
#define SAT_ATA_PASS_THROUGH16      0x85
#define SAT_ATA_PASS_THROUGH16_LEN  16

// ATA commands
#define ATA_SMART                   0xb0
#define ATA_CHECK_POWER_MODE        0xe5
#define ATA_IDENTIFY_DEVICE         0xec
#define ATA_IDENTIFY_DATA_LEN       512

// ATA SMART subcommands (ATA8 7.52.1)
#define ATA_SMART_READ_DATA                 0xd0
#define ATA_SMART_READ_DATA_LEN             512

#define ATA_SMART_READ_THRESHOLDS           0xd1
#define ATA_SMART_EXECUTE_OFFLINE_IMMEDIATE 0xd4
#define ATA_SMART_READ_LOG                  0xd5
#define ATA_SMART_ENABLE_OPERATIONS         0xd8
#define ATA_SMART_DISABLE_OPERATIONS        0xd9
#define ATA_SMART_RETURN_STATUS             0xda
// SCSI ATA PASSTHROUGH }

// Bytes per LUN item
#define LUN_SIZE 8

/* The "safe" size for an initial INQURIY
 * (larger sizes may lock some SCSI devices)
 */
#define SAFE_STD_INQ_SIZE 36

// The size of the LOG SENSE response when probing for the actual response size
#define LOG_SENSE_PROBE_ALLOC_LEN   4

// The size of the MODE SENSE response when probing for the actual response size
#define MODE_SENSE_PROBE_ALLOC_LEN  8

// Max response buffer length (bytes)
#define RSP_BUF_LEN (0xc000 + 0x80)
static unsigned char  _rspBuf[RSP_BUF_LEN + 2];

// Max string compose buffer (bytes)
#define TMP_BUF_LEN 128
static char         _pyErrBuf[TMP_BUF_LEN];
static char         _errStrBuf[TMP_BUF_LEN];

static const char*    _vpd83_assoc_map[] = {
    "Addressed logical unit",
    "Target port",      /* that received request; unless SCSI ports VPD */
    "Target device that contains addressed lu",
    "Reserved [0x3]",
};

static const char*    _vpd83_set_map[] = {
    "Reserved [0x0]",
    "Binary",
    "ASCII",
    "UTF-8",
    "Reserved [0x4]", "Reserved [0x5]", "Reserved [0x6]", "Reserved [0x7]",
    "Reserved [0x8]", "Reserved [0x9]", "Reserved [0xa]", "Reserved [0xb]",
    "Reserved [0xc]", "Reserved [0xd]", "Reserved [0xe]", "Reserved [0xf]",
};

static const char*    _vpd83_desig_map[] = {
    "vendor specific [0x0]",
    "T10 vendor identification",
    "EUI-64 based",
    "NAA",
    "Relative target port",
    "Target port group",        /* spc4r09: _primary_ target port group */
    "Logical unit group",
    "MD5 logical unit identifier",
    "SCSI name string",
    "Protocol specific port identifier",  /* spc4r36 */
    "Reserved [0xa]", "Reserved [0xb]",
    "Reserved [0xc]", "Reserved [0xd]", "Reserved [0xe]", "Reserved [0xf]",
};

/* Globals }
 *****************************************************************************
 * Private helpers {
 */

#ifdef  DEBUG // {
/**
 *  Output a Python dict as key/value pairs interpreting the value as an
 *  unsigned long.
 *  @method _dumpDict
 *  @param  dict    The target dict {PyObject*};
 *
 *  @return void
 *  @private
 */
static void
_dumpDict( PyObject*  dict ) {
  PyObject*   key;
  PyObject*   val;
  Py_ssize_t  pos = 0;

  if (! dict) { return; }

  while (PyDict_Next( dict, &pos, &key, &val )) {
    pr2ws("dict:%2lu: key[ %s ], unsigned long value[ 0x%lx ]...\n",
          pos, PyUnicode_AsUTF8( key ), PyLong_AsUnsignedLong( val ) );
  }
}

/**
 *  Output a Python sequence as as set of unsigned long values.
 *  @method _dumpSeq
 *  @param  src     The target sequence {PyObject*};
 *
 *  @return void
 *  @private
 */
static void
_dumpSeq( PyObject*  src ) {
  PyObject*   seq = PySequence_Fast( src, "must be iterable" );
  int         len = PySequence_Fast_GET_SIZE( seq );;
  int         idex;

  for (idex = 0; idex < len; idex++) {
    PyObject* item  = PySequence_Fast_GET_ITEM( seq, idex );

    pr2ws("seq:%2d: ", idex);
    if (! item) {
      pr2ws("CANNOT GET\n");
      continue;
    }

    pr2ws("unsigned long value[ 0x%lx ]...\n",
          PyLong_AsUnsignedLong( item ) );
  }

  //Py_XDECREF( seq );
  Py_DECREF( seq );
}
#endif  // DEBUG }

/**
 *  Retrieve the value of the 'page' key from the given dict.
 *  @method _get_page_val
 *  @param  dict    The target dictionary {PyObject*};
 *
 *  @return The page value {unsigned long};
 *  @private
 */
static unsigned long
_get_page_val( PyObject*  dict ) {
  if (!dict)  { return 0; }

  PyObject* val = PyDict_GetItemString( dict, "page" );
  if (! val)  { return 0; }

  return PyLong_AsUnsignedLong( val );
}


/**
 *  Shorthand for generating error messages associated with the given error
 *  code.
 *  @method _errStr
 *  @param  err     The target error code (<0 errno, >0 sg3_utils error) {int};
 *
 *  @return A pointer to the error string {char*};
 *  @private
 */
static char*
_errStr( int err ) {
  if (err >= 0) {
    char* buf     = _errStrBuf;
    int   bufLen  = sizeof(_errStrBuf);

    if (! sg_exit2str( err, 0 /* verbose */, bufLen, buf )) {
      snprintf(buf, bufLen, "Unable to decode exit status %d", err);
    }

    return buf;
  }

  return strerror( (err < 0 ? -err : err) );
}

/**
 *  Generate a python error of the given type using sprintf-like capabilities
 *  to generate the string description.
 *  @method _pyErr
 *  @param  type    The python error type {PyObject*};
 *  @param  errCode The error code {int};
 *  @param  fmt     sprintf format argument (NULL if none) {const char*};
 *  @param  ...     fmt-specific arguments;
 *
 *  @return The return from snprintf() {int};
 */
static int
_pyErr( PyObject* type, int errCode, const char* fmt, ... ) {
  int         res       = 0;
  bool        useSetStr = true;
  PyObject*   errType   = NULL;
  PyObject*   errVal    = NULL;
  PyObject*   errTrace  = NULL;

  if (PyErr_Occurred()) {
    PyErr_Fetch( &errType, &errVal, &errTrace );
  }

  if (fmt) {
    va_list ap;
    va_start( ap, fmt );

      res = vsnprintf( _pyErrBuf, sizeof(_pyErrBuf), fmt, ap );

    va_end( ap );
  }

  if (errVal && (unsigned int)res < sizeof(_pyErrBuf)) {
    // Include the text of the current error
    res += snprintf( &_pyErrBuf[res], sizeof(_pyErrBuf) - res, "%s%s",
                     (fmt ? ": " : ""), PyUnicode_AsUTF8( errVal ) );
  }

  if (errCode) {
    // Create a python error with an error code and string message
    PyObject* args = Py_BuildValue( "(is)", errCode, _pyErrBuf );

    if (args) {
      PyObject* val = PyObject_Call( type, args, NULL );
      Py_DECREF( args );

      if (val) {
        PyErr_SetObject( type, val );
        Py_DECREF( val );

        useSetStr = false;
      }
    }
  }

  if (useSetStr) {
    PyErr_SetString( type, _pyErrBuf );
  }

  return res;
}

/**
 *  Given an array of 16-bit words, extract the value at the given index and
 *  add it to the provided dictionary with the given key.
 *  @method _pyAddWordAsUnsignedLong
 *  @param  dict        The target dictionary {PyObject*};
 *  @param  key         The dictionary key {char*};
 *  @param  words       The target array of 16-bit words {uint16_t*};
 *  @param  index       The target index {int};
 *
 *  @return The updated `dict` {PyObject*};
 *  @private
 */
static PyObject *
_pyAddWordAsUnsignedLong( PyObject*   dict,
                          char*       key,
                          uint16_t*   words,
                          int         index ) {

  uint32_t  val = (uint32_t)(words[index]);

  PyDict_SetItemString( dict, key, PyLong_FromUnsignedLong( val ) );

  return dict;
}

/**
 *  Given a boolean indicator, add it to the provided dictionary with the given
 *  key.
 *  @method _pyAddBool
 *  @param  dict    The target dictionary {PyObject*};
 *  @param  key     The dictionary key {char*};
 *  @param  val     The target value {bool};
 *
 *  @return The updated `dict` {PyObject*};
 *  @private
 */
static PyObject *
_pyAddBool( PyObject* dict, char* key, bool val ) {

  PyDict_SetItemString( dict, key, PyBool_FromLong( (long)val ) );

  return dict;
}

/**
 *  Given an array of words, swap the bytes of all words to create a properly
 *  ordered UTF-8 string.
 *  @method _pyStringFromWords
 *  @param  words   The target array of 16-bit words {uint16_t*};
 *  @param  nWords  The number of words to swap {int};
 *  @param  out     The output array {uint8_t*};
 *  @param  outMax  The size (in bytes) of `out` {int};
 *
 *  @return A new python string {PyObject*};
 *  @private
 */
static PyObject*
_pyStringFromWords( uint16_t* words,
                    int       nWords,
                    uint8_t*  out,
                    int       outMax ) {

  PyObject* res     = NULL;
  int       nBytes  = nWords * 2;
  int       idex;

  for (idex = 0; idex < nBytes && idex < outMax; idex += 2) {
    uint8_t*  bytes   = (uint8_t*)(words + (idex/2));

    out[idex+1] = bytes[0];
    out[idex]   = bytes[1];
  }

  res = PyUnicode_FromStringAndSize( (const char*)out, nBytes );

  /*
  pr2ws("%s(): %ld bytes res[ %s ]\n",
          __func__, PyUnicode_GetSize(res), PyUnicode_AsUTF8(res) );
  // */

  return res;
}

/**
 *  Perform a simple inquiry
 *  @method _inq_simple
 *  @param  ptp         The pass-through {struct sg_pt_base*};
 *  @param  verbosity   Verbosity level {int};
 *
 *  @return On sucess, the inquiry dict;
 *            vendor                      : (str)
 *            product                     : (str)
 *            revision                    : (str)
 *            serial                      : (str)
 *            peripheral_qualifier        : (long)
 *            peripheral_type             : (long)
 *            version                     : (long)
 *            is_removable_media          : (bool)
 *            normal_aca_support          : (bool)
 *            hierarchical_support        : (bool)
 *            response_data_format        : (long)
 *            scc_support                 : (bool)
 *            access_controls_coordinator : (bool)
 *            target_port_group_support   : (bool)
 *            third_party_copy            : (long)
 *            protect                     : (bool)
 *            enclosure_services          : (bool)
 *            multi_port                  : (bool)
 *            command_queuing             : (bool)
 *
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_inq_simple( struct sg_pt_base* ptp, int verbosity ) {
  int               pg_op     = SCSI_VPD_SUPPORTED_PAGES;
  unsigned char*    rbuf      = _rspBuf;
  int               rbufLen   = sizeof(_rspBuf);
  sg_inquiry_data*  inq     = (sg_inquiry_data*)rbuf; // overlay
  int               ret;
  int               resid;
  unsigned int      len;
  PyObject*         dict;

  memset(rbuf, 0, rbufLen);

  // Do a "safe" 36-byte inquiry to determine the full length
  rbufLen = SAFE_STD_INQ_SIZE;
  ret     = sg_ll_inquiry_pt(ptp,
                              false,      // bool   evpd
                              pg_op,      // int    pg_op
                              rbuf,       // void*  resp
                              rbufLen,    // int    mx_resp_len
                              0,          // int    timeout_secs
                              &resid,     // int*   residp
                              false,      // bool   noisy
                              verbosity); // int    verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI INQ [0x%02x] failed: %s",
            pg_op, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  len = rbuf[4] + 5;  // account for the bytes BEFORE the additional-length
  if (len > SAFE_STD_INQ_SIZE) {
    // Perform a second query for the full data
    rbufLen = len;
    memset(rbuf, 0, rbufLen);

    ret = sg_ll_inquiry_pt(ptp,
                            false,      // bool   evpd
                            0x00,       // int    pg_op
                            rbuf,       // void*  resp
                            rbufLen,    // int    mx_resp_len
                            0,          // int    timeout_secs
                            &resid,     // int*   residp
                            false,      // bool   noisy
                            verbosity); // int    verbose
    if (ret) {
      _pyErr( PyExc_IOError, ret, "SCSI INQ [0x00] (%d) failed: %s",
              rbufLen, _errStr( (errno ? errno : ret) ) );
      return NULL;
    }

    // assert( rbuf[4] + 5 == len )
    len = rbuf[4] + 5;  // account for the bytes BEFORE the additional-length
  }

  /************************************************************************
   * Create the dictionary
   *
   * Use the sg_inquriy_data structure (inq) that has been overlayed onto
   * the raw data in `rbuf`
   */
  inq  = (sg_inquiry_data*)rbuf;  // overlay
  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  PyDict_SetItemString( dict, "vendor",
                                PyUnicode_FromStringAndSize(
                                  (const char *)&inq->vendor,
                                  sizeof(inq->vendor) ));
  PyDict_SetItemString( dict, "product",
                                PyUnicode_FromStringAndSize(
                                  (const char *)&inq->product,
                                  sizeof(inq->product) ));
  PyDict_SetItemString( dict, "revision",
                                PyUnicode_FromStringAndSize(
                                  (const char *)&inq->revision,
                                  sizeof(inq->revision) ));
  if (len > 36) {
    PyDict_SetItemString( dict, "serial",
                                  PyUnicode_FromStringAndSize(
                                    (const char *)&inq->serial,
                                    sizeof(inq->serial) ));
  }

  PyDict_SetItemString( dict, "peripheral_qualifier",
                                PyLong_FromLong(inq->peripheral_qualifier));
  PyDict_SetItemString( dict, "peripheral_type",
                                PyLong_FromLong(inq->peripheral_type));
  PyDict_SetItemString( dict, "version",
                                PyLong_FromLong(inq->version));
  PyDict_SetItemString( dict, "is_removable_media",
                                PyBool_FromLong( inq->rmb ));

  PyDict_SetItemString( dict, "normal_aca_support",
                                PyBool_FromLong( inq->norm_aca ));
  PyDict_SetItemString( dict, "hierarchical_support",
                                PyBool_FromLong( inq->hisup ));
  PyDict_SetItemString( dict, "response_data_format",
                                PyLong_FromLong( inq->response_data_fmt ));

  PyDict_SetItemString( dict, "scc_support",
                                PyBool_FromLong( inq->sccs ));
  PyDict_SetItemString( dict, "access_controls_coordinator",
                                PyBool_FromLong( inq->acc ));
  PyDict_SetItemString( dict, "target_port_group_support",
                                PyLong_FromLong( inq->tpgs ));
  PyDict_SetItemString( dict, "third_party_copy",
                                PyBool_FromLong( inq->third_party_copy ));
  PyDict_SetItemString( dict, "protect",
                                PyBool_FromLong( inq->protect ));

  PyDict_SetItemString( dict, "enclosure_services",
                                PyBool_FromLong( inq->encserv ));
  PyDict_SetItemString( dict, "multi_port",
                                PyBool_FromLong( inq->multip ));

  PyDict_SetItemString( dict, "command_queuing",
                                PyBool_FromLong( inq->cmdque ));

  //if (len > 44) { vendor unique, vendor desciptors ... }

  return dict;

  /*
  return Py_BuildValue("(sssbbbbbbbbbbbbbbbbbbbbb)", inq_data.vendor,
           inq_data.product, inq_data.revision,
           inq_data.peripheral_qualifier,
           inq_data.peripheral_type,
           !!(0x80 & inq_data.byte_1),
           inq_data.version,
           ((inq_data.byte_3 >> 5) & 0x1),  // NormACA
           ((inq_data.byte_3 >> 4) & 0x1),  // HiSup
           (inq_data.byte_3 & 0xF),         // response data format
           ((inq_data.byte_5 >> 7) & 0x1),  // sccs
           ((inq_data.byte_5 >> 6) & 0x1),  // acc
           ((inq_data.byte_5 >> 5) & 0x3),  // tpgs
           ((inq_data.byte_5 >> 3) & 0x1),  // 3pc
           (inq_data.byte_5 & 0x1),         // protect
           ((inq_data.byte_6 >> 7) & 0x1),  // BQue
           ((inq_data.byte_6 >> 6) & 0x1),  // EncServ
           ((inq_data.byte_6 >> 4) & 0x1),  // MultiP
           ((inq_data.byte_6 >> 3) & 0x1),  // MChngr
           (inq_data.byte_6 & 0x1),         // Addr16
           ((inq_data.byte_7 >> 5) & 0x1),  // WBus16
           ((inq_data.byte_7 >> 4) & 0x1),  // sync
           ((inq_data.byte_7 >> 3) & 0x1),  // linked
           ((inq_data.byte_7 >> 1) & 0x1)   // CmdQue
  );
  // */
}

/**
 *  Perform an inquiry for VPD page 0x80 (serial number).
 *  @method _inq_vpd_usn
 *  @param  ptp         The pass-through {struct sg_pt_base*};
 *  @param  verbosity   Verbosity level {int};
 *
 *  @return On sucess, the serial number {str};
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_inq_vpd_usn( struct sg_pt_base* ptp, int verbosity ) {
  int             pg_op     = SCSI_VPD_UNIT_SERIAL_NUMBER;
  unsigned char*  rbuf      = _rspBuf;
  int             rbufLen   = SAFE_STD_INQ_SIZE;
  int             ret;
  int             resid;

  memset(rbuf, 0, rbufLen);

  ret = sg_ll_inquiry_pt(ptp,
                          true,       // bool   evpd
                          pg_op,      // int    pg_op
                          rbuf,       // void*  resp
                          rbufLen,    // int    mx_resp_len
                          0,          // int    timeout_secs
                          &resid,     // int*   residp
                          false,      // bool   noisy
                          verbosity); // int    verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI INQ [0x%02x] failed: %s",
            pg_op, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  /************************************************************************
   * Return just the serial number.
   *
   *   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *  Byte |     |     |     |     |     |     |     |     |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   0   | Peripherial Qual| Peripheral Device Type      |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   1   | Page Code (0x80)                              |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   2   | Reserved                                      |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   3   | Page Length (0x8 - 14)                        |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   4   | Product Serial Number                         |
   *   ... |                                               |
   *   n   |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   */
  return Py_BuildValue("s#", &rbuf[4], rbuf[3]);
}

/**
 *  Decode a VPD 0x83 descriptor.
 *  @method _vpd83_decode
 *  @param  ip      The pointer to the identifier data {const unsigned char*};
 *  @param  i_len   The size (bytes) of `ip` {int};
 *  @param  p_id    The protocol identifier {int};
 *  @param  c_set   The code set {int};
 *  @param  piv     The PIV value {int};
 *  @param  assoc   The association value {int};
 *  @param  id_type The identifier type {int};
 *
 *  @return On sucess, the decoded dict:
 *              protocol_id (str);
 *              char_set (str);
 *              association (str);
 *              id_type (str);
 *              ip (bytearray); # Identifier value
 *
 *          On failure, NULL;
 *  @private
 */
static PyObject *
_vpd83_decode(  const unsigned char*  ip,
                int                   i_len,
                int                   p_id,
                int                   c_set,
                int                   piv,
                int                   assoc,
                int                   id_type) {
  PyObject *dict;

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  PyDict_SetItemString(dict, "association",
                            PyUnicode_FromString(_vpd83_assoc_map[assoc]));
  PyDict_SetItemString(dict, "id_type",
                            PyUnicode_FromString(_vpd83_desig_map[id_type]));
  PyDict_SetItemString(dict, "char_set",
                            PyUnicode_FromString(_vpd83_set_map[c_set]));

  if (piv && ((1 == assoc) || (2 == assoc))) {
    char proto_str[64];

    sg_get_trans_proto_str(p_id, sizeof(proto_str), proto_str);

    PyDict_SetItemString(dict, "protocol_id", PyUnicode_FromString(proto_str));

  } else {
    PyDict_SetItemString(dict, "protocol_id", PyUnicode_FromString("N/A"));

  }

  PyDict_SetItemString(dict, "id_value",
                        PyByteArray_FromStringAndSize((const char *)ip, i_len));

  return dict;
}

/**
 *  Perform an inquiry for VPD page 0x83 (device identification).
 *  @method _inq_vpd_di
 *  @param  ptp         The pass-through {struct sg_pt_base*};
 *  @param  verbosity   Verbosity level {int};
 *
 *  @return On sucess, the inquiry results;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_inq_vpd_di( struct sg_pt_base* ptp, int verbosity ) {
  int                   pg_op     = SCSI_VPD_DEVICE_IDENTIFICATION;
  unsigned char*        rbuf      = _rspBuf;
  unsigned int          rbufLen   = sizeof(_rspBuf);
  int                   ret;
  int                   resid;
  int                   off;
  int                   u;
  unsigned int          data_len;
  unsigned char*        id_buff;
  PyObject*             list;

  memset(rbuf, 0, rbufLen);

  ret = sg_ll_inquiry_pt(ptp,
                          true,       // bool   evpd
                          pg_op,      // int    pg_op
                          rbuf,       // void*  resp
                          rbufLen,    // int    mx_resp_len
                          0,          // int    timeout_secs
                          &resid,     // int*   residp
                          false,      // bool   noisy
                          verbosity); // int    verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI INQ [0x%02x] failed: %s",
            pg_op, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  data_len = sg_get_unaligned_be16( rbuf + 2 ) + 4;

  /************************************************************************
   * SCSI SBC-3, Table 459: Device identification VPD page
   *
   *   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *  Byte |     |     |     |     |     |     |     |     |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   0   | Peripherial Qual| Peripheral Device Type      |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   1   | Page Code (0x83)                              |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   2   | Page Length (n-3)                             |
   *   3   |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   4   | Identification Descriptor (first)             |
   *   ... |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   ... | Identification Descriptor (last)              |
   *   n   |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   */
  //data_len = ((rbuf[2] << 8) + rbuf[3]) + 4;
  if (data_len > rbufLen) {
    _pyErr( PyExc_IOError, errno, "Return data truncated to %u of %u",
            rbufLen, data_len );
    data_len = rbufLen;
  }

  list = PyList_New(0);
  if (!list) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new list" );
    return NULL;
  }

  off     = -1;
  id_buff = rbuf + 4;

  while ((u = sg_vpd_dev_id_iter(id_buff, (data_len - 4), &off,
                                 -1, -1, -1)) == 0) {
    /* SCSI SBC-3, Table 460: Identification Descriptor
     *
     *   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
     *  Byte |     |     |     |     |     |     |     |     |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     *   0   | Protocol Identifier   | Code Set              |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     *   1   | PIV | RSVD|Association| Identifier Type       |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     *   2   | Reserved                                      |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     *   3   | Identifier Length (n-3)                       |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     *   4   | Identifier                                    |
     *   ... |                                               |
     *   n   |                                               |
     *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
     */
    PyObject*             desc  = NULL;
    const unsigned char*  ucp   = id_buff + off;
    int                   i_len = ucp[3];
    int                   p_id;
    int                   c_set;
    int                   assoc;
    int                   piv;
    int                   id_type;

    i_len = ucp[3];
    if ((unsigned int)(off + i_len + 4) > data_len) {
      /*
      pr2ws("=== VPD page error: 0x%x designator length [ %d ] "
                    "longer than remaining response length [ %u ]\n",
                off, off + i_len + 4, data_len);
      // */
      break;

      /*
      _pyErr(PyExc_IOError, errno,
                "VPD page error: 0x%x designator length [ %d ] "
                "longer than remaining response length [ %u ]",
                            off, off + i_len + 4, data_len);
      goto py_err;
      // */
    }

    p_id    = ((ucp[0] >> 4) & 0xf);
    c_set   =  (ucp[0] & 0xf);
    piv     = ((ucp[1] & 0x80) ? 1 : 0);
    assoc   = ((ucp[1] >> 4) & 0x3);
    id_type =  (ucp[1] & 0xf);

    desc    = _vpd83_decode( ucp + 4, i_len,
                             p_id, c_set, piv, assoc, id_type );
    if (!desc) {
      // _vpd83_decode() should have set the error
      goto py_err;
    }

    if (PyList_Append(list, desc)) {
      _pyErr( PyExc_IOError, errno, "Failed to append list item" );
      Py_XDECREF(desc);
      goto py_err;
    }
  }

  return list;

py_err:
  Py_XDECREF(list);

  return NULL;
}

/**
 *  Perform an inquiry for VPD page 0xb1 (block device characteristics).
 *  @method _inq_vpd_bdc
 *  @param  ptp         The pass-through {struct sg_pt_base*};
 *  @param  verbosity   Verbosity level {int};
 *
 *  @return On sucess, the inquiry dict;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_inq_vpd_bdc( struct sg_pt_base* ptp, int verbosity ) {
  int                   pg_op     = SCSI_VPD_BLOCK_DEVICE_CHARACTERISTICS;
  unsigned char*        rbuf      = _rspBuf;
  unsigned int          rbufLen   = sizeof(_rspBuf);
  int                   ret;
  int                   resid;
  unsigned int          data_len;
  unsigned int          rpm;
  unsigned char         byte_val;
  const char*           product_type;
  const char*           form_factor;
  char                  str_buf[32] = {0};
  int                   str_bufLen  = sizeof(str_buf);
  PyObject*             dict;

  memset(rbuf, 0, rbufLen);

  ret = sg_ll_inquiry_pt(ptp,
                          true,       // bool   evpd
                          pg_op,      // int    pg_op
                          rbuf,       // void*  resp
                          rbufLen,    // int    mx_resp_len
                          0,          // int    timeout_secs
                          &resid,     // int*   residp
                          false,      // bool   noisy
                          verbosity); // int    verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI INQ [0x%02x] failed: %s",
            pg_op, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  data_len = sg_get_unaligned_be16( rbuf + 2 ) + 4;

  /************************************************************************
   * SCSI SBC-3, Table 439: Block Device Characteristics VPD page
   *
   *   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *  Byte |     |     |     |     |     |     |     |     |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   0   | Peripherial Qual| Peripheral Device Type      |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   1   | Page Code (0xb1)                              |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   2   | Page Length (0x03c)                           |
   *   3   |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   4   | Medium Rotation Rate                          |
   *   5   |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   6   | Product Type                                  |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   7   | WABEREQ   | WACEREQ   | Nominal Form Factor   |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   8   | Reserved  | ZONED     | RSVD| BOCS| FUAB|VBULS|
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   9   | Reserved                                      |
   *   ... |                                               |
   *   63  |                                               |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   */
  if (data_len > rbufLen) {
    _pyErr( PyExc_IOError, errno, "Return data truncated to %u of %u",
            rbufLen, data_len );
    data_len = rbufLen;
  }

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  /****************************************************************************
   * SCSI SBC-3, Table 440: Medium Rotation Rate field
   *  0x0000        : Not reported
   *  0x0001        : Non-rotating medium (e.g. solid state)
   *  0x0002 - 0400 : Reserved
   *  0x0401 - fffe : Nominal medium rotation rate in revolutions per minute
   *                  (e.g.  7200 rpm = 0x1c20,
   *                        10000 rpm = 0x2710,
   *                        15000 rpm = 0x3a98)
   *  0xffff        : Reserved
   */
  rpm = sg_get_unaligned_be16( rbuf + 4 );
  if (rpm == 0) {
    PyDict_SetItemString(dict, "rpm",
                          PyUnicode_FromString("not-reported"));

  } else if (rpm == 1) {
    PyDict_SetItemString(dict, "rpm",
                          PyUnicode_FromString("non-rotating"));

  } else if (rpm >= 0x0401 && rpm <= 0xfffe) {
    PyDict_SetItemString(dict, "rpm", PyLong_FromLong( rpm ) );

  } else {
    snprintf(str_buf, str_bufLen, "reserved[0x%x]", rpm);

    PyDict_SetItemString(dict, "rpm",
                          PyUnicode_FromString( str_buf ));
  }

  /****************************************************************************
   * SCSI SBC-3, Table 441: Product type field
   *  0x00      : Not indicated
   *  0x01      : CFast
   *  0x02      : CompactFlash            (CF)
   *  0x03      : Memory Stick            (MS)
   *  0x04      : MultiMediaCard          (eMMC)
   *  0x05      : Secure Digital Card     (SD Card)
   *  0x06      : XQD
   *  0x07      : Universal Flash Storage (UFS)
   *  0x08 - ef : Reserved
   *  0xf0 - ff : Vendor specific
   */
  byte_val = rbuf[ 6 ];

  switch( byte_val ) {
    case 0x00:  product_type = "not indicated";           break;
    case 0x01:  product_type = "CFast";                   break;
    case 0x02:  product_type = "CompactFlash";            break;
    case 0x03:  product_type = "Memory Stick";            break;
    case 0x04:  product_type = "MultiMediaCard";          break;
    case 0x05:  product_type = "Secure Digital Card";     break;
    case 0x06:  product_type = "XQD";                     break;
    case 0x07:  product_type = "Universal Flash Storage"; break;
    default:
      product_type = str_buf;
      if (byte_val < 0xf0) {
        snprintf(str_buf, str_bufLen, "reserved[0x%x]", byte_val);
        product_type = "reserved";
      } else {
        snprintf(str_buf, str_bufLen, "vendor-specific[0x%x]", byte_val);
      }
      break;
  }
  PyDict_SetItemString(dict, "product_type",
                        PyUnicode_FromString( product_type ));

  /* WABEREQ            : (rbuf[7] >> 6) & 0x01
   * WACEREQ            : (rbuf[7] >> 4) & 0x01
   */

  /****************************************************************************
   * SCSI SBC-3, Table 444: Nominal Form Factor Field
   *    0x0     : Not reported
   *    0x1     : 5.25  inch
   *    0x2     : 3.5   inch
   *    0x3     : 2.5   inch
   *    0x4     : 1.8   inch
   *    0x5     : < 1.8 inch
   *    0x6 - f : Reserved
   */
  byte_val = rbuf[7] & 0x0f;

  switch( byte_val ) {
    case 0x0: form_factor = "not reported"; break;
    case 0x1: form_factor = "5.25 inch";    break;
    case 0x2: form_factor = "3.5 inch";     break;
    case 0x3: form_factor = "2.5 inch";     break;
    case 0x4: form_factor = "1.8 inch";     break;
    case 0x5: form_factor = "< 1.8 inch";   break;
    default:
      snprintf(str_buf, str_bufLen, "reserved[0x%x]", byte_val);
      form_factor = str_buf;
      break;
  }
  PyDict_SetItemString(dict, "form_factor",
                        PyUnicode_FromString( form_factor ));

  /****************************************************************************
   * ZONED              : (rbuf[8] >> 4) & 0x03
   *
   * SCSI SBC-3, Table 445: ZONED field
   *  00b   : Not reported
   *  01b   : Device server implements the host aware zoned block device
   *          capabilities
   *  10b   : Device server implements device managed zoned block device
   *          capabilities
   *  11b   : Reserved
   */

  /* BOCS               : (rbuf[8] >> 2) & 0x01
   * FUAB               : (rbuf[8] >> 1) & 0x01
   * VBULS              : (rbuf[8]       & 0x01)
   */

  return dict;
}

/**
 *  Perform an inquiry for the specified VPD page.
 *  @method _inq_raw
 *  @param  ptp         The pass-through {struct sg_pt_base*};
 *  @param  page        The target VPD page {int};
 *  @param  verbosity   Verbosity level {int};
 *
 *  @return On sucess, the bytearray of raw data;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_inq_raw( struct sg_pt_base* ptp, int page, int verbosity ) {
  unsigned char*        rbuf      = _rspBuf;
  unsigned int          rbufLen   = sizeof(_rspBuf);
  int                   ret;
  int                   resid;
  unsigned int          data_len;

  memset(rbuf, 0, rbufLen);

  ret = sg_ll_inquiry_pt(ptp,
                          true,       // bool   evpd
                          page,       // int    pg_op
                          rbuf,       // void*  resp
                          rbufLen,    // int    mx_resp_len
                          0,          // int    timeout_secs
                          &resid,     // int*   residp
                          false,      // bool   noisy
                          verbosity); // int    verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI INQ [0x%02x] failed: %s",
            page, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  data_len = sg_get_unaligned_be16( rbuf + 2 ) + 4;

  // Do NOT interpret the data, rather return a raw bytearray
  return PyByteArray_FromStringAndSize((const char *)rbuf, data_len);
}

/**
 *  Given a log sense data, extract all log parameters.
 *  @method _extract_log_params
 *  @param  page        The target log page {int};
 *  @param  subpage     The target log sub page {int};
 *  @param  rbuf        A pointer to the first parameter {unsigned char*};
 *  @param  rbufLen     The length (bytes) of `rbuf` {int};
 *
 *  For page/subpage 0x00,00 (supported log pages), parameters are single byte
 *  entries, one per supported page;
 *    => [ {page: (int)}, ... ]
 *
 *  For page/subPages 0x00,FF (supported log pages and subpages), parameters
 *  are two byte entries, one per supported page/subpage;
 *    => [ {page    : (int),
 *          subpage: (int),
 *         },
 *         ...
 *       ]
 *
 *         Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
 *        Byte |     |     |     |     |     |     |     |     |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         0   | Reserved  | Page Code                         |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         1   | Subpage Code                                  |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *
 *  For the remainder of page/subpage:
 *    => [ {code                        : (int),
 *          offset                      : (int),
 *          disable_update              : (bool),
 *          target_save_disable         : (bool),
 *          enable_threshold_comparison : (bool),
 *
 *          threshold_met_criteria      : (int),
 *            0x00  every update of the cumulative value;
 *            0x01  Cumulative value  equal to      threshold value;
 *            0x02  Cumulative value  not equal to  threshold value;
 *            0x03  Cumulative value  greater than  threshold value;
 *
 *          format_and_linking          : (int),
 *            0x00  Bounded data counter
 *            0x01  ASCII format list
 *            0x02  Bounded data counter or unbounded data counter
 *            0x03  Binary format list
 *
 *          value_raw                   : (bytearray),
 *          value                       : (str | int | bytearray),
 *         },
 *         ...
 *       ]
 *
 *    These should have a form following SCSI SBC-3, Table 238: Log parameter
 *
 *         Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
 *        Byte |     |     |     |     |     |     |     |     |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         0   | Parameter Code                                |
 *         1   |                                               |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         2   | Parameter control byte                        |
 *             |-----+-----+-----+-----+-----+-----+-----+-----|
 *             | DU  |Obslt| TSD | ETC | TMC       | Fmt/Link  |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         3   | Parameter Length (n-3)                        |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *         4   | Parameter Value                               |
 *        ...  |                                               |
 *         n   |                                               |
 *       ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *
 *     Disable Update (DU):
 *       0   the device will update the log parameter value to reflect all
 *           events that should be noted by that parameter;
 *       1   the device will NOT update the log parameter value except in
 *           response to a LOG SELECT command;
 *
 *     Target Save Disable (TSD):
 *       0   the log parameter is saved at vendor specific intervals;
 *       1   the log parameter is either nto implicitly saved or saving has
 *           been disabled;
 *
 *     Enable Threshold Comparison (ETC):
 *       0   a comparison is not performed;
 *       1   a comparison to the threshold value is performed whenever the
 *           cumulative value is updated;
 *
 *     Threshold Met Criteria (TMC):
 *       Defines the basis for comparison of the cumulative and threshold
 *       values:
 *
 *         00b   every update of the cumulative value;
 *         01b   Cumulative value  equal to      threshold value;
 *         10b   Cumulative value  not equal to  threshold value;
 *         11b   Cumulative value  greater than  threshold value;
 *
 *     Format and Linking (Fmt/Link):
 *       00b   Bounded data counter
 *       01b   ASCII format list
 *       10b   Bounded data counter or unbounded data counter
 *       11b   Binary format list
 *
 *  @return On sucess, a list of extracted parameters;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject*
_extract_log_params(  int             page,
                      int             subpage,
                      unsigned char*  rbuf,
                      int             rbufLen ) {

  PyObject* list  = PyList_New( 0 );
  PyObject* dict  = NULL;
  int       idex;

  if (!list) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new list" );
    goto py_err;
  }

  if (subpage == 0xff) {
    // Supported log page/subpage list
    for (idex = 0; idex < rbufLen; idex += 2) {
      int page_code = rbuf[idex];
      int sub_code  = rbuf[idex+1];

      dict = PyDict_New();
      if (! dict) {
        _pyErr( PyExc_IOError, errno,
                "Failed to create a dict for page/subpage" );
        goto py_err;
      }

      PyDict_SetItemString( dict, "page",
                                  PyLong_FromLong( page_code ) );
      PyDict_SetItemString( dict, "subpage",
                                  PyLong_FromLong( sub_code ) );

      if (PyList_Append(list, dict)) {
        _pyErr( PyExc_IOError, errno, "Failed to append page/subpage" );

        goto py_err;
      }

      dict = NULL;
    }

  } else if (page == 0) {
    // Supported log page list
    for (idex = 0; idex < rbufLen; idex++) {
      int page_code = rbuf[idex];

      dict = PyDict_New();
      if (! dict) {
        _pyErr( PyExc_IOError, errno, "Failed to create a dict for page" );
        goto py_err;
      }

      PyDict_SetItemString( dict, "page",
                                  PyLong_FromLong( page_code ) );

      if (PyList_Append(list, dict)) {
        _pyErr( PyExc_IOError, errno, "Failed to append page" );

        goto py_err;
      }

      dict = NULL;
    }

  } else {
    // Standard parameters
    int param_len = 0;

    for (idex = 0; idex < rbufLen; idex += (param_len + 4) ) {
      int             param_code    = sg_get_unaligned_be16( rbuf + idex );
      bool            du            = !!(rbuf[idex+2] & 0x80);  // bit 7
      bool            tsd           = !!(rbuf[idex+2] & 0x20);  // bit 5
      bool            etc           = !!(rbuf[idex+2] & 0x10);  // bit 4
      int             tmc           =    rbuf[idex+2] & 0x0C;   // bits 2,3
      int             fmt           =    rbuf[idex+2] & 0x03;   // bits 0,1
      unsigned char*  value         =   &rbuf[idex+4];
      PyObject*       valObj        = NULL;

      param_len = rbuf[idex+3];

      dict = PyDict_New();
      if (! dict) {
        _pyErr( PyExc_IOError, errno,
                "Failed to create a dict for parameter" );
        goto py_err;
      }

      PyDict_SetItemString( dict, "code",
                                  PyLong_FromLong( param_code ) );
      PyDict_SetItemString( dict, "offset",
                                  PyLong_FromLong( idex ) );
      PyDict_SetItemString( dict, "disable_update",
                                  PyBool_FromLong( du ) );
      PyDict_SetItemString( dict, "target_save_disable",
                                  PyBool_FromLong( tsd ) );
      PyDict_SetItemString( dict, "enable_threshold_comparison",
                                  PyBool_FromLong( etc ) );
      PyDict_SetItemString( dict, "threshold_met_criteria",
                                  PyLong_FromLong( tmc ) );
      PyDict_SetItemString( dict, "format_and_linking",
                                  PyLong_FromLong( fmt ) );

      PyDict_SetItemString( dict, "value_raw",
                                  PyByteArray_FromStringAndSize(
                                      (const char *)value, param_len) );

      // Convert the value according to the parameter format
      switch( fmt ) {
        case 0x0:   // Bounded data counter (fall-through)
        case 0x2: { // Bounded or unbounded data counter (MSB first)
          /* param_len:
           *  1:   8-bit : uint8_t
           *  2:  16-bit : uint16_t
           *  4:  32-bit : uint32_t
           *  8:  64-bit : uint65_t
           */
          uint64_t  intVal  = 0;
          int       jdex;

          for (jdex = 0; jdex < param_len; jdex++) {
            intVal = (intVal << 8) | value[jdex];
          }

          valObj = PyLong_FromUnsignedLongLong( intVal );

        } break;


        case 0x1: { // ASCII format list
          /* If all bytes are ASCII, convert this to a string, otherwise
           * treat it as a raw byte array.
           *
           * :XXX: If we do not do this test and `value` contains an
           *       "invalid" set of bytes, PyUnicode_FromStringAndSize()
           *       will cause a SEGFAULT and PyUnicode_DecodeASCII() will
           *       cause a different sort of obscure error in later code
           *       that seems unrelated...
           *
           *       e.g. [ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05,
           *              0x9f, 0x00, 0x20, 0x00, 0x00, 0x00, 0xc9, 0x00,
           *              0x66, 0x00, 0x01, 0x3f, 0x00, 0x00, 0x00, 0x00,
           *              0x00, 0x00, 0x00, 0x05, 0xf0, 0x00, 0x20, 0x00,
           *              0x00, 0x01, 0x49, 0x00, 0x76, 0x00, 0x03, 0x00,
           *              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
           *              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
           *              0x00, 0x00, 0x00, 0x00, 0x04, 0x02, 0x94 ]
           */
          bool  isAscii = true;
          int   jdex;
          for (jdex = 0; jdex < param_len && isAscii; jdex++) {
            isAscii = isascii( value[jdex] );
          }

          if (isAscii) {
            // All bytes are ASCII -- convert `value` to a string
            valObj = PyUnicode_DecodeASCII(
                                      (const char *)value, param_len, NULL );
            /*
            valObj = PyUnicode_FromStringAndSize(
                                      (const char *)value, param_len );
            // */
            if (valObj) {
              break;
            }
          }

          /* NOT yet converted -- `value` will be converted to a raw byte array
          pr2ws("%s(): Non-ascii values in param_code[ 0x%04x ], "
                        "param_len[ %d ], fmt[ %d ]:",
                __func__, param_code, param_len, fmt);
          for (jdex = 0; jdex < param_len; jdex++) {
            if (jdex % 16 == 0) {
              pr2ws("\n%s():   ", __func__);
            }

            pr2ws("%02x ", value[jdex]);
          }
          pr2ws("\n");
          // */

        } break;

        case 0x3: // Binary format list (converted to a raw bytearray)
          break;
      }

      if (valObj == NULL) {
        // No conversion -- treat as raw bytes
        valObj = PyByteArray_FromStringAndSize(
                                    (const char *)value, param_len );
      }

      // Set the "value"
      PyDict_SetItemString( dict, "value", valObj );

      if (PyList_Append(list, dict)) {
        _pyErr( PyExc_IOError, errno, "Failed to append parameter" );

        goto py_err;
      }

      dict = NULL;
    }
  }

  return list;

py_err:
  if (dict) { Py_XDECREF(dict); }
  if (list) { Py_XDECREF(list); }

  return NULL;
}

/**
 *  Generate a python dictionary from the data returned from a LOG SENSE.
 *  @method _parse_log_sense
 *  @param  rbuf      A pointer to the LOG SENSE data {unsigned char*};
 *  @param  rbufLen   The length (bytes) of `rbuf` {int};
 *
 *  SCSI SBC-3, Table 236: Log page format
 *
 *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
 *     Byte |     |     |     |     |     |     |     |     |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *      0   | DS  | SPF | Page Code                         |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *      1   | Subpage Code                                  |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *      2   | Page Length (n-3)                             |
 *      3   |                                               |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *          | Log parameters                                |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *      4   | Log parameter (1, length x)                   |
 *     ...  |                                               |
 *     x+3  |                                               |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *     ...  |                                               |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *    n-y+1 | Log parameter (n, length y)                   |
 *     ...  |                                               |
 *      n   |                                               |
 *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
 *
 *  Disable Save (DS):
 *    0   save log parameter if SP bit is set to 1
 *    1   do NOT save the log parameter;
 *
 *  Subpage Format (SPF):
 *    0   the Subpage Code field will contain 00h
 *    1   the Subpage Code field will contain a value between 01h and FFh
 *
 *  Page Code:
 *    The page code being transferred;
 *
 *  Subpage Code:
 *    The subpage code being transferred;
 *
 *  Page Length:
 *    The length, in bytes, of the following log parameters;
 *
 *
 *  On success, the returned dict will have the form:
 *      { page          : (int),
 *        subpage       : (int),
 *        disable_save  : (bool),
 *        subpage_format: (bool),
 *        params        : [
 *          ###
 *          # page/subpage 0x00,00 (supported log pages), there will be an
 *          # entry per supported page:
 *          #   {page: (int)}, ...
 *          ###
 *          # page/subPages 0x00,FF (supported log pages and subpages),
 *          # there will be an entry per supported page/subpage:
 *          #   {page: (int), subpage: (int)}, ...
 *          ###
 *          # All other page/subPages will have an entry per parsed parameter
 *          # code:
 *          #   {code                        : (int),
 *          #    offset                      : (int),
 *          #    disable_update              : (bool),
 *          #    target_save_disable         : (bool),
 *          #    enable_threshold_comparison : (bool),
 *          #    threshold_met_criteria      : (int),
 *          #       0x00  every update of the cumulative value;
 *          #       0x01  Cumulative value equal to     threshold value;
 *          #       0x02  Cumulative value not equal to threshold value;
 *          #       0x03  Cumulative value greater than threshold value;
 *          #    format_and_linking          : (int),
 *          #       0x00  Bounded data counter
 *          #       0x01  ASCII format list
 *          #       0x02  Bounded data counter or unbounded data counter
 *          #       0x03  Binary format list
 *          #    value_raw                   : (bytearray),
 *          #    value                       : (str | int | bytearray),
 *          #   }, ...
 *        ]
 *      }
 *
 *  @return On sucess, a dict representing the extracted LOG SENSE data;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject*
_parse_log_sense( unsigned char*  rbuf,
                  int             rbufLen ) {

  PyObject*     dict      = NULL;
  PyObject*     list      = NULL;
  unsigned int  data_len  = 0;
  int           page_len  = 0;
  bool          ds;
  bool          spf;
  int           page;
  int           subpage;

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    goto py_err;
  }

  data_len = sg_get_unaligned_be16( rbuf + 2 ) + 4;
  page_len = data_len - 4;
  if (data_len > (unsigned int)rbufLen) {
    // Only fetched rbufLen of page_len + 4 bytes
    page_len = rbufLen - 4;
  }

  ds      = !!(rbuf[0] & 0x80);
  spf     = !!(rbuf[0] & 0x40);
  page    =    rbuf[0] & 0x3F;
  subpage = (spf ? rbuf[1] : 0);

  PyDict_SetItemString( dict, "disable_save",   PyBool_FromLong( ds ));
  PyDict_SetItemString( dict, "subpage_format", PyBool_FromLong( spf ));
  PyDict_SetItemString( dict, "page",           PyLong_FromLong( page ));
  PyDict_SetItemString( dict, "subpage",        PyLong_FromLong( subpage ));

  list = _extract_log_params( page, subpage, rbuf + 4, page_len );
  if (list == NULL) {
    // _extract_log_params() should have called _pyErr()
    goto py_err;
  }

  // Finally, assign the parameter list
  PyDict_SetItemString( dict, "params", list);

  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

  return NULL;
}

/**
 *  Perform a simple inquiry
 *  @method _mode_pc_ie( rbuf, data_len );
 *  @param  buf     The beginning of the parameters area within the retrieved
 *                  sense buffer {unsigned char*};
 *  @param  bufLen  The length in bytes of `buf` {int};
 *
 *  @return On sucess, the parsed dict;
 *            performance               : (bool)
 *            enable_background_function: (bool)
 *            enable_warning            : (bool)
 *            disable_exception_control : (bool)
 *            test                      : (bool)
 *            enable_background_error   : (bool)
 *            log_error                 : (bool)
 *            method_of_reporting_ie    : (int)
 *              0 : no reporting
 *              1 : async reporting (obsolete)
 *              2 : generate unit attention
 *              3 : conditionally generate recovered error
 *              4 : unconditionally generate recovered error
 *              5 : generate no sense
 *              6 : only report informational exception condition on request
 *            interval_timer            : (int) (100 ms increments)
 *            report_count              : (int)
 *
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject *
_mode_pc_ie( unsigned char* buf, int bufLen ) {
  PyObject*     dict            = NULL;
  unsigned int  interval_timer  = 0;
  unsigned int  report_count    = 0;

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  /* SCSI SBC-3, Table 390 Information Exceptions Control mode page (params)
   *
   *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *     Byte |     |     |     |     |     |     |     |     |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0   | PERF|RSVRD| EBF |EWASC|DEXCP| TEST|EBERR|LGERR|
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      1   | Reserved              | MRIE                  |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      2   | Interval Timer (32-bit)                       |
   *     ...  |                                               |
   *      5   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      6   | Report Count (32-bit)                         |
   *     ...  |                                               |
   *      9   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *    Performance (PERF)
   *    Enable Background Function (EBF)
   *    Enable Warning (EWASC)
   *    Disable Exception Control (DEXCP)
   *    Test (TEST)
   *    Enable Background Error (EBERR)
   *    Log Error (LGERR)
   *    Method of Reporting Informational Exceptions (MRIE)
   *      0 : no reporting
   *      1 : async reporting (obsolete)
   *      2 : generate unit attention
   *      3 : conditionally generate recovered error
   *      4 : unconditionally generate recovered error
   *      5 : generate no sense
   *      6 : only report informational exception condition on request
   *
   *    Interval Timer:
   *      A period in 100 millisecond increments that the device server shall
   *      use for reporting that an informational exception condition has
   *      occurred;
   *
   *    Report Count:
   *      The maximum number of times the device server may report an
   *      information exception condition;
   */
  interval_timer = sg_get_unaligned_be32( buf + 2 );
  report_count   = sg_get_unaligned_be32( buf + 6 );

  PyDict_SetItemString( dict, "performance",
                                PyBool_FromLong( !!(buf[0] & 0x80) ) );

  PyDict_SetItemString( dict, "enable_background_function",
                                PyBool_FromLong( !!(buf[0] & 0x20) ) );
  PyDict_SetItemString( dict, "enable_warning",
                                PyBool_FromLong( !!(buf[0] & 0x10) ) );

  PyDict_SetItemString( dict, "disable_exception_control",
                                PyBool_FromLong( !!(buf[0] & 0x08) ) );
  PyDict_SetItemString( dict, "test",
                                PyBool_FromLong( !!(buf[0] & 0x04) ) );
  PyDict_SetItemString( dict, "enable_background_error",
                                PyBool_FromLong( !!(buf[0] & 0x02) ) );
  PyDict_SetItemString( dict, "log_error",
                                PyBool_FromLong( !!(buf[0] & 0x01) ) );

  PyDict_SetItemString( dict, "method_of_reporting_ie",
                                PyLong_FromLong(    buf[1] & 0x0f ) );

  PyDict_SetItemString( dict, "interval_timer",
                                PyLong_FromLong( interval_timer ) );
  PyDict_SetItemString( dict, "report_count",
                                PyLong_FromLong( report_count ) );

  return dict;
}

/**
 *  Perform an SAT ATA pass-through.
 *  @method _sat_ata_pt
 *  @param  ptp               The target device {struct sg_pt_base*};
 *  @param  do_chk_cond       Read register(s) back? {bool};
 *  @param  do_extend         Set the extend bit? {bool};
 *  @param  protocol          The ATA pass-through protocol {int};
 *                              3 : No data
 *                              4 : PIO data-in
 *                              6 : DMA           ATA_READ_LOG_DMA_EXT
 *  @param  from_dev          From (true) or to (false) the device? {bool};
 *  @param  t_len             The length {int};
 *  @param  t_len_in_blocks   Is `t_len` in blocks (true) or bytes (false)?
 *                            {bool};
 *
 *  @return A new dict representing the results {PyObject*};
 *  @private
 */
static PyObject*
_sat_ata_pt(  struct sg_pt_base*  ptp,
              bool                do_chk_cond,
              bool                do_extend,
              int                 protocol,
              bool                from_dev,
              int                 t_len,
              bool                t_len_in_blocks,
              uint8_t*            ata_cmd,
              int                 ata_cmd_len,
              uint8_t*            rbuf,
              int                 rbuf_len,
              int*                ret_len,
              bool                raw,
              int                 verbosity) {

  uint8_t             cdb[ SAT_ATA_PASS_THROUGH16_LEN ] = { 0 };
  int                 cdb_len       = sizeof(cdb);
  uint8_t             sense_buf[32] = {0};
  int                 sense_len     = sizeof(sense_buf);
  int                 ret           = 0;
  PyObject*           dict          = NULL;
  int                 status        = 0;
  int                 res_cat       = 0;
  int                 sense_cat     = 0;
  char                str[128]      = {0};
  const int           str_len       = sizeof(str);

  // Establish the command / CDB
  cdb[0]  = SAT_ATA_PASS_THROUGH16;
  cdb[1]  = (protocol << 1) | do_extend;
  cdb[2]  = (do_chk_cond     << 5) |
            (from_dev        << 3) |
            (t_len_in_blocks << 2) |
             t_len;

  // Copy in the ATA command payload
  memcpy( cdb+3, ata_cmd, ata_cmd_len );

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "%d byte CDB:\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            cdb_len);

    hex2stdout( cdb, cdb_len, true /* no_ascii */ );
  }

  // Ensure `ptp` is ready for re-uset
  clear_scsi_pt_obj( ptp );

  set_scsi_pt_cdb( ptp, cdb, cdb_len );
  set_scsi_pt_data_in( ptp, rbuf, rbuf_len );

  // Initialize the sense buffer
  set_scsi_pt_sense( ptp, sense_buf, sizeof(sense_buf) );

  // Send the command
  ret = do_scsi_pt(ptp,
                   -1,          // int fd
                   0,           // int timeout_secs
                   verbosity);  // int verbose
  if (verbosity > 2) {
    pr2ws("%s(): SCSI ATA PT Return code: %d\n", __func__, ret);
  }

  if (ret) {
    switch (ret) {
      case SCSI_PT_DO_BAD_PARAMS:
        _pyErr( PyExc_IOError, ret,
                "SCSI ATA PT pass-thru failed: Bad parameters" );
        break;

      case SCSI_PT_DO_TIMEOUT:
        _pyErr( PyExc_IOError, ret,
                "SCSI ATA PT pass-thru failed: Timeout" );
        break;

      //case SCSI_PT_DO_NVME_STATUS:
      default:
        if (ret < 0) {
          _pyErr( PyExc_IOError, ret,
                  "SCSI ATA PT pass-thru failed: %s",
                  _errStr( ret ));

          /*
          int err = get_scsi_pt_os_err( ptp );
          if (err != abRet) {
            " ... or perhaps: %s", safe_strerr(err)
          }
          ret = sg_convert_errno( err );
          // */

        } else {

          _pyErr( PyExc_IOError, ret,
                  "SCSI ATA PT pass-thru failed: %s",
                  _errStr( (errno ? errno : ret) ) );
        }
        break;
    }

    goto py_err;
  }

  if (raw) {
    // Do NOT interpret the data, rather return a raw bytearray
    return PyByteArray_FromStringAndSize((const char *)rbuf, rbuf_len);
  }

  /***********************************************************************8
   * Create an initial dict to represent the results.
   *
   */
  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  *ret_len = rbuf_len - get_scsi_pt_resid( ptp );

  if (verbosity) {
    pr2ws("%s(): Requested %d bytes, got %d\n",
          __func__, rbuf_len, *ret_len);
  }

  status  = get_scsi_pt_status_response( ptp );
  res_cat = get_scsi_pt_result_category( ptp );

  sense_len = get_scsi_pt_sense_len( ptp );
  if (sense_len > 2) {
    sense_cat = sg_err_category_sense( sense_buf, sense_len );
  }

  switch( status ) {
    case SAM_STAT_CHECK_CONDITION:
      if (verbosity) {
        if (sense_len == 0) {
          pr2ws("%s(): SCSI ATA PT Status: strange -- CHECK CONDITION but no "
                        "sense information returned\n",
                __func__);
        }
      }
      break;

    case SAM_STAT_RESERVATION_CONFLICT:
      ret = -1;
      if (verbosity) {
        pr2ws("%s(): SCSI ATA PT Status: reservation conflict", __func__);
      }
      break;

    default:
      if (verbosity) {
        sg_get_scsi_status_str( status, str_len, str );

        pr2ws("%s(): SCSI ATA PT Status: %s\n", __func__, str);
      }
      break;
  }

  switch (res_cat) {
    case SCSI_PT_RESULT_GOOD:
      ret = 0;
      if (verbosity) {
        pr2ws("%s(): SCSI ATA PT Result: Good\n", __func__);
      }
      break;

    case SCSI_PT_RESULT_STATUS:
      if (verbosity) {
        pr2ws("%s(): SCSI ATA PT Result: Status\n", __func__);
      }
      break;

    case SCSI_PT_RESULT_SENSE:
      ret = sense_cat;
      if (verbosity) {
        sg_get_category_sense_str( sense_cat, str_len, str, verbosity );

        pr2ws("%s(): SCSI ATA PT Result: %s\n", __func__, str);
      }
      break;

    case SCSI_PT_RESULT_TRANSPORT_ERR:
      ret = -1;
      if (verbosity) {
        get_scsi_pt_transport_err_str( ptp, str_len, str );

        pr2ws("%s(): SCSI ATA PT Result: transport error: %s", __func__, str);
      }
      break;

    case SCSI_PT_RESULT_OS_ERR:
      ret = -1;
      if (verbosity) {
        get_scsi_pt_os_err_str( ptp, str_len, str );

        pr2ws("%s(): SCSI ATA PT Result: os error: %s", __func__, str);
      }
      break;

    default:
      ret = -1;
      if (verbosity) {
        pr2ws("%s(): SCSI ATA PT Result: unknown pass through category[ %d ]\n",
              __func__, res_cat);
      }
      break;
  }

  if (verbosity) {
    pr2ws("%s(): SCSI ATA PT Sense :", __func__);
    if (sense_len == 0) {
      pr2ws(" <EMPTY>\n");

    } else {
      pr2ws("\n");
      sg_print_sense( NULL, sense_buf, sense_len, verbosity );
    }
  }

  if (ret && !(ret == SG_LIB_CAT_RECOVERED ||
               ret == SG_LIB_CAT_NO_SENSE)) {
    *ret_len = 0;

    if (verbosity) {
      pr2ws("%s(): SCSI ATA PT No data received due to error[ %d ]\n",
            __func__, ret);
    }
  }

  if (verbosity) {
    int outLen  = *ret_len;
    if (verbosity < 3 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): SCSI ATA PT Showing only the first %d bytes:\n",
            __func__, outLen);

    } else {
      pr2ws("%s(): SCSI ATA PT %d byte result:\n",
            __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  PyDict_SetItemString( dict, "status",          PyLong_FromLong( status ));
  PyDict_SetItemString( dict, "result_category", PyLong_FromLong( res_cat ));
  PyDict_SetItemString( dict, "sense_category",  PyLong_FromLong( sense_cat ));
  PyDict_SetItemString( dict, "sense",
                        PyByteArray_FromStringAndSize((const char *)sense_buf,
                                                      sense_len) );

  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

  return NULL;
}

/**
 *  Given SMART attributes.
 *  @method _extract_smart_attrs
 *  @param  rbuf        A pointer to the raw data containing the parameters
 *                      {uint8_t*};
 *  @param  rbufLen     The length (bytes) of `rbuf` {int};
 *
 *  @return On sucess, a list of extracted parameters;
 *          On failure, raises an exception and returns NULL;
 *  @private
 */
static PyObject*
_extract_smart_attrs( uint8_t*  rbuf,
                      int       rbufLen ) {

  PyObject* list  = PyList_New( 0 );
  PyObject* dict  = NULL;
  int       cnt, idex;
  uint8_t   id;
  uint16_t  flags;
  uint8_t   val_current,        val_worst;
  bool      prefailure;
  bool      online;
  uint8_t*  raw_val;

  if (!list) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new list" );
    goto py_err;
  }

  for (cnt = 0, idex = 2; cnt < 30; cnt++, idex += 12) {
    id = rbuf[ idex ];
    if (id == 0) { continue; }

    dict = PyDict_New();
    if (! dict) {
      _pyErr( PyExc_IOError, errno,
              "Failed to create a dict for parameter" );
      goto py_err;
    }

    flags       = rbuf[ idex+2 ] << 8 | rbuf[ idex+1 ];
    val_current = rbuf[ idex+3 ];
    val_worst   = rbuf[ idex+4 ];
    prefailure  = !!(flags & 0x1);
    online      = !!(flags & 0x2);

    /* For easier conversion of the raw value, we include all 9 bytes [3..12]
     * instead of just the 6 bytes considered "raw" by smartmontools (5..11).
     *
     * This will allow simpler conversion since we won't need to do special
     * processing/formatting for the inclusion of 'current' 'worst' and
     * 'reserved' bytes when converting the raw value.
     *
     * ---
     *
     * Interpretation of `raw_val` is dependent upon the attribute id and, in
     * many cases, drive-specific information.
     *
     * smartmontools defines 3 primary formats for interpretation of raw data
     * (ata_get_attr_raw_value()):
     *    RAWFMT_RAW/HEX64      : "543210wv"  => "76543210"
     *
     *    RAWFMT_RAW/HEX56
     *    RAWFMT_RAW24_DIV_RAW32
     *    RAWFMT_MSEC24_HOUR32  : "r543210"   => "8765432"
     *
     *    default               : "543210"    => "765432"
     *
     * These raw values are then formatted (ata_format_attr_raw_value()) into
     * the final, presented value.
     *
     * ---
     *    Raw data interpretation:
     *      buf           = rbuf[ idex .. idex+11 ]
     *      attr.current  = buf[ 3 ]
     *      attr.worst    = buf[ 4 ]
     *      attr.raw      = buf[ 5 .. 10 ]
     *      attr.reserv   = buf[ 11 ]
     *
     *      val = 0;
     *      for (idex = 0; format[idex]; idex++) {
     *        switch( format[idex] ) {
     *          case '0': b = attr.raw[0];  break;  // => case '2'
     *          case '1': b = attr.raw[1];  break;  // => case '3'
     *          case '2': b = attr.raw[2];  break;  // => case '4'
     *          case '3': b = attr.raw[3];  break;  // => case '5'
     *          case '4': b = attr.raw[4];  break;  // => case '6'
     *          case '5': b = attr.raw[5];  break;  // => case '7'
     *          case 'r': b = attr.reserv;  break;  // => case '8'
     *          case 'v': b = attr.current; break;  // => case '0'
     *          case 'w': b = attr.worst;   break;  // => case '1'
     *          default : b = 0;            break;
     *        }
     *        val <<= 8; val |= b;
     *      }
     */
    raw_val = &rbuf[ idex+3 ];  // bytes[3..12]

    //val_current_valid = !!(val_current >= 1 && val_current <= 0xfd);
    //val_worst_valid   = !!(val_worst   >= 1 && val_worst   <= 0xfd);

    PyDict_SetItemString( dict, "id",
                                PyLong_FromLong( id ) );
    PyDict_SetItemString( dict, "flags",
                                PyLong_FromLong( flags ) );

    PyDict_SetItemString( dict, "value_current",
                                PyLong_FromLong( val_current ) );
    PyDict_SetItemString( dict, "value_worst",
                                PyLong_FromLong( val_worst ) );

    PyDict_SetItemString( dict, "prefailure",
                                PyBool_FromLong( prefailure ) );
    PyDict_SetItemString( dict, "online",
                                PyBool_FromLong( online ) );

    PyDict_SetItemString( dict, "value_raw",
                                PyByteArray_FromStringAndSize(
                                    (const char *)raw_val, 9) );

    if (PyList_Append(list, dict)) {
      _pyErr( PyExc_IOError, errno, "Failed to append parameter" );

      goto py_err;
    }

    dict = NULL;
  }

  return list;

py_err:
  if (dict) { Py_XDECREF(dict); }
  if (list) { Py_XDECREF(list); }

  return NULL;
}
/* Private helpers }
 *****************************************************************************
 * API methods {
 *
 *  SCSI commands
 *
 *  Test Unit Ready : CDB CMD: 0x00
 *  Inquiry         : CDB CMD: 0x12
 *  Mode Sense      : CDB CMD: 0x1a
 *    page  subpage
 *    1c    00      : Information Exceptions Control Page
 *
 *  Read Capacity 10: CDB CMD: 0x25
 *  Read Capacity 16: CDB CMD: 0x9e
 *
 *  Read Defect 10  : CDB CMD: 0x37
 *  Read Defect 12  : CDB CMD: 0xb7
 *
 *  Log Select      : CDB CMD: 0x4c
 *  Log Sense       : CDB CMD: 0x4d
 *
 *  Report LUNS     : CDB CMD: 0xa0
 */

static char _doc_open[] =
  "Open a SCSI device returning a handle representing that opened device.\n"
  "\n"
  "Args:\n"
  "  sg_name {str}                The path to the target SCSI device\n"
  "  [read_write = False] {bool}  Open for read AND write;\n"
  "  [verbosity = 0] {int}        Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "  {int}  file_descriptor\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  handle = sg3utils.open( \"/dev/sda\" )";
static PyObject*
spc_open( PyObject *self, PyObject *args, PyObject* kwargs ) {
  const char*         sg_name     = NULL;
  int                 read_only   = 1;
  int                 verbosity   = 0;
  int                 read_write  = 0;;
  int                 sg_fd       = 0;
  struct sg_pt_base*  ptp         = NULL;
  PyObject*           cap_ptp     = NULL;
  static char*        keys[]      = {"sg_name", "read_write", "verbosity",NULL};

  // Grab the arguments
  if (!PyArg_ParseTupleAndKeywords( args, kwargs, "s|$ii:open", keys,
                                    &sg_name, &read_write, &verbosity )) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }
  if (read_write) { read_only = 0; }

  // Open the device
  sg_fd = sg_cmds_open_device(sg_name, read_only, verbosity);
  if ( sg_fd < 0 ) {
    _pyErr( PyExc_IOError, sg_fd, "Failed to open device %s; %s",
            sg_name, _errStr( (errno ? errno : sg_fd) ) );
    return NULL;
  }

  ptp = construct_scsi_pt_obj_with_fd( sg_fd, verbosity );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Failed construction of SCSI pt for %s; %s",
            sg_name, _errStr( errno ) );
    sg_cmds_close_device( sg_fd );
    return NULL;
  }

  cap_ptp = PyLong_FromVoidPtr( ptp );
  //cap_ptp = PyCapsule_New( ptp, "spc_open:sg_pt_base", _destruct_ptp );

  return cap_ptp;
}

static char _doc_scsi_id[] =
  "Retrieve the SCSI id of a SCSI device opened via sg3utils.open().\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "  {dict|None} The SCSI id {host:, channel:, target:, lun:}\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.scsi_id( handle ) ->\n"
  "   {host:1, channel:2, target:3, lun:0 }";
static PyObject*
spc_scsi_id( PyObject *self, PyObject *args, PyObject* kwargs ) {
  PyObject*           handle      = NULL;
  int                 verbosity   = 0;
  struct sg_pt_base*  ptp         = NULL;
  struct sg_scsi_id   sgid        = {0};
  struct sg_scsi_id*  psgid       = &sgid;
  PyObject*           dict        = NULL;
  int                 rc          = 0;
  static char*        keys[]      = {"handle", "verbosity",NULL};

  // Grab the arguments
  if (!PyArg_ParseTupleAndKeywords( args, kwargs, "O|$i:scsi_id", keys,
                                    &handle, &verbosity )) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.scsi_id()" );
    return NULL;
  }

  if (ptp->impl.is_mr) {
    // The SCSI id should have already been retrieved
    psgid = &ptp->impl.mr.sgid;

  } else {
    // Retrieve the SCSI id now
    rc = scsi_pt_get_scsi_id( ptp->impl.dev_fd, psgid, verbosity );
    if (rc != 0) {
      /* Don't throw an exception, just return None to indicate that this is
       * NOT a device with a SCSI id
       */
      Py_INCREF(Py_None);
      return Py_None;
    }
  }

  if (verbosity) {
    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d ...\n",
            __func__,
            ptp->impl.dev_fd,
            psgid->host_no, psgid->channel,
            psgid->scsi_id, psgid->lun);
  }

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    goto py_err;
  }

  PyDict_SetItemString( dict, "host",
                              PyLong_FromLong( psgid->host_no ) );
  PyDict_SetItemString( dict, "channel",
                              PyLong_FromLong( psgid->channel ) );
  PyDict_SetItemString( dict, "target",
                              PyLong_FromLong( psgid->scsi_id ) );
  PyDict_SetItemString( dict, "lun",
                              PyLong_FromLong( psgid->lun ) );

  return dict;

py_err:
  if (dict) { Py_XDECREF( dict ); }

  return NULL;
}

static char _doc_close[] =
  "Close a SCSI device opened via sg3utils.open().\n"
  "\n"
  "Args:\n"
  "  handle {int} The handle returned from sg3utils.open();\n"
  "\n"
  "Returns (on success):\n"
  "  {int}  0\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.close( handle )";
static PyObject*
spc_close( PyObject *self, PyObject *args ) {
  PyObject* handle;
  struct sg_pt_base* ptp;

  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "O", &handle) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.close()" );
    return NULL;
  }

  if (ptp->impl.dev_fd >= 0) {
    // Attempt to close the file
    (void) sg_cmds_close_device( ptp->impl.dev_fd );

    /*
    if ( sg_cmds_close_device( ptp->impl.dev_fd ) < 0 ) {
      _pyErr( PyExc_IOError, errno, "Error on close(); %s", _errStr( errno ) );
      return NULL;
    }
    // */
  }

  destruct_scsi_pt_obj( ptp );

  return Py_BuildValue( "i", 0 );
}

static char _doc_asc_ascq_str[] =
  "Given SCSI ASC and ASCQ codes, generate the string representing the\n"
  "codes.\n"
  "\n"
  "Args:\n"
  "  asc {int}  The SCSI Additional Sense Code (ASC);\n"
  "  ascq {int} The SCSI Additional Sense Code Qualifier (ASCQ);\n"
  "\n"
  "Returns:\n"
  "  {str}  The string representation of the provide `asc` and `ascq`\n"
  "\n"
  "Example:\n"
  "  sg3utils.asc_ascq_str( 0x5d, 0x00 )\n"
  "    -> 'Failure prediction threshold exceeded'";
static PyObject*
spc_asc_ascq_str( PyObject *self, PyObject *args ) {
  unsigned int  asc   = 0;
  unsigned int  ascq  = 0;
  char          buf[128];

  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "II", &asc, &ascq) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  (void)sg_get_asc_ascq_str( asc, ascq, sizeof(buf), buf );

  return PyUnicode_FromString( buf );
}

static char _doc_is_mr[] =
  "Check if the SCSI device opened via sg3utils.open() is controlled by a\n"
  "MegaRaid controller. If True, this handle MAY already target a specific\n"
  "physical disk behind the controller (sg3utils.mr_get_target()). If it\n"
  "does not, it may be used to retrieve the set of physical disks\n"
  "associated with the controller (sg3utils.mr_get_pd_list()) and may then\n"
  "be targeted to a specific physical disk (sg3utils.mr_set_target()).\n"
  "Until a specific disk is targeted, such a handle will provide access\n"
  "to the controller but will likely fail when used with most other\n"
  "sg3utils calls.\n"
  "\n"
  "Args:\n"
  "  handle {int} The handle returned from sg3utils.open();\n"
  "\n"
  "Returns (on success):\n"
  "  {bool}  True | False\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.is_mr( handle ) -> True";
static PyObject*
spc_is_mr( PyObject *self, PyObject *args ) {
  PyObject* handle;
  struct sg_pt_base* ptp;

  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "O", &handle) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.is_mr()" );
    return NULL;
  }

  return PyBool_FromLong( !!(ptp->impl.is_mr) );
}

static char _doc_mr_get_target[] =
  "Given a SCSI device opened via sg3utils.open() that is controlled by a\n"
  "MegaRaid controller, retrieve the id of the current target disk that\n"
  "will be used for further SCSI pass-through commands.\n"
  "\n"
  "Args:\n"
  "  handle {int} The handle returned from sg3utils.open();\n"
  "\n"
  "Returns (on success):\n"
  "  {int}  The target disk/device id;\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  did = sg3utils.mr_get_target( handle )";
static PyObject*
spc_mr_get_target( PyObject *self, PyObject *args ) {
  PyObject* handle;
  struct sg_pt_base* ptp;

  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "O", &handle ) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.mr_get_target()" );
    return NULL;
  }

  return PyLong_FromLong( ptp->impl.mr.target_id );
}

static char _doc_mr_set_target[] =
  "Given a SCSI device opened via sg3utils.open() that is controlled by a\n"
  "MegaRaid controller, set the target disk id for further SCSI\n"
  "pass-through commands.\n"
  "\n"
  "Args:\n"
  "  handle {int} The handle returned from sg3utils.open();\n"
  "  did    {int} The target disk/device id ('device_id' from the pd list);\n"
  "\n"
  "Returns (on success):\n"
  "  {int}  The new target disk id;\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.mr_set_target( handle, 123 )";
static PyObject*
spc_mr_set_target( PyObject *self, PyObject *args ) {
  PyObject* handle;
  int       did;
  struct sg_pt_base* ptp;

  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "Oi", &handle, &did) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.mr_set_target()" );
    return NULL;
  }

  ptp->impl.mr.target_id = did;

  return PyLong_FromLong( ptp->impl.mr.target_id );
}

static char _doc_mr_get_pd_list[] =
  "Given a SCSI device opened via sg3utils.open() that is controlled by a\n"
  "MegaRaid controller, retrieve the set of physical disks behind the\n"
  "controller.\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "  {list} The list of physical disks where each entry contains:\n"
  "           {device_id:, encl_device_id:, encl_index:, slot_number:,\n"
  "            scsi_dev_type:, connect_port_bitmap:,\n"
  "            sas_addr_1:, sas_addr_2:}\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.mr_get_pd_list( handle ) ->\n"
  "   [ {device_id:, encl_device_id:, encl_index:, slot_number:,\n"
  "      scsi_dev_type:, connect_port_bitmap:,\n"
  "      sas_addr_1:, sas_addr_2:},\n"
  "     ...\n"
  "   ]";
static PyObject*
spc_mr_get_pd_list( PyObject *self, PyObject *args ) {
  PyObject*           handle    = NULL;
  int                 verbosity = 0;
  struct sg_pt_base*  ptp       = NULL;
  int                 ctl_fd;
  struct sg_scsi_id*  sgid;
  struct MR_PD_LIST   pd_list   = {0};
  PyObject*           py_list   = NULL;
  int                 rc;
  uint32_t            idex;


  // Grab the arguments
  if ( !PyArg_ParseTuple( args, "O|i", &handle, &verbosity) ) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.mr_get_pd_list()" );
    return NULL;
  }

  sgid = &ptp->impl.mr.sgid;

  if (verbosity) {
    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d type [ 0x%x ] ...\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            sgid->scsi_type);
  }

  // Open the control node
  ctl_fd = sg_mr_open_control( verbosity );
  if (ctl_fd < 0) {
    _pyErr( PyExc_IOError, ctl_fd,
              "Cannot open MegaRaid control node: %s",
              _errStr( (errno ? errno : ctl_fd) ) );
    return NULL;
  }

  /* Fetch the physical disk list from the controller identified via:
   *  sgid->host_no
   */
  rc = sg_mr_get_pd_list( ctl_fd, sgid, &pd_list, verbosity );

  close( ctl_fd );

  if (rc) {
    _pyErr( PyExc_IOError, rc, "PD list failed: %s",
            _errStr( (errno ? errno : rc) ) );
    return NULL;
  }

  /*************************************************
   * :TODO: Construct the list of dictionaries
   *
   */
  py_list = PyList_New( 0 );  // ( pd_list.count );
  if (!py_list) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new list" );
    return NULL;
  }

  for (idex = 0; idex < pd_list.count; idex++) {
    PyObject*             dict  = NULL;
    struct MR_PD_ADDRESS* addr  = &pd_list.addr[ idex ];

    if (verbosity) {
      pr2ws("%s: %4d: did[ %d ], enc[ %d : %d ], slot[ %d ], type[ %d ]\n",
              __func__, idex,
              addr->device_id, addr->encl_device_id, addr->encl_index,
              addr->slot_number, addr->scsi_dev_type);
    }

    /*
    if (addr->scsi_dev_type != TYPE_DISK) {
      continue;
    }
    // */

    dict = PyDict_New();
    if (!dict) {
      _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
      goto py_err;
    }

    PyDict_SetItemString( dict, "device_id",
                                PyLong_FromLong( addr->device_id ) );
    PyDict_SetItemString( dict, "encl_device_id",
                                PyLong_FromLong( addr->encl_device_id ) );
    PyDict_SetItemString( dict, "encl_index",
                                PyLong_FromLong( addr->encl_index ) );
    PyDict_SetItemString( dict, "slot_number",
                                PyLong_FromLong( addr->slot_number ) );
    PyDict_SetItemString( dict, "scsi_dev_type",
                                PyLong_FromLong( addr->scsi_dev_type ) );
    PyDict_SetItemString( dict, "connect_port_bitmap",
                                PyLong_FromLong( addr->connect_port_bitmap ) );
    PyDict_SetItemString( dict, "sas_addr_0",
                                PyLong_FromLongLong( addr->sas_addr[0] ) );
    PyDict_SetItemString( dict, "sas_addr_1",
                                PyLong_FromLongLong( addr->sas_addr[1] ) );

    if (PyList_Append(py_list, dict)) {
      _pyErr( PyExc_IOError, errno, "Failed to append pd list item" );

      Py_XDECREF( dict );

      goto py_err;
    }
  }

  return py_list;

py_err:
  if (py_list) {
    Py_XDECREF( py_list );
  }

  return NULL;
}

static char _doc_parse_log_sense[] =
  "Parse the raw data returned from a LOG SENSE.\n"
  "See SCSI SPC-3 spec for more info:\n"
  "\n"
  "Args:\n"
  "  data {bytearray}   The raw LOG SENSE data;\n"
  "\n"
  "Returns (on success):\n"
  "   {dict}      If `raw` is False;\n"
  "                 { disable_save  : (bool),\n"
  "                   subpage_format: (int),\n"
  "                   page          : (int),\n"
  "                   subpage       : (int),\n"
  "                   params        : (list of dicts),\n"
  "                 }\n"
  "\n"
  "   For page 0, subpage 0, each entry in `params` will be:\n"
  "     { page: (int) }\n"
  "\n"
  "   For page 0, subpage 0xff, each entry in `params` will be:\n"
  "     { page: (int), subpage: (int) }\n"
  "\n"
  "   For all other log pages, each entry in `params` will be:\n"
  "     { code                        : (int),\n"
  "       offset                      : (int),\n"
  "       disable_update              : (bool),\n"
  "       target_save_disable         : (bool),\n"
  "       enable_threshold_comparison : (bool),\n"
  "\n"
  "       threshold_met_criteria      : (int),\n"
  "       # 0x00  every update of the cumulative value;\n"
  "       # 0x01  Cumulative value  equal to      threshold value;\n"
  "       # 0x02  Cumulative value  not equal to  threshold value;\n"
  "       # 0x03  Cumulative value  greater than  threshold value;\n"
  "\n"
  "       format_and_linking          : (int),\n"
  "       # 0x00  Bounded data counter\n"
  "       # 0x01  ASCII format list\n"
  "       # 0x02  Bounded data counter or unbounded data counter\n"
  "       # 0x03  Binary format list\n"
  "\n"
  "       value_raw                   : (bytearray),\n"
  "       # Raw bytes of the parameter value\n"
  "\n"
  "       value                       : (str | int | bytearray),\n"
  "       # `value_raw` interpreted according to `format_and_linking`\n"
  "     }\n"
  "\n"
  "Example:\n"
  "  sg3utils.parse_log_sense( data ) ->\n"
  "    { disable_save:,\n"
  "      subpage_format:,\n"
  "      page:,\n"
  "      subpage:,\n"
  "      params:,\n"
  "    }\n";
static PyObject*
spc_parse_log_sense(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyByteArrayObject*  data          = NULL;
  static char*        keys[]    = {"data", NULL};
  uint8_t*            buf       = NULL;
  int                 buf_len   = 0;

  // Grab the arguments
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "Y:parse_log_sense", keys,
                                   &data)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  buf     = (uint8_t*)PyByteArray_AsString( (PyObject*)data );
  buf_len = (int     )PyByteArray_Size(     (PyObject*)data );

  return _parse_log_sense( buf, buf_len );
}

static char _doc_readcap[] =
  "Returns the result of READ CAPACITY(16), as a dict.\n"
  "See SCSI SBC-3 spec for more info\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "  {dict}       If `raw` is False, the constructed dictionary of\n"
  "               interpreted response data:\n"
  "                 last_logical_block_address,\n"
  "                 logical_block_length,\n"
  "                 p_type,\n"
  "                 prot_en,\n"
  "                 p_i_exponent,\n"
  "                 lbppbe,\n"
  "                 lbpme,\n"
  "                 lbprz,\n"
  "                 lalba\n"
  "  {bytearray}  If `raw` is True;\n"
  "\n"
  "Exception (on failure)\n"
  "\n"
  "Example:\n"
  "  sg3utils.readcap( handle ) ->\n"
  "   (last_logical_block_address, logical_block_length, p_type, prot_en,\n"
  "    p_i_exponent, lbppbe, lbpme, lbprz, lalba)";
static PyObject *
spc_readcap(PyObject *self, PyObject *args, PyObject* kwargs) {
  unsigned char*      rbuf      = _rspBuf;
  int                 rbufLen   = sizeof(_rspBuf);
  bool                raw       = false;
  int                 verbosity = 0;
  PyObject*           handle;
  struct sg_pt_base*  ptp;
  int                 ret;
  uint64_t            llast_blk_addr;
  uint64_t            block_size;
  int                 idex;
  PyObject*           dict;
  static char*        keys[]    = {"handle", "raw", "verbosity", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$pi:readcap", keys,
                                   &handle, &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.readcap()" );
    return NULL;
  }

  memset(rbuf, 0, rbufLen);

  ret = sg_ll_readcap_16(ptp->impl.dev_fd,
                          false,      // bool     pmi
                          0,          // uint64_t llba
                          rbuf,       // void*    resp
                          rbufLen,    // int      mx_resp_len
                          false,      // bool     noisy
                          verbosity); // int      verbose
  if (ret < 0) {
    _pyErr( PyExc_IOError, ret, "SCSI READ CAP failed: %s",
            _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  if (raw) {
    // Do NOT interpret the data, rather return a raw bytearray
    return PyByteArray_FromStringAndSize((const char *)rbuf, rbufLen);
  }

  /************************************************************************
   * SCSI SBC-3, Table 111: READ CAPACITY (16) parameter data
   *
   *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *     Byte |     |     |     |     |     |     |     |     |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0   |                                               |
   *     ...  | Returned Logical Block Addr                   |
   *      7   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      8   |                                               |
   *     ...  | Logical Block Length (bytes)                  |
   *      11  |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      12  | Reserved              | p_type          | p_en|
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      13  | pi_exp                | lbppbe                |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      14  |lbpme|lbprz| lalba                             |
   *    ------+-----+-----+                                   |
   *      15  |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      16  |                                               |
   *      ... | Reserved                                      |
   *      31  |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *  Protection type (p_type, p_en):
   *    p_en  p_type
   *     0     XXXb   Type 0 protection
   *     1     000b   Type 1 protection
   *     1     001b   Type 2 protection
   *     1     010b   Type 3 protection
   *
   *  Protection Information Intervals (pi_exp,
   *    pi_exp/p_i_exponent
   *              may be used to determine the number of protection information
   *              intervals placed within each logical block;
   *
   *                number of protection information intervals = 2(pi_exp)
   *
   *    lbppbe  Logical blocks per physical blocks exponent:
   *              0     One or more physical blocks per logical block;
   *              n>0   2n logical blocks per physical block;
   *
   *    lbpme   Logical Block Provisioning Management Enabled
   *              does the logical unit implement logical block provisioning:
   *                1   yes
   *                0   no
   *
   *    lbprz   Logical Block Provisioning Read Zeros
   *              for an unmapped LBA specified by a read operation, the device
   *              server:
   *                1   shall send user data with all bits set to zero;
   *                0   may send user data with all bits set to any value;
   *
   *    lalba   Lowest Aligned Logical Block Address, indicates the LBA of the
   *            first logical block that is located at the beginning of a
   *            physical block;
   */
  for (idex = 0, llast_blk_addr = 0; idex < 8; ++idex) {
    llast_blk_addr <<= 8;
    llast_blk_addr |= rbuf[ idex ];
  }

  //block_size = ((rbuf[8] << 24) | (rbuf[9] << 16) | (rbuf[10] << 8)|rbuf[11]);
  block_size = sg_get_unaligned_be32( rbuf + 8 );

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    return NULL;
  }

  PyDict_SetItemString(dict, "logical_blk_addr",
                                PyLong_FromLongLong( llast_blk_addr ));
  PyDict_SetItemString(dict, "logical_blk_len",
                                PyLong_FromLongLong( block_size ));
                          // "p_type",
  PyDict_SetItemString(dict, "prot_type",
                                PyLong_FromLong((rbuf[12] >> 1) & 0x7));
                          // "prot_en",
  PyDict_SetItemString(dict, "prot_enabled",
                                PyBool_FromLong( !!(rbuf[12] & 0x1)) );
                          // "p_i_exponent",
  PyDict_SetItemString(dict, "prot_interval_exp",
                                PyLong_FromLong((rbuf[13] >> 4)) );
                          // "lbppbe",
  PyDict_SetItemString(dict, "logical_blocks_per_phsical_block_exp",
                                PyLong_FromLong( rbuf[13] & 0xf) );
                          // "lbpme",
  PyDict_SetItemString(dict, "logical_block_provisioning_mgmt_enabled",
                                PyBool_FromLong( !!(rbuf[14] & 0x80)) );
                          // "lbprz",
  PyDict_SetItemString(dict, "logical_block_provisioning_read_zeros",
                                PyBool_FromLong( !!(rbuf[14] & 0x40)) );
                          // "lalba",
  PyDict_SetItemString(dict, "lowest_aligned_logical_block_addr",
                                PyLong_FromLong( ((rbuf[14] & 0x3f) << 8)
                                                 + rbuf[15]) );

  return dict;
}

static char _doc_inquiry[] =
  "Returns the result of INQUIRY.\n"
  "See SCSI SPC-3 spec for more info:\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [page = 0] {int}       If provided, perform an inquiry for the given\n"
  "                         VPD page. If NOT provided (or 0), perform a\n"
  "                         simple inquiry.\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "  {bytearray}  If `raw` is True;\n"
  "  {mixed}      If `raw` is False, the interpreted response data\n"
  "               depending on the requested page;\n"
  "\n"
  "Example:\n"
  "  sg3utils.inquiry( handle ) ->\n"
  "    {vendor:, product:, revision:, serial:, peripheral_qualifier:,\n"
  "     peripheral_type:, version:, is_removable_media:,\n"
  "     normal_aca_support:, hierarchical_support:, response_data_format:,\n"
  "     scc_support:, access_controls_coordinator:,\n"
  "     target_port_group_support:, third_party_copy:, protect:,\n"
  "     enclosure_services:, multi_port:, command_queuing: }\n"
  "\n"
  "Currently supported pages:\n"
  "  0x00 (default): Simple Inquiry\n"
  "    Returns a dict of inquiry data:\n"
  "       vendor                      : (str)\n"
  "       product                     : (str)\n"
  "       revision                    : (str)\n"
  "       serial                      : (str)  \n"
  "       peripheral_qualifier        : (long)\n"
  "       peripheral_type             : (long)\n"
  "       version                     : (long)\n"
  "       is_removable_media          : (bool)\n"
  "       normal_aca_support          : (bool)\n"
  "       hierarchical_support        : (bool)\n"
  "       response_data_format        : (long)\n"
  "       scc_support                 : (bool)\n"
  "       access_controls_coordinator : (bool)\n"
  "       target_port_group_support   : (bool)\n"
  "       third_party_copy            : (long)\n"
  "       protect                     : (bool)\n"
  "       enclosure_services          : (bool)\n"
  "       multi_port                  : (bool)\n"
  "       command_queuing             : (bool)\n"
  "\n"
  "  0x80: Unit serial number\n"
  "    Returns a string of the unit serial number.\n"
  "\n"
  "  0x83: Device information\n"
  "    Returns a list of identification descriptors where each\n"
  "    element is a dict containing:\n"
  "     protocol_id : (str)\n"
  "     char_set    : (str) Character set\n"
  "     association : (str) Associated entity\n"
  "     id_type     : (str)\n"
  "     id_value    : (bytearray)\n"
  "\n"
  "  0xb1: Block Device Characteristics\n"
  "    Returns a dict of inquiry data:\n"
  "       product_type                : (str)\n"
  "       form_factor                 : (str)\n"
  "       rpm                         : (str | int)\n"
  "         valid strings: not-reported, non-rotating\n";
static PyObject *
spc_inquiry(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle    = NULL;
  struct sg_pt_base*  ptp       = NULL;
  int                 page      = 0;
  bool                raw       = false;
  int                 verbosity = 0;
  static char*        keys[]    = {"handle", "page", "raw", "verbosity", NULL};


  if ( !PyArg_ParseTupleAndKeywords(args, kwargs, "O|i$pi:inquiry", keys,
                                    &handle, &page, &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  /* :XXX: PyArg_ParseTupleAndKeywords() seems to properly validate and extract
   *       all arguments EXCEPT the first integer keyword value (page) ALWAYS
   *       setting the value to 0.
   */
  if (kwargs && page == 0) {
    page = _get_page_val( kwargs );
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.inquiry()" );
    return NULL;
  }

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ]\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id);
  }

  if (raw) {
    return _inq_raw( ptp, page, verbosity );
  }

  switch (page) {
  case SCSI_VPD_SUPPORTED_PAGES:                  // 0x00
    return _inq_simple( ptp, verbosity );

  case SCSI_VPD_UNIT_SERIAL_NUMBER:               // 0x80
    return _inq_vpd_usn( ptp, verbosity );

  case SCSI_VPD_DEVICE_IDENTIFICATION:            // 0x83
    return _inq_vpd_di( ptp, verbosity );

  case SCSI_VPD_BLOCK_DEVICE_CHARACTERISTICS:     // 0xb1
    return _inq_vpd_bdc( ptp, verbosity );

  default:
    return _inq_raw( ptp, page, verbosity );
  }
}

static char _doc_mode_sense[] =
  "Returns the result of MODE SENSE(10), as a dictionary of sense mode\n"
  "codes to page code data.\n"
  "See SCSI SPC-3 spec for more info.\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [page = 0x3f] {int}    The target log page (0x3f == supported pages);\n"
  "  [subpage = 0] {int}    The target subpage;\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {bytearray} If `raw` is True;\n"
  "   {dict}      If `raw` is False;\n"
  "                 { page        : (int),\n"
  "                   subpage     : (int),\n"
  "                   medium_type : (int),\n"
  "                   specific    : (int),\n"
  "                   long_lba    : (bool),\n"
  "                   data        : (list | dict | bytearray),\n"
  "                 }\n"
  "\n"
  "   The default type of `data` will bytearray;\n"
  "\n"
  "   For page 0x1c,00, `data` will be a dict of the form:\n"
  "     performance               : (bool)\n"
  "     enable_background_function: (bool)\n"
  "     enable_warning            : (bool)\n"
  "     disable_exception_control : (bool)\n"
  "     test                      : (bool)\n"
  "     enable_background_error   : (bool)\n"
  "     log_error                 : (bool)\n"
  "     method_of_reporting_ie    : (int)\n"
  "       0 : no reporting\n"
  "       1 : async reporting (obsolete)\n"
  "       2 : generate unit attention\n"
  "       3 : conditionally generate recovered error\n"
  "       4 : unconditionally generate recovered error\n"
  "       5 : generate no sense\n"
  "       6 : only report informational exception condition on request\n"
  "     interval_timer            : (int) (100 ms increments)\n"
  "     report_count              : (int)\n"
  "\n"
  "   For page 0x3f, `data` will be a list of supported pages with\n"
  "   bytearray data for each\n"
  "\n"
  "Example:\n"
  "  sg3utils.mode_sense( fd ) ->\n"
  "   { ... }\n";
static PyObject *
spc_mode_sense(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle      = NULL;
  struct sg_pt_base*  ptp         = NULL;
  int                 page        = MODE_PAGE_ALL;
  int                 subpage     = 0;
  bool                raw         = false;
  int                 verbosity   = 0;
  static char*        keys[]      = {"handle", "page", "subpage",
                                               "raw",  "verbosity", NULL};
  int                 ret;
  int                 resid       = 0;
  int                 header_len  = 8;  // mode_sense10 header length
  unsigned char*      md_params   = NULL;
  unsigned int        md_len;
  int                 bd_len;           // block descriptor length
  int                 medium_type = 0;
  int                 specific    = 0;
  bool                long_lba    = false;
  int                 idex;
  PyObject*           dict        = NULL;
  PyObject*           list        = NULL;
  PyObject*           data        = NULL;
#ifdef  MODE_SENSE_PROBE  // {
  unsigned char       probeBuf[ MODE_SENSE_PROBE_ALLOC_LEN ];
  unsigned char*      rbuf      = probeBuf;
  int                 rbufLen   = sizeof(probeBuf);
#else   // !MODE_SENSE_PROBE }{
  unsigned char*      rbuf      = _rspBuf;
  int                 rbufLen   = sizeof(_rspBuf);
#endif  // MODE_SENSE_PROBE }

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|i$ipi:mode_sense", keys,
                                   &handle, &page, &subpage,
                                   &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  /* :XXX: PyArg_ParseTupleAndKeywords() seems to properly validate and extract
   *       all arguments EXCEPT the first integer keyword value (page) ALWAYS
   *       setting the value to 0.
   */
  if (kwargs && page == 0) {
    page = _get_page_val( kwargs );
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.mode_sense()" );
    return NULL;
  }

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "page[ 0x%02x, %02x ]\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            page, subpage);
  }

  memset(rbuf, 0, rbufLen);

#ifdef  MODE_SENSE_PROBE // {
  // First, probe for the length of the actual response
  ret = sg_ll_mode_sense10_v2(ptp->impl.dev_fd,
                              false,                  // bool  llbaa
                              false,                  // bool  dbd
                              MPAGE_CONTROL_CURRENT,  // int   pc
                              page,                   // int   pg_code
                              subpage,                // int   sub_pg_code
                              rbuf,                   // void* resp
                              rbufLen,                // int   mx_resp_len
                              0,                      // int   timeout_secs
                              &resid,                 // int*  resid
                              false,                  // bool  noisy
                              verbosity);             // int   verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI MODE SENSE 10 [ 0x%02x,%02s ] probe "
                              "failed: %s",
            page, subpage, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  pr2ws("%s(): fd[ %d ], Mode sense (find length): resid[ %d ] ...\n",
          __func__, ptp->impl.dev_fd, resid);

  rbufLen = sg_msense_calc_length( rbuf, rbufLen,
                                   false,  // mode_sense_6
                                   NULL);  // bd_lenp
  if (rbufLen < 0) {
    rbufLen = 0;
  }

  if (verbosity > 1) {
    pr2ws("%s(): fd[ %d ], Mode sense (find length): %d\n",
            __func__, ptp->impl.dev_fd, rbufLen);
  }

  if ((unsigned int)rbufLen > sizeof(_rspBuf)) {
    // TOO big (trim)
    if (verbosity) {
      pr2ws("%s(): fd[ %d ], Trim response length to max response buffer\n",
              __func__, ptp->impl.dev_fd);
    }

    rbufLen = sizeof(_rspBuf) - 1;
  }

  rbuf = _rspBuf;

  /**************************************************************************
   * Perform the log sense request for real
   *
   */
#endif  // MODE_SENSE_PROBE }
  ret = sg_ll_mode_sense10_v2(ptp->impl.dev_fd,
                              false,                  // bool  llbaa
                              false,                  // bool  dbd
                              MPAGE_CONTROL_CURRENT,  // int   pc
                              page,                   // int   pg_code
                              subpage,                // int   sub_pg_code
                              rbuf,                   // void* resp
                              rbufLen,                // int   mx_resp_len
                              0,                      // int   timeout_secs
                              &resid,                 // int*  resid
                              false,                  // bool  noisy
                              verbosity);             // int   verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI MODE SENSE 10 [ 0x%02x,%02s ] "
                              "failed: %s",
            page, subpage, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  /* SCSI SBC-3, Table 362 Mode parameter header(10)
   *
   *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *     Byte |     |     |     |     |     |     |     |     |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0   | Mode Data Length                              |
   *      1   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      2   | Medium Type                                   |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      3   | WP  |RSVD |DPOFU| Reserved                    |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      4   | Reserved                                |LLBA |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      5   | Reserved                                      |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      6   | Block Descriptor Length                       |
   *      7   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *  Medium Type:  Page-specific values;
   *
   *  Write Protect (WP):
   *    0   Medium is not write-protected;
   *    1   Medium is write-protected;
   *
   *  DPO & FUA support (DPOFU)
   *
   *  Long LBA (LLBA):
   *    0 Mode parameter block descriptor(s) are  8-bytes long;
   *    1 Mode parameter block descriptor(s) are 16-bytes long;
   */
  md_len = sg_msense_calc_length( rbuf, rbufLen,
                                  false,    // mode_sense_6
                                  &bd_len); // bd_lenp
  if (md_len < 0) {
    _pyErr( PyExc_IOError, md_len, "sg_msense_cal_length() failed\n");
    return NULL;
  }
  if ((int)md_len >= rbufLen) {
    md_len = rbufLen;
  }

  if ((unsigned int)(bd_len + header_len) > md_len) {
    pr2serr("Invalid block descriptor length=%d, ignore\n", bd_len);
    bd_len = 0;
  }

  if (verbosity > 3) {
    int outLen  = md_len;
    if (verbosity < 6 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): Showing only the first %d bytes:\n", __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  if (raw) {
    // Do NOT interpret the data, rather return a raw bytearray
    return PyByteArray_FromStringAndSize((const char *)rbuf, md_len);
  }

  if (verbosity) {
    pr2ws("%s(): rbufLen[ %d ], resid[ %d ] => [ %d ]: "
                    "md_len[ %d ], bd_len[ %x ]\n",
          __func__, rbufLen, resid, rbufLen - resid,
                    md_len, bd_len);
  }

  medium_type = rbuf[2];
  specific    = rbuf[3];
  long_lba    = !!(rbuf[4] & 0x01);

  // Locate the set of parameters for the retrieved page
  md_params   = rbuf + bd_len + header_len;
  md_len     -= bd_len + header_len;

  /************************************************************************
   * Generate a python dictionary containing the mode sense data.
   *
   */
  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dictionary" );
    return NULL;
  }

  if ( page == MODE_PAGE_ALL ) {
    list = PyList_New(0);
    if (!list) {
      _pyErr( PyExc_IOError, errno, "Failed to create a new list" );
      return NULL;
    }
  }

  PyDict_SetItemString( dict, "page",         PyLong_FromLong( page ));
  PyDict_SetItemString( dict, "subpage",      PyLong_FromLong( subpage ));
  PyDict_SetItemString( dict, "medium_type",  PyLong_FromLong( medium_type ));
  PyDict_SetItemString( dict, "specific",     PyLong_FromLong( specific ));
  PyDict_SetItemString( dict, "long_lba",     PyBool_FromLong( long_lba ));

  for (idex = 0; md_len > 0; ++idex) {
    /* Most pages should have a single block which should have matching
     * page/sugpage values in the block header.
     *
     * Page 0x3f is special and will have a block for each supported mode
     * page/subpage with the full data for that page/subpage.
     *
     * Each block is interpreted according to the page/subpage recorded for
     * that block.
     *
     * SCSI SBC-3, Table 365: Page_0 mode page format
     *
     *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
     *     Byte |     |     |     |     |     |     |     |     |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      0   | PS  | SPF | Page Code                         |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      1   | Page Length (n-1)                             |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      2   | Mode Parameters                               |
     *     ...  |                                               |
     *      n   |                                               |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *
     * SCSI SBC-3, Table 366: SUB_PAGE mode page format
     *
     *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
     *     Byte |     |     |     |     |     |     |     |     |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      0   | PS  | SPF | Page Code                         |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      1   | Subpage Code                                  |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      2   | Page Length (n-3)                             |
     *      3   |                                               |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *      4   | Mode Parameters                               |
     *     ...  |                                               |
     *      n   |                                               |
     *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
     *
     *  Parameters saveable (PS):
     *    0   device server is not able to save the supported parameters;
     *    1   mode page may be saved by the logical unit in nonvolatile store;
     *
     *  Subpage Format (SPF):
     *    0   Page_0   mode page format;
     *    1   SUB_PAGE mode page format;
     *
     *  Page Code:
     *    The page code being transferred;
     *
     *  Subpage Code:
     *    The subpage code being transferred;
     *
     *  Page Length:
     *    The length, in bytes, of the following mode parameters;
     */
    bool            spf         = !!(md_params[0] & 0x40);
    int             mpage       = md_params[0] & 0x3f;
    int             phdr_len    = (spf ? 4 : 2);
    int             param_len   = (spf ? sg_get_unaligned_be16(md_params + 2)
                                       : md_params[1]);
    int             page_len    = param_len + phdr_len;
    int             msubpage    = (spf ? md_params[1] : 0);
    unsigned char*  param       = md_params + phdr_len;

    if (data) {
      pr2ws("%s(): %2d: Unexpected multiple blocks in page 0x%02x,%02x\n",
            __func__, idex, page, subpage);

      Py_XDECREF(data);
      data = NULL;
    }

    /*  if (page == MODE_PAGE_ALL) {
     *    assert( page != mpage );
     *  } else {
     *    assert( page == mpage && idex < 1 );
     *  }
     */

    if (verbosity) {
      pr2ws("%s(): %2d: spf[ %d ], page_len[ %3d ], mpage[ 0x%02x, %02x ]\n",
            __func__, idex, spf, page_len, mpage, msubpage);

      if (verbosity > 1) {
        pr2ws("%s(): param:\n", __func__);
        hex2stdout( param, param_len, true /* no_ascii */ );
      }
    }

    if (param_len > page_len) {
      if (verbosity) {
        pr2ws("%s(): param_len[ %d ] > page_len[ %d ]: trim\n",
              __func__, param_len, page_len);
      }

      param_len = page_len;
    }

    if ((unsigned int)param_len > md_len) {
      if (verbosity) {
        pr2ws("%s(): param_len[ %d ] > remainder[ %d ]: trim\n",
              __func__, param_len, md_len);
      }

      param_len = md_len;
    }

    switch (mpage) {
      case MODE_PAGE_CONTROL: // 0x1c
        // assert( page    == mpage )
        // assert( subpage == msubpage )
        switch (msubpage) {
          case MODE_SUBPAGE_CONTROL_INFORMATIONAL_EXCEPTIONS:
            data = _mode_pc_ie( param, param_len );
            break;

          // :TODO: MODE_SUBPAGE_CONTROL_BACKGROUND
          // :TODO: MODE_SUBPAGE_CONTROL_LOGICAL_BLOCK_PROVISION

          default:
            data = PyByteArray_FromStringAndSize(
                                    (const char *)param, param_len );
            break;
        }
        break;

      default:
        data = PyByteArray_FromStringAndSize(
                                    (const char *)md_params, page_len );
        break;
    }

    if (!data) {
      _pyErr( PyExc_IOError, errno, "Failed to interpret block descriptors" );
      goto py_err;
    }

    if (list) {
      // assert( page == MODE_PAGE_ALL )

      /* :TODO: Wrap 'data' in a full page description:
       *
       *    { page               : (int)
       *      subpage            : (int)
       *      parameters_saveable: (bool)
       *      subpage_format     : (bool)
       *      data               : data
       *    }
       */
      if (PyList_Append(list, data)) {
        _pyErr( PyExc_IOError, errno, "Failed to append list item" );
        Py_XDECREF( data );
        goto py_err;
      }

      data = NULL;
    }

    md_params += page_len;
    md_len    -= page_len;
  }

  if (list) {
    PyDict_SetItemString( dict, "data", list );

  } else if (data) {
    PyDict_SetItemString( dict, "data", data );

  }

  return dict;

py_err:
  if (dict)   { Py_XDECREF(dict); }
  if (list)   { Py_XDECREF(list); }
  if (data)   { Py_XDECREF(data); }

  return NULL;
}

static char _doc_report_luns[] =
  "Returns the result of REPORT LUNS.\n"
  "Currently only non-hierarchical LUNs are supported.\n"
  "See SCSI SPC-3 and SAM-5 specs for more info.\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {bytearray} If `raw` is True;\n"
  "   {dict}      If `raw` is False;\n"
  "\n"
  "Example:\n"
  "  sg3utils.report_luns( fd ) ->\n"
  "   { ... }\n";
static PyObject *
spc_report_luns(PyObject *self, PyObject *args, PyObject* kwargs) {
  /*  REPORT_LUNS_MAX_COUNT 256
   *  LUN_SIZE              8
   *
   *  unsigned char rbuf[REPORT_LUNS_MAX_COUNT * LUN_SIZE];
   */
  unsigned char*      rbuf      = _rspBuf;
  int                 rbufLen   = sizeof(_rspBuf);
  bool                raw       = false;
  int                 verbosity = 0;
  PyObject*           handle;
  struct sg_pt_base*  ptp;
  int                 ret;
  unsigned int        data_len;
  int                 idex;
  PyObject*           tuple;
  int                 luns;
  int                 off;
  static char*        keys[]    = {"handle", "raw", "verbosity", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$pi:report_luns", keys,
                                   &handle, &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.report_luns()" );
    return NULL;
  }

  memset(rbuf, 0, rbufLen);

  /* only report=0 for now */
  ret = sg_ll_report_luns_pt(ptp,
                              0,          // int    select_report
                              rbuf,       // void*  resp
                              rbufLen,    // int    mx_resp_len
                              false,      // bool   noisy
                              verbosity); // int    verbose
  if (ret < 0) {
    _pyErr( PyExc_IOError, errno, "SCSI REPORT LUNS failed: %s",
            _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  //data_len = ((rbuf[0] << 24) + (rbuf[1] << 16) + (rbuf[2] << 8) + rbuf[3])+8;
  data_len = sg_get_unaligned_be32( rbuf ) + 8;

  if (verbosity > 3) {
    unsigned int outLen  = data_len;
    if (verbosity < 6 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): Showing only the first %u bytes:\n", __func__, outLen);

    } else {
      pr2ws("%s(): Received %u bytes:\n", __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  if (raw) {
    // Do NOT interpret the data, rather return a raw bytearray
    return PyByteArray_FromStringAndSize((const char *)rbuf, data_len);
  }

  /************************************************************************
   * Convert data to a `dict`
   *
   */
  luns = (data_len - 8) / LUN_SIZE;

  if (verbosity > 0) {
    pr2ws("%s(): Found %d luns in %d bytes\n", __func__, luns, data_len - 8);
  }

  tuple = PyTuple_New(luns);
  if (!tuple) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new tuple" );
    return NULL;
  }

  /*
   * Currently only support non-hierarchical LUNs up to 256.
   * In this (easy) case, LUN is at byte 1 in the 8-bytes.
   */
  for (idex = 0, off = 8; idex < luns; idex++, off += LUN_SIZE) {
    int lun;

    lun = rbuf[off+1];

    PyTuple_SET_ITEM(tuple, idex, PyLong_FromLong(lun));
  }

  return tuple;
}

static char _doc_log_sense[] =
  "Returns the result of LOG_SENSE.\n"
  "See SCSI SPC-3 spec for more info:\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [page = 0] {int}       The target log page (0 == list of pages);\n"
  "  [subpage = 0] {int}    The target subpage;\n"
  "  [param  = 0] {int}     The target parameter;\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {bytearray} If `raw` is True;\n"
  "   {dict}      If `raw` is False;\n"
  "                 { disable_save  : (bool),\n"
  "                   subpage_format: (int),\n"
  "                   page          : (int),\n"
  "                   subpage       : (int),\n"
  "                   params        : (list of dicts),\n"
  "                 }\n"
  "\n"
  "   For page 0, subpage 0, each entry in `params` will be:\n"
  "     { page: (int) }\n"
  "\n"
  "   For page 0, subpage 0xff, each entry in `params` will be:\n"
  "     { page: (int), subpage: (int) }\n"
  "\n"
  "   For all other log pages, each entry in `params` will be:\n"
  "     { code                        : (int),\n"
  "       offset                      : (int),\n"
  "       disable_update              : (bool),\n"
  "       target_save_disable         : (bool),\n"
  "       enable_threshold_comparison : (bool),\n"
  "\n"
  "       threshold_met_criteria      : (int),\n"
  "       # 0x00  every update of the cumulative value;\n"
  "       # 0x01  Cumulative value  equal to      threshold value;\n"
  "       # 0x02  Cumulative value  not equal to  threshold value;\n"
  "       # 0x03  Cumulative value  greater than  threshold value;\n"
  "\n"
  "       format_and_linking          : (int),\n"
  "       # 0x00  Bounded data counter\n"
  "       # 0x01  ASCII format list\n"
  "       # 0x02  Bounded data counter or unbounded data counter\n"
  "       # 0x03  Binary format list\n"
  "\n"
  "       value_raw                   : (bytearray),\n"
  "       # Raw bytes of the parameter value\n"
  "\n"
  "       value                       : (str | int | bytearray),\n"
  "       # `value_raw` interpreted according to `format_and_linking`\n"
  "     }\n"
  "\n"
  "Example:\n"
  "  sg3utils.log_sense( fd ) ->\n"
  "    { disable_save:,\n"
  "      subpage_format:,\n"
  "      page:,\n"
  "      subpage:,\n"
  "      params:,\n"
  "    }\n";
static PyObject*
spc_log_sense(PyObject *self, PyObject *args, PyObject* kwargs) {

  /* SCSI SBC-3, Table 62: LOG SENSE command
   *
   *      Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *     Byte |     |     |     |     |     |     |     |     |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0   | Operation Code (4Dh)                          |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      1   | Reserved                          |Obslt| SP  |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      2   | PC        | Page Code                         |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      3   | Subpage Code                                  |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      4   | Reserved                                      |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      5   | Parameter Pointer                             |
   *      6   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      7   | Allocation Length                             |
   *      8   |                                               |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      9   | Control                                       |
   *    ------+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *  Saving Parameter (SP):
   *     0    Do not save any log parameters
   *     1    Save all log parameters identified as saveable by the DS bit to a
   *          nonvolatile, vendor specific location;
   *
   *  Page Control (PC):
   *    - data counter log parameters : specifies which log parameter values
   *                                    are to be returned;
   *    - list parameters             : ignored;
   *
   *  Parmeter Pointer:
   *    Allows the client to request parameter data beginning from a specific
   *    parameter code to the maximum allocation length or maximum parameter
   *    code supported, whichever is less;
   *
   *  Allocation Length:
   *    the maximum number of bytes allocated to receive parameter data;
   */
  PyObject*           handle    = NULL;
  struct sg_pt_base*  ptp       = NULL;
  int                 page      = 0;
  int                 subpage   = 0;
  bool                raw       = false;
  int                 verbosity = 0;
  int                 ppc       = 0;
  int                 sp        = 0;
  int                 pc        = 1;
  int                 paramp    = 0;
  int                 timeout   = 0;  // (default timeout)
  int                 resid     = 0;
  static char*        keys[]    = {"handle",
                                   "page", "subpage", "param",
                                   "raw", "verbosity", NULL};
  PyObject*           dict      = NULL;
  int                 ret;
#ifdef  LOG_SENSE_PROBE // {
  unsigned char       probeBuf[ LOG_SENSE_PROBE_ALLOC_LEN ];
  unsigned char*      rbuf      = NULL;
  int                 rbufLen   = sizeof(probeBuf);
#else   // !LOG_SENSE_PROBE }{
  unsigned char*      rbuf      = _rspBuf;
  int                 rbufLen   = sizeof(_rspBuf);
#endif  // LOG_SENSE_PROBE }

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|i$iipi:log_sense", keys,
                                   &handle, &page, &subpage, &paramp,
                                   &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  /* :XXX: PyArg_ParseTupleAndKeywords() seems to properly validate and extract
   *       all arguments EXCEPT the first integer keyword value (page) ALWAYS
   *       setting the value to 0.
   */
  if (kwargs && page == 0) {
    page = _get_page_val( kwargs );
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.log_sense()" );
    return NULL;
  }

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "page[ 0x%02x, %02x ]\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            page, subpage);
  }

#ifdef  LOG_SENSE_PROBE // {
  // First, probe for the length of the actual response
  ret = sg_ll_log_sense_pt(ptp,
                           ppc,           // bool     ppc
                           sp,            // bool     sp
                           pc,            // int      pc
                           page,          // int      pg_code
                           subpage,       // int      subpg_code
                           paramp,        // int      paramp
                           probeBuf,      // uint8_t* resp
                           rbufLen,       // int      mx_resp_len
                           timeout,       // int      timeout_secs
                           &resid,        // int*     residp
                           false,         // bool     noisy
                           verbosity);    // int      verbose
  if (ret) {
    pr2ws("%s(): LOG SENSE FAILED[ %d ]\n", __func__, ret);

    _pyErr( PyExc_IOError, ret, "SCSI LOG SENSE[ 0x%02x,%02x ] probe "
                                            "failed: %s",
            page, subpage, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }
  if (resid > 0) {
    // Did not receive the full data
    _pyErr( PyExc_IOError, 0, "request_len=%d, resid=%d, problems",
            rbufLen + resid, resid);
    return NULL;
  }

  rbufLen = sg_get_unaligned_be16( probeBuf + 2 ) + 4;
  if (verbosity > 1) {
    pr2ws("%s(): fd[ %d ], Log sense (find length): %d\n",
            __func__,
            ptp->impl.dev_fd,
            rbufLen);
  }
  if (page != (probeBuf[0] & 0x3f)) {
    if (verbosity) {
      pr2ws("%s(): fd[ %d ], Page code does not appear in the first "
                              "byte of response so it is suspect...\n",
              __func__,
              ptp->impl.dev_fd);
    }
    if (rbufLen > 0x40) {
      rbufLen = 0x40;
      if (verbosity) {
        pr2ws("%s(): fd[ %d ], Trim response length to 64 bytes due "
                                "to suspect response format\n",
                __func__,
                ptp->impl.dev_fd);

      }
    }
  }

  // Some HBAs don't like odd transfer lengths
  if (rbufLen % 2) { rbufLen++; }

  rbuf = malloc( rbufLen + 2 );
  if (!rbuf) {
    _pyErr( PyExc_IOError, errno,
              "cannot allocate %d bytes for log sense response", rbufLen);
    return NULL;
  }

  /**************************************************************************
   * Perform the log sense request for real
   *
   */
#endif  // LOG_SENSE_PROBE }
  ret = sg_ll_log_sense_pt(ptp,
                           ppc,           // bool     ppc
                           sp,            // bool     sp
                           pc,            // int      pc
                           page,          // int      pg_code
                           subpage,       // int      subpg_code
                           paramp,        // int      paramp
                           rbuf,          // uint8_t* resp
                           rbufLen,       // int      mx_resp_len
                           timeout,       // int      timeout_secs
                           &resid,        // int*     residp
                           false,         // bool     noisy
                           verbosity);    // int      verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret,
            "SCSI LOG SENSE[ 0x%02x,%02x ] failed: %s",
            page, subpage, _errStr( (errno ? errno : ret) ) );
    goto py_err;
  }
  if (resid > 0) {
    // Did not receive the full data
    rbufLen -= resid;

    if (rbufLen < 4) {
      _pyErr( PyExc_IOError, 0, "request_len=%d, resid=%d, problems",
              rbufLen + resid, resid);

      goto py_err;
    }
  }

  if (verbosity > 3) {
    int outLen  = rbufLen;
    if (verbosity < 6 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): Showing only the first %d bytes:\n", __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  if (raw) {
    // Do NOT interpret the data, rather return a raw bytearray
    unsigned int  data_len = sg_get_unaligned_be16( rbuf + 2 ) + 4;

    return PyByteArray_FromStringAndSize((const char *)rbuf, data_len);
  }

  // Parse the LOG SENSE data into a python dict
  dict = _parse_log_sense( rbuf, rbufLen );
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to parse LOG SENSE data" );
    goto py_err;
  }

#ifdef  LOG_SENSE_PROBE // {
  free(rbuf);
#endif  // LOG_SENSE_PROBE }

  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

#ifdef  LOG_SENSE_PROBE // {
  if (rbuf) { free(rbuf); }
#endif  // LOG_SENSE_PROBE }

  return NULL;
}

static char _doc_sat_identify[] =
  "Perform a SCSI ATA PASSTHROUGH to identify a device.\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {dict}  of the form:\n"
  "             { status {int},           SCSI status response code\n"
  "               result_category {int},  SCSI result category\n"
  "               sense_category {int},   SCSI sense category\n"
  "               sense {bytearray},      Raw SCSI sense data\n"
  "               raw {bytearray},        Raw ATA IDENTIFY data\n"
  "               smart_available {bool}, Is SMART available?\n"
  "               smart_enabled {bool},   Is SMART enabled?\n"
  "               serial {str},           The device serial id\n"
  "               revision {str},         The device firmware revision\n"
  "               product {str},          The device product/model name\n"
  "             }\n"
  "\n"
  "Example:\n"
  "  sg3utils.sat_identify( fd ) ->\n"
  "   { 'status': 0\n"
  "     'result_category': 0\n"
  "     'sense_category': 0\n"
  "     'sense': ...,\n"
  "     'raw': (raw bytes from the identify command)\n"
  "     'smart_available': True,\n"
  "     'smart_enabled': True,\n"
  "     'serial': 'SN1234567',\n"
  "     'revision': '1011',\n"
  "     'product': 'INTEL SSD12345',\n"
  "   }\n"
  "\n";
static PyObject*
spc_sat_identify(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle        = NULL;
  struct sg_pt_base*  ptp           = NULL;
  PyObject*           dict          = NULL;
  int                 verbosity     = 0;
  bool                raw           = false;
  uint8_t*            rbuf          = _rspBuf;
  int                 rbuf_len      = ATA_IDENTIFY_DATA_LEN;
  int                 ret_len       = 0;
  // ATA PASSTHROUGH {
  bool                do_chk_cond     = false;
  bool                do_extend       = false;
  int                 protocol        = 4;  // PIO data-in
  bool                from_dev        = true;
  bool                t_len_in_blocks = true;
  int                 t_len           = 2;  // 0 -> no data   2 -> sector count
  // ATA PASSTHROUGH }
  uint8_t             ata_cmd[ SAT_ATA_PASS_THROUGH16_LEN ] = { 0 };
  int                 ata_cmd_len   = sizeof(ata_cmd);
  char                str[128]      = {0};
  const int           str_len       = sizeof(str);
  uint8_t*            pbuf          = NULL;
  static char*        keys[]        = {"handle", "raw", "verbosity", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$pi:sat_identify", keys,
                                   &handle, &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.sat_identify()" );
    goto py_err;
  }

  /*  ATA 6.17.4 IDENTIFY DEVICE Inputs
   *
   *                   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *   Byte: Register      |     |     |     |     |     |     |     |     |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0: Features      |                                               |
   *      1:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      2: Sector Count  |                                               |
   *      3:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      4: LBA Low       |                                               |
   *      5:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      6: LBA Mid       |                                               |
   *      7:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      8: LBA High      |                                               |
   *      9:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *     10: Device        | obs |  na | obs | DEV |          na           |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *     11: Command       |               0xEC                            |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *  DEV must specify the selected device
   */
  ata_cmd[3]  = 1;                    // sector count (blocks)
  ata_cmd[11] = ATA_IDENTIFY_DEVICE;
  ata_cmd_len = 12;

  dict = _sat_ata_pt( ptp, do_chk_cond, do_extend, protocol, from_dev,
                      t_len_in_blocks, t_len,
                      ata_cmd, ata_cmd_len,
                      rbuf,    rbuf_len, &ret_len,
                      raw, verbosity );
  if (dict == NULL || raw) {
    return dict;
  }

  PyDict_SetItemString( dict, "raw",
                        PyByteArray_FromStringAndSize((const char *)rbuf,
                                                      ret_len) );

  // Check if results are all NULL (invalid)
  for (pbuf = rbuf; pbuf < rbuf + ret_len; pbuf++) {
    if (*pbuf) {
      pbuf = NULL;
      break;
    }
  }
  if (pbuf) {
    if (verbosity) {
      pr2ws("%s(): ATA IDENTIFY DEVICE returned no data\n", __func__);
    }

    _pyAddBool( dict, "smart_available",  false );
    _pyAddBool( dict, "smart_enabled",    false );

    return dict;
  }

  /***************************************************************************
   *  ATA Table 16 - IDENTIFY DEVICE data
   *    2-byte
   *    Word    O/M F/V Description
   *    0       M       General configuration bit-significant information:
   *                F     15    0 = ATA device
   *                X     14-8  Retired
   *                F     7     1 = removable media device
   *                X     6     Obsolete
   *                X     5-3   Retired
   *                V     2     Response incomplete
   *                X     1     Retired
   *                F     0     Reserved
   *    1       X       Obsolete
   *    2       O   V   Specific configuration
   *    3           X   Obsolete
   *    4-5         X   Retired
   *    6           X   Obsolete
   *    7-8     O   V   Reserved for assignment by the CompactFlash™
   *                    Association
   *    9           X   Retired
   *    10-19   M   F   Serial number (20 ASCII characters)
   *    20-21       X   Retired
   *    22          X   Obsolete
   *    23-26   M   F   Firmware revision (8 ASCII characters)
   *    27-46   M   F   Model number (40 ASCII characters)
   *    47      M   F     15-8  80h
   *                F     7-0   00h     = Reserved
   *                F           01h-FFh = Maximum number of sectors that shall
   *                                      be transferred per interrupt on
   *                                      READ/WRITE MULTIPLE commands
   *    48          F   Reserved
   *    ....
   *    82      M       Command set supported.
   *                X     15  Obsolete
   *                F     14  1 = NOP command supported
   *                F     13  1 = READ BUFFER command supported
   *                F     12  1 = WRITE BUFFER command supported
   *                X     11  Obsolete
   *                F     10  1 = Host Protected Area feature set supported
   *                F     9   1 = DEVICE RESET command supported
   *                F     8   1 = SERVICE interrupt supported
   *                F     7   1 = release interrupt supported
   *                F     6   1 = look-ahead supported
   *                F     5   1 = write cache supported
   *                F     4   Shall be cleared to zero to indicate that the
   *                          PACKET Command feature set is not supported.
   *                F     3   1 = mandatory Power Management feature set
   *                              supported
   *                F     2   1 = Removable Media feature set supported
   *                F     1   1 = Security Mode feature set supported
   *                F     0   1 = SMART feature set supported
   *    ....
   *    85      M       Command set/feature enabled.
   *                X     15  Obsolete
   *                F     14  1 = NOP command enabled
   *                F     13  1 = READ BUFFER command enabled
   *                F     12  1 = WRITE BUFFER command enabled
   *                X     11  Obsolete
   *                V     10  1 = Host Protected Area feature set enabled
   *                F     9   1 = DEVICE RESET command enabled
   *                V     8   1 = SERVICE interrupt enabled
   *                V     7   1 = release interrupt enabled
   *                V     6   1 = look-ahead enabled
   *                V     5   1 = write cache enabled
   *                F     4   Shall be cleared to zero to indicate that the
   *                          PACKET Command feature set is not supported.
   *                F     3   1 = Power Management feature set enabled
   *                F     2   1 = Removable Media feature set enabled
   *                V     1   1 = Security Mode feature set enabled
   *                V     0   1 = SMART feature set enabled
   */
  _pyAddWordAsUnsignedLong( dict, "config_general",   (uint16_t*)rbuf, 0 );
  _pyAddWordAsUnsignedLong( dict, "config_specific",  (uint16_t*)rbuf, 2 );

  // The bytes for string values must be swapped
  PyDict_SetItemString( dict, "serial",
                                _pyStringFromWords( (uint16_t*)rbuf + 10, 10,
                                                    (uint8_t*)str, str_len ) );
  PyDict_SetItemString( dict, "revision",
                                _pyStringFromWords( (uint16_t*)rbuf + 23, 4,
                                                    (uint8_t*)str, str_len ) );
  PyDict_SetItemString( dict, "product",
                                _pyStringFromWords( (uint16_t*)rbuf + 27, 20,
                                                    (uint8_t*)str, str_len ) );

  _pyAddBool( dict, "smart_available",  !!(rbuf[164] & 1 ));  // 82 * 2
  _pyAddBool( dict, "smart_enabled",    !!(rbuf[170] & 1 ));  // 85 * 2

  /*  ATA Table 16 - IDENTIFY DEVICE data
   *    Word    O/M F/V Description
   *     49     M       Capabilities
   *                F     15-14 Reserved for the IDENTIFY PACKET DEVICE command.
   *                F     13    1 = Standby timer values as specified in this
   *                                standard are supported
   *                            0 = Standby timer values shall be managed by the
   *                                device
   *                F     12    Reserved for the IDENTIFY PACKET DEVICE
   *                            command.
   *                F     11    1 = IORDY supported
   *                            0 = IORDY may be supported
   *                F     10    1 = IORDY may be disabled
   *                F     9     1 = LBA supported
   *                F     8     1 = DMA supported.
   *                X     7-0   Retired
   *     50     M       Capabilities
   *                F     15    Shall be cleared to zero.
   *                F     14    Shall be set to one.
   *                F     13-2  Reserved.
   *                X     1     Obsolete
   *                F     0     Shall be set to one to indicate a device
   *                            specific Standby timer value minimum.
   *     51-52      X   Obsolete
   *     53     M   F     15-3  Reserved
   *                F     2     1 = the fields reported in word 88 are valid
   *                            0 = the fields reported in word 88 are not
   *                                valid
   *                F     1     1 = the fields reported in words (70:64) are
   *                                valid
   *                            0 = the fields reported in words (70:64) are
   *                                not valid
   *                X     0 Obsolete
   *
   *    Word    O/M F/V Description
   *     54-58      X   Obsolete
   *     59     M   F     15-9  Reserved
   *                V     8     1   = Multiple sector setting is valid
   *                V     7-0   xxh = Current setting for number of sectors
   *                                  that shall be transferred per interrupt
   *                                  on R/W Multiple command
   *     60-61  M   F   Total number of user addressable sectors
   *     62         X   Obsolete
   *     63     M   F     15-11 Reserved
   *                V     10    1 = Multiword DMA mode 2 is selected
   *                            0 = Multiword DMA mode 2 is not selected
   *                V     9     1 = Multiword DMA mode 1 is selected
   *                            0 = Multiword DMA mode 1 is not selected
   *                V     8     1 = Multiword DMA mode 0 is selected
   *                            0 = Multiword DMA mode 0 is not selected
   *                F     7-3   Reserved
   *                F     2     1 = Multiword DMA mode 2 and below are
   *                                supported
   *                F     1     1 = Multiword DMA mode 1 and below are
   *                                supported
   *                F     0     1 = Multiword DMA mode 0 is supported
   *     64     M   F     15-8  Reserved
   *                F     7-0   PIO modes supported
   *     65     M       Minimum Multiword DMA transfer cycle time per word
   *                F     15-0  Cycle time in nanoseconds
   *     66     M       Manufacturer’s recommended Multiword DMA transfer cycle
   *                    time
   *                F     15-0  Cycle time in nanoseconds
   *     67     M       Minimum PIO transfer cycle time without flow control
   *                F     15-0  Cycle time in nanoseconds
   *     68     M       Minimum PIO transfer cycle time with IORDY flow control
   *                F     15-0  Cycle time in nanoseconds
   *     69-70      F   Reserved (for future command overlap and queuing)
   *     71-74      F   Reserved for the IDENTIFY PACKET DEVICE command.
   *
   *    Word    O/M F/V Description
   *     75     O       Queue depth
   *                F     15-5  Reserved
   *                F     4-0   Maximum queue depth - 1
   *     76-79      F   Reserved for Serial ATA
   *     80     M       Major version number
   *                    0000h or FFFFh = device does not report version
   *                F     15  Reserved
   *                F     14  Reserved for ATA/ATAPI-14
   *                F     13  Reserved for ATA/ATAPI-13
   *                F     12  Reserved for ATA/ATAPI-12
   *                F     11  Reserved for ATA/ATAPI-11
   *                F     10  Reserved for ATA/ATAPI-10
   *                F     9   Reserved for ATA/ATAPI-9
   *                F     8   Reserved for ATA/ATAPI-8
   *                F     7   1 = supports ATA/ATAPI-7
   *                F     6   1 = supports ATA/ATAPI-6
   *                F     5   1 = supports ATA/ATAPI-5
   *                F     4   1 = supports ATA/ATAPI-4
   *                F     3   Obsolete
   *                X     2   Obsolete
   *                X     1   Obsolete
   *                F     0   Reserved
   *     81     M   F   Minor version number
   *                    0000h or FFFFh  = device does not report version
   *                    0001h-FFFEh     = See 6.17.41
   *    ....
   *
   *    Word    O/M F/V Description
   *     83     M       Command sets supported.
   *                F     15  Shall be cleared to zero
   *                F     14  Shall be set to one
   *                F     13  1 = FLUSH CACHE EXT command supported
   *                F     12  1 = mandatory FLUSH CACHE command supported
   *                F     11  1 = Device Configuration Overlay feature set
   *                              supported
   *                F     10  1 = 48-bit Address feature set supported
   *                F     9   1 = Automatic Acoustic Management feature set
   *                              supported
   *                F     8   1 = SET MAX security extension supported
   *                F     7   See Address Offset Reserved Area Boot, INCITS
   *                          TR27:2001
   *                F     6   1 = SET FEATURES subcommand required to spinup
   *                              after power-up
   *                F     5   1 = Power-Up In Standby feature set supported
   *                F     4   1 = Removable Media Status Notification feature
   *                              set supported
   *                F     3   1 = Advanced Power Management feature set
   *                              supported
   *                F     2   1 = CFA feature set supported
   *                F     1   1 = READ/WRITE DMA QUEUED supported
   *                F     0   1 = DOWNLOAD MICROCODE command supported
   *     84     M       Command set/feature supported extension.
   *                F     15  Shall be cleared to zero
   *                F     14  Shall be set to one
   *                F     13  1 = IDLE IMMEDIATE with UNLOAD FEATURE supported
   *                F     12  Reserved for technical report
   *                F     11  Reserved for technical report
   *                F     10  1 = URG bit supported for WRITE STREAM DMA EXT
   *                              and WRITE STREAM EXT
   *                F     9   1 = URG bit supported for READ STREAM DMA EXT and
   *                              READ STREAM EXT
   *                F     8   1 = 64-bit World wide name supported
   *                F     7   1 = WRITE DMA QUEUED FUA EXT command supported
   *                F     6   1 = WRITE DMA FUA EXT and WRITE MULTIPLE FUA EXT
   *                              commands supported
   *                F     5   1 = General Purpose Logging feature set supported
   *                F     4   1 = Streaming feature set supported
   *                F     3   1 = Media Card Pass Through Command feature set
   *                              supported
   *                F     2   1 = Media serial number supported
   *                F     1   1 = SMART self-test supported
   *                F     0   1 = SMART error logging supported
   *    ....
   *
   *    Word    O/M F/V Description
   *     86     M       Command set/feature enabled.
   *                F     15-14 Reserved
   *                F     13    1 = FLUSH CACHE EXT command supported
   *                F     12    1 = FLUSH CACHE command supported
   *                F     11    1 = Device Configuration Overlay supported
   *                F     10    1 = 48-bit Address features set supported
   *                V     9     1 = Automatic Acoustic Management feature set
   *                                enabled
   *                F     8     1 = SET MAX security extension enabled by SET
   *                                MAX SET PASSWORD
   *                F     7     See Address Offset Reserved Area Boot, INCITS
   *                            TR27:2001
   *                F     6     1 = SET FEATURES subcommand required to spin-up
   *                                after power-up
   *                V     5     1 = Power-Up In Standby feature set enabled
   *                V     4     1 = Removable Media Status Notification feature
   *                                set enabled
   *                V     3     1 = Advanced Power Management feature set
   *                                enabled
   *                F     2     1 = CFA feature set enabled
   *                F     1     1 = READ/WRITE DMA QUEUED command supported
   *                F     0     1 = DOWNLOAD MICROCODE command supported
   *
   *    Word    O/M F/V Description
   *     87     M       Command set/feature default.
   *                F     15    Shall be cleared to zero
   *                F     14    Shall be set to one
   *                F     13    1 = IDLE IMMEDIATE with UNLOAD FEATURE supported
   *                V     12    Reserved for technical report-
   *                V     11    Reserved for technical report-
   *                F     10    1 = URG bit supported for WRITE STREAM DMA EXT
   *                                and WRITE STREAM EXT
   *                F     9     1 = URG bit supported for READ STREAM DMA EXT
   *                                and READ STREAM EXT
   *                F     8     1 = 64 bit World wide name supported
   *                F     7     1 = WRITE DMA QUEUED FUA EXT command supported
   *                F     6     1 = WRITE DMA FUA EXT and WRITE MULTIPLE FUA
   *                                EXT commands supported
   *                F     5     1 = General Purpose Logging feature set
   *                                supported
   *                V     4     1 = Valid CONFIGURE STREAM command has been
   *                                executed
   *                V     3     1 = Media Card Pass Through Command feature set
   *                                enabled
   *                V     2     1 = Media serial number is valid
   *                F     1     1 = SMART self-test supported
   *                F     0     1 = SMART error logging supported
   *     88     O   F     15    Reserved
   *                V     14    1 = Ultra DMA mode 6 is selected
   *                            0 = Ultra DMA mode 6 is not selected
   *                V     13    1 = Ultra DMA mode 5 is selected
   *                            0 = Ultra DMA mode 5 is not selected
   *                V     12    1 = Ultra DMA mode 4 is selected
   *                            0 = Ultra DMA mode 4 is not selected
   *                V     11    1 = Ultra DMA mode 3 is selected
   *                            0 = Ultra DMA mode 3 is not selected
   *                V     10    1 = Ultra DMA mode 2 is selected
   *                            0 = Ultra DMA mode 2 is not selected
   *                V     9     1 = Ultra DMA mode 1 is selected
   *                            0 = Ultra DMA mode 1 is not selected
   *                V     8     1 = Ultra DMA mode 0 is selected
   *                            0 = Ultra DMA mode 0 is not selected
   *                F     7     Reserved
   *                F     6     1 = Ultra DMA mode 6 and below are supported
   *                F     5     1 = Ultra DMA mode 5 and below are supported
   *                F     4     1 = Ultra DMA mode 4 and below are supported
   *                F     3     1 = Ultra DMA mode 3 and below are supported
   *                F     2     1 = Ultra DMA mode 2 and below are supported
   *                F     1     1 = Ultra DMA mode 1 and below are supported
   *                F     0     1 = Ultra DMA mode 0 is supported
   *
   *    Word    O/M F/V Description
   *     89     O   F   Time required for security erase unit completion
   *     90     O   F   Time required for Enhanced security erase completion
   *     91     O   V   Current advanced power management value
   *     92     O   V   Master Password Revision Code
   *     93     *       Hardware reset result. The contents of bits (12:0) of
   *                    this word shall change only during the execution of a
   *                    hardware reset.
   *                F     15    Shall be cleared to zero.
   *                F     14    Shall be set to one.
   *                V     13    1 = device detected CBLID- above ViH
   *                            0 = device detected CBLID- below ViL
   *                      12-8  Device 1 hardware reset result. Device 0 shall
   *                            clear these bits to zero. Device 1 shall set
   *                            these bits as follows:
   *                F             12    Reserved.
   *                V             11    0 = Device 1 did not assert PDIAG-.
   *                                    1 = Device 1 asserted PDIAG-.
   *                V             10-9  These bits indicate how Device 1
   *                                    determined the device number:
   *                                      00 = Reserved.
   *                                      01 = a jumper was used.
   *                                      10 = the CSEL signal was used.
   *                                      11 = some other method was used or
   *                                           the method is unknown.
   *
   *                              8     Shall be set to one.
   *                      7-0   Device 0 hardware reset result. Device 1 shall
   *                            clear these bits to zero.  Device 0 shall set
   *                            these bits as follows:
   *                F             7     Reserved.
   *                F             6     0 = Device 0 does not respond when
   *                                        Device 1 is selected.
   *                                    1 = Device 0 responds when Device 1 is
   *                                        selected.
   *                V             5     0 = Device 0 did not detect the
   *                                        assertion of DASP-.
   *                                    1 = Device 0 detected the assertion of
   *                                        DASP-.
   *                V             4     0 = Device 0 did not detect the
   *                                        assertion of PDIAG-.
   *                                    1 = Device 0 detected the assertion of
   *                                        PDIAG-.
   *                V             3     0 = Device 0 failed diagnostics.
   *                                    1 = Device 0 passed diagnostics.
   *                V             2-1   These bits indicate how Device 0
   *                                    determined the device number:
   *                                      00 = Reserved.
   *                                      01 = a jumper was used.
   *                                      10 = the CSEL signal was used.
   *                                      11 = some other method was used or
   *                                           the method is unknown.
   *                F             0     Shall be set to one.
   *
   *    Word    O/M F/V Description
   *     94     O   V     15-8  Vendor’s recommended acoustic management value.
   *                V     7-0   Current automatic acoustic management value.
   *     95         F   Stream Minimum Request Size
   *     96         V   Streaming Transfer Time - DMA
   *     97         V   Streaming Access Latency - DMA and PIO
   *     98-99      F   Streaming Performance Granularity
   *     100-103 O  V   Maximum user LBA for 48-bit Address feature set.
   *     104    O   V   Streaming Transfer Time - PIO
   *     105        F   Reserved
   *     106        O   Physical sector size / Logical Sector Size
   *                F     15    Shall be cleared to zero
   *                F     14    Shall be set to one
   *                F     13    1 = Device has multiple logical sectors per
   *                                physical sector.
   *                      12    1 = Device Logical Sector Longer than 256 Words
   *                F     11-4  Reserved
   *                F     3-0   2**X logical sectors per physical sector
   *     107    O   F   Inter-seek delay for ISO-7779 acoustic testing in
   *                    microseconds
   *     108    O   F     15-12 NAA (3:0)
   *                      11-0  IEEE OUI (23:12)
   *     109    O   F     15-4  IEEE OUI (11:0)
   *                      3-0   Unique ID (35:32)
   *     110    O   F     15-0  Unique ID (31:16)
   *     111    O   F     15-0  Unique ID (15:0)
   *     112-115 O  F   Reserved for world wide name extension to 128 bits
   *     116    O   V   Reserved for technical report
   *     117-118 O  F   Words per Logical Sector
   *     119-126    F   Reserved
   *     127    O       Removable Media Status Notification feature set support
   *                F     15-2  Reserved
   *                F     1-0   00 =  Removable Media Status Notification
   *                                  feature set not supported
   *                            01 =  Removable Media Status Notification
   *                                  feature supported
   *                            10 =  Reserved
   *                            11 =  Reserved
   *
   *    Word    O/M F/V Description
   *     128    O       Security status
   *                F     15-9  Reserved
   *                V     8     Security level 0 = High, 1 = Maximum
   *                F     7-6   Reserved
   *                F     5     1 = Enhanced security erase supported
   *                V     4     1 = Security count expired
   *                V     3     1 = Security frozen
   *                V     2     1 = Security locked
   *                V     1     1 = Security enabled
   *                F     0     1 = Security supported
   *     129-159    X   Vendor specific
   *     160    O       CFA power mode 1
   *                F     15    Word 160 supported
   *                F     14    Reserved
   *                F     13    CFA power mode 1 is required for one or more
   *                            commands implemented by the device
   *                V     12    CFA power mode 1 disabled
   *                F     11-0  Maximum current in ma
   *     161-175    X   Reserved for assignment by the CompactFlash™
   *                    Association
   *     176-205 O  V   Current media serial number
   *     206-254    F   Reserved
   *     255    M   X   Integrity word
   *                      15-8  Checksum
   *                      7-0   Signature
   *
   *     Key:
   *      O/M = Mandatory/optional requirement.
   *            M = Support of the word is mandatory.
   *            O = Support of the word is optional.
   *            * = See 6.17.49.
   *      F/V = Fixed/variable content
   *            F = the content of the word is fixed and does not change. For
   *                removable media devices, these values may change when media
   *                is removed or changed.
   *            V = the contents of the word is variable and may change
   *                depending on the state of the device or the commands
   *                executed by the device.
   *            X = the content of the word may be fixed or variable.
   */
  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

  return NULL;
}

static char _doc_sat_smart_data[] =
  "Perform a SCSI ATA PASSTHROUGH to retrieve SMART data.\n"
  "\n"
  "Args:\n"
  "  handle {int}           The handle returned from sg3utils.open();\n"
  "  [raw = False] {bool}   If True, return raw data;\n"
  "  [verbosity = 0] {int}  Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {dict}  of the form:\n"
  "             { status {int},           SCSI status response code\n"
  "               result_category {int},  SCSI result category\n"
  "               sense_category {int},   SCSI sense category\n"
  "               sense {bytearray},      Raw SCSI sense data\n"
  "               raw {bytearray},        Raw SCSI result data\n"
  "               offline_data_collection_status {int},\n"
  "               self_test_execution_status {int},\n"
  "               offline_data_collection_capability {int},\n"
  "               smart_capability {int},\n"
  "               error_logging_capability {int},\n"
  "               short_self_test_recommended_poll_time {int},\n"
  "               extended_self_test_recommended_poll_time {int},\n"
  "               conveyance_self_test_recommended_poll_time {int},\n"
  "               checkxum {int},\n"
  "               attrs {list},\n"
  "             }\n"
  "\n"
  "   Each entry in `attrs` will have the form:\n"
  "             { id {int},               The SMART id of this attribute\n"
  "               flags {int},            Attribute flags\n"
  "               value_current {int},    The current value\n"
  "               value_worst {int},      The worst value\n"
  "               prefailure {bool},      Is this attribute prefail?\n"
  "               online {bool},          Is this attribute online?\n"
  "               value_raw {bytearray},  The raw value of the attribute\n"
  "             }\n"
  "\n"
  "Example:\n"
  "  sg3utils.sat_smart_data( fd ) ->\n"
  "   { 'status': 0\n"
  "     'result_category': 0\n"
  "     'sense_category': 0\n"
  "     'sense': ...,\n"
  "     'raw': b'<raw-result-data>'\n"
  "     ...\n"
  "   }\n"
  "\n";
static PyObject*
spc_sat_smart_data(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle        = NULL;
  struct sg_pt_base*  ptp           = NULL;
  PyObject*           dict          = NULL;
  PyObject*           list          = NULL;
  int                 verbosity     = 0;
  bool                raw           = false;
  uint8_t*            rbuf          = _rspBuf;
  int                 rbuf_len      = ATA_SMART_READ_DATA_LEN;
  int                 ret_len       = 0;
  // ATA PASSTHROUGH {
  bool                do_chk_cond     = false;
  bool                do_extend       = false;
  int                 protocol        = 4;  // PIO data-in
  bool                from_dev        = true;
  bool                t_len_in_blocks = true;
  int                 t_len           = 2;  // 0 -> no data   2 -> sector count
  // ATA PASSTHROUGH }
  uint8_t             ata_cmd[ SAT_ATA_PASS_THROUGH16_LEN ] = { 0 };
  int                 ata_cmd_len   = sizeof(ata_cmd);
  static char*        keys[]        = {"handle", "raw", "verbosity", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$pi:sat_smart_data", keys,
                                   &handle, &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.sat_smart_data()" );
    goto py_err;
  }

  /*  ATA 6.54.5 SMART READ DATA
   *
   *                   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *   Byte: Register      |     |     |     |     |     |     |     |     |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      0: Features      |                                               |
   *      1:               |               0xD0                            |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      2: Sector Count  |                                               |
   *      3:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      4: LBA Low       |                                               |
   *      5:               |                na                             |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      6: LBA Mid       |                                               |
   *      7:               |               0x4F                            |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *      8: LBA High      |                                               |
   *      9:               |               0xC2                            |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *     10: Device        | obs |  na | obs | DEV |          na           |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   *     11: Command       |               0xB0                            |
   *  ---------------------+-----+-----+-----+-----+-----+-----+-----+-----+
   */
  ata_cmd[1]  = ATA_SMART_READ_DATA;  // feature (7:0)
  ata_cmd[3]  = 1;                    // sector count (blocks)
  ata_cmd[7]  = 0x4f;                 // lba_mid (7:0)
  ata_cmd[9]  = 0xc2;                 // lba_high(7:0)
  ata_cmd[11] = ATA_SMART;
  ata_cmd_len = 12;

  dict = _sat_ata_pt( ptp, do_chk_cond, do_extend, protocol, from_dev,
                      t_len_in_blocks, t_len,
                      ata_cmd, ata_cmd_len,
                      rbuf,    rbuf_len, &ret_len,
                      raw, verbosity );
  if (dict == NULL || raw) {
    return dict;
  }

  PyDict_SetItemString( dict, "raw",
                        PyByteArray_FromStringAndSize((const char *)rbuf,
                                                      ret_len) );

  /*  ATA Table 49 - Device SMART data structure
   *    Byte    F/V Descriptions
   *    0-361   X   Vendor specific
   *    362     V   Off-line data collection status
   *    363     X   Self-test execution status byte
   *    364-365 V   Total time in seconds to complete off-line data collection
   *                activity
   *    366     X   Vendor specific
   *    367     F   Off-line data collection capability
   *    368-369 F   SMART capability
   *    370     F   Error logging capability
   *                  7-1 Reserved
   *                   0  1 = Device error logging supported
   *    371     X   Vendor specific
   *    372     F   Short self-test routine recommended polling time
   *                (in minutes)
   *    373     F   Extended self-test routine recommended polling time
   *                (in minutes)
   *    374     F   Conveyance self-test routine recommended polling time
   *                (in minutes)
   *
   *    375-385 R   Reserved
   *    386-510 X   Vendor specific
   *    511     V   Data structure checksum
   *
   *  Key:
   *    F = the content of the byte is fixed and does not change.
   *    V = the content of the byte is variable and may change depending on the
   *        state of the device or the commands executed by the device.
   *    X = the content of the byte is vendor specific and may be fixed or
   *        variable.
   *    R = the content of the byte is reserved and shall be zero.
   *
   *
   *  ATA Table 50 - Off-line data collection status byte values
   *    Value     Definition
   *    00h | 80h Off-line data collection activity was never started.
   *    01h       Reserved
   *    02h | 82h Off-line data collection activity was completed without error.
   *    03h       Off-line activity in progress.
   *    04h | 84h Off-line data collection activity was suspended by an
   *              interrupting command from host.
   *    05h | 85h Off-line data collection activity was aborted by an
   *              interrupting command from host.
   *    06h | 86h Off-line data collection activity was aborted by the device
   *              with a fatal error.
   *    07h-3Fh   Reserved
   *    40h-7Fh   Vendor specific
   *    81h       Reserved
   *    83h       Reserved
   *    87h-BFh   Reserved
   *    C0h-FFh   Vendor specific
   *
   *
   *  ATA Table 51 - Self-test execution status values
   *    Value Description
   *    0     The previous self-test routine completed without error or no
   *          self-test has ever been run
   *    1     The self-test routine was aborted by the host
   *    2     The self-test routine was interrupted by the host with a hardware
   *          or software reset
   *    3     A fatal error or unknown test error occurred while the device was
   *          executing its self-test routine and the device was unable to
   *          complete the self-test routine.
   *    4     The previous self-test completed having a test element that
   *          failed and the test element that failed is not known.
   *    5     The previous self-test completed having the electrical element of
   *          the test failed.
   *    6     The previous self-test completed having the servo (and/or seek)
   *          test element of the test failed.
   *    7     The previous self-test completed having the read element of the
   *          test failed.
   *    8     The previous self-test completed having a test element that
   *          failed and the device is suspected of having handling damage.
   *    9-14  Reserved.
   *    15    Self-test routine in progress.
   */
  PyDict_SetItemString( dict, "offline_data_collection_status",
                                PyLong_FromLong( rbuf[362] ) );
  PyDict_SetItemString( dict, "self_test_execution_status",
                                PyLong_FromLong( rbuf[363] ) );
  PyDict_SetItemString( dict, "offline_data_collection_capability",
                                PyLong_FromLong( rbuf[367] ) );
  PyDict_SetItemString( dict, "smart_capability",
                                PyLong_FromLong((rbuf[368] << 8) |
                                                 rbuf[369]) );
  PyDict_SetItemString( dict, "error_logging_capability",
                                PyLong_FromLong( rbuf[370] ) );
  PyDict_SetItemString( dict, "short_self_test_recommended_poll_time",
                                PyLong_FromLong( rbuf[372] ) );
  PyDict_SetItemString( dict, "extended_self_test_recommended_poll_time",
                                PyLong_FromLong( rbuf[373] ) );
  PyDict_SetItemString( dict, "conveyance_self_test_recommended_poll_time",
                                PyLong_FromLong( rbuf[374] ) );
  PyDict_SetItemString( dict, "checksum",
                                PyLong_FromLong( rbuf[511] ) );

  // Extract SMART attributes
  list = _extract_smart_attrs( rbuf, ret_len );
  if (list == NULL) {
    // _extract_log_params() should have called _pyErr()
    goto py_err;
  }

  // Assign the attribute list
  PyDict_SetItemString( dict, "attrs", list);

  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

  return NULL;
}

static char _doc_raw[] =
  "Sends between 6 and 256 SCSI command bytes returning the response.\n"
  "\n"
  "Args:\n"
  "  handle {int}                 The handle returned from sg3utils.open();\n"
  "  cmd {bytearray}              The raw SCSI command bytes to send\n"
  "                               between 6 and 256 bytes, may be a CDB;\n"
  "  [response_max = 1024] {int}  The maximum number of bytes to return;\n"
  "  [data = None] {bytearray}    Additional data to send;\n"
  "  [verbosity = 0] {int}        Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {dict}  of the form:\n"
  "             { status {int},           SCSI status response code\n"
  "               result_category {int},  SCSI result category\n"
  "               sense_category {int},   SCSI sense category\n"
  "               sense {bytearray},      Raw SCSI sense data\n"
  "               result {bytearray},     Raw SCSI result data\n"
  "             }\n"
  "\n"
  "Example:\n"
  "  sg3utils.raw( fd, b'\x4d\x00\x4d\x00\x00\x00\x00\xc0\x82\x00' ) ->\n"
  "   { 'status': 0\n"
  "     'result_category': 0\n"
  "     'sense_category': 0\n"
  "     'sense': ...,\n"
  "     'result': b'\x0d\x00\x00\x0c\x00\x00\x03 \x2\x00\x1d\x00\x01\x03\x02\x00\x3c'\n"
  "   }\n"
  "\n";
static PyObject*
spc_raw(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle        = NULL;
  struct sg_pt_base*  ptp           = NULL;
  PyByteArrayObject*  cmd           = NULL;
  PyByteArrayObject*  data          = NULL;
  PyObject*           dict          = NULL;
  int                 response_max  = 1024;
  int                 verbosity     = 0;
  bool                is_cdb        = true;
  uint8_t*            rbuf          = _rspBuf;
  int                 rbuf_len      = sizeof(_rspBuf);
  int                 ret_len       = 0;
  uint8_t*            buf           = NULL;
  int                 buf_len       = 0;
  uint8_t             sense_buf[32] = {0};
  int                 sense_len     = sizeof(sense_buf);
  int                 ret           = 0;
  char                str[128]      = {0};
  const int           str_len       = sizeof(str);
  int                 res_cat       = 0;
  int                 sense_cat     = 0;
  int                 status        = 0;
  static char*        keys[]        = {"handle",
                                       "cmd", "data",
                                       "response_max", "verbosity", NULL};

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OY|$Yii:raw", keys,
                                   &handle, &cmd, &data,
                                   &response_max, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.raw()" );
    goto py_err;
  }

  if (response_max > rbuf_len) {
    // ... Allocate space for a larger response
    rbuf_len = response_max;
    rbuf     = malloc( rbuf_len + 2 );
    if (!rbuf) {
      _pyErr( PyExc_IOError, errno,
                "cannot allocate %d bytes for raw response", rbuf_len);
      return NULL;
    }
  }

  // Establish the command / CDB
  buf     = (uint8_t*)PyByteArray_AsString( (PyObject*)cmd );
  buf_len = (int     )PyByteArray_Size(     (PyObject*)cmd );

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "%d byte CDB:\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            buf_len);

    hex2stdout( buf, buf_len, true /* no_ascii */ );
  }

  is_cdb = sg_is_scsi_cdb( buf, buf_len );
  if (! is_cdb) { // if not CDB, NVME command
    _pyErr( PyExc_IOError, 0, "Invalid %d byte CDB", buf_len );
    goto py_err;
  }

  // Ensure `ptp` is ready for re-uset
  clear_scsi_pt_obj( ptp );

  set_scsi_pt_cdb( ptp, buf, buf_len );
  set_scsi_pt_data_in( ptp, rbuf, rbuf_len );

  // If additional send data was provided, put it into place
  if (data) {
    buf     = (uint8_t*)PyByteArray_AsString( (PyObject*)data );
    buf_len = (int     )PyByteArray_Size(     (PyObject*)data );

    if (verbosity) {
      pr2ws("%s(): With %d bytes of data:\n", __func__, buf_len);
      hex2stdout( buf, buf_len, true /* no_ascii */ );
    }

    set_scsi_pt_data_out( ptp, buf, buf_len );
  }

  // Initialize the sense buffer
  set_scsi_pt_sense( ptp, sense_buf, sizeof(sense_buf) );

  // Send the command
  ret = do_scsi_pt(ptp,
                   -1,          // int fd
                   0,           // int timeout_secs
                   verbosity);  // int verbose
  if (verbosity > 2) {
    pr2ws("%s(): SCSI RAW Return code: %d\n", __func__, ret);
  }

  if (ret) {
    switch (ret) {
      case SCSI_PT_DO_BAD_PARAMS:
        _pyErr( PyExc_IOError, ret,
                "SCSI RAW pass-thru failed: Bad parameters" );
        break;

      case SCSI_PT_DO_TIMEOUT:
        _pyErr( PyExc_IOError, ret,
                "SCSI RAW pass-thru failed: Timeout" );
        break;

      //case SCSI_PT_DO_NVME_STATUS:
      default:
        if (ret < 0) {
          _pyErr( PyExc_IOError, ret,
                  "SCSI RAW pass-thru failed: %s",
                  _errStr( ret ));

          /*
          int err = get_scsi_pt_os_err( ptp );
          if (err != abRet) {
            " ... or perhaps: %s", safe_strerr(err)
          }
          ret = sg_convert_errno( err );
          // */

        } else {

          _pyErr( PyExc_IOError, ret,
                  "SCSI RAW pass-thru failed: %s",
                  _errStr( (errno ? errno : ret) ) );
        }
        break;
    }

    goto py_err;
  }

  dict = PyDict_New();
  if (!dict) {
    _pyErr( PyExc_IOError, errno, "Failed to create a new dict" );
    goto py_err;
  }

  ret_len   = rbuf_len - get_scsi_pt_resid( ptp );

  if (verbosity) {
    pr2ws("%s(): Requested %d bytes, got %d\n",
          __func__, rbuf_len, ret_len);
  }

  status    = get_scsi_pt_status_response( ptp );
  res_cat   = get_scsi_pt_result_category( ptp );

  sense_len = get_scsi_pt_sense_len( ptp );
  if (sense_len > 2) {
    sense_cat = sg_err_category_sense( sense_buf, sense_len );
  }

  switch( status ) {
    case SAM_STAT_CHECK_CONDITION:
      if (verbosity) {
        if (sense_len == 0) {
          pr2ws("%s(): SCSI RAW Status: strange -- CHECK CONDITION but no "
                        "sense information returned\n",
                __func__);
        }
      }
      break;

    case SAM_STAT_RESERVATION_CONFLICT:
      ret = -1;
      if (verbosity) {
        pr2ws("%s(): SCSI RAW Status: reservation conflict", __func__);
      }
      break;

    default:
      if (verbosity) {
        sg_get_scsi_status_str( status, str_len, str );

        pr2ws("%s(): SCSI RAW Status: %s\n", __func__, str);
      }
      break;
  }

  switch (res_cat) {
    case SCSI_PT_RESULT_GOOD:
      ret = 0;
      if (verbosity) {
        pr2ws("%s(): SCSI RAW Result: Good\n", __func__);
      }
      break;

    case SCSI_PT_RESULT_STATUS:
      if (verbosity) {
        pr2ws("%s(): SCSI RAW Result: Status\n", __func__);
      }
      break;

    case SCSI_PT_RESULT_SENSE:
      ret = sense_cat;
      if (verbosity) {
        sg_get_category_sense_str( sense_cat, str_len, str, verbosity );

        pr2ws("%s(): SCSI RAW Result: %s\n", __func__, str);
      }
      break;

    case SCSI_PT_RESULT_TRANSPORT_ERR:
      ret = -1;
      if (verbosity) {
        get_scsi_pt_transport_err_str( ptp, str_len, str );

        pr2ws("%s(): SCSI RAW Result: transport error: %s", __func__, str);
      }
      break;

    case SCSI_PT_RESULT_OS_ERR:
      ret = -1;
      if (verbosity) {
        get_scsi_pt_os_err_str( ptp, str_len, str );

        pr2ws("%s(): SCSI RAW Result: os error: %s", __func__, str);
      }
      break;

    default:
      ret = -1;
      if (verbosity) {
        pr2ws("%s(): SCSI RAW Result: unknown pass through category [ %d ]\n",
              __func__, res_cat);
      }
      break;
  }

  if (verbosity) {
    pr2ws("%s(): SCSI RAW Sense :", __func__);
    if (sense_len == 0) {
      pr2ws(" <EMPTY>\n");

    } else {
      pr2ws("\n");
      sg_print_sense( NULL, sense_buf, sense_len, verbosity );
    }
  }

  if (ret && !(ret == SG_LIB_CAT_RECOVERED ||
               ret == SG_LIB_CAT_NO_SENSE)) {
    ret_len = 0;

    if (verbosity) {
      pr2ws("%s(): SCSI RAW No data received due to error[ %d ]\n",
            __func__, ret);
    }
  }

  if (verbosity) {
    int outLen  = ret_len;
    if (verbosity < 3 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): SCSI RAW Showing only the first %d bytes:\n",
            __func__, outLen);

    } else {
      pr2ws("%s(): SCSI RAW %d byte result:\n",
            __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  PyDict_SetItemString( dict, "status",          PyLong_FromLong( status ));
  PyDict_SetItemString( dict, "result_category", PyLong_FromLong( res_cat ));
  PyDict_SetItemString( dict, "sense_category",  PyLong_FromLong( sense_cat ));
  PyDict_SetItemString( dict, "sense",
                        PyByteArray_FromStringAndSize((const char *)sense_buf,
                                                      sense_len) );
  PyDict_SetItemString( dict, "result",
                        PyByteArray_FromStringAndSize((const char *)rbuf,
                                                      ret_len) );

  if (rbuf && rbuf != _rspBuf) { free(rbuf); }

  return dict;

py_err:
  if (dict) { Py_XDECREF(dict); }

  if (rbuf && rbuf != _rspBuf) { free(rbuf); }

  return NULL;
}

static char _doc_recv_diag[] =
  "Returns the result of RECEIVE DIAGNOSTIC RESULTS, as a byte array\n"
  "See SCSI SPC-3 spec for more info.\n"
  "\n"
  "Args:\n"
  "  handle {int}                 The handle returned from sg3utils.open();\n"
  "  [page = None] {int}          The target page. If not provided, return the\n"
  "                               results of the most recent SEND DIAGNOSTIC\n"
  "                               (requires that the SEND DIAGNOSTIC defined\n"
  "                                parmaeter data to return and no\n"
  "                                interveening RECEIVE DIAGNOSTIC RESULTS\n"
  "                                has been issued);\n"
  "  [response_max = 1024] {int}  The maximum number of bytes to return;\n"
  "  [raw = True] {bool}          If True, return raw data (always true);\n"
  "  [verbosity = 0] {int}        Verbosity level;\n"
  "\n"
  "Returns (on success):\n"
  "   {bytearray} If `raw` is True (or False for now);\n"
  "\n"
  "Example:\n"
  "  sg3utils.recv_diag( fd ) ->\n"
  "   bytearray( ... )\n";
static PyObject *
spc_recv_diag(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle        = NULL;
  struct sg_pt_base*  ptp           = NULL;
  int                 page          = -1;
  int                 response_max  = 1024;
  bool                raw           = false;
  int                 verbosity     = 0;
  static char*        keys[]        = {"handle",
                                       "page", "response_max", "raw",
                                       "verbosity", NULL};
  bool                pcv           = false;
  int                 resid         = 0;
  int                 rsp_len       = 0;
  PyObject*           data          = NULL;
  unsigned char*      rbuf          = _rspBuf;
  int                 rbuf_len      = sizeof(_rspBuf);
  int                 ret;

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$iipi:recv_diag", keys,
                                   &handle, &page, &response_max,
                                   &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return NULL;
  }

  if (response_max > rbuf_len) {
    // ... Allocate space for a larger response
    rbuf_len = response_max;
    rbuf     = malloc( rbuf_len + 2 );
    if (!rbuf) {
      _pyErr( PyExc_IOError, errno,
                "cannot allocate %d bytes for raw response", rbuf_len);
      return NULL;
    }
  }

  /* :XXX: PyArg_ParseTupleAndKeywords() seems to properly validate and extract
   *       all arguments EXCEPT the first integer keyword value (page) ALWAYS
   *       setting the value to 0.
   */
  if (kwargs && page == 0) {
    page = _get_page_val( kwargs );
  }

  if (page >= 0) {
    pcv = true;

  } else {
    page = 0;
    pcv  = false;

  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.recv_diag()" );
    return NULL;
  }

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "page[ 0x%02x ]\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            page);
  }

  memset(rbuf, 0, rbuf_len);

  /* RECEIVE DIAGNOSTIC RESULTS:
   *  Operation Code [0x1C] : CDB[0]
   *
   *  pcv                   => CDB[1] (bit 0)
   *  page                  => CDB[2]
   *  mx_resp_len           => CDB[3..4] (allocation length)
   *
   *  Control [0x00]        : CDB[5]
   */
  ret = sg_ll_receive_diag_v2(ptp->impl.dev_fd,
                                pcv,                    // bool  pcv
                                page,                   // int   pg_code
                                rbuf,                   // void* resp
                                rbuf_len,               // int   mx_resp_len
                                0,                      // int   timeout_secs
                                &resid,                 // int*  residp
                                false,                  // bool  noisy
                                verbosity);             // int   verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI RECEIVE DIAG [ 0x%02x ] "
                              "failed: %s",
            page, _errStr( (errno ? errno : ret) ) );
    return NULL;
  }

  /* SCSI SBC-3, Table 137 Diagnostic Data Bytes
   * Table 137 Diagnostic Data Bytes
   *
   *     Code | Byte  | Description
   *    ------+-------+-------------------------------------------------------
   *     0x00 | 0     | ADDITIONAL LENGTH (MSB) [1]
   *     0x28 | 1     | ADDITIONAL LENGTH (LSB) [1]
   *     xxxx | 2     | FRU CODE (most probable) [2]
   *     xxxx | 3     | FRU CODE [2]
   *     xxxx | 4     | FRU CODE [2]
   *     xxxx | 5     | FRU CODE (least probable) [2]
   *     xxxx | 6     | ERROR CODE (MSB) [3]
   *     VU   | 7     | ERROR CODE (LSB)
   *     VU   | 8..n  | Additional vendor unique fault information (if any)
   *          |       | (not available)
   *
   *    [1] ADDITIONAL LENGTH. This two byte value indicates the number of
   *    additional bytes included in the diagnostic data list. For example, if
   *    no product unique byte (byte 7) is available, this value would be
   *    0006h. A value of 0000h means that there are no additional bytes.
   *
   *    [2] A FIELD REPLACEABLE UNIT (FRU) Code is a byte that identifies an
   *    assembly that may have failed. The codes will be listed in probability
   *    order, with the most probable assembly listed first and the least
   *    probable listed last. A code of 00h indicates there is no FRU
   *    information and a code of 01h indicates the entire unit should be
   *    replaced. Seagate drives return 00h in these bytes.
   *
   *    [3] The ERROR CODE is a two byte value that provides information
   *    designating which part of a diagnostic operation has failed. The LSB of
   *    the error code is vendor unique.
   */
  rbuf_len -= resid;
  if (rbuf_len < 4) {
    if (verbosity) {
      pr2ws("%s(): RD resid (%d) indicates response too small (%d < 4)\n",
            __func__, resid, rbuf_len);
    }

    _pyErr( PyExc_IOError, ret, "SCSI RECEIVE DIAG [ 0x%02x ] "
                              "response too small (%d < 4)",
            page, rbuf_len);
    goto py_err;
  }

  rsp_len = sg_get_unaligned_be16(rbuf + 2) + 4;
  rsp_len = (rsp_len < rbuf_len) ? rsp_len : rbuf_len;

  if (verbosity > 3) {
    int outLen  = rsp_len;
    if (verbosity < 6 && outLen >= 1024) {
      outLen = 1024;

      pr2ws("%s(): Showing only the first %d bytes:\n", __func__, outLen);
    }

    hex2stdout( rbuf, outLen, true /* no_ascii */ );
  }

  if (verbosity) {
    pr2ws("%s(): rbuf_len[ %d ], rsp_len[ %d ]\n",
          __func__, rbuf_len, rsp_len);
  }

  // :TODO: Interpret the data into a Python Dictionary
  data = PyByteArray_FromStringAndSize((const char *)rbuf, rsp_len);

  if (rbuf && rbuf != _rspBuf) { free(rbuf); }

  return data;

py_err:
  if (data)   { Py_XDECREF(data); }

  if (rbuf && rbuf != _rspBuf) { free(rbuf); }

  return NULL;
}

static char _doc_send_diag[] =
  "Returns the result of SEND DIAGNOSTIC, as a byte array\n"
  "See SCSI SPC-3 spec for more info.\n"
  "\n"
  "Args:\n"
  "  handle {int}                   The handle returned from sg3utils.open();\n"
  "  [st_code = 0x00] {int}         The target self-test;\n"
  "  [st_default = false] {bool}    Perform the default self-test;\n"
  "  [pf_bit = false] {bool}        Set the page format bit;\n"
  "  [dev_offline = false] {bool}   Take the device offline for this test\n"
  "                                 (only valid for `st_default`);\n"
  "  [unit_offline = false] {bool}  Take the unit offline for this test\n"
  "                                 (only valid for `st_default`);\n"
  "  [params = None] {bytearray}    Parameter data to send;\n"
  "  [timeout = 7200] {int}         Foreground self test timeout (seconds);\n"
  "  [raw = True] {bool}            If True, return raw data (always true);\n"
  "  [verbosity = 0] {int}          Verbosity level;\n"
  "\n"
  "Returns:\n"
  "   {long} A status indicator (0 == success)\n;"
  "\n"
  "Example:\n"
  "  sg3utils.send_diag( fd ) -> long\n";
static PyObject *
spc_send_diag(PyObject *self, PyObject *args, PyObject* kwargs) {
  PyObject*           handle        = NULL;
  struct sg_pt_base*  ptp           = NULL;
  int                 st_code       = 0;
  bool                st_default    = false;
  bool                pf_bit        = false;
  bool                dev_offline   = false;  // useful iff st_default
  bool                unit_offline  = false;  // useful iff st_default
  PyByteArrayObject*  params        = NULL;
  int                 timeout       = 0;
  bool                raw           = false;
  int                 verbosity     = 0;
  static char*        keys[]        = {"handle",
                                       "st_code", "st_default", "pf_bit",
                                       "dev_offline", "unit_offline",
                                       "params", "timeout",
                                       "raw", "verbosity", NULL};
  int                 long_duration = 0;
  unsigned char*      pbuf          = NULL;
  int                 pbuf_len      = 0;
  int                 ret;

  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|$ippppYipi:send_diag", keys,
                                   &handle, &st_code, &st_default, &pf_bit,
                                   &dev_offline, &unit_offline,
                                   &params, &timeout,
                                   &raw, &verbosity)) {
    _pyErr( PyExc_IOError, errno, NULL );
    return PyLong_FromLong( (errno ? errno : -1) );
  }

  /* :XXX: PyArg_ParseTupleAndKeywords() seems to properly validate and extract
   *       all arguments EXCEPT the first integer keyword value (st_code)
   *       ALWAYS setting the value to 0.
   */
  if (kwargs && st_code == 0) {
    st_code = _get_page_val( kwargs );
  }

  if (st_code == 5 || st_code == 6) {
    // Foreground self-tests : honor any incoming timeout
    if (timeout <= 0) long_duration = 1;
    else              long_duration = timeout;
  }

  ptp = PyLong_AsVoidPtr( handle );
  if (ptp == NULL) {
    _pyErr( PyExc_IOError, errno, "Invalid handle sgutils.send_diag()" );
    return PyLong_FromLong( (errno ? errno : -1) );
  }

  if (verbosity) {
    struct sg_scsi_id*  sgid  = &ptp->impl.mr.sgid;

    pr2ws("%s(): fd[ %d ], SCSI %d:%d:%d:%d target_id[ %d ], "
                    "st_code[ 0x%02x ]\n",
            __func__,
            ptp->impl.dev_fd,
            sgid->host_no, sgid->channel, sgid->scsi_id, sgid->lun,
            ptp->impl.mr.target_id,
            st_code);
  }

  pbuf     = (uint8_t*)PyByteArray_AsString( (PyObject*)params );
  pbuf_len = (int     )PyByteArray_Size(     (PyObject*)params );

  /* SEND DIAGNOSTIC:
   *   Bit |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   *  Byte |     |     |     |     |     |     |     |     |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   0   | Operation Code [0x1D]                         |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   1   | Self-Test Code  | PF  | RSV | ST  | DOF | UOF |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   2   | Reserved                                      |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   3   | (MSB)                                         |
   *   4   | Parameter List Length                    (LSB)|
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *   5   | Control                                       |
   *  -----+-----+-----+-----+-----+-----+-----+-----+-----+
   *
   *  Control [0x00]        : CDB[5]
   */
  ret = sg_ll_send_diag(ptp->impl.dev_fd,
                          st_code,                // int   st_code
                          pf_bit,                 // bool  pf_bit
                          st_default,             // bool  st_bit
                          dev_offline,            // bool  devofl_bit
                          unit_offline,           // bool  unitofl_bit
                          long_duration,          // int   long_duration,
                          pbuf,                   // void* paramp
                          pbuf_len,               // int   param_len
                          false,                  // bool  noisy
                          verbosity);             // int   verbose
  if (ret) {
    _pyErr( PyExc_IOError, ret, "SCSI SEND DIAG [ 0x%0x ] failed: %s",
            st_code, _errStr( (errno ? errno : ret) ) );
    return PyLong_FromLong( (errno ? errno : ret ) );
  }

  return PyLong_FromLong( ret );
}
/* API methods }
 *****************************************************************************/

static PyMethodDef sg3utils_methods[] = {
  { "open",         (PyCFunction)spc_open,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_open },
  { "close",        spc_close,        METH_VARARGS, _doc_close },

  { "asc_ascq_str", spc_asc_ascq_str, METH_VARARGS, _doc_asc_ascq_str },

  { "scsi_id",      (PyCFunction)spc_scsi_id,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_scsi_id },

  { "is_mr",        spc_is_mr,        METH_VARARGS, _doc_is_mr },
  { "mr_get_target",spc_mr_get_target,METH_VARARGS, _doc_mr_get_target },
  { "mr_set_target",spc_mr_set_target,METH_VARARGS, _doc_mr_set_target },
  { "mr_get_pd_list",
                    spc_mr_get_pd_list,
                                      METH_VARARGS, _doc_mr_get_pd_list },

  { "parse_log_sense",
                    (PyCFunction)spc_parse_log_sense,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_parse_log_sense },

  { "inquiry",      (PyCFunction)spc_inquiry,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_inquiry},
  { "mode_sense",   (PyCFunction)spc_mode_sense,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_mode_sense},
  { "readcap",      (PyCFunction)spc_readcap,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_readcap},
  { "report_luns",  (PyCFunction)spc_report_luns,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_report_luns},

  { "log_sense",    (PyCFunction)spc_log_sense,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_log_sense},

  { "raw",          (PyCFunction)spc_raw,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_raw},

  { "sat_identify", (PyCFunction)spc_sat_identify,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_sat_identify},
  { "sat_smart_data",(PyCFunction)spc_sat_smart_data,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_sat_smart_data},
  { "recv_diag",     (PyCFunction)spc_recv_diag,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_recv_diag},
  { "send_diag",     (PyCFunction)spc_send_diag,
                                      METH_VARARGS | METH_KEYWORDS,
                                                    _doc_send_diag},
#if 0
  /* TODO */
  { "format_unit", x, METH_VARARGS, x}, /* for DIF support */
  { "persistent_reserve", x, METH_VARARGS, x},
  { "unmap", sbc_unmap, METH_VARARGS, unmap_doc}, /* use v2? */
  /* newer stuff */
  { "extended_copy", x, METH_VARARGS, x},
  { "receive_copy_results", x, METH_VARARGS, x},
#endif
  { NULL,      NULL}     /* sentinel */
};

static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  "sg3utils",         // m_name
  "sg3utils_module",  // m_doc
  -1,                 // m_size
  sg3utils_methods,   // m_methods
};

PyMODINIT_FUNC
PyInit_sg3utils(void)
{
  Py_Initialize();

  return PyModule_Create( &moduledef );
}
