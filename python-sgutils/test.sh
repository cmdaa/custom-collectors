#!/bin/bash
#
# Run the test.py script using the locally compiled sg3_utils library
#
# :NOTE: This ASSUMES the extension has been build via `make`
#
DIR=$(realpath "$(dirname "$0")")
#echo ">>> DIR: ${DIR}"

BUILD=$(realpath "$(ls -d "$DIR/build/lib."*)")
#echo ">>> BUILD: $(pwd)"

# Move to the build area, copy the script in and run it in the build context
cd "${BUILD}"
cp "${DIR}/test.py" .
python3 test.py "$@"
rm -f test.py
