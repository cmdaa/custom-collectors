import os
import sys
from distutils.core import setup, Extension

sources          = ['sg3utils.c']
static_libraries = [ 'sgutils2' ]
static_lib_dir   = ''
libraries        = None
library_dirs     = []
include_dirs     = ['./include']

################################################################
# Make sure there is a locally compiled sg3_utils
DIR = os.path.dirname( os.path.realpath( __file__ ) )
SG3_UTILS = os.path.realpath( DIR +'/../sg3_utils' )

if os.path.exists( SG3_UTILS +'/lib/.libs' ): #{
  # Enable the use of the locally compiled sg3 utils
  static_lib_dir = SG3_UTILS +'/lib/.libs'
  library_dirs   = [ static_lib_dir ]
else: #}{
  print('***',                                  file=sys.stderr)
  print('*** sg3_utils must be compiled first', file=sys.stderr)
  print('***    cd ../sg3_utils',               file=sys.stderr)
  print('***    ./configure --with-pic',        file=sys.stderr)
  print('***    make',                          file=sys.stderr)
  print('***',                                  file=sys.stderr)
  sys.exit()
#}

# Compile against the static sg3_utils library
extra_objects = [ '{}/lib{}.a'.format( static_lib_dir, lib)
                    for lib in static_libraries ]

libsg3utils = Extension('sg3utils',
                        sources       = sources,
                        include_dirs  = include_dirs,
                        library_dirs  = library_dirs,
                        libraries     = libraries,
                        extra_objects = extra_objects)

setup (name             = 'sg3utils',
       version          = '0.2.3',
       description      = 'Python binding for sg3_utils',
       maintainer       = 'D. Elmo Peele',
       maintainer_email = 'elmo.peele@gmail.com',
       url              = 'https://gitlab.com/cmdaa/custom-collectors/-/tree/master/python-sgutils',
       ext_modules      = [libsg3utils])
