This is a custom version of the
[public sg3_utils](https://github.com/hreinecke/sg3_utils)
forked at version [1.45-20190327-r815](https://github.com/hreinecke/sg3_utils/tree/b0042ccf801d0008c0f9f090ea93b3871e8a542e).


This version enables pass-through access to SCSI drives managed by a MegaRaid
controller.

### Building
To build:
``` bash
# Configure (:NOTE: `--with-pic` is only required if compiling the python
#                   module using this as a static library)
$ ./configure --with-pic

# Build
$ make

# Install
$ make install
```

If there is no `configure` script, it may be regenerated using `autoconf` and
`automake` via:
``` bash
# Regenerate the build infrastructure
$ ./autogen.sh

```

See Building in [README](README) for more details.


### Primary changes

The primary changes to the public sg3_utils are:
- [config.h.in](config.h.in) : `HAVE_MR` and `IGNORE_MR`;
- [configure.ac](configure.ac) : version `1.45a`, `HAVE_MR`, `IGNORE_MR`,
  `mr-supp`;
- [Makefile.am](Makefile.am) : add `sg_pt_linux_mr.c` to
  `libsgutils2_la_SOURCES`;
- [sg3_utils.spec](sg3_utils.spec) : version `1.45a`;
- [include/sg_cmds_basic.h](include/sg_cmds_basic.h) : add
  `sg_ll_log_sense_pt()`;
- [include/sg_pt.h](include/sg_pt.h) : add `scsi_pt_get_scsi_id()`;
- [include/sg_pt_linux.h](include/sg_pt_linux.h) : include `sg_pt_mr.h`,
  add `is_mr` and `mr` to `struct sg_pt_linux_scsi`;

- [lib/sg_cmds_basic2.c](lib/sg_cmds_basic2.c)
  - add `sg_ll_log_sense_com()` to consolidate logic and support new methods;
  - add `sg_ll_log_sense_pt()`;
  - update `sg_ll_log_sense_v2()` to use `sg_ll_log_sense_com()`;
- [lib/sg_pt_linux.c](lib/sg_pt_linux.c)
  - add `sg_linux_state_t` and `sgl_state` to hold global state;
  - rename `sg_find_bsg_nvme_char_major()` to `sg_device_scan()` and include
    support for identifying a MegaRaid controller within the system;
  - update `check_file_type()` to check whether the target device is controlled
    by a MegaRaid controller;
  - update to support MegaRaid changes:
    - `check_pt_file_handle()`;
    - `clear_scsi_pt_obj()`;
    - `set_pt_file_handle()`;
  - add `scsi_pt_get_scsi_id()` to retrieve the SCSI id of the target
    device/fd;

New files:
- [include/sg_pt_mr.h](include/sg_pt_mr.h);
- [lib/sg_pt_linux_mr.c](include/sg_pt_linux_mr.c);



***NOTE:** https://github.com/hreinecke/sg3_utils is the official git-svn
mirror for sg3_utils http://sg.danny.cz/sg/sg3_utils.html*
