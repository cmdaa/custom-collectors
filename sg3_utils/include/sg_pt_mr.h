#ifndef SG_PT_MR_H
#define SG_PT_MR_H

/*
 * Copyright (c) 2017-2019 D. Elmo Peele.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the BSD_LICENSE file.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdint.h>
#include <stdbool.h>
#include <scsi/sg.h>  // sg_scsi_id
#include <sys/uio.h>  // struct iovec

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
 * structures copied and slightly modified from:
 *    linux-kernel : drivers/scsi/megaraid.h
 * {
 *
 * :NOTE: kernel sources use u(8|16|32|64) vice uint(8|16|32|64)_t
 */

// ioctl opcodes
enum  MRI_OP {
    MRI_OP_MBOX_CMD       = 0x00000,  // DCMD or passthru command
    MRI_OP_GET_DRIVER_VER = 0x10000,  // Get driver version
    MRI_OP_GET_N_ADAP     = 0x20000,  // Get number of adapters
    MRI_OP_GET_ADAP_INFO  = 0x30000,  /* Get information about an adapter
                                       *    struct mcontroller
                                       */
    MRI_OP_GET_CAP        = 0x40000,  // Get ioctl capabilities
    MRI_OP_GET_STATS      = 0x50000,  // Get statistics
};

#define MAX_IOCTL_SGE     16

struct megasas_sge32    {
    uint32_t    phys_addr;
    uint32_t    length;

} __attribute__ ((packed));

struct megasas_sge64    {
    uint64_t    phys_addr;
    uint32_t    length;

} __attribute__ ((packed));

struct megasas_sge_skinny    {
    uint64_t    phys_addr;
    uint32_t    length;
    uint32_t    flag;

} __attribute__ ((packed));

union megasas_sgl {
    struct megasas_sge32        sge32[1];
    struct megasas_sge64        sge64[1];
    struct megasas_sge_skinny   sge_skinny[1];
} __attribute__ ((packed));

struct megasas_header {
    uint8_t     cmd;            // 00h    : MFI_CMD_*
    uint8_t     sense_len;      // 01h
    uint8_t     cmd_status;     // 02h
    uint8_t     scsi_status;    // 03h

    uint8_t     target_id;      // 04h
    uint8_t     lun;            // 05h
    uint8_t     cdb_len;        // 06h
    uint8_t     sge_count;      // 07h

    uint32_t    context;        // 08h
    uint32_t    pad_0;          // 0Ch

    uint16_t    flags;          // 10h
    uint16_t    timeout;        // 12h
    uint32_t    data_xferlen;   // 14h

} __attribute__ ((packed));

// struct megasas_init_frame
// struct megasas_io_frame
// struct megasas_abort_frame
// struct megasas_smp_frame
// struct megasas_stp_frame

struct megasas_pthru_frame {
    uint8_t     cmd;            // 00h    : MFI_CMD_PD_SCSI_IO
    uint8_t     sense_len;      // 01h
    uint8_t     cmd_status;     // 02h
    uint8_t     scsi_status;    // 03h

    uint8_t     target_id;      // 04h
    uint8_t     lun;            // 05h
    uint8_t     cdb_len;        // 06h
    uint8_t     sge_count;      // 07h

    uint32_t    context;        // 08h
    uint32_t    pad_0;          // 0Ch

    uint16_t    flags;          // 10h
    uint16_t    timeout;        // 12h
    uint32_t    data_xfer_len;  // 14h

    /****************************************/
    uint32_t    sense_buf_phys_addr_lo; // 18h
    uint32_t    sense_buf_phys_addr_hi; // 1Ch

    uint8_t                     cdb[16];// 20h    : SCSI command
    union megasas_sgl sgl;              // 30h

} __attribute__ ((packed));

union megasas_mbox {
  uint8_t  b[12];
  uint16_t s[6];
  uint32_t w[3];
} __attribute__ ((packed));
typedef union megasas_mbox  mr_mbox_t;

struct megasas_dcmd_frame {
    uint8_t     cmd;            // 00h    : MFI_CMD_DCMD
    uint8_t     reserved_0;     // 01h
    uint8_t     cmd_status;     // 02h
    uint8_t     reserved_1[4];  // 03h

    uint8_t     sge_count;      // 07h

    uint32_t    context;        // 08h
    uint32_t    pad_0;          // 0Ch

    uint16_t    flags;          // 10h
    uint16_t    timeout;        // 12h
    uint32_t    data_xfer_len;  // 14h

    /****************************************/
    uint32_t    opcode;         // 18h    : MFI_DCMD_*

    mr_mbox_t   mbox;           // 1Ch

    union megasas_sgl sgl;      // 30h

} __attribute__ ((packed));

union megasas_frame {
    struct megasas_header       hdr;
    struct megasas_pthru_frame  pthru;
    struct megasas_dcmd_frame   dcmd;

    //struct megasas_init_frame   init;
    //struct megasas_io_frame     io;
    //struct megasas_abort_frame  abort;
    //struct megasas_smp_frame    smp;
    //struct megasas_stp_frame    stp;

    /* 128-bytes used by megasas_iocpacket.frame
     *    64-bytes used by 4.9 kernel sources for megasas_frame
     *
     * Use 128-bytes so this can be used directly in megasas_iocpacket
     */
    uint8_t raw[128];

} __attribute__ ((packed));

struct megasas_iocpacket {
    uint16_t    host_no;
    uint16_t    __pad1;
    uint32_t    sgl_off;
    uint32_t    sge_count;
    uint32_t    sense_off;
    uint32_t    sense_len;

    union megasas_frame frame;
    /*
    union {
        uint8_t                     raw[128];
        struct megasas_header       hdr;
        struct megasas_pthru_frame  pthru;
        struct megasas_dcmd_frame   dcmd;
    } frame;
    // */

    struct iovec    sgl[ MAX_IOCTL_SGE ];

} __attribute__ ((packed));

/****************************************************
 * PERC5/6 Passthrough SCSI Command Interface {
 *
 * Taken from:
 *        drivers/scsi/megaraid/megaraid_sas.h
 */
#define MEGASAS_MAGIC         'M'
#define MEGASAS_IOC_FIRMWARE  _IOWR( MEGASAS_MAGIC, 1, struct megasas_iocpacket)

#define MFI_FRAME_SGL64       0x02

// Controller command frame identifiers
enum    MFI_CMD {
    MFI_CMD_PD_SCSI_IO  = 0x04, // SCSI pass-through    (megasas_pthru_frame)
    MFI_CMD_DCMD        = 0x05, // controller command (megasas_dcmd_frame)
};

// controller command frame opcodes
enum    MFI_DCMD {
    MFI_DCMD_CTRL_GET_INFO      = 0x01010000, // megasas_ctrl_info

    MFI_DCMD_PD_GET_LIST        = 0x02010000, // MR_PD_LIST
    MFI_DCMD_PD_LIST_QUERY      = 0x02010100, // MR_PD_LIST_QUERY
    MFI_DCMD_PD_GET_INFO        = 0x02020000, // MR_PD_INFO

    MFI_DCMD_LD_GET_LIST        = 0x03010000, // MR_LD_LIST
    MFI_DCMD_LD_LIST_QUERY      = 0x03010100, // MR_LD_TARGETID_LIST
    MFI_DCMD_LD_GET_PROPERTIES  = 0x03030000,
};

/* Pseudo values used to fill the mbox0_8 and/or mbox0_16 arguments to
 * _megasas_dcmd_cmd() with values that will be ignored.
 */
#define MFI_DCMD_MBOX0_8_NONE   (uint8_t)0x00
#define MFI_DCMD_MBOX0_16_NONE  (uint16_t)0x00

// command completion codes
enum    MFI_STAT    {
    MFI_STAT_OK                     = 0x00,

    MFI_STAT_INVALID_CMD            = 0x01,
    MFI_STAT_INVALID_DCMD           = 0x02,
    MFI_STAT_INVALID_PARAMETER      = 0x03,

    MFI_STAT_DEVICE_NOT_FOUND       = 0x0c,

    MFI_STAT_NO_HW_PRESENT          = 0x22,
    MFI_STAT_NOT_FOUND              = 0x23,
    MFI_STAT_NOT_IN_ENCL            = 0x24,

    MFI_STAT_SCSI_DONE_WITH_ERROR   = 0x2d,
    MFI_STAT_SCSI_IO_FAILED         = 0x2e,

    MFI_STAT_LD_OFFLINE             = 0x33,

    MFI_STAT_I2C_ERRORS_DETECTED    = 0x37,
    MFI_STAT_PCI_ERRORS_DETECTED    = 0x38,

    MFI_STAT_INVALID                = 0xFF,
};

enum    MFI_FRAME_DIR {
    MFI_FRAME_DIR_NONE  = 0x0000,
    MFI_FRAME_DIR_WRITE = 0x0008,
    MFI_FRAME_DIR_READ  = 0x0010,
    MFI_FRAME_DIR_BOTH  = 0x0018,
};

enum    MR_PD_STATE {
    MR_PD_STATE_UNCONFIGURED_GOOD   = 0x00,
    MR_PD_STATE_UNCONFIGURED_BAD    = 0x01,
    MR_PD_STATE_HOT_SPARE           = 0x02, // mbox_defs.h: 0x06
    MR_PD_STATE_OFFLINE             = 0x10, // mbox_defs.h: 0x06
    MR_PD_STATE_FAILED              = 0x11, // mbox_defs.h: 0x04
    MR_PD_STATE_REBUILD             = 0x14, // mbox_defs.h: 0x05
    MR_PD_STATE_ONLINE              = 0x18, // mbox_defs.h: 0x03
    MR_PD_STATE_COPYBACK            = 0x20,
    MR_PD_STATE_SYSTEM              = 0x40,
};

enum    MR_LD_STATE {
    MR_LD_STATE_PARTIALLY_DEGRADED  = 0x01,
    MR_LD_STATE_OPTIMAL             = 0x03, // megaraid_sas_fp.c
    /* Additional entries that have been inferred (via strocli output) but the
     * codes are unknown:
     *  MR_LD_STATE_DEGRADED  = 0x??,
     *  MR_LD_STATE_RECOVERY  = 0x??,
     *  MR_LD_STATE_HIDDEN    = 0x??,
     */
};

enum    MR_RAID_LD_STATE {                  // mbox_defs.h
    MR_RAID_LD_STATE_OFFLINE    = 0x00,
    MR_RAID_LD_STATE_DEGRADED   = 0x01,
    MR_RAID_LD_STATE_OPTIMAL    = 0x02,
    MR_RAID_LD_STATE_DELETED    = 0x03,
};

#define MAX_SYS_PDS             240 // Maximum physical devices
#define MAX_SYS_LDS             256 // Maximum logical devices

// Controller info (MFI_DCMD_CTRL_GET_INFO)
//    struct megasas_ctrl_info ;

// Physical device list (MFI_DCMD_PD_GET_LIST, MFI_DCMD_PD_LIST_QUERY)
enum MR_PD_QUERY_TYPE {
    MR_PD_QUERY_TYPE_ALL                = 0,
    MR_PD_QUERY_TYPE_STATE              = 1,
    MR_PD_QUERY_TYPE_POWER_STATE        = 2,
    MR_PD_QUERY_TYPE_MEDIA_TYPE         = 3,
    MR_PD_QUERY_TYPE_SPEED              = 4,
    MR_PD_QUERY_TYPE_EXPOSED_TO_HOST    = 5,
};

struct MR_PD_ADDRESS {
    uint16_t    device_id;          // => megasas_pd_entry.tid
    uint16_t    encl_device_id;
    uint8_t     encl_index;
    uint8_t     slot_number;
    uint8_t     scsi_dev_type;      // => megasas_pd_entry.driveType (0 == disk)
    uint8_t     connect_port_bitmap;
    uint64_t    sas_addr[2];

} __attribute__ ((packed));

struct MR_PD_LIST {
    uint32_t                size;
    uint32_t                count;
    struct MR_PD_ADDRESS    addr[ MAX_SYS_PDS ];
} __attribute__ ((packed));

// Physical device info (MFI_DCMD_PD_GET_INFO)
union MR_PD_REF {
    struct {
        uint16_t    deviceId;
        uint16_t    seqNum;
    }           mrPdRef;
    uint32_t    ref;
};

union MR_PD_DDF_TYPE {
    struct {
        union {
            struct {
                uint16_t    intf:4;
                uint16_t    reserved:7;
                uint16_t    isForeign:1;
                uint16_t    isSpare:1;
                uint16_t    isGlobalSpare:1;
                uint16_t    inVD:1;
                uint16_t    forcedPDGUID:1;
            } pdType;
            uint16_t        type;
        };
    }           ddf;
    struct {
        uint32_t            reserved;
    }                       nonDisk;
    uint32_t    type;

} __attribute__ ((packed));

union MR_PROGRESS {
    struct {
        uint16_t    progress;
        union {
            uint16_t    elapsedSecs;
            uint16_t    elapsedSecsForLastPercent;
        };
    }           mrProgress;
    uint32_t    w;
} __attribute__ ((packed));

struct MR_PD_PROGRESS {
    struct {
        uint32_t    reserved:26;
        uint32_t    locate:1;
        uint32_t    erase:1;
        uint32_t    copyBack:1;
        uint32_t    clear:1;
        uint32_t    patrol:1;
        uint32_t    rbld:1;
    }                   active;

    union MR_PROGRESS   rbld;
    union MR_PROGRESS   patrol;
    union {
        union MR_PROGRESS clear;
        union MR_PROGRESS erase;
    };

    struct {
        uint32_t    reserved:27;
        uint32_t    erase:1;
        uint32_t    copyBack:1;
        uint32_t    clear:1;
        uint32_t    patrol:1;
        uint32_t    rbld:1;
    }                   pause;

    union MR_PROGRESS   reserved[3];

} __attribute__ ((packed));

struct MR_PD_INFO {
    union MR_PD_REF ref;
    uint8_t         inquiryData[96];
    uint8_t         vpdPage83[64];
    uint8_t         notSupported;
    uint8_t         scsiDevType;

    union {
        uint8_t connectedPortBitmap;
        uint8_t connectedPortNumbers;
    };

    uint8_t         deviceSpeed;
    uint32_t        mediaErrCount;
    uint32_t        otherErrCount;
    uint32_t        predFailCount;
    uint32_t        lastPredFailEventSeqNum;

    uint16_t        fwState;
    uint8_t         disabledForRemoval;
    uint8_t         linkSpeed;

    union MR_PD_DDF_TYPE    state;

    struct {
        uint8_t     count;

        uint8_t     widePortCapable:1;
        uint8_t     reserved3:3;
        uint8_t     isPathBroken:4;

        uint8_t     connectorIndex[2];
        uint8_t     reserved[4];
        uint64_t    sasAddr[2];
        uint8_t     reserved2[16];

    }               pathInfo;

    uint64_t        rawSize;
    uint64_t        nonCoercedSize;
    uint64_t        coercedSize;
    uint16_t        enclDeviceId;
    uint8_t         enclIndex;

    union {
        uint8_t     slotNumber;
        uint8_t     enclConnectorIndex;
    };

    struct MR_PD_PROGRESS progInfo;

    uint8_t         badBlockTableFull;
    uint8_t         unusableInCurrentConfig;
    uint8_t         vpdPage83Ext[64];
    uint8_t         powerState;
    uint8_t         enclPosition;
    uint32_t        allowedOps;
    uint16_t        copyBackPartnerId;
    uint16_t        enclPartnerDeviceId;

    struct {
        uint16_t    reserved:10;
        uint16_t    needsEKM:1;
        uint16_t    foreign:1;
        uint16_t    locked:1;
        uint16_t    secured:1;
        uint16_t    fdeEnabled:1;
        uint16_t    fdeCapable:1;
    }               security;

    uint8_t         mediaType;
    uint8_t         notCertified;
    uint8_t         bridgeVendor[8];
    uint8_t         bridgeProductIdentification[16];
    uint8_t         bridgeProductRevisionLevel[4];
    uint8_t         satBridgeExists;

    uint8_t         interfaceType;
    uint8_t         temperature;
    uint8_t         emulatedBlockSize;
    uint16_t        userDataBlockSize;
    uint16_t        reserved2;;

    struct {
        uint32_t    reserved:18;
        uint32_t    supportsScsiUnmap:1;
        uint32_t    wceUnchanged:1;
        uint32_t    useSSEraseType:1;
        uint32_t    ineligibleForLd:1;
        uint32_t    ineligibleForSSCD:1;
        uint32_t    emergencySpare:1;
        uint32_t    commissionedSpare:1;
        uint32_t    WCE:1;
        uint32_t    NCQ:1;
        uint32_t    piEligible:1;
        uint32_t    piFormatted:1;
        uint32_t    piType:3;
    }               properties;

    uint64_t        shieldDiagCompletionTime;
    uint8_t         shieldCounter;

    uint8_t         linkSpeedOther;
    uint8_t         reserved4[2];

    struct {
        uint32_t    bbmErrCount:31;
        uint32_t    bbmErrCountSupported:1;
    }               bbmErr;

    uint8_t         reserved1[ 512-428 ];
} __attribute__ ((packed));

// Logical devices (MFI_DCMD_LD_LIST_QUERY)
enum MR_LD_QUERY_TYPE {
    MR_LD_QUERY_TYPE_ALL                = 0,
    MR_LD_QUERY_TYPE_EXPOSED_TO_HOST    = 1,
    MR_LD_QUERY_TYPE_USED_TGT_IDS       = 2,
    MR_LD_QUERY_TYPE_CLUSTER_ACCESS     = 3,
    MR_LD_QUERY_TYPE_CLUSTER_LOCALED    = 4,
};

struct MR_LD_TARGETID_LIST {
    uint32_t    size;
    uint32_t    count;
    uint8_t     pad[3];
    uint8_t     targetId[ MAX_SYS_LDS ];
} __attribute__ ((packed));

// Logical device list (MFI_DCMD_LD_GET_LIST)
struct MR_LD_REF {
    uint8_t     targetId;
    uint8_t     reserved;
    uint16_t    seqNum;
} __attribute__ ((packed));

struct MR_LD_ENTRY {
    struct MR_LD_REF    ref;
    uint8_t             state;    // MR_LD_STATE_*
    uint8_t             reserved[3];
    uint64_t            size;
} __attribute__ ((packed));

struct MR_LD_LIST {
    uint32_t            count;
    uint32_t            reserved;

    struct MR_LD_ENTRY  list[ MAX_SYS_LDS ];
} __attribute__ ((packed));


/* PERC5/6 Passthrough SCSI Command Interface }
 ****************************************************/

/* }
 ***************************************************************************/

/**
 *  Internal mapping between a SCSI id and a MegaRaid target.
 *  @type megaraid_id_t
 */
typedef struct {
    // :XXX: Should we put the ctl_fd here?
    // int                 ctl_fd;
    struct sg_scsi_id   sgid;
    int                 target_id;
} megaraid_id_t;

/**
 *  Given a open device, determine if it is connected to a MegaRaid controller.
 *  @method sg_is_mr
 *  @param  fd        The connection to the target device {int};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of whether the device is connected to a MegaRaid
 *          controller {int};
 *              0   : connected;
 *              !0  : not connected;
 */
extern int
sg_is_mr( int fd, int verbose );

/**
 *  Given connection to the MegaRaid controller along with the SCSI id of a
 *  target device, check if the device is managed by the controller.
 *  @method sg_mr_get_target_id
 *  @param  ctl_fd    The connection to the MegaRaid controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  verbose   Verbosity {int};
 *
 *  @note   It APPEARS that the physical disk indices are correlated to the
 *          RAID drive group, which APPEARS to be correlated to the registered
 *          block (/dev/sd?) and character (/dev/sg?) devices.
 *
 *          The indices seem to be off-by-1 since the controller is always the
 *          first devices and is not listed in the controller's list of
 *          physical disks.
 *
 *          The targetId that we need here is the DID of the physical disk
 *          entry from the physical disk list.
 *
 *          Example:
 *                              bus:chn:id:lun
 *            /dev/sde  => SCSI   0:  2: 4:  0
 *            /dev/sg5  => SCSI   0:  2: 4:  0
 *
 *            SCSI.id : 4 => physical disk index 5
 *
 *            pd_list : idx : hba : enc : slt : did : type
 *                       ...
 *                        5 :   0 :   8 :  15 :  13 : 0x00 / disk
 *                       ...
 *
 *
 *  @return The target id {int};
 *              >=0 : success;
 *              <0  : failure (negated errno or controller error);
 */
extern int
sg_mr_get_target_id( int                 ctl_fd,
                     struct sg_scsi_id*  sgid,
                     int                 verbose );

/**
 *  Fetch information about a specific physical device
 *  @method sg_mr_get_pd_info
 *  @param  ctl_fd
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  did       The target device id {uint16_t};
 *  @param  pinfo     A pointer to the structure to be filled
 *                    {struct MR_PD_INFO*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
extern int
sg_mr_get_pd_info( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   uint16_t            did,
                   struct MR_PD_INFO*  pinfo,
                   int                 verbose );

/**
 *  Fetch the set of physical devices connected to the target controller.
 *  @method sg_mr_get_pd_list
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_PD_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
extern int
sg_mr_get_pd_list( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   struct MR_PD_LIST*  plist,
                   int                 verbose );

/**
 *  Fetch the set of logical devices connected to the target controller.
 *  @method sg_mr_get_ld_list
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_LD_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
extern int
sg_mr_get_ld_list( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   struct MR_LD_LIST*  plist,
                   int                 verbose );

/**
 *  Fetch the set of targetIds for logical devices connected to the target
 *  controller.
 *  @method sg_mr_get_ld_list_query
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_LD_TARGETID_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
extern int
sg_mr_get_ld_list_query( int                         ctl_fd,
                         struct sg_scsi_id*          sgid,
                         struct MR_LD_TARGETID_LIST* plist,
                         int                         verbose );

/**
 *  Open the ioctl control node.
 *  @method sg_mr_open_control
 *  @param  verbose       Verbosity {int};
 *
 *  @return The connection to the ioctl control node {int};
 *            >=0 : success;
 *            <0  : failure (negated errno);
 */
extern int
sg_mr_open_control( int verbose );

/**
 *  Given a connection to the target device, determine if it is connected to
 *  a MegaRaid controller.
 *  @method sg_mr_check
 *  @param  dev_fd    The connection to the target device {int};
 *  @param  mr_p      A pointer to receive MegaRaid identification information
 *                    if the device is connected to a MegaRaid controller
 *                    {megaraid_id_t*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of whether `dev_fd` is connected to a MegaRaid
 *          controller. If true, `mr_p` will be filled with identification
 *          information {bool};
 */
extern bool
sg_mr_check(  int             dev_fd,
              megaraid_id_t*  mr_p,
              int             verbose );

/**
 *  Perform a SCSI pass-through via a MegaRaid controller.
 *  @method sg_do_mr_pt
 *  @param  pt_base     The generic SCSI pass-through state
 *                      {struct sg_pt_base*};
 *  @param  dev_fd      The file descriptor for the target device {int}
 *  @param  time_secs   The timeout (>0 seconds, <0 milliseconds) {int};
 *  @param  verbose     Verbosity {int};
 *
 *  @note   `dev_fd` MUST either be < 0 or match `pt_base->impl.dev_fd`;
 *
 *  @return An indication of success {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
extern int
sg_do_mr_pt(  struct sg_pt_base*  pt_base,
              int                 dev_fd,
              int                 time_secs,
              int                 verbose);

#ifdef __cplusplus
}
#endif

#endif          /* SG_PT_MR_H */
