/*
 * Copyright (c) 2017-2019 D. Elmo Peele
 * All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the BSD_LICENSE file.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * The code to use the MegaRaid (mr)  Management Interface pass-through.
 */

/* sg_pt_linux_mr version 1.00 20190606 */

#include <stddef.h>             // offsetof()
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include <errno.h>
#include <fcntl.h>              // open(), O_RDONLY
#include <unistd.h>             // close()

#include <sys/ioctl.h>          // ioctl()
#include <scsi/scsi.h>          // SCSI_IOCTL_GET_IDLUN
#include <scsi/sg.h>            // sg_scsi_id, SG_GET_SCSI_ID

#include <linux/param.h>        // HZ

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sg_pt.h"
#include "sg_lib.h"
#include "sg_linux_inc.h"
#include "sg_pt_linux.h"
#include "sg_unaligned.h"
#include "sg_pr2serr.h"

/*****************************************************************************
 * Private methods {
 *
 */

#if (HAVE_MR && (! IGNORE_MR))  // {

/**
 *  Return a string representation of the command completion status.
 *  @method _mr_stat_str
 *  @param  status    The command completion status {uint8_t};
 *
 *  @return The string representation {const char*};
 *  @private
 */
static const char*
_mr_stat_str( uint8_t status ) {
    switch( status ) {
    case MFI_STAT_OK:                   return "OK";

    case MFI_STAT_INVALID_CMD:          return "INVALID_CMD";
    case MFI_STAT_INVALID_DCMD:         return "INVALID_DCMD";
    case MFI_STAT_INVALID_PARAMETER:    return "INVALID_PARAMETER";

    case MFI_STAT_DEVICE_NOT_FOUND:     return "DEVICE_NOT_FOUND";

    case MFI_STAT_NO_HW_PRESENT:        return "NO_HW_PRESENT";
    case MFI_STAT_NOT_FOUND:            return "NOT_FOUND";
    case MFI_STAT_NOT_IN_ENCL:          return "NOT_IN_ENCL";

    case MFI_STAT_SCSI_DONE_WITH_ERROR: return "SCSI_DONE_WITH_ERROR";
    case MFI_STAT_SCSI_IO_FAILED:       return "SCSI_IO_FAILED";

    case MFI_STAT_LD_OFFLINE:           return "LD_OFFLINE";

    case MFI_STAT_I2C_ERRORS_DETECTED:  return "I2C_ERRORS_DETECTED";
    case MFI_STAT_PCI_ERRORS_DETECTED:  return "PCI_ERRORS_DETECTED";

    case MFI_STAT_INVALID:              return "INVALID";

    default:                            return "???";
    }
}

/**
 *  Perform a controller command (not specific to a target)
 *  @method _mr_dcmd_cmd
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  host_no   The SCSI host number of the target MegaRaid controller
 *                    {uin16_t};
 *  @param  opcode    The controller command code {uint32_t};
 *  @param  buf       A pointer to the data to send {void*};
 *  @param  bufSize   The size (in bytes) of `buf` {size_t};
 *  @param  mbox_p    The mbox value {mr_mbox_t*};
 *  @param  verbose   Verbosity {int};
 *
 *  @note   This only allows reads to avoid inadvertant changes to the target
 *          controller.
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 *  @private
 */
static int
_mr_dcmd_cmd(  int        ctl_fd,
               uint16_t   host_no,
               uint32_t   opcode,
               void*      buf,
               size_t     bufSize,
               mr_mbox_t* mbox_p,
               int        verbose ) {

    struct megasas_iocpacket    ioc   = {0};
    struct megasas_dcmd_frame*  dcmd  = &ioc.frame.dcmd;
    int                         rc    = -1;

    ioc.host_no       = host_no;

    dcmd->cmd         = MFI_CMD_DCMD;
    dcmd->cmd_status  = MFI_STAT_INVALID;
    dcmd->timeout     = 0;
    dcmd->flags       = MFI_FRAME_DIR_NONE; // cpu_to_le16( ... );
    dcmd->opcode      = opcode;             // cpu_to_le32( opcode );

    if (mbox_p) { memcpy( &dcmd->mbox, mbox_p, sizeof(*mbox_p) ); }

    if (verbose > 2) {
        pr2ws("%s: MR DCMD command: hba: %d, opcode: 0x%04x\n",
                __func__, host_no, opcode);
    }

    if (bufSize > 0) {
        dcmd->flags                  = MFI_FRAME_DIR_READ;// cpu_to_le16( ... );
        dcmd->sge_count              = 1;
        dcmd->data_xfer_len          = bufSize;           // cpu_to_le32( ... );
        dcmd->sgl.sge32[0].phys_addr = (intptr_t)buf;     // cpu_to_le32( ... );
        dcmd->sgl.sge32[0].length    = (uint32_t)bufSize; // cpu_to_le32( ... );

        ioc.sge_count       = 1;
        ioc.sgl_off         = offsetof( struct megasas_dcmd_frame, sgl );
        ioc.sgl[0].iov_base = buf;
        ioc.sgl[0].iov_len  = bufSize;
    }

    rc = ioctl( ctl_fd, MEGASAS_IOC_FIRMWARE, &ioc );
    if (rc < 0) {
        if (errno)  { rc = -errno; }

        if (verbose > 1) {
            pr2ws("%s: MR DCMD command: hba: %d, opcode: 0x%04x failed: %s "
                            "(errno=%d)\n",
                  __func__, host_no, opcode, strerror(-rc), -rc);
        }

        goto done;
    }

    if (dcmd->cmd_status != MFI_STAT_OK) {
        rc = dcmd->cmd_status;

        if (verbose > 1) {
            pr2ws("%s: MR DCMD command: hba: %d, opcode: 0x%04x failed: %s "
                            "(status=0x%x)\n",
                  __func__, host_no, opcode,
                  _mr_stat_str( dcmd->cmd_status ),
                  dcmd->cmd_status);
        }

        goto done;
    }

    if (verbose > 1) {
        pr2ws("%s: MR DCMD command: hba: %d, opcode: 0x%04x : SUCCEEDED\n",
                __func__, host_no, opcode);
    }

    if (verbose > 3 && bufSize > 0) {
        //size_t  outLen  = bufSize;
        uint16_t outLen = sg_get_unaligned_be16( buf );
        if (outLen > bufSize) { outLen = bufSize; }

        if (verbose > 5 || outLen < 1024) {
            pr2ws("\nData-out buffer (%u bytes):\n", outLen);
        } else {
            outLen = 1024;
            pr2ws("\nData-out buffer (first 1024 of %lu bytes):\n", bufSize);
        }
        hex2stderr((const uint8_t *)buf, outLen, 0);
    }

done:
    return rc;
}

#else   // (HAVE_MR && (! IGNORE_MR))  }{

/**
 *  MegaRaid support has been excluded.
 *  @method _mr_not_supported
 *  @param  func    The name of the calling function (__func__) {const char*};
 *  @param  verbose Verbosity {int};
 *
 *  @return void
 *  @private
 */
static void
_mr_not_supported( const char* func, int verbose ) {
    if (verbose) {
        pr2ws("%s: not supported, ", func);

# ifdef HAVE_MR // {
        pr2ws("HAVE_MR, ");
# else  // HAVE_MR }{
        pr2ws("don't HAVE_MR, ");
# endif // HAVE_MR }

# ifdef IGNORE_MR // {
        pr2ws("IGNORE_MR");
# else  // IGNORE_MR }{
        pr2ws("don't IGNORE_MR");
# endif // IGNORE_MR }

        pr2ws("\n");
    }
}
#endif  // if (HAVE_MR && (! IGNORE_MR))  }

/* Private methods }
 *****************************************************************************/

/**
 *  Given a open device, determine if it is connected to a MegaRaid controller.
 *  @method sg_is_mr
 *  @param  fd        The connection to the target device {int};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of whether the device is connected to a MegaRaid
 *          controller {int};
 *              0   : connected;
 *              !0  : not connected;
 */
int
sg_is_mr( int fd, int verbose ) {
  int res = check_pt_file_handle(fd, "unknown", verbose);

  return (res == 5);
}

/**
 *  Given connection to the MegaRaid controller along with the SCSI id of a
 *  target device, check if the device is managed by the controller.
 *  @method sg_mr_get_target_id
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  verbose   Verbosity {int};
 *
 *
 *  Observational notes:
 *    For MegaRaid controlled devices, the /dev/ nodes refer to a RAID
 *    logical/virtual disk.
 *
 *    The MegaRaid controller provides a "VD LIST" that maps between
 *    logical/virtual disks and disk groups.
 *
 *    A disk group is comprised of one or more physical disks which together
 *    provide some RAID-level logical/virtual disk.
 *
 *    It appears that the logical/virtual disk index/id is directly represented
 *    by the SCSI id.
 *
 *    The related disk group identifies all physical disks comprising that
 *    logical/virtual disk.
 *
 *      - Where do we get the map between virtual disk and disk group?
 *        - Is the targetId in the VD List equivalent to the disk group?
 *        - Is the virtual disk the same as a disk group?
 *
 *      - Where do we get the disk group with which a physical device is
 *        associated?
 *          struct MR_PD_LIST { u32 size, u32 count, MR_PD_ADDRESS addr[] }
 *          struct MR_PD_ADDRESS {
 *            u16 device_id;          // => megasas_pd_entry.tid
 *            u16 encl_device_id;
 *            u8  encl_index;
 *            u8  slot_number;
 *            u8  scsi_dev_type;      // 0 == disk
 *            u8  connect_port_bitmap;
 *            u64 sas_addr[2];
 *          }
 *
 *          * I don't see any externally queryable structure that seems to hold
 *            a mapping between logical/virtual disks and the physical disks
 *            that comprise it.  BUT there MUST be one since `storcli` is able
 *            to identify the disk group with which a physical disk is
 *            associated...
 *
 *    We need a way to differentiate physical disks within a disk
 *    group/logical/virtual disk.
 *
 *    We COULD introduce a numeric suffix to a /dev/ node name, but that
 *    complicates the logic since we now have to know at the highest level that
 *    there are portions of a device filename that should be excluded...
 *
 *                                      SCSI               MegaRaid
 *    Example:                          bus:chn:id:lun  => EID:SLT:DID:DG
 *      /dev/sdhj.3   => /dev/sdhj  =>    0:  2: 5: 0   =>   9: 53: 32: 5
 *      /dev/sg230.3  => /dev/sg230 =>    0:  2: 5: 0   =>   9: 53: 32: 5
 *                 ^                             ^
 *                 |                             +-- logical/virtual disk index
 *                 +-- physical disk index            |
 *                       | within group               |
 *                       +--------------------------- | ------------------+
 *                                                    |                   |
 *            vd_list : idx : DG  : VD                |                   |
 *                        0 :  0  :  0                |                   |
 *                        1 :  1  :  1                |                   |
 *                        2 :  2  :  2                |                   |
 *                        3 :  3  :  3                |                   |
 *                        4 :  4  :  4                |                   |
 *                        5 :  5  :  5  <-------------+                   |
 *                        6 :  6  :  6                                    |
 *                        7 :  7  :  7                                    |
 *                        8 :  8  :  8                                    |
 *                        9 :  9  :  9                                    |
 *                       10 : 10  : 10                                    |
 *                       11 : 11  : 11                                    |
 *                       12 : 12  : 12                                    |
 *                       13 : 13  : 13                                    |
 *                       14 : 14  : 14                                    |
 *                       15 : 15  : 15                                    |
 *                                                                        |
 *            pd_list : idx : grp-idx : EID:Slt DID   DG  /dev/ pseudo    |
 *                       ...                                              |
 *                       50 :       0 :   9:50   31    5  /dev/sg230.0    |
 *                       51 :       1 :   9:51   33    5  /dev/sg230.1    |
 *                       52 :       2 :   9:52   94    5  /dev/sg230.2    |
 *                       53 :       3 :   9:53   32    5  /dev/sg230.3 <--+
 *                       54 :       4 :   9:54   34    5  /dev/sg230.4
 *                       55 :       5 :   9:55   26    5  /dev/sg230.5
 *                       56 :       6 :   9:56   37    5  /dev/sg230.6
 *                       57 :       7 :   9:57   93    5  /dev/sg230.7
 *                       58 :       8 :   9:58   29    5  /dev/sg230.8
 *                       59 :       9 :   9:59   27    5  /dev/sg230.9
 *                       ...                           ^
 *                                                     |
 *                        * Cannot find this mapping --+
 *
 *
 *  @return The target id {int};
 *              >=0 : success;
 *              <0  : failure (negated errno or controller error);
 */
int
sg_mr_get_target_id( int                 ctl_fd,
                     struct sg_scsi_id*  sgid,
                     int                 verbose ) {

    int target_id   = -1;

#if (HAVE_MR && (! IGNORE_MR))    // {
    /* :XXX: 2021.03.17 : This WAS 'target_idex = 0' for MeadowGate
     *                    but that doesn't work on a non-multipath setup.
     */
    uint16_t            target_idex = sgid->scsi_id + 1;
    struct MR_PD_LIST   pd_list     = {0};
    int                 rc;

# if  0 // {
    if (verbose) {
        struct MR_LD_LIST           ld_list = {0};
        struct MR_LD_TARGETID_LIST  ld_tids = {0};

        (void)sg_mr_get_ld_list( ctl_fd, sgid, &ld_list, verbose );
        (void)sg_mr_get_ld_list_query( ctl_fd, sgid, &ld_tids, verbose );
    }
# endif // }

    rc = sg_mr_get_pd_list( ctl_fd, sgid, &pd_list, verbose );
    if (rc < 0) {
        return rc;
    }

    if (target_idex < pd_list.count) {
        struct MR_PD_ADDRESS* addr  = &pd_list.addr[ target_idex ];

        if( addr->scsi_dev_type == TYPE_DISK ) {
            target_id = addr->device_id;
        }
    }

    if (verbose) {
        pr2ws("%s: target SCSI: %d:%d:%d:%d => vd %d, index %d, id %d : ",
                __func__, sgid->host_no,
                          sgid->channel,
                          sgid->scsi_id,
                          sgid->lun,
                          sgid->scsi_id,
                          target_idex,
                          target_id);

        if (target_id >= 0) {
            struct MR_PD_ADDRESS* addr  = &pd_list.addr[target_idex];

            pr2ws("Enclosure %d, slot %d, did %d\n",
                        addr->encl_device_id,
                        addr->slot_number,
                        addr->device_id);

        } else {
            pr2ws("NO MATCH\n");

        }
    }

#else     // (HAVE_MR && (! IGNORE_MR))    }{
    _mr_not_supported( __func__, verbose );
#endif    // (HAVE_MR && (! IGNORE_MR))    }

    return target_id;
}

/**
 *  Fetch information about a specific physical device
 *  @method sg_mr_get_pd_info
 *  @param  ctl_fd
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  did       The target device id {uint16_t};
 *  @param  pinfo     A pointer to the structure to be filled
 *                    {struct MR_PD_INFO*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
int
sg_mr_get_pd_info( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   uint16_t            did,
                   struct MR_PD_INFO*  pinfo,
                   int                 verbose ) {

    int         rc;
    mr_mbox_t   mbox    = {{0}};

    // mbox: a single 16-bit did
    mbox.s[0] = did;

    memset( pinfo, 0, sizeof(*pinfo) );

    // Physical device list (MFI_DCMD_PD_GET_LIST, MFI_DCMD_PD_LIST_QUERY)
    rc = _mr_dcmd_cmd( ctl_fd, sgid->host_no,
                       MFI_DCMD_PD_GET_INFO,
                       pinfo, sizeof(*pinfo),
                       &mbox,
                       verbose );

    if (verbose) {
        if (rc == 0) {
            /*
             *  pinfo->inquiryData[96]          {u8};
             *  pinfo->vpdPage83[64]            {u8};
             *  pinfo->vpdPage83Ext[64]         {u8};
             *  pinfo->temperature              {u8};
             *
             *  pinfo->powerState               {u8};
             *
             *  pinfo->mediaErrCount            {u32};
             *  pinfo->otherErrCount            {u32};
             *  pinfo->predFailCount            {u32};
             *  pinfo->lastPredFailEventSeqNum  {u32};
             *
             *  pinfo->rawSize                  {u64};
             *  pinfo->nonCoercedSize           {u64};
             *  pinfo->coercedSize              {u64};
             *
             *  pinfo->emulatedBlockSize        {u8};
             *  pinfo->userDataBlockSize        {u16};
             */
            pr2ws("%s: Found information for did %u, ref[ did %u : seq %u ], "
                          "enc[ idx %u, did %u, pos %u ], slot[ %u ]\n",
                  __func__,
                  did,
                  pinfo->ref.mrPdRef.deviceId, pinfo->ref.mrPdRef.seqNum,
                  pinfo->enclIndex, pinfo->enclDeviceId, pinfo->enclPosition,
                  pinfo->slotNumber);

        } else {
            pr2serr("%s: Error %d: %s\n",
                    __func__, rc,
                    (rc < 0 ? safe_strerror( abs(rc) ) : _mr_stat_str(rc)) );
        }
    }

    return rc;
}

/**
 *  Fetch the set of physical devices connected to the target controller.
 *  @method sg_mr_get_pd_list
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_PD_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
int
sg_mr_get_pd_list( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   struct MR_PD_LIST*  plist,
                   int                 verbose ) {

    int         rc;
    mr_mbox_t   mbox    = {{0}};

    // mbox: a single 8-bit flag
    mbox.b[0] = MR_PD_QUERY_TYPE_EXPOSED_TO_HOST;

    memset( plist, 0, sizeof(*plist) );

    // Physical device list (MFI_DCMD_PD_GET_LIST, MFI_DCMD_PD_LIST_QUERY)
    rc = _mr_dcmd_cmd( ctl_fd, sgid->host_no,
                       MFI_DCMD_PD_GET_LIST,
                       plist, sizeof(*plist),
                       &mbox,
                       verbose );
    if (verbose) {
        if (rc == 0) {
            pr2ws("%s: Found %d physical devices\n", __func__, plist->count);

            if (verbose > 1) {
                uint32_t idex;
                for (idex = 0; idex < plist->count; idex++) {
                  struct MR_PD_ADDRESS* addr  = &plist->addr[ idex ];

                  pr2ws("%s:   %4d: did[ %d ], enc_did[ %d ], enc_idx[ %d ], "
                              "slot[ %d ], scsi_dev_type[ %d ], "
                              "connect_port_bitmap[ 0x%x ], "
                              "sas_addr[ 0x%lx : %lx ]\n",
                        __func__,
                        idex,
                        addr->device_id,
                        addr->encl_device_id,
                        addr->encl_index,
                        addr->slot_number,
                        addr->scsi_dev_type,
                        addr->connect_port_bitmap,
                        addr->sas_addr[0],
                        addr->sas_addr[1]);

                    if (verbose > 2) {
                        struct MR_PD_INFO pd_info;

                        (void)sg_mr_get_pd_info( ctl_fd, sgid,
                                                 addr->device_id,
                                                 &pd_info, verbose );
                    }
                }
            }

        } else {
            pr2serr("%s: Error %d: %s\n",
                    __func__, rc,
                    (rc < 0 ? safe_strerror( abs(rc) ) : _mr_stat_str(rc)) );
        }
    }

    return rc;
}

/**
 *  Fetch the set of logical devices connected to the target controller.
 *  @method sg_mr_get_ld_list
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_LD_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
int
sg_mr_get_ld_list( int                 ctl_fd,
                   struct sg_scsi_id*  sgid,
                   struct MR_LD_LIST*  plist,
                   int                 verbose ) {

    int         rc;
    mr_mbox_t   mbox    = {{0}};

    // mbox: a single 8-bit flag
    mbox.b[0] = MR_PD_QUERY_TYPE_EXPOSED_TO_HOST;

    memset( plist, 0, sizeof(*plist) );

    rc = _mr_dcmd_cmd( ctl_fd, sgid->host_no,
                       MFI_DCMD_LD_GET_LIST,
                       plist, sizeof(*plist),
                       &mbox,
                       verbose );
    if (verbose) {
        if (rc == 0) {
            pr2ws("%s: Found %d logical devices\n", __func__, plist->count);

            if (verbose > 1) {
              uint32_t idex;
              for (idex = 0; idex < plist->count; idex++) {
                struct MR_LD_ENTRY* entry = &plist->list[ idex ];

                pr2ws("%s:   %4d: ref.targetId[ %u ], ref.seqNum[ %u ], "
                                    "state[ 0x%x ], size[ %lu ]\n",
                      __func__,
                      idex,
                      entry->ref.targetId,
                      entry->ref.seqNum,
                      entry->state,
                      entry->size);
              }
            }

        } else {
            pr2serr("%s: Error %d: %s\n",
                    __func__, rc,
                    (rc < 0 ? safe_strerror( abs(rc) ) : _mr_stat_str(rc)) );
        }
    }

    return rc;
}

/**
 *  Fetch the set of targetIds for logical devices connected to the target
 *  controller.
 *  @method sg_mr_get_ld_list_query
 *  @param  ctl_fd    The connection to the iocl controller {int};
 *  @param  sgid      The SCSI id of the target device which identifies the
 *                    target MegaRaid controller (host_no) {struct sg_scsi_id*};
 *  @param  plist     A pointer to the list to be filled
 *                    {struct MR_LD_TARGETID_LIST*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of success/failure {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
int
sg_mr_get_ld_list_query( int                         ctl_fd,
                         struct sg_scsi_id*          sgid,
                         struct MR_LD_TARGETID_LIST* plist,
                         int                         verbose ) {

    int         rc;
    mr_mbox_t   mbox    = {{0}};

    // mbox: a single 8-bit flag
    mbox.b[0] = MR_PD_QUERY_TYPE_EXPOSED_TO_HOST;
    //mbox.b[2] = 1;  // supportmax256vd

    memset( plist, 0, sizeof(*plist) );

    rc = _mr_dcmd_cmd( ctl_fd, sgid->host_no,
                       MFI_DCMD_LD_LIST_QUERY,
                       plist, sizeof(*plist),
                       &mbox,
                       verbose );
    if (verbose) {
        if (rc == 0) {
            pr2ws("%s: Found %d logical devices\n", __func__, plist->count);

            if (verbose > 1) {
              uint32_t idex;
              for (idex = 0; idex < plist->count; idex++) {
                uint8_t targetId  = plist->targetId[ idex ];

                pr2ws("%s:   %4d: targetId[ %u ]\n",
                      __func__,
                      idex,
                      targetId);
              }
            }

        } else {
            pr2serr("%s: Error %d: %s\n",
                    __func__, rc,
                    (rc < 0 ? safe_strerror( abs(rc) ) : _mr_stat_str(rc)) );
        }
    }

    return rc;
}

/**
 *  Open the ioctl control node.
 *  @method sg_mr_open_control
 *  @param  verbose       Verbosity {int};
 *
 *  @return The connection to the ioctl control node {int};
 *            >=0 : success;
 *            <0  : failure (negated errno);
 */
int
sg_mr_open_control( int verbose ) {
    int ctl_fd  = -1;

#if (HAVE_MR && (! IGNORE_MR))  // {
    char*  path[] = {
        "/dev/megaraid_sas_ioctl_node",
        "/dev/megadev0",
    };

    if ( (ctl_fd = open( path[0], O_RDWR )) >= 0) {
        if (verbose) {
            pr2ws("%s: opened '%s': %d\n", __func__, path[0], ctl_fd);
        }

    } else if ( (ctl_fd = open( path[1], O_RDWR )) >= 0) {
        if (verbose) {
            pr2ws("%s: opened '%s': %d\n", __func__, path[1], ctl_fd);
        }

    } else if (verbose) {
        pr2ws("%s: Cannot open the MegaRaid ioctl controller\n", __func__);
    }

    if (ctl_fd < 0 && errno)  { ctl_fd = -errno; }

#else   // (HAVE_MR && (! IGNORE_MR))  }{
    _mr_not_supported( __func__, verbose );
#endif  // (HAVE_MR && (! IGNORE_MR))  }

    return ctl_fd;
}

/**
 *  Given a connection to the target device, determine if it is connected to
 *  a MegaRaid controller.
 *  @method sg_mr_check
 *  @param  dev_fd    The connection to the target device {int};
 *  @param  mr_p      A pointer to receive MegaRaid identification information
 *                    if the device is connected to a MegaRaid controller
 *                    {megaraid_id_t*};
 *  @param  verbose   Verbosity {int};
 *
 *  @return An indication of whether `dev_fd` is connected to a MegaRaid
 *          controller. If true, `mr_p` will be filled with identification
 *          information {bool};
 */
bool
sg_mr_check(  int             dev_fd,
              megaraid_id_t*  mr_p,
              int             verbose ) {

    bool  rc  = false;

#if (HAVE_MR && (! IGNORE_MR))  // {

    if (scsi_pt_get_scsi_id( dev_fd, &mr_p->sgid, verbose ) == 0) {
        /* We were able to fetch the SCSI id, now open a connection to
         * the MegaRaid controller ...
         */
        int ctl_fd  = -1;

        if ( (ctl_fd = sg_mr_open_control( verbose )) >= 0 ) {
            // ... and see if the target id is managed by controller
            mr_p->target_id = sg_mr_get_target_id( ctl_fd, &mr_p->sgid,
                                                   verbose );

            //mr_p->ctl_fd = ctl_fd;
            close( ctl_fd );

            if (mr_p->target_id >= 0) {
                /* YES -- it is controlled by the MegaRaid controller
                 *    so cache the id information
                 */
                rc = true;
            }

            // NO -- not controlled by the MegaRaid controller
        }
    }

#else   // (HAVE_MR && (! IGNORE_MR))  }{
    _mr_not_supported( __func__, verbose );
#endif  // (HAVE_MR && (! IGNORE_MR))  }

    return rc;
}

/**
 *  Perform a SCSI pass-through via a MegaRaid controller.
 *  @method sg_do_mr_pt
 *  @param  pt_base     The generic SCSI pass-through state
 *                      {struct sg_pt_base*};
 *  @param  dev_fd      The file descriptor for the target device {int}
 *  @param  time_secs   The timeout (>0 seconds, <0 milliseconds) {int};
 *  @param  verbose     Verbosity {int};
 *
 *  @note   If `dev_fd` != `pt_base->impl.dev_fd` it will be used as the
 *          disk id of the target physical disk.
 *
 *  @note   Data will be passed in/out via:
 *            set_scsi_pt_cdb( vp, cdb, cdb_len )
 *              => ptp->io_hdr.request
 *                 ptp->io_hdr.request_len
 *            set_scsi_pt_sense( vp, sense, max_sense_len )
 *              => ptp->io_hdr.response
 *                 ptp->io_hdr.max_response_len
 *            set_scsi_pt_data_in( vp, dxferp, dxfer_ilen )
 *              => ptp->io_hdr.din_xferp
 *                 ptp->io_hdr.din_xfer_len
 *            set_scsi_pt_data_out( vp, dxferp, dxfer_olen )
 *              => ptp->io_hdr.dout_xferp
 *                 ptp->io_hdr.dout_xfer_len
 *            set_pt_data_out( vp, dxferp, dxfer_len, out_true )
 *              => ptp->mdxferp
 *                 ptp->mdxfer_len
 *                 ptp->mdxfer_out
 *
 *            get_scsi_pt_status_response( vp )
 *              => ptp->io_hdr.device_status
 *
 *            set_scsi_pt_transport_err( vp, err )
 *              => ptp->io_hdr.transport_status
 *
 *  @return An indication of success {int};
 *              0   : success;
 *              <0  : failure (negated errno);
 *              >0  : failure (controller error);
 */
int
sg_do_mr_pt(  struct sg_pt_base*  pt_base,
              int                 dev_fd,
              int                 time_secs,
              int                 verbose) {

    int res = -1;

#if (HAVE_MR && (! IGNORE_MR))    // {
    struct sg_pt_linux_scsi*    ptp         = &pt_base->impl;
    megaraid_id_t*              mr_p        = &ptp->mr;
    struct megasas_iocpacket    uio         = {0};
    struct megasas_pthru_frame* pthru       = &uio.frame.pthru;
    int                         ctl_fd      = -1;
    uint32_t                    ioBufLen    = 0;
    uint8_t*                    ioBuf       = NULL;
    uint8_t                     target_id   = mr_p->target_id;
    uint8_t                     cdb_len;        // 06h
    const uint8_t*              cdb;

    if (! ptp->is_mr) {
        // The target is NOT connected via a MegaRaid controller
        res = -EINVAL;
        goto done;
    }

    if (! ptp->io_hdr.request) {
        if (verbose) {
            pr2ws("No MR command given (set_scsi_pt_cdb())\n");
        }
        res = SCSI_PT_DO_BAD_PARAMS;
        goto done;
    }

    if (ptp->dev_fd < 0) {
        if (verbose) {
            pr2ws("%s: invalid file descriptors\n", __func__);
        }
        res = SCSI_PT_DO_BAD_PARAMS;
        goto done;
    }

    if (dev_fd >= 0 && dev_fd != ptp->dev_fd) {
        /* :XXX: Due to the caller do_scsi_pt(), this can never be true...
         *
         * Use the id as the physical disk target over-riding any
         * auto-identified target id.
         */
        target_id = (uint8_t)(dev_fd & 0x0ff);

        if (verbose) {
            pr2ws("%s: over-ride target id [ 0x%x ] with [ 0x%x ]\n",
                  __func__, mr_p->target_id, target_id);
        }
    }

    cdb_len = ptp->io_hdr.request_len;
    cdb     = (const uint8_t *)(sg_uintptr_t)ptp->io_hdr.request;

    if (cdb_len > sizeof(pthru->cdb)) {
        // TOO MUCH DATA
        if (verbose) {
            pr2ws("%s: command length of %u bytes is too long (> %lu) \n",
                    __func__, cdb_len, sizeof(pthru->cdb));
        }

        return SCSI_PT_DO_BAD_PARAMS;
    }

    if (verbose > 3) {
        pr2ws("%s: opcode=0x%x, fd=%d (dev_fd=%d), time_secs=%d\n", __func__,
              cdb[0], dev_fd, ptp->dev_fd, time_secs);
    }

    /*
    if (sg_is_scsi_cdb(cdb, cdb_len)) {
        switch( cdb[0] ) {
        case SCSI_INQUIRY_OPC:
        case SCSI_REPORT_LUNS_OPC:
        case SCSI_TEST_UNIT_READY_OPC:
        case SCSI_REQUEST_SENSE_OPC:
        case SCSI_SEND_DIAGNOSTIC_OPC:
        case SCSI_RECEIVE_DIAGNOSTIC_OPC:
        case SCSI_SENSE10_OPC:
        case SCSI_SELECT10_OPC:
        case SCSI_CAPACITY10_OPC:
        case SCSI_SERVICE_ACT_IN_OPC:
        case SCSI_MAINT_IN_OPC:
        default:
            // No translation to MR for SCSI command
        }
    }
    // */

    // Setup the top-level ioctl packet
    uio.host_no = mr_p->sgid.host_no;

    // Establish the pass-through frame
    pthru->cmd          = MFI_CMD_PD_SCSI_IO;
    pthru->cmd_status   = MFI_STAT_INVALID;
    pthru->scsi_status  = 0x00;
    pthru->target_id    = target_id;
    pthru->lun          = 0;
    pthru->flags        = MFI_FRAME_DIR_NONE;
    pthru->timeout      = (time_secs >= 0
                            ? time_secs
                            : abs(time_secs) * 1000);

    if (verbose > 2) {
        uint32_t bufLen = ptp->io_hdr.din_xfer_len;

        pr2ws("%s: MR PT command: hba: %d, target: %d\n",
                __func__, uio.host_no, pthru->target_id);

        if (bufLen > 0 && verbose > 3) {
            const uint8_t* buf  = (const uint8_t*)ptp->io_hdr.din_xferp;

            if (verbose > 5 || bufLen < 512) {
                pr2ws("\nData-in buffer (%u bytes):\n", bufLen);
            } else {
                bufLen = 512;
                pr2ws("\nData-in buffer (first 512 of %u bytes):\n", bufLen);
            }
            hex2stderr( buf, bufLen, 0 );
        }
    }

    if (ptp->io_hdr.dout_xfer_len > 0 &&
        ptp->io_hdr.din_xfer_len  > 0) {
        /* Both din and dout are provided.
         *
         * Since the ioctl only accepts one buffer, we will use the largest for
         * both in and out juggling data as needed.
         */
        pthru->flags = MFI_FRAME_DIR_BOTH;

        // Select the largest of the two buffers
        ioBufLen = ( ptp->io_hdr.din_xfer_len > ptp->io_hdr.dout_xfer_len
                        ? ptp->io_hdr.din_xfer_len
                        : ptp->io_hdr.dout_xfer_len );
        ioBuf    = (uint8_t*)( ioBufLen == ptp->io_hdr.din_xfer_len
                                    ? ptp->io_hdr.din_xferp
                                    : ptp->io_hdr.dout_xferp);

        if (ioBuf == (void*)ptp->io_hdr.dout_xferp) {
            // Copy `din` to `ioBuf` since we're using `dout` for both in/out
            memcpy( ioBuf, (void*)ptp->io_hdr.din_xferp,
                    ptp->io_hdr.din_xfer_len );
        }

    } else if (ptp->io_hdr.dout_xfer_len > 0) {
        pthru->flags = MFI_FRAME_DIR_READ;

        ioBufLen = ptp->io_hdr.dout_xfer_len;
        ioBuf    = (uint8_t*)ptp->io_hdr.dout_xferp;

    } else if (ptp->io_hdr.din_xfer_len > 0) {
        pthru->flags  = MFI_FRAME_DIR_WRITE;

        ioBufLen = ptp->io_hdr.din_xfer_len;
        ioBuf    = (uint8_t*)ptp->io_hdr.din_xferp;
    }

    if (ioBufLen > 0) {
        // Include a reference to the incoming data buffer
        pthru->sge_count              = 1;
        pthru->data_xfer_len          = ioBufLen;
        pthru->sgl.sge32[0].phys_addr = (intptr_t)ioBuf;
        pthru->sgl.sge32[0].length    = (uint32_t)ioBufLen;

        uio.sge_count       = 1;
        uio.sgl_off         = offsetof( struct megasas_pthru_frame, sgl );
        uio.sgl[0].iov_base = (void*)ioBuf;
        uio.sgl[0].iov_len  = ioBufLen;
    }

    // Inject SCSI command data
    pthru->cdb_len = cdb_len;
    memcpy( pthru->cdb, cdb, cdb_len );

    /* :XXX: Sense data?? (response)
     *          kernel/include/scsi/scsi_cmnd.h
     *              SCSI_SENSE_BUFFERSIZE 96
     *
     *              cmd->sense_phys_addr    : dma_addr_t (u32/u64)
     *
     *              pthru->sense_len = SCSI_SENSE_BUFFERSIZE;
     *              pthru->sense_buf_phys_addr_hi =
     *                  cpu_to_le( upper_32_bits( cmd->sense_phys_addr ) );
     *              pthru->sense_buf_phys_addr_lo =
     *                  cpu_to_le( lower_32_bits( cmd->sense_phys_addr ) );
     *
     * :XXX: If `pthru->sense_len` > 0, sense data will be returned
     *       within the raw uio buffer at the offset:
     *              uio.sense_off
     *                  => (unsigned long)uio.frame.raw + uio.sense_off
     */

    if ( (ctl_fd = sg_mr_open_control( verbose )) < 0 ) {
        res = (errno ? -errno : ctl_fd);
        goto done;
    }

    // Invoke the pass-thru via ioctl()
    res = ioctl( ctl_fd, MEGASAS_IOC_FIRMWARE, &uio );
    if (res < 0) {
        if (errno)  { res = -errno; }

        if (verbose > 1) {
            pr2ws("%s: MR PT command: hba: %d, target: %d failed: %s "
                            "(errno=%d)\n",
                  __func__, uio.host_no, pthru->target_id,
                  strerror(-res), -res);
        }

        goto done;
    }

    if (pthru->cmd_status != MFI_STAT_OK) {
        res = pthru->cmd_status;

        if (verbose > 1) {
            pr2ws("%s: MR PT command: hba: %d, target: %d failed: %s "
                            "(status=0x%x)\n",
                  __func__, uio.host_no, pthru->target_id,
                  _mr_stat_str( pthru->cmd_status ),
                  pthru->cmd_status);
        }

        goto done;
    }

    /* SUCCESS : any output data is in `ioBuf`
     *
     * :XXX: MAY need to copy out sense data and/or build a sense buffer
     */
    if (verbose > 1) {
        pr2ws("%s: MR PT command: hba: %d, target: %d : SUCCEEDED\n",
                __func__, uio.host_no, pthru->target_id);
    }

    if (ptp->io_hdr.dout_xfer_len > 0 && ioBuf == (void*)ptp->io_hdr.din_xferp){
        uint16_t len = sg_get_unaligned_be16( ioBuf );

        // Copy `ioBuf` to `dout` since we used `din` for both in/out
        memcpy( (void*)ptp->io_hdr.dout_xferp, ioBuf, len );
        ptp->io_hdr.dout_xfer_len = len;

        if (verbose > 3) {
            uint32_t        bufLen  = ptp->io_hdr.dout_xfer_len;
            uint32_t        outLen  = bufLen;
            const uint8_t*  buf     = (const uint8_t*)ptp->io_hdr.dout_xferp;

            if (verbose > 5 || bufLen < 1024) {
                pr2ws("\nData-out buffer (%u bytes):\n", bufLen);
            } else {
                outLen = 1024;
                pr2ws("\nData-out buffer (first 1024 of %u bytes):\n", bufLen);
            }
            hex2stderr( buf, outLen, 0 );
        }
    }

done:
    ptp->io_hdr.driver_status = res;

    if (ctl_fd >= 0) {
        close( ctl_fd );
    }

#else     // (HAVE_MR && (! IGNORE_MR)) }{
    _mr_not_supported( __func__, verbose );

    if (pt_base) { ; }        // suppress warning
    if (dev_fd) { ; }         // suppress warning
    if (time_secs) { ; }    // suppress warning

    res = -ENOTTY;                // inappropriate ioctl error

#endif    // (HAVE_MR && (! IGNORE_MR)) }

    return res;
}
