/**
 *  Testing process priorities.
 *
 *  The normal scheduler (SCHED_OTHER) only allows priorities > 0:
 *    value   : priority  nice
 *    1       : 21        1
 *    0       : 20        0
 *    -1      : 19        -1
 *    -10     : 10        -10
 *    -20     : 0         -20
 *
 *    PID   USER     PR    NI   VIRT    RES    SHR S   %CPU  %MEM  TIME+ COMMAND
 *    19447 root       0 -20    2488    508    444 S   0.0   0.0   0:00.00 pri
 *
 *  See the available schedulers and their min/max priorities:
 *    chrt -m
 *
 *  Uncomment the USE_RR define to use SCHED_RR which allows using the realtime
 *  scheduler with priorities between 1 and 99:
 *   value       : priority  nice
 *   minPri[1]   : -2        0
 *   50          : -51       0
 *   98          : -99       0
 *   maxPri[99]  : rt        0
 *
 *    PID   USER     PR    NI   VIRT    RES    SHR S   %CPU  %MEM  TIME+ COMMAND
 *    19376 root     -51   0    2488    572    508 S   0.0   0.0   0:00.00 pri
 */
#define USE_RR

#include <stdio.h>          // for printf()
#include <sys/types.h>      // For getpid()
#include <sys/time.h>       // For setpriority() / getpriority()
#include <sys/resource.h>   // For setpriority() / getpriority()
#include <unistd.h>         // for sleep() / getpid()
#ifdef  USE_RR  //  {
#include <sched.h>          /* for sched_getscheduler() / sched_setscheduler()
                             *     sched_get_priority_min/max()
                             */
#endif  // USE_RR   }

int main(){
  pid_t pid       = getpid();

#ifdef  USE_RR  //  {
  int   scheduler = sched_getscheduler(pid);

  printf("PID %d: Scheduler[ %d ]\n", pid, scheduler);
  switch( scheduler) {
#ifdef SCHED_OTHER  //  {
    case SCHED_OTHER: printf("--- SCHED_OTHER\n");  break;
#endif // SCHED_OTHER   }
#ifdef SCHED_BATCH  //  {
    case SCHED_BATCH: printf("--- SCHED_BATCH\n");  break;
#endif // SCHED_BATCH   }
#ifdef SCHED_IDLE   //  {
    case SCHED_IDLE : printf("--- SCHED_IDLE\n");   break;
#endif // SCHED_IDLE    }
#ifdef SCHED_FIFO   //  {
    case SCHED_FIFO : printf("--- SCHED_FIFO\n");   break;
#endif // SCHED_FIFO    }
#ifdef SCHED_RR     //  {
    case SCHED_RR   : printf("--- SCHED_RR\n");     break;
#endif // SCHED_RR      }
    default         : printf("--- unknown\n");      break;
  }

# ifdef  SCHED_RR  // {
  {
    int minPri  = sched_get_priority_min( SCHED_RR );
    int maxPri  = sched_get_priority_max( SCHED_RR );
    struct sched_param  param = {0};
    int rc;

    printf("SCHED_RR: %d .. %d\n", minPri, maxPri);

    param.sched_priority = (int)((minPri + maxPri) / 2);

    /* sched_priority:
     *  value       : priority  nice
     *  minPri[1]   : -2        0
     *  50          : -51       0
     *  98          : -99       0
     *  maxPri[99]  : rt        0
     *
     *  Interrupt handlers run at priority -51
     */
    rc = sched_setscheduler( pid, SCHED_RR, &param );
    if (rc) {
      printf("PID %d: Setting scheduler to RR failed: %d\n", pid, rc);

    } else {
      printf("PID %d: Scheduler set to RR with priority %d\n",
              pid, param.sched_priority);
    }
  }

# endif  // SCHED_RR  }

#else   // USE_RR     }{
  /* priority value:
   *  value   : priority  nice
   *  1       : 21        1
   *  0       : 20        0
   *  -1      : 19        -1
   *  -10     : 10        -10
   *  -20     : 0         -20
   */
  setpriority(PRIO_PROCESS, 0, -20);

#endif  // USE_RR     }

  printf("PID %d: Priority: %d\n", pid, getpriority( PRIO_PROCESS, 0 ));

  for (int idex = 0; idex < 1000; idex++) {
    printf("Sleep 5s\n");

    sleep( 5 );
  }

  //exit(0);
}
