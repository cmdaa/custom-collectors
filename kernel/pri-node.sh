#!/bin/bash
#
# 
function handle_sig() {
  echo ">>> handle_sig, kill $PID ..."
  kill -9 $PID
}

./pri-watch.js &
PID=$!

# Trap exit to kill the background process
trap "handle_sig" EXIT


echo -n ">>> Current prioirity for $PID: "
ps -heo policy,pri,nice -q $PID

echo ">>> Change scheduler to RR and priority to 80 for PID[ $PID ] ..."
sudo chrt --rr -p 80 $PID

echo -n ">>> Updated prioirity for $PID: "
ps -heo policy,pri,nice -q $PID

# Wait for the background process to exit
wait $PID
