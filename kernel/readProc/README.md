While it is commonly suggested that you should NOT read files from within the
kernel, it IS possible &mdash; [Reading files from the linux
kernel](https://www.howtoforge.com/reading-files-from-the-linux-kernel-space-module-driver-fedora-14).

For our use cases, we would be most interested in reading the special system
files from `/proc` or `/sys` to gain access to system-maintained metrics in a
(mostly) kernel-independent fashion.

This is a quick, simple test module that does just that.

# Building on Centos8
To build this test module on Centos7/8, you will need kernel development
packages:
``` bash
sudo yum install -y kernel-devel kernel-headers
```

With these packages and the source and Makefile from this directory, build and
test:

- In one window, start watching `/var/log/messages`
  ``` bash
  sudo tail -f /var/log/messages
  ```
- In a second window, build and install the module:
  ``` bash
  cd %src-path%/custom-collectors/kernel
  make
  ...

  # IF the module built properly, install and immediately remove the module
  #
  # When loaded, the module will open 2 /proc files (/proc/stat and
  # /proc/meminfo) in-turn, output the contents of each line-by-line
  # (via printk to /var/log/messages), and close the file.
  #
  # Since the entire process occurs on load, it is safe to immedately remove
  # the module.
  #
  sudo insmod readProc.ko; sudo rmmod readProc
  ```
