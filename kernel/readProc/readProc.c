#include <linux/module.h> // Needed by all modules
#include <linux/kernel.h> // Needed for KERN_INFO
#include <linux/fs.h>     // Needed by filp
#include <linux/slab.h>   // kzalloc() / kfree()
#include <asm/uaccess.h>  // Needed by segment descriptors

// Avoid a kernel tail that disables required kernel symbols
MODULE_LICENSE("GPL");
MODULE_AUTHOR("D. ELmo Peele");
MODULE_DESCRIPTION("Test to see if we can read /proc");
MODULE_VERSION("0.0.1");

/**
 *  A callback method for _read_proc() to process a single line of proc file
 *  data.
 *
 *  @type   ProcessLine
 *  @param  buf     The line buffer to process {char*};
 *
 *  @return void
 */
typedef void (*ProcessLine) ( char* buf );

/**
 *  Read a (/proc) file and output line-by-line.
 *
 *  @method _read_proc
 *  @param  path        The absolute path to the target proc file {char*};
 *  @param  proc_line() A callback to invoke for each line {Function}
 *                        void ( char* buf );
 *
 *  @return void
 *  @private
 */
static void _read_proc( char* path, ProcessLine proc_line ) {
  struct file*  fp  = NULL;

  // To see in /var/log/messages that the module is operating
  printk(KERN_INFO "readProc._read_proc(): open '%s' ...\n", path);

  fp = filp_open( path, O_RDONLY, 0);
  if (fp == NULL) {
    printk(KERN_ALERT "readProc._read_proc(): filp_open error!!\n");

  } else {
    unsigned int  bufSize   = 4096;
    char*         buf;
    unsigned int  segment;
    unsigned int  idex;
    mm_segment_t  fs;

    printk( KERN_INFO "readProc._read_proc(): '%s' opened ...\n", path);

    buf = (char*) kzalloc( bufSize, GFP_KERNEL );

    printk( KERN_INFO "readProc._read_proc(): %d bytes allocated for buf ...\n",
            bufSize);

    // Get current segment descriptor
    fs = get_fs();

    // Set segment descriptor associated to kernel space
    set_fs( get_ds() );

    // Read the file
    fp->f_op->read( fp, buf, bufSize, &fp->f_pos );

    printk( KERN_INFO "readProc._read_proc(): read %lld bytes ...\n", fp->f_pos );

    // Restore segment descriptor
    set_fs(fs);

    // Process the data line-by-line
    for (segment = idex = 0; idex < fp->f_pos; idex++) {
      if (buf[idex] == '\n') {
        buf[idex] = '\0';

        /*
        printk( KERN_INFO " %d .. %d: '%s'\n",
                segment, idex, &buf[segment] );
        // */
        proc_line( &buf[segment] );

        segment = idex + 1;
      }
    }
    if (idex > segment) {
      // One final line
      proc_line( &buf[segment] );
    }

    printk( KERN_INFO "readProc._read_proc(): free buf ...\n");
    kfree( buf );

    printk( KERN_INFO "readProc._read_proc(): done\n");
  }
  filp_close( fp, NULL );
}

/**
 *  A callback method for _read_proc() to process a single line of proc file
 *  data from '/proc/stat' (cpu information).
 *
 *  @method _procStatLine
 *  @param  buf     The line buffer to process {char*};
 *
 *  Lines should have the format:
 *    '%source% %int-value%...'
 *
 *    Sources are:
 *      cpu, cpu0, cpu1, cpu[n], intr, ctxt, btime, processes, procs_running,
 *      procs_blocked, softirq
 *
 *    Each source has a different set of integer values, each with different
 *    meaning.
 *
 *  @return void
 *  @private
 */
static void _procStatLine( char* buf ) {
  printk( KERN_INFO "statLine: '%s'\n", buf);
}

/**
 *  A callback method for _read_proc() to process a single line of proc file
 *  data from '/proc/meminfo' (memory allocation information).
 *
 *  @method _procMemLine
 *  @param  buf     The line buffer to process {char*};
 *
 *  Lines should have the format:
 *    '%VariableName%:  %int-value%[ %unit%]'
 *
 *    VariableName values are:
 *      MemTotal, MemFree, MemAvailable, Buffers, Cached, SwapCached, Active,
 *      Inactive, Active(anon), Inactive(anon), Active(file), Inactive(file),
 *      Unevictable, Mlocked, SwapTotal, SwapFree, Dirty, Writeback, AnonPages,
 *      Mapped, Shmem, KReclaimable, Slab, SReclaimable, SUnreclaim,
 *      KernelStack, PageTables, NFS_Unstable, Bounce, WritebackTmp,
 *      CommitLimit, Committed_AS, VmallocTotal, VmallocUsed, VmallocChunk,
 *      Percpu, HardwareCorrupted, AnonHugePages, ShmemHugePages,
 *      ShmemPmdMapped, FileHugePages, FilePmdMapped, HugePages_Total,
 *      HugePages_Free, HugePages_Rsvd, HugePages_Surp, Hugepagesize, Hugetlb,
 *      DirectMap4k, DirectMap2M
 *
 *    Unit should be:
 *      kB  = 1024 bytes
 *
 *    Each source has a different set of integer values, each with different
 *    meaning.
 *  @return void
 *  @private
 */
static void _procMemLine( char* buf ) {
  printk( KERN_INFO "memLine: '%s'\n", buf);
}

int init_module(void) {
  _read_proc( "/proc/stat",    _procStatLine );
  _read_proc( "/proc/meminfo", _procMemLine );

  return 0;
}

void cleanup_module(void) {
  printk( KERN_INFO "readProc: unloaded\n" );
}
