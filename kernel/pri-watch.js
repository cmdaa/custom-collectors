#!/usr/bin/env node
const Os = require('os');
  
/* Setting priority for the current process
 *
 * Normal scheduler (Round-Robin : TS):
 *   priority value:
 *    value   : priority  nice
 *    1       : 21        1
 *    0       : 20        0
 *    -1      : 19        -1
 *    -10     : 10        -10
 *    -20     : 0         -20
 *
 *    PID   USER     PR    NI   VIRT    RES    SHR S   %CPU  %MEM  TIME+ COMMAND
 *    19447 root       0 -20    2488    508    444 S   0.0   0.0   0:00.00 pri
 *
 * Real-time scheduler (RR):
 *   priority value:
 *    value       : priority  nice
 *    minPri[1]   : -2        0
 *    50          : -51       0
 *    98          : -99       0
 *    maxPri[99]  : rt        0
 *
 *   Interrupt handlers run at priority -51
 *
 *    int minPri  = sched_get_priority_min( SCHED_RR );     # 1
 *    int maxPri  = sched_get_priority_max( SCHED_RR );     # 99
 *    struct sched_param  param = {0};
 *
 *    param.sched_priority = (int)((minPri + maxPri) / 2);  # 50
 *    sched_setscheduler( pid, SCHED_RR, &param );
 *
 *    PID   USER     PR    NI   VIRT    RES    SHR S   %CPU  %MEM  TIME+ COMMAND
 *    19376 root     -51   0    2488    572    508 S   0.0   0.0   0:00.00 pri
 *
 * Os.constants.priority:
 *    PRIORITY_HIGHEST      -20
 *    PRIORITY_HIGH         -14
 *    PRIORITY_ABOVE_NORMAL -7
 *    PRIORITY_NORMAL       0
 *    PRIORITY_BELOW_NORMAL 10
 *    PRIORITY_LOW          19
 *
 ****************************************************************************
 * We cannot switch to the real-time scheduler from within node.
 *
 * The best we can do here is change the priority to -20, which equates to a
 * priority value of 0 and nice value of -20 in the round-robin (TS) scheduler.
 *
 * In fact, even when externally switched to the real-time scheduler with
 * priority 80 (chrt --rr -p 80), this will STILL show the initial priority
 * value from the round-robin (TS) scheduler.
 *
 * In this case, the scheduler will be real-time (RR) and priority value -81
 * with a nice value of 0.
 */
console.log('node PID[ %d ]', process.pid);

console.log('Priority[ %d ]: sleep 5s ...', Os.getPriority());

const timer = setInterval( () => {
  console.log('Priority[ %d ]: sleep 5s ...', Os.getPriority());
}, 5000 );
