import os
import setuptools
import metadata

with open( metadata.long_description.file, 'r') as fh: #{
  metadata.long_description.content = fh.read()
#}

with open('requirements.txt', 'r') as fh: #{
  requires = filter(lambda x: not x.startswith('#'), fh.readlines())
#}

#print('requires:', requires)

setuptools.setup(
  name                          = metadata.name,
  version                       = metadata.version,
  author_email                  = metadata.author.email,
  maintainer                    = metadata.maintainer.name,
  maintainer_email              = metadata.maintainer.email,
  description                   = metadata.description,
  long_description              = metadata.long_description.content,
  # Not valid for python2.7
  #long_description_content_type = metadata.long_description.type,
  url                           = metadata.url,
  license                       = 'Apache 2.0',

  classifiers                   = [
    # https://pypi.org/pypi?%3Aaction=list_classifiers
    #
    # Maturity:
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 4 - Beta',

    'License :: OSI Approved :: Apache Software License',

    'Programming Language :: Python :: 2',
    'Programming Language :: Python :: 2.7',

    'Operating System :: POSIX :: Linux',

    'Intended Audience :: System Administrators',

    'Topic :: System :: Monitoring',
    'Topic :: Utilities',
  ],

  # Not valid for python2.7
  #project_urls                  = {
  #  'Documentation' : 'https://gitlab.com/cmdaa/custom-collectors/blob/master/yum/README.md',
  #  'Source'        : 'https://gitlab.com/cmdaa/custom-collectors/tree/master/yum',
  #  'Tracker'       : 'https://gitlab.com/cmdaa/custom-collectors/issues',
  #},

  scripts                       = [ 'yum-logs' ],

  install_requires              = requires,

  # Not valid for python2.7
  #python_requires               = '>=2.7'
)
