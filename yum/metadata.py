#
# Meta-data about yum-logs
#
name        = 'yum-logs'
version     = '0.0.1'
description = """
A YUM transaction history collector that can retrieve and report on completed
yum transactions.

A report is comprised of JSON-formatted records, one for each transaction, that
includes the set of packages and, for each package, all primary
files/directories.

Reports may be published to `stdout`, `stderr`, or a TCP endpoint of the form
`tcp://%host%:%port%`.
"""
url         = 'https://gitlab.com/cmdaa/custom-collectors/tree/master/yum'

class author: #{
  name  = 'D. Elmo Peele'
  email = 'elmo.peele@gmail.com'
#}
maintainer  = author

class long_description: #{
  file  = 'README.md'
  type  = 'text/markdown'
#}
