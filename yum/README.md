`yum` is a python-2 application and as such we can leverage the python
libraries that it uses to retrieve recorded information about yum transactions.

### History
[history](history) is an example of retrieving information about all completed
yum transactions, all packages involved with each, and all files associated
with each package.

### yum-logs
[yum-logs](yum-logs) is a simple collector that retrieves information about
completed yum transactions, all packages involved with each, and all files
associated and reports each transaction to a publication location.

#### Installation
Install and uninstall may be performed using the included [Makefile](Makefile).
``` bash
$ make install
...
$ make uninstall
...
```

This will install/uninstall `yum-logs` in the system-defined directory for
python-2 applications.


#### Usage
``` bash
usage: yum-logs [-h] [--all-files] [--publish PUBLISH]
                [--transaction TRANSACTION]
                [--transaction-file TRANSACTION_FILE]

Retrieve yum log information

optional arguments:
  -h, --help            show this help message and exit
  --all-files, -a       Show all installed files not just primaries
  --publish PUBLISH, -p PUBLISH
                        The publication end-point [stdout]
  --transaction TRANSACTION, -t TRANSACTION
                        Show history transactions at or above the given id
  --transaction-file TRANSACTION_FILE, -f TRANSACTION_FILE
                        Store the last transaction id in the given file. If no
                        `transaction` is provided and this file exists, it
                        will be read to establish the id of the next
                        transaction to present.
```

The publication end-point may be: `stdout`, `stderr`, or a TCP endpoint of the
form `tcp://%host%:%port%`.

Transactions are reported as JSON-formatted strings and include:
- `tid` : the id of the yum transaction (monotonically increasing);
- `cmdline` : the yum command line arguments;
- `user` : the name/id of the use who ran the yum command;
- `return_code` : the return code of the yum transaction (0 == success);
- `beg_rpmdbversion` : the version of the RPM database at the beginning of the
  transaction;
- `end_rpmdbversion` : the version of the RPM database when the transaction
  completed;
- `beg_timestamp` : the timestamp (seconds since epoch) when the transaction
  started;
- `end_timestamp` : the timestamp (seconds since epoch) when the transaction
  completed;
- `packages` : an object containing the set of packages, indexed by package id,
  each an object that may contain:
  - `file` : installed files;
  - `dir` : installed directories;
  - `ghost` : "ghost" entries (I believe these are things like hard and soft
      links, but I'm not sure);
- `errors` : an array of any error messages;
- `output` : an array of any output messages;

#### systemd service
There is a timer-based systemd service definition in the [system](system)
sub-directory. Using this definition, systemd will run `yum-logs` every 15-30
minutes, using a 15 minute inactivity timer and randomized delay timer of 0-15
minutes.

There are several service-related rules in the includes [Makefile](Makefile)
- `service-install` : install the timer-based systemd yum-logs service
  (requires that `yum-logs` has already been installed);
- `service-uninstall` : disable, stop, and remove the yum-logs service;
- `service-start` : start the yum-logs service (after `service-install`);
- `service-stop` : stop the yum-logs service (after `service-install`);
- `service-enable` : enable the yum-logs service to be automatically started at
  boot;
- `service-disable` : disable the yum-logs service from being automatically
  started at boot;
- `service-status` : show the status of the yum-logs service;
- `service-logs` : show and follow the journald logs realted to the yum-logs
  service;


### Locking a package
There are a number of ways to exclude a yum package from being installed or
updated.

- Exclude package(s) from update during a `yum update` by using the `--exclude`
  flag and specifying the package(s) that should not be updated;

- Modify `/etc/yum.conf` to permanently exclude package(s) from update by
  adding `exclude=%package-name(s)-or-pattern%`;

- Modify a specific repository by adding `exclude=%package-name(s)-or-pattern%`
  to the repository configuration file in `/etc/yum.repos.d/`;

- Use the `yum-plugin-versionlock` plugin:
  ``` bash
  $ sudo yum install -y yum-plugin-versionlock
  ...

  $ yum versionlock list
  Loaded plugins: fastestmirror, versionlock
  versionlock list done

  # Lock `yum-plugin-versionlock` to the current version
  $ sudo yum versionlock add yum-plugin-versionlock
  Loaded plugins: fastestmirror, versionlock
  Adding versionlock on: 0:yum-plugin-versionlock-1.1.31-52.el7
  versionlock added: 1

  $ yum versionlock list
  Loaded plugins: fastestmirror, versionlock
  0:yum-plugin-versionlock-1.1.31-52.el7.*
  versionlock list done

  # Unlock `yum-plugin-versionlock`
  $ sudo yum versionlock delete yum-plugin-versionlock
  Loaded plugins: fastestmirror, versionlock
  Deleting versionlock for: 0:yum-plugin-versionlock-1.1.31-52.el7.*
  versionlock deleted: 1

  $ yum versionlock list
  Loaded plugins: fastestmirror, versionlock
  versionlock list done
  ```
