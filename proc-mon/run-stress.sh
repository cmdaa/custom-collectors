#!/bin/bash
#
# Place enough stress on the system that normal processes won't respond.
#
sudo stress --cpu 8 --io 8 --vm 8
