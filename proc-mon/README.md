If `clang` v10+, `llvm` v10+, and `bpftool` v6.7+ are installed, you may use
the `Makefile` in this directory directly without the need for
`libbpf-bootstrap`.

Otherwise, you will need to compile within a clone of
[libbpf-bootstrap](https://github.com/libbpf/libbpf-bootstrap):
- copy proc_mon*.[ch] to the `examples/c` subdirectory of the `libbpf-bootstrap`
  clone;
- update `examples/c/Makefile` to include 'proc_mon' in the `APPS` variable;
- invoke `make proc_mon` to create the `proc_mon` binary;

To see `printk()` messages from any of the compiled BPF programs (including
`proc_mon`), use the following command in a separate window:
```bash
$ sudo cat /sys/kernel/debug/tracing/trace_pipe
```
