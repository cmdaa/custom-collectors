#ifndef __PROC_MON_H
#define __PROC_MON_H

#define TASK_COMM_LEN     16
#define MAX_FILENAME_LEN  127
#define MAX_ARG_BYTES     180
#define MAX_CGROUPS       32

typedef struct event {
  __u64 timestamp;  // nanoseconds since boot
  __s32 pid;
  __s32 ppid;
  __u32 uid;
  __u32 gid;

  char  comm[TASK_COMM_LEN];
  char  filename[MAX_FILENAME_LEN];
  bool  is_exit;

  // Exit-related data {
  char  args[MAX_ARG_BYTES];
  __u32 arg_full;     // Full argument length : MAY be > MAX_ARG_BYTES
  __u32 arg_len;      // Available length     : < MAX_ARG_BYTES

  __u32 cgroups[MAX_CGROUPS];

  __u64 cpu_start;    // nanoseconds
  __u64 cpu_end;      // nanoseconds
  __u64 duration_ns;  // nanoseconds

  __u8  exit_code;
  __u8  signal;

  __u32 error_code;
  __u32 trap_num;

  __u32 cpu_user;     // nanoseconds
  __u32 cpu_sys;      // nanoseconds

  __u32 min_flt;      // Minor faults
  __u32 maj_flt;      // Major faults

  __u64 task_size;    // Task size (bytes)
  __u64 hiwater_rss;  // High-water mark of RSS usage
  __u64 hiwater_vm;   // High-water mark of virtual memory usage
  __u64 total_vm;     // Total pages mapped
  __u64 locked_vm;    // Pages with PG_mlocked set
  __u64 data_vm;      // VM_WRITE & ~VM_SHARED & ~VM_STACK
  __u64 exec_vm;      // VM_EXEC  & ~VM_WRITE  & ~VM_STACK
  __u64 stack_vm;     // VM_STACK
  // Exit-related data }
} event_t;

#endif /* __PROC_MON_H */
