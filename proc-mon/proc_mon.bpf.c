/* Process/Task information sources:
 *
 * task->
 *  state  (-1 unrunnable, 0 runnable, >0 stopped)
 *  exit_state
 *    linux/include/linux/sched.h
 *      task_state_index()  : (state | exit_state) & TASK_REPORT
 *      task_index_to_char()
 *      task_state_to_char()
 *
 *  pid    task_pid_nr(   task )
 *  ppid   task_ppid_nr(  task )
 *  pgrp   task_pgrp_vnr( task ) : __task_pid_nr_ns(tsk, PIDTYPE_PGID, NULL);
 *  signal
 *        ->cutime                        cpuUser
 *        ->cstime                        cpuSys
 *        ->cmin_flt                      memFaultsMinor
 *        ->cmaj_flt                      memFaultsMajor
 *        ->rlim[RLIMIT_NOFILE].rlim_max  fdLimitHard
 *        ->rlim[RLIMIT_NOFILE].rlim_cur  fdLimitSoft
 *                7
 *
 *  thread
 *    m68k      : signo, code
 *    xtensa    : error_code
 *    powerpc   : trap_nr
 *    unicore32 : trap_no, error_code
 *    mips      : trap_nr, error_code
 *    arm       : trap_no, error_code
 *    x86       : trap_nr, error_code
 *
 *    ->error_code
 *    ->trap_nr
 *
 *  mm = get_task_mm( task )
 *    ->total_vm                            memSize
 *
 *    get_mm_counter(mm, MM_FILEPAGES)
 *      + get_mm_counter(mm, MM_SHMEMPAGES) memShare
 *    memShare
 *      + get_mm_counter(mm, MM_ANONPAGES)  memRss
 *
 *  real_start_time  (added to bootTime : bf_ktime_get_ns())
 *    renamed `start_boottime` in v5.5 (cf25e24db61cc : 2019-11-13)
 *
 *  cgroups
 *    struct css_set*   cgroups
 *    struct list_head  cg_list
 *      for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
 *        //task->cgroups->subsys[ ssid ];
 *        //struct cgroup_subsys*       ss  = task->cgroups->subsys[ ssid ]
 *        struct cgroup_subsys_state* css = task_get_css( task, ssid );
 *        u32                         id;
 *
 *        rcu_read_lock();
 *          id = cgroup_v1_touch_rcu( ns, ssid, css );
 *          // CGROUP_SSID_TO_UUID( cgroup, CGROUP_SUBSYS_ID( ss ) )
 *          // CGROUP_SSID_TO_UUID( cgroup, ssid )
 *          //  => (CGROUP_ID(cgroup) * 100 + ssid)
 *          //      => <5.5.0 : cgroup->id
 *          //      => >=5.5.0: cgroup->kn->id
 *        rcu_read_unlock();
 *      }
 *
 *  get_task_cred( task )    ->uid
 *    bpf_get_current_uid_gid()   : (u64) ((gid << 32) | uid)
 *
 *  get_files_struct( task ) -> max_fds
 *
 *  bpf_get_current_comm( void* buf, u32 size );
 *
 *  (u64) bpf_get_current_cgroup_id()
 *
 *  bpf_get_func_arg_cnt(*ctx )
 *  bpf_get_func_arg(*ctx, n, *value )
 *
 * task->fs           (struct fs_struct*)
 * task->files        (struct files_struct*)
 * task->nsproxy      (struct nsproxy*)
 * task->signal       (struct signal_struct*)
 * task->last_siginfo (kernel_siginfo_t*)
 * task->cgroups      (struct css_set*)
 */
#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <bpf/bpf_core_read.h>
#include "proc_mon.h"

// Specify which optional calls to handle {
#define HANDLE_SIGNAL_GENERATE  0
#define HANDLE_SIGNAL_DELIVER   0
#define HANDLE_FORCE_SIG_INFO   0
// Specify which optional calls to handle }

char LICENSE[] SEC("license") = "Dual BSD/GPL";

typedef struct {
  u64   start_ts;

  /* Volatile information that is not available by the time
   * sched_process_exit() [handle_process_exit()] is invoked.
   *
   * From task->mm : mm_struct {
   */
  u64   task_size;
  u64   hiwater_rss;  // High-water mark of RSS usage
  u64   hiwater_vm;   // High-water mark of virtual memory usage
  u64   total_vm;     // Total pages mapped
  u64   locked_vm;    // Pages with PG_mlocked set
  u64   data_vm;      // VM_WRITE & ~VM_SHARED & ~VM_STACK
  u64   exec_vm;      // VM_EXEC  & ~VM_WRITE  & ~VM_STACK
  u64   stack_vm;     // VM_STACK

  /* struct mm_rss_stat rss_stat
   *  u64 count[NR_MM_COUNTERS];
   *
   *  enum {
   *    MM_FILEPAGES,   // Resident file mapping pages
   *    MM_ANONPAGES,   // Resident anonymous pages
   *    MM_SWAPENTS,    // Resident swap entries
   *    MM_SHMEMPAGES,  // Resident shared memory pages
   *    NR_MM_COUNTERS
   *  };
   */
  // From task->mm : mm_struct }

  char  args[MAX_ARG_BYTES];
  u32   arg_full;
  u32   arg_len;

} task_info_t;

struct {
  __uint(type,        BPF_MAP_TYPE_HASH);
  __uint(max_entries, 8192);
  __type(key,         pid_t);
  __type(value,       task_info_t);
} map_task_info SEC(".maps");

struct {
  __uint(type,        BPF_MAP_TYPE_RINGBUF);
  __uint(max_entries, 256 * 1024);
} map_rb SEC(".maps");

const volatile unsigned long long min_duration_ns = 0;

/**
 *  Gather task arguments.
 *
 *  @method gather_args
 *  @param  task    The target tast {struct task_struct*};
 *  @param  info    The event info collection area {task_info_t*};
 *
 *  @return void
 */
static void __attribute__((noinline))
gather_args(  struct task_struct* task,
              task_info_t*        info ) {

  u64 arg_start = BPF_CORE_READ( task, mm, arg_start );
  u64 arg_end   = BPF_CORE_READ( task, mm, arg_end );
  u64 nBytes    = arg_end - arg_start;

  info->arg_full = (u32)nBytes;

  /*
  bpf_printk("gather_args: args [ %x .. %x : %u ]",
              arg_start, arg_end, nBytes);
  // */

  if (nBytes >= MAX_ARG_BYTES)  {
    nBytes = MAX_ARG_BYTES - 1;

    bpf_printk("gather_args: truncate args to [ %u ]", nBytes);
  }

  // Read task arguments
  info->arg_len = (u32)nBytes;
  (void)bpf_core_read_user( info->args, (u32)nBytes, (void*)arg_start );
  /*
  if (! bpf_core_read_user( info->args, (u32)nBytes, (void*)arg_start )) {
    bpf_printk("gather_args: read %u bytes[ %s ]", nBytes, info->args);
  }
  // */
}

/**
 *  Gather volatile, exit-related information that is not available by the time
 *  sched_process_exit() [handle_process_exit()] is invoked.
 *
 *  @method gather_exit_info
 *
 *  @return void
 */
static void __attribute__((noinline)) gather_exit_info( ) {
  struct task_struct* task    = (struct task_struct *)bpf_get_current_task();
  pid_t               pid     = BPF_CORE_READ( task, pid );
  task_info_t         newInfo = {0};
  task_info_t*        info    = (task_info_t*)bpf_map_lookup_elem(&map_task_info,
                                                                  &pid);

  if (!info) {
    // Create a new entry with a starting timestamp from the task itself
    bpf_printk("gather_exit_info: Create a new task_info entry...");

    newInfo.start_ts = BPF_CORE_READ( task, real_start_time );

    info = &newInfo;
  }

  if (info) {
    info->task_size   = BPF_CORE_READ( task, mm, task_size );
    info->hiwater_rss = BPF_CORE_READ( task, mm, hiwater_rss );
    info->hiwater_vm  = BPF_CORE_READ( task, mm, hiwater_vm );
    info->total_vm    = BPF_CORE_READ( task, mm, total_vm );
    info->locked_vm   = BPF_CORE_READ( task, mm, locked_vm );
    info->data_vm     = BPF_CORE_READ( task, mm, data_vm );
    info->exec_vm     = BPF_CORE_READ( task, mm, exec_vm );
    info->stack_vm    = BPF_CORE_READ( task, mm, stack_vm );

    /*
    bpf_printk("gather_exit_info: pid[ %u : %u ]",
                pid, BPF_CORE_READ( task, pid ));
    bpf_printk("gather_exit_info: start[ %u ], start_time[ %u ], real[ %u ]",
                info->start_ts,
                BPF_CORE_READ( task, start_time ),
                BPF_CORE_READ( task, real_start_time ));

    bpf_printk("gather_exit_info: task_size    %u", info->task_size);
    bpf_printk("gather_exit_info: hiwater_rss  %u", info->hiwater_rss);
    bpf_printk("gather_exit_info: hiwater_vm   %u", info->hiwater_vm);
    bpf_printk("gather_exit_info: total_vm     %u", info->total_vm);
    bpf_printk("gather_exit_info: locked_vm    %u", info->locked_vm);
    bpf_printk("gather_exit_info: data_vm      %u", info->data_vm);
    bpf_printk("gather_exit_info: exec_vm      %u", info->exec_vm);
    bpf_printk("gather_exit_info: stack_vm     %u", info->stack_vm);

    bpf_printk("gather_exit_info: utime[ %u ], stime[ %u ], gtime[ %u ]",
                BPF_CORE_READ( task, utime ),
                BPF_CORE_READ( task, stime ),
                BPF_CORE_READ( task, gtime ));
    bpf_printk("gather_exit_info: mm faults min[ %u ], maj[ %u ]",
                BPF_CORE_READ( task, min_flt ),
                BPF_CORE_READ( task, maj_flt ));
    // */

    gather_args( task, info );

    bpf_map_update_elem(&map_task_info, &pid, info, BPF_ANY);
  }

  return;
}


/**
 *  Gather uid and gid information for the tiven task
 *
 *  @method gather_uid_gid
 *  @param  task    The target tast {struct task_struct*};
 *  @param  event   The event info collection area {event_t*};
 *
 *  @return void
 */
static void __attribute__((noinline))
gather_uid_gid( struct task_struct* task,
                event_t*            event ) {
  // (u64) ((gid << 32) | uid)
  u64 ugid  = bpf_get_current_uid_gid();

  event->uid = (u32)ugid; //(ugid &  0x0ffffffff);
  event->gid = (u32)(ugid >> 32);

  bpf_printk("gather_uid_gid: uid[ %lu ], gid[ %lu ]",
             event->uid, event->gid);
}


/**
 *  Gather cgroup ids for the given task.
 *
 *  @method gather_cgroups
 *  @param  task    The target tast {struct task_struct*};
 *  @param  event   The event info collection area {event_t*};
 *
 *    #define COUNT(a)  ((size_t) sizeof(a) / sizeof((a)[0]))
 *    assert( COUNT(task->cgroups) === CGROUP_SUBSYS_COUNT );
 *
 *    struct css_set* cgroups;
 *      struct cgroup_subsys_state* subsys[ CGROUP_SUBSYS_COUNT ];
 *        struct cgroup*        cgroup
 *          struct cgroup_subsys_state  self;
 *          unsigned long               flags;
 *          int                         id;
 *        struct cgroup_subsys* ss;
 *
 *    for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
 *      struct cgroup_subsys_state* css     = task->cgroups->subsys[ ssid ];
 *      struct cgroup*              cgroup  = css->cgroup;
 *      u32                         id      = CGROUP_ID( cgroup ) * 100 + ssid;
 *
 *      // CGROUP_ID( cgroup )
 *      //    => <5.5.0 : cgroup->id
 *      //    => >=5.5.0: cgroup->kn->id
 *    }
 *
 *  @return void
 */
static void __attribute__((noinline))
gather_cgroups( struct task_struct* task,
                event_t*            event ) {

  struct css_set*               cgroups = BPF_CORE_READ( task, cgroups );
  struct cgroup_subsys_state**  states  = BPF_CORE_READ( task, cgroups,
                                                          subsys );
  int                           ssid;

  /*
  bpf_printk("gather_cgroups: cgroups[ %x ], states[ %x ]",
            cgroups, states);
  bpf_printk("gather_cgroups: CGROUP_SUBSYS_COUNT[ %u ]",
            CGROUP_SUBSYS_COUNT);
  // */

  for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
    if (ssid >= MAX_CGROUPS) {
      bpf_printk("gather_cgroups: maximum reportable cgroup @%d, skipping %d",
                  ssid, (CGROUP_SUBSYS_COUNT - ssid));
      break;
    }

    struct cgroup_subsys_state* css     = states[ ssid ];
    u32                         id      = BPF_CORE_READ( css, id );

    /* css->flags:
     *    CSS_NO_REF    (1 << 0)  : 1   0x01
     *    CSS_ONLINE    (1 << 1)  : 2   0x02
     *    CSS_RELEASED  (1 << 2)  : 4   0x04
     *    CSS_VISIBLE   (1 << 3)  : 8   0x08
     *    CSS_DYING     (1 << 4)  : 16  0x10
     *
     *    0x0A  == 1010 :             CSS_ONLINE, CSS_VISIBLE
     *    0x0B  == 1011 : CSS_NO_REF, CSS_ONLINE, CSS_VISIBLE
     */
    /*
    const char* ssName  = BPF_CORE_READ( css, ss, name );
    u32         flags   = BPF_CORE_READ( css, flags );

    bpf_printk("gather_cgroups: ssid[ %u : %s ], id[ %u ], flags[ 0x%x ]",
                ssid, ssName, id, flags);
    // */

    /*
    const char  name[32]  = {0};

    bpf_core_read_str((void*)name, sizeof(name), ssName);
    bpf_printk("gather_cgroups: ssid[ %u ], copied name[ %s ]",
                ssid, name);
    // */

    event->cgroups[ ssid ] = id;
  }
}

/**
 *  Handle a sched_process_exec tracepoint.
 *  @method handle_process_exec
 *  @param  ctx   The tracepoint context
 *                {struct trace_event_raw_sched_process_exec *};
 *
 *  This tracepoint is invoked on any exec-related syscall.
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/sched/sched_process_exec")
int handle_process_exec(struct trace_event_raw_sched_process_exec *ctx)
{
  struct task_struct* task  = (struct task_struct *)bpf_get_current_task();
  pid_t               pid   = BPF_CORE_READ( task, pid );
                            // bpf_get_current_pid_tgid() >> 32;
  event_t*            evt;
  task_info_t         info  = {0};
  unsigned            fname_off;
  int idex;


  /* remember time exec() was executed for this PID */
  info.start_ts = BPF_CORE_READ( task, real_start_time ); //bpf_ktime_get_ns();

  bpf_printk("sched_process_exec: pid[ %u ], start[ %llu ]",
              pid, info.start_ts);

  /* :XXX: Task arguments are NOT available at this point
  gather_args( task, &info );
  // */

  bpf_map_update_elem(&map_task_info, &pid, &info, BPF_ANY);

  /* don't emit exec events when minimum duration is specified */
  if (min_duration_ns) {
    return 0;
  }

  /* reserve sample from BPF ringbuf */
  evt = bpf_ringbuf_reserve(&map_rb, sizeof(*evt), 0);
  if (!evt) {
    return 0;
  }

  /* fill out the sample with data */
  evt->is_exit    = false;
  evt->timestamp  = bpf_ktime_get_ns();
  evt->pid        = pid;
  evt->ppid       = BPF_CORE_READ(task, real_parent, tgid);

  evt->cpu_start = info.start_ts;

  gather_uid_gid( task, evt );

  bpf_get_current_comm(&evt->comm, sizeof(evt->comm));

  fname_off = ctx->__data_loc_filename & 0xFFFF;
  bpf_probe_read_str(&evt->filename, sizeof(evt->filename),
                     (void *)ctx + fname_off);

  bpf_printk("sched_process_exec: pid = %d, comm = %s, filename = %s",
              evt->pid, evt->comm, evt->filename);

  // Include cgroups in the event
  gather_cgroups( task, evt );

  /* submit it to user-space for post-processing */
  bpf_ringbuf_submit(evt, 0);

  return 0;
}

/**
 *  Handle a sched_process_exit tracepoint.
 *  @method handle_process_exit
 *  @param  ctx   The tracepoint context
 *                {struct trace_event_raw_sched_process_exec *};
 *
 *  This tracepoint is invoked when a process is terminated.
 *
 *  At the time of this call, a large portion of the task has already been
 *  zeroed (e.g. task->thread, task->mm) so information that WAS contained
 *  there should have bee collected via other, coordinating tracepoints.
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/sched/sched_process_exit")
int handle_process_exit(struct trace_event_raw_sched_process_template* ctx)
{
  struct task_struct* task        = (struct task_struct *)bpf_get_current_task();
  u64                 id          = bpf_get_current_pid_tgid();
  pid_t               pid         = id >> 32;
  pid_t               tid         = (u32)id;
  u64                 duration_ns = 0;
  u64                 end_ts      = 0;
  task_info_t*        info        = NULL;
  event_t*            evt         = NULL;
  unsigned            exit_code;

  bpf_printk("sched_process_exit: pid/tid[ %d / %d ] ...",
             pid, tid);

  bpf_printk("sched_process_exit: error_code[ %d ]",
             BPF_CORE_READ( task, thread.error_code ));
  bpf_printk("sched_process_exit: trap_nr[ %d ]",
             BPF_CORE_READ( task, thread.trap_nr ));

  /* ignore thread exits */
  if (pid != tid) {
    bpf_printk("sched_process_exit: pid/tid[ %d / %d ] -- ignore thread exit",
               pid, tid);

    return 0;
  }

  /* if we recorded start of the process, calculate lifetime duration */
  info = (task_info_t*)bpf_map_lookup_elem(&map_task_info, &pid);
  if (info) {
    end_ts      = bpf_ktime_get_ns();
    duration_ns = end_ts - info->start_ts;

  } else if (min_duration_ns) {
    bpf_printk("sched_process_exit: no info w/min_duration_ns -- done",
               pid, tid);

    goto done;
  }

  // assert( info != NULL );

  /* if process didn't live long enough, return early */
  if (min_duration_ns && duration_ns < min_duration_ns) {
    bpf_printk("sched_process_exit: duration[ %u ] < min[ %u ] -- done",
               duration_ns, min_duration_ns);
    goto done;
  }

  /* reserve sample from BPF ringbuf */
  evt = bpf_ringbuf_reserve(&map_rb, sizeof(*evt), 0);
  if (!evt) {
    bpf_printk("sched_process_exit: Cannot reserve rinbuffer space -- done");
    goto done;
  }

  evt->is_exit    = true;
  evt->timestamp  = bpf_ktime_get_ns();
  evt->pid        = pid;
  evt->ppid       = BPF_CORE_READ(task, real_parent, tgid);

  gather_uid_gid( task, evt );

  evt->cpu_start   = (info ? info->start_ts
                           : BPF_CORE_READ( task, real_start_time ));
  evt->cpu_end     = end_ts;
  evt->duration_ns = duration_ns;

  evt->error_code  = BPF_CORE_READ( task, thread.error_code );
  evt->trap_num    = BPF_CORE_READ( task, thread.trap_nr );


  /* :XXX: The exit_code seems to have the format:
   *        0xff 7f
   *          ^^ ^^
   *          |  |
   *          |  +-- signal (minus the top bit => mask 0x7f)
   *          +----- process exit code
   */
  exit_code      = BPF_CORE_READ(task, exit_code);
  evt->signal    = (exit_code & 0x7f);
  evt->exit_code = (exit_code >> 8) & 0x0ff;

  if (evt->signal > 0 && evt->exit_code == 0) {
    // Return the full exit_code instead of a masked value
    evt->exit_code = exit_code;
  }

  evt->cpu_user = BPF_CORE_READ( task, utime );
  evt->cpu_sys  = BPF_CORE_READ( task, stime );
  evt->min_flt  = BPF_CORE_READ( task, min_flt );
  evt->maj_flt  = BPF_CORE_READ( task, maj_flt );

  // /*
  bpf_printk("sched_process_exit: exit_code[ 0x%x ], signal[ %u ], exit[ %u ]",
              exit_code, evt->signal, evt->exit_code);
  // */

  if (info) {
    // Copy in volatile task information collected earlier
    evt->task_size    = info->task_size;
    evt->hiwater_rss  = info->hiwater_rss;
    evt->hiwater_vm   = info->hiwater_vm;
    evt->total_vm     = info->total_vm;
    evt->locked_vm    = info->locked_vm;
    evt->data_vm      = info->data_vm;
    evt->exec_vm      = info->exec_vm;
    evt->stack_vm     = info->stack_vm;

    evt->arg_full     = info->arg_full;
    evt->arg_len      = info->arg_len;

    /* :XXX: MUST use a constant here. If we use `info->arg_len`, BPF will not
     *       be able to verify that this is a bounded loop and will refuse to
     *       load the module.
     */
    bpf_probe_read( &(evt->args[0]), MAX_ARG_BYTES, &(info->args[0]) );
  }

  // Fetch the task name
  bpf_get_current_comm(&evt->comm, sizeof(evt->comm));

  // Include cgroups in the event
  gather_cgroups( task, evt );

  /* send data to user-space for post-processing */
  bpf_ringbuf_submit(evt, 0);

 done:
  if (info) { bpf_map_delete_elem(&map_task_info, &pid); }

  return 0;
}

/****************************************************************************
 * sys_enter_exit {
 *
 */

/* Calling context for tp/syscalls/sys_enter_exit
 *    (also used by: sys_enter_exit_group)
 *
 *    From /sys/kernel/debug/tracing/events/syscalls/sys_enter_exit/format
 *
 *    name: sys_enter_exit
 *    ID: 145
 *    format:
 *      field:unsigned short common_type; offset:0;       size:2; signed:0;
 *      field:unsigned char common_flags; offset:2;       size:1; signed:0;
 *      field:unsigned char common_preempt_count;
 *                                        offset:3;       size:1; signed:0;
 *
 *      field:int common_pid;             offset:4;       size:4; signed:1;
 *
 *      field:int __syscall_nr;           offset:8;       size:4; signed:1;
 *      field:int error_code;             offset:16;      size:8; signed:0;
 *
 *      print fmt: "error_code: 0x%08lx", ((unsigned long)(REC->error_code))
 */
typedef struct {
  u64 unused;
  u32 syscall_nr;
  u64 error_code;
} sys_enter_exit_args_t;

/**
 *  Handle a sys_enter_exit tracepoint.
 *  @method handle_sys_exit
 *  @param  ctx   The tracepoint context
 *                {sys_enter_exit_args_t*};
 *
 *  This tracepoint is invoked IN SOME CASES on entry to the exit syscall
 *  when a process invokes exit() and is followed by a call to
 *  sched_process_exit().
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/syscalls/sys_enter_exit")
int handle_sys_exit(sys_enter_exit_args_t* ctx)
{
  gather_exit_info();

  // /*
  {
    u64   id  = bpf_get_current_pid_tgid();
    pid_t pid = id >> 32;
    pid_t tid = (u32)id;

    bpf_printk("sys_exit: pid/tid[ %d / %d ], error_code[ %d ]",
               pid, tid, ctx->error_code);
  }
  // */

  return 0;
}

/**
 *  Handle a sys_enter_exit_group tracepoint.
 *  @method handle_sys_exit_group
 *  @param  ctx   The tracepoint context
 *                {sys_enter_exit_args_t*};
 *
 *  This tracepoint is invoked IN MOST CASES on entry to the exit_group
 *  syscall when a process invokes exit() and is followed by a call to
 *  sched_process_exit().
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/syscalls/sys_enter_exit_group")
int handle_sys_exit_group(sys_enter_exit_args_t* ctx)
{
  gather_exit_info();

  // /*
  {
    u64   id  = bpf_get_current_pid_tgid();
    pid_t pid = id >> 32;
    pid_t tid = (u32)id;

    bpf_printk("sys_exit_group: ctx.syscall_nr[ %u ]",
               ctx->syscall_nr);
    bpf_printk("sys_exit_group: pid/tid[ %d / %d ], error_code[ %d ]",
               pid, tid, ctx->error_code);
  }
  // */

  return 0;
}

/* sys_enter_exit }
 ****************************************************************************
 * signal_generate {
 *
 */

#if HANDLE_SIGNAL_GENERATE != 0 // {

/* Calling context for tp/signal/signal_generate
 *    From /sys/kernel/debug/tracing/events/signal/signal_generate/format
 *
 *    name: signal_generate
 *    ID: 186
 *    format:
 *      field:unsigned short common_type; offset:0;       size:2; signed:0;
 *      field:unsigned char common_flags; offset:2;       size:1; signed:0;
 *      field:unsigned char common_preempt_count;
 *                                        offset:3;       size:1; signed:0;
 *
 *      field:int common_pid;             offset:4;       size:4; signed:1;
 *
 *      field:int sig;                    offset:8;       size:4; signed:1;
 *      field:int errno;                  offset:12;      size:4; signed:1;
 *      field:int code;                   offset:16;      size:4; signed:1;
 *      field:char comm[16];              offset:20;      size:16; signed:1;
 *      field:pid_t pid;                  offset:36;      size:4; signed:1;
 *      field:int group;                  offset:40;      size:4; signed:1;
 *      field:int result;                 offset:44;      size:4; signed:1;
 *
 *      print fmt: "sig=%d errno=%d code=%d comm=%s pid=%d grp=%d res=%d",
 *              REC->sig, REC->errno, REC->code, REC->comm, REC->pid,
 *              REC->group, REC->result
 */
typedef struct {
  u64   unused;
  u32   sig;
  s32   errno;
  u32   code;
  char  comm[16];
  u32   group;
  s32   result;
} signal_generate_args_t;

/**
 *  Handle a signal_generate tracepoint.
 *  @method handle_signal_generate
 *  @param  ctx   The tracepoint context
 *                {signal_generate_args_t *};
 *
 *  This tracepoint is invoked when a signal is generated for delivery to a
 *  process (e.g. SIGILL). The signal may or may not be delivered via
 *  signal_deliver() to the process depending upon signal ignore settings
 *  and/or debugger intervention.
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/signal/signal_generate")
int handle_signal_generate(signal_generate_args_t* ctx)
{
  struct task_struct* task  = (struct task_struct *)bpf_get_current_task();
  u64   id  = bpf_get_current_pid_tgid();
  pid_t pid = id >> 32;
  pid_t tid = (u32)id;

  bpf_printk("signal_generate: pid/tid[ %d / %d ]", pid, tid);
  bpf_printk("signal_generate: sig    [ %u ]", ctx->sig);
  bpf_printk("signal_generate: errno  [ %d ]", ctx->errno);
  bpf_printk("signal_generate: code   [ %u ]", ctx->code);
  bpf_printk("signal_generate: result [ %u ]", ctx->result);

  return 0;
}

#endif  // HANDLE_SIGNAL_GENERATE }

/* signal_generate }
 ****************************************************************************
 * signal_deliver {
 *
 */

#if HANDLE_SIGNAL_DELIVER != 0 // {

/* Calling context for tp/signal/signal_deliver
 *    From /sys/kernel/debug/tracing/events/signal/signal_deliver/format
 *
 *    name: signal_deliver
 *    ID: 185
 *    format:
 *      field:unsigned short common_type; offset:0;       size:2; signed:0;
 *      field:unsigned char common_flags; offset:2;       size:1; signed:0;
 *      field:unsigned char common_preempt_count;
 *                                        offset:3;       size:1; signed:0;
 *
 *      field:int common_pid;             offset:4;       size:4; signed:1;
 *
 *      field:int sig;                    offset:8;       size:4; signed:1;
 *      field:int errno;                  offset:12;      size:4; signed:1;
 *      field:int code;                   offset:16;      size:4; signed:1;
 *      field:unsigned long sa_handler;   offset:24;      size:8; signed:0;
 *      field:unsigned long sa_flags;     offset:32;      size:8; signed:0;
 *
 *      print fmt: "sig=%d errno=%d code=%d sa_handler=%lx sa_flags=%lx",
 *          REC->sig, REC->errno, REC->code, REC->sa_handler, REC->sa_flags
 */
typedef struct {
  u64   unused;
  u32   sig;
  s32   errno;
  u32   code;
  u64   sa_handler;
  u64   sa_flags;
} signal_deliver_args_t;

/**
 *  Handle a signal_deliver tracepoint.
 *  @method handle_signal_deliver
 *  @param  ctx   The tracepoint context
 *                {signal_deliver_args_t *};
 *
 *  This tracepoint is invoked when a generated signal is finally delivered to
 *  a process.
 *
 *  @note See /sys/kernel/debug/tracing/events for available tracepoints.
 *
 *  @return 0 {int};
 */
SEC("tp/signal/signal_deliver")
int handle_signal_deliver(signal_deliver_args_t* ctx)
{
  struct task_struct* task  = (struct task_struct *)bpf_get_current_task();
  u64   id  = bpf_get_current_pid_tgid();
  pid_t pid = id >> 32;
  pid_t tid = (u32)id;

  bpf_printk("signal_deliver: pid/tid   [ %d / %d ]", pid, tid);
  bpf_printk("signal_deliver: sig       [ %u ]", ctx->sig);
  bpf_printk("signal_deliver: errno     [ %d ]", ctx->errno);
  bpf_printk("signal_deliver: code      [ %u ]", ctx->code);
  bpf_printk("signal_deliver: sa_handler[ 0x%x ]", ctx->sa_handler);
  bpf_printk("signal_deliver: sa_flags  [ 0x%x ]", ctx->sa_flags);

  return 0;
}

#endif  // HANDLE_SIGNAL_DELIVER }

/* signal_deliver }
 ****************************************************************************
 * force_sig_info {
 *
 */

#if HANDLE_FORCE_SIG_INFO != 0  // {

/**
 *  Handle a force_sig_info kprobe.
 *  @method handle_force_sig_info
 *  @param  info  The kernel signal information {struct kernel_siginfo*};
 *
 *  This kprobe is invoked when the kernel needs to force the termination of a
 *  process due to a fault of some sort. This injects additional information
 *  into the task structure and is followed by a forced exit via
 *  sched_process_exit().
 *
 *
 *  force_sig_info_to_task( struct siginfo* info, struct task_struct* task );
 *
 *  force_sig_info( struct siginfo* info );
 *    force_sig_info_to_task( info, current );
 *
 *  force_sig( int sig )
 *    struct kernel_siginfo info;
 *      info.si_signo = sig;        (int)
 *      info.si_errno = 0;          (int)
 *      info.si_code  = SI_KERNEL;  (int)
 *      info.si_pid   = 0;          _sifields._kill._pid
 *      info.si_uid   = 0;          _sifields._kill._uid
 *                              _sifields._sigfault._trapno
 *  @return 0 {int};
 */
SEC("kprobe/force_sig_info")
int BPF_KPROBE(handle_force_sig_info, struct kernel_siginfo*  siginfo) {
  gather_exit_info();

  bpf_printk("force_sig_info: si_signo [ %x ]",
                BPF_CORE_READ(siginfo, si_signo));
  bpf_printk("force_sig_info: si_errno [ %x ]",
                BPF_CORE_READ(siginfo, si_errno));
  bpf_printk("force_sig_info: si_code [ %x ]",
                BPF_CORE_READ(siginfo, si_code));

  return 0;
}

#endif  // HANDLE_FORCE_SIG_INFO }

/* force_sig_info }
 ****************************************************************************/
