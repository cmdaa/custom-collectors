#!/bin/bash
#
# Run a proc-mon image.
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

#BUILD_IMAGE="${1:-debian:11.4-slim}"
BUILD_IMAGE='cmdaa/proc-mon'

echo -n ">>> Running $BUILD_IMAGE"

docker run --rm -it \
  --privileged \
  --volume  /sys/kernel/btf/vmlinux:/root/vmlinux \
    $BUILD_IMAGE \
      /bin/bash
