#!/bin/bash
#
# Watch this process along with the `proc_mon` BPF process and the `stress`
# process
#
# Increase priority so we will continue to run under load
sudo chrt --rr -p 89 $$
while [ 1 ]; do
  echo "$(date +'%T')"
  ps axo pid,comm,pri,nice,cls | \
    awk '/proc_mon|^  *'$$'/{print}/stress/{if(!done){print; done=1}}'
  sleep 1
  echo "---------------------------------------------------"
done
