#include <argp.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>       // write(), close(), gethostname();
#include <stdarg.h>       // va_list, va_start(), va_end()
#include <time.h>
#include <stdlib.h>       // getenv(); atoi();
#include <string.h>       // strncpy(); strerror(); bcopy();
#include <sys/time.h>     // gettimeofday(); struct timeval
#include <sys/resource.h>
#include <bpf/libbpf.h>

#include <sys/socket.h>   // socket();
#include <arpa/inet.h>    // inet_addr();
#include <netdb.h>        // gethostbyname();, h_errno; hstrerror();

#include "proc_mon.h"
#include "proc_mon.skel.h"

// Specify which optional labels to include {
#define INCLUDE_IS_EXIT 1
// Specify which optional labels to include }

// Running state initialized via command line arg (argp)
static struct state {
  bool  verbose;
  long  min_duration_ms;
  bool  exiting;
  bool  kafka;
  bool  prom;
  char* tcp_host;
  char* metric_class;
  int   tcp_port;
  int   socket;
} state = {
  false,      // verbose
  0,          // min_duratin_ms
  false,      // exiting
  false,      // kafka
  false,      // prom
  NULL,       // tcp_host
  "process",  // metric_class
  0,          // tcp_port
  0           // socket
};

// argp information
const char* argp_program_version      = "proc_mon 0.0.9";
const char* argp_program_bug_address  = "<cmdaa@gmail.com>";
const char  argp_program_doc[]        =
"BPF proc_mon demo application.\n"
"\n"
"It traces process start and exits and shows associated \n"
"information (filename, process duration, PID and PPID, etc).\n"
"\n"
"USAGE: proc_mon [-d <min-duration-ms>] [-k | -p] [-t tcp-endpoint] [-v]\n";

static const struct argp_option opts[] = {
  {"verbose", 'v',NULL,          0,"Verbose debug output"},
  {"kafka",   'k',NULL,          0,"Output kafka records"},
  {"prom",    'p',NULL,          0,"Output prometheus records"},
  {"tcp",     't',"TCP-ENDPOINT",0,"Output to a TCP endpoint: host:port"},
  {"duration",'d',"DURATION-MS", 0,"Minimum process duration (ms) to report"},
  {},
};

// Hostname
static char   g_hostname[128];

static const char*  field_units[] = {
  /* (default : 'seconds'
   *    cpu_start
   *    cpu_user
   *    cpu_sys
   * )
   *
   * Report field   : unit */
  "mem_faults_minor", "count",  // min_flt
  "mem_faults_major", "count",  // maj_flt
  "mem_size",         "bytes",  // task_size

  "mem_rss",          "pages",  // hiwater_rss

  // Unused
  "hiwater_vm",       "pages",  // hiwater_vm
  "total_vm",         "pages",  // total_vm
  "locked_vm",        "pages",  // locked_vm
  "data_vm",          "pages",  // data_vm
  "exec_vm",          "pages",  // exec_vm
  "stack_vm",         "pages",  // stack_vm

  // Unavailable
  "fd_open",          "count",
  "fd_limit_hard",    "count",
  "fd_limit_soft",    "count",
  "mem_share",        "pages",
};

/**
 *  Return the current timestamp in milliseconds.
 *
 *  @method get_timestamp
 *
 *  @return The current time (ms) {u64};
 */
u64
get_timestamp() {
  struct timeval  tv  = {0};
  u64             ms  = 0;

  gettimeofday(&tv, NULL);

  ms = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);

  return ms;
}

/**
 *  Return a string version of the given signal.
 *
 *  @method get_signal_str
 *  @param  signal    The target signal {int};
 *
 *  @note   We use this instead of strsignal() from string.h to get strings
 *          that better mirror the actual constants.
 *
 *  @return A static string {char*};
 *  @private
 */
const char*
get_signal_str( int signal ) {
  switch( signal ) {
    case SIGHUP:    return "SIGHUP";    // 1
    case SIGINT:    return "SIGINT";    // 2
    case SIGQUIT:   return "SIGQUIT";   // 3
    case SIGILL:    return "SIGILL";    // 4
    case SIGTRAP:   return "SIGTRAP";   // 5
    case SIGABRT:   return "SIGABRT";   // 6  aka SIGIOT
    case SIGBUS:    return "SIGBUS";    // 7
    case SIGFPE:    return "SIGFPE";    // 8
    case SIGKILL:   return "SIGKILL";   // 9
    case SIGUSR1:   return "SIGUSR1";   // 10
    case SIGSEGV:   return "SIGSEGV";   // 11
    case SIGUSR2:   return "SIGUSR2";   // 12
    case SIGPIPE:   return "SIGPIPE";   // 13
    case SIGALRM:   return "SIGALRM";   // 14
    case SIGTERM:   return "SIGTERM";   // 15
    case SIGSTKFLT: return "SIGSTKFLT"; // 16
    case SIGCHLD:   return "SIGCHLD";   // 17
    case SIGCONT:   return "SIGCONT";   // 18
    case SIGSTOP:   return "SIGSTOP";   // 19
    case SIGTSTP:   return "SIGTSTP";   // 20
    case SIGTTIN:   return "SIGTTIN";   // 21
    case SIGTTOU:   return "SIGTTOU";   // 22
    case SIGURG:    return "SIGURG";    // 23
    case SIGXCPU:   return "SIGXCPU";   // 24
    case SIGXFSZ:   return "SIGXFSZ";   // 25
    case SIGVTALRM: return "SIGVTALRM"; // 26
    case SIGPROF:   return "SIGPROF";   // 27
    case SIGWINCH:  return "SIGWINCH";  // 28
    case SIGIO:     return "SIGIO";     // 29 aka SIGPOLL, SIGLOST
    case SIGPWR:    return "SIGPWR";    // 30
    case SIGSYS:    return "SIGSYS";    // 31 aka SIGUNUSED
  }

  return "SIG_UNKNOWN";
}

/**
 *  Return a unit version of the given field.
 *
 *  @method get_field_unit
 *  @param  field   The name of the target field {const char*};
 *
 *    _procUnit( key ) {
 *      let unit  = 'seconds';
 *
 *      switch( key ) {
 *        case 'memRss':
 *        case 'memShare':
 *        case 'memSize':
 *          unit = 'bytes'
 *          break;
 *
 *        case 'fdOpen':
 *        case 'fdLimitHard':
 *        case 'fdLimitSoft':
 *        case 'memFaultsMinor':
 *        case 'memFaultsMajor':
 *          unit = 'count'
 *          break;
 *      }
 *
 *  @return A static string {char*};
 *  @private
 */
const char*
get_field_unit( const char* field ) {
  int nUnits  = (sizeof(field_units) / sizeof(field_units[0]));
  int idex;

  for (idex = 0; idex < nUnits; idex += 2) {
    if (! strcmp( field, field_units[idex] )) {
      return field_units[idex+1];
    }
  }

  return "seconds";
}

/**
 *  Parse an incoming argument
 *  @method parse_arg
 *  @param  key   The argument key (minus any leading '-') {int};
 *  @param  arg   The argument value {char*};
 *  @param  state The argument collection state {struct argp_state};
 *
 *  @return An error indicator (0 == success) {error_t};
 *  @private
 */
static error_t
parse_arg(int key, char *arg, struct argp_state *argp_state) {
  switch (key) {
  case 'v':
    state.verbose = true;
    break;

  case 'd':
    errno = 0;
    state.min_duration_ms = strtol(arg, NULL, 10);
    if (errno || state.min_duration_ms <= 0) {
      fprintf(stderr, "*** Invalid duration: %s\n", arg);
      argp_usage(argp_state);
    }
    break;

  case 't': {
    int len = strnlen( arg, 128 );
    char* end = &arg[len];
    char* cur = arg;

    state.tcp_host = arg;

    while (cur < end) {
      if (*cur == ':') {
        *cur = '\0';

        state.tcp_port = atoi( cur+1 );
        break;
      }
      cur++;
    }
  } break;

  case 'k':
    state.kafka = true;
    break;

  case 'p':
    state.prom = true;
    break;

  case ARGP_KEY_ARG:
    argp_usage(argp_state);
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

/**
 *  Handle print for eBPF errors and debug.
 *  @method libbpf_print_fn
 *  @param  level   The print/debug/log level {enum libbpf_print_level};
 *  @param  format  The print format {const char*};
 *  @param  ...     format-related arguments
 *
 *  @return The number of bytes written {int};
 *  @private
 */
static int
libbpf_print_fn( enum libbpf_print_level level,
                 const char*             format,
                 va_list                 args) {
  if (level == LIBBPF_DEBUG && !state.verbose) {
    return 0;
  }
  return vfprintf(stderr, format, args);
}

/**
 *  Handle a signal (SIGINT, SIGTERM);
 *  @method sig_handler
 *  @param  {int} sig   The signal;
 *
 *  @return void
 *  @private
 */
static void
sig_handler(int sig) {
  state.exiting = true;
}

/**
 *  Output the given data to stdout
 *  @method voutput_stdout
 *  @param  format  The output format {char*};
 *  @param  argp    format-related arguments {va_list};
 *
 *  @return The number of bytes output {int};
 *  @private
 */
static int
voutput_stdout( const char* format, va_list argp ) {
  int     rc;

  // By default, output to stdout
  rc = vfprintf(stdout, format, argp);

  return rc;
}

/**
 *  Output the given data to a TCP socket
 *  @method voutput_socket
 *  @param  format  The output format {char*};
 *  @param  argp    format-related arguments {va_list};
 *
 *  @return The number of bytes output {int};
 *  @private
 */
static int
voutput_socket( const char* format, va_list argp ) {
  char  buf[2048] = {0};
  int   rc;

  rc = vsnprintf(buf, sizeof(buf), format, argp);

  if (rc > 0) {
    if (state.socket > 0) {
      if (write( state.socket, buf, rc ) <= 0) {
        fprintf(stderr, "*** socket write error[ %d : %s ]\n",
                errno, strerror( errno ));
        close( state.socket );
        state.socket = 0;
        rc = errno;
      }

    } else {
      fprintf(stdout, ">>> sock[ %s ]\n", buf);
    }
  }

  return rc;
}

/**
 *  Output the given data to the current output endpoint.
 *  @method output
 *  @param  format  The output format {char*};
 *  @param  ...     format-related arguments {va_list};
 *
 *  @return The number of bytes output {int};
 *  @private
 */
static int
output( const char* format, ... ) {
  int     rc;

  va_list argp;
  va_start(argp, format);

  if (state.socket > 0) {
    rc = voutput_socket(format, argp);

  } else {
    // By default, output to stdout
    rc = voutput_stdout(format, argp);

  }

  va_end(argp);

  return rc;

  /*
  va_list argp;
  va_start(argp, format);

  while (*format != '\0') {
    if (*format == '%') {
      format++;
      if (*format == '%') {
        putchar('%');
      } else if (*format == 'c') {
        char char_to_print = va_arg(argp, int);
        putchar(char_to_print);
      } else {
        fputs("Not implemented", stdout);
      }
    } else {
      putchar(*format);
    }
    format++;
  }
  va_end(argp);
  // */
}

/**
 *  Output the command line label value, escaping characters as needed.
 *  @method output_cmd_line_value
 *  @param  evt     The target event {event_t* evt};
 *
 *  @return void
 *  @private
 */
static void
output_cmd_line_value( const event_t* evt ) {
  if (evt->arg_len < 1) { return; }

  char* beg     = (char*)(evt->args);
  char* end     = (char*)&(evt->args[ evt->arg_len ]);
  char* cur     = beg;
  bool  stopped = false;

  while (cur < end) {
    if (*cur == 0) {
      if (cur > beg) {
        output(" ");

      } else {
        // Two null characters in a row -- end of string
        stopped= true;
        break;
      }

    } else {
      if (*cur == '"') {
        output("\\\"");
      } else if (*cur == '\n') {
        output("\\n");
      } else if (*cur == '\r') {
        output("\\r");
      } else if (*cur == '\\') {
        output("\\\\");
      } else {
        output("%c", *cur);
      }
    }

    cur++;
  }

  if (!stopped && evt->arg_len < evt->arg_full) {
    output("...");
  }
}

/**
 *  Output Prometheus labels for the given event
 *  @method output_prom_labels
 *  @param  evt     The target event {event_t* evt};
 *  @param  [unit]  The unit for the current metric {const char*};
 *
 *    metric_name  | labels                 | val  | timestamp
 *    -------------+------------------------+------+-----------
 *    %metric_name%[{%label%="%val%"[,...]}] %val%  [ %ts%]
 *
 *  @return void
 *  @private
 */
static void
output_prom_labels( const event_t* evt, const char* unit ) {
  bool  needComma = false;
  int   idex;

  output("{__metric_class=\"%s\",", state.metric_class);
  output( "node=\"%s\",",           g_hostname);
  output( "id=\"%u\",",             evt->pid);
  output( "name=\"%s\",",           evt->comm);
  output( "state=\"R (running)\",");
  output( "parent_id=\"%u\",",      evt->ppid);
  //output( "\"process_group\"=\"%u\",",  evt->pgrp);
  output( "user_id=\"%u\",",        evt->uid);
  output( "group_id=\"%u\",",       evt->gid);

  /* :NOTE: The cgroup subsystem number/hierarchy to name mapping is available
   *        via `/proc/cgroups`
   *
   *          #subsys_name  hierarchy   num_cgroups   enabled
   *          cpuset        3           1             1
   *          cpu           8           63            1
   *          cpuacct       8           63            1
   *          blkio         10          63            1
   *          memory        6           122           1
   *          devices       5           63            1
   *          freezer       12          1             1
   *          net_cls       4           1             1
   *          perf_event    11          1             1
   *          net_prio      4           1             1
   *          hugetlb       2           1             1
   *          pids          9           69            1
   *          rdma          7           1             1
   *
   *        In the following loop, the subsystem number/hierarchy is `idex`.
   *        The value at that index is the cgroup id within the subsystem.
   */
  output( "cgroups=\"");
  for (idex = 0; idex < MAX_CGROUPS; idex++) {
    __u32 id  = evt->cgroups[idex];
    if (id == 0)    { continue; }

    if (needComma)  { output(","); }
    else            { needComma = true; }

    output("%u", id);
  }
  output( "\"");

#if INCLUDE_IS_EXIT != 0 // {
  output( ",");
  output( "is_exit=\"%d\"", evt->is_exit);
#endif  // INCLUDE_IS_EXIT }

  if (evt->is_exit) {
    // evt->arg_len, evt->arg_full, evt->args
    output( ",");
    output( "cmdLine=\"");
    output_cmd_line_value( evt );
    output( "\",");

    output( "error_code=\"%d\",", evt->error_code);
    output( "trapNum=\"%d\",",    evt->trap_num);
    output( "exit_code=\"%d\"",   evt->exit_code);

    if (evt->signal > 0) {
      output( "," );
      output( "exception=\"%s\",", get_signal_str( evt->signal ));
      output( "sigNum=\"%d\"", evt->signal);

    }
  }

  if (unit) {
    output( ",");
    output( "\"unit\"=\"%s\"", unit);
  }

  output("}");
}

/**
 *  Output a single prometheus record for the named field and value.
 *  @method output_prom_record
 *  @param  evt       The target event {event_t* evt};
 *  @param  field     The name of the target field {const char*};
 *  @param  value     A string version of the field value {const char*};
 *
 *    metric_name  | labels                 | val  | timestamp
 *    -------------+------------------------+------+-----------
 *    %metric_name%[{%label%="%val%"[,...]}] %val%  [ %ts%]
 *
 *  @return void
 *  @private
 */
static void
output_prom_record( const event_t*  evt,
                    const char*     field,
                    const char*     value ) {

  output("os_proc_process_%s", field);

  output_prom_labels( evt, get_field_unit( field ) );

  /* :XXX: Do NOT use evt->timestamp (ns) since it is the relative time since
   *       boot and not an absolute time.
   */
  output(" %s %lu\n", value, get_timestamp());
}

/**
 *  Output JSON labels for the given event.
 *  @method output_json_labels
 *  @param  evt     The target event {event_t* evt};
 *  @param  [unit]  The unit for the current metric {const char*};
 *
 *  Metrics Bridge
 *  (custom-collectors/prometheus-bridge/handlers/async_source/kernelbeat.js)
 *  generates the following JSON for each process-related exec/exit event:
 *    labels  : {
 *      id            : pid,
 *      name          : comm,
 *      state         : process state (single character, e.g. 'R' running),
 *                      [ not really needed since all processes available to
 *                        the eBPF collector will be in the 'R' running state ]
 *      parent_id     : ppid,
 *      process_group : pgrp,
 *      user_id       : uid,
 *      cgroups       : cgroups.join(','),
 *      unit          : 'seconds',
 *
 *      // For exit
 *      cmdLine       : cmdLine,          # space-separated arguments possibly
 *                                        # ending with '...' if too long
 *                                        # NOT available for exec
 *      [error        : errorCode, ]
 *      [exit         : exitCode, ]
 *      [exception    : exception string, ]
 *      [trapNum      : trapNum, ]
 *      [sigNum       : sigNum, ]
 *    }
 *
 *  @return void
 *  @private
 */
static void
output_json_labels( const event_t* evt, const char* unit ) {
  bool  needComma = false;
  int   idex;

  output("{\"id\":%u,",         evt->pid);
  output( "\"name\":\"%s\",",   evt->comm);
  output( "\"state\":\"R (running)\",");
  output( "\"parent_id\":%u,",  evt->ppid);
  //output( "\"process_group\":%u,",  evt->pgrp);
  output( "\"user_id\":%u,",    evt->uid);
  output( "\"group_id\":%u,",   evt->gid);

  output( "\"cgroups\":[");
  for (idex = 0; idex < MAX_CGROUPS; idex++) {
    __u32 id  = evt->cgroups[idex];
    if (id == 0)    { continue; }

    if (needComma)  { output(","); }
    else            { needComma = true; }

    printf("%u", id);
  }
  output( "]");

#if INCLUDE_IS_EXIT != 0 // {
  output( ",");
  output( "\"is_exit\":%d", evt->is_exit);
#endif  // INCLUDE_IS_EXIT }

  if (evt->is_exit) {
    // evt->arg_len, evt->arg_full, evt->args
    output( ",");
    output( "\"cmdLine\":\"");
    output_cmd_line_value( evt );
    output( "\",");

    // error      %d
    // trapNum    %u
    output( "\"error_code\":%d,", evt->error_code);
    output( "\"trapNum\":%d,",    evt->trap_num);
    output( "\"exit_code\":%d",   evt->exit_code);

    if (evt->signal > 0) {
      output( "," );
      output( "\"exception\":\"%s\",", get_signal_str( evt->signal ));
      output( "\"sigNum\":%d", evt->signal);

    }
  }

  if (unit) {
    output( ",");
    output( "\"unit\":\"%s\"", unit);
  }

  output("}");
}

/**
 *  Output a single kafka-based JSON record for the named field and value.
 *  @method output_kafka_record
 *  @param  evt       The target event {event_t* evt};
 *  @param  field     The name of the target field {const char*};
 *  @param  value     A string version of the field value {const char*};
 *
 *  Kafka metrics format (pushed to the 'metric_class' [process] topic):
 *    {
 *      type      : 'uni-variate',
 *      name      : os_proc_process_( camelCase2_str(key) ),
 *      node      : '%collection-host%',
 *      unit      : '%unit%',                   // instead of in labels
 *      value     : '1234',
 *      timestamp : 1653558203281,              // milliseconds
 *
 *      labels    : {
 *        id            : 26267,
 *        parent_id     : 26260
 *        process_group : 6680,
 *        state         : 'R (running)',
 *
 *        name          : 'longhorn',           // process name
 *        cmdLine       : '...',                // actually just args
 *        cgroups       :  'csv,list,...',
 *        exit          : 0,
 *        user_id       : 0
 *      }
 *    }
 *
 *  @return void
 *  @private
 */
static void
output_kafka_record(  const event_t*  evt,
                      const char*     field,
                      const char*     value ) {

  output("{");
  output("\"type\":\"uni-variate\",");
  output("\"name\":\"os_proc_process_%s\",", field);
  output("\"node\":\"%s\",", g_hostname);
  output("\"unit\":\"%s\",", get_field_unit( field ));

  /* :XXX: Do NOT use evt->timestamp (ns) since it is the relative time since
   *       boot and not an absolute time.
   */
  output("\"timestamp\":%lu,", get_timestamp());

  output("\"labels\":");
  output_json_labels( evt, NULL );  // No 'unit' label
  output(",");

  output("\"value\":%s", value);
  output("}\n");
}

/**
 *  Output a single JSON record for the named field and value.
 *  @method output_json_record
 *  @param  evt       The target event {event_t* evt};
 *  @param  field     The name of the target field {const char*};
 *  @param  value     A string version of the field value {const char*};
 *
 *  Metrics Bridge
 *  (custom-collectors/prometheus-bridge/handlers/async_source/kernelbeat.js)
 *  generates the following JSON for each process-related exec/exit event:
 *    Create samples for each key that is NOT:
 *      pid, name, state, ppid, pgrp, uid, cgroups, cmdLine, error, exit,
 *      exception, trapNum, sigNum
 *
 *    labels.unit = _procUnit( key );
 *
 *    sample  = {
 *      name        : os_proc_process_( camelCase2_str(key) ),
 *      metric_class: 'process',
 *      timestamp   : (needs to come from the eBPF collector),
 *      labels      : output_json_labels( evt ),
 *      value       : String( value ),
 *    }
 *
 *  @return void
 *  @private
 */
static void
output_json_record( const event_t*  evt,
                    const char*     field,
                    const char*     value ) {

  output("{");
  output("\"name\":\"os_proc_process_%s\",",  field);
  output("\"metric_class\":\"%s\",",          state.metric_class);

  /* :XXX: Do NOT use evt->timestamp (ns) since it is the relative time since
   *       boot and not an absolute time.
   */
  output("\"timestamp\":%lu,", get_timestamp());

  output("\"labels\":");
  output_json_labels( evt, get_field_unit( field ) );
  output(",");

  output("\"value\":%s", value);
  output("}\n");
}

/**
 *  Output a single record for the named field and value.
 *  @method output_record
 *  @param  evt       The target event {event_t* evt};
 *  @param  field     The name of the target field {const char*};
 *  @param  value     A string version of the field value {const char*};
 *
 *  @return void
 *  @private
 */
static void
output_record( const event_t*  evt,
               const char*     field,
               const char*     value ) {
  if (state.kafka) {
    output_kafka_record( evt, field, value );

  } else if (state.prom) {
    output_prom_record( evt, field, value );

  } else {
    output_json_record( evt, field, value );
  }
}

/**
 *  Output records for each metric within the given event.
 *  @method output_records
 *  @param  evt   The target event {event_t* evt};
 *
 *  Metrics Bridge
 *  (custom-collectors/prometheus-bridge/handlers/async_source/kernelbeat.js)
 *  generates the following JSON for each process-related exec/exit event:
 *    Create samples for each key that is NOT:
 *      pid, name, state, ppid, pgrp, uid, cgroups, cmdLine, error, exit,
 *      exception, trapNum, sigNum
 *
 *    Object.entries( report ).forEach( ([key,value]) => {
 *      if (key excluded) { return }
 *
 *      labels.unit = _procUnit( key );
 *
 *      sample  = {
 *        name        : os_proc_process_( camelCase2_str(key) ),
 *        metric_class: 'process',
 *        timestamp   : (needs to come from the eBPF collector),
 *        labels      : output_json_labels( evt ),
 *        value       : String( value ),
 *      }
 *
 *      send sample ...
 *    });
 *
 *  @return void
 *  @private
 */
static void
output_records( const event_t* evt ) {
  char  value[64] = {0};

  /* kernel-beat records:
   *  cpuStart => cpu_start
   *  cpuUser  ...
   *  cpuSys
   *  fdOpen
   *  fdLimitHard
   *  fdLimitSoft
   *  memRss
   *  memShare
   *  memSize
   *  memFaultsMinor
   *  memFaultsMajor
   */

  // nanoseconds to seconds
  snprintf(value, sizeof(value), "%f", (evt->cpu_start / 1E9) );
  output_record( evt, "cpu_start", value );

  if (evt->is_exit) {
    // evt->cpu_end
    // evt->duration_ns
    snprintf(value, sizeof(value), "%f", (evt->cpu_user / 1E9) );
    output_record( evt, "cpu_user",  value );

    snprintf(value, sizeof(value), "%f", (evt->cpu_sys / 1E9) );
    output_record( evt, "cpu_sys",   value );

    // fd_open        (not available)
    // fd_limit_hard  (not available)
    // fd_limit_soft  (not available)

    snprintf(value, sizeof(value), "%llu", evt->hiwater_rss);
    output_record( evt, "mem_rss",  value );

    // mem_share (not available)
    snprintf(value, sizeof(value), "%llu", evt->task_size);
    output_record( evt, "mem_size",  value );

    snprintf(value, sizeof(value), "%u", evt->min_flt);
    output_record( evt, "mem_faults_minor",  value );

    snprintf(value, sizeof(value), "%u", evt->maj_flt);
    output_record( evt, "mem_faults_major",  value );
  }

  fflush(stdout);
}

/**
 *  Handle an exec-related ringbuffer insert.
 *  @method handle_event_exec
 *
 *  @return 0
 *  @private
 */
static int
handle_event_exec(void *ctx, void *data, size_t data_sz) {
  const event_t* evt = data;

  // assert( data_sz >= sizeof(*e) );
  // assert( ! evt->is_exit );

  output_records( evt );

  return 0;
}

/**
 *  Handle a exit-related ringbuffer insert.
 *  @method handle_event_exit
 *
 *  @return 0
 *  @private
 */
static int
handle_event_exit(void *ctx, void *data, size_t data_sz) {
  const event_t* evt = data;

  // assert( data_sz >= sizeof(*e) );
  // assert( evt->is_exit );

  output_records( evt );

  return 0;
}

/**
 *  Handle a ringbuffer insert.
 *  @method handle_event
 *
 *  @return 0
 *  @private
 */
static int
handle_event(void *ctx, void *data, size_t data_sz) {
  const event_t* evt = data;

  // assert( data_sz >= sizeof(*e) );

  if (evt->is_exit) {
    handle_event_exit( ctx, data, data_sz );

  } else {
    handle_event_exec( ctx, data, data_sz );
  }

  return 0;
}

/**
 *  Retrieve the hostname.
 *  @method get_hostname
 *
 *  If this is running within a k8s pod that has injected host-based
 *  information, we can pull the host name of the containing node instead of
 *  the name of this pod.
 *
 *  :NOTE: This requires the deployment/daemonset to use the k8s DownwardAPI to
 *         inject this information, for example:
 *          env:
 *            - name: K8S_NODE_NAME
 *              valueFrom:
 *                fieldRef:
 *                  fieldPath: spec.nodeName
 *
 *  If this information is not avaialble, simply return the OS-reported
 *  hostname.
 *
 *  @return The hostname {char*};
 *  @private
 *
 *    return process.env['K8S_NODE_NAME'] || Os.hostname();
 */
char*
get_hostname() {
  char* hostname  = getenv( "K8S_NODE_NAME" );

  if (hostname) {
    strncpy( g_hostname, hostname, sizeof(g_hostname) );

  } else {
    gethostname( g_hostname, sizeof(g_hostname) );

  }

  return hostname;
}

/**
 *  Attempt to open a TCP socket with the given host and port.
 *  @method open_socket
 *  @param  host    The target host {const char*};
 *  @param  port    The target port {int};
 *
 *  @return The socket file descriptor (< 0 failed, > 0 success) {int};
 *  @private
 */
static int
open_socket( const char* host, int port ) {
  struct hostent*     he;
  struct in_addr**    addr_list;
  struct sockaddr_in  servaddr = {0};
  int                 sock_fd;

  // Lookup the address of the target host
  he = gethostbyname( host );
  if (he == NULL) { // do some error checking
    fprintf(stderr, "*** lookup of [ %s ] failed [ %d : %s ]\n",
            host, h_errno, hstrerror( h_errno ));
    exit(1);
  }

  fprintf(stdout, ">>> open_socket: host[ %s ] => %s [ %s ]\n",
          host, he->h_name, inet_ntoa(*(struct in_addr*)he->h_addr));

  // socket create and verification
  sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd == -1) {
    fprintf(stderr, "*** socket creation failed [ %d : %s ]\n",
            errno, strerror( errno ));
    return sock_fd;
  }

  // assign FAMILY, PORT, and IP
  servaddr.sin_family = AF_INET;
  servaddr.sin_port   = htons( port );
  bcopy((char *)he->h_addr, (char *)&servaddr.sin_addr.s_addr, he->h_length);

  // connect the client socket to server socket
  if (connect(sock_fd, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) {
    fprintf(stderr, "*** connection with server [ %s:%d ] failed[ %d : %s ]\n",
            host, port, errno, strerror( errno ));
    close( sock_fd );

    sock_fd = (errno > 0 ? -errno : errno);

  } else {
    fprintf(stdout, ">>> open_socket: connected to the server [ %s:%d ] ...\n",
            host, port);
  }

  return sock_fd;
}

// argp parsing setup
static const struct argp argp = {
  .options  = opts,
  .parser   = parse_arg,
  .doc      = argp_program_doc,
};

/**
 *  The main entry point
 *  @method main
 *  @param  argc    The number of arguments {int};
 *  @param  argv    The arguments {char**};
 *
 *  @return An error indicator (0 == success) {int};
 */
int
main(int argc, char** argv) {

  struct ring_buffer*   rb    = NULL;
  struct proc_mon_bpf*  skel;
  int                   err;

  /* Parse command line arguments */
  err = argp_parse(&argp, argc, argv, 0, NULL, NULL);
  if (err) {
    return err;
  }

  get_hostname(); // => g_hostname[]

  libbpf_set_strict_mode(LIBBPF_STRICT_ALL);

  /* Set up libbpf errors and debug info callback */
  libbpf_set_print(libbpf_print_fn);

  /* Cleaner handling of Ctrl-C */
  signal(SIGINT, sig_handler);
  signal(SIGTERM, sig_handler);

  if (state.tcp_host && state.tcp_port) {
    state.socket = open_socket( state.tcp_host, state.tcp_port );

    if (state.socket < 0) {
      return state.socket;
    }
  }

  /* Open the BPF application */
  skel = proc_mon_bpf__open();
  if (!skel) {
    fprintf(stderr, "*** Failed to open BPF skeletom [ %d : %s ]\n",
            errno, strerror( errno ));
    return 1;
  }

  /* Parameterize BPF code with minimum duration parameter */
  skel->rodata->min_duration_ns = state.min_duration_ms * 1000000ULL;

  /* Load & verify BPF programs */
  err = proc_mon_bpf__load(skel);
  if (err) {
    fprintf(stderr, "*** Failed to load and verify BPF skeletom [ %d : %s ]\n",
            errno, strerror( errno ));
    goto cleanup;
  }

  /* Attach tracepoints */
  err = proc_mon_bpf__attach(skel);
  if (err) {
    fprintf(stderr, "*** Failed to attach BPF skeleton [ %d : %s ]\n",
            errno, strerror( errno ));
    goto cleanup;
  }

  /* Set up ring buffer polling */
  rb = ring_buffer__new(bpf_map__fd(skel->maps.map_rb), handle_event, NULL, NULL);
  if (!rb) {
    err = -1;
    fprintf(stderr, "*** Failed to create ring buffer [ %d : %s ]\n",
            errno, strerror( errno ));
    goto cleanup;
  }

  /* Process events */
  int reconnect_beat  = 50;
  int skip_count      = 0;
  while (!state.exiting) {
    if (state.tcp_host && state.tcp_port && state.socket <= 0) {
      if (skip_count++ >= reconnect_beat) {
        skip_count = 0;
        open_socket( state.tcp_host, state.tcp_port );
      }
    }

    err = ring_buffer__poll(rb, 100 /* timeout, ms */);

    /* Ctrl-C will cause -EINTR */
    if (err == -EINTR) {
      err = 0;
      break;
    }

    if (err < 0) {
      printf("Error polling perf buffer: %d\n", err);
      break;
    }
  }

cleanup:
  /* Clean up */
  ring_buffer__free(rb);
  proc_mon_bpf__destroy(skel);

  if (state.socket > 0) {
    close( state.socket );
    state.socket = 0;
  }

  return err < 0 ? -err : 0;
}
