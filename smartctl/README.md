This is a simple wrapper around `smartctl`, requiring at least version 7.0 for
its JSON output.

### Usage
``` python3
import smartctl

# Retrieve the available list of devices
dl = smartctl.DeviceList()

# Iterate over all available devices
for dev in devices: #{
  # Each device of interest should be updated to allow gathering information.
  # This is a separate step since each update may take a number of seconds.
  dev.update()
  
  # dev.path            the device path
  # dev.interface       the interface type for this device
  #                     (e.g. ata, nvme, sat, scsi)
  # dev.iface_type      a more version of `interface` which maps specific
  #                     interfaces to more generic types, for example:
  #                       csmi  => ata
  #                       sas   => scsi
  #                       sata  => ata
  #
  # dev.vendor          the name of the device vendor
  # dev.model           the name of the device model
  # dev.serial          the serial number of the device
  # dev.firmware        the id of the device firmware revision
  #
  # dev.power_on_hours  the number of lifetime hours the device has been
  #                     powered on
  # dev.temperature     the current temperature in Celsius
  #
  # dev.is_ssd          is the device an SSD?
  # dev.rotation_rate   the rotation rate (if NOT an SSD)
  # dev.capacity        the device capacity in bytes
  #
  # dev.smart_capable   is the device SMART capable?
  # dev.smart_enabled   is SMART enabled for this device?
  # dev.smart_status    a simple 'PASS' / 'FAIL' SMART status
  #
  # dev.attributes      a dict containing SMART-related attributes
  #
  # dev.info            the full information returned by `smartctl`, populated
  #                     via `update()` or on first reference
  #
#}
```
