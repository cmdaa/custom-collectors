import os
import json

from .attribute   import Attribute
from .device      import Device
from .device_list import DeviceList
from shutil       import which
from subprocess   import Popen, PIPE

#SMARTCTL_PATH = which('smartctl')
SMARTCTL_PATH = '/ABJ/home/depeele/bin/smartctl'

type_map = {
  'ata'     : 'ata',
  'csmi'    : 'ata',
  'nvme'    : 'nvme',
  'sas'     : 'scsi',
  'sat'     : 'sat',
  'sata'    : 'ata',
  'scsi'    : 'scsi',
  'atacam'  : 'atacam',
  'megaraid': 'scsi',
}

def _smartctl( args ): #{
  """
    Invoke `smartctl` using the provided arguments and returning all data read
    from smartctl's stdout.

    Args:
      args (list)   The list of command line arguments, beginning with
                    the absolute path to the `smartctl` executable;

    Returns:
      (str)         The data read from the child's stdout;
  """
  if args[0] != SMARTCTL_PATH: #{
    args.insert( 0, SMARTCTL_PATH )
  #}

  if True: #{
    #########################################################################
    # Use Popen()
    cmd    = Popen( args, stdout=PIPE )
    output = cmd.communicate()[0]
    return output
  #}

  ###########################################################################
  # Directly use fork/exec
  #
  import io

  c2pread, c2pwrite = os.pipe()
  buf_size          = 100000
  from_child        = io.open( c2pread, 'rb', buf_size )
  child_pid         = os.fork()
  if child_pid > 0: #{
    ####################
    # Parent process
    #
    # Close child pipe end
    os.close( c2pwrite )

    # Wait for child to complete
    cpid, child_status = os.waitpid( child_pid, 0 ) #, os.WNOHANG )

    # Read output from the child process
    output = from_child.read()

    # Close our end of the pipe
    from_child.close()

    return output

  else: #}{
    ####################
    # Child process
    if c2pread  != -1: os.close( c2pread )    # close parent's pipe end
    if c2pwrite != -1: os.dup2( c2pwrite, 1 ) # make our pipe end stdout

    os.execv( args[0], args )
  #}
#}

def scan( ): #{
  """
    Perform a smartctl scan.

    Returns:
      (dict)    The decoded JSON of the results
                  { devices: [ {name:, type:} ] }
  """
  arg_list = [ SMARTCTL_PATH, '--json', '--scan' ]

  output = _smartctl( arg_list )
  data   = json.loads( output )

  return data
#}

def dev_info( dev_path, dev_type = None ): #{
  """
    Given a device path, execute smartctl to retrieve all available information
    about that drive.

    Args:
      dev_path (str)          The path to the target device;
      [dev_type = None] (str) The explicit device type (e.g. scsi, ata, nvme);

    Returns:
      (dict)    The decoded JSON
  """
  arg_list = [ SMARTCTL_PATH, '--json', '--info', '--attributes', dev_path ]
  if dev_type is not None:  arg_list.extend( ['--device', dev_type] )

  output = _smartctl( arg_list )
  data   = json.loads( output )

  return data
#}

__version__ = '0.1'
#__all__     = ['scan', 'dev_info', 'type_map', 'SMARTCTL_PATH']
