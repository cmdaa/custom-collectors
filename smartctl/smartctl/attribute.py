class Attribute( object ): #{
  """
    A single SMART atribute.
  """

  FLAG_PREFAILURE   = 0x0001
  FLAG_ONLINE       = 0x0002
  FLAG_PERFORMANCE  = 0x0004
  FLAG_ERROR_RATE   = 0x0008
  FLAG_EVENT_COUNT  = 0x0010
  FLAG_AUTO_KEEP    = 0x0020
  FLAG_RESERVED     = 0xffc0

  def __init__( self, name, value,
                        id          = None,
                        flags       = None,
                        worst       = None,
                        thresh      = None,
                        type        = None,
                        updated     = None,
                        when_failed = None,
                        raw         = None ): #{
    if raw is None: #{
      raw = {
        'value' : value,
        'string': str(value),
      }
    #}

    if isinstance(flags, dict): #{
      # flags: {
      #   value: (int),
      #   string: (str),
      #   prefailure: (bool),
      #   updated_online: (bool),
      #   performance: (bool),
      #   error_rate: (bool),
      #   event_count: (bool),
      #   auto_keep: (bool),
      # }
      if type is None: #{
        type    = ('pre-fail' if flags.get('prefailure', False)
                              else 'old-age')
      #}
      if updated is None: #{
        updated = ('always'   if flags.get('updated_online', False)
                              else 'offline')
      #}

      self._flags = flags
      flags       = flags.get('value', None)
    else: #}{
      flags = 0
    #}

    self.id          = id
    self.name        = name
    self.flags       = flags
    self.value       = value
    self.worst       = worst
    self.thresh      = thresh
    self.type        = type         # pre-fail, old-age
    self.updated     = updated      # Always, Offline
    self.when_failed = when_failed  # now, past, ''
    self.raw         = raw
  #}

  def __repr__( self ): #{
    """
      Return a basic representation of this instance

      Returns:
        (str)
    """
    return ('<SMART Attribute %r %s/%s raw:%s>' %
              (self.name,
               self.value,
               self.thresh,
               self.raw) )
  #}

  @property
  def header( self ): #{
    """
      Return a formatted header identify the columns that will be generate
      via str( self )

      Returns:
        (str)
    """
    return ('%4s: %-24s %-6s %-6s %-4s %-4s %-9s %-8s %-12s %-12s %s' %
              ('id',
               'name',
               'flags',
               'val',
               'wrst',
               'thld',
               'type',
               'updated',
               'when_failed',
               'raw.value', 'raw.string') )
  #}

  def __str__( self ): #{
    """
      Return a formatted string representation of this instance.

      Returns:
        (str)
    """
    raw_val = self.raw.get('value', -1)
    if isinstance(raw_val, int): #{
      raw_fmt = ('0x%010x' % (raw_val))

    else: #}{
      raw_fmt = ('%-12s' % (raw_val))
    #}

    return ('%4s: %-24s 0x%04x %-6s %-4s %-4s %-9s %-8s %-12s %s %s' %
              (self.id,
               self.name,
               self.flags,
               self.value,
               self.worst,
               self.thresh,
               self.type,
               self.updated,
               self.when_failed,
               raw_fmt,
               self.raw.get('string', '')) )
  #}

  def __getstate__( self ): #{
    """
      Return a simple dict representation of this instance.

      Returns:
        (dict)
    """
    return {
      'id'          : self.id,
      'name'        : self.name,
      'flags'       : self.flags,
      'value'       : self.value,
      'worst'       : self.worst,
      'thresh'      : self.thresh,
      'type'        : self.type,
      'updated'     : self.updated,
      'when_failed' : self.when_failed,
      'raw'         : self.raw,
    }
  #}
#}

__all__ = [ 'Attribute' ]
