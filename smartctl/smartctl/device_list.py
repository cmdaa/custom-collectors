import smartctl

from .device  import Device

class DeviceList(object): #{
  """
    Represents a list of all storage devices connected to this system.
  """

  def __init__( self, init=True ): #{
    """
      Initialize a new instance.

      Args:
        init (bool)   If False, skip initialization;
    """
    self.devices = []

    if init: #{
      self._initialize()
    #}
  #}

  def __repr__( self ): #{
    """
      Return a basic representation of this instance

      Returns:
        (str)
    """
    res = '<DeviceList contents:\n'
    for dev in self.devices: #{
      res += str(dev) +'\n'
    #}
    return res +'>'
  #}

  def _initialize( self ): #{
    """
      Perform a system scan for attached devices.
    """
    data = smartctl.scan()
    for dev in data['devices']: #{
      name  = dev['name']
      iface = dev['type']

      self.devices.append( Device( name, interface=iface ) )
    #}
  #}

  def __iter__( self ): #{
    """
      Return an iterator for this instance
    """
    return DeviceListIterator( self )
  #}
#}

class DeviceListIterator: #{
  """
    An iterator for DeviceList
  """
  def __init__( self, dev_list ): #{
    self._list  = dev_list
    self._index = 0
  #}

  def __next__( self ): #{
    if self._index < len(self._list.devices): #{
      dev = self._list.devices[ self._index ]
      self._index += 1
      return dev
    #}

    raise StopIteration
  #}
#}

__all__ = [ 'DeviceList' ]
