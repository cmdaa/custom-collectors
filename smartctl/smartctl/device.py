import re

import smartctl

Attribute = smartctl.Attribute
#from .attibute  import Attribute

class Device(object): #{
  """
    Represents a device attached to an internal storage interface.
  """

  def __init__( self, path, interface=None, info=None ): #{
    """
      Create a new instance

      Args:
        path (str)                The absolute path to the target device;
        [interface = None] (str)  The explicit interface to use;
        [info = None] (dict)      If provided, initial information about this
                                  device;
    """
    iface_type = None
    if interface is not None: #{
      parts = interface.split(',')

      iface_type = smartctl.type_map.get( parts[0].lower() )
      if iface_type is None: #{
        raise ValueError('Unknown interface[ %s ] for %s' % (interface, path))
      #}
    #}

    self.path           = path
    self.interface      = interface
    self.iface_type     = iface_type

    self.info           = info
  #}

  @property
  def info( self ): #{
    if self._info is None: #{
      self.update()
    #}

    return self._info
  #}

  @info.setter
  def info( self, val ): #{
    self._info = val

    self.vendor         = None
    self.model          = None
    self.serial         = None
    self.firmware       = None
    self.power_on_hours = None
    self.temperature    = None

    self.capacity       = None
    self.is_ssd         = None
    self.rotation_rate  = None

    self.smart_capable  = None
    self.smart_enabled  = None
    self.smart_status   = None

    self.attributes     = {}

    if isinstance(val, dict): #{
      self.gather_vendor_model()
      self.gather_serial()

      self.gather_firmware()
      self.gather_power_on_hours()
      self.gather_temperature()

      self.gather_capacity()
      self.gather_rotation_info()

      self.gather_smart_info()
      self.gather_attributes()
    #}
  #}

  def __repr__( self ): #{
    """
      Return a basic representation of this instance

      Returns:
        (str)
    """
    return ('<Device %s on %s vendor:%s, model:%s, serial:%s>' %
            (self.interface.upper() if self.interface else 'UNKNOWN',
             self.path,
             self.vendor,
             self.model,
             self.serial) )
  #}

  def update( self ): #{
    """
      Update avaialble information for this drive.
    """
    self.info = smartctl.dev_info( self.path, self.interface )
  #}

  def gather_vendor_model( self ): #{
    """
      Gather vendor and model information from within the collected smartctl
      data for this device.
    """
    self.vendor = self.model = None

    # [VENDOR ]Model
    fields = self.info.get( 'model_name', '' ).split()
    if len(fields) > 1: self.vendor = fields.pop( 0 )
    if len(fields) > 0: self.model  = fields.pop()  #' '.join( fields )

    if self.iface_type == 'scsi': #{
      # Check if the reported SCSI Vendor different that what we extracted from
      # the 'model_name'
      vendor = self.info.get( 'vendor', self.vendor )
      if vendor != self.vendor: self.vendor = vendor
    #}
  #}

  def gather_serial( self ): #{
    """
      Gather serial information from within the collected smartctl data for
      this device.
    """
    self.serial = None

    self.serial = self.info.get( 'serial_number', None )
  #}

  def gather_firmware( self ): #{
    """
      Gather firmware information from within the collected smartctl data for
      this device.
    """
    self.firmware = None

    if self.iface_type == 'scsi': #{
      self.firmware = self.info.get( 'revision', None )
    #}

    if self.firmware is None: #{
      self.firmware = self.info.get( 'firmware_version', None )
    #}
  #}

  def gather_power_on_hours( self ): #{
    """
      Gather power_on_hours information from within the collected smartctl data
      for this device.
    """
    self.power_on_hours = None

    power = self.info.get('power_on_time', {})
    hours = power.get('hours', None)
    if hours is not None: #{
      mins = power.get('minutes', 0)

      self.power_on_hours = hours + (mins / 60)
    #}
  #}

  def gather_temperature( self ): #{
    """
      Gather temperature information from within the collected smartctl data for
      this device.
    """
    self.temperature = None

    temp = self.info.get( 'temperature', None )
    if temp: #{
      self.temperature = temp.get( 'current', None )
    #}
  #}

  def gather_capacity( self ): #{
    """
      Gather capacity information from within the collected smartctl data for
      this device.
    """
    self.capacity = None

    cap = self.info.get( 'user_capacity', None )
    if cap: #{
      self.capacity = cap.get( 'bytes', None )
    #}
  #}

  def gather_rotation_info( self ): #{
    """
      Gather rotation_rate and SSD information from within the collected
      smartctl data for this device.
    """
    self.rotation_rate = None
    self.is_ssd        = False

    rotation = self.info.get( 'rotation_rate', None )
    if not rotation: #{
      self.is_ssd = True

    else: #}{
      self.rotation_rate = rotation
    #}
  #}

  def gather_smart_info( self ): #{
    """
      Gather SMART information from within the collected smartctl data for this
      device.
    """
    self.smart_capable = None
    self.smart_enabled = None
    self.smart_status  = None

    status = self.info.get( 'smart_status', None )
    if status: #{
      self.smart_capable = True

      passed = status.get('passed', None)

      if passed is None: #{
        self.smart_enabled = False
        self.smart_status  = 'UNKNOWN'

      elif not passed: #}{
        self.smart_enabled = True
        self.smart_status  = 'FAIL'
      else: #}{
        self.smart_enabled = True
        self.smart_status  = 'PASS'
      #}

    else: #}{
      smart = self.info.get( 'ata_smart_attributes', None )
      if smart: #{
        # There are ATA SMART attributes so SMART is avaialble
        # (and presumabely enabled)
        self.smart_capable = True
        self.smart_enabled = True
        self.smart_status  = 'PASS' # until we see a failure

        # Check the SMART attribute table for threshold failures
        for attr in smart.get('table', []): #{
          failed = attr.get('when_failed', False)
          if failed: #{
            self.smart_status = ('FAIL (%s)' % (failed))
            break
          #}

          thresh  = attr.get('thresh', None)
          if thresh is None: continue

          # Check against thresholds directly
          current = attr.get('value', None)
          if current is not None and current <= thresh: #{
            # Failed NOW
            self.smart_status = 'FAIL (now)'
            break
          #}

          worst = attr.get('worst', None)
          if worst is not None and worst <= thresh: #{
            # Failed in the past
            self.smart_status = 'FAIL (past)'
            break
          #}
        #}
      #}
    #}
  #}

  def gather_attributes( self ): #{
    """
      Gather SMART-related attributes.
    """
    attrs = []

    if self.power_on_hours is not None: #{
      attrs.append( Attribute( 'power_on_hours', self.power_on_hours ))
    #}

    val = self.info.get('power_cycle_count', None)
    if val is not None: #{
      attrs.append( Attribute( 'power_cycles', val ) )
    #}

    val = self.info.get('temperature', None)
    if isinstance(val, dict): #{
      # SCSI temperature with 'drive_trip' threshold
      attrs.append( Attribute( 'temperature', self.temperature,
                                  thresh = val.get('drive_trip', None) ) )
    else: #}{
      # Simple temperature
      attrs.append( Attribute( 'temperature', self.temperature ) )
    #}

    val = self.info.get('scsi_grown_defects_list', None)
    if val is not None: #{
      attrs.append( Attribute( 'grow_defects_list', val ) )
    #}

    log = self.info.get('nvme_smart_health_information_log', None)
    if isinstance(log, dict): #{
      for key,val in log.items(): #{
        name = key.lower()

        attrs.append( Attribute( name, val ) )
      #}
    #}

    log = self.info.get('ata_smart_attributes', None)
    if isinstance(log, dict): #{
      for item in log.get('table', []): #{
        name = item.pop( 'name' ).lower()

        attrs.append( Attribute( name, **item ) )
      #}
    #}

    # Generate attributes map using the attribute id/name as keys.
    self.attributes = {}
    for attr in attrs: #{
      if attr.id is not None: self.attributes[ attr.id ]   = attr
      else:                   self.attributes[ attr.name ] = attr
    #}
  #}
#}
