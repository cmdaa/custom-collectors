import smartctl
import json

from time import monotonic as _time

def show_dev( dev, interpret=False ): #{
  print('@%s: show_dev( %s, %s / %s )' %
          (_time(), dev.path, dev.interface, dev.iface_type))

  dev.update()
  print('@%s: updated' % (_time()), end='' )

  if interpret: #{
    # Output the interpolated/normalized properties
    print('')
    print('vendor        : %s' % (dev.vendor))
    print('model         : %s' % (dev.model))
    print('serial        : %s' % (dev.serial))
    print('firmware      : %s' % (dev.firmware))

    print('power_on_hours: %s' % (dev.power_on_hours))
    print('temperature   : %s' % (dev.temperature))

    print('is_ssd        : %s' % (dev.is_ssd))
    print('rotation_rate : %s' % (dev.rotation_rate))
    print('capacity      : %s' % (dev.capacity))

    print('smart_capable : %s' % (dev.smart_capable))
    print('smart_enabled : %s' % (dev.smart_enabled))
    print('smart_status  : %s' % (dev.smart_status))

    print('attributes    :')
    need_header = True
    for key,attr in dev.attributes.items(): #{
      if not attr: continue
      if need_header: #{
        print('  %s' % (attr.header))
        need_header = False
      #}

      print('  %s' % (attr))
    #}

  else: #}{
    # Output the raw smartctl info (as JSON)
    print(': %s' % (json.dumps( dev.info, indent=2 )) )
  #}

  print('===========================================================')
#}

timing_test     = True
single_dev_test = True
interpret       = True

if timing_test: #{
  dev      = smartctl.Device('/dev/sdk')
  #dev      = smartctl.Device('/dev/sda', 'megaraid,0')
  iters    = 10
  time_sum = 0
  for idex in range( 0, iters ): #{
    start = _time()
    dev.update()
    end = _time()
    print('%3d: @%4.3f: updated' % (idex, end - start) )
    time_sum += (end - start)
  #}
  print('%d iterations in %4.3f seconds, %4.3f average' %
        (iters, time_sum, time_sum / iters))

elif single_dev_test: #}{
  dev = smartctl.Device('/dev/sdk')
  #dev  = smartctl.Device('/dev/sda', 'megaraid,0')
  show_dev( dev, interpret=interpret )

else: #}{
  devs = smartctl.DeviceList()
  for dev in devs: #{
    show_dev( dev, interpret=interpret )

    break
  #}
#}
