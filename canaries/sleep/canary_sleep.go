package main

import (
  "flag"
  "log"
  "time"
  "net/http"
  "fmt"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
  addr    = flag.String("listen-address", ":8080", "Metrics listening address")
  path    = flag.String("metrics-path",   "/metrics",
                                          "The path to the collection endpoint")
  sleep   = flag.Int(   "sleep",          60,      "Test sleep time (seconds)")

  canary  prometheus.Histogram
)

/**
 *  Initialize this module
 *  @method init
 */
func init() {
  flag.Parse()

  // Create the `canary` gauge with help that indicates the test sleep time
  canary  = prometheus.NewHistogram( prometheus.HistogramOpts{
    Name: "canary_sleep_variance_seconds",
    Help: fmt.Sprintf("Variance between expected and actual sleep times (%d s)",
                      *sleep),
    Buckets:  prometheus.ExponentialBuckets(0.0001, 10.0, 5),
  })

  // Register the canary metrics
  prometheus.MustRegister( canary )
}

/**
 *  Perform a canary tests
 *  @method test_canary
 *  @param  canary  The target prometheus metric {prometheus.Histogram};
 *  @param  sleep   The sleep time (seconds) {int};
 *
 *  Upon completion, this method will invoke `canary.Set()` to establish a new
 *  metric value.
 */
func test_canary( canary prometheus.Histogram, sleep int ) {
  start := time.Now()

  time.Sleep( time.Duration( sleep ) * time.Second )

  elapsed  := time.Since( start )
  variance := elapsed.Seconds() - float64(sleep)

  log.Printf("Canary %d second sleep variance: %f\n",
             sleep, variance)

  canary.Observe( variance )
}

/**
 *  The main entry point
 *  @method main
 */
func main() {
  // Periodically record latencies
  go func() { for { test_canary( canary, *sleep ) } }()

  // Expose the registered metrics via HTTP
  http.Handle( *path, promhttp.Handler() )

  log.Printf("Listening on http://%s%s\n", *addr, *path)

  log.Fatal( http.ListenAndServe( *addr, nil ) )
}
