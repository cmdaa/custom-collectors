#include <stdio.h>
#include <unistd.h>             // STDOUT_FILENO
#include <string.h>
#include <systemd/sd-journal.h> // sd_journal*()
#include <poll.h>               // ppoll()
#include <errno.h>              // errno
#include <time.h>               // timespec, localtime()

typedef uint64_t  usec_t;
typedef uint64_t  nsec_t;

#define USEC_PER_SEC  ((usec_t) 1000000ULL)
#define NSEC_PER_USEC ((nsec_t) 1000ULL)
#define USEC_INFINITY ((usec_t) -1)
#define TIME_T_MAX (time_t)((UINTMAX_C(1) << ((sizeof(time_t) << 3) - 1)) - 1)

#define ELEMENTS(a) (sizeof(a) / sizeof((a)[0]))

static char _timeStr[256] = {0};

/**
 *  Convert a time (us) to a string representation.
 *  @method _time_str
 *  @param  us      The time (us) {usec_t);
 *
 *  @note   Since this uses a static buffer, it should be called once per
 *          fprintf().
 *
 *  @return The formatted time string {char*};
 *  @private
 */
static char*
_time_str( usec_t time ) {
  time_t      secs  = (time / USEC_PER_SEC);
  struct tm*  tmp   = localtime( &secs );
  size_t      rc;
  memset((void*)_timeStr, 0, sizeof(_timeStr));

  rc = strftime( (char*)_timeStr, sizeof(_timeStr), "%F@%T", tmp );
  if (rc > 0) {
    snprintf( &_timeStr[rc], sizeof(_timeStr) - rc,
                ".%06llu", (time % USEC_PER_SEC));
  }

  return _timeStr;
}


/**
 *  Store a time value (us) into a timespec
 *  @method _timespec_store
 *  @param  ts      The target timespec {struct timespec*};
 *  @param  us      The time (us) {usec_t};
 *
 *  @return The updated `ts` {struct timespec*};
 *  @private
 */
static struct timespec*
_timespec_store( struct timespec* ts, usec_t us ) {
  if (us == USEC_INFINITY || us / USEC_PER_SEC >= TIME_T_MAX) {
    ts->tv_sec  = (time_t) -1;
    ts->tv_nsec = (long)   -1;

  } else {
    ts->tv_sec  = (time_t)(us / USEC_PER_SEC);
    ts->tv_nsec = (long int)((us % USEC_PER_SEC) * NSEC_PER_USEC);
  }

  return ts;
}

/**
 *  Wait for a change in the journal.
 *  @method _wait_for_change
 *  @param  journal   The target journal handle {sd_journal*};
 *  @param  poll_fd   The fd associated with `journal` {int};
 *
 *  @return The status (0 = success) {int};
 *  @private
 */
static int
_wait_for_change( sd_journal* journal, int poll_fd ) {
  struct pollfd   pollfds[] = {
    { .fd = poll_fd, .events = POLLIN },
    { .fd = STDOUT_FILENO },
  };
  struct timespec*  timeout_ts  = NULL;
  int               rc;
  struct timespec   ts;
  usec_t            timeout;

  rc = sd_journal_get_timeout( journal, &timeout );
  if (rc) {
    fprintf(stderr, "*** Failed to determine journal wait time: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  if (timeout != USEC_INFINITY) {
    fprintf(stdout, ">>> Timeout %llu us\n", timeout);

    timeout_ts = _timespec_store( &ts, timeout );
  }

  if (ppoll( pollfds, ELEMENTS(pollfds), timeout_ts, NULL ) < 0) {
    if (errno == EINTR) {
      return 0;
    }

    fprintf(stderr, "*** Couldn't wait for journal event: %s\n",
            strerror(errno));
    return errno;
  }

  if (pollfds[1].revents & (POLLHUP | POLLERR)) {
    // STDOUT closed?
    fprintf(stderr, "*** Stdout closed\n");
    return ECANCELED;
  }

  // Process the current event(s)
  rc = sd_journal_process( journal );
  if (rc < 0) {
    fprintf(stderr, "*** Failed to process journal events: %s\n",
            strerror(abs(rc)));
    return rc;
  }

  return 0;
}

/**
 *  Show the current journal entry
 *  @method _show_entry
 *  @param  journal   The target journal {sd_journal*};
 *
 *  @return The status (0 == success) {int};
 *  @private
 */
static int
_show_entry( sd_journal* journal ) {
  int         rc;
  const char* msg;
  size_t      msgLen;
  usec_t      realtime;
  /*
  usec_t      monotonic;
  sd_id128_t  bootId;
  // */

  rc = sd_journal_get_realtime_usec( journal, &realtime );
  if (rc < 0) {
    fprintf(stderr, "  *** Failed to retrieve realtime timestamp: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  /*
  rc = sd_journal_get_monotonic_usec( journal, &monotonic, &bootId );
  if (rc < 0) {
    fprintf(stderr, "  *** Failed to retrieve monotonic timestamp: %s\n",
            strerror( abs(rc) ));
    return rc;
  }
  // */


  fprintf(stdout, "%12llu : %s : {\n", realtime, _time_str( realtime ));
  do {
    rc = sd_journal_enumerate_data( journal, (void*)&msg, &msgLen );
    if (rc < 0) {
      fprintf(stderr, "  *** Failed to enumerate message data: %s\n",
              strerror( abs(rc) ));

    } else if (rc > 0) {
      fprintf(stdout, "  %.*s\n", (int)msgLen, msg);
    }

  } while (rc > 0);
  fprintf(stdout, "}\n");

  /*
  rc = sd_journal_get_data( journal, "MESSAGE", (const void**)&msg, &msgLen );
  if (rc) {
    fprintf(stderr, "*** Failed to read message field: %s\n",
            strerror( abs(rc) ));

  } else {
    fprintf(stdout, "%.*s\n", (int)msgLen, msg);
  }
  // */

  return rc;
}

/**
 *  Given a journal handle that has already been read to the end,
 *  wait for changes and output new entries.
 *  @method _watch_journal
 *  @param  journal   The target journal {sd_journal*};
 *
 *  @return The status (0 == success) {int};
 *  @private
 */
static int
_watch_journal( sd_journal* journal ) {
  int poll_fd = sd_journal_get_fd( journal );
  int rc;

  if (poll_fd < 0) {
    fprintf(stderr, "*** Failed to get journal fd: %s\n",
            strerror( abs(rc) ));
    return poll_fd;

  }

  do {
    rc = _wait_for_change( journal, poll_fd );
    if (rc) { break; }

    rc = sd_journal_next( journal );
    if (rc < 0) {
      fprintf(stderr, "*** Failed to retrieve next: %s\n",
              strerror( abs(rc) ));
      break;
    }

    _show_entry( journal );

  } while (rc >= 0);

  return rc;
}

/*****************************************************************************/
int
main( int argc, char** argv ) {
  int         rc;
  sd_journal* journal;

  rc = sd_journal_open( &journal, SD_JOURNAL_LOCAL_ONLY );
  if (rc) {
    fprintf(stderr, "*** Failed to open journal: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  // Show all existing entries
  SD_JOURNAL_FOREACH( journal ) {
    _show_entry( journal );
  }

  fprintf(stdout, "-------------------- live ----------------------\n");

  // Watch for new entries
  (void) _watch_journal( journal );

  sd_journal_close( journal );

  return 0;
}
