#!/usr/bin/env python3
import sys
import signal

from systemd import journal

def _print_entry( entry ): #{
  #print( entry )
  print('{')
  for key,val in entry.items(): #{
    print('  %s: %s' % (key, val))
  #}
  print('}')
#}

def _watch( jr ): #{
  print('------------------------------ live ----------------------------')

  while True: #{
    # :XXX: wait() defers any signals until timeout
    rc = jr.wait( 1 )
    if rc: #{
      print('.')
    else: #}{
      sys.stdout.write('.')
      sys.stdout.flush()
    #}

    while True: #{
      entry = jr.get_next()
      if not entry: break

      _print_entry( entry )
      #print( entry['MESSAGE'] )
    #}
  #}
#}

def _exit( signum, frame ): #{
  print('=== exit [ %d ]' % (signum))
  sys.exit(0)
#}

#######################################################################
signal.signal( signal.SIGINT, _exit )

jr = journal.Reader();
jr.this_boot()
#jr.log_level( journal.LOG_INFO )
#jr.add_match( _SYSTEMD_UNIT='dockerd.service' )

#for entry in jr: #{
#  _print_entry( entry )
##}

_watch( jr )

jr.close()
