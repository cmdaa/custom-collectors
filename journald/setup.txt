#!/bin/bash
#
# Install headers/libraries for using journald and python3
sudo yum install -y systemd-devel python3-devel

# Install the systemd python module
sudo pip3 install systemd-python
