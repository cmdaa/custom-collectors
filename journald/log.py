#!/usr/bin/env python3
import sys
import signal
import time
import logging

from systemd import journal

def _generate_messages( interval, max = 0 ): #{
  log = logging.getLogger('generator')
  log.addHandler( journal.JournalHandler() )
  log.setLevel( logging.INFO )

  count = 0
  while True: #{
    count += 1

    print('%d' % (count), end=' ', flush=True)

    log.info( ('pseudo log message #%d' % (count) ) )

    if (max > 0 and count >= max): break

    time.sleep( interval )
  #}
#}

def _exit( signum, frame ): #{
  print('=== exit [ %d ]' % (signum))
  sys.exit(0)
#}

#######################################################################
signal.signal( signal.SIGINT, _exit )

_generate_messages( 5 )
