#include <unistd.h>             // sleep()
#include <stdio.h>
#include <string.h>
#include <systemd/sd-journal.h> // sd_journal*()
#include <errno.h>              // errno
#include <time.h>               // localtime(), strftime()

typedef unsigned char bool;
#define false (bool)0
#define true  (bool)1

typedef uint64_t  usec_t;

#define USEC_PER_SEC  ((usec_t) 1000000ULL)

static char _timeStr[256] = {0};

#define PAUSE_SECONDS_FOR_JOURNAL_ENTRIES 1

/**
 *  Convert a time (us) to a string representation.
 *  @method _time_str
 *  @param  us      The time (us) {usec_t);
 *
 *  @note   Since this uses a static buffer, it should be called once per
 *          fprintf().
 *
 *  @return The formatted time string {char*};
 *  @private
 */
static char*
_time_str( usec_t time ) {
  time_t      secs  = (time / USEC_PER_SEC);
  struct tm*  tmp   = localtime( &secs );
  size_t      rc;
  memset((void*)_timeStr, 0, sizeof(_timeStr));

  rc = strftime( (char*)_timeStr, sizeof(_timeStr), "%F@%T", tmp );
  if (rc > 0) {
    snprintf( &_timeStr[rc], sizeof(_timeStr) - rc,
                ".%06llu", (time % USEC_PER_SEC));
  }

  return _timeStr;
}


/**
 *  Show the current journal entry
 *  @method _show_entry
 *  @param  journal   The target journal {sd_journal*};
 *
 *  @return The status (0 == success) {int};
 *  @private
 */
static int
_show_entry( sd_journal* journal ) {
  int         rc;
  const char* msg;
  size_t      msgLen;
  usec_t      realtime;
  /*
  usec_t      monotonic;
  sd_id128_t  bootId;
  // */

  rc = sd_journal_get_realtime_usec( journal, &realtime );
  if (rc < 0) {
    fprintf(stderr, "  *** Failed to retrieve realtime timestamp: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  /*
  rc = sd_journal_get_monotonic_usec( journal, &monotonic, &bootId );
  if (rc < 0) {
    fprintf(stderr, "  *** Failed to retrieve monotonic timestamp: %s\n",
            strerror( abs(rc) ));
    return rc;
  }
  // */

  fprintf(stdout, "%12llu : %s : {\n", realtime, _time_str( realtime ));
  do {
    rc = sd_journal_enumerate_data( journal, (void*)&msg, &msgLen );
    if (rc < 0) {
      fprintf(stderr, "  *** Failed to enumerate message data: %s\n",
              strerror( abs(rc) ));

    } else if (rc > 0) {
      fprintf(stdout, "  %.*s\n", (int)msgLen, msg);
    }

  } while (rc > 0);
  fprintf(stdout, "}\n");

  /*
  rc = sd_journal_get_data( journal, "MESSAGE", (const void**)&msg, &msgLen );
  if (rc) {
    fprintf(stderr, "*** Failed to read message field: %s\n",
            strerror( abs(rc) ));

  } else {
    fprintf(stdout, "%.*s\n", (int)msgLen, msg);
  }
  // */

  return rc;
}

/*****************************************************************************/
int
main( int argc, char** argv ) {
  int         rc;
  sd_journal* journal;

  rc = sd_journal_open( &journal, SD_JOURNAL_LOCAL_ONLY );
  if (rc) {
    fprintf(stderr, "*** Failed to open journal: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  // Show all entries and then continue to wait...
  rc = sd_journal_seek_head( journal );
  if (rc < 0) {
    fprintf(stderr, "*** Failed to seek to head: %s\n",
            strerror( abs(rc) ) );
  } else {
    bool  waiting = false;

    do {
      rc = sd_journal_next( journal );
      if (rc < 0) {
        fprintf(stderr, "*** Failed to retrieve next: %s\n",
                strerror( abs(rc) ));
        break;

      } else if (rc == 0) {
        // Pause ...
        if (! waiting) {
          waiting = true;

          fprintf(stdout, ">>> Wait for new entries ...");
        } else {
          fprintf(stdout, ".");
        }

        fflush(stdout);
        sleep( PAUSE_SECONDS_FOR_JOURNAL_ENTRIES );

      } else {
        if (waiting) {
          fprintf(stdout, "\n");
          waiting = false;
        }

        _show_entry( journal );
      }

    } while (rc >= 0);
  }

  sd_journal_close( journal );

  return 0;
}
