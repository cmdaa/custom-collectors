# Journald

journald is the part of [systemd](https://systemd.io/) that deals with logging.

From [systemd-journald.service](https://www.freedesktop.org/software/systemd/man/systemd-journald.service.html):
> systemd-journald is a system service that collects and stores logging data.
> It creates and maintains structured, indexed journals based on logging
> information that is received from a variety of sources:
> - Kernel log messages, via kmsg
> - Simple system log messages, via the libc [syslog(3)](http://man7.org/linux/man-pages/man3/syslog.3.html) call
> - Structured system log messages via the native Journal API, see
>   [sd_journal_print(3)](https://www.freedesktop.org/software/systemd/man/sd_journal_print.html)
> - Standard output and standard error of service units...
> - Audit records, originating from the kernel audit subsystem
>
> The daemon will implicitly collect numerous metadata fields for each log
> messages in a secure and unfakeable way. See [systemd.journal-fields(7)](https://www.freedesktop.org/software/systemd/man/systemd.journal-fields.html) for
> more information about the collected metadata.
>
> ...
>
> Log data collected by the journal is primarily text-based but can also
> include binary data where necessary. Individual fields making up a log record
> stored in the journal may be up to 2^64-1 bytes in size.
>
> The journal service stores log data either persistently below
> `/var/log/journal` or in a volatile way below `/run/log/journal/` (in the
> latter case it is lost at reboot). By default, log data is stored
> persistently if `/var/log/journal/` exists during boot, with an implicit
> fallback to volatile storage otherwise. Use `Storage=` in [journald.conf(5)](https://www.freedesktop.org/software/systemd/man/journald.conf.html) to
> configure where log data is placed, independently of the existence of
> `/var/log/journal/`.


For a decent overview of journald, see [Logging with
journald](https://sematext.com/blog/journald-logging-tutorial/).


## Fluentd
Fluentd has a plugin to ingest data from journald:
- `fluent-plugin-systemd`
  - https://github.com/fluent-plugin-systemd/fluent-plugin-systemd
  - https://www.fluentd.org/plugins/all

### Fluentd Configuration:
```
<source>
  @type systemd
  tag   journald
  path  /var/log/journal

  <storage>
    @type local
    path  /var/log/fluentd-journald-cursor.json
  </storage>

  <entry>
    fields_strip_underscores  true
    fields_lowercase          true
  </entry>
</source>

<match journald>
  @type stdout
</match>

<system>
  root_dir  /var/log/fluentd
</system>
```

**NOTE**: fluentd must be in the 'systemd-journal' system group in order to
read journald files.

**NOTE**: If fluentd is logging to STDOUT within a docker container, it will
ikely cause output to be logged to journald resulting in a logging loop. If
this is happening, you will need to alter the configuration:
- use another fluentd output;
- use 'matches' in the source configuration to avoid processing
  fluentd-generated journal messages;
- disable the systemd log driver when launching the fluentd docker container
  that processes journald message (e.g. `--log-driver json-file`);

**NOTE**: The included `tail.py` script requires the installation of the
`systemd` pip module, which requires the `python3-devel` and `systemd-devel` yum
packages:
``` bash
$ sudo yum install systemd-devel python3-devel
...
$ sudo pip3 install systemd-python
...
```

