#include <stdio.h>
#include <string.h>
#include <systemd/sd-journal.h>

int
main( int argc, char** argv ) {
  int         rc;
  sd_journal* journal;

  rc = sd_journal_open( &journal, SD_JOURNAL_LOCAL_ONLY );
  if (rc) {
    fprintf(stderr, "*** Failed to open journal: %s\n",
            strerror( abs(rc) ));
    return rc;
  }

  SD_JOURNAL_FOREACH( journal ) {
    const char* msg;
    size_t      msgLen;

    rc = sd_journal_get_data( journal, "MESSAGE", (const void**)&msg, &msgLen );
    if (rc) {
      fprintf(stderr, "*** Failed to read message field: %s\n",
              strerror( abs(rc) ));
      continue;
    }

    fprintf(stdout, "%.*s\n", (int)msgLen, msg);
  }

  sd_journal_close( journal );

  return 0;
}
