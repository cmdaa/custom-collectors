#######
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################
# sg-logs {
#
SGLOGS_IMAGE := $(shell make -sC sg-logs docker-image-name)
SGLOGS_TAG   := $(shell make -sC sg-logs docker-tag-name)

sg-logs-image:
	docker build \
	  --file Dockerfile.sg-logs \
	  --build-arg DOCKER_TAG=$(SGLOGS_TAG) \
	  --tag  $(SGLOGS_IMAGE):$(SGLOGS_TAG) \
	    .
	docker tag  $(SGLOGS_IMAGE):$(SGLOGS_TAG) $(SGLOGS_IMAGE):latest

sg-logs-push:
	docker push $(SGLOGS_IMAGE):$(SGLOGS_TAG)
	docker push $(SGLOGS_IMAGE):latest

# sg-logs }
################################################################
# prometheus-bridge {
#
prometheus-bridge-image:
	@make -C prometheus-bridge image

prometheus-bridge-push:
	@make -C prometheus-bridge push

# prometheus-bridge }
################################################################
# procfs-collector {
#
procfs-collector-image:
	@make -C procfs-collector image

procfs-collector-push:
	@make -C procfs-collector push

# procfs-collector }
################################################################
# proc-mon {
#
proc-mon-image:
	@make -C proc-mon image

proc-mon-push:
	@make -C proc-mon push

# proc-mon }
################################################################
