/**
 *  A Prometheus-like metric sample.
 *  @class  Sample
 *
 */
class Sample {
  static extract = _extractSample;

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config  The configuration data {String | Object};
   *
   *  If `config` is a string, it is expected to be in Prometheus Exposition
   *  format.
   *
   *  If `config` is an object, it should have one of the following forms:
   *    - { str           : A string in Prometheus Exposition format,
   *        [metric_class : The metric class [application],]
   *        [timestamp    : A scrape timestamp to use as the default [now],]
   *        [extra_labels : Additional labels to include,]
   *      }
   *
   *    - { name          : The metric name,
   *        labels        : { label/value pairs },
   *        value         : The sample value,
   *        [metric_class : The metric class [application],]
   *        [timestamp    : A scrape timestamp to use as the default [now],]
   *        [extra_labels : Additional labels to include,]
   *      }
   *
   *  :NOTE: Timestamp values should be milliseconds since the epoch.
   *
   *  :NOTE: If `labels` extracted via `_extractSample()` or provided via
   *         `config` include a '__metric_class' label, the label will be
   *         removed and the value promoted to the Sample's `metric_class`.
   */
  constructor( config ) {
    let timestamp = Date.now();
    let sample;

    if (typeof(config) === 'string') {
      sample = _extractSample( config );

    } else if (typeof(config.str) === 'string') {
      sample = _extractSample( config.str );

      if (typeof(config.timestamp) === 'number') {
        // Over-ride default timestamp
        timestamp = config.timestamp;
      }

    } else {
      // Requires 'name' and 'value'
      if (typeof(config.name) !== 'string' || config.name.length < 1) {
        throw new Error('invalid sample name');
      }
      if (config.value === undefined) {
        throw new Error('missing sample value');
      }

      switch( typeof(config.timestamp) ) {
        case 'number':
          timestamp = config.timestamp;
          break;

        case 'string':
          try {
            const ts  = new Date( config.timestamp );
            timestamp = ts.getTime();
          } catch(ex) {
            console.error('*** %s(): Ignore timestamp string[ %s ]:',
                    this.constructor.name, config.timestamp, ex);
          }
          break;
      }

      // Make a clone of 'config' excluding 'extra_labels'
      sample = Object.assign({}, config);
      if (sample.extra_labels) { delete sample.extra_labels }
    }

    if (typeof(sample.timestamp) !== 'number') {
      sample.timestamp = timestamp;
    }

    if (typeof(sample.labels) === 'object' &&
        sample.labels.hasOwnProperty('__metric_class')) {
      // Promote the '__metric_class' label to the metric_class of the sample
      sample.metric_class = sample.labels.__metric_class;
      delete sample.labels.__metric_class;
    }

    /* Sort labels by key while also generating a `nameLabel` which is a
     * combination of the sample name and sorted labels:
     *    %name%{%key%="%label%",...}
     */
    const allLabels     = Object.assign({},
                                        config.extra_labels || {},
                                        sample.labels);
    const sortedLabels  = {};
    const labelStr      = Object.keys( allLabels )
                            .sort()
                            .map( key => {
                              const val = allLabels[key];

                              sortedLabels[key] = val;
                              return `${key}="${val}"`;
                            })
                            .join(',');
    const nameLabel     = sample.name
                        + (labelStr.length > 0
                            ? `{${labelStr}}`
                            : '');

    /*
    console.log('=== %s(): sortedLabels[ %s ] => labelStr[ %s ]',
                this.constructor.name,
                JSON.stringify(sortedLabels),
                labelStr);
    // */

    // Replace the original labels with the sorted set.
    sample.labels = sortedLabels;

    // Ensure the sample has 'metric_class'
    if (typeof(sample.metric_class) !== 'string') {
      /* By default, use the first component of the metric name when split by
       * '_'. For example:
       *    'device_storage_write'  => 'device'
       */
      sample.metric_class = sample.name.split('_').shift();
      if (! sample.metric_class) {
        sample.metric_class = 'application';
      }
    }

    Object.defineProperties( this, {
      name        : { value: sample.name },
      metric_class: { value: sample.metric_class },
      labels      : { value: sample.labels },
      value       : { value: sample.value },
      timestamp   : { value: sample.timestamp },

      _nameLabel  : { value: nameLabel }, // name with sorted labels
      _sample     : { value: sample },    // extracted sample/source
    });
  }

  /**
   *  Convert this instance to a string in Prometheus Exposition format.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this._nameLabel} ${this.value} ${this.timestamp}`;
  }

  /**
   *  Convert this instance to a simple object.
   *  @method toJSON
   *
   *  @return A simple object representation of this instance {Object};
   */
  toJSON() {
    return this._sample;
  }
}

/*****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Given a metric sample in Prometheus Exposition format, split it out into
 *  metric name, labels, value, and timestamp.
 *  @method _extractSample
 *  @param  line      The metric sample line {String};
 *
 *    metric_name  | labels                | val  | timestamp
 *    -------------+-----------------------+------+-----------
 *    %metric_name%[{%label%="%val%[,...]}] %val% [ %ts%]
 *
 *  @note   Each metric sample MAY have an over-riding timestamp
 *
 *  @return The extracted sample information {Object}
 *            { name      : The metric name,
 *              labels    : { label/value pairs },
 *              value     : The sample value,
 *              timestamp : Any explicit timestamp,
 *            }
 *  @private
 */
function _extractSample( line ) {
  const sample      = {
    name      : null,
    labels    : {},
    value     : null,
    timestamp : null,
  };
  const labelStart  = line.indexOf('{');
  let   rest        = '';

  /* :NOTE: A sample line is expected to have the form:
   *          metric_name{label1="val1",label2="val2"} metric_val [timestamp]
   *
   *        This allows:
   *        - simple splitting of labels by       ",
   *        - simple splitting of label/value by  ="
   */
  if (labelStart >= 0) {
    let   labelCnt    = 0;
    const labelEnd    = line.indexOf('}');
    const labelParts  = line
                          .slice( labelStart+1, labelEnd-1 )
                          .split(/"\s*,\s*/); //.split('",');
    const labels      = {};

    rest        = line.slice( labelEnd+2 );
    sample.name = line.slice( 0, labelStart );

    labelParts.forEach( part => {
      if (part == null || part.length < 1)  { return }

      const [key,val] = part.split(/\s*=\s*"/); //part.split('="');
      if (val === undefined) {
        /* This label component is not properly formatted, which means this
         * entire sample is suspect, likely truncated due to not receiving all
         * data from the endpoint.
         */
        console.error('*** Invalid label part[ %s ] from sample: %s',
                      part, line);
        return;
      }

      labels[key] = val;
      labelCnt++;
    });

    if (labelCnt > 0) { sample.labels = labels }

  } else {
    // White-space split
    const ws   = line.indexOf(' ');
    rest        = line.slice( ws+1 );
    sample.name = line.slice( 0, ws );
  }

  if (rest && rest.length > 0) {
    const tokens  = rest.trim().split(/\s+/);

    sample.value = tokens[0];

    /*
    const num = parseFloat( tokens[0] );
    if ( ! Number.isNaN( num ) ) {
      sample.value = num;
    }
    // */

    if (tokens.length > 1) {
      const timestamp = parseInt( tokens[1] );

      if ( ! Number.isNaN( timestamp ) ) {
        sample.timestamp = timestamp;
      }
    }
  }

  /*
  console.log('%s => %s : rest[ %s ]',
              line, JSON.stringify( sample ), rest);
  // */

  return sample;
}

/* Private helpers }
 *****************************************************************************/

module.exports  = Sample;
