/**
 *  Given a numeric codepoint, encode it according to the specified form.
 *  @method encode_codepoint
 *  @param  codepoint       The target codepoint {Number};
 *  @param  [forJson=false] If true, for non-ASCII characters use
 *                          JSON-compatible encoding '\u####', otherwise use
 *                          compact encoding '\x#' {Boolean};
 *
 *  @return The encoded codepoint {String};
 */
function encode_codepoint( codepoint, forJson=false ) {
  const hex = codepoint.toString(16);

  if (forJson) {
    /* :NOTE: Javascript encodes Unicode characters that require more than 4
     *        hex digits as '\u{####}' while JSON requires exacly 4 hex digits,
     *        no more and no less.
     *
     *        For Javascript:
     *          if (hex.length > 4) { return '\\u{'+ hex +'}' }
     */

    return '\\u'+ hex.padStart(4,'0');
  }

  if (codepoint < 0x80) {
    return '\\x'+ hex.padStart(2,'0');;

  } else {
     if (hex.length > 4) { return '\\u{'+ hex +'}' }

    return '\\u'+ hex.padStart(4,'0');
  }
}

/**
 *  Given a string or buffer, return a version with any non-ASCII characters
 *  escaped, possibly in a JSON-compatible form.
 *  @method esc
 *  @param  src             The target data source {String | Buffer};
 *  @param  [forJson=false] If true, for non-ASCII characters use
 *                          JSON-compatible encoding '\u####', otherwise use
 *                          compact encoding '\x#' {Boolean};
 *
 *  @note   Proper handling of characters above the normal ASCII range
 *          (0x80 .. ff) requires that the incoming `src` be passed in as
 *          either a Buffer or UTF8 String.
 *
 *  @return A new Buffer {Buffer};
 */
function esc( src, forJson=false ) {
  const isBuf     = Buffer.isBuffer( src );
  const sequences = [];

  for (let idex = 0; idex < src.length; idex++) {
    let   seq   = src[idex];
    const code  = (isBuf ? seq : seq.codePointAt(0));

    if (code < 0x20) {
      /*****************************************************
       * Control character. Generate a representative escape
       * sequence.
       *
       * The JSON spec only allows escape sequences:
       *  \"    "
       *  \'    '
       *  \/    /
       *  \b    backspace           (8)
       *  \t    horizontal-tab      (9)
       *  \n    new-line            (10)
       *  \f    new-page/form-feed  (12)
       *  \r    carriage-return     (13)
       *  \u####  where # are hex digits
       */
      switch( code ) {
        case 8  : seq = '\\b'; break;
        case 9  : seq = '\\t'; break;
        case 10 : seq = '\\n'; break;
        case 12 : seq = '\\f'; break;
        case 13 : seq = '\\r'; break;

        default :
          seq = encode_codepoint( code, forJson );

          /*
          console.log('1-byte @%d: CTRL  [ 0x%s ] => [ %s ]',
                       idex, code.toString(16), seq);
          // */
          break;
      }

    } else if (code <= 0x7f) {
      /*****************************************************
       * ASCII range: 0x20 .. 7f
       *
       */
      seq = String.fromCodePoint( code );

      /*
      console.log('1-byte @%d: ASCII [ 0x%s ] => [ %s ]',
                   idex, code.toString(16), seq);
      // */

    } else {
      /*****************************************************
       * Non-ASCII range: 0x80 .. ff
       *
       */
      if (!isBuf) {
        /* The source is NOT a buffer (String) so we must ASSUME it has already
         * been properly UTF8 encoded and this byte simply needs to be
         * converted to the encoded form.
         */
        seq = encode_codepoint( code, forJson );

        /*
        console.log('n-byte @%d: UTF8 [ 0x%s ] => [ %s ]',
                     idex, code.toString(16), seq);
        // */

      } else {
        /* The source is a buffer so we will need to handle raw UTF8 encoded
         * data.
         *
         *  See if this is a UTF-8 encoding:
         *    0x00 .. 7f    1 byte encoding : ASCII             Byte 0
         *    0xc0 .. df    2 byte encoding : U+0080  U+07FF    110xxxxx
         *    0xe0 .. ef    3 byte encoding : U+0800  U+FFFF    1110xxxx
         *    0xf0 .. ff    4 byte encoding : U+10000 U+10FFFF  11110xxx
         *
         *                                                      Byte 1-n
         *                                                      10xxxxxx
         *
         *  Ref: https://en.wikipedia.org/wiki/UTF-8
         */
        let   nByte = 1;
        let   mask0 = 0x1f;
        const mask  = 0x3f;
        let   u32   = code;

        if      (code <= 0xdf) { /* 2-byte */  mask0 = 0x1f; nByte = 2 }
        else if (code <= 0xef) { /* 3-byte */  mask0 = 0x0f; nByte = 3 }
        else /* (code <= 0xff*/{ /* 4-byte */  mask0 = 0x07; nByte = 4 }

        // Decode
        u32 &= mask0;

        for (let bdex = 1; bdex < nByte; bdex++) {
          const chr = (isBuf ? src[idex+bdex] : src.codePointAt(idex+bdex));

          u32 = (u32 << 6) | (chr & mask);
        }

        // Convert to a JSON-compatible form
        seq = encode_codepoint( u32, forJson );

        /*
        console.log('%d-byte @%d: UTF8  [ 0x%s ] => [ %s ]',
                     nByte, idex, code.toString(16), seq);
        // */

        idex += (nByte - 1);
      }
    }


    sequences.push( seq );
  }

  return Buffer.from( sequences.join('') );
}

/**
 *  Given a string or buffer, return a version with any non-ASCII characters
 *  escaped in a JSON-compatible form '\u####'.
 *  @method json_safe
 *  @param  str   The target string or buffer {String | Buffer};
 *
 *  @return A new Buffer that should be parsable by JSON.parse() {Buffer};
 */
function json_safe( str ) {
  return esc( str, 'forJson' );
}

module.exports = {
  encode_codepoint: encode_codepoint,
  esc             : esc,
  json            : json_safe,
};
