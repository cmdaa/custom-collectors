const Dns = require('dns').promises;

/**
 *  A reverse DNS (IPv4) lookup cache
 *  @class  ReverseCache
 */
class ReverseCache {
  /**
   *  Create a new DNS (IPv4) reverse lookup cache.
   *  @constructor
   *  @param  config                  The cache configuration {Object};
   *  @param  [config.ttl = 300]      The time-to-live (ttl) for cache entries
   *                                  (seconds) {Number};
   *  @param  [config.verbosity = 0]  Debug verbosity {Number};
   */
  constructor( config = {ttl: 300, verbosity: 0} ) {
    this.config = config;

    Object.defineProperties( this, {
      _ttl        : { value: config.ttl * 1000 }, // ttl in ms
      _verbosity  : { value: config.verbosity },
      _cache      : { value: {} },

      _flushTimer : { value: null, writable: true },
    });
  }

  /**
   *  Perform a reverse lookup/resolution for the given IPv4 address.
   *  @method resolve
   *  @param  ip      The target IPv4 address {String};
   *
   *  @return A promise for results:
   *          - on success, an array of hostnames {Array};
   *          - on failure, an error {Error};
   */
  async resolve( ip ) {
    const cached = this._cache[ ip ];
    if (cached != null) {
      if (this._verbosity > 0) {
        console.log('=== resolve( %s ): cached entry:', ip, cached);
      }

      if (cached.isExpired()) {
        // This cache entry has expired
        if (this._verbosity > 0) {
          console.log('=== resolve( %s ): cached entry has expired', ip);
        }

        delete this._cache[ ip ];

      } else {
        // Return the hostnames from this cache entry
        return cached.hostnames;
      }
    }

    if (this._verbosity > 0) {
      console.log('=== resolve( %s ): full reverse lookup ...', ip);
    }

    // Perform a full resolution
    const promise = Dns.reverse( ip );

    promise
      .then( hostnames => {
        // Success
        if (this._verbosity > 0) {
          console.log('=== resolve( %s ): lookup complete:', ip, hostnames);
        }

        const cached  = new CacheEntry( ip, hostnames, this._ttl );

        this._cache[ ip ] = cached;

        return hostnames;
      })
      .catch( err => {
        // Failure
        if (this._verbosity > 0) {
          console.log('*** resolve( %s ): lookup FAILED:', ip, err);
        }

        return err;
      })
      .finally( () => {
        /* Regardless of the results, if we do not have a flush pending,
         * schedule one.
         */
        if (this._flushTimer == null) {
          // Setup a flush timer to flush the cache after 'ttl' ms
          this._flushTimer = setTimeout( () => {
            this._flushTimer = null;

            this.flush();
          }, this._ttl + 10);
        }

      });

    return promise;
  }

  /**
   *  Flush any expired entries from the cache.
   *  @method flush
   *
   *  @return The number of entries that were flushed {Number};
   */
  flush() {
    let flushed = 0;
    Object.entries( this._cache ).forEach( ([ip,entry]) => {
      const isExpired = entry.isExpired();

      if (this._verbosity > 0) {
        console.log('=== flush(): %s: age[ %s ms ], ttl[ %s ms ]: %sexpired',
                    ip, entry.age, entry.ttl,
                    (isExpired ? ' ' : '!') );
      }

      if (isExpired) {
        // This cache entry has expired
        delete this._cache[ ip ];

        flushed++;
      }
    });

    return flushed;
  }
}

/**
 *  A cache entry that can compute its age and determine whether it has
 *  expired.
 *  @class  CacheEntry
 */
class CacheEntry  {
  /**
   *  Create a new cache entry.
   *  @constructor
   *  @param  ip          The target IPv4 address {String};
   *  @param  hostnames   The resolved hostnames {Array};
   *  @param  ttl         The time-to-live (ttl) for this entry (ms) {Number};
   */
  constructor( ip, hostnames, ttl ) {
    this.ip        = ip;
    this.hostnames = hostnames;
    this.ttl       = ttl;

    this._birth    = Date.now();
  }

  /**
   *  The age of this entry (ms).
   *  @property age
   */
  get age() {
    return Date.now() - this._birth;
  }

  /**
   *  Check if this entry has expired.
   *  @method isExpired
   *
   *  @return An indication of whether this entry has expired {Boolean};
   */
  isExpired() {
    return ( this.age >= this.ttl );
  }
}

module.exports = ReverseCache;
