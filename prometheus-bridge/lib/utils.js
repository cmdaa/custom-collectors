/**
 *  General helpers and utilities.
 *
 */
const Util      = require('util');
const Fs        = require('fs');
const Duration  = require('parse-duration');

/**
 *  Create a contextualized error instance that includes the name of the source
 *  class.
 *  @method makeError
 *  @param  self    The source class instance;
 *  @param  message The error message {String};
 *
 *  @return A new error instance {Error};
 */
function makeError( self, message ) {
  return new Error( `${self.constructor.name}: ${message}` )
}

/**
 *  Invoke `Util.inspect()` with a set of default/common options.
 *  @method inspect
 *  @param  obj     The item to inspect {Mixed};
 *  @param  [opts]  Additional/overriding options {Object};
 *
 *  @return The generated representation {String};
 */
function inspect( obj, opts={} ) {
  opts = Object.assign({colors:true, depth:10, breakLength:70}, opts || {});

  return Util.inspect( obj, opts );
}

/**
 *  Given a configuration object, convert any timeout/interval duration strings
 *  to numbers (ms)
 *  @method convertDurations
 *  @param  config  The configuration {Object};
 *
 *  @note   This will convert any keys that end with '_interval' or '_timeout'
 *
 *  @return `config` with durations converted {Object};
 */
function convertDurations( config ) {
  const reMatch = /_(interval|timeout)$/;

  Object.entries( config ).forEach( ([key,val]) => {
    if (typeof(val) !== 'string' || ! reMatch.test( key ))  { return }

    config[ key ] = Duration( val );

    /*
    console.log('_processDurationKeys(): key[ %s ], val[ %s ] => [ %s ]',
                key, val, config[ key ]);
    // */
  });

  return config;
}

/**
 *  Given a configuration object, look for any value with a prefix of 'file:'
 *  and read the file as the new value.
 *  @method expandValues
 *  @param  config  The configuration {Object};
 *
 *  @return `config` with 'file:' values updated {Object};
 */
function expandValues( config ) {
  const rePrefix  = /^([^:]+):(.+)$/;

  Object.entries( config ).forEach( ([key,val]) => {
    let match;

    if (typeof(val) !== 'string' || (match = val.match( rePrefix )) == null) {
      return;
    }
    const [ str, prefix, path ] = match;

    /*
    console.log('_processValuePrefixes(): key[ %s ], val[ %s ] => '
                +     'prefix[ %s ], path[ %s ]',
                key, val, prefix, path);
    // */

    switch( prefix ) {
      case 'file': {
        let data;

        // Remember the file path as an independent key
        config[ `${key}_path` ] = path;

        try {
          data = Fs.readFileSync( path, 'utf8' );

          config[ key ] = data.trim();

        } catch(ex) {
          /* The target file is not accessible so remember the error and empty
           * the key
           */
          config[ `${key}_error` ] = ex;
          config[ key ]            = undefined;

          /*
          console.warn(`*** Config key '%s' file path '%s': %s`,
                       key, path, ex);
          // */
        }

      } break;
    }
  });

  return config;
}

/**
 *  Convert a URL instance to a simple object.
 *  @method url2obj
 *  @param  url     The target url instance {URL};
 *
 *  @return A simple object with the same property values {Object};
 */
function url2obj( url ) {
  const obj   = {};

  Object.getOwnPropertyNames( Object.getPrototypeOf( url ) ).forEach( key => {
    if (typeof(url[key]) === 'function') { return }

    obj[key] = url[key];
  });

  return obj;
}

/**
 *  Convert a camel-case string to underscore separated.
 *  @method camelCase2_str
 *  @param  str     The target string {String};
 *
 *  @return An underscore separated version of `str` {String};
 */
function camelCase2_str( str ) {
  return str.replace( /(\p{Uppercase_Letter})/gu, '_$1' )
            .toLowerCase();
}

module.exports = {
  makeError       : makeError,
  inspect         : inspect,
  convertDurations: convertDurations,
  expandValues    : expandValues,
  url2obj         : url2obj,
  camelCase2_str  : camelCase2_str,
};
