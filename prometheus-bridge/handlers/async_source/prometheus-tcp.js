const Utils         = require('../../lib/utils');
const Sample        = require('../../lib/sample');
const ReverseCache  = require('../../lib/dns-reverse')
const AsyncSource   = require('./index.js');
const Net           = require('net');

/**
 *  A TCP server that processes asynchronous metrics as new-line separated
 *  lines each in Prometheus Exposition format.
 *
 *  @class  PrometheusTcpSource
 *  @extend AsyncSource
 *
 *  @note   An incoming label of '__metric_class' will be extracted for use as
 *          the generated Sample's 'metric_class'
 *
 *  @emit   'sample', sample: emitted when an metric is made available where
 *                            `sample` is a Sample instance;
 *  @emit   'error', err    : emitted on client/consumer error;
 */
class PrometheusTcpSource extends AsyncSource {
  static xoff  = Buffer.from( [19] );
  static xon   = Buffer.from( [17] );

  static get defaults() {
    return Object.assign({}, super.defaults, {
      host  : '0.0.0.0',
      port  : 3377,
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                      The configuration data {Object};
   *  @param  config.url                  The URL of the remote UDP endpoint
   *                                      (prometheus:// | prometheus-tcp://)
   *                                      {URL};
   *  @param  [config.dry_run = false]    If truthy, do NOT attempt to connect
   *                                      {Boolean};
   *  @param  [config.labels]             Additional labels that should be
   *                                      applied to metrics from this source
   *                                      {Object};
   *  @param  [config.verbosity = 0]      Debug verbosity {Number};
   */
  constructor( config ) {
    super( config );

    /* assert( this.config.url.protocol === 'prometheus:' ||
     *         this.config.url.protocol === 'prometheus-tcp:' );
     */

    // Explicit URL values over-ride defaults
    if (this.config.url && this.config.url.hostname) {
      this.config.host = this.config.url.hostname;
    }
    if (this.config.url && this.config.url.port) {
      this.config.port = this.config.url.port;
    }

    const reverseConfig = {
      ttl       : 300,
      verbosity : config.verbosity,
    };

    Object.defineProperties( this, {
      type    : { value: 'prometheus', enumerable: true },

      server  : { value: null,    writable: true },

      paused  : { value: false,   writable: true },
      socks   : { value: [],      writable: true },

      _dnsRev : { value: new ReverseCache( reverseConfig ) },
    });
  }

  get xon()   { return this.constructor.xon }
  get xoff()  { return this.constructor.xoff }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.config.url}]`;
  }

  /**
   *  Start this source.
   *  @method start
   *
   *  @return A promise resolved when this source is ready to generate metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = this._initServer();
    }

    return this._ready;
  }

  /**
   *  Pause this source.
   *  @method pause
   *
   *  @return A promise resolved when this source has been paused {Promise};
   */
  async pause() {
    /* Iterate over all current clients and send a TCP-based xoff requesting
     * that they pause (flow control).
     */
    const nclients  = this.socks.length;
    this.paused = true;

    if (this.config.verbosity > 0) {
      console.error('=== %s: pause(): xoff => %d clients',
                    this.constructor.name, nclients);
    }

    this.socks.forEach( sock => {
      this._pause( sock );
    });

    return nclients;
  }

  /**
   *  Resume this source.
   *  @method resume
   *
   *  @return A promise resolved when this source has been resumed {Promise};
   */
  async resume() {
    /* Iterate over all current clients and send a TCP-based xon requesting
     * that they resume (flow control).
     */
    const nclients  = this.socks.length;
    this.paused = false;

    if (this.config.verbosity > 0) {
      console.error('=== %s: resume(): xon  => %d clients',
                    this.constructor.name, nclients);
    }
    if (nclients > 0) {
      this.socks.forEach( sock => {
        this._resume( sock );
      });
    }

    return nclients;
  }

  /**
   *  Stop this source.
   *  @method stop
   *
   *  @return A promise resolved when this source has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {

      this._stopping = new Promise( (resolve, reject) => {
        if (this.server == null) {
          // No socket/server
          return resolve( false );
        }

        // Close the socket/server
        this.server.close( () => {
          this.server = null;

          if (this.config.verbosity >= 0) {
            console.error('=== %s: socket/server closed',
                          this.constructor.name);
          }

          resolve( true );
        });
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Initialize the Prometheus socket/server.
   *  @method _initServer
   *
   *  @return A ready promise {Promise};
   *  @private
   */
  _initServer() {

    return new Promise( (resolve, reject) => {
      // Create a TCP server as an asyncronous metric sink
      this.server = Net.createServer();

      this.server
        .once('listening',  ()    => this._handleListen( resolve ) )
        .on(  'error',      err   => this._handleError( reject, err ) )
        .on(  'connection', sock  => this._handleConnect( sock ) )
        .on(  'close',      ()    => this._handleClose() );

      if (this.config.verbosity >= 1) {
        console.error('=== %s: listen on %s:%s ...',
                      this.constructor.name, this.config.host,
                      this.config.port);
      }

      // Listen for incoming connections
      this.server.listen( this.config.port, this.config.host );
    });
  }

  /**
   *  Handle a socket 'listen' event.
   *  @method _handleListen
   *  @param  resolve   The resolver for the ready promise {Function};
   *
   *  @protected
   */
  _handleListen( resolve ) {
    const addr  = this.server.address();

    if (this.config.verbosity >= 0) {
      console.error('=== %s: server.listening on %s:%s',
                    this.constructor.name, addr.address, addr.port);
    }

    // Signal "ready"
    this.ready = Date.now();
    resolve( this.ready );
  }

  /**
   *  Handle a socket 'error' event.
   *  @method _handleError
   *  @param  reject    The rejector for the ready promise {Function};
   *  @param  err       The error {Error};
   *
   *  @return void
   *  @protected
   */
  _handleError( reject, err ) {
    if (! this.ready) {
      if (this.config.verbosity > 0) {
        console.error('*** %s: initialization failed: %s',
                      this.constructor.name, err);
      }

      reject( err );

    } else {
      if (this.config.verbosity > 0) {
        console.error('*** %s: server.error:', this.constructor.name, err);
      }

      this.emit( 'error', err );
    }
  }

  /**
   *  Handle a 'close' event
   *  @method _handleClose
   *
   *  @return void
   *  @protected
   */
  _handleClose() {
    if (this.config.verbosity >= 0) {
      console.error('=== %s: server.close',
                    this.constructor.name);
    }

    // Mark as not-ready
    this.ready = false;
  }

  /**
   *  Handle a new, incoming connection.
   *  @method _handleConnect
   *  @param  sock      The socket for the new, incoming connection {Socket};
   *
   *  @protected
   */
  _handleConnect( sock ) {
    const remote  = sock.address();

    // Add this socket to our list and assign an id.
    this.socks.push( sock );
    sock.__id = this.socks.length;

    if (remote.address === '127.0.0.1' || remote.address === '::1') {
      remote.hostnames = [ 'localhost' ];
      this._handleClient( sock, remote );

    } else {
      let resolved  = [];

      this._dnsRev.resolve( remote.address )
        .then( hostnames => { resolved = hostnames } )
        .catch( err => {
          console.warn('*** %s: resolve of [ %s ] failed:',
                      this.constructor.name, remote.address, err);
        })
        .finally( () => {
          remote.hostnames = ( Array.isArray(resolved) && resolved.length > 0
                                ? resolved
                                : [ remote.address ] );

          this._handleClient( sock, remote );
        });
    }
  }

  /**
   *  Once the remote client has been identified, handle the client.
   *  @method _handleClient
   *  @param  sock    The incoming connection {Socket};
   *  @param  remote  The remote address information {Object};
   *
   *  `remote` includes:
   *    address   {String};
   *    family    {String} (IPv4, IPv6);
   *    port      {Number};
   *    size      {Number};
   *    hostnames {Array};
   *
   *  @protected
   */
  _handleClient( sock, remote ) {
    const hostname  = (Array.isArray(remote.hostnames) &&
                       remote.hostnames.length > 0
                        ? remote.hostnames[0]
                        : remote.address);

    if (this.config.verbosity >= 0) {
      console.error('=== %s: %s client connected: %s [ %s ]: %d clients',
                    this.constructor.name, remote.family,
                    hostname, remote.address, this.socks.length);
    }

    // Pass along useful meta-data
    sock._remote   = remote;
    sock._hostname = hostname;

    // Include a placeholder for an incomplete message as well as a line counter
    sock._incomplete = null;
    sock._lineCount  = 0;

    sock
      .on('error',  err   => this._handleSocketError( sock, err ))
      .on('data',   data  => this._handleSocketData( sock, data ))
      .on('end',    ()    => this._handleSocketEnd( sock ));

    if (this.paused) {
      // Pause this new client (xoff)
      this._pause( sock );
    }
  }

  /**
   *  Handle an error on a particular socket.
   *  @method _handleSocketError
   *  @param  sock      The target socket {Socket};
   *  @param  err       The error {Error};
   *
   *  @return void
   *  @protected
   */
  _handleSocketError( sock, err ) {
    const remote    = sock._remote;
    const hostname  = sock._hostname;

    if (this.config.verbosity > 0) {
      console.error('*** %s: %s %s client.error:',
                    this.constructor.name, remote.family, hostname, err);
    }

    this.emit( 'error', err );
  }

  /**
   *  Handle an incoming Prometheus message.
   *  @method _handleSocketData
   *  @param  sock    The source socket {Socket};
   *  @param  data    The incoming data {Buffer};
   *
   *  @return void
   *  @protected
   */
  _handleSocketData( sock, data ) {
    const remote    = sock._remote;
    const hostname  = sock._hostname;
    let   msg       = data;
    let   json, sample, lines;

    if (Buffer.isBuffer(msg)) {
      msg = msg.toString();
    }

    if (sock._incomplete) {
      msg = sock._incomplete + msg;

      sock._incomplete = null;
    }

    // Split by newline
    lines = msg.split( /\s*\n\s*/ )

    // If the final string is empty, filter it out (final newline)
    if (lines.length > 0 && lines[ lines.length - 1 ].length < 1) {
      lines.pop();
    }

    if (! msg.endsWith('\n')) {
      // Save the last entry as an incomplete line
      sock._incomplete = lines.pop();
    }

    if (this.config.verbosity > 1) {
      console.error('=== %s: %s %d byte msg[ %s ], %d lines',
                    this.constructor.name, hostname,
                    msg.length, msg, lines.length);
    }

    // Process each line, expected to be in Prometheus Exposition format
    const config  = {
      extra_labels  : Object.assign({}, this.config.labels || {}, {
        node  : hostname,
        unit  : null,
      }),
    };

    lines.forEach( line => {
      sock._lineCount++;

      if (line == null || line.length < 1)  { return }

      let data, sample;

      try {
        data = Object.assign( Sample.extract( line ), config );

      } catch( err ) {
        this.emit( 'error', err );
        return;
      }


      if (this.config.verbosity > 1) {
        console.error('=== %s: %s line[ %d:%s ] => extract[ %s ]',
                      this.constructor.name, hostname,
                      sock._lineCount, line, JSON.stringify(data));
      }

      /* Minimal label normalization:
       *    units => unit
       */
      if (data.labels.unit == null && data.labels.units != null) {
        data.labels.unit = data.labels.units;
        delete data.labels.units;

        if (this.config.verbosity > 1) {
          console.error('=== %s: %s normalize labels.units to'
                        +                     ' labels.unit[ %s ]',
                        this.constructor.name, hostname,
                        data.labels.unit);
        }
      }

      if (this.config.verbosity > 1) {
        console.error('=== %s: %s data[ %s ]',
                      this.constructor.name, hostname,
                      JSON.stringify(data));
      }

      try {
        sample  = new Sample( data );

      } catch( err ) {
        this.emit( 'error', err );
        return;
      }

      if (this.config.verbosity > 1) {
        console.error('=== %s: %s sample[ %s ]',
                      this.constructor.name, hostname,
                      JSON.stringify(sample));
      }

      this.emit( 'sample', sample );
    });
  }

  /**
   *  Handle a socket end.
   *  @method _handleSocketEnd
   *  @param  sock      The client connection {Socket};
   *
   *  @return void
   *  @protected
   */
  _handleSocketEnd( sock ) {
    const remote    = sock._remote;
    const hostname  = sock._hostname;

    if (this.config.verbosity >= 0) {
      console.error('=== %s: %s client disconnected: %s [ %s ]',
                    this.constructor.name, remote.family, hostname,
                    remote.address);
    }

    // Remove this socket from our list
    const idex = this.socks.indexOf( sock );
    if (idex >= 0) {
      this.socks.splice( idex, 1 );
    } else {
      console.warn('*** %s: %s cannot find socket %d for %s [ %s ]',
                    this.constructor.name, remote.family,
                    sock.__id,
                    hostname, remote.address);
    }
  }

  /**
   *  Send an pause/xoff to the given socket
   *  @method _pause
   *  @param  sock
   *
   *  @protected
   */
  _pause( sock ) {
    sock.write( this.xoff );

    // :XXX: Should we also do sock.pause()?
  }

  /**
   *  Send an resume/xon to the given socket
   *  @method _resume
   *  @param  sock
   *
   *  @protected
   */
  _resume( sock ) {
    // :XXX: Should we also do sock.resume()?

    sock.write( this.xon );
  }

  /* Protected methods }
   **************************************************************************/
}

module.exports  = PrometheusTcpSource;
