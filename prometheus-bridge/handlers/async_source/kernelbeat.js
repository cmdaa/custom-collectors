const Utils         = require('../../lib/utils');
const Sample        = require('../../lib/sample');
const Safe          = require('../../lib/safe');
const ReverseCache  = require('../../lib/dns-reverse')
const AsyncSource   = require('./index.js');
const Os            = require('os');
const Fs            = require('fs');
const Net           = require('net');
const Exec          = require('child_process').execFile

/**
 *  This async source can receive KernelBeat data from either the local
 *  `/dev/kernel_beat` character device or via TCP messages sent from the
 *  KernelBeat module when it detects that its local reader is falling behind
 *  (indicating that the system is having issues).
 *
 *  @class  KernelBeatSource
 *  @extend AsyncSource
 *
 *  @emit   'sample', sample: emitted when an metric is made available where
 *                            `sample` is a Sample instance;
 *  @emit   'error', err    : emitted on client/consumer error;
 */
class KernelBeatSource extends AsyncSource {

  static get defaults() {
    return Object.assign({}, super.defaults, {
      host          : '0.0.0.0',
      port          : 3333,

      /* When using the local `/dev/kernel_beat` character device as a file
       * source {
       */
      beat_interval : '30s',
      beat_sync     : 10,   // 5 minutes with an interval of 30s

      buf_size      : 4096, // Read buffer size
      poll_interval : '1s', // Read poll interval
      // file source }
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                      The configuration data {Object};
   *  @param  config.url                  The URL of the remote UDP endpoint
   *                                      (kernelbeat://) {URL};
   *  @param  [config.buf_size = 4096]    Buffer size when reading from a file
   *                                      source {Number};
   *  @param  [config.poll_interval = '1s']
   *                                      Polling interval when reading from a
   *                                      file source (ms) {String|Number};
   *  @param  [config.dry_run = false]    If truthy, do NOT attempt to connect
   *                                      {Boolean};
   *  @param  [config.labels]             Additional labels that should be
   *                                      applied to metrics from this source
   *                                      {Object};
   *  @param  [config.verbosity = 0]      Debug verbosity {Number};
   */
  constructor( config ) {
    super( config );

    // assert( this.config.url.protocol === 'kernelbeat:' );

    // Explicit URL values over-ride defaults
    if (this.config.url && this.config.url.hostname) {
      this.config.host = this.config.url.hostname;
    }
    if (this.config.url && this.config.url.port) {
      this.config.port = this.config.url.port;
    }

    Object.defineProperties( this, {
      type    : { value: 'kernelbeat', enumerable: true },

      server  : { value: null,    writable: true },
      _lastTs : { value: 0,       writable: true },
    });
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    const url = (this.config ? this.config.url : '<not-yet-set>');

    return `${this.constructor.name}[${url}]`;
  }

  /**
   *  Start this source.
   *  @method start
   *
   *  @return A promise resolved when this source is ready to generate metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = _initSource( this );
    }

    this._ready.then( res => { this.ready = true } )

    return this._ready;
  }

  /**
   *  Pause this source.
   *  @method pause
   *
   *  @return A promise resolved when this source has been paused {Promise};
   */
  async pause() {
    // :XXX: For a hard-stop/pause, we could also invoke this.stop()
    if (this.server == null) { return false }

    return this.server.pause();
  }

  /**
   *  Resume this source.
   *  @method resume
   *
   *  @return A promise resolved when this source has been resumed {Promise};
   */
  async resume() {
    // :XXX: For a hard-start/resume, we could also invoke this.start()
    if (this.server == null) { return false }

    return this.server.resume();
  }

  /**
   *  Stop this source.
   *  @method stop
   *
   *  @return A promise resolved when this source has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {

      this._stopping = new Promise( (resolve, reject) => {
        if (this.server == null) {
          // No file or socket server
          return resolve( false );
        }

        // Close the file or socket server
        this.server.close( () => {
          this.server = null;

          resolve( true );
        });
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this.ready = false;

        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }
}

/**
 *  A server-like wrapper for reading a local `/dev/kernel_beat` character
 *  device.
 *  @class  KernelBeatSource_dev
 *
 *  :NOTE:  This REQUIRES v0.16 of kernel-beat, which adds support for device
 *          polling.
 *
 *  :NOTE:  This class is completely dependent upon the controlling
 *          KernelBeatSource instance.
 */
class KernelBeatSource_dev {
  /**
   *  Constructor
   *  @constructor
   *  @param
   */
  constructor( src ) {
    const host = _hostname();

    Object.defineProperties( this, {
      _kbSource : { value: src },
      _listeners: { value: {
                      close   : this._onClose.bind( this ),
                      error   : this._onError.bind( this ),
                      readable: this._onReadable.bind( this ),
                    }
                  },
      host      : { value: host },

      _inDestroy: { value: false, writable: true },
      ready     : { value: null,  writable: true },
      sock      : { value: null,  writable: true },
      parts     : { value: [] },
      remote    : { value: { // TCP remote-like information
        family    : 'file',
        address   : src.config.url.pathname,
        hostnames : [ host, 'localhost' ],
      }},
    });

    // Generate a ready promise
    this.ready = new Promise( (resolve, reject) => {
      /* Establish the configured kernel beat interval and sync values before
       * we open kernel_beat for reading:
       *    sysctl kernel.beat.interval
       *    sysctl kernel.beat.sync
       */
      const sysctl_args = [
        `kernel.beat.interval=${ src.config.beat_interval }`,
        `kernel.beat.sync=${ src.config.beat_sync }`,
      ];

      _sysctl( sysctl_args )
        .then( res => {
          if (src.config.verbosity >= 0) {
            console.error('=== %s: sysctl complete[ %s ], interval=%d, sync=%d',
                          this.constructor.name, res,
                          src.config.beat_interval, src.config.beat_sync);
          }

          // Begin/resume reading
          return this.resume();
        })
        .then( res => {
          resolve( res );
        })
        .catch( err => {
          if (src.config.verbosity > 0) {
            console.error('*** %s: initialization failed: %s',
                          this.constructor.name, err);
          }

          src.emit( 'error', err );

          reject( err );
        });
    });
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    const src = this._kbSource;
    const url = (src && src.config ? src.config.url : '<not-yet-set>');

    return `${this.constructor.name}[${url}]`;
  }

  /**
   *  Pause reading.
   *  @method pause
   *
   *  For the kernel-beat character device, flow control pause is done by
   *  closing our file handle to the device.
   *
   *  @return A promise for results {Promise};
   */
  pause() {
    const src = this._kbSource;

    return new Promise( (resolve, reject) => {
      if (this.sock == null) { return resolve( true ) }

      console.error('=== %s.pause() ...', this.constructor.name);

      // Add finalization listeners
      this.sock
        .once('error', err => reject( err ) )
        .once('close', ()  => resolve( true ) );

      if (! this._inDestroy) {
        this._inDestroy = true; // avoid destroy/error loops

        /* Destroy our socket/wrapper which should close the underlying
         * kernel-beat file descriptor and trigger the 'close' event on the
         * socket.
         *
         * :NOTE: Operational listeners are removed in the _onClose handler
         */
        this.sock.destroy();
      }
    });
  }

  /**
   *  Resume reading.
   *  @method resume
   *
   *  For the kernel-beat character device, flow control resume is done by
   *  opening a file handle to the device.
   *
   *  @return A promise for results {Promise};
   */
  resume() {
    const src = this._kbSource;

    return new Promise( (resolve, reject) => {
      if (this.sock != null) { return resolve( true ) }

      // Attempt to open the target file
      Fs.open( src.config.url.pathname, 'r', (err, fd) => {
        if (err) {
          // Throw this error to trigger the 'catch' below
          return reject( err );
        }

        if (src.config.verbosity >= 0) {
          console.error('=== %s: opened [ %s ] for read',
                        this.constructor.name, src.config.url.pathname);
        }

        /* Wrap this file descriptor and create a socket that will be used to
         * allow interruptable, polling reads.
         */
        this.sock = this._makeSock( fd );

        // Reset our "being destroy" flag
        this._inDestroy = false;

        // Add operational listeners
        for (const [name, listener] of Object.entries( this._listeners )) {
          this.sock.on( name, listener )
        }

        // Signal ready
        resolve( true );
      });
    });
  }

  /**
   *  Close this pseudo-server.
   *  @method close
   *  @param  cb    A callback to invoke upon close {Function};
   */
  close( cb ) {
    const src = this._kbSource;

    // Use pause() to close any source file
    this.pause()
      .catch( err => {
        if (src.config.verbosity > 0) {
          console.error('*** %s: server.close error:',
                          this.constructor.name, err);
        }

        src.emit( 'error', err );
      })
      .finally( () => {
        // Invoke the callback
        if (cb && cb instanceof Function) {
          cb();
        }
      });
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Create a socket/pipe wrapper for the given file descriptor.
   *  @method _makeSock
   *  @param  fd    The primary file descriptor for the kernel-beat device
   *                {Number};
   *
   *  This socket wrapping allows the creation of a readable, interruptable
   *  stream. Without this wrapping, any attempt to close a handle to the
   *  /dev/kernel_beat device (or any stream wrapping such a handle) will hang
   *  until the next beat when data is made available and the read returns.
   *
   *  @return A socket that fully wraps the file descriptor;
   *  @protected
   */
  _makeSock( fd ) {
    const src   = this._kbSource;

    /* Create a pipe wrapper around the incoming file descriptor
     *  @ref  https://github.com/nodejs/node/issues/9285#issuecomment-256283359
     */
    const Wrap  = process.binding('pipe_wrap');
    const pipe  = new Wrap.Pipe( Wrap.constants.SOCKET );
    pipe.open( fd );

    /* Use the pipe wrapper/handle to create a pollable socket representing the
     * kernel-beat device.
     */
    const sock_opts = {
      highWaterMark : 0,  // src.config.buf_size
      readable      : true,
      writable      : false,
      handle        : pipe,
    };

    return new Net.Socket( sock_opts );
  }

  /**
   *  Handle a socket 'close' event.
   *  @method _onClose
   *
   *  @return void
   *  @protected
   */
  _onClose() {
    const src = this._kbSource;

    if (src.config.verbosity >= 0) {
      console.error('=== %s: %s closed',
                    this.constructor.name,
                    src.config.url.pathname);
    }

    // Remove all operational listeners
    for (const [name, listener] of Object.entries( this._listeners )) {
      this.sock.off( name, listener )
    }

    this.sock       = null;   // release our reference to the socket
    this._inDestroy = false;  // reset our "being destroyed" flag
  }

  /**
   *  Handle a socket 'error' event.
   *  @method _onError
   *  @param  err   The triggering error {Error};
   *
   *  @return void
   *  @protected
   */
  _onError( err ) {
    const src = this._kbSource;

    if (src.config.verbosity > 0) {
      console.error('*** %s: %s error:',
                    this.constructor.name, src.config.url.pathname, err);
    }

    if (this.sock && !this._inDestroy) {
      this._inDestroy = true; // avoid destroy/error loops

      /* Destroy our socket/wrapper which should close the underlying
       * kernel-beat file descriptor and trigger the 'close' event on the
       * socket.
       *
       * :NOTE: Operational listeners are removed in the _onClose handler
       */
      this.sock.destroy( err );
    }
  }

  /**
   *  Handle a socket 'readable' event.
   *  @method _onReadable
   *
   *  @return void
   *  @protected
   */
  _onReadable() {
    const src = this._kbSource;

    // Retrieve all available readable data placing each in our parts list
    let   data;
    while (data = this.sock.read() ) {
      this.parts.push( data );
    }

    const partCnt = this.parts.length;
    if (partCnt < 1) {
      // Nothing to process
      return;
    }

    /*************************************************
     * Process all data in our parts list
     *
     */
    const inBuf   = Buffer.concat( this.parts );
    const inLen   = inBuf.length;
    let   offset  = 0;
    let   eol;

    this.parts.length = 0;  // Reset our parts list

    while ( offset < inLen && (eol = inBuf.indexOf( 0x0a, offset )) > offset ) {
      const msg = inBuf.slice( offset, eol );

      if (msg.indexOf( 0x0a ) > 0) {
        /* :XXX: Not yet sure how this happens but at this point the solution
         *       is to re-split this full buffer.
         */
        _handleSubMessages( msg );

      } else {
        // Normal, single-message processing
        if (src.config.verbosity > 2) {
          console.error('=== %s: %s %s %d byte full msg from %d parts',
                        this.constructor.name, this.remote.family, this.host,
                        msg.length, partCnt);
        }

        _handleMessage( src, msg, this.remote );
      }

      // Move just beyond the current EOL
      offset = eol + 1;
    }

    if (offset < inLen) {
      /* Save a copy of the unused portion of the current buffer pending EOL
       *
       * :NOTE: We keep these as buffers to avoid potential decoding issues if
       *        there are UTF-8 characters with bytes that span multiple
       *        buffers.
       */
      this.parts.push( inBuf.slice( offset, inLen ) );

      if (src.config.verbosity > 2) {
        console.error('=== %s: %s %s %d byte partial msg -- %d buffered',
                      this.constructor.name, this.remote.family, this.host,
                      inLen - offset, this.parts.length );
      }
    }
  }

  /**
   *  Handle a strange condition where a "line" of data contains intermittent
   *  newline characters.
   *
   *  @method _handleSubMessages
   *  @param  msg     The problematic message {Buffer};
   *
   *  @return void
   *  @protected
   */
  _handleSubMessages( msg ) {
    const src = this._kbSource;

    console.error('*** %s: %s %s %d byte full msg contains newline @%d',
                  this.constructor.name, this.remote.family, this.host,
                  msg.length, msg.indexOf( 0x0a ));

    let msgCnt    = 0;
    let msgBytes  = msg.length;
    let msgOffset = 0;
    let msgEol;
    while ( msgOffset < msgBytes &&
            (msgEol = msg.indexOf( 0x0a, msgOffset )) > msgOffset ) {
      let subMsg  = msg.slice( msgOffset, msgEol );

      console.error('*** %s: %s %s %d byte subMsg %d',
                    this.constructor.name, this.remote.family, this.host,
                    subMsg.length, msgCnt);

      _handleMessage( src, subMsg, this.remote );

      msgCnt++;
      msgOffset = msgEol + 1;
    }
  }

  /* Protected methods }
   **************************************************************************/
}

/**
 *  A Net.server subclass to allow TCP-based flow-control.
 *  @class  KernelBeatSource_tcp
 *  @extend Net.Server
 *
 *  :NOTE:  This class is completely dependent upon the controlling
 *          KernelBeatSource instance.
 */
class KernelBeatSource_tcp extends Net.Server {
  static xoff  = Buffer.from( [19] );
  static xon   = Buffer.from( [17] );

  /**
   *  Constructor
   *  @constructor
   *  @param  src   The AsyncSource controller {AsyncSource};
   */
  constructor( src ) {
    super();

    const reverseConfig = {
      ttl       : 300,
      verbosity : src.config.verbosity,
    };

    Object.defineProperties( this, {
      _kbSource : { value: src },
      _dnsRev   : { value: new ReverseCache( reverseConfig ) },

      paused    : { value: false, writable: true },
      socks     : { value: [],    writable: true },

      ready     : { value: null,  writable: true },
    });

    this.ready = this._initServer();
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    const src = this._kbSource;
    const url = (src && src.config ? src.config.url : '<not-yet-set>');

    return `${this.constructor.name}[${url}]`;
  }

  get xon()   { return this.constructor.xon }
  get xoff()  { return this.constructor.xoff }

  /**
   *  Pause reading.
   *  @method pause
   *
   *  @return A promise for results {Promise};
   */
  async pause() {
    /* Iterate over all current clients and send a TCP-based xoff requesting
     * that they pause (flow control).
     */
    const src       = this._kbSource;
    const nclients  = this.socks.length;
    this.paused = true;

    if (src.config.verbosity > 0) {
      console.error('=== %s: pause() : TCP xoff => %d clients',
                    this.constructor.name, nclients);
    }
    if (nclients > 0) {
      this.socks.forEach( sock => {
        this._pause( sock );
      });
    }

    return nclients;
  }

  /**
   *  Resume reading.
   *  @method resume
   *
   *  @return A promise for results {Promise};
   */
  async resume() {
    /* Iterate over all current clients and send a TCP-based xon requesting
     * that they resume (flow control).
     */
    const src       = this._kbSource;
    const nclients  = this.socks.length;
    this.paused = false;

    if (src.config.verbosity > 0) {
      console.error('=== %s: resume(): TCP xon  => %d clients',
                    this.constructor.name, nclients);
    }
    if (nclients > 0) {
      this.socks.forEach( sock => {
        this._resume( sock );
      });
    }

    return nclients;
  }

  /***************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Initialize this socket/server.
   *  @method _initServer
   *
   *  @return A ready promise {Promise};
   *  @protected
   */
  _initServer() {
    const src = this._kbSource;

    return new Promise( (resolve, reject) => {

      // Has this promist been resolved?
      this._resolved = false;

      // Attach event handlers
      this
        .once('listening',  ()    => this._handleListen( resolve ) )
        .on(  'error',      err   => this._handleError( reject, err ))
        .on(  'connection', sock  => this._handleConnect( sock ) )
        .on(  'close',      ()    => this._handleClose() );

      if (src.config.verbosity >= 1) {
        console.error('=== %s: listen on %s:%s ...',
                      this.constructor.name, src.config.host, src.config.port);
      }

      // Listen for incoming connections
      this.listen( src.config.port, src.config.host );
    });
  }

  /**
   *  Handle a socket 'listen' event.
   *  @method _handleListen
   *  @param  resolve   The resolver for the ready promise {Function};
   *
   *  @return void
   *  @protected
   */
  _handleListen( resolve ) {
    const src   = this._kbSource;
    const addr  = this.address();

    if (src.config.verbosity >= 0) {
      console.error('=== %s: server.listening on %s:%s',
                    this.constructor.name, addr.address, addr.port);
    }

    this._resolved = true;  // Mark the promise "resolved"
    resolve( this );
  }

  /**
   *  Handle a socket 'error' event.
   *  @method _handleError
   *  @param  reject    The rejector for the ready promise {Function};
   *  @param  err       The error {Error};
   *
   *  @return void
   *  @protected
   */
  _handleError( reject, err ) {
    const src   = this._kbSource;

    if (src.config.verbosity > 0) {
      console.error('*** %s: TCP server.error:',
                    this.constructor.name, err);
    }

    if (! this.listening && ! this._resolved) {
      this._resolved = true;  // Mark the promise "resolved"

      if (src.config.verbosity > 0) {
        console.error('*** %s: initialization failed: %s',
                      this.constructor.name, err);
      }

      reject( err );

    } else {
      src.emit('error', err);
    }
  }

  /**
   *  Handle a 'close' event
   *  @method _handleClose
   *
   *  @return void
   *  @protected
   */
  _handleClose() {
    const src = this._kbSource;

    if (src.config.verbosity > 1) {
      console.error('=== %s: TCP server.close', this.constructor.name);
    }
  }

  /**
   *  Handle a new, incoming connection.
   *  @method _handleConnect
   *  @param  sock    The socket for the new, incoming connection {Socket};
   *
   *  @return void
   *  @protected
   */
  _handleConnect( sock ) {
    const src     = this._kbSource;
    const remote  = sock.address();

    // Add this socket to our list and assign an id.
    this.socks.push( sock );
    sock.__id = this.socks.length;

    if (remote.address === '127.0.0.1' || remote.address === '::1') {
      remote.hostnames = [ _hostname(), 'localhost' ];
      this._handleClient( sock, remote );

    } else {
      let resolved  = [];

      this._dnsRev.resolve( remote.address )
        .then( hostnames => { resolved = hostnames } )
        .catch( err => {
          console.warn('*** %s: resolve of [ %s ] failed:',
                      this.constructor.name, remote.address, err);
        })
        .finally( () => {
          remote.hostnames = ( Array.isArray(resolved) && resolved.length > 0
                                ? resolved
                                : [ remote.address ] );

          this._handleClient( sock, remote );
        });
    }
  }

  /**
   *  Once the remote client has been identified, handle the client.
   *  @method _handleClient
   *  @param  sock    The client connection {Socket};
   *  @param  remote  The remote address information {Object};
   *
   *  `remote` includes:
   *    address   {String};
   *    family    {String} (IPv4, IPv6);
   *    port      {Number};
   *    size      {Number};
   *    hostnames {Array};
   *
   *  @return void
   *  @protected
   */
  _handleClient( sock, remote ) {
    const src       = this._kbSource;
    const hostname  = (Array.isArray(remote.hostnames) &&
                       remote.hostnames.length > 0
                        ? remote.hostnames[0]
                        : remote.address);

    if (src.config.verbosity >= 0) {
      console.error('=== %s: %s client connected: %s [ %s ]: %d clients',
                    this.constructor.name, remote.family, hostname,
                    remote.address, this.socks.length);
    }

    // Pass along useful meta-data
    sock._remote   = remote;
    sock._hostname = hostname;

    // Include a partial buffer array
    sock._parts    = [];

    sock
      .on('error',  err   => this._handleSocketError( sock, err ))
      .on('data',   data  => this._handleSocketData( sock, data ))
      .on('end',    ()    => this._handleSocketEnd( sock ));

    if (this.paused) {
      // Pause this new client (xoff)
      this._pause( sock );
    }
  }

  /**
   *  Handle an error on a particular socket.
   *  @method _handleSocketError
   *  @param  sock      The target socket {Socket};
   *  @param  err       The error {Error};
   *
   *  @return void
   *  @protected
   */
  _handleSocketError( sock, err ) {
    const src       = this._kbSource;
    const remote    = sock._remote;
    const hostname  = sock._hostname;

    if (src.config.verbosity > 0) {
      console.error('*** %s: %s %s client.error:',
                    this.constructor.name, remote.family, hostname,
                    err);
    }

    src.emit( 'error', err );
  }

  /**
   *  Handle incoming data on a particular socket.
   *  @method _handleSocketData
   *  @param  sock    The source socket {Socket};
   *  @param  data    The incoming data {Buffer};
   *
   *  @return void
   *  @protected
   */
  _handleSocketData( sock, data ) {
    const src       = this._kbSource;
    const remote    = sock._remote;
    const hostname  = sock._hostname;
    const parts     = sock._parts;

    /* If `data` is not new-line terminated, it is not a full buffer and so
     * we will need to wait for the rest before processing it as a message.
     *
     *  assert( Buffer.isBuffer(data) )
     */
    parts.push( data );

    if (data[ data.length-1 ] != 0x0a) { // '\n'
      if (src.config.verbosity > 2) {
        console.error('=== %s: %s %s %d byte partial msg -- %d buffered',
                      this.constructor.name, remote.family, hostname,
                      data.length, parts.length );
      }

      return;
    }

    let fullMsg = Buffer.concat( parts );

    if (src.config.verbosity > 2) {
      console.error('=== %s: %s %s %d byte full msg from %d parts',
                    this.constructor.name, remote.family, hostname,
                    fullMsg.length, parts.length );
      /*
      for (let idex = 0; idex < parts.length; idex++) {
        console.error('===   %d: %s', idex, parts[idex]);
      }
      // */
    }

    // Empty our partials
    parts.length = 0;

    // Process this full message
    _handleMessage( src, fullMsg, remote );
  }

  /**
   *  Handle a socket end.
   *  @method _handleSocketEnd
   *  @param  sock      The client connection {Socket};
   *
   *  @return void
   *  @protected
   */
  _handleSocketEnd( sock ) {
    const src       = this._kbSource;
    const remote    = sock._remote;
    const hostname  = sock._hostname;

    if (src.config.verbosity >= 0) {
      console.error('=== %s: %s %s client disconnected',
                    this.constructor.name, remote.family, hostname);
    }

    // Remove this socket from our list
    const idex = this.socks.indexOf( sock );
    if (idex >= 0) {
      this.socks.splice( idex, 1 );
    } else {
      console.warn('*** %s: %s cannot find socket %d for %s [ %s ]',
                    this.constructor.name, remote.family,
                    sock.__id,
                    hostname, remote.address);
    }
  }

  /**
   *  Send an pause/xoff to the given socket
   *  @method _pause
   *  @param  sock
   *
   *  @protected
   */
  _pause( sock ) {
    sock.write( this.xoff );

    // :XXX: Should we also do sock.pause()?
  }

  /**
   *  Send an resume/xon to the given socket
   *  @method _resume
   *  @param  sock
   *
   *  @protected
   */
  _resume( sock ) {
    // :XXX: Should we also do sock.resume()?

    sock.write( this.xon );
  }
  /* Protected methods }
   ***************************************************************************/
}


/*****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Initialize the KernelBeat source.
 *  @method _initSource
 *  @param  self  The controlling instance {KernelBeatSource};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initSource( self ) {

  /* Check what type of source this should be:
   *  - file-based : empty `hostname`, empty `port`, non-empty `pathname`
   *  - tcp        : default
   */
  if (self.config.url) {
    if (!self.config.url.host &&
        !self.config.url.port &&
        self.config.url.pathname) {
      // This *appears* to be a file-based source
      return _initFileSource( self );
    }
  }

  // Treat this as a TCP-based source
  return _initTcpServer( self );
}

/**
 *  Retrieve the hostname.
 *  @method _hostname
 *
 *  If this is running within a k8s pod that has injected host-based
 *  information, we can pull the host name of the containing node instead of
 *  the name of this pod.
 *
 *  :NOTE: This requires the deployment/daemonset to use the k8s DownwardAPI to
 *         inject this information, for example:
 *          env:
 *            - name: K8S_NODE_NAME
 *              valueFrom:
 *                fieldRef:
 *                  fieldPath: spec.nodeName
 *
 *  If this information is not avaialble, simply return the OS-reported
 *  hostname.
 *
 *  @return The host name {String};
 *  @private
 */
function _hostname() {
  return process.env['K8S_NODE_NAME'] || Os.hostname();
}

/****************************************************************************
 * File-based source {
 *
 */

/**
 *  Initialize a KernelBeat file-based source.
 *  @method _initFileSource
 *  @param  self  The controlling instance {KernelBeatSource};
 *
 *  The file-based source is determined via `self.config.url.pathname`;
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initFileSource( self ) {
  self.server = new KernelBeatSource_dev( self );

  return self.server.ready;
}

/**
 *  Perform a sysctl call.
 *  @method _sysctl
 *  @param  args    An array of arguments {Array};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _sysctl( args ) {
  return new Promise((resolve, reject) => {
    Exec('sysctl', args, (error, stdout, stderr) => {
      if (error)  { reject(error) }
      if (stderr) { reject(stderr) }

      resolve(stdout.trim())
    })
  });
}

/* File-based source }
 ****************************************************************************
 * TCP-based source {
 *
 */

/**
 *  Initialize the KernelBeat socket/server.
 *  @method _initTcpServer
 *  @param  self  The controlling instance {KernelBeatSource};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initTcpServer( self ) {
  self.server = new KernelBeatSource_tcp( self );

  return self.server.ready;
}

/* TCP-based source }
 ****************************************************************************/

/**
 *  Handle an incoming KernelBeat message.
 *  @method _handleMessage
 *  @param  self    The controlling instance {KernelBeatSource};
 *  @param  msg     The incoming message {Buffer};
 *  @param  remote  The remote address information {Object};
 *
 *  `remote` includes:
 *    address   {String};
 *    family    {String} (IPv4, IPv6, file);
 *    [port]    {Number};
 *    [size]    {Number};
 *    hostnames {Array};
 *
 *  @return void
 *  @private
 */
function _handleMessage( self, msg, remote ) {
  const hostname  = (Array.isArray(remote.hostnames) &&
                     remote.hostnames.length > 0
                      ? remote.hostnames[0]
                      : remote.address);
  let   json;
  let   sample;

  if (self.config.verbosity > 2) {
    console.error('=== %s:%s msg: %s', self.constructor.name, hostname, msg);
  }

  try {
    json = JSON.parse( msg );

  } catch(ex) {
    //const bytes   = msg.map( by => by.toString(10) );

    console.error('*** %s:%s Invalid JSON [ %s ] in %d byte msg: %s',
                  self.constructor.name, hostname,
                  Safe.esc( ex.message ),
                  msg.length,
                  Safe.esc( msg, 'forJson' ).toString());
                  //bytes.join(','));

    self.emit('error', ex );
    return;
  }

  let timestamp = json.timestamp;
  if (timestamp != null) {
    /* Any timestamp from KernelBeat is in seconds since the epoch while a
     * sample timestamp must be milliseconds since the epoch.
     */
    timestamp *= 1000;

    if (json.process_terminated != null) {
      /* Since process_terminated is an asynchronous metric that will include
       * process-related cgroups, mixin a millisecond measure to the timestamp
       * to help differentiate these cgroup measurements from any before/after.
       */
      timestamp += (Date.now() % 1000);
    }

  } else {
    timestamp = Date.now();
  }

  // Ensure that we report an *increasing* timestamp value
  if (timestamp <= self._lastTs) {
    timestamp = self._lastTs + 1;
  }
  self._lastTs = timestamp;

  // Process the incoming KernelBeat report object
  const config  = {
    timestamp : timestamp,
    labels    : Object.assign({}, self.config.labels || {}, {
      node  : hostname,
      unit  : null,
    }),
  };

  Object.entries( json ).forEach( ([key,val]) => {

    switch( key ) {
    case 'cgroup_v1':         _handleCgroups(  self, config, val ); break;
    case 'cpu':               _handleCpu(      self, config, val ); break;
    case 'filesystems':       _handleFs(       self, config, val ); break;
    case 'fsstats':           _handleFsStats(  self, config, val ); break;
    case 'load':              _handleLoad(     self, config, val ); break;
    case 'mem':               _handleMem(      self, config, val ); break;
    case 'networks':          _handleNetworks( self, config, val ); break;
    case 'processes':         _handleProcesses(self, config, val ); break;
    case 'process_summary':   _handleProcSum(  self, config, val ); break;
    case 'process_terminated':_handleProcTerm( self, config, val ); break;
    case 'uptime':            _handleUptime(   self, config, val ); break;

    case 'status':            _handleStatus(   self, config, val ); break;

    default:
      if (! ['timestamp','beat'].includes(key)) {
        console.warn('*** %s: unexpected key [ %s ], val:',
                      self.constructor.name, key, val);
      }
    }
  });
}

/**
 *  Handle a KernelBeat 'status'.
 *  @method _handleStatus
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  val               The status value {String};
 *
 *  @return void
 *  @private
 */
function _handleStatus( self, config, val ) {
  console.error('>>> %s: KernelBeat Status: %s',
                  self.constructor.name, val);
}

/**
 *  Handle a KernelBeat 'cpu' report element.
 *  @method _handleCpu
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    user      : u64,  // CPU time spent in user space;
 *    nice      : u64,  // CPU time spent on low-priority processes;
 *    sys       : u64,  // CPU time spent in kernel space;
 *    idle      : u64,  // CPU time spent idle;
 *    iowait    : u64,  // CPU time spent waiting on disk access;
 *    irq       : u64,  // CPU time spent servicing hardware interrupts;
 *    softIrq   : u64,  // CPU time spent servicing software interrupts;
 *    steal     : u64,  // CPU time spent in involuntary wait by the virtual
 *                      // CPU while the hypervisor was servicing another
 *                      // processor;
 *    guest     : u64,  // CPU time spent by a virtual CPU in a hypervisor;
 *    guestNice : u64   // CPU time spent by a virtual CPU in a hypervisor on
 *                      // low-priority processes;
 *
 *  This will generate metric samples with names of the form:
 *    `os_cpu_%report-key%` (e.g. 'os_cpu_user', 'os_cpu_nice');
 *
 *  @return void
 *  @private
 */
function _handleCpu( self, config, report ) {
  Object.entries( report ).forEach( ([key, value]) => {
    // Create a sample from this entry
    const entry   = {
      name        : `os_cpu_${Utils.camelCase2_str(key)}`,
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit: 'seconds'}),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );

    /* :XXX:  For all handlers, we could also emit a 'metric' event with a
     *        generated help and type information.
     *        For example:
     *          help: `KernelBeat CPU time spent (${key})`
     *          type: 'GAUGE'
     */
  });
}

/**
 *  Handle a KernelBeat 'filesystems' report element.
 *  @method _handleFs
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be an array of filesystem entries each of the form:
 *    id         : u32,    // Unique filesystem id (superblock device id);
 *    type       : "str",  // Filesystem type;
 *    filesTotal : u64,    // Total file count;
 *    filesFree  : u64,    // Available file count;
 *    sizeTotal  : u64,    // Total size (bytes);
 *    sizeFree   : u64,    // Free size (bytes);
 *    sizeAvail  : u64,    // Free size (bytes) available to unprivileged users;
 *    devName    : "str",  // The absolute path of the mounted device;
 *    mountpoint : "str"   // The absolute path where the filesystem is mounted;
 *
 *  The string-based values (e.g. type, devName, mountpoint) in addition to
 *  'id' will be used as labels:
 *    id          => id
 *    type        => type
 *    devName     => dev_name
 *    mountpoint  => mount_point
 *
 *  The remainder of the entries will result in samples with names of the form:
 *    `os_fs_volume_%report-key%` (e.g. 'os_fs_volume_files_total',
 *                                      'os_fs_volume_files_free');
 *
 *  @return void
 *  @private
 */
function _handleFs( self, config, report ) {
  report.forEach( entry => {
    // Create labels from the string properties

    const labels  = Object.assign({}, config.labels, {
      id          : entry.id,
      type        : entry.type,
      dev_name    : entry.devName,
      mount_point : entry.mountpoint,
    });

    // Create samples from each non-string value
    Object.entries( entry ).forEach( ([key,value]) => {
      if (key === 'id' || typeof(value) === 'string') { return }

      const entry   = {
        name        : `os_fs_volume_${Utils.camelCase2_str(key)}`,
        metric_class: 'filesystem',
        timestamp   : config.timestamp,
        labels      : labels,
        value       : String( value ),
      };

      // Set the 'unit' label
      if (key.startsWith('files')) {
        entry.labels.unit = 'files';
      } else {
        entry.labels.unit = 'bytes';
      }

      const sample  = new Sample( entry );

      self.emit( 'sample', sample );
    });
  });
}

/**
 *  Handle a KernelBeat 'fsstats' report element.
 *  @method _handleFsStats
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    count       : u32,  // The number of mounted filesystems;
 *    filesTotal  : u64,  // Total file count for all filesystem;
 *    filesFree   : u64,  // Available file count;
 *    sizeTotal   : u64,  // Total size (bytes);
 *    sizeFree    : u64,  // Free size (bytes);
 *    sizeAvail   : u64   // Free size (bytes) available to unprivileged users;
 *
 *  This will generate metric samples with names of the form:
 *    `os_fs_%report-key%` (e.g. 'os_fs_count', 'os_fs_files_total');
 *
 *  @return void
 *  @private
 */
function _handleFsStats( self, config, report ) {
  Object.entries( report ).forEach( ([key, value]) => {
    // Create a sample from this entry
    const entry   = {
      name        : `os_fs_${Utils.camelCase2_str(key)}`,
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit: 'bytes'}),
      value       : String( value ),
    };

    // Adjust the 'unit' label
    if (key === 'count') {
      entry.labels.unit = 'filesystems';

    } else if (key.startsWith('files')) {
      entry.labels.unit = 'files';

    }

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  });
}

/**
 *  Handle a KernelBeat 'load' report element.
 *  @method _handleLoad
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    one     : u32,  // Load average (%) for the last 1 minute;
 *    five    : u32,  // Load average (%) for the last 5 minutes;
 *    fifteen : u32,  // Load average (%) for the last 15 minutes;
 *    cores   : u32,  // The number of CPU cores;
 *
 *  This will generate metric samples with the following names:
 *    `load_pct_1m` : one     minute load average/bucket;
 *    `load_pct_5m` : five    minute load average/bucket;
 *    `load_pct_15m`: fifteen minute load average/bucket;
 *    `load_cores`  : The number of cpu cores;
 *
 *  @return void
 *  @private
 */
function _handleLoad( self, config, report ) {
  Object.entries( report ).forEach( ([key, value]) => {
    // Create a sample from this entry
    const entry   = {
      name        : `os_load_${key}`,
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {
        unit: (key === 'cores' ? 'cores' : 'percent'),
      }),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  });
}

/**
 *  Handle a KernelBeat 'mem' report element.
 *  @method _handleMem
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    total              : u64,  // Total memory (bytes);
 *    free               : u64,  // Total free (bytes);
 *    available          : u64,  // The available memory (bytes);
 *    used               : u64,  // The used memory (bytes)
 *                               //   [total - free];
 *    actualUsed         : u64,  // Actual used memory (bytes)
 *                               //   [total - available];
 *    buffer             : u64,  // Memory used as buffers (bytes);
 *    shared             : u64,  // Shared memory (bytes);
 *    cached             : u64,  // Memory used as cache {bytes);
 *    swapTotal          : u64,  // Total swap memory (bytes);
 *    swapFree           : u64,  // Free swap memory (bytes);
 *    swapUsed           : u64,  // Used swap memory (bytes)
 *                               //   [swapTotal - swapFree];
 *    swapCached         : u64,  // Cached swap memory (bytes);
 *    highTotal          : u64,  // Total high memory (bytes);
 *    highFree           : u64,  // Free high memory (bytes);
 *    lowTotal           : u64,  // Total low memory (bytes);
 *    lowFree            : u64,  // Free low memory (bytes);
 *    commitLimit        : u64,  // Virtual memory commit limit (bytes);
 *    commited           : u64,  // Committed virtual memory (bytes);
 *    kernStack          : u64,  // The kernel stack size (bytes);
 *    vmallocTotal       : u64,  // Total virtual memory (bytes);
 *
 *    active             : u32,  // Active memory pages;
 *    inactive           : u32,  // Inactive memory pages;
 *    activeAnon         : u32,  // Active anonymous pages;
 *    inactiveAnon       : u32,  // Inactive anonymous pages;
 *    activeFile         : u32,  // Active file pages;
 *    inactiveFile       : u32,  // Inactive file pages;
 *    unevictable        : u32,  // Unevictable pages
 *                               // (e.g. RAMfs, locked shared memory, locked
 *                               //       virtual memory);
 *    mlock              : u32,  // Locked pages;
 *    dirty              : u32,  // Dirty writable pages;
 *    writeback          : u32,  // Pages under writeback;
 *    anonMapped         : u32,  // Anonymously mapped pages;
 *    nfsUnstable        : u32,  // NFS unstable pages;
 *
 *    slab               : u32,  // Pages in a slab;
 *    slabReclaimable    : u32,  // Reclaimable slabs;
 *    slabUnreclaimable  : u32,  // Unreclaimable slabs;
 *
 *    pageTables         : u32,  // Number of page tables;
 *    bounce             : u32,  // Pages for bounce buffers;
 *    writebackTmp       : u32,  // Temporary writeback pages;
 *
 *    unit               : u32,  // Page size (bytes);
 *
 *    hugePagesTotal     : u32,  // Total number of huge pages;
 *    hugePagesFree      : u32,  // Free huge pages;
 *    hugePagesReserved  : u32,  // Reserved huge pages;
 *    hugePagesSurplus   : u32,  // Surplus huge pages;
 *    hugePageSize       : u32   // Huge page size (bytes);
 *
 *
 *  This will generate metric samples with names of the form:
 *    `os_mem_%report-key%` (e.g. 'os_mem_total', 'os_mem_free');
 *
 *  @return void
 *  @private
 */
function _handleMem( self, config, report ) {
  const keyMap = {
    'actualUsed':       {name:'used_actual',            unit:'bytes'},
    'kernStack':        {name:'stack',                  unit:'bytes'},
    'vmallocTotal':     {name:'vm_total',               unit:'bytes'},

    'unit':             {name:'page_size',              unit:'bytes'},
    'active':           {name:'page_active',            unit:'pages'},
    'inactive':         {name:'page_inactive',          unit:'pages'},
    'activeAnon':       {name:'page_active_anonymous',  unit:'pages'},
    'inactiveAnon':     {name:'page_inactive_anonymous',unit:'pages'},
    'activeFile':       {name:'page_file_active',       unit:'pages'},
    'inactiveFile':     {name:'page_file_inactive',     unit:'pages'},
    'unevictable':      {name:'page_unevictable',       unit:'pages'},
    'mlock':            {name:'page_mlock',             unit:'pages'},
    'dirty':            {name:'page_dirty',             unit:'pages'},
    'writeback':        {name:'page_writeback',         unit:'pages'},
    'anonMapped':       {name:'page_mapped_anonymous',  unit:'pages'},
    'nfsUnstable':      {name:'page_nfs_unstable',      unit:'pages'},
    'bounce':           {name:'page_bounce',            unit:'pages'},
    'writebackTmp':     {name:'page_writeback_tmp',     unit:'pages'},

    'slab':             {name:'slab_size',              unit:'pages'},
    'slabReclaimable':  {name:'slab_reclaimable',       unit:'slabs'},
    'slabUnreclaimable':{name:'slab_unreclaimable',     unit:'slabs'},

    'pageTables':       {name:'page_tables',            unit:'pagetables'},

    'hugePageSize':     {name:'page_huge_size',         unit:'bytes'},
    'hugePagesTotal':   {name:'page_huge_total',        unit:'hugepages'},
    'hugePagesFree':    {name:'page_huge_free',         unit:'hugepages'},
    'hugePagesReserved':{name:'page_huge_reserved',     unit:'hugepages'},
    'hugePagesSurplus': {name:'page_huge_surplus',      unit:'hugepages'},
  };

  Object.entries( report ).forEach( ([key, value]) => {
    // Create a sample from this entry
    let metric_key  = key;  //Utils.camelCase2_str(key);
    let unit        = 'bytes';

    // If there is an entry in `keyMap` for the current `key`, apply it
    if (key in keyMap) {
      let entry = keyMap[key];
      metric_key = entry.name;
      unit       = entry.unit;

    } else {
      // Otherwise, assure we have a _-separted key
      metric_key = Utils.camelCase2_str(key);
    }

    const entry   = {
      name        : `os_mem_${metric_key}`,
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit: unit}),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  });
}

/**
 *  Handle a KernelBeat 'networks' report element.
 *  @method _handleNetworks
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be an array of network entries each of the form:
 *    name          : "%name%", // Interface name (e.g. "lo");
 *    id            : u32,      // Interface id / index;
 *
 *    rxBytes       : u64,      // Received (bytes);
 *    rxPackets     : u64,      // Received (packets);
 *    rxErrors      : u64,      // Packet receive errors;
 *    rxErrDropped  : u64,      // - errors due to packet drop or missed;
 *    rxErrFifo     : u64,      // - errors due to FIFO overruns;
 *    rxErrFrame    : u64,      // - errors due to framing issues
 *                              //   (general, length, ring buffer overflow,
 *                              //    CRC errors, frame alignment);
 *    rxCompressed  : u64,      // Compressed packets received;
 *
 *    txBytes       : u64,      // Transmitted (bytes);
 *    txPackets     : u64,      // Transmitted (packets);
 *    txErrors      : u64,      // Packet transmit errors;
 *    txErrDropped  : u64,      // - errors due to packet drop (no space);
 *    txErrFifo     : u64,      // - errors due to FIFO overruns;
 *    txErrCarrier  : u64,      // - errors due to carrier issues
 *                              //   (general, aborted, heartbeat, window);
 *    txCompressed  : u64,      // Compressed packets transmitted;
 *
 *    multicast     : u64,      // Multicast packets received;
 *    collisions    : u64,      // Packet collisions;
 *
 *  The string-based values (e.g. name) in addition to 'id' will be used as
 *  labels:
 *    id    => netId
 *    name  => netIface
 *
 *  This will generate metric samples with names of the form:
 *    `os_net_iface_%report-key%` (e.g. 'os_net_iface_receive');
 *
 *  @return void
 *  @private
 */
function _handleNetworks( self, config, report ) {
  const keyMap = {
    'rxBytes':          {name:'receive',                unit:'bytes'},
    'rxPackets':        {name:'receive',                unit:'packets'},
    'rxCompressed':     {name:'receive_compressed',     unit:'packets'},

    'txBytes':          {name:'transmit',               unit:'bytes'},
    'txPackets':        {name:'transmit',               unit:'packets'},
    'txCompressed':     {name:'transmit_compressed',    unit:'packets'},

    'multicast':        {name:'receive_multicast',      unit:'packets'},
    'collisions':       {name:'collision',              unit:'packets'},

    'rxErrors':         {name:'receive_error',          unit:'count'},
    'rxErrDropped':     {name:'receive_error_dropped',  unit:'count'},
    'rxErrFifo':        {name:'receive_error_fifo',     unit:'count'},
    'rxErrFrame':       {name:'receive_error_frame',    unit:'count'},

    'txErrors':         {name:'transmit_error',         unit:'count'},
    'txErrDropped':     {name:'transmit_error_dropped', unit:'count'},
    'txErrFifo':        {name:'transmit_error_fifo',    unit:'count'},
    'txErrCarrier':     {name:'transmit_error_carrier', unit:'count'},
  };

  report.forEach( entry => {
    // Create labels from the string properties
    const labels  = Object.assign({}, config.labels, {
      id    : entry.id,
      name  : entry.name,
    });

    // Create samples from each non-string value
    Object.entries( entry ).forEach( ([key,value]) => {
      if (key === 'id' || typeof(value) === 'string') { return }

      let metric_key  = key;  //Utils.camelCase2_str(key);
      let unit        = 'bytes';

      // If there is an entry in `keyMap` for the current `key`, apply it
      if (key in keyMap) {
        let entry = keyMap[key];
        metric_key = entry.name;
        unit       = entry.unit;

      } else {
        // Otherwise, assure we have a _-separted key
        metric_key = Utils.camelCase2_str(key);
      }

      const entry   = {
        name        : `os_net_iface_${metric_key}`,
        metric_class: 'network',
        timestamp   : config.timestamp,
        labels      : Object.assign({}, labels, {unit: unit}),
        value       : String( value ),
      };

      // Adjust the 'unit' label based upon the key
      const sample  = new Sample( entry );

      self.emit( 'sample', sample );
    });
  });
}

/**
 *  Given a process-related metric key, return the appropriate unit label.
 *  @method _procUnit
 *  @param  entry   The process-related entry {Object};
 *  @param  key     The metric key/name {String};
 *
 *  @return The updated `entry` {Object};
 *  @private
 */
function _procUnit( key ) {
  let unit  = 'seconds';

  switch( key ) {
    case 'memRss':
    case 'memShare':
    case 'memSize':
      unit = 'bytes';
      break;

    case 'fdOpen':
    case 'fdLimitHard':
    case 'fdLimitSoft':
    case 'memFaultsMinor':
    case 'memFaultsMajor':
      unit = 'count';
      break;
  }

  return unit;
}

/**
 *  The common set of process labels.
 *  @method _commonProcLabels
 *  @param  entry   The process-related entry {Object};
 *
 *  @return The common labels {Object};
 *  @private
 */
function _commonProcLabels( entry ) {
    return {
      // Shared between _handleProcess() and _handleProcTerm()
      id            : entry.pid,
      name          : entry.name,
      state         : entry.state,
      parent_id     : entry.ppid,
      process_group : entry.pgrp,
      user_id       : entry.uid,
      cgroups       : (Array.isArray(entry.cgroups)
                        ? entry.cgroups.join(',')
                        : undefined),
      cmd_line      : entry.cmdLine,

      unit          : 'seconds',
    };
}

/**
 *  Handle a KernelBeat 'processes' report element.
 *  @method _handleProcesses
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be an array of process entries each of the form:
 *    name            : "str",      // Process name;
 *    state           : "str",      // Process state:
 *                                  //   R (running)
 *                                  //   S (sleeping)
 *                                  //   D (disk sleep)
 *                                  //   T (stopped)
 *                                  //   t (tracing stop)
 *                                  //   X (dead)
 *                                  //   Z (zombie)
 *                                  //   x (dead)
 *                                  //   K (wake kill)
 *                                  //   W (waking)
 *                                  //   P (parked)
 *                                  //   N (noload)
 *                                  //   n (new)
 *                                  //
 *    pid             : s32,        // Process id;
 *    ppid            : s32,        // Parent process id;
 *    pgrp            : s32,        // Parent group id;
 *    uid             : u32,        // Owning user id;
 *    cgroups         : [ ... ],    // Array of "%cgroup/subsystem-id%"
 *                                  // recording the set of cgroups to which
 *                                  // this process belongs. These ids
 *                                  // reference entries in the most recent,
 *                                  // top-level 'cgroup_v1' set.
 *
 *    cpuStart        : u64,        // CPU start time (seconds since epoch);
 *    cpuUser         : u32,        // CPU time spent in user space;
 *    cpuSys          : u32,        // CPU time spent in kernel space;
 *
 *    fdOpen          : u32,        // The number of currently opened files;
 *    fdLimitHard     : u32,        // Hard limit number of open files;
 *    fdLimitSoft     : u32,        // Soft limit number of open files;
 *
 *    memRss          : u64,        // Memory allocated and in RAM (bytes)
 *                                  // Resident Set Size;
 *    memShare        : u64,        // Shared memory (bytes);
 *    memSize         : u64,        // Virtual memory size (bytes), all memory
 *                                  // accessible by this process including
 *                                  // allocated and in-use, allocated but not
 *                                  // in-use, swapped out, from shared
 *                                  // libraries;
 *
 *    memFaultsMinor  : u32,        // The number of minor memory faults
 *                                  // (no disk access);
 *    memFaultsMajor  : u32,        // The number of major memory faults
 *                                  // requiring disk access;
 *
 *
 *    cmdLine         : "str",      // The full process command line including
 *                                  // all arguments
 *                                  // (limited to 512 characters);
 *
 *  Several report values will be used as labels:
 *    name      => procName
 *    state     => procState
 *    pid       => procId
 *    ppid      => procParent
 *    pgrp      => procGroup
 *    uid       => procUid
 *    cgroups   => procCgroups with a CSV list of cgroup ids
 *    cmdLine   => procCmdline
 *
 *  This will generate metric samples with names of the form:
 *    `os_proc_process_%report-key%` (e.g. 'os_proc_process_cpu_start',
 *                                         'os_proc_process_cpu_user',
 *                                         'os_proc_process_fd_open');
 *
 *  @return void
 *  @private
 */
function _handleProcesses( self, config, report ) {
  const excludeKeys = [
    'pid', 'name', 'state', 'ppid', 'pgrp', 'uid', 'cgroups', 'cmdLine',
  ];

  report.forEach( entry => {
    // Create labels from the string properties
    const labels  = Object.assign({}, config.labels,
                                      _commonProcLabels( entry ) );

    // Create samples from each key that does not appear in `excludeKeys`
    Object.entries( entry ).forEach( ([key,value]) => {
      if (excludeKeys.includes(key))  { return }

      // Adjust the 'unit' label based upon the key
      labels.unit = _procUnit( key );

      const entry   = {
        name        : `os_proc_process_${Utils.camelCase2_str(key)}`,
        metric_class: 'process',
        timestamp   : config.timestamp,
        labels      : labels,
        value       : String( value ),
      };

      const sample  = new Sample( entry );

      self.emit( 'sample', sample );
    });
  });
}

/**
 *  Handle a KernelBeat 'process_summary' report element.
 *  @method _handleProcSum
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    total     : u32,    // Total number of processes;
 *    sleeping  : u32,    // Sleeping processes (S);
 *    running   : u32,    // Running processes (R);
 *    idle      : u32,    // Idle processes (D);
 *    stopped   : u32,    // Stopped processed (T, t);
 *    zombie    : u32,    // Zombie processes (Z);
 *    dead      : u32,    // Dead processes (X);
 *    unknown   : u32,    // All others (x, K, W, P, N, n);
 *
 *  This will generate metric samples with names of the form:
 *    `os_proc_%report-key%` (e.g. 'os_proc_total',
 *                                 'os_proc_sleeping');
 *
 *  @return void
 *  @private
 */
function _handleProcSum( self, config, report ) {
  Object.entries( report ).forEach( ([key, value]) => {
    // Create a sample from this entry
    const entry   = {
      name        : `os_proc_${key}`,
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit: 'count'}),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  });
}

/**
 *  Handle a KernelBeat 'process_terminated' report element.
 *  @method _handleProcTerm
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    name            : "str",      // Process name;
 *    state           : "str",      // Process state:
 *                                  //   R (running)
 *                                  //   S (sleeping)
 *                                  //   D (disk sleep)
 *                                  //   T (stopped)
 *                                  //   t (tracing stop)
 *                                  //   X (dead)
 *                                  //   Z (zombie)
 *                                  //   x (dead)
 *                                  //   K (wake kill)
 *                                  //   W (waking)
 *                                  //   P (parked)
 *                                  //   N (noload)
 *                                  //   n (new)
 *                                  //
 *    pid             : s32,        // Process id;
 *    ppid            : s32,        // Parent process id;
 *    pgrp            : s32,        // Parent group id;
 *    uid             : u32,        // Owning user id;
 *    cgroups         : [ ... ],    // Array of "%cgroup/subsystem-id%"
 *                                  // recording the set of cgroups to which
 *                                  // this process belongs. These ids
 *                                  // reference entries in the most recent,
 *                                  // top-level 'cgroup_v1' set.
 *
 *    cpuStart        : u64,        // CPU start time (seconds since epoch);
 *    cpuUser         : u32,        // CPU time spent in user space;
 *    cpuSys          : u32,        // CPU time spent in kernel space;
 *
 *    fdOpen          : u32,        // The number of currently opened files;
 *    fdLimitHard     : u32,        // Hard limit number of open files;
 *    fdLimitSoft     : u32,        // Soft limit number of open files;
 *
 *    memRss          : u64,        // Memory allocated and in RAM (bytes)
 *                                  // Resident Set Size;
 *    memShare        : u64,        // Shared memory (bytes);
 *    memSize         : u64,        // Virtual memory size (bytes), all memory
 *                                  // accessible by this process including
 *                                  // allocated and in-use, allocated but not
 *                                  // in-use, swapped out, from shared
 *                                  // libraries;
 *
 *    memFaultsMinor  : u32,        // The number of minor memory faults
 *                                  // (no disk access);
 *    memFaultsMajor  : u32,        // The number of major memory faults
 *                                  // requiring disk access;
 *
 *
 *    cmdLine         : "str",      // The full process command line including
 *                                  // all arguments
 *                                  // (limited to 512 characters);
 *
 *    exit            : s64 | s32,  // The exit code;
 *
 *    //=====================================================================
 *    // The following is include iff process termination was due to an
 *    // exception/signal
 *    exception       : "str",      // An indication of the exception;
 *    error           : s64 | s32,  // An error code;
 *    trapNum         : s32,        // Any trap number;
 *    sigNum          : s32,        // Any signal number;
 *
 *    // Currently, only x86 is supported;
 *    regIp           : u64 | u32,  // The instruction pointer (register);
 *    regSp           : u64 | u32,  // The stack pointer       (register);
 *
 *    // The following SHOULD BE architecture dependant but is currently limited
 *    // to x86;
 *    regFlags        : u64 | u32,  // The register flags;
 *    regAx           : u64 | u32,  // The AX register;
 *    regAxOrig       : u64 | u32,  // The original AX register;
 *    regBp           : u64 | u32,  // The BP register;
 *    regCs           : u64 | u32,  // The CS register;
 *    regSs           : u64 | u32,  // The SS register;
 *    regCx           : u64 | u32,  // The CX register;
 *    regDx           : u64 | u32,  // The DX register;
 *    regDi           : u64 | u32,  // The DI register;
 *    regSi           : u64 | u32   // The SI register;
 *
 *  Several report values will be used as labels:
 *    name      => procName
 *    state     => procState
 *    pid       => procId
 *    ppid      => procParent
 *    pgrp      => procGroup
 *    uid       => procUid
 *    cgroups   => cgroups with a CSV list of cgroup ids
 *    cmdLine   => procCmdline
 *
 *    error     => procErrorCode
 *    exit      => procExitCode
 *    exception => procException
 *    trapNum   => procTrapNum
 *    sigNum    => procSigNum
 *
 *  All `reg*` report values will be excluded;
 *
 *  This will generate metric samples with names of the form:
 *    `os_proc_process_%report-key%` (e.g. 'os_proc_process_cpu_start',
 *                                         'os_proc_process_cpu_user',
 *                                         'os_proc_process_fd_open');
 *
 *  @return void
 *  @private
 */
function  _handleProcTerm( self, config, report ) {
  const excludeKeys   = [
    'pid', 'name', 'state', 'ppid', 'pgrp', 'uid', 'cgroups', 'cmdLine',
    'error', 'exit', 'exception', 'trapNum', 'sigNum',
  ];
  const excludeRegRe  = /^reg[A-Z][a-zA-Z]+$/;

  // Create labels from the string properties
  const labels  = Object.assign({}, config.labels,
                                    _commonProcLabels( report ) );

  // Exlusive to _handleProcTerm()
  if (report.error     != null) { labels.error     = report.error }
  if (report.exit      != null) { labels.exit      = report.exit }
  if (report.exception != null) { labels.exception = report.exception }
  if (report.trapNum   != null) { labels.trap_num  = report.trapNum }
  if (report.sigNum    != null) { labels.sig_num   = report.sigNum }

  // Create samples from each key that does not appear in `excludeKeys`
  Object.entries( report ).forEach( ([key,value]) => {
    if (excludeKeys.includes(key) || excludeRegRe.test(key))  { return }

    // Adjust the 'unit' label based upon the key
    labels.unit = _procUnit( key );

    const entry   = {
      name        : `os_proc_process_${Utils.camelCase2_str(key)}`,
      metric_class: 'process',
      timestamp   : config.timestamp,
      labels      : labels,
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  });
}

/**
 *  Handle a KernelBeat 'uptime' report element.
 *  @method _handleUptime
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should have the form:
 *    seconds : u32   // Uptime (seconds)
 *
 *  This will generate metric samples with name:
 *    `os_uptime`
 *
 *  @return void
 *  @private
 */
function _handleUptime    ( self, config, report ) {
  if (typeof(report.seconds) === 'number') {
    // Create a sample from this entry
    const entry   = {
      name        : 'os_uptime',
      metric_class: 'system',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit: 'seconds'}),
      value       : String( report.seconds ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  }
}

/**
 *  Handle a KernelBeat 'cgroup_v1' report element.
 *  @method _handleCgroups
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be an array of cgroup/subsystem entries.
 *  Each should have at least:
 *    id    : {Number},   // cgroup / subsystem-id;
 *    hid   : {Number},   // cgroup hierarchy id;
 *    path  : {String},   // cgroup subsystem path (/sys/cgroup/%path%);
 *
 *  Further fields are based upon cgroup subsystem as identified via the first
 *  component of `path`:
 *    blkio/
 *    cpu/      | cpuacct/  | cpu,cpuacct/
 *    cpuset/
 *    freezer/
 *    memory/
 *    net_cls/  | net_prio/ | net_cls,net_prio/
 *    pids/
 *
 *  @return void
 *  @private
 */
function _handleCgroups( self, config, report ) {

  report.forEach( entry => {
    // Create a custom configuration for this cgroup entry
    const subsys        = entry.path.split('/')[0];
    const cgroupConfig  = Object.assign({}, config);

    cgroupConfig.labels = Object.assign({}, config.labels, {
        id  : entry.id,
        hid : entry.hid,
        path: entry.path,
    });

    switch( subsys ) {
      case 'blkio':
        _handleCgroup_blkio( self, cgroupConfig, entry );
        break;

      case 'cpu':
      case 'cpuacct':
      case 'cpu,cpuacct':
        _handleCgroup_cpu( self, cgroupConfig, entry );
        break;

      case 'cpuset':
        _handleCgroup_cpuset( self, cgroupConfig, entry );
        break;

      case 'freezer':
        _handleCgroup_freezer( self, cgroupConfig, entry );
        break;

      case 'memory':
        _handleCgroup_memory( self, cgroupConfig, entry );
        break;

      case 'net_cls':
      case 'net_prio':
      case 'net_cls,net_prio':
        _handleCgroup_net( self, cgroupConfig, entry );
        break;

      case 'pids':
        _handleCgroup_pids( self, cgroupConfig, entry );
        break;
    }
  });
}

/**
 *  Handle a KernelBeat 'cgroup_v1.blkio' report element.
 *  @method _handleCgroup_blkio
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be an object with top-level block io information and a
 *  `devices` array with per-block-device metrics:
 *    blkio.weight      : {Number}
 *    blkio.leaf_weight : {Number}
 *    devices           : {Array}
 *
 *    Each entry in `devices` should have the form:
 *      id                      : {Number};   // device unique id (MKDEV)
 *      name                    : {String};   // device name "%major%:%minor%"
 *      blkio.time              : {Number};
 *      blkio.time_recursive    : {Number};
 *      blkio.sectors           : {Number};
 *      blkio.sectors_recursive : {Number};
 *
 *    The remaining entries are io counters comprised of:
 *        read                  : {Number};
 *        write                 : {Number};
 *        sync                  : {Number};
 *        async                 : {Number};
 *        total                 : {Number};
 *
 *    These io counter entries include:
 *      blkio.io_service_bytes
 *      blkio.io_serviced
 *      blkio.io_service_time
 *      blkio.io_wait_time
 *      blkio.io_merged
 *      blkio.io_queued
 *      blkio.io_service_bytes_recursive
 *      blkio.io_serviced_recursive
 *      blkio.io_service_time_recursive
 *      blkio.io_wait_time_recursive
 *      blkio.io_merged_recursive
 *      blkio.io_queued_recursive
 *      blkio.throttle.io_service_bytes
 *      blkio.throttle.io_serviced
 *
 *  For devices, the string-based values (e.g. id, name) will be used as
 *  labels:
 *    id          => blkId
 *    name        => blkName
 *
 *  The remainder of the device entries will result in samples with names of the
 *  form:
 *    `os_cgroup_blkio_device_%report-key%`
 *
 *  @return void
 *  @private
 */
function _handleCgroup_blkio( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path', 'devices' ];
  const unitMap     = [
    { re: /time/,   unit: 'seconds' },
    { re: /sector/, unit: 'sectors' },
    { re: /byte/,   unit: 'bytes' },
    { default: 'count' },
  ];
  const blkLabels   = Object.assign({}, config.labels, {
    weight      : report['blkio.weight'],
    leaf_weight : report['blkio.leaf_weight'],
  });

  if (Array.isArray(report.devices) && report.devices.length > 0) {
    // Handle devices
    const excludeDevKeys = [ 'id', 'name' ];

    report.devices.forEach( dev => {
      const devLabels = Object.assign({}, blkLabels, {
        dev_id: dev.id,
        name  : dev.name,
      });

      Object.entries( dev ).forEach( ([devKey, devVal]) => {
        if (excludeDevKeys.includes(devKey))  { return }

        /* Strip 'blkio.' from the device key, convert '.' to '_', and
         * remove any redundant unit.
         */
        const devSubkey = devKey
                            .slice(6)
                            .replace(/\./g, '_')
                            .replace(/_(bytes|time)/g, '');

        /* Using the 'unitMap' determine which entry matches `devKey` and
         * use it to set the `devLabels.unit` value.
         */
        devLabels.unit = unitMap.reduce( (res,entry) => {
          if (res)                      { return res }
          if (entry.default)            { return entry.default }
          if (entry.re.test( devKey ))  { return entry.unit }
        }, null);

        if (typeof(devVal) !== 'object') {
          // Simple numeric value
          const entry   = {
            name        : `os_cgroup_blkio_device_${devSubkey}`,
            metric_class: 'cgroup',
            timestamp   : config.timestamp,
            labels      : devLabels,
            value       : String( devVal ),
          };

          const sample  = new Sample( entry );

          self.emit( 'sample', sample );

        } else {
          /* Treat this as an io counter object containing:
           *    read  : {Number};
           *    write : {Number};
           *    sync  : {Number};
           *    async : {Number};
           *    total : {Number};
           */
          Object.entries( devVal ).forEach( ([ioKey, ioVal]) => {
            const entry   = {
              name        : `os_cgroup_blkio_device_${ioKey}_${devSubkey}`,
              metric_class: 'cgroup',
              timestamp   : config.timestamp,
              labels      : devLabels,
              value       : String( ioVal ),
            };

            const sample  = new Sample( entry );

            self.emit( 'sample', sample );
          });
        }
      });
    });

  } else {
    // Output a single, top-level fact-of metric with weight labels
    const entry   = {
      name        : 'os_cgroup_blkio',
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : blkLabels,
      value       : 1,
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  }
}

/**
 *  Handle a KernelBeat 'cgroup_v1.cpu', 'cgroup_v1.cpuacct', or
 *  'cgroup_v1.cpu,cpuacct' report element.
 *  @method _handleCgroup_cpu
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    cpu.shares               : {Number}, unit: 'count';
 *    cpu.cfs_quota_us         : {Number}, unit: 'microseconds';
 *    cpu.cfs_period_us        : {Number}, unit: 'microseconds';
 *    cpu.rt_runtime_us        : {Number}, unit: 'microseconds';
 *    cpu.rt_period_us         : {Number}, unit: 'microseconds';
 *
 *    cpu.stat.throttled_time  : {Number}, unit: 'nanoseconds';
 *    cpu.stat.nr_throttled    : {Number}, unit: 'count';
 *    cpu.stat.nr_periods      : {Number}, unit: 'count';
 *
 *    cpuacct.usage            : {Number}, unit: 'nanoseconds';
 *    cpuacct.usage_user       : {Number}, unit: 'nanoseconds';
 *    cpuacct.usage_sys        : {Number}, unit: 'nanoseconds';
 *    cpuacct.usage_percpu     : {Array},  unit: 'nanoseconds', per CPU;
 *    cpuacct.usage_percpu_user: {Array},  unit: 'nanoseconds', per CPU;
 *    cpuacct.usage_percpu_sys : {Array},  unit: 'nanoseconds', per CPU;
 *
 *  @return void
 *  @private
 */
function _handleCgroup_cpu( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];
  const keyMap      = {
    'cpu.shares'               :{name:'cpu_shares',       unit:'count'},

    'cpu.cfs_quota_us'         :{name:'cpu_cfs_quota',    unit:'microseconds'},
    'cpu.cfs_period_us'        :{name:'cpu_cfs_period',   unit:'microseconds'},
    'cpu.rt_runtime_us'        :{name:'cpu_rt_runtime',   unit:'microseconds'},
    'cpu.rt_period_us'         :{name:'cpu_rt_period',    unit:'microseconds'},

    'cpu.stat.throttled_time'  :{name:'cpu_throttle',     unit:'nanoseconds'},
    'cpu.stat.nr_throttled'    :{name:'cpu_throttle',     unit:'count'},
    'cpu.stat.nr_periods'      :{name:'cpu_period',       unit:'count'},

    'cpuacct.usage'            :{name:'cpuacct_total',    unit:'nanoseconds'},
    'cpuacct.usage_user'       :{name:'cpuacct_user',     unit:'nanoseconds'},
    'cpuacct.usage_sys'        :{name:'cpuacct_sys',      unit:'nanoseconds'},

    'cpuacct.usage_percpu'     :{name:'cpuacct_total',    unit:'nanoseconds'},
    'cpuacct.usage_percpu_user':{name:'cpuacct_user',     unit:'nanoseconds'},
    'cpuacct.usage_percpu_sys' :{name:'cpuacct_sys',      unit:'nanoseconds'},
  };

  Object.entries( report ).forEach( ([key, value]) => {
    if (excludeKeys.includes(key))  { return }

    let metric_key  = key;  //Utils.camelCase2_str(key);
    let unit        = 'count';

    // If there is an entry in `keyMap` for the current `key`, apply it
    if (key in keyMap) {
      let entry = keyMap[key];
      metric_key = entry.name;
      unit       = entry.unit;

    } else {
      // Otherwise, assure we have a _-separted key
      metric_key = Utils.camelCase2_str(key);
    }

    // Create a sample from this entry
    const entry   = {
      name        : `os_cgroup_${metric_key}`,
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit:unit}),
    };

    if (Array.isArray(value)) {
      // A per-cpu array
      value.forEach( (cpuVal, cpuIdex) => {
        // Create an independent, cpu-specific entry
        const cpuEntry  = Object.assign( {}, entry );

        cpuEntry.labels.cpu_idex = cpuIdex;
        cpuEntry.value           = String( cpuVal );

        const sample  = new Sample( cpuEntry );

        self.emit( 'sample', sample );
      });

    } else {
      // Simple numeric value
      entry.value = String( value );

      const sample  = new Sample( entry );

      self.emit( 'sample', sample );
    }

  });
}

/**
 *  Handle a KernelBeat 'cgroup_v1.cpuset' report element.
 *  @method _handleCgroup_cpuset
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    cpuset.cpu_exclusive            : {Number}, unit: 'seconds';
 *    cpuset.mem_exclusive            : {Number}, unit: 'bytes';
 *    cpuset.mem_hardwall             : {Number}, unit: 'bytes';
 *    cpuset.memory_migrate           : {Number}, unit: 'rate'
 *    cpuset.memory_pressure          : {Number}, unit: 'rate'
 *    cpuset.memory_spread_page       : {Number}, unit: 'rate'
 *    cpuset.memory_spread_slab       : {Number}, unit: 'rate'
 *    cpuset.memory_pressure_enabled  : {Number}, unit: 'boolean';
 *    cpuset.sched_load_balance       : {Number}, unit: 'rate'
 *    cpuset.sched_relax_domain_level : {Number}, unit: 'rate'
 *
 *  @return void
 *  @private
 */
function _handleCgroup_cpuset( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];
  const unitMap     = [
    { re: /cpu_/,     unit: 'seconds' },
    { re: /mem_/,     unit: 'bytes' },
    { re: /_enable/,  unit: 'boolean' },
    { default: 'rate' },
  ];

  Object.entries( report ).forEach( ([key, value]) => {
    if (excludeKeys.includes(key))  { return }

    const metric_key  = key.slice(7); // `key` will be "cpuset.*"

    // Create a sample from this entry
    const entry   = {
      name        : `os_cgroup_cpuset_${metric_key}`,
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit:'rate'}),
      value       : String( value ),
    };

    /* Using the 'unitMap' determine which entry matches `key` and use
     * it to set the `label.unit` value.
     */
    entry.labels.unit = unitMap.reduce( (res,entry) => {
      if (res)                  { return res }
      if (entry.default)        { return entry.default }
      if (entry.re.test( key )) { return entry.unit }
    }, null);

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );

  });
}

/**
 *  Handle a KernelBeat 'cgroup_v1.freezer' report element.
 *  @method _handleCgroup_freezer
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    freezer.self_freezing           : {Number}, unit: 'processes';
 *    freezer.parent_freezing         : {Number}, unit: 'processes';
 *
 *  @return void
 *  @private
 */
function _handleCgroup_freezer( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];

  Object.entries( report ).forEach( ([key, value]) => {
    if (excludeKeys.includes(key))  { return }

    // `key` will be "freezer.%source%_freezing*"
    const source  = key.slice(8).split('_').shift();

    // Create a sample from this entry
    const entry   = {
      name        : 'os_cgroup_freezer_freeze',
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {
        source: source,
        unit  :'count',
      }),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );

  });
}

/**
 *  Handle a KernelBeat 'cgroup_v1.memory' report element.
 *  @method _handleCgroup_memory
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    memory.failcnt                    : {Number}, unit: 'count';
 *    memory.limit_in_bytes             : {Number}, unit: 'bytes';
 *    memory.max_usage_in_bytes         : {Number}, unit: 'bytes';
 *    memory.usage_in_bytes             : {Number}, unit: 'bytes';
 *    memory.soft_limit_in_bytes        : {Number}, unit: 'bytes';
 *    memory.move_charge_at_immigrate   : {Number}, unit: 'boolean';
 *    memory.use_hierarchy              : {Number}, unit: 'boolean';
 *    memory.swappiness                 : {Number}, unit: 'swappiness';
 *
 *    memory.memsw.limit_in_bytes       : {Number}, unit: 'bytes';
 *    memory.memsw.max_usage_in_bytes   : {Number}, unit: 'bytes';
 *
 *    memory.kmem.failcnt               : {Number}, unit: 'count';
 *    memory.kmem.limit_in_bytes        : {Number}, unit: 'bytes';
 *    memory.kmem.max_usage_in_bytes    : {Number}, unit: 'bytes';
 *    memory.kmem.usage_in_bytes        : {Number}, unit: 'bytes';
 *
 *    memory.kmem.tcp.failcnt           : {Number}, unit: 'count';
 *    memory.kmem.tcp.limit_in_bytes    : {Number}, unit: 'bytes';
 *    memory.kmem.tcp.max_usage_in_bytes: {Number}, unit: 'bytes';
 *    memory.kmem.tcp.usage_in_bytes    : {Number}, unit: 'bytes';
 *
 *  @return void
 *  @private
 */
function _handleCgroup_memory( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];
  const keyMap      = {
    'failcnt'                 :{name:'failures',      unit:'count'},
    'limit_in_bytes'          :{name:'limit',         unit:'bytes'},
    'soft_limit_in_bytes'     :{name:'limit_soft',    unit:'bytes'},
    'usage_in_bytes'          :{name:'usage',         unit:'bytes'},
    'max_usage_in_bytes'      :{name:'usage_max',     unit:'bytes'},
    'swappiness'              :{name:'swappiness',    unit:'swappiness'},

    'move_charge_at_immigrate':{name:'move_charge_at_immigrate',
                                                      unit:'boolean'},
    'use_hiearchy'            :{name:'use_hiearchy',  unit:'boolean'},
  };

  Object.entries( report ).forEach( ([key, value]) => {
    if (excludeKeys.includes(key))  { return }

    /* `key` will be "memory[.%mem_type%].%subKey%"
     *    mem_type:
     *      memsw (memory+swap)
     *      kmem
     *      kmem.tcp
     */
    const parts       = key.split('.');
    const mem_type    = (parts.length > 3
                          ? parts.slice( 1,3 ).join('.')
                          : (parts.length > 2
                              ? parts[1]
                              : 'global'));
    let   metric_key  = parts.pop();
    let   unit        = 'bytes';

    // If there is an entry in `keyMap` for the current `key`, apply it
    if (metric_key in keyMap) {
      let entry = keyMap[metric_key];
      metric_key = entry.name;
      unit       = entry.unit;

    } else {
      // Otherwise, assure we have a _-separted key
      metric_key = Utils.camelCase2_str(metric_key);
    }

    // Create a sample from this entry
    const entry   = {
      name        : `os_cgroup_memory_${metric_key}`,
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {
        mem_type: mem_type,
        unit    : unit,
      }),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );

  });
}

/**
 *  Handle a KernelBeat 'cgroup_v1.net_cls', 'cgroup_v1.net_prio', or
 *  'cgroup_v1.net_cls,net_prio' report element.
 *  @method _handleCgroup_net
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    net_cls.classid   : {Number}, unit: 'class';
 *    net_prio.prioidx  : {Number}, unit: 'index';
 *    interfaces        : {Array};
 *
 *    Each entry in `interfaces` should have the form:
 *      name            : {String};
 *      priority        : {Number}, unit: 'priority';
 *
 *  For devices, the string-based values (e.g. name) will be used as labels:
 *    %index% => netId
 *    name    => netIface
 *
 *  @return void
 *  @private
 */
function _handleCgroup_net( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];

  if (Array.isArray( report['interface'] )) {
    /* net_prio
     *
     * report should have the form:
     *  { 'net_prio.prioidx': {Number},
     *    'interfaces'      : [ ... ],
     *  }
     */
    report['interfaces'].forEach( (netVal, netIdex) => {
      const entry   = {
        name        : 'os_cgroup_net_prio_iface',
        metric_class: 'cgroup',
        timestamp   : config.timestamp,
        labels      : Object.assign({}, config.labels, {
          name  : netVal.name,
          index : netIdex,
          unit  : 'priority',
        }),
        value       : String( netVal.priority ),
      };

      const sample  = new Sample( entry );

      self.emit( 'sample', sample );
    });

  } else if ('net_cls.classid' in report) {
    /* net_cls
     *
     * report should have the form:
     *  { 'net_cls.classid': {Number} }
     */
    const entry   = {
      name        : 'os_cgroup_net_cls_id',
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {
        unit: 'class',
      }),
      value       : String( report['net_cls.classid'] ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );
  }
}

/**
 *  Handle a KernelBeat 'cgroup_v1.pids' report element.
 *  @method _handleCgroup_pids
 *  @param  self              The controlling instance {KernelBeatSource};
 *  @param  config            Report configuration {Object};
 *  @param  config.timestamp  The timestamp of this report {Number};
 *  @param  config.labels     Common labels to apply {Object};
 *  @param  report            The report element {Object};
 *
 *  `report` should be a relatively flat object with top-level entries
 *  identifying the subsystem and key. For example:
 *    pids.current  : {Number}, unit: 'processes';
 *    pids.max      : {Number}, unit: 'processes';
 *
 *  @return void
 *  @private
 */
function _handleCgroup_pids( self, config, report ) {
  const excludeKeys = [ 'id', 'hid', 'path' ];
  const keyMap      = {
    'pids.current'  : {name:'pids',     unit:'count'},
    'pids.max'      : {name:'pids_max', unit:'count'},
  };

  Object.entries( report ).forEach( ([key, value]) => {
    if (excludeKeys.includes(key))  { return }

    let metric_key  = key;  //Utils.camelCase2_str(key);
    let unit        = 'count';

    // If there is an entry in `keyMap` for the current `key`, apply it
    if (key in keyMap) {
      let entry = keyMap[key];
      metric_key = entry.name;
      unit       = entry.unit;

    } else {
      // Otherwise, assure we have a _-separted key
      metric_key = Utils.camelCase2_str(key);
    }

    // Convert any 'max' generated value to -1
    if (value === 'max')  { value = -1 }

    // Create a sample from this entry
    const entry   = {
      name        : `os_cgroup_${metric_key}`,
      metric_class: 'cgroup',
      timestamp   : config.timestamp,
      labels      : Object.assign({}, config.labels, {unit:unit}),
      value       : String( value ),
    };

    const sample  = new Sample( entry );

    self.emit( 'sample', sample );

  });
}

/* Private helpers }
 *****************************************************************************/

module.exports  = KernelBeatSource;
