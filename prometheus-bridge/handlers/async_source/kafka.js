const Utils         = require('../../lib/utils');
const Sample        = require('../../lib/sample');
const Safe          = require('../../lib/safe');
const AsyncSource   = require('./index.js');
const Kafka         = require('kafka-node');
const TimeoutError  = require('kafka-node/lib/errors/TimeoutError');

/**
 *  A Kafka topic monitor for asynchronous Prometheus-like metrics.
 *  @class  KafkaSource
 *  @extend AsyncSource
 *
 *  @emit   'sample', sample: emitted when an metric is made available where
 *                            `sample` is a Sample instance;
 *  @emit   'error', err    : emitted on client/consumer error;
 */
class KafkaSource extends AsyncSource {

  static get defaults() {
    return Object.assign({}, super.defaults, {
      topic             : 'metrics',  // kafka default: 'metrics'

      connect_timeout   : '10s',      // kafka default: '10s'
      idle_timeout      : '5m',       // kafka default: '5m'
      request_timeout   : '30s',      // kafka default: '30s'

      request_max_bytes : 1048576,    // 1 Mb

      consumer_group    : 'kafka-async-source',
                                      /* The groupId to use for the consumer
                                       * group.
                                       *
                                       * This should be unique for each pub/sub
                                       * group (e.g. set of k8s pods that load
                                       * balance handling of incoming metrics);
                                       */

      flow_control_topic: 'flow-control',
                                      /* The topic to use for kafka-based
                                       * flow-control messages.
                                       *
                                       * :NOTE: This MUST match the topic
                                       *        used by any RemoteKafka
                                       *        that might signal
                                       *        flow-control.
                                       */
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                              The configuration data
   *                                              {Object};
   *  @param  config.url                          The URL of the remote
   *                                              endpoint {URL};
   *  @param  [config.topic = 'metrics']          Kafka topic(s) {Array|String};
   *  @param  [config.connect_timeout = '10s']    Connection timeout (ms)
   *                                              {String|Number};
   *  @param  [config.idle_timeout = '5m']        Idle timeout (ms)
   *                                              {String|Number};
   *  @param  [config.request_timeout = '30s']    Request timeout (ms)
   *                                              {String|Number};
   *  @param  [config.request_max_bytes = 1048576]
   *                                              Maximum bytes per request
   *                                              {Number};
   *  @param  [config.dry_run = false]            If truthy, do NOT attempt to
   *                                              connect {Boolean};
   *  @param  [config.labels]                     Additional labels that should
   *                                              be applied to metrics from
   *                                              this source {Object};
   *  @param  [config.verbosity = 0]              Debug verbosity {Number};
   *
   *  Supported URL format:
   *    kafka://host:9092[/topic]
   *
   *  @note Multiple kafka hosts may be listed in a single URL IF they
   *        all share the same port AND topic(s).
   *            kafka://host1,host2,host3:9092/topic
   *              => host1:9092/topic
   *              => host2:9092/topic
   *              => host3:9092/topic
   *
   *  @note Multiple kafka topics may be specified as either an array or a
   *        comma-separated-list;
   */
  constructor( config ) {
    super( config );

    // assert( config.url.protocol === 'kafka:' );

    // Available at send() via kafkaClient.options.topic
    if (config.topic == null && config.url.pathname.length > 1) {
      config.topic = config.url.pathname.slice(1);
    }

    // Convert 'topic' to an array of topics.
    const topics  = (typeof(config.topic) === 'string'
                      ? config.topic.split(/\s*,\s*/)
                      : (Array.isArray(config.topic) ? config.topic : []));

    Object.defineProperties( this, {
      /**
       *  The "type" of this async source.
       *  @property type
       *  @readonly
       */
      type      : { value: 'kafka', enumerable: true },

      /**
       *  The set of topics to monitor.
       *  @property topics
       *  @readonly
       */
      topics    : {
        value     : topics,
        enumerable: true,
      },

      /**
       *  The Kafka consumer group instance used to monitor the metrics topics.
       *  @property consumer
       *  @protected
       */
      consumer  : { value: null,    writable: true },

      /**
       *  The Kafka producer instances used to generate flow control messages.
       *  @property fcProducer
       *  @protected
       */
      fcProducer: { value: null,    writable: true },

      /**
       *  A map of Kafka compression strings to the required topic attribute
       *  value.
       *  @property _compressionMap
       *  @readonly
       */
      _compressionMap: {
        value: {
          'none'  : 0,
          'gzip'  : 1,
          'snappy': 2,
        },
      },
    });

    if (this.topics.length < 1) {
      this._ready = Promise.reject( 'missing topic(s)' );
    }
  }

  /**
   *  Getter for the consumer-controlled client
   *  @prop client {KafkaClient}
   */
  get client() { return (this.consumer ? this.consumer.client : null) }

  /**
   *  Start this source.
   *  @method start
   *
   *  @return A promise resolved when this source is ready to generate metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = _initKafka( this );
    }

    return this._ready;
  }

  /**
   *  Pause this source.
   *  @method pause
   *
   *  Invoked when an 'xoff' event is received on the bridge event bus.
   *
   *  @return A promise resolved when this source has been paused {Promise};
   */
  async pause() {
    // Publish an 'xoff' on the kafka flow control topic
    return _publishFlowControl( this, 'xoff' );
  }

  /**
   *  Resume this source.
   *  @method resume
   *
   *  Invoked when an 'xon' event is received on the bridge event bus.
   *
   *  @return A promise resolved when this source has been resumed {Promise};
   */
  async resume() {
    // Publish an 'xon' on the kafka flow control topic
    return _publishFlowControl( this, 'xon' );
  }

  /**
   *  Stop this source.
   *  @method stop
   *
   *  @return A promise resolved when this source has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {

      this._stopping = new Promise( (resolve, reject) => {
        if (this.consumer == null) {
          // No consumer/connection
          return resolve( false );
        }

        /* Close the consumer, which should close the client along with the
         * flow-control producer.
         */
        this.consumer.close( () => {
          this.consumer = null;

          if (this.config.verbosity >= 0) {
            console.error('%s.stop(): consumer closed', this.constructor.name);
          }

          resolve( true );
        });
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this.ready = false;

        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }
}

/*****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Wait for Kafka to become available, create a consumer group, and register
 *  message handlers.
 *  @method _initKafka
 *  @param  self      The controlling instance {KafkaSource};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initKafka( self ) {
  return new Promise( (resolve, reject) => {

    // First, initialize the consumer
    let consumerRes;
    _initConsumer( self )
      .then( res => {
        // Once the consumer is ready we can create the flow-control producer
        consumerRes = res;

        return _initFlowProducer( self );
      })
      .then( flowRes => {
        // Both consumer and flow-producer are ready.
        const now = Date.now();

        if (self.config.verbosity >= 0) {
          console.error('=== %s: consumer ready[ %s ], '
                        +         'flow-control ready[ %s ]',
                        self.constructor.name,
                        consumerRes,
                        flowRes);
        }

        self.ready = now;
        resolve( self.ready );
      })
      .catch( err => {
        if (self.config.verbosity > 0) {
          console.error('*** %s: initialization failed: %s',
                        self.constructor.name, err);
        }

        if (! self.ready) { reject( err ) }

        self.emit( 'error', err );
      });
  });
}

/**
 *  Create a consumer group and register initial message handlers to determine
 *  readiness.
 *  @method _initConsumer
 *  @param  self        The controlling instance {KafkaSource};
 *
 *  @return A promise for results {Promise};
 *          - resolved with the timestamp on ready {Number};
 *          - rejected with error {Error};
 *  @private
 */
function _initConsumer( self ) {
  const url         = self.config.url;
  const topic       = self.config.topic;
  const topics      = self.topics;        // From `topic` in constructor
  const kafkaConfig = {
    // KafkaClient options {
    kafkaHost     : url.hostname.split(',')
                      .map( str => `${str}:${url.port}` )
                      .join(','),

    connectTimeout: self.config.connect_timeout,
    idleConnection: self.config.idle_timeout,
    requestTimeout: self.config.request_timeout,

    connectRetryOptions: { // node-retry
      //retries   : 5,
      //factor    : 2,

      minTimeout: self.config.connect_timeout / 2,  // 1s
      maxTimeout: self.config.connect_timeout,      // 60s

      //randomize : true,
    },
    // KafkaClient options }

    // ConsumerGroup options {
    fetchMaxBytes   : self.config.request_max_bytes,  // 1024 * 1024
    fetchMaxWaitMs  : self.config.request_timeout,    // 100

    groupId         : self.config.consumer_group,

    fromOffset      : 'latest',   // default: 'latest'
    outOfRangeOffset: 'latest',   // default: 'earliest'

    /* additional defaults {
    // Auto commit config
    autoCommit: true,
    autoCommitIntervalMs: 5000,
    // Fetch message config
    paused: false,
    maxNumSegments: 1000,
    fetchMinBytes: 1,
    maxTickMessages: 1000,
    fromOffset: 'latest',
    outOfRangeOffset: 'earliest',
    sessionTimeout: 30000,
    retries: 10,
    retryFactor: 1.8,
    retryMinTimeout: 1000,
    commitOffsetsOnFirstJoin: true,
    connectOnReady: true,
    migrateHLC: false,
    onRebalance: null,
    topicPartitionCheckInterval: 30000,
    protocol: ['roundrobin'],
    encoding: 'utf8'
    // additional defaults } */
    // ConsumerGroup options }
  };


  if (self.config.verbosity > 0) {
    console.error('>>> %s: create Kafka consumer group [ %s ] ...',
                  self.constructor.name, kafkaConfig.groupId);
    console.error('===   topics     :', topics);

    if (self.config.verbosity > 1) {
      console.error('===   kafkaConfig:', kafkaConfig);
    }
  }

  return new Promise( (resolve, reject) => {
    // Ready event handlers {
    const _onError  = (err) => {
      if (err instanceof TimeoutError) { return }

      if (self.config.verbosity > 0) {
        console.error('*** %s: consumer.init.error (_initConsumer) : %s',
                      self.constructor.name, err);
      }

      if (self.consumer) {
        self.consumer.close( () => {
          reject( err );
        });

      } else {
        reject( err );
      }
    };
    const _onReady  = (firstMsg) => {
      const now = Date.now();

      // Remove the temporary error handler
      self.consumer.off('error', _onError);

      if (self.config.verbosity >= 0) {
        console.error('=== %s: consumer [ %s ] ready',
                      self.constructor.name,
                      self.consumer.options.groupId);
      }

      if (firstMsg) {
        _handleMessage( self, firstMsg );
      }

      // Add event handlers
      self.consumer
        .on('message', msg => _handleMessage( self, msg ) )
        .on('error',   err => _handleConsumerError( self, err ) );

      resolve( now );
    };
    // Ready event handlers }

    try {
      /* Create a new consumer and wait for it to become ready before signaling
       * ready.
       */
      self.consumer  = new Kafka.ConsumerGroup( kafkaConfig, topics );

    } catch(ex) {
      _onError(ex);
    }

    /* Watch for any initial errors in both consumer
     * (and incidentally in consumer.client).
     */
    self.consumer.once( 'error', _onError );

    // Await the availability of the KafkaClient instance ...
    _awaitClient( self )
      .then( client => {
        /* the KafkaClient instance is now available so we can watch for
         * 'ready' or any initial 'error' events.
         */
        client.once( 'ready', _onReady );
      })
      .catch(err => _onError(err) );
  });
}

/**
 *  Create a simple producer for the flow control topic.
 *  @method _initFlowProducer
 *  @param  self        The controlling instance {KafkaSource};
 *
 *  @note   This REQUIRES _initConsumer first generate the ConsumerGroup
 *          instance which contains the KafkaClient needed to create a
 *          producer.
 *
 *  @return A promise for results {Promise};
 *          - resolved with the timestamp on ready {Number};
 *          - rejected with error {Error};
 *  @private
 */
function _initFlowProducer( self ) {
  return new Promise((resolve, reject) => {
    // Ready event handlers }
    const producerOptions = {
      //requireAcks     : 1,    // default 1
      //ackTimeoutMs    : 100,  // default 100

      /* Partitioner type
       *    default = 0 [default]
       *    random  = 1
       *    cyclic  = 2
       *    keyed   = 3
       *    custom  = 4
       */
      //partitionerType : 2,
    };

    self.fcProducer = new Kafka.Producer( self.client, producerOptions );

    const now = Date.now();

    if (self.config.verbosity > 0) {
      console.error('%s._initFlowProducer(): ready @ %d',
                    self.constructor.name, now);
    }

    resolve( now );
  });
}

/**
 *  A helper to await the availability of the KafkaClient contained within the
 *  primary consumer group instance.
 *  @method _awaitClient
 *  @param  self        The controlling instance {KafkaSource};
 *
 *  @return A promise for results {Promise};
 *          - resolved with the client instance {KafkaClient};
 *          - rejected with error {Error};
 */
function _awaitClient( self ) {
  return new Promise((resolve, reject) => {
    const timeout   = 250;
    const maxRounds = 40;
    let   rounds    = 0;
    const _onError  = err => {
      return reject(err);
    };
    const _wait     = () => {
      if (++rounds > maxRounds) {
        // We've waited too long...
        const err = new Error('consumer.client took too long '
                              + `(${rounds} ${timeout}ms rounds)`);

        // Emit an error which should trigger our _onError handler (above)
        return self.emit('error', err);
      }

      if (self.consumer == null || self.consumer.client == null) {
        return setTimeout( () => _wait(), timeout );
      }

      // Remove our error handler
      self.off('error', _onError);

      if (self.config.verbosity > 0) {
        console.error('=== %s: Kafka client available after %d %dms rounds',
                      self.constructor.name,
                      rounds, timeout);
      }

      resolve( self.consumer.client );
    };

    /* Watch for any error events that might signal the consumer will never be
     * ready.
     */
    self.once('error', _onError);

    if (self.config.verbosity > 1) {
      console.error('=== %s: Waiting up to %d %dms rounds for the'
                    +                                   'Kafka client...',
                    self.constructor.name,
                    maxRounds, timeout);
    }

    // Begin our wait...
    _wait();
  });
}

/**
 *  Publish/send a flow-control message on the Kafka flow control topic
 *  @method _publishFlowControl
 *  @param  self        The controlling instance {KafkaSource};
 *  @param  msg     The flow-control message {String};
 *
 *  @return A promise for results {Promise};
 *          - resolved with any data returned by prodcer.send() {Mixed};
 *          - rejected with an error {Error};
 *  @private
 */
function _publishFlowControl( self, msg ) {
  return new Promise((resolve, reject) => {
    const payload = {
      topic   : self.config.flow_control_topic,
      messages: [ msg ],
      //attributes: 0,  // no compression
    };

    if (self.config.verbosity > 0) {
      console.error('=== %s: flow-control send: topic[ %s ], msg[ %s ] ...',
                    self.constructor.name,
                    payload.topic, msg);
    }

    self.fcProducer.send( [payload], (err, data) => {
      if (err) {
        console.error('*** %s: flow-control send: topic[ %s ], msg[ %s ] '
                      +                                   'FAILED:',
                      self.constructor.name,
                      payload.topic, msg,
                      err);
        return reject( err );
      }

      if (self.config.verbosity > 0) {
        console.error('=== %s: flow-control send: topic[ %s ], msg[ %s ] sent:',
                      self.constructor.name,
                      payload.topic, msg,
                      data);
      }

      return resolve( data );
    });
  });
}


/**
 *  Convert a metric in the "uni-variate" form to normalized form.
 *  @method _normalizeUnivariateMetric
 *  @param  self          The controlling instance {KafkaSource};
 *  @param  src           The incoming JSON data {Object};
 *  @param  src.type      The metric type (uni-variate) {String};
 *  @param  src.node      The source node (implicit label) {String};
 *  @param  src.unit      The unit of measure for `value` (implicit label)
 *                        {String};
 *  @param  src.name      The metric name {String};
 *  @param  src.timestamp The timestamp {String};
 *  @param  src.value     The metric value {String};
 *  @param  [src.labels]  Optional, explicit labels {Object};
 *
 *  @return A normalized version of `src` {Object};
 *  @private
 */
function _normalizeUnivariateMetric( self, src ) {
  /* assert( src.type              === 'uni-variate' &&
   *         typeof(src.node)      === 'string' &&
   *         typeof(src.unit)      === 'string' &&
   *         typeof(src.name)      === 'string' &&
   *         typeof(src.timestamp) === 'string' &&
   *         typeof(src.value)     === 'string' )
   */
  const ts    = new Date( src.timestamp );
  const norm  = {
    name      : src.name,
    // Fold the 'node' and 'unit' in as labels
    labels    : {},
    timestamp : ts.getTime(),
    value     : src.value,
  };

  if (src.node) { norm.labels.node = src.node }
  if (src.unit) { norm.labels.unit = src.unit }

  if (!Array.isArray( src.labels ) && (src.labels instanceof Object) ) {
    // Merge the current normalized label set with theh incoming `src.labels`
    Object.assign( norm.labels, src.labels );

    /* :XXX: Do we need a key-sorted label set??
    const labels  = {};

    Object.keys( norm.labels )
      .sort()
      .forEach( key => { labels[key] = norm.labels[key] } );

    norm.labels = labels;
    // */
  }

  return norm;
}

/**
 *  Convert a metric sent from local-streaming analytics into a normalized
 *  form.
 *  @method _normalizeLsaMetric
 *  @param  self          The controlling instance {KafkaSource};
 *  @param  src           The incoming JSON data {Object};
 *  @param  src.route     The metric name {String};
 *  @param  src.node      The source node (implicit label) {String};
 *  @param  src.timestamp The timestamp {String};
 *  @param  src.value     The metric value {String};
 *  @param  [src.labels]  Optional, explicit labels {Object};
 *
 *  @return A normalized version of `src` {Object};
 *  @private
 */
function _normalizeLsaMetric( self, src ) {
  /* assert( typeof(src.route)     === 'string' &&
   *         typeof(src.node)      === 'string' &&
   *         typeof(src.timestamp) === 'string' &&
   *         typeof(src.value)     === 'string' )
   */
  const ts    = new Date( src.timestamp );
  const norm  = {
    name      : src.route,
    labels    : { node: src.node },
    timestamp : ts.getTime(),
    value     : src.value,
  };

  if (!Array.isArray( src.labels ) && (src.labels instanceof Object) ) {
    // Mix-in the explicitly included labels
    Object.assign( norm.labels, src.labels );
  }

  return norm;
}

/**
 *  Handle a consumer error.
 *  @method _handleConsumerError
 *  @param  self    The controlling instance {KafkaSource};
 *  @param  err     The error {Error};
 */
function _handleConsumerError( self, err ) {
  if (err instanceof TimeoutError) { return }

  if (self.config.verbosity > 0) {
    console.error('*** %s: consumer.error: %s',
                    self.constructor.name, err);
  }

  self.emit( 'error', err );
}

/**
 *  Handle an incoming Kafka message.
 *  @method _handleMessage
 *  @param  self            The controlling instance {KafkaSource};
 *  @param  msg             The incoming message {Object};
 *  @param  msg.topic       The kafka topic for this message {String};
 *  @param  msg.value       The value of the message {String};
 *  @param  msg.partition   The kafka partition for this message {String};
 *  @param  msg.offset      The offset within `partition` {Number};
 *
 *  @return void
 *  @private
 */
function _handleMessage( self, msg ) {
  let json;
  let sample;

  if (self.config.verbosity > 0) {
    const partition   = msg.partition;

    if (self._partitions == null)            { self._partitions = {} }
    if (self._partitions[partition] == null) { self._partitions[partition] = 0 }

    self._partitions[partition]++;

    const sampleCount = Object.values( self._partitions )
                            .reduce( (sum,cnt) => sum+cnt, 0 );

    if ( sampleCount >= 10000 ) {

      console.error('=== %s: %s %d samples over partitions:',
                    self.constructor.name,
                    self.client.clientId,
                    sampleCount,
                    self._partitions);

      self._partitions = {};
    }
  }

  if (self.config.verbosity > 2) {
    console.error('=== %s: %s read %d byte msg'
                  +               ' Topic="%s" Partition=%s Offset=%d',
                  self.constructor.name,
                  self.client.clientId,
                  msg.value.length,
                  msg.topic,
                  msg.partition,
                  msg.offset);
  }

  try {
    json = JSON.parse( msg.value );

    if (json == null || typeof(json) !== 'object') {
      throw new Error('message parsed to null');
    }

  } catch(ex) {
    //const bytes = msg.value.map( by => by.toString(10) );

    console.error('*** %s: topic=%s, partition=%s, offset=%d: '
                  +         'Invalid JSON [ %s ] in %d byte msg.value: %s',
                  self.constructor.name,
                  msg.topic,
                  msg.partition,
                  msg.offset,
                  Safe.esc( ex.message ),
                  (msg.value ? msg.value.length
                             : 0),
                  (msg.value ? Safe.esc( msg.value, 'forJson' ).toString()
                             : String(msg.value)));

    self.emit('error', ex );
    return;
  }

  if (json.type === 'uni-variate') {

    json = _normalizeUnivariateMetric( self, json );

  } else if (typeof(json.route) === 'string' &&
             typeof(json.node)  === 'string' &&
             json.labels        ==  null) {

    json = _normalizeLsaMetric( self, json );
  }

  if (self.config.labels) {
    // Mixin any additional labels, preferring any values in the incoming json
    if (json.labels == null)  { json.labels = {} }

    Object.assign(json.labels, self.config.labels, json.labels );
  }

  try {
    sample = new Sample( json );

  } catch(ex) {
    if (self.config.verbosity > 0) {
      console.error('*** %s: Invalid sample from msg [ %s ]:',
                    self.constructor.name, JSON.stringify(msg), ex);
    }

    self.emit('error', ex );
    return;
  }

  self.emit( 'sample', sample );
}

/* Private helpers }
 *****************************************************************************/

module.exports  = KafkaSource;
