const Fs          = require('fs');
const Readline    = require('readline');
const Got         = require('got');
const Sample      = require('../../lib/sample');
const Utils       = require('../../lib/utils');
const AsyncSource = require('./index.js');

/**
 *  A class to fetch and process Prometheus metrics from a specific endpoint
 *  with a specified interval and timeout.
 *  @class  PromScrape
 *
 *  @event  'sample', msg   : emitted when a metric is generated, with `msg`
 *                            having the form:
 *                              { self  : the src    instance {AsyncSource |
 *                                                             PrompScrape},
 *                                sample: the sample instance {Sample},
 *                              }
 *  @event  'error', msg    : emitted on source/connection error with `msg`
 *                            having the form:
 *                              { self  : the src   instance {AsyncSource |
 *                                                            RemoteWriter|
 *                                                            PromScrape},
 *                                error : the error instance {Error},
 *                              }
 *
 *  @event  'scrape.begin',  this : emitted when a scrape begins;
 *  @event  'scrape.cancel', this : emitted when a scrape is canceled;
 *  @event  'scrape.data',   msg  : emitted when a scrape completes with `msg`
 *                                  having the form:
 *                                    { self: the src instance {PromScrape},
 *                                      data: the scraped data {Object},
 *                                    }
 */
class PromScrape extends AsyncSource {

  static get defaults() {
    return Object.assign({}, super.defaults, {
      /**
       *  The name of this scrape job.
       *  @property name {String};
       */
      name    : 'unnamed',

      /**
       *  The URL of the Prometheus metrics endpoint to scrape.
       *  @property url {URL};
       */
      url     : null,

      /**
       *  The srape interval (ms).
       *  @property interval {Number};
       */
      interval: 6000, // 1 minute

      /**
       *  The srape timeout (seconds).
       *  @property timeout {Number};
       */
      timeout : 10,

      /**
       *  The metric class to include with each 'sample'
       *  @property timeout {Number};
       */
      metric_class  : 'application',
    });
  }

  /**
   *  Create a new instance
   *  @constructor
   *  @param  config                    Configuration data {Object};
   *  @param  config.bridge             The primary bridge / event-bus
   *                                    {Bridge};
   *  @param  config.url                The URL of the target endpoint
   *                                    {URL};
   *  @param  [config.interval = 6000]  The scrape interval (ms) {Number};
   *  @param  [config.timeout  = 1000]  The scrape timeout  (ms) {Number};
   *  @param  [config.labels]           Additional labels that should be
   *                                    applied to metrics processed by this
   *                                    writer {Object};
   *  @param  [config.verbosity = 0]    Debug verbosity {Number};
   */
  constructor( config ) {
    super( config );

    Object.defineProperties( this, {
      /**
       *  Is this scraper active?
       *  @property _active {Timer};
       *  @protected
       */
      _active: { value: false, writable: true },

      /**
       *  Scrape timer
       *  @property _timer {Timer};
       *  @protected
       */
      _timer: { value: null, writable: true },

      /**
       *  Processing state
       *  @property _state {Object};
       *  @protected
       */
      _state: { value: this._newState(), writable: true },
    });
  }

  get name()        { return this.config.name }
  get interval()    { return this.config.interval }
  get timeout()     { return this.config.timeout }

  get lineCount()   { return this._state.lineNum }
  get data()        { return this._state.data }
  get ts()          { return this._state.ts }
  get error()       { return this._state.error }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.url} : ${this.name}]`;
  }

  /**
   *  Start this scraper, performing an immediate scrape and, once complete,
   *  scheduling the next.
   *  @method start
   *
   *  @return A promise resolved when this source is ready to generate metrics
   *          {Promise};
   */
  async start() {
    if (this._active) { return this }
    this._active = true;

    // Cancel any existing timer
    this.cancel();

    // Immediately start a scrape, followed by scheduled recurrences
    return new Promise( (resolve, reject) => {
      let hasErr  = false;
      this.scrape()
        .catch(   err => { hasErr = err; reject(err) } )
        .finally( ()  => {
          // Regardless of success/error, schedule the next scrape
          this.schedule();
        });

      process.nextTick( () => {
        if (! hasErr) {
          // Signal "ready"
          this.ready = true;
          resolve(this);
        }
      });
    });
  }

  /* :TODO: Implement pause/resume() to pause/resume the pull schedule.
   *
   *        These methods are invoked by our super-class whenever xoff/xon
   *        events are received on the bridge event bus.
   */

  /**
   *  Stop this scraper.
   *  @method stop
   *
   *  @return A promise resolved when this source has been stopped {Promise};
   */
  async stop() {
    if (! this._active) { return this }

    this.cancel();

    // Mark in-active and not-ready
    this._active = false;
    this.ready   = false;

    return this;
  }

  /**
   *  Cancel the next scrape (if scheduled).
   *  @method cancel
   *
   *  @emit   'cancel'  this            : emitted when a scrape is canceled;
   *
   *  @return An indication of whether a scrape was scheduled {Boolean};
   */
  cancel() {
    if (this._timer) {
      clearTimeout( this._timer );
      this._timer = null;

      this.emit( 'scrape.cancel', this );
      return true;
    }

    return false;
  }

  /**
   *  Schedule the next scrape
   *  @method schedule
   *
   *  @return The time (ms) of the next scrape {Number};
   */
  schedule() {
    // Cancel any existing timer
    this.cancel();

    // Create a new timer
    this._timer = setTimeout( async () => {
      try {
        await this.scrape();
      } catch(ex) {
        /* Squelch this exception, which should already be reflected in
         * `this.error` AND should have already resulted in an 'error' event.
         */
      }

      if (this._timer) {
        // Reset this timer to fire again
        this._timer.refresh();
      }

    }, this.config.interval );

    return ( Date.now() + this.config.interval );
  }

  /**
   *  Initiate a scrape of metrics.
   *  @method scrape
   *
   *  @emit   'scrape'  this            : emitted when a scrape begins;
   *  @emit   'error'   {self:, error:} : emitted on error;
   *  @emit   'data'    {self:, data:}  : emitted when a scrape completes;
   *
   *  @return A promise for metrics data {Promise};
   *          - on success, the metrics data {Object};
   *          - on failure, an error {Error};
   */
  scrape() {
    return new Promise( (resolve, reject) => {
      const tsStart   = Date.now();
      const input     = this._createReadStream();
      const rl        = Readline.createInterface({
        input:  input,
      });

      // Establish a new parsing state
      this._state = this._newState();

      this.emit( 'scrape.begin', this );

      // Handle input stream errors directly
      input.on('error', err => {
        this._state.error = err;
        this._state.nErrors++;

        if (this.config.verbosity) {
          console.error('*** %s:readline.input() ERROR: %s', this, err);
        }

        this.emit( 'error', err );

        reject( err );
      });

      // Process a single metric line
      rl.on('line', line => {
        this._state.lineNum++;
        this._state.nBytes += line.length;

        line = line.trim();
        if (line.length < 1) { return }

        if (this.config.verbosity > 5) {
          console.error('%s: %d: %s', this, this._state.lineNum, line);
        }

        if (line[0] === '#') {
          this._parseComment( line );

        } else {
          this._parseSample( line );
        }
      });

      // Handle a successful completion
      rl.on('close', () => {
        const tsEnd = Date.now();

        if (this.config.verbosity) {
          const secs  = (tsEnd - tsStart) / 1000;

          console.error('=== %s: %s seconds: '
                        +       '%d bytes (%s/s) in %d lines (%s/s): '
                        +       '%d comments, %d samples (%s/s), %d errors ',
                        this,
                        secs.toFixed( 5 ),
                        this._state.nBytes,
                        (this._state.nBytes / secs).toFixed(5),
                        this._state.lineNum,
                        (this._state.lineNum / secs).toFixed(5),
                        this._state.nComments,
                        this._state.nSamples,
                        (this._state.nSamples / secs).toFixed(5),
                        this._state.nErrors);

          if (this.config.verbosity > 5) {
            console.error('=== %s: raw data: %s',
                          JSON.stringify( this._state.data, null, 2 ) );
          }
        }

        this.emit( 'scrape.data', this._state.data );

        resolve( this._state.data );
      });

    });
  }

  /**
   *  Emit an event to the bridge/event-bus.
   *  @method emit
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  payload     The event payload {Any};
   *
   *  On 'sample' events:
   *    - update the sample count;
   *    - generate a message with the form {self:this, sample:payload};
   *
   *  On 'error' events:
   *    - generate a message with the form {self:this, error:payload};
   *
   *  On 'scrape.data' events:
   *    - generate a message with the form {self:this, data:payload};
   *
   *  On all other events, use `payload` as the message;
   *
   *  @return Result {Boolean};
   */
  emit( eventName, payload ) {
    const bridge  = this.config.bridge;
    if (bridge == null) {
      return Utils.makeError(this, 'emit(): no bridge instance');
    }

    let msg = payload;
    switch( eventName ) {
      case 'sample':
        this._sampleCount++;
        msg = { self:this, sample:payload };
        break;

      case 'scrape.data':
        msg = { self:this, data:payload};
        break;

      case 'error':
        msg = { self:this, error:payload };
        break;
    }

    return bridge.emit( eventName, msg );
  }

  /***************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Generate new processing state.
   *  @method _newState
   *
   *  @return The new state {Object};
   *  @protected
   */
  _newState() {
    return {
      ts          : Date.now(), // The default "now" time
      lineNum     : 0,          // The current line number in the source
      nBytes      : 0,
      nComments   : 0,
      nSamples    : 0,
      nErrors     : 0,

      /* Information about the current metric set, which may be comprised of
       * data from multiple lines:
       *    # HELP ...
       *    # TYPE ...
       *    sample#1
       *    ...
       *    sample#n
       *
       *  Each 'sample' entry COULD BE parsed according to the metric type
       *  into a set of metric entries.
       *
       *  This comes into play primarily for types:
       *    HISTOGRAM   - uses special metric name suffixes and label keys
       *      '_bucket' : along with an 'le' label identifies a single
       *                  histogram bucket/value pair;
       *      '_sum'    : identifies the sum of all buckets;
       *      '_count'  : identifies the bucket count;
       *
       *    SUMMARY     - uses special metric name suffixes and label keys
       *      '_sum'    : identifies the sum of all quantiles;
       *      '_count'  : identifies the quantile count;
       *      'quantile' label identifies a single quantile as the label
       *                 value and the quantile measure as the sample value;
       */
      curSet      : {
        name    : null,
        help    : null,
        type    : null,
        samples : [],           // Sample entries
      },

      data        : [           // The generated metric data set.
        // { name:, help:, type:, [label:], samples:[ ... ] }
      ],
    };
  }

  /**
   *  Generate a new metric set.
   *  @method _newSet
   *  @param  props         The properties for the new set {Object};
   *  @param  props.name    The metric name {String};
   *  @param  props.help    The metric help {String};
   *  @param  [props.type]  The metric type {String};
   *
   *  @return The new metric set {Object};
   *  @protected
   */
  _newSet( props ) {
    return Object.assign({
      name      : null,
      help      : '',
      type      : null,
      timestamp : this._state.ts,
      samples   : [],
    }, props);
  }

  /**
   *  Use the current configuration, in particular `config.url`, to create
   *  a readable stream to the target metrics data.
   *  @method _createReadStream
   *
   *  @return A readable stream {Fs.ReadStream};
   */
  _createReadStream() {
    let   stream;

    switch( this.config.url.protocol ) {
      case 'file:': {
        // file://[path]
        const path  = this.config.url.href.slice( 7 );

        if (this.config.verbosity > 1) {
          console.error('=== %s:_createReadStream(): path[ %s ] ...',
                        this, path);
        }

        stream = Fs.createReadStream( path );
      } break;

      default: {
        const gotCfg  = { stream: true, timeout: this.config.timeout };

        stream = Got( this.config.url, gotCfg );

      } break;
    }

    return stream;
  }

  /**
   *  Given a comment/command line, extract any help or type information.
   *  @method _parseComment
   *  @param  line      The metric sample line {String};
   *
   *  @return void
   *  @protected
   */
  _parseComment( line ) {
    // A command line with be '# HELP ...' or '# TYPE ...'
    const token = line.slice( 2, 6 );
    const ws1   = line.slice( 7 ).indexOf(' ');
    if (token.length < 1 || ws1 < 0) { return }

    this._state.nComments++;

    let   metricName  = line.slice( 7, ws1 + 7 );
    const rest        = line.slice( ws1 + 8 );
    switch( token ) {
      case 'HELP':
        this._state.curSet = this._newSet( {name: metricName, help: rest} );
        break;

      case 'TYPE': {
        const type  = rest.toUpperCase();

        if (this._state.curSet.name !== metricName) {
          this._state.curSet.help = null;
        }

        this._state.curSet = this._newSet( {
          name  : metricName,
          type  : rest.toUpperCase(),
          help  : this._state.curSet.help,
        });

        this._state.data.push( this._state.curSet );
      } break;
    }
  }

  /**
   *  Given a line comprising a metric sample, extract the sample data.
   *  @method _parseSample
   *  @param  line      The metric sample line {String};
   *
   *  A sample line should have the form:
   *    %metric_name%[_%metric_key%][{%label%="%val%[,...]}] %val%[ %ts%]
   *
   *  @return void
   *  @protected
   */
  _parseSample( line ) {
    this._state.nSamples++;

    if (this._state.curSet.name == null ||
        ! line.startsWith( this._state.curSet.name )) {

      // New, UNTYPED metric (no help nor type information)
      const metricName  = line.split(/[ {]+/).shift();

      if (this.config.verbosity > 2) {
        console.error('=== %s:_parseSample(): new UNTYPED'
                      +                       'metric[ %s != %s ]: %s',
                      this, metricName, this._state.curSet.name, line );
      }

      this._state.curSet = this._newSet( {
        name    : metricName,
        type    : 'UNTYPED',
      });

      this._state.data.push( this._state.curSet );
    }

    // Additional labels to include
    const extraLabels = Object.assign({}, this.config.labels || {}, {
      job : this.config.name,
      node: this.url.hostname,
    });

    // Create a Sample from this Prometheus Exposition formatted line
    const sample  = new Sample({
      str         : line,
      metric_class: this.config.metric_class,
      timestamp   : this.ts,
      extra_labels: extraLabels,
    });

    this.emit( 'sample', sample );

    this._state.curSet.samples.push( sample );
  }

  /* Protected methods }
   ***************************************************************************/
}

module.exports = PromScrape;
