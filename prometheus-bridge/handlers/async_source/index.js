const Utils = require('../../lib/utils');

/**
 *  The base class for an async metric source which generates 'sample' events
 *  as asynchronous metrics are made available.
 *  @class  AsyncSource
 *
 *  @note   For bridge event listeners (e.g. flow-control) to be properly
 *          attached/detached, the concrete sub-class MUST set `this.ready` to
 *          true/false.
 *
 *  @event  'sample', msg   : emitted when a metric is generated, with `msg`
 *                            having the form:
 *                              { self  : the src    instance {AsyncSource |
 *                                                             PrompScrape},
 *                                sample: the sample instance {Sample},
 *                              }
 *  @event  'error', msg    : emitted on source/connection error with `msg`
 *                            having the form:
 *                              { self  : the src   instance {AsyncSource |
 *                                                            RemoteWriter|
 *                                                            PromScrape},
 *                                error : the error instance {Error},
 *                              }
 */
class AsyncSource {
  static get defaults() {
    return {
      /**
       *  Debug verbosity
       *  @property verbosity {Number};
       */
      verbosity : 0,
    };
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                    The configuration data {Object};
   *  @param  config.bridge             The primary bridge / event-bus
   *                                    {Bridge};
   *  @param  config.url                The URL of the asynchronous metric
   *                                    source {URL};
   *  @param  [config.dry_run = false]  If truthy, do NOT attempt to connect
   *                                    {Boolean};
   *  @param  [config.labels]           Additional labels that should be
   *                                    applied to metrics from this source
   *                                    {Object};
   *  @param  [config.verbosity = 0]    Debug verbosity {Number};
   */
  constructor( config ) {
    if (this.constructor.defaults) {
      // Extend 'config' using class defaults
      config = Object.assign( {}, this.constructor.defaults, config || {} );
    }

    /* Expand any values that contain an expandable prefix (e.g. 'file:') and
     * then convert the values of any timestamp/interval duration keys to
     * milliseconds.
     *
     * For example:
     *    {
     *      key1        : 'file:/path/to/file.txt', => contents of named file
     *      key_timeout :  '5m',                    => 300,000
     *      key_interval:  '30s',                   =>  30,000
     *    }
     */
    config = Utils.convertDurations( Utils.expandValues( config ) );

    if (config.url == null) {
      throw Utils.makeError(this, "Missing required configuration 'url'");
    }
    if (! (config.url instanceof URL)) {
      throw Utils.makeError(this,
                            "Invalid configuration 'url' (not a URL instance)");
    }
    if (config.bridge == null) {
      throw Utils.makeError(this, "Missing required configuration 'bridge'");
    }

    Object.defineProperties( this, {
      /**
       *  The instance configuration.
       *  @property config {Object};
       */
      config      : { value: config,  enumerable: true },

      /**
       *  Simple ready boolean.
       *  @property __ready {Boolean};
       *  @protected
       */
      __ready     : { value: false,                     writable: true },

      /**
       *  Ready promise.
       *  @property _ready {Promise};
       *  @protected
       */
      _ready      : { value: null,                      writable: true },

      /**
       *  Total sample count
       *  @property _sampleCount {Number};
       *  @protected
       */
      _sampleCount: { value: 0,                         writable: true },

      /**
       *  Bridge event listeners. These are attached/detatched via
       *  _attach/detachListeners().
       *  @property _listeners {Object};
       *  @protected
       *
       *  Keys are event names, values are event listeners.
       */
      _listeners  : { value: {
                        xon   : this._onXon.bind( this ),
                        xoff  : this._onXoff.bind( this ),
                      }
                    },
    });
  }

  /**
   *  The configured bridge.
   *  @property bridge {Bridge}
   *  @read-only
   */
  get bridge()      { return this.config.bridge }

  /**
   *  The configured url.
   *  @property url
   *  @read-only
   */
  get url()         { return this.config.url }

  /**
   *  The dry-run indicator.
   *  @property dry_run
   *  @read-only
   */
  get dry_run()   { return this.config.dry_run }

  /**
   *  The ready status.
   *  @property ready
   *
   *  When set to:
   *  - a truthy value when `config.dry_run` is not set, this will cause
   *    event listeners to be attached;
   *    to this writer;
   *  - a non-truthy value (or when `config.dry_run` is set), this will cause
   *    event listener to be detached;
   */
  get ready()     { return this.__ready }
  set ready(val)  {
    if (this.config.verbosity > 2) {
      console.error('=== %s: set ready( %s )', this, val);
    }

    this.__ready = val;

    if (this.__ready && ! this.config.dry_run) {
      // Attach event listeners
      this._attachListeners();

    } else {
      // Detach event listeners
      this._detachListeners();
    }
  }

  /**
   *  The count of samples that have been processed.
   *  @property sampleCount
   *  @read-only
   */
  get sampleCount() { return this._sampleCount }

  /**
   *  The count of samples that are pending / in-progress (-1 == not-reported).
   *  @property pendingCount
   *  @read-only
   */
  get pendingCount(){ return -1 }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    const url = (this.config ? this.config.url : '<not-yet-set>');

    return `${this.constructor.name}[${url}]`;
  }

  /**
   *  Start this source.
   *  @method start
   *
   *  @return A promise resolved when this source is ready to generate metrics
   *          {Promise};
   */
  async start() {
    throw Utils.makeError(this, 'start() must be implemented');
  }

  /**
   *  Pause this source.
   *  @method pause
   *
   *  Invoked when an 'xoff' event is received on the bridge event bus.
   *
   *  @return A promise resolved when this source has been paused {Promise};
   */
  async pause() {
    // Default to stop()
    return this.stop();
  }

  /**
   *  Resume this source.
   *  @method resume
   *
   *  Invoked when an 'xon' event is received on the bridge event bus.
   *
   *  @return A promise resolved when this source has been resumed {Promise};
   */
  async resume() {
    // Default to start()
    return this.start();
  }

  /**
   *  Stop this source.
   *  @method stop
   *
   *  @return A promise resolved when this source has been stopped {Promise};
   */
  async stop() {
    throw Utils.makeError(this, 'stop() must be implemented');
  }

  /**
   *  Emit an event to the bridge/event-bus.
   *  @method emit
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  payload     The event payload {Any};
   *
   *  On 'sample' events:
   *    - update the sample count;
   *    - generate a message with the form {self:this, sample:payload};
   *
   *  On 'error' events:
   *    - generate a message with the form {self:this, error:payload};
   *
   *  On all other events, use `payload` as the message;
   *
   *  @return Result {Boolean};
   */
  emit( eventName, payload ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'emit(): no bridge instance');
    }

    let msg = payload;
    switch( eventName ) {
      case 'sample':
        this._sampleCount++;
        msg = { self:this, sample:payload };
        break;

      case 'error':
        msg = { self:this, error:payload };
        break;
    }

    return this.bridge.emit( eventName, msg );
  }

  /**
   *  Subscribe to an event on the bridge/event-bus.
   *  @method on
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  on( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'on(): no bridge instance');
    }

    return this.bridge.on( eventName, listener );
  }

  /**
   *  Subscribe to a single triggering of an event on the bridge/event-bus.
   *  @method once
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  once( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'once(): no bridge instance');
    }

    return this.bridge.once( eventName, listener );
  }

  /**
   *  Unsubscribe from an event on the bridge/event-bus.
   *  @method off
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  off( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'off(): no bridge instance');
    }

    return this.bridge.off( eventName, listener );
  }

  /**************************************************************************
   * Protected listeners {
   */

  /**
   *  Event listener for the 'xon' event
   *  @method _onXon
   *
   *  Flow-control transmit on (resume).
   *
   *  @protected
   */
  _onXon() {
    if (this.config.verbosity > 0) {
      console.error('=== %s: xon / resume', this);
    }

    this.resume()
      .then( res => {
        console.error('=== %s: xon / resumed:', this, res);
      })
      .catch( err => {
        console.error('*** %s: xon / resume failed:', this, err);
      });
  }

  /**
   *  Event listener for the 'xoff' event
   *  @method _onXoff
   *
   *  Flow-control transmit on (resume).
   *
   *  @protected
   */
  _onXoff() {
    if (this.config.verbosity > 0) {
      console.error('=== %s: xoff / pause', this);
    }

    this.pause()
      .then( res => {
        console.error('=== %s: xoff / paused:', this, res);
      })
      .catch( err => {
        console.error('*** %s: xoff / pause failed:', this, err);
      });
  }

  /**
   *  Attach event listeners once this writer is ready.
   *  @method _attachListeners
   *
   *  @return `this` for a fluent interface;
   *  @protected
   */
  _attachListeners() {
    if (this.config.verbosity > 2) {
      console.error('=== %s: attach %d listeners [ %sbridge ]...',
                  this, Object.keys( this._listeners ).length,
                  (this.bridge ? '' : '!'));
    }

    if (this.bridge) {
      // Attach event listeners
      for (const [name, listener] of Object.entries( this._listeners )) {
        this.bridge.on( name, listener );
      }
    }

    return this;
  }

  /**
   *  Detach event listeners when this writer becomes non-ready.
   *  @method _detachListeners
   *
   *  @return `this` for a fluent interface;
   *  @protected
   */
  _detachListeners() {
    if (this.config.verbosity > 2) {
      console.error('=== %s: detach %d listeners [ %sbridge ]...',
                    this, Object.keys( this._listeners ).length,
                    (this.bridge ? '' : '!'));
    }

    if (this.bridge) {
      // Detach event listeners
      for (const [name, listener] of Object.entries( this._listeners )) {
        //this.config.bridge.removeListener( name, listener );
        this.bridge.off( name, listener );
      }
    }

    return this;
  }

  /* Protected listeners }
   **************************************************************************/
}

module.exports = AsyncSource;
