const Utils         = require('../../lib/utils');
const Format        = require('util').format;
const RemoteWriter  = require('./index.js');
const Kafka         = require('kafka-node');
const Uuid          = require('uuid');

const TopicsNotExistError  = require('kafka-node/lib/errors/TopicsNotExistError');

/**
 *  A concrete remote writer for a Kafka endpoint.
 *  @class  RemoteKafka
 *  @extend RemoteWriter
 */
class RemoteKafka extends RemoteWriter {
  static get defaults() {
    return Object.assign({}, super.defaults, {
      topic_prefix      : 'metrics',  // kafka default: 'metrics'
      compression       : 'none',     /* kafka default: 'none'
                                       *    gzip[1], snappy[2]
                                       */

      connect_timeout   : '10s',      // kafka default: '10s'
      idle_timeout      : '5m',       // kafka default: '5m'
      request_timeout   : '30s',      // kafka default: '30s'
      request_max_bytes : 1048576,    // The maximum request/batch size (bytes).
      request_fill      : 0.9,        /* Maximum to use from
                                       * `request_max_bytes` (percent).
                                       */

      batch_num_samples : 10000,      /* The number of samples to batch within
                                       * a single push
                                       */
      batch_timeout     : '1s',       /* The time to wait for additional
                                       * samples, up to `batch_num_samples` or
                                       * `request_max_bytes`, before sending
                                       * (ms)
                                       */

      flow_control_topic: 'flow-control', /* The topic to use for kafka-based
                                           * flow-control messages.
                                           *
                                           * :NOTE: This MUST match the topic
                                           *        used by any KafkaSource
                                           *        that might signal
                                           *        flow-control.
                                           */
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                              The configuration data
   *                                              {Object};
   *  @param  config.url                          The URL of the remote
   *                                              endpoint {URL};
   *  @param  [config.topic_prefix = 'metrics']   Kafka topic prefix {String};
   *  @param  [config.compression = 'none']       Kafka compression {String};
   *  @param  [config.request_max_bytes = 1048576]
   *                                              The maximum request/batch
   *                                              size (bytes) {Number};
   *  @param  [config.request_fill = 0.9]         The maximum to use from
   *                                              `request_max_bytes` (percent)
   *                                              {Number};
   *  @param  [config.batch_num_samples = 10000]  Number of samples to batch
   *                                              within a single push
   *                                              {Number};
   *  @param  [config.batch_timeout = '1s']       The time to wait for
   *                                              additional samples, up to
   *                                              `batch_num_samples` or
   *                                              `request_max_bytes`, before
   *                                              sending (ms) {String|Number};
   *  @param  [config.connect_timeout = '10s']    Connection timeout (ms)
   *                                              {String|Number};
   *  @param  [config.idle_timeout = '5m']        Idle timeout (ms)
   *                                              {String|Number};
   *  @param  [config.request_timeout = '30s']    Request timeout (ms)
   *                                              {String|Number};
   *  @param  [config.dry_run = false]            If truthy, do NOT attempt to
   *                                              connect {Boolean};
   *  @param  [config.labels]                     Additional labels that should
   *                                              be applied to metrics
   *                                              processed by this writer
   *                                              {Object};
   *  @param  [config.verbosity = 0]              Debug verbosity {Number};
   *
   *  Supported URL format:
   *    kafka://host:9092[/topic_prefix]
   *
   *  @note Multiple kafka hosts may be listed in a single URL IF they
   *        all share the same port AND topic prefix.
   *            kafka://host1,host2,host3:9092/topic_prefix
   *              => host1:9092/topic_prefix
   *              => host2:9092/topic_prefix
   *              => host3:9092/topic_prefix
   */
  constructor( config ) {
    super( config );

    Object.defineProperties( this, {
      /**
       *  The "type" of this remote writer.
       *  @property type
       *  @readonly
       */
      type      : { value: 'kafka', enumerable: true },

      /**
       *  The KafkaClient instance.
       *  @property client
       *  @protected
       */
      client    : { value: null,    writable: true },

      /**
       *  The Kafka producer instance.
       *  @property producer
       *  @protected
       */
      producer  : { value: null,    writable: true },

      /**
       *  The internal metrics queue used to batch metrics.
       *  @property queue
       *  @protected
       */
      queue     : { value: [],      writable: true },

      /**
       *  The Kafka consumer instance used to monitor the flow control topic.
       *  @property fcConsumer
       *  @protected
       */
      fcConsumer: { value: null,    writable: true },

      /**
       *  A map of Kafka compression strings to the required topic attribute
       *  value.
       *  @property _compressionMap
       *  @readonly
       */
      _compressionMap: {
        value: {
          'none'  : 0,
          'gzip'  : 1,
          'snappy': 2,
        },
      },
    });

    if (this.config.dry_run) {
      // this._ready = Promise.reject( 'dry-run' ); // super-class
      return;
    }
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.config.url}]`;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = _initKafka( this );
    }

    return this._ready;
  }

  /**************************************
   * No need to override pause/resume
   *
  /** Pause this writer.
   *  @method pause
   *
   *  The default pause simply emits an transmit off (xoff) event on the bridge
   *  event bus.
   *
   *  @return A promise resolved when this writer has been paused {Promise};
   *
  async pause() {
    // Emit a flow control transmit off (xoff) event on the bridge event bus.
    this.xoff();
  }

  /**
   *  Resume this writer.
   *  @method resume
   *
   *  The default resume simply emits an transmit on (xon) event on the bridge
   *  event bus.
   *
   *  @return A promise resolved when this writer has been resumed {Promise};
   *
  async resume() {
    // Emit a flow control transmit on (xon) event on the bridge event bus.
    this.xon();
  }
  // */

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {

      this._stopping = new Promise( (resolve, reject) => {
        if (this.client == null) {
          // No client/connection
          return resolve( false );
        }

        if (this.config.verbosity > 0) {
          console.error('=== %s: %s stopping ...',
                        this.constructor.name,
                        this.client.clientId);
        }

        // Ensure all pending pushes have been completed
        const pending = [];
        if (this._pendingPush) {
          // Push the current queue immediately
          clearTimeout( this._pendingPush );

          if (this.queue.length > 0) {
            if (this.config.verbosity > 0) {
              console.error('=== %s: %s flush %d samples ...',
                            this.constructor.name,
                            this.client.clientId,
                            this.queue.length);
            }

            pending.push( _pushQueue( this ) );
          }
        }

        Promise.all( pending )
          .then( res => {
            // Close the client
            this.client.close( () => {
              if (this.config.verbosity > 0) {
                console.error('=== %s: %s client closed',
                              this.constructor.name,
                              this.client.clientId);
              }

              resolve( true );
            });
          })
          .catch( err => reject(err) )
          .finally( () => {
            // Wait a beat before we actually release the client
            setTimeout( () => { this.client = null }, 100 );
          });
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {

    if (! Array.isArray(metrics)) { metrics = [ metrics ] }

    // Enhance `metrics` with the scrape job name and any additional labels
    metrics = this.enhance( metrics, scrape );

    if (this._pendingPush) {
      clearTimeout( this._pendingPush );
      this._pendingPush = null;
    }

    // Push these metrics onto our message queue
    metrics.forEach( metric => {
      this.queue.push( metric );
    });

    if (! this.ready)             { await this._ready }

    if (this.queue.length > this.config.batch_num_samples) {
      // Our queue is full -- send it immediately
      await _pushQueue( this );

    } else {
      // Our queue is not yet full -- wait for additional samples
      this._pendingPush = setTimeout( () => {
        _pushQueue( this )
          .catch( err => { /* squelch */ } );

      }, this.config.batch_timeout );
    }

    return this.queue.length;
  }
}

/*****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Initialize the Kafka client and create a producer.
 *  @method _initKafka
 *  @param  self      The controlling instance {RemoteKafka};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initKafka( self ) {
  const url         = self.config.url;
  const kafkaConfig = {
    kafkaHost     : url.hostname.split(',')
                      .map( str => `${str}:${url.port}` )
                      .join(','),

    connectTimeout: self.config.connect_timeout,
    idleConnection: self.config.idle_timeout,
    requestTimeout: self.config.request_timeout,

    connectRetryOptions: { // node-retry
      //retries   : 5,
      //factor    : 2,

      minTimeout: self.config.connect_timeout / 2,  // 1s
      maxTimeout: self.config.connect_timeout,      // 60s

      //randomize : true,
    },

    // Available at send() via kafkaClient.options.topicPrefix
    topicPrefix   : (url.pathname.length > 1
                      ? url.pathname.slice(1)
                      : self.config.topic_prefix),
  };

  /*
  console.error('_initKafka(): kafkaConfig: %s',
                JSON.stringify(kafkaConfig, null, 2));
  // */

  // Create a Kafka client
  self.client = new Kafka.KafkaClient( kafkaConfig );

  /* Generate a promise (_ready) that will be resolved once the producer
   * reports 'ready'
   */
  return new Promise( (resolve, reject) => {
    const pending = [ _initProducer(self), _initFlowConsumer(self) ];

    Promise.all( pending )
      .then( res => {
        // Both producer and flow-consumer are ready.
        const now = Date.now();

        if (self.config.verbosity > 1) {
          const producerRes = res[0];
          const flowRes     = res[1];

          console.error('=== %s: %s producer ready[ %s ], '
                        +          'flow-control ready[ %s %s ]',
                        self.constructor.name,
                        self.client.clientId,
                        producerRes,
                        self.fcConsumer.options.groupId,
                        flowRes);
        }

        self.ready = now;
        resolve( now );
      })
      .catch( err => {
        if (self.config.verbosity > 0) {
          console.error('*** %s: initialization failed: %s',
                        self.constructor.name, err);
        }

        reject( err );
      });
  });
}

/**
 *  Create a Kafka flow control consumer.
 *  @method _initFlowConsumer
 *  @param  self      The controlling instance {RemoteKafka};
 *
 *  @return A promise for results {Promise};
 *          - resolved with the timestamp on ready {Number};
 *          - rejected with error {Error};
 *  @private
 */
function _initFlowConsumer( self ) {
  return new Promise( (resolve, reject) => {
    const topics      = [
      {
        topic     : self.config.flow_control_topic,
        //offset    : 0,  // default 0
        //partition : 0,  // default 0
      },
    ];
    const topicNames  = topics.map( entry => entry.topic );

    // Check if the target topic(s) exist
    self.client.topicExists( topicNames, err => {
      let promise;

      if (err == null) {
        // The topic(s) already exist
        promise = Promise.resolve( self );

      } else {
        if (err instanceof TopicsNotExistError) {
          /* The topic(s) do not exist -- create them before creating our
           *                              consumer
           */
          promise = _createFlowControlTopics( self, topics );

        } else {

          // Unexpected error -- we're done
          return reject( err );
        }
      }

      /**********************************************************************
       * Once the topic(s) exist (either pre-existed or after creation)...
       *
       */
      promise
        .then( res => {
          // ... Identify the latest offsets ...
          return _getFlowControlOffset( self, topics );
        })
        .then( topics => {
          const options = {
            /* :XXX: Ensure we have a unique group for each flow-control
             *       consumer to allow ALL consumers to receive all
             *       flow-control messages (pub/sub).
             */
            groupId   : `flow-${Uuid.v4()}`,
            fromOffset: true,
          };

          if (self.config.verbosity > 0) {
            console.error('=== %s: %s flow-control topic/offsets:',
                          self.constructor.name,
                          self.client.clientId,
                          topics);
          }

          // ... create a new consumer for the topic(s) ...
          return new Kafka.Consumer( self.client, topics, options );
        })
        .then( consumer => {
          // ... consumer ready
          const now = Date.now();

          self.fcConsumer = consumer;

          if (self.config.verbosity > 1) {
            console.error('=== %s: %s flow-control ready @ %d',
                          self.constructor.name,
                          self.client.clientId,
                          now);
          }

          // Attach our primary handlers
          self.fcConsumer
            .on('error',   err => _handleFlowControlError(   self, err ) )
            .on('message', msg => _handleFlowControlMessage( self, msg ) );

          resolve( now );
        })
        .catch( err => {
          if (self.config.verbosity > 0) {
            console.error('*** %s: flow initialization failed: %s',
                          self.constructor.name, err);
          }

          reject( err );
        });
    })
  });
}

/**
 *  Attempt to create the flow control topic(s).
 *  @method _createFlowControlTopics
 *  @param  self      The controlling instance {RemoteKafka};
 *  @param  topics    The set of topics to create {Array};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _createFlowControlTopics( self, topics ) {
  return new Promise((resolve, reject) => {
    // Attempt to create the topic and try initialization again.
    const numBrokers    = Object.keys( self.client.brokerMetadata ).length;
    const createTopics  = topics.map( entry => {
      return {
        topic             : entry.topic,
        partitions        : numBrokers,
        replicationFactor : 1,
      };
    });
    const logInfo       = Format('%d topic%s in %d partition%s [ %s ]',
                                  createTopics.length,
                                  (createTopics.length === 1 ? '' : 's'),
                                  numBrokers,
                                  (numBrokers === 1 ? '' : 's'),
                                  createTopics
                                    .map( entry => entry.topic )
                                    .join(', '));

    if (self.config.verbosity > 1) {
      console.error('=== %s: %s flow-control create %s ...',
                    self.constructor.name,
                    self.client.clientId,
                    logInfo);
    }

    self.client.createTopics( createTopics, (err, res) => {
      if (err) {
        if (self.config.verbosity > 0) {
          console.error('*** %s: %s flow-control create %s FAILED:',
                        self.constructor.name,
                      self.client.clientId,
                      logInfo,
                      err);
        }

        return reject(err);
      }

      if (self.config.verbosity > 0) {
        console.error('=== %s: %s flow-control create %s SUCCESS:',
                      self.constructor.name,
                      self.client.clientId,
                      logInfo,
                      res);
      }

      return resolve( self );
    });
  });
}

/**
 *  Identify the latest offset of the flow control topic(s).
 *  @method _getFlowControlOffset
 *  @param  self      The controlling instance {RemoteKafka};
 *  @param  topics    The flow control topic(s) {Array};
 *
 *  @return A promise for results {Promise};
 *          - on success, the updated array of topics with the latest offset
 *                        for each;
 *  @private
 */
function _getFlowControlOffset( self, topics ) {
  return new Promise((resolve, reject) => {
    if (self.offset == null) {
      self.offset = new Kafka.Offset( self.client );
      self.offset.on('error', () => { /* no-op */ });
    }

    const topicNames  = topics.map( entry => entry.topic );

    self.offset.fetchLatestOffsets( topicNames, (err, offsets) => {
      if (err) {
        if (self.config.verbosity > 0) {
          console.error('*** %s: flow-control offset fetch FAILED: %s',
                        self.constructor.name, err);
        }
        return reject(err);
      }

      /* Locate the last offset for each topic and set the initial offset to
       * that value.
       *
       *  offsets == {
       *    %topic%: {
       *      %partition%: %offset%,
       *      ...
       *    },
       *    ...
       *  }
       */
      topics.forEach( entry => {
        // Find the maximum offset from all topic partitions
        const topic   = entry.topic;
        const offset  = Object.values( offsets[topic] )
                              .reduce( (max, offset) => {
                                return (offset > max ? offset : max);
                              }, 0);

        /* Set the initial offset for this topic to start with the latest
         * flow-control message.
         *
         * This should cause each new flow-control listener to start in the
         * same flow-control state.
         */
        entry.offset = (offset > 0 ? offset - 1 : offset);
      });

      resolve( topics );
    });
  });
}

/**
 *  Handle an incoming Kafka flow control message.
 *  @method _handleFlowControlMessage
 *  @param  self            The controlling instance {KafkaSource};
 *  @param  msg             The incoming message {Object};
 *  @param  msg.topic       The kafka topic for this message {String};
 *  @param  msg.value       The value of the message {String};
 *  @param  msg.partition   The kafka partition for this message {String};
 *  @param  msg.offset      The offset within `partition` {Number};
 *
 *  @return void
 *  @private
 */
function _handleFlowControlMessage( self, msg ) {
  // assert( msg.topic === self.config.flow_control_topic );
  if (self.config.verbosity > 1) {
    console.error('=== %s: %s flow-control Topic="%s" Partition=%s Offset=%d:',
                  self.constructor.name,
                  self.client.clientId,
                  msg.topic,
                  msg.partition,
                  msg.offset,
                  msg.value);
  }

  switch( msg.value ) {
    case 'xoff':  // xoff/transmit off : pause()
      if (self.config.verbosity > 0) {
        console.error('=== %s: %s flow-control: pause ...',
                      self.constructor.name,
                      self.client.clientId);
      }

      self.pause()
        .then( res => {
          if (self.config.verbosity >= 0) {
            console.error('=== %s: %s flow-control: paused  / xoff:',
                          self.constructor.name,
                          self.client.clientId,
                          res);
          }
        })
        .catch(err => {
          console.error('*** %s: %s flow-control: pause failed:',
                          self.constructor.name,
                          (self.client ? self.client.clientId : '<unset>'),
                          err);
        });
      break;

    case 'xon':   // xon/transmit on  : resume()
      if (self.config.verbosity > 0) {
        console.error('=== %s: %s flow-control: resume ...',
                      self.constructor.name,
                      self.client.clientId);
      }

      self.resume()
        .then( res => {
          if (self.config.verbosity >= 0) {
            console.error('=== %s: %s flow-control: resumed / xon:',
                          self.constructor.name,
                          self.client.clientId,
                          res);
          }
        })
        .catch(err => {
          console.error('*** %s: %s flow-control: resume failed:',
                          self.constructor.name,
                          (self.client ? self.client.clientId : '<unset>'),
                          err);
        });
      break;

    default:
      console.warn('=== %s: %s flow-control: ignore unexpected message:',
                      self.constructor.name,
                      (self.client ? self.client.clientId : '<unset>'),
                      msg.value);
      break;
  }
}

/**
 *  Handle an error with the flow control consumer.
 *  @method _handleFlowControlError
 *  @param  self  The controlling instance {KafkaSource};
 *  @param  err   The error {Error};
 *
 *  @return void
 *  @private
 */
function _handleFlowControlError( self, err ) {
  if (self.config.verbosity > 0) {
    console.error('*** %s: %s flow-control error:',
                    self.constructor.name,
                    self.client.clientId,
                    err);
  }

  self.emit('error', err );
}

/**
 *  Create the Kafka producer.
 *  @method _initProducer
 *  @param  self      The controlling instance {RemoteKafka};
 *
 *  @return A promise for results {Promise};
 *          - resolved with the timestamp on ready {Number};
 *          - rejected with error {Error};
 *  @private
 */
function _initProducer( self ) {
  return new Promise( (resolve, reject) => {
    // Ready event handlers {
    const _onError  = (err) => {
      if (self.config.verbosity > 0) {
        console.error('*** %s._initProducer():error: %s',
                      self.constructor.name, err);
      }

      reject( err );
    };
    const _onReady  = () => {
      const now = Date.now();

      
      if (self.config.verbosity > 0) {
        console.error('=== %s._initProducer(): ready @ %d',
                      self.constructor.name, now);
      }

      // Remove the ready error handler
      self.producer.off( 'error', _onError );

      resolve( now );
    };
    // Ready event handlers }
    const producerOptions = {
      //requireAcks     : 1,    // default 1
      //ackTimeoutMs    : 100,  // default 100

      /* Partitioner type
       *    default = 0 [default]
       *    random  = 1
       *    cyclic  = 2
       *    keyed   = 3
       *    custom  = 4
       */
      partitionerType : 2,
    };

    self.producer = new Kafka.Producer( self.client, producerOptions );

    if (! self.producer.ready) {
      /*
      console.error('_initProducer(): wait for ready ...');
      // */

      self.producer
        .once('ready', _onReady)
        .once('error', _onError);

    } else {
      // Immediately ready
      _onReady();
    }
  });
}

/**
 *  Push all samples in the current queue and reset the queue.
 *  @method _pushQueue
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _pushQueue( self ) {
  const pending   = [];
  const sampleCnt = self.queue.length;

  if (sampleCnt < 1)  { return Promise.resolve( 0 ) }

  /*
  console.error('%s._pushQueue(): %d sample%s: ...',
                self.constructor.name,
                sampleCnt, (sampleCnt === 1 ? '' : 's'));
  // */

  while (self.queue.length > 0) {
    pending.push( _pushBatch(self) );
  }

  const promise = Promise.all( pending );

  promise
    .then( res => {
      const batchCnt  = res.length;

      self._sampleCount += sampleCnt;

      if (self.config.verbosity > 1) {
        console.error('=== %s: %s %d sample%s in %d batch%s',
                      self.constructor.name,
                      self.client.clientId,
                      sampleCnt, (sampleCnt === 1 ? '' : 's'),
                      batchCnt,  (batchCnt  === 1 ? '' : 'es'));
      }

      return batchCnt;
    });

  return promise;
}

/**
 *  Generate a JSON-encoded version of the given metric sample.
 *  @method _jsonEncodeSample
 *  @param  sample    The target sample {Sample};
 *
 *  @return A JSON-encoded version {String};
 *  @private
 */
function _jsonEncodeSample( sample ) {
  /* Make a clone of the JSON object version of `sample` so we don't
   * inadvertantly modify it.
   */
  const json  = {
    type      : 'uni-variate',

    node      : null,
    unit      : null,

    name      : sample.name,
    value     : sample.value,
    timestamp : sample.timestamp,
    labels    : {},
  };

  // Promote 'node' and 'unit' labels to the top, removing them from labels
  Object.entries(sample.labels).forEach( ([key,val]) => {
    switch( key ) {
      case 'node':  json.node = val;  return;
      case 'unit':  json.unit = val;  return;
    }

    json.labels[key] = val;
  });

  return JSON.stringify( json );
}

/**
 *  Given a set of samples to be sent, generate kafka payloads, each limited to
 *  the maximum kafka message size.
 *  @method _generatePayloads
 *  @param  self      The controlling instance {RemoteTimescale};
 *  @param  samples   The set of samples to be sent {Array};
 *  @param  fullCnt   The full count of enqueued samples {Number};
 *
 *  @return The set of payloads {Array};
 *          Each payload will have the form:
 *              { topic     : 'kafka topic',
 *                messages  : [ %json-encoded-messages% ],
 *                attributes: %compression-attribute%,
 *              }
 *  @private
 */
function _generatePayloads( self, samples, fullCnt ) {
  const payloads  = [];

  /* First, divide samples into metric classes, each to be sent to a
   * class-related topic (using `self.client.options.topicPrefix` as a prefix).
   */
  const classes = {};
  samples.forEach( sample => {
    const cls = (sample.metric_class || 'application');

    if (! Array.isArray( classes[cls] )) {
      classes[cls] = [];
    }

    // Convert the sample to it's JSON representation for publication
    const encoded = _jsonEncodeSample( sample );
    classes[cls].push( encoded );
  });

  if (self.config.verbosity > 1) {
    const clsCnt    = Object.keys(classes).length;
    const sampleCnt = samples.length;

    console.error('=== %s: %s generate payloads: %d / %d sample%s, '
                  +                                 '%d class%s: ...',
                  self.constructor.name,
                  self.client.clientId,
                  sampleCnt, fullCnt, (sampleCnt === 1 ? '' : 's'),
                  clsCnt, (clsCnt === 1 ? '' : 'es') );
  }

  /* Given the accumulated metric classes, break them each into one or more
   * payloads that fit within the kafka message size limit.
   */
  const topicPrefix = self.client.options.topicPrefix;
  Object.entries( classes ).forEach( ([cls, clsSamples]) => {
    const topic         = `${topicPrefix}_${cls}`;
    let   payloadCnt    = 0;
    let   payloadSize   = 0;
    const payload       = {
      topic     : topic,
      messages  : [],
      attributes: self._compressionMap[ self.config.compression ],
    };

    const max_bytes = Math.ceil( self.config.request_max_bytes *
                                 self.config.request_fill );
    clsSamples.forEach( sample => {
      const sampleSize  = sample.length;

      if (payloadSize + sampleSize > max_bytes) {
        if (payloadSize < 1) {
          // A single sample is too big!!
          console.error('*** %s: %s generate: single message for'
                        +   ' topic[ %s ]: too big'
                        +   ' (%d bytes exceeds configured %d kafka max * %s)'
                        +                       ': %s',
                        self.constructor.name,
                        self.client.clientId,
                        payload.topic,
                        samplesize,
                        self.config.request_max_bytes,
                        self.config.request_fill,
                        Utils.inspect(sample));
          return;
        }

        // This payload entry is complete.
        const newPayload  = Object.assign({}, payload);
        newPayload.messages = payload.messages.slice();

        payloads.push( newPayload );

        // Reset the message accumulation state
        payload.messages.length = 0;
        payloadSize             = 0;
        payloadCnt++;
      }

      payload.messages.push( sample );
      payloadSize += sampleSize;
    });

    if (payloadSize > 0) {
      // Last payload
      payloads.push( payload );
      payloadCnt++;
    }

    if (payloadCnt > 1) {
      // ALWAYS report if we have to split samples into multiple payloads
      const now = new Date();

      console.error('=== %s %s topic=%s; samples=%d; payloads=%d',
                    now, self,
                    topic, clsSamples.length, payloadCnt);

    } else if (self.config.verbosity > 0) {
      const sampleCnt = clsSamples.length;

      console.error('=== %s: %s generate %d sample%s to topic[ %s ]:'
                    +                             ' %d payload%s',
                    self.constructor.name,
                    self.client.clientId,
                    sampleCnt,  (sampleCnt === 1 ? '' : 's'),
                    topic,
                    payloadCnt, (payloadCnt === 1 ? '' : 's'));
    }
  });

  return payloads;
}

/**
 *  Push the next batch of queued samples, removing them from the queue.
 *  @method _pushBatch
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _pushBatch( self ) {
  return new Promise( (resolve, reject) => {
    const fullCnt   = self.queue.length;

    // Extract the samples for this batch
    const samples   = self.queue.splice( 0, self.config.batch_num_samples );

    if (samples.length < 1)   { return resolve( null ) }

    // Generate kafka payloads from the samples for this batch
    const payloads  = _generatePayloads( self, samples, fullCnt );

    if (payloads.length < 1)  { return resolve( null ) }

    // Send the payloads
    const pending = payloads.map( payload => {
      return new Promise( (subResolve, subReject) => {
        // Send the payload
        const messageCnt  = payload.messages.length;

        if (self.config.verbosity > 1) {
          console.error('=== %s: %s %d message%s to topic[ %s ]',
                        self.constructor.name,
                        self.client.clientId,
                        messageCnt, (messageCnt === 1 ? '' : 's'),
                        payload.topic,
                        Utils.inspect(payload));

        } else if (self.config.verbosity > 0) {
          console.error('=== %s: %s %d message%s to topic[ %s ]: ...',
                        self.constructor.name,
                        self.client.clientId,
                        messageCnt, (messageCnt === 1 ? '' : 's'),
                        payload.topic);
        }

        self.producer.send( [payload], (err, data) => {
          if (err)  {
            const messageBytes  = payload.messages.reduce( (sum,msg) => {
                                    return sum + msg.length;
                                  }, 0);

            console.error('*** %s._pushBatch(): topic[ %s ], ERROR[ %s ],'
                          +                     ' %d messages, %d bytes',
                          self.constructor.name, payload.topic,
                          err.message,
                          messageCnt, messageBytes);

            /* :TODO: At some error level, set a flow-control pause flag and
             *        emit an xoff flow control event on the bridge event bus
             *          e.g. this.xoff() or this.emit('xoff');
             */

            return subReject(err);
          }

          if (self.config.verbosity > 0) {
            console.error('=== %s: %s pushed %d message%s to topic[ %s ]',
                          self.constructor.name,
                          self.client.clientId,
                          messageCnt, (messageCnt === 1 ? '' : 's'),
                          payload.topic);
          }

          /* :TODO: If the flow-control pause flag is set, perhaps after some
           *        number of successful messages, reset the flow-control pause
           *        flag and emit an xon flow control event on the bridge
           *        event bus
           *          e.g. this.xon() or this.emit('xon');
           */

          return subResolve( data );
        });
      });
    });

    Promise.all( pending )
      .then(  resolve )
      .catch( reject );
  });
}
/* Private helpers }
 *****************************************************************************/

module.exports  = RemoteKafka;
