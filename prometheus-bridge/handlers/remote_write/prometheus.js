const Util          = require('util');
const Utils         = require('../../lib/utils');
const RemoteWriter  = require('./index.js');
const Http          = require('http');
const Https         = require('https');
const Protobuf      = require('protobufjs');
const SnappyJs      = require('snappyjs');

/**
 *  A concrete remote writer for a Prometheus remote write endpoint.
 *  @class  RemotePrometheus
 *  @extend RemoteWriter
 */
class RemotePrometheus extends RemoteWriter {
  static get defaults() {
    return Object.assign({}, super.defaults, {
      idle_timeout      : '5s',     // http default (keepAlive)     : 1s
      connect_timeout   : '5s',
      request_timeout   : '45s',    // http default (timeout)       : 0
      max_sockets       : -1,       // http default (maxSockets)    : Infinity
      max_free_sockets  : -1,       // http default (maxFreeSockets): 256

      batch_num_samples : 10000,    /* The number of samples to batch within
                                     * a single push
                                     */
      batch_timeout     : '15s',    /* The time to wait for additional
                                     * samples, up to `batch_num_samples`,
                                     * before sending (ms)
                                     */
      max_in_flight     : 128,      /* The maximum number of parallel,
                                     * in-flight pushes
                                     */
      max_resends       : 2,        // The maximum number of resend attempts
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                              The configuration data
   *                                              {Object};
   *  @param  config.url                          The URL of the remote
   *                                              endpoint {URL};
   *  @param  [config.idle_timeout = '5s']        The keep alive timeout (ms)
   *  @param  [config.connect_timeout = '5s']     The request connect timeout
   *                                              (ms) {Number | String};
   *  @param  [config.request_timeout = '15s']    The request timeout (ms)
   *                                              {Number | String};
   *  @param  [config.max_sockets = 128]          The total number of sockets
   *                                              that may be created before
   *                                              reuse {Number};
   *  @param  [config.max_free_sockets = 64]      The maximum number of sockets
   *                                              to keep around for potential
   *                                              reuse {Number};
   *  @param  [config.batch_num_samples = 10000]  Number of samples to batch
   *                                              within a single write
   *                                              {Number};
   *  @param  [config.batch_timeout = '5s']       The time to wait for
   *                                              additional samples, up to
   *                                              `batch_num_samples`, before
   *                                              sending (ms) {String|Number};
   *  @param  [config.max_in_flight = 128]        The maximum number of
   *                                              parallel, in-flight push
   *                                              requests {String|Number};
   *  @param  [config.dry_run = false]            If truthy, do NOT attempt to
   *                                              connect {Boolean};
   *  @param  [config.labels]                     Additional labels that should
   *                                              be applied to metrics
   *                                              processed by this writer
   *                                              {Object};
   *  @param  [config.verbosity = 0]              Debug verbosity {Number};
   *
   *  Supported URL format:
   *    %scheme%://host[:80][/database]
   *
   *    Typical 'scheme' values:
   *      prometheus
   */
  constructor( config ) {
    super( config );

    Object.defineProperties( this, {
      type      : { value: 'prometheus',enumerable: true },

      /*****************************
       * non-enumerable properties
       *
       * Http(s) protocol/module
       */
      _Proto    : {
        value   : (this.config.url.protocol === 'sprometheus:' ? Https : Http),
        writable: true,
      },

      // Pending queue and total number of samples enqueued within all slots
      _queue    : { value: {},                            writable: true },
      _enqueued : { value: 0,                             writable: true },

      // In-flight http push requests
      _inFlight : { value: [],                            writable: true },
      _flights  : { value: 0,                             writable: true },

      // Http(s) Request options
      _reqOpts  : { value: null,                          writable: true },

      // WriteRequest Protobuf handler
      _WriteRequest:{ value: null,                        writable: true },

      // Are we closing?
      _stopping : { value: null,                          writable: true },
    });
  }

  /**
   *  The current Http(s) Agent instance.
   *  @property agent
   *  @read-only
   */
  get agent() { return this._reqOpts ? this._reqOpts.agent : null }

  /**
   *  The count of samples that are pending / in-progress.
   *  @property pendingCount
   *  @read-only
   */
  get pendingCount()  { return this._inFlight.length }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.config.url}]`;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = _init( this );
    }

    return this._ready;
  }

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {
      this._stopping = new Promise( (resolve, reject) => {
        const reqOpts = this._reqOpts;

        if (reqOpts == null) { return resolve( false ) }

        /* If there are any samples in our queues that have not yet been
         * pushed, push them now regardless of in-flight limits.
         */
        if (this._enqueued > 0) {
          if (this.config.verbosity > 1) {
            console.error('%s.stop(): flush %d samples ...',
                          this.constructor.name, this._enqueued);
          }

          /* :NOTE: This push will be added to `this._inFlight` which we await
           *        below
           */
          _pushSamples( this );
        }

        // Wait for all in-flight pushes to complete
        Promise.all( this._inFlight )
          .then( res => {
            // Close the client
            reqOpts.agent.destroy();

            this._reqOpts.agent = null;
            this._reqOpts       = null;

            if (this.config.verbosity > 1) {
              console.error('%s.stop(): agent destroyed',
                            this.constructor.name);
            }

            resolve( true );
          })
          .catch( err => reject(err) );
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {

    if (! this.ready)   { await this._ready }
    if (this._stopping) { return 0 }

    if (this._squelchPush) {
      /* We have reached the maximum number of in-flight requests and are
       * waiting for one or more to complete. Until that time, additional
       * pushes are being squelched.
       */
      const threshold = Math.ceil( this.config.batch_num_samples * .25 );
      if (this._pushWarn == null) { this._pushWarn = 0 }
      else                        { this._pushWarn++ }

      if (this._pushWarn % threshold === 0) {
        console.warn('%s.push(): %d squelched awaiting in-flight completion',
                     this.constructor.name, this._pushWarn);
      }

      return 0;
    }

    if (this._delayedPush) {
      clearTimeout( this._delayedPush );
      this._delayedPush = null;
    }

    /* Enhance and enqueue `metrics` into `self._queue`, updating the
     * `self._enqueued` count
     */
    _enhanceAndEnqueue( this, metrics, scrape );

    if (this._enqueued < this.config.batch_num_samples) {
      /*
       * Our queues are not yet full -- wait for additional samples
       */
      this._delayedPush = setTimeout( () => {
        this._delayedPush = null;

        _pushSamples( this );

      }, this.config.batch_timeout );

    } else if (this._inFlight.length < this.config.max_in_flight) {
      /*
       * Our queues are full and we can send them immediately
       */
      await _pushSamples( this );

    } else {
      /*
       * Our queues are full but we have reached the maximum number of
       * in-flight push requests.
       *
       * Log a warning, squelch additional pushes and wait for at least one
       * of the current in-flight push requests to complete before
       * continuing this push and re-enabling additional pushes.
       */
      console.warn('%s.push(): %d / %d in-flight pushes, '
                   +  'delay this push and squelch additional pushes',
                   this.constructor.name,
                   this._inFlight.length, this.config.max_in_flight);

      this._squelchPush = true;

      /* Wait for at least one in-flight request to complete before
       * pushing this new set.
       */
      Promise.race( this._inFlight )
        .finally( () => {
          this._squelchPush = false;
          this._pushWarn    = null;

          _pushSamples( this );
        });
    }

    return metrics.length;
  }
}

/*****************************************************************************
 * Private classes and helpers {
 *
 */

/**
 *  A class to perform a stateful push to a Promscale remote write endpoint.
 *  @class  PromscalePush
 */
class PromscalePush {
  /**
   *  Create a new PromscalePush request.
   *  @constructor
   *  @param  remote    The Promscale remote write endpoint {Promscale};
   *  @param  flight    The flight number {Number};
   *  @param  entries   The timeseries entries {Array};
   *
   *  `entry` should have the form:
   *    { labels  : [ {name:'__name__', value: metricName}, ... ],
   *      samples : [ {timestamp: , value: }, ... ]
   *    }
   */
  constructor( remote, flight, entries ) {
    this.remote     = remote;
    this.flight     = flight;

    this.sampleCnt  = entries.reduce( (sum,entry) => {
                                        return sum + entry.samples.length;
                                      }, 0 );
    this.msg        = { timeseries: entries };
    this.enc        = remote._WriteRequest.encode( this.msg ).finish();
    this.buf        = SnappyJs.compress( this.enc );
    this.reqOpts    = Object.assign( {}, remote._reqOpts, {
      headers : {
        'Content-Encoding'                  : 'snappy',
        'Content-Type'                      : 'application/x-protobuf',
        'X-Prometheus-Remote-Write-Version' : '0.1.0',
        'Content-Length'                    : this.buf.length,
      },
    });
    this.req        = null;
    this.connected  = false;
    this.resendCnt  = 0;
    this.sendStart  = null;
    this.completed  = false;

    // Pre-bind request event handlers.
    this.__onReqSocket    = this._onReqSocket.bind( this );
    this.__onReqConnect   = this._onReqConnect.bind( this );
    this.__onReqResponse  = this._onReqResponse.bind( this );
    this.__onReqTimeout   = this._onReqTimeout.bind( this );
    this.__onReqError     = this._onReqError.bind( this );

    /*
    console.error('=== %s: %d samples :',
                  this, this.sampleCnt,
                  JSON.stringify( this.msg, null, '  '));

    console.error('=== %s: encoded %d b; compressed %d b ...',
                  this,
                  this.enc.length,
                  this.buf.length);
    // */

    this.promise    = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject  = reject;

      /* Initiate the send, which should end at some point by calling either:
       * - resolve() with the count of samples sent {Number};
       * - reject()  with an error {Error};
       */
      this.send();
    });
  }

  get config()    { return (this.remote ? this.remote.config : {}) }
  get Proto()     { return (this.remote ? this.remote._Proto : null) }
  get verbosity() { return (this.config ? this.config.verbosity : 0) }

  toString() {
    const parentName  = (this.remote ? this.remote.constructor.name : '');
    return `${parentName}:${this.constructor.name}#${this.flight}`
  }

  /**
   *  Generate a consistent informational string regarding samples.
   *  @method sampleString
   *
   *  @return An informational string {String};
   */
  sampleString() {
    const sampleStr =
      Util.format('%d sample%s, %d type%s, %s/%s Kb buffer',
                  this.sampleCnt,
                  (this.sampleCnt === 1 ? '' : 's'),
                  this.msg.timeseries.length,
                  (this.msg.timeseries.length === 1 ? '' : 's'),
                  (this.enc.length / 1024).toFixed(2),
                  (this.buf.length / 1024).toFixed(2));

    return sampleStr;
  }

  /**
   *  Generate a consistent informational string regarding the current state.
   *  @method infoString
   *  @param  [statusCode]  If provided, the status code of the HTTP response
   *                        {Number};
   *
   *  @return An informational string {String};
   */
  infoString( statusCode = -1 ) {
    if (statusCode < 0) {
      /* See if we have a request and response from which we can pull the
       * status code
       */
      if (this.req && this.req.res && this.req.res.statusCode != null) {
        statusCode = this.req.res.statusCode;
      }
    }

    const duration  = (Date.now() - this.sendStart);
    const infoStr   =
      Util.format('[ %d / %d attempt%s; statusCode %d; %s seconds ] '
                  + '%s; %d in-flight',
                  this.resendCnt + 1,
                  this.config.max_resends + 1,
                  (this.resendCnt === 1 ? '' : 's'),
                  statusCode,
                  (duration / 1000).toFixed(3),
                  this.sampleString(),
                  this.remote._inFlight.length - 1);

    return infoStr;
  }

  /**
   *  Attempt a (re)send
   *  @method send
   */
  send() {
    const isResend  = (this.req != null);

    //if (this.req) { this._destroyRequest() }

    this.sendStart = Date.now();

    // Create a request to send the protobuf representation of the metric
    this.req = this._createRequest();

    if (isResend) {
      console.error('=== %s: re-send [ %d / %d attempt%s ] %s; %d in-flight',
                    this,
                    this.resendCnt + 1,
                    this.config.max_resends + 1,
                    (this.resendCnt === 1 ? '' : 's'),
                    this.sampleString(),
                    this.remote._inFlight.length);

    } else if (this.verbosity > 0) {
      console.error('=== %s: send [ %d / %d attempt%s ] %s; %d in-flight',
                    this,
                    this.resendCnt + 1,
                    this.config.max_resends + 1,
                    (this.resendCnt === 1 ? '' : 's'),
                    this.sampleString(),
                    this.remote._inFlight.length);
    }

    // Send the protobuf
    this.req.end( this.buf );
    /*
    this.req.end( this.buf, () => {
      setTimeout( () => {
        if (this.req && ! this.completed) {
          console.error('%s req.end !completed: generate pseudo response ...',
                        this);

          // Completion with no response
          //
          // Create a pseudo-response we can pass to _onReqResponse()
          const res = {
            statusCode: 204,  // No content
            headers   : {},
          };

          this.req.emit( 'response', res );
        }
      }, 500);
    });
    // */
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Create a new request and attach request handlers.
   *  @method _createRequest
   *
   *  @return The new request {Request};
   *  @protected
   */
  _createRequest() {
    //if (this.req) { this._destroyRequest() }

    // Create a request to send the protobuf representation of the metric
    const req = this.Proto.request( this.reqOpts );

    this._attachReqHandlers( req );

    return req;
  }

  /**
   *  Handle the destruction of the current request which has likely timed out,
   *  possibly without any response at all.
   *  @method _destroyRequest
   *
   *  @return void
   *  @private
   */
  _destroyRequest() {
    if (this.req == null)   { return }

    this._detachReqHandlers();

    if (this.req.destroyed) { return }

    /* When a request is destroyed via `req.destroy()`, at some point the
     * request socket will be closed, emit a 'close' event and trigger
     * socketCloseListener() in _http_client.js. This event handler will:
     *    1) flush the socket;
     *    2) mark the request as destroyed (`req.destroyed = true`);
     *    3) check if `req.res` is set;
     *       a) req.res is set -- a response has been received
     *          i)   if the response has not completed (`!req.res.complete`),
     *               mark the response 'aborted'
     *          ii)  emit the 'close' event on the request;
     *          iii) emit the 'close' event on the response;
     *
     *       b) req.res is not set -- no response for this request
     *
     *          Problems arise if the socket has not had an error
     *          (`!req.socket._hadError`). In this case, the socket will be
     *          marked as having an error (`req.socket._hadError = true`) and
     *          a 'socket hang up' error will be emitted;
     *
     * The socketOnEnd() event handler in _http_client.js may also be triggered.
     * This event handler will:
     *    1) check if there has been no response (`!req.res`) and whether the
     *       associated socket has not yet handled an error
     *       (`!req.socket._hadError`);
     *
     *       In this case, the socket will be marked as having an error
     *       (`req.socket._hadError = true`) and a 'socket hang up' error will
     *       be emitted;
     *
     * The _destroy() helper in _http_client.js is invoked by the request
     * destroy() method. In some edge cases:
     *    1) if an explict error was passed in, it will be emitted as an 'error'
     *       event;
     *    2) if no explicit error was provided AND the request is not marked as
     *       'aborted' (`!req.aborted`), a 'socket hang up' error will be
     *       emitted;
     *
     *
     * Since we have already removed our error event listener, if an error
     * event is emitted, it will be uncaught and result in termination for an
     * unhandled error.
     */
    if (this.req.res == null) {
      /* This request has had no response -- ensure that any associated request
       * socket is properly marked.
       */
      const sock  = this.req.socket;

      if (sock && !sock._hadError) {
        // Tell the socketCloseListener() we already know about this error
        sock._hadError = true;
      }
    }

    /* Mark this request as 'aborted' to avoid any inadvertant error events, and
     * destroy the request.
     *
     *    req.abort() has been deprecated but it's flow was:
     *        req.aborted = true;
     *        on-next-tick: req.emit('abort');
     *        req.destroy();
     */
    this.req.aborted = true;

    // Destroy this request
    this.req.destroy();

    this.req = null;
  }

  /**
   *  Attach request event handlers.
   *  @method _attachReqHandlers
   *  @param  req   The new request to attach handlers to {Request};
   *
   *  @return void
   *  @protected
   */
  _attachReqHandlers( req ) {
    if (req) {
      req
        .on( 'socket',    this.__onReqSocket )
        .on( 'response',  this.__onReqResponse )
        .on( 'timeout',   this.__onReqTimeout )
        .on( 'error',     this.__onReqError )
    }
  }
  /**
   *  Detach request event handlers.
   *  @method _detachReqHandlers
   *
   *  @return void
   *  @protected
   */
  _detachReqHandlers() {
    if (this.req) {
      this.req
        .off( 'socket',   this.__onReqSocket )
        .off( 'response', this.__onReqResponse )
        .off( 'timeout',  this.__onReqTimeout )
        .off( 'error',    this.__onReqError )
    }
  }

  /**
   *  Handle a request 'socket' event.
   *  @method onReqSocket
   *  @param  sock  The connected socket {Socket};
   *
   *  @return The socket {Socket};
   *  @protected
   */
  _onReqSocket( sock ) {
    /* :NOTE: The timeout SHOULD be set to `this.config.connect_timeout` from
     *        the Agent timeout configuration.
     */
    if (this.verbosity > 1) {
      console.error('=== %s: socket [ readyState: %s, timeout: %s ]',
                    this, sock.readyState, sock.timeout);
    }

    /* Prepend a 'connect' listener to allow us to reset the socket timeout
     * once connected.
     */
    sock
      .prependOnceListener( 'connect', this.__onReqConnect );
  }

  /**
   *  Handle a request socket 'connect' event.
   *  @method onReqConnect
   *
   *  @return void
   *  @protected
   */
  _onReqConnect() {
    // Reset the socket timeout to `this.config.request_timeout`
    const sock  = this.req.socket;

    this.connected = true;

    sock.setTimeout( this.config.request_timeout );

    if (this.verbosity > 1) {
      console.error('=== %s: connected [ readyState: %s, timeout: %s ]',
                    this, sock.readyState, sock.timeout);
    }
  }

  /**
   *  Handle a request 'response' event.
   *  @method onReqResponse
   *  @param  res   The response {http.IncomingMessage};
   *
   *  @return void
   *  @protected
   */
  _onReqResponse( res ) {
    // assert( this.req.res === res );
    //this._detachReqHandlers();

    const infoMsg = this.infoString( res.statusCode );

    if (this.verbosity > 2) {
      const resp_len = (res.headers && res.headers['content-length']
                          ? parseInt( res.headers['content-length'], 10 )
                          : 0);

      console.error('=== %s: response attempt %d; statusCode %d; '
                    +                 'content-length %d; %d in-flight',
                    this,
                    this.resendCnt + 1,
                    res.statusCode,
                    resp_len,
                    this.remote._inFlight.length - 1);
      //console.error('=== %s.response: headers:', this, res.headers);

      if (resp_len > 0) {
        // Log any returned data
        res.once('data', buf => {
          console.error('=== %s: response data[ %s ]',
                        this, buf);
        });
      }
    }

    if (res.statusCode !== 200) {
      // Report ALL status codes that are not pure success
      const prefix  = (res.statusCode < 300 ? '===' : '***');
      const status  = (res.statusCode < 300 ? 'sent' : '!sent');

      console.error('%s %s: %s %s', prefix, this, status, infoMsg);

    } else if (this.verbosity > 0) {
      console.error('>>> %s: sent %s', this, infoMsg);
    }

    if (res.statusCode < 300) {
      // Success
      if (this.completed) {
        console.error('*** %s.response: post completion success[ %d ]',
                      this, res.statusCode);

      } else {
        this.completed = 'resolved';
        this.resolve( this.sampleCnt );
      }

    } else {
      // Failure
      if (this.completed) {
        console.error('*** %s.response: post completion failure[ %d ]',
                      this, res.statusCode);

      } else {
        const err = Util.format('send failed %s', infoMsg);

        if (this.verbosity > 2) {
          console.error('*** %s: %s', this, errMsg);
        }

        this.completed = 'rejected';
        this.reject( new Error( err ) );
      }
    }

    // Destroy the current request
    this._destroyRequest();
  }

  /**
   *  Handle a request 'timeout' event.
   *  @method _onReqTimeout
   *  @param  state     The current push state {Object};
   *
   *  @note   `this` is the controlling instance {RemoteTimescale};
   *
   *  @return void
   *  @private
   */
  _onReqTimeout() {
    if (this.req == null) { return }

    const timeoutType = (this.connected ? 'request' : 'connect');
    if (this.verbosity > 1 && this.req.socket) {
      console.error('=== %s.timeout: socket.readyState:',
                    this, this.req.socket.readyState);
    }

    // Destroy the current request
    this._destroyRequest();

    const errMsg  = Util.format('send %s timeout %s',
                                  timeoutType, this.infoString());

    /* :TODO: With timeouts, at some point we should probably signal a
     *        flow control 'xoff' event on the internal event bus:
     *          e.g. this.xoff() or this.emit('xoff');
     *
     *        We also need to be able to recognize when flow can be resumed so
     *        we can issue a flow control 'xon' event on the internal event
     *        bus.
     */
    if (this.resendCnt++ < this.config.max_resends) {
      // Attempt a resend
      if (this.verbosity > 0) {
        console.error('=== %s: %s : re-send ...', this, errMsg);
      }

      // Initiate a re-send
      return this.send();
    }

    // We've exhausted our resend limit
    if (this.completed) {
      console.error('*** %s.timeout: post completion timeout', this);

    } else {
      if (this.verbosity > 2) {
        console.error('*** %s: %s', this, errMsg);
      }

      this.completed = 'rejected';
      this.reject( new Error( errMsg ) );
    }
  }

  /**
   *  Handle a request 'error' event.
   *  @method _onReqError
   *  @param  state     The current push state {Object};
   *  @param  err       The error {Error};
   *
   *  @note   `this` is the controlling instance {RemoteTimescale};
   *
   *  @return void
   *  @private
   */
  _onReqError( err ) {
    const errorType   = (this.connected ? 'request' : 'connect');
    if (this.verbosity > 0 && this.req.socket) {
      console.error('*** %s.error: socket.readyState:',
                     this, this.req.socket.readyState);
    }

    this._detachReqHandlers();

    /* :TODO: Are there errors we can use to determine if flow control is
     *        needed?  If so, we could emit a flow control 'xoff' event on the
     *        internal event bus:
     *          e.g. this.xoff() or this.emit('xoff');
     *
     *        We would also need to be able to recognize when flow can be
     *        resumed so we can issue a flow control 'xon' event on the
     *        internal event bus.
     */
    const errMsg  = Util.format('send %s error [ %s ] : %s',
                                errorType, err.message, this.infoString());

    if (this.completed) {
      console.error('*** %s.error: post completion error:', this, errMsg);

    } else {
      if (this.verbosity > 2) {
        console.error('*** %s: %s', this, errMsg);
      }

      this.completed = 'rejected';
      this.reject( new Error( errMsg ) );
    }
  }
  /* Protected methods }
   **************************************************************************/
}

/**
 *  Initialize the Http(s) agent along with the ProtoBuf handler.
 *  @method _init
 *  @param  self      The controlling instance {RemotePrometheus};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _init( self ) {
  /* Generate a promise (_ready) that will be resolved once a connect has been
   * established and an initial test query completes.
   */
  return new Promise( (resolve, reject) => {

    // First, attempt to load our protobuf
    Protobuf.load(`${__dirname}/prometheus.proto`)
      .then( root => {
        return root.lookupType( 'prometheus.WriteRequest' );
      })
      .then( wr => {
        self._WriteRequest = wr;

        // Now that we have the Protobuf handler, create an Http(s) agent
        const httpConfig  = {
          keepAlive   : true,
          keepAliveMs : self.config.idle_timeout,
          /* Initial timeout, reset to `this.config.request_timeout` in
           * _onReqSocket() once a connection has been established.
           */
          timeout     : self.config.connect_timeout,
        };

        if (self.config.max_sockets > 0) {
          httpConfig.maxSockets      = self.config.max_sockets;
          httpConfig.maxTotalSockets = self.config.max_sockets;
        }
        if (self.config.max_free_sockets > 0) {
          httpConfig.maxFreeSockets  = self.config.max_free_sockets;
        }

        self._reqOpts = {
          agent   : new self._Proto.Agent( httpConfig ),
          host    : self.config.url.hostname,
          port    : (self.config.url.port || 80),
          path    : self.config.url.pathname,
          method  : 'POST',
        };

        if (self.config.verbosity > 1) {
          console.error('%s._init(): http%s Agent created for url[ %s ]',
                        self.constructor.name,
                        (self._Proto === Https ? 's' : ''),
                        self.config.url);
        }

        self.ready = true;
        resolve( self.ready );
      })
      .catch( err => {
        if (self.config.verbosity > 0) {
          console.error('*** %s: initialization failed: %s',
                        self.constructor.name, err);
        }

        reject( err );
      });
  });
}

/**
 *  Enhance and enqueue metrics.
 *  @method _enhanceAndEnqueue
 *  @param  self      The controlling instance {RemoteTimescale};
 *  @param  metrics   The incoming metrics to enhance and enqueue {Array};
 *  @param  [scrape]  The scrape instance responsible for `metrics`
 *                    {PromScrape};
 *
 *  Enhance the incoming metrics with any job name from `scrape` along with
 *  additional labels associated with `self`.
 *
 *  Once enhanced, add the metrics to name/label-based queue slots
 *  (`self._queue`), and update the total count of enqueued metrics
 *  (`self._enqueued`);
 *
 *  @return The number of metrics enqueued {Number};
 *  @private
 */
function _enhanceAndEnqueue( self, metrics, scrape ) {
  // Enhance `metrics` with the scrape job name and any additional labels
  metrics = self.enhance( metrics, scrape );

  /* Given an enhanced set of metrics, queue each into a name/label-based
   * (Sample._nameLabel) slot.
   *
   * Each slot represents a unique metric with a common label set and one
   * or more timestamp/value samples.
   *   { labels: [ {name:,      value:}, ... ],
   *     samples:[ {timestamp:, value:} , ... ]
   *   }
   *
   * These will become the 'timeseries' entries within the WriteRequest
   * protobuf sent to the Prometheus remote write endpoint.
   */
  metrics.forEach( metric => {
    const key   = metric._nameLabel;
    let   queue = self._queue[key];

    if (queue == null) {
      /* This is a new entry so we will first extract the set of labels
       * that SHOULD be shared by all entries in this slot.
       *
       * :NOTE: We include a '__name__' meta-label that is required by
       *        the Prometheus remote write endpoint to pass the metric
       *        name.
       */
      queue = {
        labels: Object.entries( metric.labels )
                  .reduce( (ar, [key,val]) => {
                      ar.push({name:key, value:String(val)});
                      return ar;
                    }, [ {name:'__name__', value:metric.name}] ),
        samples: [],
      };

      /* :XXX: For duplicate detection, add a non-enumerable reference to
       *       this original metric.
       *
      Object.defineProperties( queue, { _metric: { value: metric } });
      // */

      self._queue[key] = queue;
    }

    /* :XXX: Does this timestamp already exist?
     *       If so, we can expect a 'duplicate entry' error from
     *       PromScale.
     *
    const entry = queue.samples
                    .find( entry => entry.timestamp == metric.timestamp );
    if (entry != null) {
      console.error('*** %s.push(): Duplicate timestamp [ %d ]: '
                    + 'original[ %s ], new[ %s ]',
                      self.constructor.name,
                      metric.timestamp,
                      queue._metric, metric);
    }
    // */

    // Include this metric's timestamp/value in the set of metric samples
    queue.samples.push( {
      timestamp : metric.timestamp,
      value     : metric.value,
    } );
  });

  /* Update the total number of samples in the pending queues
   *    self._enqueued =
   *      Object.values(self._queue)
   *              .reduce( (sum,entry) => sum + entry.samples.length, 0);
   */
  self._enqueued += metrics.length;

  return metrics.length;
}

/**
 *  Push all samples in the current queues and reset the queues.
 *  @method _pushSamples
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  Promscale remote write endpoint requires:
 *    Method  : POST
 *    Headers :
 *      Content-Encoding:                   snappy
 *      Content-Type:                       application/x-protobuf
 *      X-Prometheus-Remote-Write-Version:  0.1.x
 *
 *    Body    : (protobuf encoded, snappy compressed)
 *      WriteRequest
 *        TimeSeries[]
 *          Labels  []Label   name  string, value     string
 *          Samples []Sample  value double, timestamp int64
 *
 *      The metric name is passed as a special '__name__' label.
 *
 *    ref: https://github.com/timescale/promscale/blob/7a8e6aebc99f19246c476e65ba19b07485e5f3e8/pkg/api/write.go#L121
 *         https://github.com/timescale/promscale/blob/7a8e6aebc99f19246c476e65ba19b07485e5f3e8/pkg/pgmodel/ingestor.go#L14
 *
 *  @note   This promise will be added to `self._inFlight` and removed once the
 *          promise has resolved/rejects;
 *
 *  @return A promise for results {Promise};
 *          - on success, the number of samples {Number};
 *          - on failure, an error {Error};
 *  @private
 */
function _pushSamples( self ) {
  const queue     = self._queue;
  const sampleCnt = self._enqueued;

  /* Now that we have a local copy, empty the current queue.
   *
   *  :NOTE: If this were within the Promise, it COULD lead to queue entries
   *         being handled by two separate event handlers resulting in duplicate
   *         entries being pushed to the Prometheus remote write endpoint.
   */
  self._queue    = {};
  self._enqueued = 0;

  if (sampleCnt < 1)  { return Promise.resolve( 0 ) }

  // Create a stateful push request
  const push  = new PromscalePush( self, self._flights++,
                                   Object.values( queue ) );

  // Add this pending request to the in-flight queue
  self._inFlight.push( push );

  push.promise
    .then( numSamples => {
      // Success -- update the sample count
      self._sampleCount += numSamples;

      return numSamples;
    })
    .catch( err => {
      /* Catch any rejection to avoid an 'Unhandled Rejection'
      console.error('*** %s._pushSamples(): send-%d failure: %s',
                    self.constructor.name, flight, err.message);
      // */
      return err;
    })
    .finally( () => {
      /* Upon completion (success or error), remove this push from the
       * in-flight queue.
       */
      const index = self._inFlight.findIndex( entry => entry === push );
      if (index >= 0) {
        self._inFlight.splice( index, 1 );
      }
    });

  return push.promise;
}

/* Private classes and helpers }
 *****************************************************************************/
module.exports  = RemotePrometheus;
