const Utils   = require('../../lib/utils');
const Sample  = require('../../lib/sample');

/**
 *  The base class for a remote write endpoint to which Prometheus metrics will
 *  be pushed.
 *  @class  RemoteWriter
 *
 *  @event  'sample', msg   : received when a metric is generated, with `msg`
 *                            having the form:
 *                              { self  : the src    instance {AsyncSource |
 *                                                             PrompScrape},
 *                                sample: the sample instance {Sample},
 *                              }
 *  @event  'error', msg    : emitted on remote/connection error with `msg`
 *                            having the form:
 *                              { self  : the src   instance {AsyncSource |
 *                                                            RemoteWriter|
 *                                                            PromScrape},
 *                                error : the error instance {Error},
 *                              }
 */
class RemoteWriter {
  static get defaults() {
    return {
      /**
       *  Debug verbosity
       *  @prop verbosity {Number};
       */
      verbosity : 0,
    };
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                    The configuration data {Object};
   *  @param  config.bridge             The primary bridge / event-bus
   *                                    {Bridge};
   *  @param  [config.dry_run = false]  If truthy, do NOT attempt to connect
   *                                    {Boolean};
   *  @param  [config.labels]           Additional labels that should be
   *                                    applied to metrics processed by this
   *                                    writer {Object};
   *  @param  [config.verbosity = 0]    Debug verbosity {Number};
   */
  constructor( config ) {
    if (this.constructor.defaults) {
      // Extend 'config' using class defaults
      config = Object.assign( {}, this.constructor.defaults, config || {} );
    }

    /* Expand any values that contain an expandable prefix (e.g. 'file:') and
     * then convert the values of any timestamp/interval duration keys to
     * milliseconds.
     *
     * For example:
     *    {
     *      key1        : 'file:/path/to/file.txt', => contents of named file
     *      key_timeout :  '5m',                    => 300,000
     *      key_interval:  '30s',                   =>  30,000
     *    }
     */
    config = Utils.convertDurations( Utils.expandValues( config ) );

    if (config.url == null) {
      throw Utils.makeError(this, "Missing required configuration 'url'");
    }
    if (config.bridge == null) {
      throw Utils.makeError(this, "Missing required configuration 'bridge'");
    }

    Object.defineProperties( this, {
      /**
       *  The instance configuration.
       *  @property config {Object};
       */
      config      : { value: config,  enumerable: true },

      /**
       *  Simple ready boolean.
       *  @property __ready {Boolean};
       *  @protected
       */
      __ready     : { value: false,                     writable: true },

      /**
       *  Ready promise.
       *  @property _ready {Promise};
       *  @protected
       */
      _ready      : { value: null,                      writable: true },

      /**
       *  Total sample count
       *  @property _sampleCount {Number};
       *  @protected
       */
      _sampleCount: { value: 0,                         writable: true },

      /**
       *  Bridge event listeners. These are attached/detatched via
       *  _attach/detachListeners().
       *  @property _listeners {Object};
       *  @protected
       *
       *  Keys are event names, values are event listeners.
       */
      _listeners  : { value: {
                        sample  : this._onSample.bind( this ),
                      }
                    },
    });
  }

  /**
   *  The configured bridge.
   *  @property bridge {Bridge}
   *  @read-only
   */
  get bridge()      { return this.config.bridge }

  /**
   *  The ready status.
   *  @property ready
   *
   *  When set to:
   *  - a truthy value when `config.dry_run` is not set, this will cause
   *    event listeners to be attached;
   *    to this writer;
   *  - a non-truthy value (or when `config.dry_run` is set), this will cause
   *    event listener to be detached;
   */
  get ready()     { return this.__ready }
  set ready(val)  {
    this.__ready = val;

    if (this.__ready && ! this.config.dry_run) {
      // Attach event listenerS
      this._attachListeners();

    } else {
      // Detach event listenerS
      this._detachListeners();
    }
  }

  /**
   *  The count of samples that have been processed.
   *  @property sampleCount
   *  @read-only
   */
  get sampleCount() { return this._sampleCount }

  /**
   *  The count of samples that are pending / in-progress (-1 == not-reported).
   *  @property pendingCount
   *  @read-only
   */
  get pendingCount(){ return -1 }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[abstract]`;
  }

  /**
   *  Update the given set of metrics to include any scrape and/or
   *  remote-specific labels, ensuring the labels are in sorted order in both
   *  the `labels` object and `_raw` string.
   *  @method enhance
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  scrape    The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return The updated set of metrics {Array};
   */
  enhance( metrics, scrape ) {
    if (! Array.isArray(metrics)) { metrics = [ metrics ] }

    if (scrape == null || this.config.labels == null) {
      // Nothing to enhance
      return metrics;
    }

    const job_name      = (scrape ? scrape.name : null);
    const enhanced      = [];
    const extra_labels  = {job:job_name};

    if (this.config.labels != null) {
      // Include explicitly configured, remote-specific labels
      Object.entries(this.config.labels).forEach( ([key,val]) => {
        extra_labels[ key ] = val;
      });
    }

    metrics.forEach( metric => {
      // Include the scrape job name as an implicit label
      if (job_name && metric.labels.job == null) {
        extra_labels.job = job_name;

      } else {
        extra_labels.job = undefined;
      }

      const enhanced_metric = new Sample({
        name        : metric.name,
        labels      : metric.labels,
        value       : metric.value,
        metric_class: metric.metric_class,
        timestamp   : metric.timestamp,
        extra_labels: extra_labels,
      });

      enhanced.push( enhanced_metric );
    });

    return enhanced;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    throw Utils.makeError(this, 'start() must be implemented');
  }

  /**
   *  Pause this writer.
   *  @method pause
   *
   *  The default pause simply emits an transmit off (xoff) event on the bridge
   *  event bus.
   *
   *  @return A promise resolved when this writer has been paused {Promise};
   */
  async pause() {
    // Emit a flow control transmit off (xoff) event on the bridge event bus.
    this.xoff();

    return true;
  }

  /**
   *  Resume this writer.
   *  @method resume
   *
   *  The default resume simply emits an transmit on (xon) event on the bridge
   *  event bus.
   *
   *  @return A promise resolved when this writer has been resumed {Promise};
   */
  async resume() {
    // Emit a flow control transmit on (xon) event on the bridge event bus.
    this.xon();

    return true;
  }

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    throw Utils.makeError(this, 'stop() must be implemented');
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {
    throw Utils.makeError(this, 'push() must be implemented');
  }

  /**
   *  Emit an event to the bridge/event-bus.
   *  @method emit
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  payload     The event payload {Any};
   *
   *  On 'sample' events:
   *    - update the sample count;
   *    - generate a message with the form {self:this, sample:payload};
   *
   *  On 'error' events:
   *    - generate a message with the form {self:this, error:payload};
   *
   *  On all other events, use `payload` as the message;
   *
   *  @return Result {Boolean};
   */
  emit( eventName, payload ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'emit(): no bridge instance');
    }

    let msg = payload;
    switch( eventName ) {
      case 'sample':
        //this._sampleCount++;
        msg = { self:this, sample:payload };
        break;

      case 'error':
        msg = { self:this, error:payload };
        break;
    }

    return this.bridge.emit( eventName, msg );
  }

  /**
   *  Subscribe to an event on the bridge/event-bus.
   *  @method on
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  on( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'on(): no bridge instance');
    }

    return this.bridge.on( eventName, listener );
  }

  /**
   *  Subscribe to a single triggering of an event on the bridge/event-bus.
   *  @method once
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  once( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'once(): no bridge instance');
    }

    return this.bridge.once( eventName, listener );
  }

  /**
   *  Unsubscribe from an event on the bridge/event-bus.
   *  @method off
   *  @param  eventName   The name of the event {String | Symbol};
   *  @param  listener    The event listener callback {Function};
   *
   *  @return The bridge instance {Bridge};
   */
  off( eventName, listener ) {
    if (this.bridge == null) {
      return Utils.makeError(this, 'off(): no bridge instance');
    }

    return this.bridge.off( eventName, listener );
  }

  /**
   *  Emit a flow control 'xon' event (resume / transmit-on).
   *  @method xon
   */
  xon() {
    this.emit('xon');
  }

  /**
   *  Emit a flow control 'xoff' event (pause / transmit-off).
   *  @method xoff
   */
  xoff() {
    this.emit('xoff');
  }

  /**************************************************************************
   * Protected listeners {
   */

  /**
   *  Event listener for the 'sample' event
   *  @method _onSample
   *  @param  msg         The incoming message {Object};
   *  @param  msg.self    The async source instance {AsyncSOurce};
   *  @param  msg.sample  The incoming sample {Sample};
   *
   *  @protected
   */
  _onSample( msg ) {
    const src     = msg.self;
    const sample  = msg.sample;

    if (this.config.verbosity > 3) {
      console.error('=== %s: process %s sample: %s', this, src, sample);
    }

    this.push( sample, src )
      .then( res => {
        if (this.config.verbosity > 2) {
          console.error('=== %s: pushed sample: %s',
                        this, sample);
        }
        return res;
      })
      .catch( err => {
        if (this.config.verbosity > 2) {
          console.error('*** %s: Error sending sample[ %s ]:',
                        this, JSON.stringify(sample), err);
        } else {
          console.error('*** %s: Error sending sample[ %s ]: %s',
                        this, JSON.stringify(sample), err.message);
        }
      });
  }

  /**
   *  Attach event listeners once this writer is ready.
   *  @method _attachListeners
   *
   *  @return `this` for a fluent interface;
   *  @protected
   */
  _attachListeners() {
    if (this.bridge) {
      // Attach event listeners
      for (const [name, listener] of Object.entries( this._listeners )) {
        this.bridge.on( name, listener );
      }
    }

    return this;
  }

  /**
   *  Detach event listeners when this writer becomes non-ready.
   *  @method _detachListeners
   *
   *  @return `this` for a fluent interface;
   *  @protected
   */
  _detachListeners() {
    if (this.bridge) {
      // Detach event listeners
      for (const [name, listener] of Object.entries( this._listeners )) {
        //this.config.bridge.removeListener( name, listener );
        this.bridge.off( name, listener );
      }
    }

    return this;
  }

  /* Protected listeners }
   **************************************************************************/
}

module.exports = RemoteWriter;
