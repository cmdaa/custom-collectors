const Util          = require('util');
const Utils         = require('../../lib/utils');
const RemoteWriter  = require('./index.js');
const Pg            = require('pg');

/**
 *  A concrete remote writer for a PostegreSQL/TimescaleDb endpoint that
 *  includes the `pg_prometheus` plugin.
 *  @class  RemoteTimescale
 *  @extend RemoteWriter
 *
 *  :TODO:  Add flow control
 */
class RemoteTimescale extends RemoteWriter {
  static get defaults() {
    return Object.assign({}, super.defaults, {
      user              : 'postgres',   // PostgreSQL user name
      password          : '',           // Password or 'file:%path%'
      database          : 'postgres',   // PostgreSQL database
      table             : 'metrics',    // Prefix for tables

      max_clients       : 10,           // pg default: 10

      connect_timeout   : '10s',        // pg default: 0
      idle_timeout      : '30s',        // pg default: 10s
      statement_timeout : '30s',        // pg default: 0

      batch_num_samples : 250,          /* The number of samples to batch
                                         * within a single INSERT.
                                         */
      batch_timeout     : '500ms',      /* The time to wait for additional
                                         * samples, up to `batch_num_samples`,
                                         * before sending (ms)
                                         */

      init_timeout      : '5s',         /* During initialization, the time to
                                         * wait between
                                         * connection/initialization attempts
                                         * (converted to ms).
                                         */

      ssl                 : false,      // Use ssl for database connection?
      reject_unauthorized : true,       // Reject (or accept) unauthorized CAs?
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                              The configuration data
   *                                              {Object};
   *  @param  config.url                          The URL of the remote
   *                                              endpoint {URL};
   *  @param  [config.user = 'postgres']          User {String};
   *  @param  [config.password = '']              Password, either direct or
   *                                              indirect (via 'file:%path%')
   *                                              {String};
   *  @param  [config.database = 'postgres']      Database name {String};
   *  @param  [config.table = 'metrics']          Table prefix {String};
   *  @param  [config.max_clients = 10]           Maximum number of parallel
   *                                              clients (pg default: 10)
   *                                              {Number};
   *  @param  [config.connect_timeout = '10s']    Connection timeout
   *                                              (pg default: 0)
   *                                              {String|Number};
   *  @param  [config.idle_timeout = '30s']       The idle connection timeout
   *                                              (pg default: 10s)
   *                                              {String|Number};
   *  @param  [config.statement_timeout = '30s' ] The timeout for insert/query
   *                                              statements (pg default: 0)
   *                                              {String|Number};
   *  @param  [config.batch_num_samples = 250]    Number of samples to batch
   *                                              within a single INSERT
   *                                              {Number};
   *  @param  [config.batch_timeout = '500ms']    The time to wait for
   *                                              additional samples, up to
   *                                              `batch_num_samples`, before
   *                                              sending (ms) {String|Number};
   *  @param  [config.init_timeout = '5s' ]       The time between consecutive
   *                                              attempts to connect and
   *                                              initialize (converted to ms)
   *                                              {String|Number};
   *  @param  [config.ssl = false]                If true, use ssl for database
   *                                              connections {Boolean};
   *  @param  [config.reject_unauthorized = true] Should unauthorized CAs be
   *                                              rejected {Boolean};
   *  @param  [config.dry_run = false]            If truthy, do NOT attempt to
   *                                              connect {Boolean};
   *  @param  [config.labels]                     Additional labels that should
   *                                              be applied to metrics
   *                                              processed by this writer
   *                                              {Object};
   *  @param  [config.verbosity = 0]              Debug verbosity {Number};
   *
   *  Supported URL format:
   *    %scheme%://[dbuser:password@]host:5432[/database]
   *
   *    Typical 'scheme' values:
   *      postgres
   *      postgresql
   *      timescale
   *      timescaledb
   */
  constructor( config ) {
    super( config );

    Object.defineProperties( this, {
      type  : { value: 'timescale', enumerable: true },

      pool  : { value: null,        writable: true },
      queue : { value: [],          writable: true },
    });
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.config.url}]`;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    if (this._ready == null) {
      this._ready = _initDb( this );
    }

    return this._ready;
  }

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    if (this._stopping == null) {

      this._stopping = new Promise( (resolve, reject) => {
        if (this.pool == null) {
          // No pool/connection
          return resolve( false );
        }

        // Ensure all pending pushes have been completed
        const pending = [];
        if (this._pendingPush) {
          // Push the current queue immediately
          clearTimeout( this._pendingPush );

          if (this.queue.length > 0) {
            if (this.config.verbosity > 1) {
              console.error('%s.stop(): flush %d samples ...',
                            this.constructor.name, this.queue.length);
            }

            pending.push( _pushQueue( this ) );
          }
        }

        Promise.all( pending )
          .then( res => {
            // End the pool
            this.pool.end( () => {
              this.pool = null;

              if (this.config.verbosity > 1) {
                console.error('%s.stop(): pool ended', this.constructor.name);
              }

              resolve( true );
            });
          })
          .catch( err => reject(err) );
      });

      // On completion, reset both '_ready' and '_stopping'
      this._stopping.finally( () => {
        this._ready = this._stopping = null;
      } );
    }

    return this._stopping;
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {

    // Enhance `metrics` with the scrape job name and any additional labels
    metrics = this.enhance( metrics, scrape );

    if (this._pendingPush) {
      clearTimeout( this._pendingPush );
      this._pendingPush = null;
    }

    // Push the Prometheus-formatted version of these metrics onto our queue
    metrics.forEach( metric => this.queue.push( metric.toString() ) );

    if (! this.ready)             { await this._ready }

    if (this.queue.length > this.config.batch_num_samples) {
      // Our queue is full -- send it immediately
      await _pushQueue( this );

    } else {
      // Our queue is not yet full -- wait for additional samples
      this._pendingPush = setTimeout( () => {
        _pushQueue( this )
          .catch( err => { /* squelch */ } );

      }, this.config.batch_timeout );
    }

    return this.queue.length;
  }
}

/*****************************************************************************
 * Private helpers {
 *
 */


/**
 *  Initialize the Postgres client and ensure the required extensions and
 *  tables are enabled and created.
 *  @method _initDb
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  @return A ready promise {Promise};
 *  @private
 */
function _initDb( self ) {
  /* Generate a promise (_ready) that will be resolved once a connect has been
   * established and an initial test query completes.
   */
  return new Promise( (resolve, reject) => {

    // Connect to the database and check if the target table exists
    _connectDb( self )
      .then( exists => {

        if (exists) {
          // The table exists
          return true;
        }

        // The table does not yet exist -- activate
        return _activateDb( self );
      })
      .then( res => {
        self.ready = true;

        resolve( res );
      })
      .catch( err => {
        if (self.config.verbosity > 0) {
          console.error('*** %s: initialization failed: %s',
                        self.constructor.name, err);
        }

        reject( err );
      });
  });
}

/**
 *  Pause for the given number of seconds.
 *  @method _pause
 *  @param  timeout The desired pause time (seconds) {Number};
 *
 *  @return A promise resolved when the specified time has elapsed {Promise};
 *  @private
 */
function _pause( timeout ) {
  return new Promise( (resolve, reject) => {
    setTimeout( () => { resolve(true) }, timeout );
  });
}

/**
 *  Create a connection/pool and wait until we can perform a select on the
 *  target database for the target table.
 *  @method _connectDb
 *  @param  self    The controlling instance {RemoteTimescale};
 *
 *  @return A promise for results {Promise};
 *          - on success, an indication of whether the target table exists
 *                        {Boolean};
 *          - on failure, an error {Error};
 *  @private
 */
async function _connectDb( self ) {
  const url       = self.config.url;
  const pgConfig  = {
    host                    : url.hostname,
    port                    : url.port,
    user                    : url.username || self.config.user,
    password                : url.password || self.config.password,
    database                : (url.pathname.length > 1
                                ? url.pathname.slice(1)
                                : self.config.database),

    max                     : self.config.max_clients,
    connectionTimeoutMillis : self.config.connect_timeout,
    idleTimeoutMillis       : self.config.idle_timeout,
    statement_timeout       : self.config.statement_timeout,

    ssl                     : !!self.config.ssl,
    rejectUnauthorized      : !!self.config.reject_unauthorized,
  };
  const sql     = `SELECT to_regclass( 'public.${self.config.table}' )`;
  const timeout = self.config.init_timeout;
  const makeErr = (err) => Utils.makeError( self, err );
  let   pool    = null;
  let   exists  = false;

  if (pgConfig.user     == null) { throw makeErr('Missing database user') }
  if (pgConfig.password == null) { throw makeErr('Missing database password')}
  if (pgConfig.database == null) { throw makeErr('Missing database name') }

  /*
  console.error('_connectDb(): url.password   [ %s ]', url.password);
  console.error('_connectDb(): config.password[ %s ]', self.config.password);

  console.error('_connectDb(): pgConfig: %s',
                JSON.stringify(pgConfig, null, 2));
  // */

  while (!pool) {
    pool = new Pg.Pool( pgConfig );

    /* Check if the target table exists
     *
     *    Fastest PostgreSQL 9.4+ option:
     *      `SELECT to_regclass( 'public.${self.config.table}')`
     */
    try {
      const res = await pool.query( sql );

      /*
      console.error('%s._connectDb(): res:',
                    self.constructor.name, res);
      // */

      exists = (res && res.rowCount > 0 && res.rows[0].to_regclass != null);

      // Keep this pool connection
      self.pool = pool;

    } catch(ex) {
      console.error('*** %s._connectDb(): ERROR: %s (retry in %d ms) ...',
                    self.constructor.name, ex, timeout);

      // Close this pool connection to reset our metadata and retry
      pool.end();
      pool = null;
    }

    // Pause a bit (even on success)
    await _pause( timeout );
  }

  return exists;
}

/**
 *  Enable the required extensions and create our metrics table.
 *  @method _activateDb
 *  @param  self    The controlling instance {RemoteTimescale};
 *
 *    CREATE EXTENSION IF NOT EXISTS pg_prometheus;
 *    CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
 *    SELECT create_prometheus_table( %table% )
 *
 *  @return A promise for results {Promise};
 *          - on success, results {Mixed};
 *          - on failure, an error {Error};
 *  @private
 */
async function _activateDb( self ) {
  const table   = self.config.table;
  const sql     = [
    'CREATE EXTENSION IF NOT EXISTS pg_prometheus',
    'CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE',
    `SELECT create_prometheus_table( '${table}' )`,
  ];

  if (self.pool == null)  { return Utils.makeError( self, 'pool closed') }

  const client  = await self.pool.connect();
  let   res;

  try {
    const pending = sql.map( async (sql) => {
      res = await client.query( sql );

      /*
      console.error('=== %s._activeDb(): sql[ %s ]:',
                    self.constructor.name, sql, res);
      // */
    });

    await Promise.all( pending );

  } finally {
    if (client) { client.release() }
  }

  return res;
}

/**
 *  Push all samples in the current queue and reset the queue.
 *  @method _pushQueue
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _pushQueue( self ) {
  const pending   = [];
  const sampleCnt = self.queue.length;

  if (sampleCnt < 1)  { return Promise.resolve( 0 ) }

  /*
  console.error('%s._pushQueue(): %d sample%s: ...',
                self.constructor.name,
                sampleCnt, (sampleCnt === 1 ? '' : 's'));
  // */

  while (self.queue.length > 0) {
    pending.push( _pushBatch(self) );
  }

  const promise = Promise.all( pending );

  promise
    .then( res => {
      const batchCnt  = res.length;

      self._sampleCount += sampleCnt;

      if (self.config.verbosity > 1) {
        console.error('%s._pushQueue(): %d sample%s in %d batch%s',
                      self.constructor.name,
                      sampleCnt, (sampleCnt === 1 ? '' : 's'),
                      batchCnt,  (batchCnt  === 1 ? '' : 'es'));
      }

      return batchCnt;
    });

  return promise;
}

/**
 *  Push the next batch of queued samples, removing them from the queue.
 *  @method _pushBatch
 *  @param  self      The controlling instance {RemoteTimescale};
 *
 *  @return A promise for results {Promise};
 *  @private
 */
function _pushBatch( self ) {
  return new Promise( (resolve, reject) => {
    const fullCnt     = self.queue.length;

    // Extract the samples for this batch
    const samples     = self.queue.splice( 0, self.config.batch_num_samples );
    const sampleCnt   = samples.length;

    if (sampleCnt < 1) { return resolve( null ) }

    if (self.config.verbosity > 1) {
      console.error('%s._pushBatch(): %d / %d sample%s: ...',
                    self.constructor.name,
                    sampleCnt, fullCnt, (sampleCnt === 1 ? '' : 's'));
    }

    // Create query parameters/slots and the associated SQL
    const slots = samples.map( (val,idex) => `( $${idex+1} )` ).join(',');
    const sql   = `INSERT INTO ${self.config.table} VALUES ${slots}`;

    /*
    console.error('%s._pushBatch(): sql[ %s ]',
                  self.constructor.name, sql);
    console.error('%s._pushBatch(): samples:',
                  self.constructor.name, JSON.stringify( samples, null, 2 ) );
    // */

    // Insert the queued samples.
    self.pool.query( sql, samples )
      .then(  res => {
        /*
        console.error('%s._pushBatch(): pushed %d sample%s:',
                      self.constructor.name,
                      sampleCnt, (sampleCnt === 1 ? '' : 's'),
                      res);
        // */

        resolve( res );
      })
      .catch( err => {
        /*
        console.error('*** %s._pushBatch(): ERROR:',
                      self.constructor.name, err);
        // */

        reject( err );
      });
  });
}
/* Private helpers }
 *****************************************************************************/

module.exports  = RemoteTimescale;
