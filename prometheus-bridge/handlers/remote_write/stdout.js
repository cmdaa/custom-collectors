const RemoteWriter  = require('./index.js');

/**
 *  A concrete remote writer to stdout.
 *  @class  RemoteStdout
 *  @extend RemoteWriter
 */
class RemoteStdout extends RemoteWriter {
  /*
  static get defaults() {
    return Object.assign({}, super.defaults, {
      idle_timeout      : '30s',  // http default (keepAlive): 1s
      request_timeout   : '5s',   // http default (timeout)  : 0
    });
  }
  // */

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                    The configuration data {Object};
   *  @param  [config.verbosity = 0]    Debug verbosity {Number};
   *  @param  [config.kb_flow = false]  If truthy, initialize keyboard-based
   *                                    flow-control monitoring
   *                                    (ctl-s : xoff, ctl-q : xon) {Boolean};
   */
  constructor( config ) {
    super( config );

    if (this.config.dry_run) {
      // this._ready = Promise.reject( 'dry-run' ); // super-class
      return;
    }

    if (this.config.kb_flow) {
      this._initKbFlow();
    }
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}`;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    if (! this.ready) {
      this.ready  = true;
    }

    return true;
  }

  /**
   *  Pause this writer.
   *  @method pause
   *
   *  @return A promise resolved when this writer has been paused {Promise};
   */
  async pause() {
    // pause: xoff => stop

    // Emit a flow control transmit off (xoff) event on the bridge event bus.
    this.xoff();

    this.stop();
  }

  /**
   *  Resume this writer.
   *  @method resume
   *
   *  @return A promise resolved when this writer has been resumed {Promise};
   */
  async resume() {
    // resume: start => xon
    await this.start();

    /* AFTER a successful start,
     * Emit a flow control transmit on (xon) event on the bridge event bus.
     */
    this.xon();
  }

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    if (this.ready) {
      this.ready = false;
    }

    return true;
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {
    if (! this.ready) { return false }

    // Enhance `metrics` with the scrape job name and any additional labels
    metrics = this.enhance( metrics, scrape );

    this._sampleCount += metrics.length;

    if (this.config.verbosity > 1) {
      console.log('%s.push(): %d metric%s: %s',
                  this.constructor.name,
                  metrics.length, (metrics.length === 1 ? '' : 's'),
                  JSON.stringify(metrics, null, 2) );

    } else {
      console.log('%s.push(): %d metric%s: %s',
                  this.constructor.name,
                  metrics.length, (metrics.length === 1 ? '' : 's'),
                  JSON.stringify(metrics) );

    }
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Initialize keyboard-based flow-control monitoring.
   *  @method _initKbFlow
   *
   *  On ctl-s  => pause()  => xoff
   *     ctl-q  => resume() => xon
   *
   *  @return this for a fluent interface;
   *  @protected
   */
  _initKbFlow() {
    const Readline  = require('readline');

    // Setup to receive keypress events
    Readline.emitKeypressEvents( process.stdin );
    process.stdin.setRawMode( true );

    this.__handleKeypress = this._handleKeypress.bind( this );

    process.stdin.on('keypress', this.__handleKeypress);
  }

  /**
   *  Handle a keypress event.
   *  @method _handleKeypress
   *  @param  str     The keypress {String};
   *  @param  key     Key information {Object};
   *
   *  @return void
   *  @protected
   */
  _handleKeypress( str, key ) {
    const log_ignore  = () => console.error('=== ignore:', key);

    switch( key.name ) {
      case 's':
        if (key.ctrl) { // flow-control : transmit off / xoff
          console.error('=== ctl-s => pause ...');
          this.pause()
            .then( res => console.error('=== ctl-s : paused  / xoff:', res) )
            .catch(err => console.error('*** ctl-s : pause  failed:', err));

        } else {
          log_ignore();
        }
        break;

      case 'q':
        if (key.ctrl) { // flow-control : transmit on  / xon
          console.error('=== ctl-q => resume ...');
          this.resume()
            .then( res => console.error('=== ctl-q : resumed / xon:', res) )
            .catch(err => console.error('*** ctl-q : resume failed:', err));

        } else {
          log_ignore();
        }
        break;

      case 'c':
        if (key.ctrl) {
          console.error('=== ctl-c : quit');
          process.exit( 0 );

        } else {
          log_ignore();
        }
        break;

      default:
        log_ignore();
        break;
    }
  }

  /* Protected methods }
   **************************************************************************/
}

module.exports  = RemoteStdout;
