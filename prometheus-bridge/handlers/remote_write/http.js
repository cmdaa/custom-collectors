//const Util          = require('util');
const RemoteWriter  = require('./index.js');
const Http          = require('http');
const Https         = require('https');
const Utils         = require('../../lib/utils');

/**
 *  A concrete remote writer for an http/https endpoint.
 *  @class  RemoteHttp
 *  @extend RemoteWriter
 *
 *  :TODO:  Add flow control
 */
class RemoteHttp extends RemoteWriter {
  static get defaults() {
    return Object.assign({}, super.defaults, {
      idle_timeout      : '30s',  // http default (keepAlive): 1s
      request_timeout   : '5s',   // http default (timeout)  : 0
    });
  }

  /**
   *  Create a new instance.
   *  @constructor
   *  @param  config                          The configuration data {Object};
   *  @param  config.url                      The URL of the remote endpoint
   *                                          {URL};
   *  @param  [config.idle_timeout = '30s']   The keep alive timeout (ms)
   *                                          {Number | String};
   *  @param  [config.request_timeout = '5s'] The request timeout (ms)
   *                                          {Number | String};
   *  @param  [config.dry_run = false]        If truthy, do NOT attempt to
   *                                          connect {Boolean};
   *  @param  [config.labels]                 Additional labels that should be
   *                                          applied to metrics processed by
   *                                          this writer {Object};
   *  @param  [config.verbosity = 0]          Debug verbosity {Number};
   */
  constructor( config ) {
    super( config );

    Object.defineProperties( this, {
      type  : { value: 'http', enumerable: true },
      agent : { value: null,                    writable: true },
    });
  }

  /**
   *  Generate a string representation of this instance.
   *  @method toString
   *
   *  @return A string representation of this instance {String};
   */
  toString() {
    return `${this.constructor.name}[${this.config.url}]`;
  }

  /**
   *  Start this writer.
   *  @method start
   *
   *  @return A promise resolved when this writer is ready to receive metrics
   *          {Promise};
   */
  async start() {
    if (this.agent == null) {
      const url         = this.config.url;
      const httpConfig  = {
        keepAlive   : true,
        keepAliveMs : this.config.idle_timeout,
        timeout     : this.config.request_timeout,
      };

      this.agent = (url.protocol === 'https:'
                      ? new Https.Agent( httpConfig )
                      : new Http.Agent(  httpConfig ));
    }

    return this;
  }

  /**
   *  Stop this writer.
   *  @method stop
   *
   *  @return A promise resolved when this writer has been stopped {Promise};
   */
  async stop() {
    const res = (this.agent != null);

    if (this.agent) {
      this.agent.destroy();

      this.agent = null;

      console.error('%s.close(): agent destroyed', this.constructor.name);
    }

    return res;
  }

  /**
   *  Push the given Promethes metric to this remote endpoint.
   *  @method push
   *  @param  metrics   The prometheus metric(s) {Array | Object};
   *  @param  [scrape]  The scrape instance responsible for `metrics`
   *                    {PromScrape};
   *
   *  @return A promise for results {Promise};
   *          - on success, results {Mixed};
   *          - on failure, an error {Error};
   */
  async push( metrics, scrape=null ) {
    // :TODO: Implement this method : push()
    throw Utils.makeError(this, 'push() NYI');
  }
}

module.exports  = RemoteHttp;
