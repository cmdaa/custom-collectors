#!/usr/bin/env node
// Parse command line options
const Fs            = require('fs');
const Yaml          = require('js-yaml');
const Utils         = require('./lib/utils');
const EventEmitter  = require('events');
const PromScrape    = require('./handlers/async_source/prometheus-scrape');
const RemoteWrite   = { // remote_write handlers
  http          : require('./handlers/remote_write/http'),
  kafka         : require('./handlers/remote_write/kafka'),
  timescale     : require('./handlers/remote_write/timescale'),
  prometheus    : require('./handlers/remote_write/prometheus'),
  /* :NYI:
  prometheus_tcp: require('./handlers/remote_write/prometheus-tcp'),
  // */
  stdout        : require('./handlers/remote_write/stdout'),
};
const AsyncSource   = { // async_source handlers
  kafka         : require('./handlers/async_source/kafka'),
  kernelbeat    : require('./handlers/async_source/kernelbeat'),
  prometheus_tcp: require('./handlers/async_source/prometheus-tcp'),
};

/**
 *  The primary bridge application class / event bus.
 *  @class  Bridge
 */
class Bridge extends EventEmitter {
  ready   = false;
  config  = {
    verbosity             : 0,
    one_run               : false,
    dry_run               : false,
    max_listeners         : 0,      // EventEmitter default 10, 0 == infinite
    report_interval       : '10s',  /* Interval with which to trigger the
                                     * report of samples/second
                                     */

    global: {
      scrape_interval     : '1m',
      scrape_timeout      : '10s',
    },

    scrape: {
      job_name        : 'unnamed',
      metrics_path    : '/metrics',
      scheme          : 'http',

      /*
      honor_labels    : false,
      honor_timestamps: true,

      params          : [ 'query parameters' ],
      basic_auth      : { username: , password:, password_file: },
      tls_config      : {
        ca_file:, cert_file:, key_file:, server_name:, insecure_skip_verify:,
      },

      sample_limit    : Number,
      */
    },
  };

  /**
   *  Create a new instance
   *  @constructor
   *  @param  args            Incoming command-line arguments {Object};
   *  @param  args.config     The path to a configuration file {String};
   *  @param  args.verbosity  The verbosity level {Number};
   *  @param  args.dryRun     Is this a dry-run? {Boolean};
   */
  constructor( args ) {
    super();

    if (args.verbosity > 1) {
      console.error( 'Bridge(): args: %s', Utils.inspect( args ) );
    }

    Object.defineProperties( this, {
      _shutdown     : { value: false,         writable: true },
      _reportTimer  : { value: null,          writable: true },
      _remoteWriters: { value: [],            writable: true },
      _asyncSources : { value: [],            writable: true },
      _scrapers     : { value: [],            writable: true },

      _startTime    : { value: Date.now() },

    });


    if (args.config) {
      try {
        const config = Yaml.safeLoad( Fs.readFileSync( args.config, 'utf8' ) );

        // Don't allow a null value to override the complete global object
        if (config.global === null) { delete config.global; }

        this.config = Object.assign( this.config, config );

      } catch(ex) {
        this.ready = Promise.reject( ex );
        return;
      }
    }

    // Propagate incoming args into the config
    if (args.oneRun)    { this.config.one_run   = true }
    if (args.dryRun)    { this.config.dry_run   = true }
    if (args.verbosity) { this.config.verbosity = args.verbosity }

    /* Expand any values that contain an expandable prefix (e.g. 'file:') and
     * then convert the values of any timestamp/interval duration keys to
     * milliseconds.
     *
     * For example:
     *    {
     *      key1        : 'file:/path/to/file.txt', => contents of named file
     *      key_timeout :  '5m',                    => 300,000
     *      key_interval:  '30s',                   =>  30,000
     *    }
     */
    Utils.convertDurations( this.config );
    Utils.convertDurations( this.config.global );

    // Update the max_listeners (EventEmitter default: 10)
    this.setMaxListeners( this.config.max_listeners );

    if (this.config.verbosity > 1) {
      console.error( 'Bridge(): config' );
      console.error( Utils.inspect( this.config ) );
    }

    // Initialize any configured scrapers, remote_writers, and async_sources
    return this.__init();
  }

  /**
   *  Start a run
   *  @method start
   *
   *  @return A promise for results {Promise};
   *          - on success, `this`;
   *          - on failure, an error {Error};
   */
  start() {
    if (this.__onError == null) {
      this.__onError = this._onError.bind( this );
      this.on('error', this.__onError);
    }

    // Wait for all remote writers to report ready
    const errors    = [];
    const nWriters  = this._remoteWriters.length;

    console.error('==== Await %d remote writer%s ...',
                  nWriters, (nWriters === 1 ? '' : 's'));

    return Promise.all( this._remoteWriters.map( writer => {

      return writer.start()
        .then( res => {
          console.error('==== Remote writer [ %s ]: started', writer);
          return res;
        })
        .catch( err => {
          const msg = `Remote writer[ ${writer} ] failed to start: `
                    +   err.message;

          console.error('**** %s', msg);

          errors.push( msg );
        });

    } ) )
    .then( res => {
      if (errors.length > 0) {
        // One or more of our writers failed
        const err = new Error( errors.join('; ') );
        throw err;
      }

      // All remote writers have started
      const count = res.length;

      if (count) {
        console.error('=== %d remote writer%s ready, '
                    + 'process async_sources and scrape_configs ...',
                    count, (count === 1 ? '' : 's'));

        if (this.config.verbosity > 0) {
          console.error( this._remoteWriters );
        }
      }
      return res;
    })
    .then( res => {
      // Start scrapers and async_sources
      const pending   = [];
      let   scrapers  = this._scrapers.length;

      const nSources  = this._asyncSources.length;
      const nScrapers = this._scrapers.length;

      console.error('=== Await %d async source%s and %d scraper%s ...',
                    nSources, (nSources === 1 ? '' : 's'),
                    nScrapers, (nScrapers === 1 ? '' : 's'));

      this._scrapers.forEach( scraper => {
        const promise = scraper.start();

        if (this.config.one_run) {
          /* Wait for the first scrape to complete or fail and then stop the
           * scraper.
           */
          const _done = () => {
            console.error('=== Scraper [ %s ]: completed', scraper);

            // Stop this scraper
            scraper.stop();

            if (--scrapers < 1) {
              // Once all scrapers have completed, shutdown
              this.shutdown();
            }
          };

          scraper.once('scrape.data', _done);
          scraper.once('error',       _done);
        }

        promise.then( res => {
          console.error('==== Scraper [ %s ]: started', scraper);

          return res;
        })
        .catch( err => {
          const msg = `Scraper [ ${scraper} ] failed to start: `
                    +   err.message;

          console.error('**** %s', msg);

          errors.push( msg );
        });

        pending.push( promise );
      });

      this._asyncSources.forEach( src => {
        const promise = src.start();

        promise.then( res => {
          console.error('==== Async source [ %s ]: started', src);
          return res;
        })
        .catch( err => {
          const msg = `Async source [ ${src} ] failed to start: `
                    +   err.message;

          console.error('**** %s', msg);

          errors.push( msg );
        });

        pending.push( promise );
      });

      return Promise.all( pending );
    })
    .then( res => {
      if (errors.length > 0) {
        // One or more sources failed
        const err = new Error( errors.join('; ') );
        throw err;
      }

      // All metric sources have started
      const nSources  = this._asyncSources.length;
      const nScrapers = this._scrapers.length;

      if (nSources > 0) {
        console.error('=== %d async source%s ready',
                      nSources, (nSources === 1 ? '' : 's'));

        if (this.config.verbosity > 0) {
          console.error( this._asyncSources );
        }
      }

      if (nScrapers > 0) {
        console.error('=== %d scraper%s ready',
                      nScrapers, (nScrapers === 1 ? '' : 's'));

        if (this.config.verbosity > 0) {
          console.error( this._scrapers );
        }
      }

      this.__reportSamplesPerSec();

      return res;
    })
    .catch( err => {
      console.error('*** Startup failed:', err);

      throw err;
    });
  }

  /**
   *  Shutdown ths current run
   *  @method shutdown
   *
   *  @return `this` for a fluent interface;
   */
  shutdown( ) {
    if (this._shutdown) { return this }
    this._shutdown = true;

    if (this._reportTimer) {
      clearTimeout( this._reportTimer );
    }

    const pending = [
      // Stop all scrapers
      Promise.all( this._scrapers.map( scrape => scrape.stop() ) )
        .then( res => {
          console.error('>>> %d scrapers stopped', res.length );
        })
        .catch( err => {
          console.error('*** Scraper stop error: %s', err);
        }),

      // Stop all async sources
      Promise.all( this._asyncSources.map( remote => remote.stop() ) )
        .then( res => {
          console.error('>>> %d async sources stopped', res.length );
        })
        .catch( err => {
          console.error('*** Async source stop error: %s', err);

        }),
      // Stop all remote writers
      Promise.all( this._remoteWriters.map( remote => remote.stop() ) )
        .then( res => {
          console.error('>>> %d remote writers stopped', res.length );
        })
        .catch( err => {
          console.error('*** Remote writer stop error: %s', err);
        })
    ];

    Promise.all( pending )
      .finally( () => {
        if (this.__onError) {
          this.off('error', this.__onError);
          this.__onError = null;
        }

        process.exit();
      });

    return this;
  }

  /**************************************************************************
   * Private methods {
   */

  /**
   *  Handle an 'error' event.
   *  @method _onError
   *  @param  info        The error information {Object};
   *  @param  info.self   The source instance;
   *  @param  info.error  The error {Error};
   *
   *  @return void
   *  @private
   */
  _onError( info ) {
    console.error('*** Bridge error event: src[ %s ]:',
                  info.self, info.error);
  }

  /**
   *  Initialize scrapers, remote_writers, and async_sources.
   *  @method __init
   *
   *  @return `this` for a fluent interface;
   *  @private
   */
  __init() {
    this.ready = new Promise( (resolve, reject) => {
      this._remoteWriters = this.__processRemoteWrite();
      if (this._remoteWriters.length < 1) {
        // There should be at least 1 remote writer
        console.warn('*** no remote writer');
        //return reject( 'no remote_write entries' );
      }

      this._asyncSources = this.__processAsyncSource();
      this._scrapers     = this.__processScrapeConfigs();

      // There should be at least 1 asyncSource or scraper
      if (this._asyncSources.length < 1 && this._scrapers.length < 1) {
        return reject( 'no metric sources' );
      }

      return resolve( this );
    });

    return this;
  }

  /**
   *  Process the 'remote_write' portion of the configuration.
   *  @method __processRemoteWrite
   *
   *  @return The set of remote writers {Array};
   *  @private
   */
  __processRemoteWrite( ) {
    const remote_write  = this.config.remote_write;

    if (!Array.isArray(remote_write) || remote_write.length < 1) {
      //console.warn('*** remote_write configuration has no entries');
      return [];
    }

    const writers = remote_write.map( (remoteConfig, idex) => {
      let   url;

      // Propagate any 'dry-run' indicator
      if (this.config.dry_run) { remoteConfig.dry_run = true }

      try {
        url = new URL( remoteConfig.url );

      } catch(ex) {
        console.error('*** remote_write.url is invalid: %s', ex);
        return;
      }

      remoteConfig.bridge = this;
      remoteConfig.url    = url;

      if (remoteConfig.verbosity == null) {
        // Propagate verbosity
        remoteConfig.verbosity = this.config.verbosity;
      }

      let writer;

      switch( url.protocol ) {
        case 'timescale:':
        case 'timescaledb:':
        case 'postgres:':
        case 'postgresql:':
          if (remoteConfig.timescaledb_config) {
            remoteConfig = Object.assign({},
                                         remoteConfig,
                                         remoteConfig.timescaledb_config );
            delete remoteConfig.timescaledb_config;
          }
          writer = new RemoteWrite.timescale( remoteConfig );
          break;

        case 'kafka:':
          if (remoteConfig.kafka_config) {
            remoteConfig = Object.assign({},
                                         remoteConfig,
                                         remoteConfig.kafka_config );
            delete remoteConfig.kafka_config;
          }
          writer = new RemoteWrite.kafka( remoteConfig );
          break;

        case 'http:':
        case 'https:':
          if (remoteConfig.http_config) {
            remoteConfig = Object.assign({},
                                         remoteConfig,
                                         remoteConfig.http_config );
            delete remoteConfig.http_config;
          }
          writer = new RemoteWrite.http( remoteConfig );
          break;

        case 'prometheus:':
        case 'sprometheus:':
          if (remoteConfig.http_config) {
            remoteConfig = Object.assign({},
                                         remoteConfig,
                                         remoteConfig.http_config );
            delete remoteConfig.http_config;
          }
          writer = new RemoteWrite.prometheus( remoteConfig );
          break;

        /* :NYI:
        case 'prometheus-tcp:':
          if (remoteConfig.http_config) {
            remoteConfig = Object.assign({},
                                         remoteConfig,
                                         remoteConfig.http_config );
            delete remoteConfig.http_config;
          }
          writer = new RemoteWrite.prometheus_tcp( remoteConfig );
          break;
        // */

        case 'stdout:':
          writer = new RemoteWrite.stdout( remoteConfig );
          break;

        default:
          console.error('*** remote_write.url unsupported protocol: %s',
                        url.protocol);
          return;
      }

      return writer;
    });

    return writers.filter( remote => remote );
  }

  /**
   *  Process the 'async_source' portion of the configuration.
   *  @method __processAsyncSource
   *
   *  @return The set of async metric sources {Array};
   *  @private
   */
  __processAsyncSource( ) {
    const async_source  = this.config.async_source;

    if (!Array.isArray(async_source) || async_source.length < 1) {
      //console.warn('*** async_source configuration has no entries');
      return [];
    }

    const sources = async_source.map( (srcConfig, idex) => {
      let   url;

      // Propagate any 'dry-run' indicator
      if (this.config.dry_run) { srcConfig.dry_run = true }

      try {
        url = new URL( srcConfig.url );

      } catch(ex) {
        console.error('*** async_source.url is invalid: %s', ex);
        return;
      }

      srcConfig.bridge = this;
      srcConfig.url    = url;

      if (srcConfig.verbosity == null) {
        // Propagate verbosity
        srcConfig.verbosity = this.config.verbosity;
      }

      let src;
      switch( url.protocol ) {
        case 'kafka:':
          if (srcConfig.kafka_config) {
            srcConfig = Object.assign({}, srcConfig,
                                          srcConfig.kafka_config );
            delete srcConfig.kafka_config;
          }
          src = new AsyncSource.kafka( srcConfig );
          break;

        case 'kernelbeat:':
          src = new AsyncSource.kernelbeat( srcConfig );
          break;

        case 'prometheus:':
        case 'prometheus-tcp:':
          src = new AsyncSource.prometheus_tcp( srcConfig );
          break;

        default:
          console.error('*** async_source.url unsupported protocol: %s',
                        url.protocol);
          return;
      }

      return src;
    });

    return sources.filter( src => src );
  }

  /**
   *  Process the 'scrape_configs' portion of the configuration.
   *  @method __processScrapeConfigs
   *
   *  @return The set of scrapers {Array};
   *  @private
   */
  __processScrapeConfigs( ) {
    const scrape_configs  = this.config.scrape_configs;

    if (!Array.isArray(scrape_configs) || scrape_configs.length < 1) {
      //console.warn('*** scrape_configs configuration has no entries');
      return [];
    }

    // Propagate any 'dry-run' indicator
    if (this.config.dry_run) { scrape_configs.dry_run = true }

    const scrapers  = [];

    scrape_configs.forEach( (scrapeConfig, idex) => {
      if (! Array.isArray( scrapeConfig.static_configs )) {
        console.warn(`*** scrape_config #%d [ %s ] has no 'static_configs'`,
                     idex, scrapeConfig.job_name);

        return;
      }

      scrapeConfig.bridge = this;

      // Mixin globals and scrape defaults
      scrapeConfig = Object.assign( {},
                                    this.config.global,
                                    this.config.scrape,
                                    scrapeConfig );

      if (scrapeConfig.verbosity == null) {
        // Propagate verbosity
        scrapeConfig.verbosity = this.config.verbosity;
      }

      scrapeConfig.static_configs.forEach( (staticConfig, jdex) => {
        if (! Array.isArray( staticConfig.targets )) {
          console.warn(`*** scrape_config #%d [ %s ], static_config #%d `
                        +       `has no 'targets'`,
                      idex, scrapeConfig.job_name, jdex);

          return;
        }

        // Mixin scrape config
        staticConfig = Object.assign( {}, scrapeConfig, staticConfig );

        if (staticConfig.verbosity == null) {
          // Propagate verbosity
          staticConfig.verbosity = scrapeConfig.verbosity;
        }

        // Convert any unconverted timestamp/interval durations
        Utils.convertDurations( staticConfig );

        staticConfig.targets.forEach( target => {
          const urlStr  = staticConfig.scheme +'://'+ target
                          + staticConfig.metrics_path;
          let   url;
          try {
            url = new URL( urlStr );

          } catch(ex) {
            console.warn('*** static_config.url [ %s ] is invalid: %s',
                         urlStr, ex);
            return;
          }

          /* :TODO: Support 'staticConfig.labels' as additional labels that
           *        should be applied to metrics from this scraper.
           */
          const sc  = {
            bridge      : this,
            name        : staticConfig.job_name,
            url         : url,
            interval    : staticConfig.scrape_interval,
            timeout     : staticConfig.scrape_timeout,
            metricClass : staticConfig.metric_class,
            labels      : staticConfig.labels,
            verbosity   : staticConfig.verbosity,
          };

          const scraper = new PromScrape( sc );

          scrapers.push( scraper );
        });
      });
    });

    return scrapers.filter( scraper => scraper );
  }

  /**
   *  Output a report for a single scraper/async source/remote writer.
   *  @method __reportHandler
   *  @param  handler   The scraper/async source/remote writer handler;
   *  @param  sec       The number of seconds since collection began;
   *
   *  @return null
   *  @private
   */
  __reportHandler( handler, secs ) {
    const now         = new Date();
    const thisTime    = now.getTime();
    const thisCount   = handler.sampleCount;
    const pending     = handler.pendingCount;
    const lastCount   = handler._lastCount || 0;
    const diffCount   = (thisCount - lastCount);
    const diffSecs    = (handler._lastTime
                          ? (thisTime - handler._lastTime) / 1000
                          : secs);
    const avgPerSec   = (thisCount > 0
                          ? thisCount / secs
                          : 0);
    let   pendingStr  = '';

    handler._lastTime  = thisTime;
    handler._lastCount = thisCount;

    if (pending >= 0) {
      pendingStr = `; pending=${pending}`;
    }

    console.error('=== %s %s samples=%d/%s secs; last=%d/%s secs; avg/sec=%s%s',
                  now, handler,
                  thisCount, secs.toFixed(3),
                  diffCount, diffSecs.toFixed(3),
                  avgPerSec.toFixed(3),
                  pendingStr);
  }

  /**
   *  A periodic, repeating timer to report samples per second for all scrapers,
   *  async sources, and remote writers.
   *  @method __reportSamplesPerSec
   *
   *  @return null
   *  @private
   */
  __reportSamplesPerSec() {
    this._reportTimer = setTimeout( () => {
      const reportHandler = this.__reportHandler;
      const secs          = (Date.now() - this._startTime) / 1000;

      this._scrapers.forEach(      handler => reportHandler( handler, secs ) );
      this._asyncSources.forEach(  handler => reportHandler( handler, secs ) );
      this._remoteWriters.forEach( handler => reportHandler( handler, secs ) );

      /*
      const measures  = [
        { name:     'scrapers',
          count:    this._scrapers.length,
          samples:  this._scrapers.reduce(
                      (sum, scraper) => sum + scraper.sampleCount, 0 ),
        },
        { name:     'asyncSources',
          count:    this._asyncSources.length,
          samples:  this._asyncSources.reduce(
                      (sum, source) => sum + source.sampleCount, 0 ),
        },
        { name:     'remoteWriters',
          count:    this._remoteWriters.length,
          samples:  this._remoteWriters.reduce(
                      (sum, writer) => sum + writer.sampleCount, 0 ),
        },
      ];

      measures.forEach( measure => {
        if (measure.count < 1) { return }

        const sps = ( measure.samples < 1
                        ? '0.000'
                        : (measure.samples / secs).toFixed(3) );

        console.error('=== %s %s: %s samples/sec',
                      measure.count.toString().padStart( 4, ' ' ),
                      measure.name.padEnd( 13, ' ' ),
                      sps.padStart( 10, ' ' ) );
      });
      // */

      if (! this._shutdown) {
        // Repeat
        this.__reportSamplesPerSec();
      }
    }, this.config.report_interval);
  }
  /* Private methods }
   **************************************************************************/
}

/***************************************************************************
 * Main and Private helpers {
 *
 */

var bridge  = null; // Global (eventually, an instance of Bridge)

// Establish initial listeners (will be replace once _run() is invoked)
process
  .on('unhandledRejection', _handleRejection )
  .on('SIGINT',             _handleSignal )
  .on('SIGTERM',            _handleSignal );

const args    = require('yargs')
  .options( {
    c: {
      alias       : 'config',
      default     : 'scrape.yml',
      describe    : 'The YAML configuration file.',
      type        : 'string',
      demandOption: true,
    },
    o: {
      alias       : 'one-run',
      default     : false,
      describe    : 'Run the scrapers once and exit. '
                    + 'The default is to start all scrapers with interval '
                    + 'timers to run until the bridge is terminated.',
      type        : 'boolean',
      demandOption: true,
    },
    n: {
      alias       : 'dry-run',
      default     : false,
      describe    : 'Simply parse the configuration file and output.',
      type        : 'boolean',
      demandOption: true,
    },
    v: {
      alias       : 'verbosity',
      describe    : 'Increase debug verbosity.',
      count       : true,
      //default     : 0,
      //type        : 'boolean',
      //demandOption: true,
    },
    h: { alias : [ '?', 'help' ] },
  })
  .help()
  .argv;

bridge = new Bridge( args );

// Wait for the bridge to report 'ready'
bridge.ready
  .then( res => {
    if (bridge.config.dry_run) {
      // dry-run
      console.error( 'dry-run: config' );
      console.error( Utils.inspect( bridge.config ) );
      process.exit( -1 );
    }

    return bridge.start();
  })
  .then( res => {
    console.error('>>> Bridge ready');
  })
  .catch( err => {
    if (bridge.config.verbosity > 0) {
      console.error('*** Bridge error:', err);

    } else {
      console.error('*** Bridge error:', err.message);
    }
    process.exit( -1 );
  });

/**
 *  Handle promise rejections
 *  @method _handleRejection
 *  @param  reason    The reject reason {String};
 *  @param  promise   The rejected promise {Promise};
 */
function _handleRejection( reason, promise ) {
  if (reason.stack) {
    console.error('*** Unhandled Rejection stack:', reason);
  } else {
    console.error('*** Unhandled Rejection:', reason);
  }
}

/**
 *  Handle a signal
 *  @method _handleSignal
 *  @param  sig     The signal {String};
 *  @param  code    The associated (exit) code {Number};
 */
function _handleSignal( sig, code ) {
  console.error('=== %s: shutdown ...', sig);
  if (bridge) {
    bridge.shutdown();
  }
}
/* Main and Private helpers }
 ***************************************************************************/
