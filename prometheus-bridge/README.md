A node-based bridge between Prometheus exporters, asynchronous metric sources,
and remote writers.

This [Prometheus bridge](bin/bridge) only supports basic Prometheus options:
- `scrape_configs` which use `static_configs` to identify Prometheus
  exporters to be scraped;
- `remote_write` with a `url` that uniquely identifies a
  [remote writer](#remote_writer) via the URL protocol/scheme and MAY
  contain a custom configuration for that writer;
- a non-standard `async_source` with a `url` that uniquely identifies an
  [asynchronous source](#async_source) via the URL protocol/scheme and MAY
  contain a custom configuration for that source;


For all configurations, keys may trigger parsing and values may trigger
expansion:
- if the key has either `_timeout` or `_interval` as a suffix, the key's value
  will be parsed as a duration, converting it into a milisecond value;

- if the value contains `file:` as a prefix:
  - a new key (`${key}_path`) will be added containing the file path;
  - an attempt will be made to read the target file:
    - on success, the key's value will be replaced with the contents of the
      file;
    - on failure, the key's value will be set to `undefined` and a new key
      (`${key}_error`) will be added to communicate the error;

---
**Table of Contents**

[[_TOC_]]

### <a name="components">Components</a>

The bridge has a number of components which may be used together via run-time
configuration to provide various metrics-bridging solutions.

- [Async Source](#async_source) : provides access to asynchronously generated
  metrics from a remote endpoint, and locally emits `sample` and/or `metric`
  events on the bridge event bus when metrics arrive;
- [Remote Writer](#remote_writer) : listens for local `sample` and/or `metric`
  events on the bridge event bus and then pushes the data to a remote endpoint;


Example usage:
![MetricsBridge](docs/MetricsBridge.jpg)


#### <a name="flow_control">Flow Control</a>

The bridge supports flow control to allow remote writers to request that
metrics generation be paused when flow-restricting congestion arises on a
remote writer. Once the congestion has been resolved, a writer can then request
that metrics generation be resumed.

Flow control is enabled via the per-bridge event bus. This allows writers to
signal flow control requests (transmit-on/`xon` and transmit-off/`xoff` events)
that sources are responsible for honoring. The implementation of source-based
flow control is completely source dependant &mdash; e.g. The Kafka source makes
use of a kafka flow-control topic, which the Kafka remote writer must use as
one of its congestion indicators; TCP-based sources use TCP-based flow-control,
writing xon/xoff bytes to remote endpoints which are required to honor these
flow control requests.

Note that flow control requires the cooperation of metric sources. If a source
does not honor flow control requests, one or more writers may become
overwhelmed causing metric data to be lost.


#### <a name="async_source">Asynchronous Source</a>

An asynchronous source allows incorporation of asynchronously generated metrics
into the normally-synchronous Prometheus metrics flow.

##### <a name="scraper">Prometheus Scrape</a>

A scraper is a timer-based ansynchronous source that periodically pulls data
from a Prometheus exporter.

It is implemented by the [PromScrape](handlers/async_source/prometheus-scrape.js)
class which, for a scheduled pull, creates an input stream from a Prometheus
exporter under the assumption that the exporter will return metric samples in
the [Prometheus Exposition Format](https://prometheus.io/docs/instrumenting/exposition_formats/).

The scraper instance will then process the stream line-by-line, generating
events on the bridge event bus:
- a `sample` event for each explicit [metric sample](#metric_sample);
- a `metric` event for each [consolidated metric](#consolidated_metric);


**:TODO:** The scraper should listen for `xoff/xon` events on the bridge event
bus and pause/resume the schedule.


Scrapers are configured using multiple sections from a configuration file, with
one scraper instance for each `scrape_configs.static_configs` entry:
- `global` : global configuration
  - `scrape_interval` [6s] : the scrape interval;
  - `scrape_timeout` [1s] : the scrape timeout;
- `scrape_configs`: scrape job configuration
  - `job_name` [unnamed] : the name of this scrape job;
  - `metrics_path` [/metrics] : the URL path to use for configured targets;
  - `scheme` [http] : the URL scheme to use for configured targets;
  - `scrape_interval` [6s] : the scrape interval (over-ride global);
  - `scrape_timeout` [1s] : the scrape timeout (over-ride global);
  - `static_configs` : static target configuration
    - `targets` : an array of target `host:port` strings used in combination
      with other options to generate the URL of the target Prometheus exporter;
    - `metric_class` [application] : the [metric class](#metric_class) to
      include with generated [metric samples](#metric_sample) and/or
      [consolidated metric](#consolidated_metric) entry;
    - `labels` : an object of label name/value pairs to include with any
      metrics from this scrape target;


Standard Prometheus configuration options that are currently not supported, but
could be added if needed:
- for `scrape_configs`:
  - `honor_labels` [false] : an indication of how to handle label conflicts
    (only needed if `static_configs.labels` is supported);
  - `honor_timestamps` [false] : an indication of whether metric-specified
     timestamps should be passed on (currently, if a sample includes a
     timestamp, it will always be honored);
  - `params` : an array of query parameters;
  - `basic_auth` : an object containing basic authentication information
    [username, password, password_file];
  - `bearer_token` / `bearer_token_file` : enables an `Authorization` header on
    every scrape request using the provided token value;
  - `tls_config` : an object containing TLS configuration parameters
    [ca_file, cert_file, key_file, server_name, insecure_skip_verify];


##### <a name="kafka_source">Kafka</a>

[KafkaSource](handlers/async_source/kafka.js) is an asynchronous metric source
from Kafka that:
- registers as a consumer for all specified `topic` values;
- expects incoming messages to follow the [Kafka Topic
  protocol](#kafka_topic_protocol);
- for each message that arrives, parses the message value as JSON, creates
  a [metric sample](#metric_sample) instance from the generated object, and
  emits a local `sample` event on the bridge event bus with the sample
  instance;
- supports a custom `kafka_config` configuration object with:
  - `topic` [metrics] : the topic(s) for which this instance should register
    as a consumer.  Note that this may be a string with a single topic, a
    comma-separated string with multiple topics, or a list of single or
    multiple topics;
  - `connect_timeout` [10s] : timeout for connecting to Kafka;
  - `idle_timeout` [5m] : timeout before a connection is released;
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this asyncronous source;
- **:TODO:** listens for flow control events on the bridge event bus:
  - `xoff` : publish an `xoff` message on the kafka flow control topic;
  - `xon`  : publish an `xon`  message on the kafka flow control topic;


##### <a name="prometheus_source">Prometheus</a>

[PrometheusSource](handlers/async_source/prometheus-tcp.js) is an asynchronous,
TCP-based metric source fed from a metrics generator that produces
newline-separated metrics in [Prometheus Exposition
Format](https://prometheus.io/docs/instrumenting/exposition_formats/)

This source:
- listens for TCP messages from asynchronous metrics generator(s) which send
  newline-delimited, prometheus exposition formatted metrics;
- for each message that arrives, splits the message into individual lines,
  creates [metric sample](#metric_sample) instances from each line, and emits
  local `sample` events on the bridge event bus with the sample instances;
- uses the `async_source` URL to determine:
  - `host` [0.0.0.0] : the TCP host on which to listen;
  - `port` [3377] : the TCP port on which to listen;
- supports additional configuration data in `async_source`:
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this asyncronous source;
- listens for flow control events on the bridge event bus:
  - `xoff` : send an `xoff` message to all TCP clients;
  - `xon`  : send an `xon`  message to all TCP clients;


##### <a name="kernelbeat_source">KernelBeat</a>

[KernelBeatSource](handlers/async_source/kernelbeat.js) is an asynchronous
metric source that is fed from
[KernelBeat](https://gitlab.com/cmdaa/linux-kernel-module/-/blob/master/docs/KernelBeat.md).

This source:
- either, reads from the `/dev/kernel_beat` character device *or* listens for
  TCP messages sent from the KernelBeat module when the module determines that
  the local reader is falling behind;
- for each message that arrives, parses the message value as JSON, creates
  [metric sample](#metric_sample) instances based upon the contents of the
  generated object, and emits local `sample` events on the bridge event bus
  with the sample instances;
- uses the `async_source` URL, in the form `kernelbeat://` to determine:
  - data source:
    - TCP : `kernelbeat://[host][:port]`
      - `host` [0.0.0.0] : for TCP, the host/ip-address on which to listen;
      - `port` [3333] : for TCP, the port on which to listen;
    - character device : `kernelbeat:///dev/kernel_beat`
- supports additional configuration data in `async_source`:
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this asyncronous source;
- listens for flow control events on the bridge event bus:
  - TCP-based source:
    - `xoff` : send an `xoff` message to all TCP clients;
    - `xon`  : send an `xon`  message to all TCP clients;
  - Character device source:
    - `xoff` : close the character device, terminating metrics generation;
    - `xon`  : open  the character device, resuming metrics generation;


#### <a name="remote_writer">Remote Writer</a>

A Remote writer listens for local `sample` and/or `metric` events on the bridge
event bus and sends the provided metric data on to a remote endpoint.

##### <a name="kafka_writer">Kafka</a>

[RemoteKafka](handlers/remote_write/kafka.js) is a remote writer of metrics to
Kafka that:
- is created for the `kafka:` URL scheme;
- listens for local `sample` events on the bridge event bus and acts on each;
- pushes [metric samples](#metric_sample) to a specified
  [Kafka](https://kafka.apache.org/) endpoint using a topic that is a
  combination of the `topic_prefix` from the writers `kafka_config` and the
  [metric_class](#metric_class) on the sample entry. Kafka messages follow the
  [Kafka Topic protocol](#kafka_topic_protocol);
- supports a custom `kafka_config` configuration object with:
  - `topic_prefix` [metrics] : used as the prefix of the topic on which to
     send messages (along with a `_` separator and the `metric_class` from
     a set of grouped samples);
  - `connect_timeout` [10s] : timeout for connecting to Kafka;
  - `idle_timeout` [5m] : timeout before a connection is released;
  - `request_timeout` [30s] : timeout when pushing a payload to Kafka;
  - `batch_num_samples` [10,000] : the maximum number of messages to batch in
    a single payload;
  - `batch_timeout` [500ms] : the maximum time to wait for additional samples
    before sending a batch/payload;
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this remote writer;
- **:TODO:** watches for incoming messages on the kafka flow control topic:
  - `xoff` : emit an `xoff` event on the bridge event bus;
  - `xon`  : emit an `xon`  event on the bridge event bus;
- **:TODO:** monitors conditions that require flow control (e.g. client errors,
  timeouts):
  - congestion : emit an `xoff` event on the bridge event bus;
  - resolved   : emit an `xon`  event on the bridge event bus;


##### <a name="prometheus_writer">Prometheus</a>

[RemotePrometheus](handlers/remote_write/prometheus.js) is a remote writer,
to a Prometheus remote write endpoint, specifically
[PromScale](https://github.com/timescale/promscale).

This writer:
- is created for the URL schemes: `prometheus`, `sprometheus`;
- listens for local `sample` events on the bridge event bus and acts on each;
- writes each sample to the PromScale remote write endpoint using the
  [Prometheus remote write protocol](#prometheus_remote_write_protocol);
- supports a custom `http_config` configuration object with:
  - `idle_timeout` [2s] : timeout before a connection is released
    (keep-alive);
  - `request_timeout` [6s] : timeout when sending a request;
  - `max_sockets` [128] : number of sockets that may be created before reuse;
  - `max_free_sockets` [16] : number of sockets to keep around for reuse;
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this remote writer;
- **:TODO:** monitors conditions that require flow control (e.g. client errors,
  timeouts):
  - congestion : emit an `xoff` event on the bridge event bus;
  - resolved   : emit an `xon`  event on the bridge event bus;


##### <a name="timescale_writer">Timescale</a>

**NOTE** The [Prometheus remote writer](#prometheus_writer) is the preferred
method for writing data into a Timescale database.


[RemoteTimescale](handlers/remote_write/timescale.js) is a remote writer of
metrics to TimescaleDb that:
- is created for the URL schemes: `timescale`, `timescaledb`, `postgres`,
  `postgresql`;
- listens for local `sample` events on the bridge event bus and acts on each;
- pushes [metric samples](#metric_sample) in
  [Prometheus Exposition Format](https://prometheus.io/docs/instrumenting/exposition_formats/)
  to the given [TimescaleDb](https://www.timescale.com/) endpoint;
- requires that the TimescaleDb endpoint include the
  [pg_prometheus](https://github.com/timescale/pg_prometheus) extension;
- supports a custom `timescaledb_config` configuration object with:
  - `user` [postgres] : the username for authenticating to
    timescale/postgres;
  - `password` [] : the password for authenticating to timescale/postgres;
  - `database` [postgres] : the name of the target database;
  - `table` [metrics] : the name of the target database table;
  - `max_clients` [10] : the maximum number of parallel clients;
  - `connect_timeout` [10s] : timeout for connecting to Postgres;
  - `idle_timeout` [30s] : timeout before a connection is released;
  - `statement_timeout` [30s] : timeout for insert/query statements;
  - `batch_num_samples` [25] : the maximum number of samples to batch within
    a single INSERT;
  - `batch_timeout` [500ms] : the maximum time to wait for additional samples
    before performing a batch insert;
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this remote writer;
  - `ssl` : should SSL be used to connect to the database [false];
  - `reject_unauthorized` : reject an unauthorized CA [true] or disable the
    CA authorization check;
- **:TODO:** monitors conditions that require flow control (e.g. client errors,
  timeouts):
  - congestion : emit an `xoff` event on the bridge event bus;
  - resolved   : emit an `xon`  event on the bridge event bus;


##### <a name="stdout_writer">Stdout</a>
[RemoteStdout](handlers/remote_write/stdout.js) is a simple writer that will
echo received metrics to stdout.

This writer:
- is created for the URL scheme: `stdout`;
- listens for local `sample` events on the bridge event bus and acts on each;
- writes each sample to stdout;
- can be configured via the `kb_flow` flag to recognize keyboard-based flow
  control events and emit flow control events to the bridge event bus:
  - `ctl-s` : emit an `xoff` event on the bridge event bus;
  - `ctl-q` : emit an `xon`  event on the bridge event bus;


##### <a name="http_writer">Http</a>

[RemoteHttp](handlers/remote_write/http.js) is an *incomplete* remote writer
for http and/or https that:
- could be created for the URL schemes: `http`, `https`;
- could listen for local `sample` and/or `metric` events on the bridge event
  bus and act on each;
- is not yet completed, could push [metric samples](#metric_sample) in either
  [Prometheus Exposition Format](https://prometheus.io/docs/instrumenting/exposition_formats/)
  or as a [consolidated metric](#consolidated_metric) depending on the need;
- supports a custom `http_config` configuration object with:
  - `idle_timeout` [30s] : timeout before a connection is released
    (keep-alive);
  - `request_timeout` [5s] : timeout when sending a request;
  - `labels` : an object of label name/value pairs to include with any
    metrics that pass through this remote writer;


### <a name="data_format">Data Formats</a>

#### <a name="metric_class">Metric Class</a>

A metric class provides a conceptual grouping of like-type/like-purpose metrics
and may be used by [remote writers](#remote_writer) (e.g.
[Kafka Writer](#kafka_writer)), to channel and route metric entries.

Currently, the standard set of metric classes is:
- `application` : application-level metrics (default);
- `system` : system-level metrics (e.g. cpu usage, load, memory usage, uptime,
                                        process summary, filesystem summary);
- `process` : per-process metrics;
- `network` : per-interface metrics;
- `filesystem` : per-filesystem metrics;
- `cgroup` : per-cgroup metrics;


#### <a name="metric_sample">Metric Sample</a>

A metric sample is a parsed version of a metric, often from a string adhering to
the [Prometheus Exposition
Format](https://prometheus.io/docs/instrumenting/exposition_formats/),
and has the form:
``` javascript
{
  name        : %metric_name%,
  metric_class: %class_of_metric%,  // If not provided, the first component of
                                    // `name` when split by '_'
  value       : %metric_value%,     // String representation of a numeric value
  timestamp   : %timestamp_ms%,     /* Either explicit or from sample creation
                                     * (milliseconds since epoch)
                                     */
  [labels     : { %key%: %value%, ... }]
}

// Example:
{
  name        : "os_cpu_user",
  metric_class: "os",
  value       : "739862",
  timestamp   : 1603384176,
  labels      : {
    node  : "source.org",
    unit  : "seconds"
  }
}
```

#### <a name="consolidated_metric">Consolidated Metric</a>

A consolidated metric is generated from one or more Prometheus samples as well
as `# HELP` and `# TYPE` lines from an exporter source. It has the basic form:
``` javascript
{
  name        : %metric_name%,
  help        : %metric_help%,
  type        : %metric_type%,
  metric_class: %class_of_metric%,
  metrics     : [ %type_specific_entries% ... ]
}

// Example:
{
  name        : "os_cpu_user",
  help        : "CPU time dedicated to user-level processing",
  type        : "GAUGE",
  metric_class: "os",
  metrics     : [
    { value: "739862", labels: {node: "source.org", unit: "seconds"} }
  ]
}
```

The type-specific `metrics` entries are further differentiated by
`metric_type`:
- `COUNTER`, `GAUGE`, and `UNTYPED` `metrics` entries have the form:
  ``` javascript
    {
      value: %value%

      [labels: { %key%: %value%, ... }]
    }
  ```

- `HISTOGRAM` `metrics` entries have the form:
  ``` javascript
    {
      buckets: {                // from %metric_name%_bucket entries
        %le_label_value%: %bucket_value%,
        ...
      },
      sum   : %sum_value%,      // from %metric_name%_sum
      count : %count_value%,    // from %metric_name%_count

      [labels: { %key%: %value%, ... }]
    }
  ```

- `SUMMARY` `metrics` entries have the form:
  ``` javascript
    {
      [quantiles: {             // from %metric_name% entries
        %quantile_label_value%: %quantile_value%,
        ...
      },]
      [sum   : %sum_value%,]    // from %metric_name%_sum
      [count : %count_value%,]  // from %metric_name%_count

      [labels: { %key%: %value%, ... }]

      [%additional_key%: %value%, ... ]
    }
  ```

#### <a name="prometheus_remote_write_protocol">Prometheus remote write protocol</a>
Based upon the
[PromScale](https://github.com/timescale/promscale/tree/master/pkg/pgmodel#write-path)
implementation and Prometheus protobuf definitions  ( [Prometheus
WriteRequest](https://github.com/prometheus/prometheus/blob/master/prompb/remote.proto#L22),
[Prometheus
TimeSeries](https://github.com/prometheus/prometheus/blob/master/prompb/types.proto#L27),
consolidated into [prometheus.proto](handlers/remote_write/prometheus.proto) ),
the remote write protocol and protobuf formats are:
```yaml
HTTP Method : POST

HTTP Headers:
 Content-Encoding:                   snappy
 Content-Type:                       application/x-protobuf
 X-Prometheus-Remote-Write-Version:  0.1.x

HTTP Body   : (protobuf encoded, snappy compressed)
  WriteRequest
    TimeSeries[]
      Labels  []Label   name  string, value     string
      Samples []Sample  value double, timestamp int64

  The metric name is passed as a special '__name__' label.
```

#### <a name="kafka_topic_protocol">Kafka Topic protocol</a>

The Kafka topic used by this bridge when publishing a metric is a combination of
the `topic_prefix` from the writers `kafka_config` (e.g. metrics) and the
[metric_class](#metric_class) on the sample entry. If the `metric_class` is not
provided, it will be set to the first component of the metric name when split by
`_`. For example, the metric topic of `os_cpu_user` would be `os`.

Messages passed via a Kafka topic will be JSON-encoded versions of
[metric samples](#metric_sample).


### <a name="testing">Testing</a>
This bridge may be tested using [kubernetes](#testing_k8s) or
[docker directly](#testing_docker).


#### <a name="testing_k8s">Testing with Kubernetes</a>

##### <a name="creating_resources">Creating resources</a>

You may create the required Kubernetes resource, including an `access`
service/pod from which testing may be performed, using either:

1. <a name="create_kubectl">`kubectl`</a> and the [k8s/](k8s/) subdirectory:
   ``` bash
   $ kubectl create -f k8s/
   ...
   #
   # Now monitor the set of pods until all report ready:
   #   pod/timescaledb-0
   #   pod/zookeeper-0
   #   pod/kafka-0
   #   pod/access-0
   #
   $ kubectl get pods
   ...
   ```

2. <a name="create_helm">`helm`</a> and the [helm/](helm/) subdirectory:
   ``` bash
   $ helm install prometheus-bridge --set access.srcDir=$(pwd) ./helm
   #
   # Now monitor the set of pods until all report ready:
   #   pod/timescaledb-0
   #   pod/zookeeper-0
   #   pod/kafka-0
   #   pod/access-0
   #
   $ kubectl get pods
   ```

##### <a name="testing_kubectl">Access and testing</a>
Once all resources are ready, you may connect to the `access-0` pod to perform
testing:
``` bash
$ kubectl exec -it access-0 -- /bin/sh
#
# :NOTE: If you have not yet installed the node modules in the source
#        directory, you will need to do that before the tests may be run.
#
#        Since the source directory is mounted within the access pod, this
#        should only need to be done once.
#
#        To install the required modules, run:
#           npm install
#
#        Since `python` is not installed in the acess pod, you will receive a
#        few errors and warnings related to the optional 'snappy' dependency.
#        These may be safely ignored.
#
# Check if timescale is available
/app # ./bin/ping-timescale
...
# Check if kafka is available
/app # ./bin/ping-kafka
...
#
# Start a general metrics consumer using a bridge configuration that includes
# a kafka asynchronous source and timescale remote writer.
#
# This will push any metrics published to `metrics_%metric-class%` kafka topics
# to TimescaleDb.
#
/app # ./bin/bridge -c test/config-kafka2tsdb.yml

#
# :NOTE: Since the test producer only uses the `metrics_application` topic,
#        you could start a simple kafka consumer to watch the raw messages.
#
/app # ./bin/kafka-consumer -h kafka:9092 -t metrics_application
...

```

In a separate window, connect to `access-0` and start the bridge with a
configuration that includes a simple scraper and Kafka remote writer:
``` bash
$ kubectl exec -it access-0 -- /bin/sh
/app # ./bin/bridge -c test/config-exporter2kafka.yml
...
```

At this point, you should start seeing messages in the first terminal.


If you are using the bridge was a general metrics consumer, you can check if
the metrics are being written to TimescaleDb by connecting to the
`timescaledb-0` pod:
``` bash
$ kubectl exec -it timescaledb-0 -- psql
psql (10.6)
Type "help" for help.

postgres=# select * from metrics order by time desc limit 2;
...
postgres=# \q
```

#### <a name="testing_docker">Testing with Docker</a>
The bridge may also be tested using a combination of docker containers with a
shared network and the data located in the [test](test) directory.


Using a terminal per entry:
- Kafka in docker (using the [spotify/kafka](https://hub.docker.com/r/spotify/kafka) Kafka/Zookeeper image):
  ``` bash
  ./bin/start-kafka
  ```
- TimescaleDb in docker (using the [timescale/pg_prometheus](https://hub.docker.com/r/timescale/pg_prometheus) image):
  ``` bash
  ./bin/start-timescaledb
  ```
- a Kafka consumer to watch what is produced:
  ``` bash
  ./bin/kafka-consumer
  ```
- a nodejs container to run the bridge (using [node:10.15.3-alpine](https://hub.docker.com/_/node)):
  ``` bash
  ./bin/start-bridge
  ```


To view the last two entries that were added to TimescaleDb:
``` bash
./bin/connect-timescaledb
/ # psql
psql (10.6)
Type "help" for help.

postgres=# select * from metrics order by time desc limit 2;
...
postgres=# \q
/ # exit
```

Or, more simply:
``` bash
./bin/connect-timescaledb 'select * from metrics order by time desc limit 2'
...
```

##### <a name="deleting_resources">Deleting resources</a>

Shutting down the test environment depends upon how it was started:

1. <a name="deleting_kubectl">`kubectl`</a> and the [k8s/](k8s/) subdirectory:
   ``` bash
   $ kubectl delete -f k8s/
   ...
   #
   # Now monitor the set of pods until all have terminated:
   #   pod/timescaledb-0
   #   pod/zookeeper-0
   #   pod/kafka-0
   #   pod/access-0
   #
   $ kubectl get pods
   ...
   ```

2. <a name="deleting_helm">`helm`</a> and the [helm/](helm/) subdirectory:
   ``` bash
   $ helm ls
   $ helm uninstall prometheus-bridge
   ```

3. <a name="deleting_docker">`docker`</a>, simply terminate the `./bin/*`
   processes that were started (e.g. `start-kafka`, `start-timescaledb`,
   `kafka-consumer`, `start-bridge`);

### Troubleshooting
- if the kafka async source is throwing errors like
  `Error: Not a message set. Magic byte is 2`, then according to
  [kafka-node issue #1208](https://github.com/SOHU-Co/kafka-node/issues/1208)
  the problem indicates that:
  > ... the `fetchMaxBytes` is set too low [so] the broker will start sending
  > fetch responses in RecordBatch format instead of MessageSet.


## TODO
- bridge remote writer that can write to fluentd -- unix domain socket, TCP
  socket, ...
- bridge asynchronous source that can accept and process data from
  [sg-logs](../sg-logs/README.md) via TCP socket;
