#!/bin/sh
#
# Start the kafka consumer => Timescaledb
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-kafka2tsdb.yml
