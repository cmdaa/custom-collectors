#!/bin/sh
#
# Start staitc prometheus => stdout
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-prom2stdout.yml $@
