#!/usr/bin/env node
/**
 *  Test the Prometheus AsyncSource's handling of incomplete lines caused by
 *  data that crosses the TCP packet threshold.
 *
 *  In version 0.1.0 after a number of such packets, there would be one which
 *  we would try to parse. This lead to improper sample names, label parsing
 *  issues, and general mayhem.
 *
 *  Version 0.1.1 includes a fix to address this issue by checking if the
 *  incoming buffer ends with '\n'. If it does not, the final "line" will be
 *  pulled aside awaiting the next incoming buffer.
 */
const Net     = require('net');
const Source  = require('../handlers/async_source/prometheus');
const config  = {
  url       : new URL( 'prometheus://localhost:3377' ),
  verbosity : 2,
};
const source  = new Source( config );

source._ready
  .then( res => {
    console.log('>>> Source Ready:', res);

    start_tests( source );
  })
  .catch( err => {
    console.error('*** Source Error:', err);
  });

/**
 *  Given a ready source, create a connection and send test samples.
 *  @method start_tests
 *  @param  src     The target async source instance {AsyncSource};
 *
 *  @return  void
 */
function start_tests( src ) {

  // Establish event listeners
  src
    .on('error', ({self, error}) => {
      console.error('*** Error Event:', error);
    })
    .on('sample', ({self, sample}) => {
      console.log('=== %s sample: %s', self, sample);
    })

  /* Create a TCP connection to this listener
   *    src.config.host:src.config.port
   */
  connect_and_send( src.config.host, src.config.port )
    .then( res => {
      console.log('>>> Samples done:', res);
    })
    .catch( err => {
      console.error('*** Sample error:', err);
    })
    .finally( () => {
      // Allow events to be processed
      setTimeout( () => {
        console.log('>>> Test complete');
        src.close();
      }, 100)
    });
}

/**
 *  Open a TCP connection to the given source (src.config.host:src.config.port)
 *  and send the set of test samples.
 *  @method connect_and_send
 *  @param  host    The target host {String};
 *  @param  port    The target port {Number};
 *
 *  @return A promise for results {Promise};
 */
function connect_and_send( host, port ) {
  const test_lines  = [
'device_storage_logpage{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",log_page="0x0,0",log_page_cost="0.25",log_page_level="0",log_page_name="supported log pages",metric_source="scsi-log-page:0x0,0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="supported",vendor_name="SEAGATE"} 1',
'device_storage_logpage{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",log_page="0x0,0",log_page_cost="0.25",log_page_level="0",log_page_name="supported log pages",metric_source="scsi-log-page:0x0,ff",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="supported",vendor_name="SEAGATE"} 1',
'device_storage_write_error{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x2,0:0x1:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="total corrected with possible delay",units="count",vendor_name="SEAGATE"} 0',
'device_storage_read_error{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x3,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="total corrected without substantial delay",units="count",vendor_name="SEAGATE"} 0',
'device_storage_verify_error{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x5,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="total corrected without substantial delay",units="count",vendor_name="SEAGATE"} 0',
'device_storage_nonmedium_error{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x6,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="total error count",units="count",vendor_name="SEAGATE"} 0',
'device_storage_parameter_unknown{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x8,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",value_raw="bytearray([255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255])",vendor_name="SEAGATE"} -1',
'device_storage_environment_temperature{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0xd,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="current",units="celsius",vendor_name="SEAGATE"} 29',
'device_storage_read{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x37,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="blocks",vendor_name="SEAGATE"} 3718338803',
'device_storage_write{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x37,0:0x1:8",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="blocks",vendor_name="SEAGATE"} 4283928526',
'device_storage_cache_read{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x37,0:0x2:16",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="blocks",vendor_name="SEAGATE"} 708836',
'device_storage_cache_io{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x37,0:0x3:24",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",stat_type="total (single segment)",units="commands",vendor_name="SEAGATE"} 7566946',
'device_storage_power_on{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x3e,0:0x0:0",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="hours",vendor_name="SEAGATE"} 12224.2',
'device_storage_smart_next_test{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="scsi-log-page:0x3e,0:0x8:8",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="minutes",vendor_name="SEAGATE"} 10',
'device_storage_smart_status{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="smart",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",smart_capable="True",smart_enabled="True",status="OK",units="failure",vendor_name="SEAGATE"} 0',
'device_storage_smart_temperature_warnings{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="smart",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="enabled",vendor_name="SEAGATE"} 1',
'device_storage_smart_next_test{dev_paths="/dev/sde,/dev/sg5",dev_type="disk",disk_type="hdd",metric_source="smart",model_id="ST12345AB678C",mount_points="",node="localhost.localdomain",revision_id="ABC0",serial_id="ZY9876543210X987WV65",units="minutes",vendor_name="SEAGATE"} 10',
  ];

  return new Promise( (resolve,reject) => {
    const socket = Net.createConnection( port, host );

    socket
      .on('connect', () => {
        const test_data = test_lines.join('\n');

        /* To trigger multiple packets we need more than 65536 bytes of data.
         *
         * To trigger our assembly bug, we need at least 6 of these 65536
         * bundles.
         */
        const count     = Math.ceil(65536 / test_data.length) * 7;

        console.log('=== Socket: connect, send %d lines %d times ...',
                    test_lines.length, count);

        for (let idex = 0; idex < count; idex++) {
          socket.write( test_data );
        }

        /*
        test_lines.forEach( line => {
          socket.write( line ); socket.write( '\n' );
        });
        // */

        setTimeout( () => {
          socket.destroy();
          return resolve( {lines: test_lines.length, count: count} );
        }, 50);

      })
      .on('error', (err) => {
        console.error('*** Socket: error:', err);
        socket.destroy();
        return reject( err );
      })
      .on('data', (data) => {
        // Log the response from the HTTP server.
        console.log('=== Socket: data[ %s ]', data);
      })
      .on('end', () => {
        console.log('=== Socket: end');
        socket.destroy();
        return resolve( 'end' );
      });
  });
}
