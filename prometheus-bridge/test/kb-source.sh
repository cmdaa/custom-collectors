#!/bin/sh
#
# Start the KernelBeat source (no writer)
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-kernelbeat-source.yml
