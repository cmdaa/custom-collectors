#!/bin/sh
#
# Start the KernelBeat source => Kafka producer
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-kernelbeat2kafka.yml
