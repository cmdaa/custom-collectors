#!/bin/sh
#
# Start the Prometheus Exporter => Kafka producer
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-exporter2kafka.yml
