## Keyboard-based flow control tests

These make use of the `kb_flow` option of the `stdout` remote writer to allow
keyboard-based flow control events:
- `ctl-s` : transmit off / xoff
- `ctl-q` : transmit on  / xon


### TCP endpoints

This test provides an example of flow control initiated via keyboard controls
on the sink, passing to the TCP sources, through TCP-based flow control, to the
metric source:
```
  Sink:
    stdout (w/keyboard-based flow control)
      : bridge event bus
        : TCP async sources <---------------+
            |                               |
            | TCP-based flow-control        | metrics data
            | (to all connected clients)    |
            |                               |
  Source:   v                               |
    fc-bridge-generator.js -----------------+
      TCP-based metrics generation with flow control and logging to stdout
```

Once both sink and source are started, you may use keyboard-based flow control
of the stdout remote writer within the sink to initiate flow-control events.


#### Sink
In one terminal, start a bridge with a configuration for two TCP async sources
and a stdout remote writer with keyboard-based flow control:
```
../bin/bridge -c config-tcp-fc-sink.yaml
```

The two TCP async sources will be:
- `127.0.0.1:3333` : `KernelBeatSource_tcp` : expects JSON-formatted,
  kernel-beat samples;
- `127.0.0.1:3377` : `PrometheusTcpSource` : expects Prometheus-formatted
  samples;


#### Source
The primary metric source for this test is a custom script that can feed either
of the two TCP async sources associated with the Sink. This custom script
supports TCP-based flow control.

To periodically send JSON-formatted, kernel-beat samples to the kernel beat
sink, in another terminal run:
```
./fc-bridge-generator.js -h 127.0.0.1 -p 3333 kernel-beat-load.json
```

To periodically send Prometheus-formatted, kernel-beat samples to the
Prometheus sink, in another terminal run:
```
./fc-bridge-generator.js -h 127.0.0.1 -p 3377 kernel-beat-load.prom
```

### Kafka endpoints

This test provides an example of flow control initiated via keyboard controls
on the sink, passing to the Kafka async source, through Kafka-based flow
control, to the Kafka remote writer, and reflected in TCP-based flow control in
the TCP async sources:
```
  Sink:
    stdout (w/keyboard-based flow control)
      : bridge event bus
        : Kafka async source <--------------+
            |                               |
            | Kafka-based flow-control      | metrics data
            | (to all subscribers)          |
            |                               |
  Source:   v                               |
    Kafka remote writer --------------------+
      : bridge event bus
        : Prometheus TCP async source <-----+
            |                               |
            | TCP-based flow-control        | metrics data
            | (to all connected clients)    |
            |                               |
            v                               |
  [ fc-bridge-generator.js -----------------+ ]
    Optional, TCP-based metrics generation with flow control and logging to
    stdout
```

Once both sink and source are started, you may use keyboard-based flow control
of the stdout remote writer within the sink to initiate flow-control events.



#### Sink
In one terminal, start a bridge with a configuration for a Kafka source and a
stdout remote writer with keyboard-based flow control:
```
../bin/bridge -c config-kafka-fc-sink.yaml
```

#### Source
In a second terminal, start a second bridge with a configuration for a
Prometheus TCP async source and Kafka remote writer:
```
../bin/bridge -c config-kafka-fc-source.yaml
```

Optionally, to perform a full end-to-end test that includes periodic metric
generation and full TCP-based flow-control, you could make use of the
`fc-bridge-generator.js` to connect to the Prometheus TCP async source. To do
so, in a third terminal, run:
```
./fc-bridge-generator.js -h 127.0.0.1 -p 3377 kernel-beat-load.prom
```
