#!/usr/bin/env node
const Fs    = require('fs');
const Net   = require('net');

class Client {
  static xoff  = Buffer.from( [19] );
  static xon   = Buffer.from( [17] );

  host  = '127.0.0.1';
  port  = 3377;
  file  = null;

  /**
   *  Create a new instance.
   *  @method constructor
   *  @param  args        Incoming arguments from yargs {Object};
   *  @param  args.host   The target TCP host {String};
   *  @param  args.port   The target TCP port {Number | String};
   *  @param  args.file   The source of the prometheus metrics {String};
   */
  constructor( args ) {
    /*
    console.log('=== xoff:', this.xoff);
    console.log('=== xon :', this.xon);
    // */

    if (args.host)  { this.host = args.host }
    if (args.port)  { this.port = args.port }
    if (args.file)  { this.file = args.file }

    Object.defineProperties( this, {
      sock    : { value: null,  writable: true },
      timeout : { value: 2000,  writable: true },
      _timer  : { value: null,  writable: true },
      flow    : { value: true,  writable: true },
      data    : { value: Fs.readFileSync( this.file ) },
    });

    process
      .on('SIGINT', (sig, code) => this.stop());
  }

  get xon()   { return this.constructor.xon }
  get xoff()  { return this.constructor.xoff }

  /**
   *  Connect to the configured flow-control host:port and the data send loop
   *  and flow-control monitoring.
   *  @method start
   */
  start() {
    this.sock = new Net.Socket();

    this.sock
      .on('connect', ()     => this._handleConnect() )
      .on('error',   (err)  => {
        console.log('*** sock.error:', err);
        this.stop();
      })
      .on('end',     ()     => this._handleEnd() )
      .on('data',    (data) => this._handleData( data ) );

    console.log('=== Connecting to %s:%s ...', this.host, this.port);
    this.sock.connect( this.port, this.host );
  }

  /**
   *  Shutdown and close our socket, terminating data generation and
   *  flow-control monitoring.
   *  @method stop
   */
  stop() {
    if (this.sock == null)  { return }

    this._stopGeneration();

    console.log('=== Disconnecting from %s:%s ...', this.host, this.port);

    this.sock.end();
    this.sock.destroy();
    //this.sock.unref();

    this.sock = null;
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Handle a socket 'connect' event.
   *  @method _handleConnect
   *
   *  @protected
   */
  _handleConnect() {
    console.log('>>> sock.connect');
    this._generateData();
  }

  /**
   *  Handle a socket 'end' event.
   *  @method _handleEnd
   *
   *  @protected
   */
  _handleEnd() {
    console.log('>>> sock.end');

    this._stopGeneration();
  }

  /**
   *  Data generation loop (write).
   *  @method _generateData
   *
   *  @protected
   */
  _generateData() {
    this._timer = setTimeout( () => {
      this._timer = null;

      // If our socket is closed or flow has been stopped, terminate the loop
      if (this.sock == null || ! this.flow) { return }

      console.log('=== send %d bytes', this.data.length);
      this.sock.write( this.data );

      // Run the genration loop again
      this._generateData();

    }, this.timeout);
  }

  /**
   *  Stop the data generation loop.
   *  @method _stopGeneration
   *
   *  @protected
   */
  _stopGeneration() {
    if (this._timer) { clearTimeout( this._timer ); this._timer = null }
  }

  /**
   *  Handle incoming flow-control data (read).
   *  @method _handleData
   *  @param  data    The incoming data {Buffer};
   *
   *  @protected
   */
  _handleData( data ) {
    switch( data[0] ) {
      case this.xoff[0]:  // Flow stop : xoff, SIGSTOP,  ^S, ASCII 19
        console.log('=== sock.xoff');
        this.flow = false;
        this._stopGeneration();
        break;

      case this.xon[0]:   // Flow start: xon,  SIGSTART, ^Q, ASCII 17
        console.log('=== sock.xon');
        this.flow = true;
        this._generateData( );
        break;

      default:
        console.log('=== sock.data:', data);
        break;
    }
  }
  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Main {
 *
 */
if (require.main === module) {
  // Parse command line options
  const args    = require('yargs')
    .usage( '$0 <file>',
            'Send the contents of the given file to the given TCP host:port',
            (yargs) => {
      yargs.positional('file', {
        describe    : 'The file containing the JSON KernelBeat record to send.',
        type        : 'string',
        demandOption: true,
      });
    })
    .options( {
      h: {
        alias       : 'host',
        default     : 'localhost',
        describe    : 'The target TCP host.',
        type        : 'string',
        demandOption: true,
      },
      p: {
        alias       : 'port',
        default     : 3377,
        describe    : 'The target TCP port.',
        type        : 'number',
        demandOption: true,
      },
      f: {
        alias       : 'file',
        describe    : 'The file containing the JSON KernelBeat record to send.',
        type        : 'string',
        demandOption: true,
      },
      '?': { alias : 'help' },
    })
    .help()
    .argv;

  const client  = new Client( args );
  client.start();
  
} else {
  module.exports = Client
}

/* Main }
 ****************************************************************************/
