#!/bin/sh
#
# Start the Prometheus TCP => stdout test
#
TST="$(realpath "$(dirname "$0")")"
BIN="$(realpath "${TST}/../bin")"

${BIN}/bridge -c ${TST}/config-tcp2stdout.yml $@
