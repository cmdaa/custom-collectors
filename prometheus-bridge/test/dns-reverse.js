const ReverseCache  = require('../lib/dns-reverse');
const cache         = new ReverseCache( {ttl: 14, verbosity: 1} );

function do_test( ip ) {
  cache.resolve( ip )
    .then( hostnames => {
      console.log('>>> Resolve of %s succeeded:', ip, hostnames);
    })
    .catch( err => {
      console.log('*** Resolve of %s FAILED:', ip, err);
    })
    .finally( () => {
      setTimeout( () => { do_test(ip) }, 1000 * 10 );
    });
}

do_test( '172.217.15.100' );
//do_test( '2607:f8b0:4004:c09::67' );  // Reverse IPv6 does not work
