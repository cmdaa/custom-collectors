#!/usr/bin/env python3
##
# Python3 client for flow control tests.
#
# Connects to a flow control server:
#   - listen for 'connect', 'error', 'end', and 'data'
#     - on 'connect': start a data generation loop
#     - on 'data'   : if xoff, pause  data generation
#                     if xon,  resume data generation
#
import sys
import socket
import select
import time
import signal

class Client(object): #{
  Port    = 9333
  Host    = '127.0.0.1'

  sock    = None
  timeout = 500

  flow    = True
  cnt     = 0

  xoff    = bytearray( [19] )
  xon     = bytearray( [17] )

  def __init__( self ): #{
    #print('=== xoff:', self.xoff)
    #print('=== xon :', self.xon)

    def _signal_handler( sig, frame ): self.stop()

    signal.signal( signal.SIGINT, _signal_handler )
  #}

  def start( self ): #{
    """
      Connect to the configured flow-control host:port and begin data
      generation and flow-control monitoring.
    """
    try: #{
      print('=== Connecting to %s:%s ...' % (self.Host, self.Port),
              file=sys.stderr, end='.')
      sys.stderr.flush()

      self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
      self.sock.connect( (self.Host, self.Port) )

      print('. connected', file=sys.stderr)

    except Exception as ex: #}{
      print(' failed: %s' % (ex), file=sys.stderr)

      sys.exit(-1)
    #}

    self.generateData()
  #}

  def stop( self ): #{
    """
      Shutdown and close our socket, terminating data generation and
      flow-control monitoring.
    """
    if self.sock is None: return

    print('=== Disconnecting from %s:%s ...' % (self.Host, self.Port))

    try: #{
      self.sock.shutdown( socket.SHUT_RDWR )
      self.sock.close()

    except Exception as ex: #}{
      print('*** socket.shutdown/close.error:', ex, file=sys.stderr)
    #}

    self.sock = None
  #}

  def generateData( self ): #{
    """
      While our socket exists, manage a select/read-write loop to handle flow
      control while generating a regular stream of outgoing data.
    """
    timeout_s = self.timeout / 1000

    while self.sock is not None: #{
      socks = [ self.sock ]

      try: #{
        read, write, err = select.select( socks, socks, socks )
      except select.error: #}{
        print('*** select error', file=sys.stderr)
        return self.stop()
      #}

      if read and len(read) > 0: #{
        # There is data to be read
        self.read()
      #}

      if write and len(write) > 0: #{
        # We can write data
        time.sleep( timeout_s )
        self.write()
      #}

      if err and len(err) > 0: #{
        # There is an error
        print('*** errors in select', file=sys.stderr)
        self.stop()
      #}
    #}
  #}

  def write( self ): #{
    """
      If our socket exists and the flow has not been paused, generate a write
      data.
    """
    if self.sock is None or self.flow is False: return

    try: #{
      buf = ('Cnt %d' % (self.cnt))
      self.cnt += 1

      print('=== send[ %s ]' % (buf))
      self.sock.send( bytes(buf, 'ascii') )

    except socket.error as ex: #}{
      print('*** socket.send.error:', ex, file=sys.stderr)
      self.stop()
    #}
  #}

  def read( self ): #{
    """
      If our socket exists, it should have data available to read.
      Read any pending data and check for flow control commands:
        xon   flow on
        xoff  flow off

      This will alter the `self.flow` flag.
    """
    if self.sock is None: return

    try: #{
      data = self.sock.recv( 512 )

    except socket.error as ex: #}{
      print('*** socket.recv.error:', ex, file=sys.stderr)
      return self.stop()
    #}

    if len(data) == 0: #{
      # select() said read data was available but there is no data
      #   The other end has hung up
      print('=== sock.hangup')
      return self.stop()
    #}

    if data[0] == self.xoff[0]: #{
      print('=== sock.xoff')
      self.flow = False

    elif data[0] == self.xon[0]: #}{
      print('=== sock.xon')
      self.flow = True

    else: #}{
      print('=== sock.data:', data)
    #}
  #}
#}

if __name__ == '__main__': #{
  # Create a new flow control client
  client = Client()

  client.start()
#}
