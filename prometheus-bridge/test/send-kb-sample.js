#!/usr/bin/env node
const Fs    = require('fs');
const Net   = require('net');

// Parse command line options
const args    = require('yargs')
  .usage( '$0 <file>',
          'Send the contents of the given file to the given TCP host:port',
          (yargs) => {
    yargs.positional('file', {
      describe    : 'The file containing the JSON KernelBeat record to send.',
      type        : 'string',
      demandOption: true,
    });
  })
  .options( {
    h: {
      alias       : 'host',
      default     : 'localhost',
      describe    : 'The target UDP host.',
      type        : 'string',
      demandOption: true,
    },
    p: {
      alias       : 'port',
      default     : 3333,
      describe    : 'The target UDP port.',
      type        : 'number',
      demandOption: true,
    },
    f: {
      alias       : 'file',
      describe    : 'The file containing the JSON KernelBeat record to send.',
      type        : 'string',
      demandOption: true,
    },
    '?': { alias : 'help' },
  })
  .help()
  .argv;

const data  = Fs.readFileSync( args.file );
const sock  = Net.createConnection( {host: args.host, port: args.port}, () => {
  console.log('>>> socket.connect: %s:%s ...', args.host, args.port);

  console.log('>>> Sending %d bytes of data ...', data.length);

  sock.write( data, err => {
    if (err) {
      console.error('*** send.error:', err);

    } else {
      console.log('>>> Sent %d bytes of data', data.length);
    }

    //sock.close();

    sock.end();
  });
});

sock
  .on('error', err => { console.error('*** socket.error:', err) } )
  .on('close', ()  => { console.log('>>> socket.close') } )
  .on('end',   ()  => { console.log('>>> socket.disconnected') } );
