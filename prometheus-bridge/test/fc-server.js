#!/usr/bin/env node
/**
 *  Server for flow control tests.
 *
 *  Creates a server instance:
 *    - listen for 'listening', 'connection', 'close', 'error'
 *      - on 'connection':
 *        - listen for 'data', 'error', and 'end'
 *        - start a flow control loop to:
 *              set a timeout to send xoff
 *              followed by a timeout to send xon
 *      - on 'end': terminate the flow control loop
 *
 *  :NOTE: This really only handles a single connection at a time.
 */
const Net       = require('net');
const Readline  = require('readline');

class Server {
  static xoff  = Buffer.from( [19] );
  static xon   = Buffer.from( [17] );

  host  = '127.0.0.1';
  port  = 9333;

  /**
   *  Create a new instance.
   *  @method constructor
   */
  constructor() {
    /*
    console.log('=== xoff:', this.xoff);
    console.log('=== xon :', this.xon);
    // */

    Object.defineProperties( this, {
      server: { value: null,  writable: true },

      paused: { value: false, writable: true },
      socks : { value: [], writable: true },
    });

    process
      .on('SIGINT', (sig, code) => this.stop());
 
    // Setup to receive keypress events
    Readline.emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    process.stdin.on('keypress', this._handleKeypress.bind(this));
  }

  get xon()   { return this.constructor.xon }
  get xoff()  { return this.constructor.xoff }

  /**
   *  Start our server and listen for incoming connections.
   *  @method start
   */
  start() {
    this.server = Net.createServer();

    this.server
      .on('listening',  ()     => this._handleListen() )
      .on('connection', (sock) => this._handleConnect( sock ) )
      .on('close',      ()     => { console.log('>>> server.close') })
      .on('error',      err    => { console.error('*** server.error:', err) });

    console.log('=== Starting server %s:%s ...', this.host, this.port);

    this.server.listen( this.port, this.host );
  }

  /**
   *  Stop our server.
   *  @method stop
   */
  stop() {
    if (this.server == null)  { return }

    console.log('=== Shutting down %s:%s ...', this.host, this.port);

    this.server.close();

    this.server = null;
  }

  /**
   *  Stop and terminate
   *  @method shutdown
   */
  shutdown() {
    this.stop();
    process.exit();
  }

  /**
   *  Set our paused state to true and send a pause/xoff to all current
   *  connections
   *  @method pause
   *
   *  @protected
   */
  pause( ) {
    const nclients  = this.socks.length;
    this.paused = true;

    console.log('=== pause : xoff => %d clients', nclients);
    if (nclients < 1) { return }

    this.socks.forEach( sock => {
      this._pause( sock );
    });
  }

  /**
   *  Set our paused state to false and send a resume/xon to all current
   *  connections
   *  @method resume
   *
   *  @protected
   */
  resume( ) {
    const nclients  = this.socks.length;
    this.paused = false;

    console.log('=== resume: xon  => %d clients', nclients);
    if (nclients < 1) { return }

    console.log('=== server.xon : %d clients', this.socks.length);

    this.socks.forEach( sock => {
      this._resume( sock );
    });
  }

  /**
   *  Toggle the pause state.
   *  @method toggle
   *
   *  @protected
   */
  toggle( ) {
    if (this.paused)  { this.resume() }
    else              { this.pause() }
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Send an pause/xoff to the given socket
   *  @method _pause
   *  @param  sock
   *
   *  @protected
   */
  _pause( sock ) {
    sock.write( this.xoff );
  }

  /**
   *  Send an resume/xon to the given socket
   *  @method _resume
   *  @param  sock
   *
   *  @protected
   */
  _resume( sock ) {
    sock.write( this.xon );
  }

  /**
   *  Handle a 'keypress' event
   *  @method _handleKeypress
   *  @param  str
   *  @param  key
   *
   *  @protected
   */
  _handleKeypress( str, key ) {
    switch( key.name ) {
      case 'space':
        // toggle the pause state
        this.toggle();
        break;

      case 'q':
        return this.shutdown();

      case 'c':
        if (key.ctrl) {
          return this.shutdown();
        }
        // Fall-through

      default:
        console.log(`Ignore the "${str}" key:`, key);
        break;
    }
  }

  /**
   *  Handle a socket 'listen' event.
   *  @method _handleListen
   *
   *  @protected
   */
  _handleListen( ) {
    const addr  = this.server.address();

    console.log('>>> server.listening on %s:%s', addr.address, addr.port);
  }

  /**
   *  Handle a new, incoming connection.
   *  @method _handleConnect
   *  @param  sock      The socket for the new, incoming connection {Socket};
   *
   *  @protected
   */
  _handleConnect( sock ) {
    this.socks.push( sock );
    sock.__id = this.socks.length;

    console.log('>>> server.connection: %s:%s : sock#%d ...',
                sock.remoteAddress, sock.remotePort, sock.__id);

    sock
      .on('data', data => {
        console.log('=== sock#%d.data: %s', sock.__id, data);
      })
      .on('error', err => {
        console.log('*** sock#%d.error:', sock.__id, err);
      })
      .on('end', () => {
        console.log('=== sock#%d.end', sock.__id);

        const idex = this.socks.indexOf( sock );
        if (idex >= 0) {
          this.socks.splice( idex, 1 );
        } else {
          console.error('*** Cannot find socket #%d', sock.__id);
        }
      });

    if (this.paused) {
      this._pause( sock );
    }
  }

  /* Protected methods }
   **************************************************************************/
}

if (require.main === module) {
  server = new Server();
  server.start();

} else {
  module.exports = Server

}
