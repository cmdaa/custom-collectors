#!/usr/bin/env node
/**
 *  Test the Prometheus RemoveWriter.
 */
const EventEmitter  = require('events');
const Writer        = require('../handlers/remote_write/prometheus');
const Sample        = require('../lib/sample');
const url           = 'prometheus://cmdaa-promscale-connector:9201/write';
const config        = {
  bridge            : new EventEmitter(),
  url               : new URL( url ),
  verbosity         : 2,

  batch_num_samples : 1000,
  batch_timeout     : '1s',
  max_resends       : 0,
};
const writer        = new Writer( config );
let   timer;

writer.start()
  .then( res => {
    const timeout     = 10000;
    const num_samples = 100;
    const num_repeats = 2;

    console.log('>>> Writer Ready:', res);

    start_tests( writer, timeout, num_samples, num_repeats );
  })
  .catch( err => {
    console.error('*** Writer Error:', err);
  });


/**
 *  Given a ready writer, send test samples.
 *  @method start_tests
 *  @param  writer                The target writer {RemoteWriter};
 *  @param  [timeout = 10000]     The time (ms) between sends {Number};
 *  @param  [num_samples = 100]   The number of samples to generate/send
 *                                {Number};
 *  @param  [num_repeats = 0]     The number of times to repeat the send
 *                                {Number};
 *
 *  @return  void
 */
function start_tests( writer, timeout     = 10000,
                              num_samples = 100,
                              num_repeats = 0) {

  // Establish event listeners
  writer
    .on('error', ({self, error}) => {
      console.error('*** Error Event:', error);
    });

  periodic_sends( writer, timeout, num_samples, num_repeats );
}

/**
 *  Helper to periodically send samples.
 *  @param  writer                The target writer {RemoteWriter};
 *  @param  [timeout = 10000]     The time (ms) between sends {Number};
 *  @param  [num_samples = 100]   The number of samples to generate/send
 *                                {Number};
 *  @param  [num_repeats = 0]     The number of times to repeat the send
 *                                {Number};
 *
 *  @return void
 */
function periodic_sends( writer,  timeout     = 10000,
                                  num_samples = 100,
                                  num_repeats = 0) {

  send_samples( writer, num_samples, num_repeats );

  setTimeout( () => {
    periodic_sends( writer, timeout, num_samples, num_repeats );
  }, timeout);
}

/**
 *  Helper to send metric samples.
 *  @method send_samples
 *  @param  writer                The target writer {RemoteWriter};
 *  @param  [num_samples = 100]   The number of samples to generate/send
 *                                {Number};
 *  @param  [num_repeats = 0]     The number of times to repeat the send
 *                                {Number};
 *
 *  @return void
 */
function send_samples( writer,  num_samples = 100,
                                num_repeats = 0 ) {
  /*  Each entry in `entries` should be:
   *    { labels  : [ {name:'__name__', value: metricName}, ... ],
   *      samples : [ {timestamp: , value: }, ... ]
   *    }
   */
  const now           = Date.now();
  const samples       = [];
  const metric_name   = 'test_duplicate';
  const metric_class  = (metric_name.split('_').shift());
  let   send_cnt      = 0;

  // Generate samples
  for (let idex = 0; idex < num_samples; idex++) {
    const sample = new Sample( {
      name          : metric_name,
      metric_class  : metric_class,
      labels        : [],
      timestamp     : now + idex,
      value         : Math.random() * 1000,
    });

    samples.push( sample );
  }

  do {
    console.log('>>> Send %d samples (%d / %d) ...',
                samples.length, send_cnt+1, num_repeats+1);

    // Emit 'sample' events, one for each sample
    samples.forEach( sample => writer.emit( 'sample', sample ) );
  } while (send_cnt++ < num_repeats );
}
