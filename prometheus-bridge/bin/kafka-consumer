#!/usr/bin/env node
const Kafka         = require('kafka-node');
const args          = require('yargs')
  .options( {
    h: {
      alias       : 'host',
      default     : 'kafka:9092',
      describe    : 'The Kafka host:port.',
      type        : 'string',
      demandOption: true,
    },
    p: {
      alias       : 'partition',
      default     : 0,
      describe    : 'The Kafka partition.',
      type        : 'number',
      demandOption: true,
    },
    t: {
      alias       : 'topic',
      default     : 'metrics',
      describe    : 'The Kafka topic.',
      type        : 'string',
      demandOption: true,
    },
  })
  .argv;

const config  = {
  topics  : [ {topic: args.topic} ],
  client  : {
    kafkaHost     : args.host,
    connectTimeout: 2000,

    connectRetryOptions: { // node-retry
      retries   : 0,
      factor    : 2,
      minTimeout: 1000,
      maxTimeout: 2000,
      randomize : true,
    },
  },
  consumer: {
    //autoCommit    : false,
    fetchMaxWaitMs: 1000,
    fetchMaxBytes : 1024 * 1024,
  },
};

const client  = new Kafka.KafkaClient( config.client );
client
  .on('ready', () => {
    console.log('=== client.ready');
  })
  .on('connect', () => {
    console.log('=== client.connect');
  })
  .on('brokersChanged', () => {
    console.log('=== client.brokersChanged');
  })
  .on('reconnect', () => {
    console.log('=== client.reconnect');
  })
  .on('socket_error', err => {
    console.log('=== client.socket_error:', err);
  })
  .on('close', self => {
    console.log('=== client.close');
  })
  .on('error', (err) => {
    console.log('*** client.error: %s', err);
  })
  .on('data', data => {
    console.log('=== client.data:', data);
  });

const consumer  = new Kafka.Consumer( client, config.topics, config.consumer );
consumer
  .on('ready', () => {
    console.log('=== consumer.ready');
  })
  .on('error', err => {
    console.error('*** consumer.error: %s', err);
  })
  .on('message', msg => {
    if (typeof(msg.value) === 'string') {
      try {
        const obj = JSON.parse( msg.value );
        msg.value = obj;
      } catch(ex) { /* leave msg.value alone */ }
    }

    console.log('>>> msg: %s', JSON.stringify(msg, null, 2));
  })
  .on('offsetOutOfRange', topic => {
    console.error('*** Out of range error for topic: %s', topic);

    topic.maxNum = 2;

    const offset  = new Kafka.Offset( client );
    offset.fetch([topic], (err, offsets) => {
      if (err) {
        console.error('*** Offset.fetch error: %s', err);
        return;
      }

      const min = Math.min.apply( null, offsets[topic.topic][topic.partition] );
      consumer.setoffset( topic.topic, topic.partition, min );
    });
  });

// vi: ft=javascript
