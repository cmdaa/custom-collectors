#!/usr/bin/env node
const Fs        = require('fs');
const Yaml      = require('js-yaml');
const Utils     = require('../lib/utils');

/*
function consoleLoggerProvider( name ) {
  return {
    debug: console.debug.bind( console ),
    info:  console.info.bind( console ),
    warn:  console.warn.bind( console ),
    error: console.error.bind( console ),
  };
}

const Logging   = require('kafka-node/logging');
Logging.setLoggerProvider( consoleLoggerProvider );
// */

const Kafka     = require('kafka-node');

process
  .on('unhandledRejection', (reason, promise) => {
    console.log('*** Unhandled Rejection at:', reason.stack || reason);
  })


const path      = './test/config.yml';
const defaults  = {
  topic             : 'metrics',  // kafka default: 'metrics'
  compression       : 'none',     // kafka default: 'none'

  connect_timeout   : '10s',      // kafka default: '10s'
  idle_timeout      : '5m',       // kafka default: '5m'
  request_timeout   : '10s',      // kafka default: '30s'

  batch_num_samples : 10000,      /* The number of samples to batch within
                                   * a single push
                                   */
  batch_timeout     : '500ms',    /* The time to wait for additional
                                   * samples, up to `batch_num_samples`,
                                   * before sending (ms)
                                   */
};
const yaml      = Yaml.safeLoad( Fs.readFileSync( path, 'utf8' ) )
                    .remote_write.find( rec => rec.kafka_config );
const config    = Utils.convertDurations(
                    Utils.expandValues(
                      Object.assign( {url:yaml.url},
                                     defaults,
                                     yaml.kafka_config )
                    )
                  );

/*
console.log('config:', JSON.stringify(config, null, 2));
process.exit(-1);
// */

const url         = new URL( config.url );
const kafkaConfig = {
  kafkaHost     : url.hostname.split(',')
                    .map( str => `${str}:${url.port}` )
                    .join(','),

  connectRetryOptions: { // node-retry
    retries   : 0,
    factor    : 2,
    minTimeout: 1 * 1000,
    maxTimeout: config.connect_timeout,
    randomize : true,
  },
  connectTimeout: config.connect_timeout,
  idleConnection: config.idle_timeout,
  requestTimeout: config.request_timeout,

  // Available at send() via kafkaClient.options.topic
  topic         : (url.pathname.length > 1
                    ? url.pathname.slice(1)
                    : config.topic),
};

console.log('>>> Connecting to Kafka @ %s ...', kafkaConfig.kafkaHost);
const client  = new Kafka.KafkaClient( kafkaConfig );
const admin   = new Kafka.Admin( client );

client
  .on('ready', () => {
    console.log('=== client.ready');
  })
  .on('connect', () => {
    console.log('=== client.connect');
  })
  .on('brokersChanged', () => {
    console.log('=== client.brokersChanged');
  })
  .on('reconnect', () => {
    console.log('=== client.reconnect');
  })
  .on('socket_error', err => {
    console.log('=== client.socket_error:', err);
  })
  .on('close', self => {
    console.log('=== client.close');
  })
  .on('error', (err) => {
    console.log('*** client.error: %s', err);
  })
  .on('data', data => {
    console.log('=== client.data:', data);
  });

admin
  .on('ready', () => {
    console.log('=== admin.ready');
  })
  .on('connect', () => {
    console.log('=== admin.connect');
  })
  .on('error', (err) => {
    console.log('*** admin.error: %s', err);
  });

console.log('>>> listTopics ...');
admin.listTopics( (err, res) => {
  if (err) {
    console.error('*** listTopics failed: %s', err);

  } else {
    console.log('>>> listTopics:', JSON.stringify(res, null, 2));
  }

  client.close( () => {
    console.log('>>> client close completed');
  });
});

/*
const offset    = new Kafka.Offset( client );

const topics    = [ {topic: kafkaConfig.topic} ];
const opts      = {
  //autoCommit    : false,
  fetchMaxWaitMs: 1000,
  fetchMaxBytes : 1024 * 1024,
};
const consumer  = new Kafka.Consumer( client, topics, opts );

consumer
  .on('message', msg => {
    if (typeof(msg.value) === 'string') {
      try {
        const obj = JSON.parse( msg.value );
        msg.value = obj;
      } catch(ex) {
        // leave msg.value alone
      }
    }

    console.log('>>> msg: %s', JSON.stringify(msg, null, 2));
  })
  .on('offsetOutOfRange', topic => {
    console.error('*** Out of range error for topic: %s', topic);

    topic.maxNum = 2;
    offset.fetch([topic], (err, offsets) => {
      if (err) {
        console.error('*** Offset.fetch error: %s', err);
        return;
      }

      const min = Math.min.apply( null, offsets[topic.topic][topic.partition] );
      consumer.setoffset( topic.topic, topic.partition, min );
    });
  })
  .on('error', err => {
    console.error('*** Error: %s', err);
  });
// */
