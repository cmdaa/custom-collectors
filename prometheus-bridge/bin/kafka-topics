#!/usr/bin/env node
const Kafka         = require('kafka-node');
const args          = require('yargs')
  .options( {
    h: {
      alias       : 'host',
      default     : 'kafka:9092',
      describe    : 'The Kafka host:port.',
      type        : 'string',
      demandOption: true,
    },
    p: {
      alias       : 'partition',
      default     : 0,
      describe    : 'The Kafka partition.',
      type        : 'number',
      demandOption: true,
    },
    a: {
      alias       : 'attributes',
      default     : 0,
      describe    : 'The Kafka attributes.',
      type        : 'number',
      demandOption: true,
    },
  })
  .argv;

const config  = {
  client  : {
    kafkaHost     : args.host,
    connectTimeout: 2000,

    connectRetryOptions: { // node-retry
      retries   : 0,
      factor    : 2,
      minTimeout: 1000,
      maxTimeout: 2000,
      randomize : true,
    },
  },
};

const client  = new Kafka.KafkaClient( config.client );
client
  .on('ready', () => {
    console.log('=== client.ready');
  })
  .on('connect', () => {
    console.log('=== client.connect');
  })
  .on('brokersChanged', () => {
    console.log('=== client.brokersChanged');
  })
  .on('reconnect', () => {
    console.log('=== client.reconnect');
  })
  .on('socket_error', err => {
    console.log('=== client.socket_error:', err);
  })
  .on('close', self => {
    console.log('=== client.close');
  })
  .on('error', (err) => {
    console.log('*** client.error: %s', err);
  })
  .on('data', data => {
    console.log('=== client.data:', data);
  });

const admin = new Kafka.Admin( client );
admin.listTopics( (err, res) => {
  if (err) {
    console.error('*** admin.listTopics(): FAILED: %s', err);

  } else {
    console.log('>>> admin.listTopics(): %s', JSON.stringify(res, null, 2));
  }

  client.close();
});

// vi: ft=javascript
